﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintServiceInvoice.aspx.cs"
    Inherits="PrintServiceInvoice" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Verdana; background: none; font-size: 10px;">
    <form id="form1" runat="server">
    <div style="width: 650px; margin-left: 75px; text-align: center">
        <div style="text-align: left; width: 650px; float: left">
            <table style="width: 650px; float: left; font-size: 14px; border-bottom: solid 1px black;
                font-weight: bold;">
                <tr>
                    <td>
                        <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                        <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                        <span id="spAddress2" runat="server">
                            <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                        </span><span id="spAddress3" runat="server">
                            <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                        </span>
                        <asp:Label ID="lblCity" runat="server"></asp:Label>,
                        <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                        <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                    </td>
                    <td style="text-align: right; vertical-align: top">
                        <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2" style="text-align: center">
                        <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Service
                            Invoice<br />
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    </td>
                </tr>
            </table>
        </div>
        <div style="text-align: left; width: 650px; float: left">
            <table style="width: 100%">
                <tr>
                    <td>
                        <asp:Label Text="Customer" Style="font-weight: bold; font-size: 14px; text-decoration: underline"
                            runat="server" ID="lblCustomer"></asp:Label><br />
                        <asp:Label ID="lblBuyerName" Style="font-weight: bold; font-size: 12px;" runat="server"></asp:Label><br />
                        <asp:Label ID="lblBuyerAddress" runat="server"></asp:Label><br />
                        <asp:Label ID="lblBuyerAddress2" runat="server"></asp:Label><br />
                        <asp:Label ID="lblBuyerAddress3" runat="server"></asp:Label><br />
                        <asp:Label ID="lblBuyerTelephone" runat="server"></asp:Label><br />
                        <%--                        <asp:Label ID="lblBuyerRegNo" runat="server"></asp:Label><br />
                        <asp:Label ID="lblBuyerNTN" runat="server"></asp:Label>--%>
                    </td>
                    <td style="float: right">
                        <table style="float: right; width: 200px; border-collapse: collapse" border="1">
                            <tr>
                                <td style="text-align: lef; height: 30px;">
                                    <b>Invoice No. </b>
                                </td>
                                <td style="padding-left: 10px">
                                    <asp:Label ID="lblVoucherNo" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: lef; height: 30px;">
                                    <b>Invoice Date</b>
                                </td>
                                <td style="padding-left: 10px">
                                    <asp:Label ID="lblVoucherDate" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                ShowFooter="true" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                HeaderStyle-CssClass="gridheader" AlternatingItemStyle-CssClass="gridAlternateItem"
                GridLines="none" BorderColor="black" FooterStyle-CssClass="gridFooter" BorderStyle="Dotted"
                BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery_RowDataBound">
                <Columns>
                    <asp:TemplateColumn HeaderText="S.No.">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                        <ItemTemplate>
                            <%# Container.DataSetIndex + 1%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Description">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="left" Width="65%" />
                        <ItemTemplate>
                            <%# Eval("Description")%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Amount">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemStyle HorizontalAlign="Right" Width="20%" />
                        <FooterStyle HorizontalAlign="Right" Font-Size="12px" />
                        <ItemTemplate>
                            <%#String.Format("{0:C}", Eval("Amount")).Replace('$', ' ')%>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                        </FooterTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
        </div>
        <div style="text-align: left; width: 650px; float: left">
            <table style="width: 650px; float: left; font-size: 10px;">
                <tr>
                    <td>
                        <b>
                            <asp:Label ID="lblCurrency" runat="server"></asp:Label>
                            :</b>
                        <asp:Label ID="lblTotalEng" runat="server"></asp:Label>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table width="650px">
                            <tr>
                                <td style="padding: 10px; text-align: center">
                                    <hr />
                                    <br />
                                    Prepared By
                                </td>
                                <td style="padding: 10px; text-align: center">
                                    <hr />
                                    <br />
                                    Authorized By
                                </td>
                                <td style="padding: 10px; text-align: center">
                                    <hr />
                                    <br />
                                    Approved By
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <br />
                        <br />
                        <br />
                    </td>
                </tr>
                 <tr>
                <td style="text-align: right">
                    <asp:LinkButton Text="Print" ID="lnkPrint" OnClientClick="window.print();" runat="server"></asp:LinkButton>
                </td>
            </tr>
                <tr>
                    <td style="text-align: center" colspan="4">
                        <hr />
                        <table style="width: 650px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>
                                <script type="text/javascript">

                                    var currentTime = new Date()
                                    var hours = currentTime.getHours()
                                    var Newhours = 0;
                                    var minutes = currentTime.getMinutes()
                                    if (minutes < 10) {
                                        minutes = "0" + minutes
                                    }
                                    if (hours > 12) {
                                        Newhours = hours - 12;
                                    }
                                    else {
                                        Newhours = hours;
                                    }
                                    document.write(Newhours + ":" + minutes + " ")
                                    if (hours > 11) {
                                        document.write("PM")
                                    } else {
                                        document.write("AM")
                                    }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                        Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                            sales@accmaster.com</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
