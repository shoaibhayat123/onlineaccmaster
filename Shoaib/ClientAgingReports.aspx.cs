﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class ClientAgingReports : System.Web.UI.Page
{
    Reports objReport = new Reports();
    clsCookie ck = new clsCookie();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        if (!IsPostBack)
        {
            FillGroup();
            ddlGroup.Focus();
            txtstartDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
        }
    }

    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        string urllast;
        urllast = "PrintClientAgingReports.aspx?AsOn=" + txtstartDate.Text + "&Acfile=" + ddlGroup.SelectedValue;
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);

    }
    #region Fill  Client A/C
    protected void FillGroup()
    {

        //objReport.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        //DataTable dtClient = new DataTable();
        //dtClient = objReport.g();
        //if (dtClient.Rows.Count > 0)
        //{
        //    ddlGroup.DataSource = dtClient;
        //    ddlGroup.DataTextField = "Title";
        //    ddlGroup.DataValueField = "Id";
        //    ddlGroup.DataBind();
        //    ddlGroup.Items.Insert(0, new ListItem("Select Group A/c", "0"));

        //}

        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtClient = new DataTable();
        dtClient = objSalesInvoice.getAcFileBySundryDebtorOnly();
        if (dtClient.Rows.Count > 0)
        {
            ddlGroup.DataSource = dtClient;
            ddlGroup.DataTextField = "Title";
            ddlGroup.DataValueField = "Id";
            ddlGroup.DataBind();
            ddlGroup.Items.Insert(0, new ListItem("Select Client A/c", "0"));

        }

    }
    #endregion
}