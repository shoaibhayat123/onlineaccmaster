﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class _Default : System.Web.UI.Page 
{
    User objUser = new User();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnLogin_Click(object sender, ImageClickEventArgs e)
    {
        if (txtUsername.Text != "" && txtPassword.Text != "")
        {
            DataSet ds = new DataSet();
            objUser.Username = txtUsername.Text.Trim().Replace("'", "").ToString();
            objUser.UserPassword = txtPassword.Text.Trim().Replace("'", "").ToString();
            objUser.CustomerCode=decimal.Parse(txtCustomerCode.Text);
            
            ds = objUser.GetLogin();

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(ds.Tables[0].Rows[0]["UserPassword"]) == txtPassword.Text.Trim().Replace("'", "").ToString())
                {
                    objUser.getAccountingYear();
                    lblError.Visible = false;
                    DataSet ds2 = new DataSet();
                    ds2 = objUser.GetRemainingDays();

                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        if (Convert.ToInt32(ds2.Tables[0].Rows[0]["remainingday"]) > 0 && Convert.ToInt32(ds2.Tables[0].Rows[0]["remainingday"]) < 16)
                        {
                            string result = "Your Account will expire in " + Convert.ToString(ds2.Tables[0].Rows[0]["remainingday"]) + " Days, Renew your account to continue our services.";
                            Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowStatus", "javascript:alert('" + result + "'); location.href='Welcome.aspx'", true);
                        }
                        else
                        {
                            if ((Convert.ToInt32(ds2.Tables[0].Rows[0]["remainingday"])) <= 0)
                            {
                                objUser.updatecustomerAccountBlocked();

                            }
                            else
                            {
                                Response.Redirect("Welcome.aspx");
                            }
                        }
                    }
                    else
                    {

                        Response.Redirect("Welcome.aspx");
                    }
                }
                else
                {
                    lblError.Visible = true;
                }
            }
            else
            {
                lblError.Visible = true;
            }
        }
    }
    
    protected void btnForget_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        objUser.UserID = txtUsername.Text.Trim().Replace("'", "").ToString();
        objUser.CustomerCode = decimal.Parse(txtCustomerCode.Text);
        dt = objUser.getForgetPassword();
        if (dt.Rows.Count > 0)
        {
            SendMail(dt);
            txtCustomerCode.Text = "";
            txtUsername.Text = "";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowStatus", "javascript:alert('Your password has been sent to your administrator');", true);
        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowStatus", "javascript:alert('Enter Valid Details');", true);
        }




    }

    protected void SendMail(DataTable dt)
    {

        string[] strArray = new string[7];
        strArray[0] = dt.Rows[0]["CustomerName"].ToString();
        strArray[1] = dt.Rows[0]["CustomerCode"].ToString();
        strArray[2] = dt.Rows[0]["UserID"].ToString();
        strArray[3] = dt.Rows[0]["UserID"].ToString();
        strArray[4] = dt.Rows[0]["CustomerCode"].ToString();
        strArray[5] = dt.Rows[0]["UserID"].ToString();
        strArray[6] = dt.Rows[0]["UserPassword"].ToString();
        
        EmailSender objMail = new EmailSender();
        objMail.ValueArray = strArray;
        objMail.MailTo = dt.Rows[0]["PrimaryEmail"].ToString();  
        objMail.Subject = "Password recovery request";
        objMail.MailBcc = "support@accmaster.com";
        objMail.TemplateDoc = "ForgetPassword.htm";
        objMail.Send();

    }
}
