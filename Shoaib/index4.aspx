﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaticPages.master" AutoEventWireup="true"
    CodeFile="~/index4.aspx.cs" Inherits="index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div id="content_wraper5">
        <div class="hm-slide-box">
            
            <div class="center-box-slide">

            
                 <div class="slide-sml-box"><!--open slide-sml-box-->
    <div class="heading1">Xero is online accounting software for your small business</div>
    <div class="slide-p">See why over 600,000 subscribers choose AccMaster</div>
                     <div class="box1">
                         <a href="#">
                             <div class="try-btn">Try Acc for free</div> </a>
                     </div>
    
    </div><!--close slide-sml-box-->
    
     </div><!--close center box-->
    
                    <div class="hm-slide-box-image"></div><!--close hm-slide-box-image-->
            </div><!--close hm-slid box-->

                
            <div class="accmiddle-bg">
                <div class="top-heading" style=" padding-left:39px;">
                    <h1>
                        AccMaster Complete Accounting Solution to Systematize Your Financial Records In Single Place.</h1>
                </div>
                <div class="slider-backgourng">
                    <div class="slider-bg">
                        <div class="slider-left-part">
                            <div class="left-part-title">
                                <h1>
                                    AccMaster Online</h1>
                            </div>
                            <div class="save">
                                <span class="savebox">Effective way to manage your accounts online with AccMaster Complete
                                    Accounting Solution, Use it from any where from any operating system, it is compatible
                                    with all browsers, even fetch reports from your mobile.</span><br />
                                <%-- <ul>
                                    <li><a href="#">Learn more</a></li>
                                </ul>--%>
                            </div>
                            <div class="button" style="padding-top: 15px;">
                                <ul>
                                    <li><a href="register.aspx">Try It Free</a></li>
                                </ul>
                            </div>
                            <div class="credit">
                                <span class="creditcard"><strong>No credit card required</strong><br />
                                    Free for 30 days.</span>
                            </div>
                        </div>
                        <div class="slider-right-part">
                            <div class="slider-content-box">
                                <div class="slider-content-title">
                                    <h1 style="text-decoration: underline">
                                        AccMaster Include </h1>
                                </div>
                                <div class="list-items">
                                    <ul style=" margin-top:25px">
                                        <li style="line-height: 18px;">
                                            <img src="images/right-mark.png" width="17" height="15" alt="" class="right-mark" /><span
                                                class="anytime" style=" margin-bottom:10px">Get Access of your data from anywhere , from any place.</span></li>
                                       <li style="line-height: 18px">
                                            <img src="images/right-mark.png" width="17" height="15" alt="" class="right-mark" /><span
                                                class="anytime"  style=" margin-bottom:10px">Free access for iphone, Blackberry, or Android</span></li>
                                        <li style="line-height: 18px;">
                                            <img src="images/right-mark.png" width="17" height="15" alt="" class="right-mark" /><span
                                                class="anytime"  style=" margin-bottom:10px">Free upgrades and automatic backup data facility</span></li>
                                    </ul>
                                </div>
                                <div class="protected-data">
                                    <span class="protdata">Your data is 100% protected
                                        <br />
                                        See our<a href="security.aspx">  security measures. </a>
                                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--close accmiddle-bg-->
            

        <div id="content_midd5">
            

            <div class="accmiddle-bg-2">
                <div class="accmiddle-backg">
                    <div class="content-bg">
                        <div class="content-heading">
                            <span class="heading">Save time</span>
                        </div>
                        <div class="content-heading2">
                            <span class="heading">Go Global</span>
                        </div>
                        <div class="content-heading3">
                            <span class="heading">Real Accounting</span>
                        </div>
                        <div class="content-heading4">
                            <span class="heading">Secure. Private. Reliable.</span>
                        </div>
                    </div>
                    <div class="all-image-box">
                        <div class="first-iamge">
                            <div class="image-content">
                                <div class="oss-icon">
                                    <img src="images/ie-icon.png" alt="" />
                                </div>
                                <div class="oss-icon-heading">
                                    <span class="iconheding" style="color: #F68C34">Online AccMaster </span>
                                    <br />
                                    <span class="iconheding" style="color: #F68C34">Simple</span>
                                </div>
                                <div class="free-days">
                                    <span class="frda"><b>30 days free </b>
                                        <br />
                                        then $11.95/month*</span>
                                </div>
                                <div class="try-it-free-icon-bg">
                                    <ul>
                                        <li><a href="register.aspx">Try It Free</a></li>
                                    </ul>
                                </div>
                                <div class="easy-create-invoice">
                                    <span class="easycreatin">Create vouchers,
                                        <br />
                                        make invoices and keep 
                                        <br />
                                        track of receivable & payables. </span>
                                </div>
                            </div>
                        </div>
                        <div class="second-image">
                            <div class="image-content2">
                                <div class="oss-icon">
                                    <img src="images/ie-icon.png" alt="" />
                                </div>
                                <div class="oss-icon-heading2">
                                    <span class="iconheding" style="color: #28ADE3;">Online AccMaster</span><br />
                                    <span class="iconheding" style="color: #28ADE3;">Basic</span>
                                </div>
                                <div class="free-days">
                                    <span class="frda"><b>30 days free </b>
                                        <br />
                                        then $19.95/month*</span>
                                </div>
                                <div class="try-it-free-icon-bg">
                                    <ul>
                                        <li><a href="register.aspx">Try It Free</a></li>
                                    </ul>
                                </div>
                                <div class="easy-create-invoice">
                                    <span class="easycreatin">Maintain Inventory,
                                        <br />
                                        manage cost centers,and
                                        <br />
                                         view different type of reports. </span>
                                </div>
                            </div>
                        </div>
                        <div class="thirld-image">
                            <div class="image-content3">
                                <div class="oss-icon">
                                    <img src="images/ie-icon.png" alt="" />
                                </div>
                                <div class="oss-icon-heading3">
                                    <span class="iconheding" style="color: #F5A71B">Online AccMaster</span><br />
                                    <span class="iconheding" style="color: #505050;">Extra</span>
                                </div>
                                <div class="free-days">
                                    <span class="frda"><b>30 days free </b>
                                        <br />
                                        then $27.95/month*</span>
                                </div>
                                <div class="try-it-free-icon-bg">
                                    <ul>
                                        <li><a href="register.aspx">Try It Free</a></li>
                                    </ul>
                                </div>
                                <div class="easy-create-invoice">
                                    <span class="easycreatin">Manage expenses,
                                        <br />
                                        and income keeps <br />track of your bottom line </span>
                                </div>
                            </div>
                        </div>
                        <div class="fourth-image">
                            <div class="image-content4">
                                <div class="oss-icon">
                                    <img src="images/ie-icon.png" alt="" />
                                </div>
                                <div class="oss-icon-heading4">
                                    <span class="iconheding" style="color: #0989BB">Online AccMaster<br />
                                    </span><span class="iconheding" style="color: #505050;">Plus Plus</span>
                                </div>
                                <div class="free-days">
                                    <span class="frda"><b>30 days free </b>
                                        <br />
                                        then $35.95/month*</span>
                                </div>
                                <div class="try-it-free-icon-bg">
                                    <ul>
                                        <li><a href="register.aspx">Try It Free</a></li>
                                    </ul>
                                </div>
                                <div class="easy-create-invoice">
                                    <span class="easycreatin">Specially designed brokerage
                                        <br />
                                        system to maintain your income
                                        <br />
                                        with detailed ledgers. </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </span>
    </div>
           
</div>
</div><!--close content_wraper5-->
</asp:Content>
    