﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintStockAdjustment.aspx.cs"
    Inherits="PrintStockAdjustment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .gridItem
        {
            background: #ffffff;
        }
        .gridItem td
        {
            border: 1px dotted #000000;
            padding-left: 10px;
        }
        
        
        .gridheader
        {
            background: #85A68C;
            color: #ffffff;
        }
        .gridheader td
        {
            border: 1px dotted #000000;
        }
        .gridAlternateItem
        {
            background: #DDEEDC;
        }
        .gridAlternateItem td
        {
            border: 1px dotted #000000;
            padding-left: 10px;
        }
        
        .gridpadding
        {
            padding-left: 10px;
        }
        .gridFooter
        {
            background: #85A68C;
            color: Black;
            font-size: 9px;
            padding-left: 5px;
            font-weight: bold;
            text-align: right;
        }
        
        .gridItems
        {
            background: #ffffff;
        }
        /* For char of account report*/
        .gridAlternateItems
        {
            background: #DDEEDC;
        }
    </style>
</head>
<body style="font-family: Verdana; background: none; font-size: 10px;">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; border-bottom: solid 1px black;
            font-weight: bold;">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <b>Supplier Name</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblSuplierName" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">
                                <b>Address</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                                <span id="spAddress2" runat="server">
                                    <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                                </span><span id="spAddress3" runat="server">
                                    <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Telephone No.</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblSupplierTelephone" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>ST Reg. No.</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblSellerRegNo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>NTN No.</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblSellerNTN" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Stock
                        Adjustment<br />
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 100%">
            <tr>
                <td colspan="2" style=" text-align:left">
                    <table style="float: left; width: 400px; border-collapse: collapse" border="1">
                        <tr>
                            <td style="text-align: left; height: 30px;">
                                <b>Voucher No  </b>
                            </td>
                            <td style="padding-left: 10px">
                                <asp:Label ID="lblVoucherNo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; height: 30px;">
                                <b>Date</b>
                            </td>
                            <td style="padding-left: 10px">
                                <asp:Label ID="lblDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; height: 30px;">
                                <b>Cost Center </b>
                            </td>
                            <td style="padding-left: 10px">
                                <asp:Label ID="LblCostCenter" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
            AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" ShowFooter="true"
            HeaderStyle-CssClass="gridheader" AlternatingItemStyle-CssClass="gridAlternateItem"
            GridLines="none" BorderColor="black" BorderStyle="Dotted" FooterStyle-CssClass="gridFooter"
            BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery_RowDataBound">
            <Columns>
                <asp:TemplateColumn HeaderText="S.No.">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                    <ItemTemplate>
                        <%# Container.DataSetIndex + 1%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Item Description">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="50%" />
                    <ItemTemplate>
                        <%# Eval("ItemDesc") %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Quantity">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                    <FooterStyle Font-Size="14px" />
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ').Replace('(', '-').Replace(')', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalQuantity" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
        <br />
        Description :
        <asp:Label ID="lblDesc" runat="server"></asp:Label>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td style="margin-top: 20px; width: 500px;">
                    <div style="float: left">
                        <b>Adjusted By :</b></div>
                    <div style="float: left; margin-left: 20px; height: 20px; padding-top: 10px; width: 200px;">
                        <hr />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%">
                        <tr>
                            <td style="margin-top: 20px; width: 50%;">
                                <div style="float: left">
                                    <b>Dated :</b></div>
                                <div style="float: left; margin-left: 50px; padding-top: 10px; height: 20px; width: 200px;">
                                    <hr />
                                </div>
                            </td>
                            <td style="margin-top: 20px; text-align: right">
                                <div style="float: right; margin-left: 20px; padding-top: 10px; height: 20px; line-height: 3px;
                                    text-align: center; width: 200px;">
                                    <hr />
                                    <b>Signature</b>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                </td>
            </tr>
             <tr>
                <td style="text-align: right">
                    <asp:LinkButton Text="Print" ID="lnkPrint" OnClientClick="window.print();" runat="server"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>
                                <script type="text/javascript">

                                    var currentTime = new Date()
                                    var hours = currentTime.getHours()
                                    var Newhours = 0;
                                    var minutes = currentTime.getMinutes()
                                    if (minutes < 10) {
                                        minutes = "0" + minutes
                                    }
                                    if (hours > 12) {
                                        Newhours = hours - 12;
                                    }
                                    else {
                                        Newhours = hours;
                                    }
                                    document.write(Newhours + ":" + minutes + " ")
                                    if (hours > 11) {
                                        document.write("PM")
                                    } else {
                                        document.write("AM")
                                    }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
