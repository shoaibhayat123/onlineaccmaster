﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TourBLL;

public partial class ViewHotel : System.Web.UI.Page
{
    StoreProc _ObjTA = new StoreProc();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string id = HttpContext.Current.Request["hotelId"] != null ? HttpContext.Current.Request["hotelId"].ToString() : "0";

            if (Convert.ToInt32(id) > 0)
            {
                BindGrid(Convert.ToInt32(id));
            }
        }
    }

    private void BindGrid(int id)
    {
        DataTable dt = new DataTable();
        dt = _ObjTA.getHotel(id);
        rptrHotelDetailsMain.DataSource = dt;
        rptrHotelDetailsMain.DataBind();

    }

    protected void rptrHotelDetailsMain_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        HiddenField hndid = (HiddenField)e.Item.FindControl("HdnHotelId");
        Repeater rptrHotelDetailsImages = (Repeater)e.Item.FindControl("rptrHotelDetailsImages");

        DataGrid dataGrid = (DataGrid)e.Item.FindControl("dgGallery");

        DataTable dt = new DataTable();
        dt = _ObjTA.getHotelDetails(Convert.ToInt32(hndid.Value));
        rptrHotelDetailsImages.DataSource = dt;
        rptrHotelDetailsImages.DataBind();

        DataTable dtDataGrid = _ObjTA.Select_RoomDetailsByHotelID(Convert.ToInt32(hndid.Value));

        if (dtDataGrid.Rows.Count > 0)
        {
            dataGrid.DataSource = dtDataGrid;
            dataGrid.DataBind();
            dataGrid.Visible = true;
        }
        else
        {
            dataGrid.Visible = false;
        }
    }

    #region dgGallery_ItemDataBound()
    protected void dgGallery_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DropDownList ddlRooms = ((DropDownList)e.Item.FindControl("ddlRooms"));

            HiddenField hdnRooms = (HiddenField)e.Item.FindControl("HdnRooms");
            HiddenField HdnPrice = (HiddenField)e.Item.FindControl("HdnPrice");

            int rooms = Convert.ToInt32(hdnRooms.Value);

            DataTable tb = null;
            tb = new DataTable("tblRooms");

            //CREATE COLUMNS
            DataColumn col = new DataColumn();

            //USED TO STORE PRODUCT ID 
            col = new DataColumn();
            col.DataType = System.Type.GetType("System.String");
            col.ColumnName = "Rooms";
            col.ReadOnly = false;
            col.Unique = false;
            tb.Columns.Add(col);

            //PRODUCT PRICE
            col = new DataColumn();
            col.DataType = System.Type.GetType("System.Single");
            col.ColumnName = "Price";
            col.ReadOnly = false;
            col.Unique = false;
            tb.Columns.Add(col);

            //PRIMARY KEY COL; USED TO STORE ITEM ID (UNIQUE NATURE)
            col = new DataColumn();
            col.DataType = System.Type.GetType("System.Int32");
            col.ColumnName = "ID";
            col.ReadOnly = false;
            col.Unique = true;
            col.AutoIncrement = true;
            col.AutoIncrementSeed = 1;
            col.AutoIncrementStep = 1;
            tb.Columns.Add(col);

            //'MAKE ONE OF THE COL PK
            DataColumn[] PrimaryKeyColumns = new DataColumn[1];
            PrimaryKeyColumns[0] = tb.Columns["ID"];
            tb.PrimaryKey = PrimaryKeyColumns;

            for (int i = 1; i <= rooms; i++)
            {
                double amount;
                amount = i * Convert.ToDouble(HdnPrice.Value);

                DataRow dr;
                //ADD A NEW ROW
                dr = tb.NewRow();
                dr["Rooms"] = Convert.ToString(i) + " (USD " + Convert.ToString(amount) + ")";
                dr["Price"] = Convert.ToString(amount);

                tb.Rows.Add(dr);
                tb.AcceptChanges();
            }

            ddlRooms.DataValueField = "Price";
            ddlRooms.DataTextField = "Rooms";
            ddlRooms.DataSource = tb;
            ddlRooms.DataBind();
        }
    }
    #endregion

    #region dgGallery_ItemCommand()
    protected void dgGallery_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "booknow")
        {
            Session["BookHotel"] = 1;
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DropDownList ddlRooms = ((DropDownList)e.Item.FindControl("ddlRooms"));
                Label lblHotelName = (Label)e.Item.FindControl("lblHotelName");
                Label lblRoomType = (Label)e.Item.FindControl("lblRoomType");
                HiddenField HdnPrice = (HiddenField)e.Item.FindControl("HdnPrice");
                HiddenField HdnHotelID = (HiddenField)e.Item.FindControl("HdnHotelID");
                TextBox txtBookingDate = (TextBox)(e.Item.FindControl("txtBookingDate"));
                TextBox txtCheckOutdate = (TextBox)(e.Item.FindControl("txtCheckOutdate"));
                string KID = e.CommandArgument.ToString();
                int Rooms = Convert.ToInt32(ddlRooms.SelectedIndex);

                string strlblHotelName = lblHotelName.Text;
                string strlblRoomType = lblRoomType.Text;


                if (txtBookingDate.Text != "" & txtCheckOutdate.Text != "")
                {
                    DateTime startTime = Convert.ToDateTime(txtBookingDate.Text);
                    DateTime endTime = Convert.ToDateTime(txtCheckOutdate.Text);
                    TimeSpan span = endTime.Subtract(startTime);
                    if (span.Days >= 0)
                    {
                        DateTime startTime2 = Convert.ToDateTime(txtBookingDate.Text);
                        DateTime endTime2 = System.DateTime.Now;
                        TimeSpan span2 = endTime2.Subtract(startTime2);
                        if (span2.Days >= 0)
                        {

                            DataTable DtHotel = _ObjTA.GetHotelRooms(Convert.ToDateTime(txtBookingDate.Text), Convert.ToDateTime(txtCheckOutdate.Text), Convert.ToInt32(KID));
                            int NoOfRooms = 0;
                            int TotalNoOfRooms = 0;
                            if (Convert.ToString(DtHotel.Rows[0]["NoOfRooms"]) != "")
                            {
                                NoOfRooms = Convert.ToInt32(DtHotel.Rows[0]["NoOfRooms"]);
                            }
                            if (Convert.ToString(DtHotel.Rows[0]["TotalNoOfRooms"]) != "")
                            {
                                TotalNoOfRooms = Convert.ToInt32(DtHotel.Rows[0]["TotalNoOfRooms"]);
                            }


                            int AvailableRooms = NoOfRooms - TotalNoOfRooms;
                            if (Convert.ToInt32(Rooms) < AvailableRooms)
                            {

                                double amount = Convert.ToDouble(ddlRooms.SelectedValue);
                                if (span.Days > 0)
                                {
                                    amount = amount * (span.Days);
                                }
                                BindHotels(KID, amount, Rooms, strlblHotelName, strlblRoomType, Convert.ToString(HdnPrice.Value), Convert.ToString(HdnHotelID.Value), Convert.ToDateTime(txtBookingDate.Text), Convert.ToDateTime(txtCheckOutdate.Text));
                            }
                            else
                            {
                                Page.RegisterStartupScript("Msg1", "<script>alert('Rooms are not Available!!!');</script>");
                            }

                        }
                        else
                        {
                            Page.RegisterStartupScript("Msg1", "<script>alert('Enter Valid Check In Date &  Check Out Date!!!');</script>");
                        }

                    }
                    else
                    {
                        Page.RegisterStartupScript("Msg1", "<script>alert('Enter Valid Check In Date &  Check Out Date!!!');</script>");
                    }
                }
                else
                {
                    Page.RegisterStartupScript("Msg1", "<script>alert('Enter Check In Date &  Check Out Date!!!');</script>");
                }

            }
        }
    }
    #endregion

    #region BindHotels()
    private void BindHotels(string KID, double amount, int Rooms, string strlblHotelName, string strlblRoomType, string Price, string HotelID, DateTime BookingDate, DateTime CheckOutdate)
    {
        ClsSession.RoomID = KID;
        ClsSession.Rooms = Rooms + 1;
        ClsSession.RoomType = strlblRoomType;
        ClsSession.HotelName = strlblHotelName;
        ClsSession.RoomPrice = Price;
        ClsSession.HotelAmount = amount;
        ClsSession.HotelID = HotelID;
        ClsSession.BookingDate = BookingDate;
        ClsSession.CheckOutdate = CheckOutdate;

        Page.RegisterStartupScript("Msg1", "<script>alert('Hotel Booked successfully !!!');location.replace('BookHotels.aspx');</script>");
    }
    #endregion
}
