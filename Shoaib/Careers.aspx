﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaticPages.master" AutoEventWireup="true"
    CodeFile="Careers.aspx.cs" Inherits="Careers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content_wraper5">
        <div id="content_midd5">
            <div class="acc-features-middle-bg">
                <div class="top-heading">
                    <h1>
                        Careers</h1>
                </div>
            </div>
            <div class="accmiddle-features-bg-2" style="min-height: 520px">
                <div class="features-wrapper">
                    <div class="features-box">
                         <div style=" float:left">
                        <div class="features-left-link-box">
                            <div class="left-link-Content">
                                <h2 style=" text-decoration:underline; font-size:18px;">
                                  AccMaster</h2><br />
                                <ul style=" margin-left:20px">
                                    <li><a href="index.aspx">Home</a></li>
                                    <li><a href="features.aspx">Features</a></li>
                                    <li><a href="pricing.aspx">Product & Pricing</a></li>
                                    <li><a href="PayOnline.aspx">Pay Online</a></li>
                                    <li><a href="Privacy.aspx">Privacy</a></li>
                                    <li><a href="Terms.aspx">Terms</a></li>
                                    <li><a href="ContactUs.aspx">Contact us</a></li>
                                    <li><a href="AboutUs.aspx">About Us</a></li>
                                    <li><a href="Careers.aspx">Careers</a></li>
                                </ul>
                            </div>
                            <div class="left-link-bottom">
                                <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                            </div>
                        </div>
                         <div class="features-left-link-box" style=" clear:both; margin-top:20px;">
                              <div class="left-link-Content" style="min-height: 100px;line-height:0.6">
                                    <asp:Literal ID="ltrlAddress" runat="server"></asp:Literal>
                                </div>
                            <div class="left-link-bottom">
                                <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                            </div>
                        </div>
                        </div>
                        <div class="feat-right-part-box">
                            <div class="feta-right-part-content">
                                <div class="feat-right-box-dtls">
                                    <div class="ft-right-box-details1"  >
                                <%--        <h1>
                                            AccMaster Privacy Highlights</h1>--%>
                                        
                                            If you are interested in pursuing a career at System development Services, <br />Send
                                            in your resume to : <a href="mailto:career@accmaster.com"> career@accmaster.com</a><br />
                                            <br />
                                            You can apply for :
                                            <ul>
                                                <li>DOT Net Developer</li>
                                                <li> Graphic Designer</li>
                                                <li>Web Designer</li>
                                                <li>Sales and Marketing</li>
                                                <li> Customer Support</li>
                                            </ul>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="feat-right-part-bottom">
                                <img src="images/feat-right-part-bottom-bg.jpg" alt="" />
                            </div>
                        </div>
</asp:Content>
