﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Globalization;

public partial class ChartofAccount : System.Web.UI.Page
{
    #region Properties
    clsCookie ck = new clsCookie();
    AccFiles objFile = new AccFiles();
    TextInfo textInfo1 = CultureInfo.InvariantCulture.TextInfo;
    DataTable dttbl = new DataTable();

    #endregion
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        if (!IsPostBack)
        {

            if (Sessions.CustomerCode != "")
            {
                FillTree();
                lblLevel.Text = "0/6";
            }
            else
            {
                Response.Redirect("default.aspx");
            }

        }
        SetUserRight();

    }
    #endregion
    #region FillTree
    protected void FillTree()
    {
        DataTable dt = new DataTable();
        objFile.id = 0;
        objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        dt = objFile.getTreeData();
        if (dt.Rows.Count > 0)
        {
            /* Seeting Group Ladger Quantity */
            DataView dv = dt.DefaultView;
            dv.RowFilter = "IsLedger = 'Group'";
            DataTable dtGroup = new DataTable();
            dtGroup = dv.ToTable();
            lblGroup.Text = Convert.ToString(dtGroup.Rows.Count);

            DataView dv1 = dt.DefaultView;
            dv1.RowFilter = "IsLedger = 'Ledger'";
            DataTable dtLedger = new DataTable();
            dtLedger = dv1.ToTable();
            lblLedger.Text = Convert.ToString(dtLedger.Rows.Count);



            /* Seeting debit credit balance */
            DataTable dtDrCr = new DataTable();
            objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            dtDrCr = objFile.getDebitCredit();
            if (dtDrCr.Rows.Count > 0)
            {
                if (Convert.ToString(dtDrCr.Rows[0]["debit"]) != "")
                {
                    decimal Debit = Convert.ToDecimal(dtDrCr.Rows[0]["debit"]);
                    lblDebits.Text = String.Format("{0:C}", Debit).Replace('$', ' ');
                }
                else
                {
                    lblDebits.Text = "0.00";
                }
                if (Convert.ToString(dtDrCr.Rows[0]["credit"]) != "")
                {
                    decimal Credit = Convert.ToDecimal(dtDrCr.Rows[0]["credit"]);
                    lblCredits.Text = String.Format("{0:C}", Credit).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                }
                else
                {
                    lblCredits.Text = "0.00";
                }

                decimal Difference = Convert.ToDecimal(lblDebits.Text) - Convert.ToDecimal(lblCredits.Text);
                lbldifferences.Text = String.Format("{0:C}", Difference).Replace('$', ' ').Replace('(', '-').Replace(')', ' ');


            }




            /* Fill Tree Logic Start here*/
            DataSet ds = new DataSet();
            ds.Reset();
            ds.DataSetName = "Menus";
            ds.Tables.Add(dt.Copy());

            ds.Tables[0].TableName = "Menu";
            DataRelation drel = new DataRelation("menu", ds.Tables["Menu"].Columns["Categoryid"], ds.Tables["Menu"].Columns["pid"], true);
            drel.Nested = true;
            ds.Relations.Add(drel);
            DataTable dtNew = new DataTable();
            dtNew = ds.Tables[0];
            XmlDataSource1.Data = ds.GetXml();
            /* Fill Tree Logic End here*/




        }
    }
    #endregion
    #region TreeNodChanged
    protected void tvcat_SelectedNodeChanged(object sender, EventArgs e)
    {

        btn_menu_addNew.Visible = true;

        Int32 id = Convert.ToInt32(tvcat.SelectedNode.Value);
        string[] Title = tvcat.SelectedNode.Text.Split('(');
        ViewState["pid"] = id;
        ViewState["id"] = id;
        ViewState["Title"] = Title[0].Trim();
        pnl_menu.Visible = true;
        pnl_MenuOption.Visible = false;
        VisibleButton();
        /*Set Tree Level*/




    }
    #endregion
    #region Tree View Events
    protected void tvcat_DataBound(object sender, EventArgs e)
    {

    }
    protected void tvcat_DataBinding(object sender, EventArgs e)
    {

    }
    protected void tvcat_TreeNodeDataBound(object sender, TreeNodeEventArgs e)
    {

        //objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objFile.id = Convert.ToInt32(ViewState["id"]);
        DataTable dtPosition = new DataTable();
        dtPosition = objFile.PositionSundryDebitors();
        if (dtPosition.Rows.Count > 0)
        {
            for (int i = 0; i < dtPosition.Rows.Count; i++)
            {
                if (e.Node.Value == dtPosition.Rows[i]["id"].ToString())
                {
                    e.Node.ExpandAll();
                }
            }
        }

    }
    #endregion
    #region AddLedger
    protected void btn_menu_Ladger_Click(object sender, ImageClickEventArgs e)
    {
        pnl_MenuOption.Visible = true;
        Session["s"] = null; // Remove session of bills grid
        hid_Ladgerid.Value = "";
        lblLadgerTitle.Text = "Add New Ledger ";
        lblParrent.Text = Convert.ToString(ViewState["Title"]);
        multiview_1.ActiveViewIndex = 1;
        int debit = ViewSundryDebitorForm();
        hideButton();
        ClearLedger(); // Blank aal fields
        DataTable dt = new DataTable();
        dgGallery.DataSource = dt;
        dgGallery.DataBind();
        ShowamountDifference();
        if (debit == 1)
        {
            dgGallery.Visible = false; // Visible false bills grid
        }

    }
    protected void btn_saveLedger_Click(object sender, ImageClickEventArgs e)
    {

        int Resultamount = CheckOpeningBalanceDetails();
        if (Resultamount == 1)
        {
            Int32 pid = Convert.ToInt32(ViewState["pid"]);
            DataTable dt = new DataTable();
            if (hid_Ladgerid.Value != "")
            {
                objFile.id = Convert.ToInt32(hid_Ladgerid.Value);
            }
            else
            {
                objFile.id = 0;
            }
            objFile.Title = textInfo1.ToTitleCase(txtTitle.Text);
            objFile.PhoneNo = textInfo1.ToTitleCase(txtPhone.Text);
            objFile.Address1 = textInfo1.ToTitleCase(txtAddress1.Text);
            objFile.Address2 = textInfo1.ToTitleCase(txtAddress2.Text);
            objFile.Address3 = textInfo1.ToTitleCase(txtAddress3.Text);
            Nullable<int> nullCreditperiod;
            nullCreditperiod = null;
            if (txtCreditperiod.Text != "")
            {
                nullCreditperiod = Convert.ToInt32(txtCreditperiod.Text);
            }
            objFile.Creditperiod = nullCreditperiod;
            objFile.PanNo = textInfo1.ToTitleCase(txtPan.Text);
            objFile.SalesTaxNo = textInfo1.ToTitleCase(txtSalesTax.Text);
            if (rdbDR.Checked == true)
            {
                objFile.OpeningBalType = rdbDR.Text;
                if (Convert.ToString(txtOpeningBalance.Text) != "")
                {
                    objFile.OpeningBal = Convert.ToDecimal(txtOpeningBalance.Text);
                }
                else
                {
                    objFile.OpeningBal = 0;
                }
            }
            else
            {
                objFile.OpeningBalType = rdbCR.Text;
                if (Convert.ToString(txtOpeningBalance.Text) != "")
                {
                    objFile.OpeningBal = (0 - Convert.ToDecimal(txtOpeningBalance.Text));
                }
                else
                {
                    objFile.OpeningBal = -0;
                }
            }


            objFile.ParrentId = pid;
            objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objFile.EmailId = txtEmail.Text;
            dt = objFile.AddEditLedger();

            if (Convert.ToInt32(dt.Rows[0]["id"]) > 0)
            {
                if (hid_Ladgerid.Value == "")           /* Add Bills Detail from Session Table */
                {
                    if (Session["s"] != null)
                    {
                        dttbl = (DataTable)Session["s"];
                        for (int i = 0; i < dttbl.Rows.Count; i++)
                        {
                            objFile.InvoiceDate = Convert.ToDateTime(dttbl.Rows[i]["InvoiceDate"]);
                            objFile.InvoiceNo = Convert.ToString(dttbl.Rows[i]["InvoiceNo"]);
                            objFile.CreditDays = Convert.ToInt32(dttbl.Rows[i]["CreditDays"]);
                            objFile.Amount = Convert.ToDecimal(dttbl.Rows[i]["Amount"]);
                            objFile.AcFileId = Convert.ToInt32(dt.Rows[0]["id"]);
                            objFile.DataFrom = "chart";
                            objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
                            if (rdbDR.Checked == true)
                            {
                                objFile.DebitCredit = "Debit";
                            }
                            else
                            {
                                objFile.DebitCredit = "Credit";
                            }
                            objFile.AddeditInvoice();
                        }
                    }
                }
                else                                   /* Edit Bills Detail from Session Table */
                {
                    objFile.id = Convert.ToInt32(dt.Rows[0]["id"]);
                    objFile.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                    int result = objFile.deleteInvoice();

                    if (Session["s"] != null)
                    {
                        dttbl = (DataTable)Session["s"];
                        for (int i = 0; i < dttbl.Rows.Count; i++)
                        {
                            objFile.id = 0;

                            objFile.InvoiceDate = Convert.ToDateTime(dttbl.Rows[i]["InvoiceDate"]);
                            objFile.InvoiceNo = Convert.ToString(dttbl.Rows[i]["InvoiceNo"]);
                            objFile.CreditDays = Convert.ToInt32(dttbl.Rows[i]["CreditDays"]);
                            objFile.Amount = Convert.ToDecimal(dttbl.Rows[i]["Amount"]);
                            objFile.AcFileId = Convert.ToInt32(dt.Rows[0]["id"]);
                            objFile.DataFrom = "chart";
                            objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
                            if (rdbDR.Checked == true)
                            {
                                objFile.DebitCredit = "Debit";
                            }
                            else
                            {
                                objFile.DebitCredit = "Credit";
                            }
                            objFile.InvoiceID = Convert.ToInt32(dttbl.Rows[i]["InvoiceID"]);
                            objFile.AddeditInvoice();

                        }

                    }
                }



                if (hid_Ladgerid.Value != "")
                {
                    hid_Ladgerid.Value = "";
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Updated.');", true);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Saved.');", true);

                }
                FillTree();
                // tvcat.SelectedNode.Value = Convert.ToString(ViewState["pid"]);
                pnl_menu.Visible = false;
                pnl_MenuOption.Visible = false;
                ClearLedger();

            }
            else
            {
                if (Convert.ToInt32(dt.Rows[0]["id"]) == 0)
                {

                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('You can not add more then 6 Lavels');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Already Exists');", true);
                }
            }
        }

        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Opening Balance amount and  Total of Invoice Amount are not equal check it again ');", true);
        }

    }
    #endregion
    #region ADDGroup
    protected void btn_menu_addNew_Click(object sender, ImageClickEventArgs e)
    {
        txt_subcate.Text = "";
        pnl_MenuOption.Visible = true;
        hid_submenuid.Value = "";
        Label1.Text = "Add New Group ";
        lblParrentGroup.Text = Convert.ToString(ViewState["Title"]);
        multiview_1.ActiveViewIndex = 0;
        hideButton();
    }
    protected void btn_save_Click(object sender, ImageClickEventArgs e)
    {
        Int32 pid = Convert.ToInt32(ViewState["pid"]);

        DataTable dt = new DataTable();
        if (hid_submenuid.Value != "")
        {
            objFile.id = Convert.ToInt32(hid_submenuid.Value);
        }
        else
        {
            objFile.id = 0;
        }

        objFile.Title = textInfo1.ToTitleCase(txt_subcate.Text);
        objFile.ParrentId = pid;
        objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        dt = objFile.AddEditSubMenu();
        if (Convert.ToInt32(dt.Rows[0]["id"]) > 0)
        {
            if (hid_submenuid.Value != "")
            {
                hid_submenuid.Value = "";
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Updatedy.');", true);

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Saved.');", true);

            }
            FillTree();

            // ((Literal)Master.FindControl("ltrTreeExpand")).Text = "<script type=\"text/javascript\">TreeView_ToggleNode(MainHome_tvcat_Data,44,document.getElementById('MainHome_tvcatn44'),'t',document.getElementById('MainHome_tvcatn44Nodes'));</script>";


            txt_subcate.Text = "";
            pnl_menu.Visible = false;
            pnl_MenuOption.Visible = false;

        }
        else
        {
            if (Convert.ToInt32(dt.Rows[0]["id"]) == 0)
            {

                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('You can not add more then 6 Lavels');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Already Exists');", true);
            }
        }

    }
    #endregion
    #region Delete
    protected void imgbtn_deletecontent_Click(object sender, ImageClickEventArgs e)
    {

        objFile.id = Convert.ToInt32(ViewState["id"]); ;
        objFile.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtResult = new DataTable();
        dtResult = objFile.deleteGroup();
        hideButton();
        FillTree();
        if (Convert.ToInt32(dtResult.Rows[0]["result"]) == 1)
        {
            ViewState["id"] = dtResult.Rows[0]["Parrent"].ToString();
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record has been deleted.');", true);

        }
        if (Convert.ToInt32(dtResult.Rows[0]["result"]) == 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('You cannot delete this account. Transaction(s) found against  this account.');", true);

        }

    }
    #endregion
    #region cancel
    protected void btn_cancel_Click(object sender, ImageClickEventArgs e)
    {
        pnl_MenuOption.Visible = false;
        ViewState["pid"] = ViewState["id"];
        VisibleButton();
    }
    #endregion
    #region Edit group and Ledger
    protected void btn_menu_EditSubCategory_Click(object sender, ImageClickEventArgs e)
    {
        Session["s"] = null; // remove session of bills grid
        int debit = ViewSundryDebitorForm();

        DataTable dt = new DataTable();
        Int32 pid = Convert.ToInt32(ViewState["pid"]);
        objFile.id = pid;
        objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        dt = objFile.getMenueDetail();

        if (Convert.ToString(dt.Rows[0]["IsLedger"]) == "Group")
        {
            pnl_MenuOption.Visible = true;
            multiview_1.ActiveViewIndex = 0;
            txt_subcate.Text = "";
            hid_submenuid.Value = "";
            Label1.Text = "Edit Group";

            hid_submenuid.Value = dt.Rows[0]["Id"].ToString();
            ViewState["pid"] = Convert.ToString(dt.Rows[0]["ParrentId"]);
            txt_subcate.Text = dt.Rows[0]["Title"].ToString();
        }
        else
        {
            pnl_MenuOption.Visible = true;
            multiview_1.ActiveViewIndex = 1;
            ClearLedger();
            hid_Ladgerid.Value = "";
            lblLadgerTitle.Text = "Edit Ledger";

            ViewState["pid"] = Convert.ToString(dt.Rows[0]["ParrentId"]);
            hid_Ladgerid.Value = dt.Rows[0]["Id"].ToString();

            txtTitle.Text = dt.Rows[0]["Title"].ToString();
            txtPhone.Text = dt.Rows[0]["PhoneNo"].ToString();
            txtAddress1.Text = dt.Rows[0]["Address1"].ToString();
            txtAddress2.Text = dt.Rows[0]["Address2"].ToString();
            txtAddress3.Text = dt.Rows[0]["Address3"].ToString();
            txtCreditperiod.Text = dt.Rows[0]["Creditperiod"].ToString();
            txtPan.Text = dt.Rows[0]["PanNo"].ToString();
            txtEmail.Text = Convert.ToString(dt.Rows[0]["EmailId"]);
            txtSalesTax.Text = dt.Rows[0]["SalesTaxNo"].ToString();

            if (rdbDR.Text == Convert.ToString(dt.Rows[0]["OpeningBalType"]))
            {
                rdbDR.Checked = true;
                txtOpeningBalance.Text = Convert.ToString(dt.Rows[0]["OpeningBal"].ToString());
            }
            else
            {
                rdbCR.Checked = true;
                if (Convert.ToString(dt.Rows[0]["OpeningBal"].ToString()) != "")
                {
                    if (Convert.ToDecimal(dt.Rows[0]["OpeningBal"]) != 0)
                    {
                        txtOpeningBalance.Text = Convert.ToString(dt.Rows[0]["OpeningBal"].ToString()).Remove(0, 1);
                    }
                    else
                    {
                        txtOpeningBalance.Text = Convert.ToString(dt.Rows[0]["OpeningBal"].ToString());
                    }
                }
            }

            if (debit == 1)
            {
                FillInvoice(pid);
            }
        }
        hideButton();



    }
    #endregion
    #region button Visiblity
    protected void hideButton()
    {
        btn_menu_addNew.Visible = false;
        imgbtn_deletecontent.Visible = false;
        btn_menu_EditSubCategory.Visible = false;
        btn_menu_Ladger.Visible = false;
        SetUserRight();
    }
    protected void VisibleButton()
    {
        Int32 pid = Convert.ToInt32(ViewState["id"]);
        objFile.id = pid;
        objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dt = new DataTable();
        dt = objFile.CheckCustomer();
        if (Convert.ToString(dt.Rows[0]["CustomerCode"]) == "0") //Default Groups not edit or delete
        {
            imgbtn_deletecontent.Visible = false;
            btn_menu_EditSubCategory.Visible = false;
            if (pid == 1)
            {
                btn_menu_Ladger.Visible = false;
                btn_menu_addNew.Visible = false;
            }
            else
            {
                btn_menu_Ladger.Visible = true;
                btn_menu_addNew.Visible = true;
            }

        }
        else
        {

            if (Convert.ToString(dt.Rows[0]["IsLedger"]) == "Group") //if this node is in group category
            {
                if (Convert.ToString(dt.Rows[0]["Child"]) == "0")  //if this node is in group category and leaf node (no child) then delete possible
                {
                    imgbtn_deletecontent.Visible = true;
                    btn_menu_EditSubCategory.Visible = true;
                    btn_menu_addNew.Visible = true;
                    btn_menu_Ladger.Visible = true;
                }
                else
                {
                    imgbtn_deletecontent.Visible = false;
                    btn_menu_EditSubCategory.Visible = true;
                    btn_menu_addNew.Visible = true;
                    btn_menu_Ladger.Visible = true;
                }

            }
            else
            {
                if (Convert.ToString(dt.Rows[0]["Child"]) == "0")//if this node is in Ledger category and leaf node (no child) then delete possible
                {
                    imgbtn_deletecontent.Visible = true;
                    btn_menu_EditSubCategory.Visible = true;
                    btn_menu_addNew.Visible = false;
                    btn_menu_Ladger.Visible = false;
                }
                else
                {
                    imgbtn_deletecontent.Visible = false;
                    btn_menu_EditSubCategory.Visible = true;
                    btn_menu_addNew.Visible = false;
                    btn_menu_Ladger.Visible = false;
                }
            }
        }


        AddNodeLevel();
        SetUserRight();
    }
    #endregion
    #region clear Ledger
    protected void ClearLedger()
    {
        txtTitle.Text = "";
        txtPhone.Text = "";
        txtAddress1.Text = "";
        txtAddress2.Text = "";
        txtAddress3.Text = "";
        txtOpeningBalance.Text = "";
        rdbDR.Checked = true;
        txtCreditperiod.Text = "";
        txtPan.Text = "";
        txtSalesTax.Text = "";
        txtBillDate.Text = "";
        Session["s"] = null;
        dgGallery.Visible = false;
        tdTotal.Style.Add("display", "none");
        tdDiscount.Style.Add("display", "none");
        txtEmail.Text = "";
    }
    #endregion
    #region Expan Colleps Tree
    protected void btnExpand_Click(object sender, EventArgs e)
    {
        tvcat.ExpandAll();
    }
    protected void btnUnExpand_Click(object sender, EventArgs e)
    {
        FillTree();
        tvcat.ExpandDepth = 1;
    }
    #endregion
    #region Add New Bill details
    protected void btnAddNew_Click(object sender, EventArgs e)
    {

        filltable();
        DataRow row;
        if (dttbl.Rows.Count > 0)
        {
            int maxid = 0;
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() != "")
                {
                    if (maxid < Convert.ToInt32(dttbl.Rows[i]["Id"].ToString()))
                    {
                        maxid = Convert.ToInt32(dttbl.Rows[i]["Id"].ToString());
                    }
                }
            }
            row = dttbl.NewRow();
            row["Id"] = maxid + 1;
            row["InvoiceDate"] = Convert.ToDateTime(txtBillDate.Text);
            row["InvoiceNo"] = txtInvoiceNo.Text;
            if (txtCreditDays.Text != "")
            {
                row["CreditDays"] = Convert.ToInt32(txtCreditDays.Text);
            }
            else
            {
                row["CreditDays"] = 0;
            }
            if (txtInvoiceAmount.Text != "")
            {
                row["Amount"] = Convert.ToDecimal(txtInvoiceAmount.Text);
            }
            else
            {
                row["Amount"] = 0.00;
            }

            row["InvoiceID"] = 0;

        }
        else
        {
            row = dttbl.NewRow();
            row["Id"] = 1;
            row["InvoiceDate"] = Convert.ToDateTime(txtBillDate.Text);
            row["InvoiceNo"] = txtInvoiceNo.Text;
            row["CreditDays"] = Convert.ToInt32(txtCreditDays.Text);
            row["Amount"] = Convert.ToDecimal(txtInvoiceAmount.Text);
            row["InvoiceID"] = 0;
        }
        dttbl.Rows.Add(row);
        dttbl.AcceptChanges();
        Session.Add("s", dttbl);
        dgGallery.DataSource = dttbl;
        dgGallery.DataBind();
        ShowamountDifference();
        if (dttbl.Rows.Count > 0)
        {
            dgGallery.Visible = true;
        }

        ClearInvoice();
    }
    #endregion
    #region ClearInvoice
    protected void ClearInvoice()
    {
        txtBillDate.Text = "";
        txtInvoiceNo.Text = "";
        txtCreditDays.Text = "";
        txtInvoiceAmount.Text = "";

    }
    #endregion
    #region Bills Grid Event
    protected void dgGallery_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        dttbl = (DataTable)Session["s"];
        if (e.CommandName == "Delete")
        {
            int j = 0;
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    dttbl.Rows.RemoveAt(i);
                }
            }
            dttbl.AcceptChanges();
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            Session.Add("s", dttbl);
            ShowamountDifference();

        }
    }
    #endregion
    #region Fill session Table
    public void filltable()
    {
        if (Session["s"] == null)
        {
            DataColumn column = new DataColumn();
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.DateTime");
            column.ColumnName = "InvoiceDate";
            column.Caption = "InvoiceDate";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "InvoiceNo";
            column.Caption = "InvoiceNo";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "CreditDays";
            column.Caption = "CreditDays";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Amount";
            column.Caption = "Amount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "InvoiceID";
            column.Caption = "InvoiceID";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


        }
        else
        {
            dttbl = (DataTable)Session["s"];
        }
    }
    #endregion
    #region ViewSundryDebitorForm
    protected int ViewSundryDebitorForm()
    {
        objFile.id = Convert.ToInt32(ViewState["id"]);
        DataTable dt = new DataTable();
        dt = objFile.PositionSundryDebitors();

        DataView dv = dt.DefaultView;
        dv.RowFilter = "Title = 'Sundry Debtors' ";
        DataTable dtresult = new DataTable();
        dtresult = dv.ToTable();

        if (dtresult.Rows.Count == 0)
        {
            dv.RowFilter = "Title = 'Sundry Creditors' ";
            dtresult = dv.ToTable();
        }



        if (dtresult.Rows.Count > 0)
        {
            sundryDR.Style.Add("display", "");
            tblSundyCredit.Style.Add("display", "");
           // RequiredFieldValidator6.Enabled = true;
            return 1;
        }
        else
        {
            sundryDR.Style.Add("display", "none");
            tblSundyCredit.Style.Add("display", "none");
           // RequiredFieldValidator6.Enabled = false;
            return 0;
        }


    }
    #endregion
    #region fillInvoice
    protected void FillInvoice(int pid)
    {
        DataTable dtInvoice = new DataTable();
        objFile.id = pid;
        objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        dtInvoice = objFile.getInvoice();

        if (dtInvoice.Rows.Count > 0)
        {
            for (int i = 0; i < dtInvoice.Rows.Count; i++)
            {
                filltable();
                DataRow row;
                row = dttbl.NewRow();
                row["Id"] = i + 1;

                row["InvoiceDate"] = Convert.ToDateTime(dtInvoice.Rows[i]["InvoiceDate"].ToString());
                row["InvoiceNo"] = dtInvoice.Rows[i]["InvoiceNo"].ToString();
                row["CreditDays"] = Convert.ToInt32(dtInvoice.Rows[i]["CreditDays"].ToString());
                row["Amount"] = Convert.ToDecimal(dtInvoice.Rows[i]["Amount"].ToString());
                row["InvoiceID"] = Convert.ToInt32(dtInvoice.Rows[i]["InvoiceID"].ToString());

                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
                Session.Add("s", dttbl);
            }
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            dgGallery.Visible = true;
            ShowamountDifference();
        }
        else
        {
            dgGallery.Visible = false;
        }

    }
    #endregion
    #region check Amount detail
    protected int CheckOpeningBalanceDetails()
    {
        if (Session["s"] != null)
        {
            dttbl = (DataTable)Session["s"];
            if (dttbl.Rows.Count > 0)
            {
                decimal TotalAmount = 0;
                for (int i = 0; i < dttbl.Rows.Count; i++)
                {
                    TotalAmount += Convert.ToDecimal(dttbl.Rows[i]["Amount"]);
                }
                if (txtOpeningBalance.Text != "")
                {
                    if (TotalAmount == Convert.ToDecimal(txtOpeningBalance.Text))
                    {
                        return 1;
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                return 1;
            }
        }
        else
        {
            return 1;
        }

    }
    #endregion
    #region show amount

    protected void ShowamountDifference()
    {
        if (Session["s"] != null)
        {

            dttbl = (DataTable)Session["s"];

            decimal TotalAmount = 0;
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                TotalAmount += Convert.ToDecimal(dttbl.Rows[i]["Amount"]);
            }
            lblTotalamount.Text = Convert.ToString(TotalAmount);
            lblDifference.Text = Convert.ToString((Convert.ToDecimal(txtOpeningBalance.Text)) - TotalAmount);
            tdTotal.Style.Add("display", "");
            tdDiscount.Style.Add("display", "");
        }
        else
        {
            tdTotal.Style.Add("display", "none");
            tdDiscount.Style.Add("display", "none");
        }
    }
    #endregion
    protected void dgGallery_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Label)e.Item.FindControl("lblInvoiceDate")).Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "InvoiceDate").ToString());

        }

    }
    protected void AddNodeLevel()
    {
        if (Convert.ToString(ViewState["id"]) != "")
        {
            objFile.id = Convert.ToInt32(ViewState["id"]);
            objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            DataTable dtlevel = new DataTable();
            dtlevel = objFile.getTreeLevel();
            lblLevel.Text = Convert.ToString(dtlevel.Rows[0]["TreeLevel"]) + "/6";
            if (Convert.ToString(dtlevel.Rows[0]["TreeLevel"]) == "6")
            {
                btn_menu_Ladger.Visible = false;
                btn_menu_addNew.Visible = false;
            }

        }
    }

    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }

            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
            {
                imgbtn_deletecontent.Enabled = false;
            }
            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {


            }

            if (Convert.ToString(dt.Rows[0]["IsAdd"]) != "True")
            {
                btn_menu_addNew.Enabled = false;
                btn_menu_Ladger.Enabled = false;
            }



            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {
                btn_menu_EditSubCategory.Enabled = false;
            }

        }
    }


}
