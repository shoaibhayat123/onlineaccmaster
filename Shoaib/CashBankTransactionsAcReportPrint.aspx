﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CashBankTransactionsAcReportPrint.aspx.cs"
    Inherits="CashBankTransactionsAcReportPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Tahoma; background: none; font-size: 10px;">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; font-weight: bold;">
               <tr id="trTrialBal" runat="server">
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Cash
                        / Bank Book Report<br />
                    </span><span style="font-size: 14px;">From :
                        <asp:Label ID="lblFromDate" runat="server" Style="padding-right: 15px;"></asp:Label>
                        To :
                        <asp:Label ID="lblToDate" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblBuyerName" runat="server"></asp:Label><br />
                    <span style="font-size: 12px; font-weight: normal">
                        <asp:Label ID="lblBuyerAddress" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress2" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress3" runat="server"></asp:Label></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <hr />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <asp:GridView ID="grdLedger" Width="800px" runat="server" ShowFooter="True" AutoGenerateColumns="False"
            AllowPaging="true" PageSize="500000" PagerSettings-Visible="false" PagerSettings-Mode="Numeric"
            HeaderStyle-CssClass="gridheader" FooterStyle-CssClass="gridFooter" AlternatingRowStyle-CssClass="gridAlternateItem"
            GridLines="none" BorderColor="black" BorderStyle="Dotted" BorderWidth="1" RowStyle-CssClass="gridItem"
            OnRowDataBound="grdLedger_RowDataBound">
            <EmptyDataTemplate>
                <div style="padding-bottom: 10px; width: 100%; text-align: center; font-family: Gisha;
                    font-size: 20px; background-color: #E1F5FD; color: #110F0F;">
                    <div style="padding-top: 10px; padding-bottom: 5px;">
                        No record found
                    </div>
                </div>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="Date">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="8%" HorizontalAlign="Center" CssClass=" gridpadding" />
                    <ItemTemplate>
                        <asp:Label ID="lbltransDate" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="A/C Title">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" CssClass=" gridpadding" />
                    <ItemTemplate>
                        <%#Eval("AcFileDesc")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" CssClass=" gridpadding" />
                    <ItemTemplate>
                        <%#Eval("Description")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Receive">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="15%" HorizontalAlign="Right" CssClass=" gridpadding" />
                    <FooterStyle HorizontalAlign="Right" Font-Size="14px" />
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("Receive")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        &nbsp; &nbsp;
                        <asp:Label ID="lblTotalReceive" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Payment">
                    <ItemStyle Width="15%" HorizontalAlign="Right" CssClass=" gridpadding" />
                    <HeaderStyle HorizontalAlign="Center" />
                    <FooterStyle HorizontalAlign="Right" Font-Size="14px" />
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("Payment")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        &nbsp; &nbsp;
                        <asp:Label ID="lblTotalPayment" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Balance">
                    <ItemStyle Width="12%" HorizontalAlign="Right" CssClass=" gridpadding" />
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <asp:Label ID="lblBalance" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
              <tr>
                <td style="text-align: right">
                    <asp:LinkButton Text="Print" ID="lnkPrint" OnClientClick="window.print();" runat="server"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>

                                <script type="text/javascript">

                                            var currentTime = new Date()
                                            var hours = currentTime.getHours()
                                             var Newhours=0;
                                            var minutes = currentTime.getMinutes()
                                            if (minutes < 10)
                                            {
                                            minutes = "0" + minutes
                                            }                                           
                                            if(hours >12)
                                            {
                                            Newhours= hours-12;
                                            }
                                            else
                                            {
                                              Newhours= hours;
                                            }
                                            document.write(Newhours + ":" + minutes + " ")
                                             if(hours > 11)
                                            {                                           
                                            document.write("PM")
                                            } else 
                                            {
                                            document.write("AM")
                                            }

                                </script>

                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
