﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class JournalVouchers : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();
    int Last = 0;
    int Corrent = 0;
    int FirstRecord = 0;
    int LastRecord = 0;
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();

        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        if (!IsPostBack)
        {
            ViewState["s"] = null;
            txtVoucherNo.Focus();
            FillAllLedgerAc();
            DataTable dt = new DataTable();
            dgGallery.DataSource = dt;
            dgGallery.DataBind();
            Session["Corrent"] = null;
            Session["FirstRecord"] = null;
            Session["LastRecord"] = null;
            Session["JournalVouchersSummary"] = null;
            if (Convert.ToString(Request["addnew"]) != "1")
            {

                bindpagging();

                if (Request["VoucherSimpleMainId"] == null)
                {
                    BindBankReceipt();
                }
                else
                {
                    DataTable dtInvoiceSummary = new DataTable();
                    dtInvoiceSummary = (DataTable)Session["JournalVouchersSummary"];
                    DataView dv = new DataView(dtInvoiceSummary);
                    dv.RowFilter = "id=" + Request["VoucherSimpleMainId"];
                    dtInvoiceSummary = dv.ToTable();
                    if (dtInvoiceSummary.Rows.Count > 0)
                    {
                        Session["Corrent"] = Convert.ToInt32(dtInvoiceSummary.Rows[0]["row"]);
                        BindBankReceipt();
                    }
                    else
                    {
                        Response.Redirect("default.aspx");
                    }
                }

            }
            else
            {
                objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objSalesInvoice.JVMainId = 0;
                DataTable dtBPV = new DataTable();
                objSalesInvoice.DataFrom = "JV";
                dtBPV = objSalesInvoice.getJvMain();
                if (dtBPV.Rows.Count > 0)
                {
                    int Last = Convert.ToInt32(dtBPV.Rows.Count) - 1;
                    txtVoucherNo.Text = Convert.ToString(Convert.ToDecimal(dtBPV.Rows[Last]["VoucherNo"]) + 1);
                    txtVoucherDate.Text = Convert.ToDateTime(dtBPV.Rows[Last]["VoucherDate"]).ToString("dd MMM yyyy");
                }
                else
                {
                    txtVoucherNo.Text = "1";
                    txtVoucherDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
                }
                spanPgging.Style.Add("display", "none");

                btnPrint.Style.Add("display", "none");
                btnDeleteInvoice.Style.Add("display", "none");
            }
        }
        SetUserRight();
    }


    #endregion
    #region Fill Accounts

    protected void FillAllLedgerAc()
    {
       
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtLedger = new DataTable();
            dtLedger = objSalesInvoice.getAcFileAllLedger();
            if (dtLedger.Rows.Count > 0)
            {

                ddlAcdesc.DataSource = dtLedger;
                ddlAcdesc.DataTextField = "Title";
                ddlAcdesc.DataValueField = "Id";
                ddlAcdesc.DataBind();
                ddlAcdesc.Items.Insert(0, new ListItem("Select A/C Title", "0"));



            }

       

    }
    #endregion

    #region Add Details
    protected void btn_saveInvoice_Click(object sender, EventArgs e)
    {
        

        if (Sessions.FromDate != null)
        {
            if (ViewState["s"] != null)
            {
                if ((Convert.ToDateTime(txtVoucherDate.Text) >= Convert.ToDateTime(Sessions.FromDate)) && (Convert.ToDateTime(txtVoucherDate.Text) <= Convert.ToDateTime(Sessions.ToDate)))
                {
                    if (Convert.ToDecimal(lblDifference.Text) == 0)
                    {
                        if (hdnVoucherSimpleMainId.Value == "")
                        {
                            hdnVoucherSimpleMainId.Value = "0";
                        }
                        /************ Add Invoice Summary **************/
                        if (Convert.ToInt32(hdnVoucherSimpleMainId.Value) > 0)
                        {
                            objSalesInvoice.id = Convert.ToInt32(hdnVoucherSimpleMainId.Value);
                        }
                        else
                        {
                            objSalesInvoice.id = 0;
                        }
                        objSalesInvoice.VoucherNo = Convert.ToDecimal(txtVoucherNo.Text);
                        objSalesInvoice.VoucherDate = Convert.ToDateTime(txtVoucherDate.Text);

                        objSalesInvoice.DataFrom = "JV";
                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                        objSalesInvoice.TotalAmount = Convert.ToDecimal(lblDebitAmount.Text);
                        DataTable dtresult = new DataTable();
                        dtresult = objSalesInvoice.AddEditJvMain();






                        /************ Add Invoice Detail **************/
                        objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["id"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                        objSalesInvoice.DataFrom = "JV";
                        objSalesInvoice.DeleteTrans();

                        if (dtresult.Rows.Count > 0)
                        {
                            if (ViewState["s"] != null)
                            {
                                DataTable dt = new DataTable();
                                dt = (DataTable)ViewState["s"];
                                if (dt.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        objSalesInvoice.id = 0;
                                        objSalesInvoice.AccountId = Convert.ToInt32(dt.Rows[i]["AccountId"].ToString());
                                        objSalesInvoice.DebitAmount = Convert.ToDecimal(dt.Rows[i]["DebitAmount"].ToString());
                                        objSalesInvoice.CreditAmount = Convert.ToDecimal(dt.Rows[i]["CreditAmount"].ToString());
                                        objSalesInvoice.Description = Convert.ToString(dt.Rows[i]["Description"].ToString());
                                        objSalesInvoice.JVMainId = Convert.ToInt32(dtresult.Rows[0]["id"].ToString()); /***** dtresult.Rows[0] is for VoucherSimpleMainId which is new****/
                                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                        objSalesInvoice.DataFrom = "JV";
                                        DataTable dtInvoiceSummary = new DataTable();
                                        dtInvoiceSummary = objSalesInvoice.AddEditJvDetails();

                                        /********* Add in trans **************/
                                        objSalesInvoice.TransId = 0;
                                        objSalesInvoice.TransactionDate = Convert.ToDateTime(txtVoucherDate.Text);
                                        objSalesInvoice.AcFileId = Convert.ToInt32(dt.Rows[i]["AccountId"].ToString());
                                        if (Convert.ToDecimal(dt.Rows[i]["DebitAmount"]) > 0)
                                        {
                                            objSalesInvoice.TransAmt = Convert.ToDecimal(dt.Rows[i]["DebitAmount"].ToString());
                                        }
                                        else
                                        {
                                            objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(dt.Rows[i]["CreditAmount"].ToString());
                                        }
                                        objSalesInvoice.Description = Convert.ToString(dt.Rows[i]["Description"].ToString());
                                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                        objSalesInvoice.DataFrom = "JV";
                                        objSalesInvoice.InvoiceNo = txtVoucherNo.Text;
                                        objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["id"].ToString());
                                        objSalesInvoice.AddEditTrans();


                                    }
                                }
                            }

                        }


                        if (Convert.ToInt32(hdnVoucherSimpleMainId.Value) > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Updated.');location.href='JournalVouchers.aspx';", true);

                        }

                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Saved.');location.href='JournalVouchers.aspx';", true);
                        }


                    }

                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('JV is not balanced.');", true);
                    }
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Date out of accouting year.');", true);

                }

            }


            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please Fill  Details.');", true);
            }
        }
    }


    #endregion

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("JournalVouchers.aspx");




    }
    #endregion
    #region Add New Bill details
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        if ((Convert.ToDecimal(txtAmount.Text) > 0 && Convert.ToDecimal(txtcreditamount.Text) == 0) || (Convert.ToDecimal(txtAmount.Text) == 0 && Convert.ToDecimal(txtcreditamount.Text) > 0))
        {
            filltable();
            if (hdndetail.Value == "")
            {
                DataRow row;

                if (dttbl.Rows.Count > 0)
                {

                    int maxid = 0;
                    for (int i = 0; i < dttbl.Rows.Count; i++)
                    {
                        if (dttbl.Rows[i]["Id"].ToString() != "")
                        {
                            if (maxid < Convert.ToInt32(dttbl.Rows[i]["Id"].ToString()))
                            {
                                maxid = Convert.ToInt32(dttbl.Rows[i]["Id"].ToString());
                            }
                        }
                    }

                    row = dttbl.NewRow();
                    row["Id"] = maxid + 1;
                    row["AccountId"] = Convert.ToInt32(ddlAcdesc.SelectedValue.ToString());
                    row["AccountDesc"] = Convert.ToString(ddlAcdesc.SelectedItem.Text);
                    row["DebitAmount"] = Convert.ToDecimal(txtAmount.Text);
                    row["CreditAmount"] = Convert.ToDecimal(txtcreditamount.Text);
                    row["Description"] = Convert.ToString(txtDescriptionDetail.Text);


                }
                else
                {

                    row = dttbl.NewRow();
                    row["Id"] = 1;
                    row["AccountId"] = Convert.ToInt32(ddlAcdesc.SelectedValue.ToString());
                    row["AccountDesc"] = Convert.ToString(ddlAcdesc.SelectedItem.Text);
                    row["DebitAmount"] = Convert.ToDecimal(txtAmount.Text);
                    row["CreditAmount"] = Convert.ToDecimal(txtcreditamount.Text);
                    row["Description"] = Convert.ToString(txtDescriptionDetail.Text);

                }
                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
            }

            else
            {
                for (int i = 0; i < dttbl.Rows.Count; i++)
                {
                    if (dttbl.Rows[i]["Id"].ToString() == hdndetail.Value)
                    {
                        dttbl.Rows[i]["AccountId"] = Convert.ToInt32(ddlAcdesc.SelectedValue);
                        dttbl.Rows[i]["AccountDesc"] = Convert.ToString(ddlAcdesc.SelectedItem.Text);
                        dttbl.Rows[i]["DebitAmount"] = Convert.ToDecimal(txtAmount.Text);
                        dttbl.Rows[i]["CreditAmount"] = Convert.ToDecimal(txtcreditamount.Text);
                        dttbl.Rows[i]["Description"] = Convert.ToString(txtDescriptionDetail.Text);

                    }
                }
            }

            ViewState.Add("s", dttbl);
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            ShowTotal();
            if (dttbl.Rows.Count > 0)
            {
                dgGallery.Visible = true;
            }

            ClearInvoiceDetail();
            ddlAcdesc.Focus();
        }

        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Enter valid amount.');", true);
        }
    }
    #endregion

    #region Grid Event
    protected void dgGallery_RowDataBound(object sender, DataGridItemEventArgs e)
    {




    }
    protected void dgGallery_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        dttbl = (DataTable)ViewState["s"];
        if (e.CommandName == "Delete")
        {
            int j = 0;
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    dttbl.Rows.RemoveAt(i);
                }
            }
            dttbl.AcceptChanges();
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            ViewState.Add("s", dttbl);
            ShowTotal();

        }


        if (e.CommandName == "Edit")
        {
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {

                    ddlAcdesc.SelectedValue = dttbl.Rows[i]["AccountId"].ToString();
                    txtAmount.Text = dttbl.Rows[i]["DebitAmount"].ToString();
                    txtcreditamount.Text = dttbl.Rows[i]["CreditAmount"].ToString();
                    txtDescriptionDetail.Text = dttbl.Rows[i]["Description"].ToString();
                    hdndetail.Value = dttbl.Rows[i]["Id"].ToString();


                }
            }
        }
    }
    #endregion

    #region ClearInvoiceDetail
    protected void ClearInvoiceDetail()
    {
        ddlAcdesc.SelectedValue = "0";
        txtDescriptionDetail.Text = "";
        txtAmount.Text = "0.00";
        txtDescriptionDetail.Text = "";
        txtcreditamount.Text = "0.00";
        hdndetail.Value = "";
    }
    #endregion

    #region Show Total
    protected void ShowTotal()
    {
        Decimal DebitAmount = 0;
        decimal CreditAmount = 0;
        Decimal Difference = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["s"];
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DebitAmount += Convert.ToDecimal(dt.Rows[i]["DebitAmount"]);
                CreditAmount += Convert.ToDecimal(dt.Rows[i]["CreditAmount"]);

            }
        }
        Difference = DebitAmount - CreditAmount;
        lblDebitAmount.Text = String.Format("{0:C}", DebitAmount).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        lblCreditAmount.Text = String.Format("{0:C}", CreditAmount).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        lblDifference.Text = String.Format("{0:C}", Difference).Replace('$', ' ').Replace('(', '-').Replace(')', ' ');


    }
    #endregion
    #region Pagging
    private void bindpagging()
    {
        Last = 0;
        Corrent = 0;
        FirstRecord = 0;
        LastRecord = 0;
        //  int row = Convert.ToInt32(Request["row"]);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.VoucherSimpleMainId = 0;
        DataTable dt = new DataTable();
        objSalesInvoice.DataFrom = "JV";
        dt = objSalesInvoice.getJvMain();
        if (dt.Rows.Count > 0)
        {
            Session["JournalVouchersSummary"] = dt;
            Last = dt.Rows.Count;
            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            Session["FirstRecord"] = FirstRecord;
            Session["LastRecord"] = LastRecord;
            Session["Corrent"] = LastRecord;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "location.href='JournalVouchers.aspx?addnew=1';", true);
        }


    }
    protected void lnkFirst_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["FirstRecord"];
        BindBankReceipt();

    }
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["FirstRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) - 1;
            BindBankReceipt();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["LastRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) + 1;
            BindBankReceipt();
        }
    }
    protected void lnLast_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["LastRecord"];
        BindBankReceipt();
    }
    #endregion

    #region Fill Bank Receipt summary
    protected void BindBankReceipt()
    {
        if (Session["JournalVouchersSummary"] != null)
        {
            DataTable dtJournalVouchersSummary = (DataTable)Session["JournalVouchersSummary"];
            if (dtJournalVouchersSummary.Rows.Count > 0)
            {

                DataView DvJournalVouchersSummary = dtJournalVouchersSummary.DefaultView;
                DvJournalVouchersSummary.RowFilter = "Row = '" + Session["Corrent"].ToString() + "'";
                dtJournalVouchersSummary = DvJournalVouchersSummary.ToTable();
                if (dtJournalVouchersSummary.Rows.Count > 0)
                {
                    txtVoucherNo.Text = Convert.ToString(dtJournalVouchersSummary.Rows[0]["VoucherNo"]);
                    txtVoucherDate.Text = Convert.ToDateTime(dtJournalVouchersSummary.Rows[0]["VoucherDate"].ToString()).ToString("dd MMM yyyy");
                    lblDebitAmount.Text = Convert.ToString(String.Format("{0:C}", dtJournalVouchersSummary.Rows[0]["TotalAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    lblCreditAmount.Text = Convert.ToString(String.Format("{0:C}", dtJournalVouchersSummary.Rows[0]["TotalAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    hdnVoucherSimpleMainId.Value = Convert.ToString(dtJournalVouchersSummary.Rows[0]["id"]);
                    fillBankReceiptDetails(Convert.ToInt32(dtJournalVouchersSummary.Rows[0]["id"]));

                }
            }

        }

    }
    #endregion
    protected void fillBankReceiptDetails(int pid)
    {
        ViewState["s"] = null;

        DataTable dtJvDetail = new DataTable();
        objSalesInvoice.JVMainId = pid;
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objSalesInvoice.DataFrom = "JV";
        dtJvDetail = objSalesInvoice.getJvDetails();

        if (dtJvDetail.Rows.Count > 0)
        {
            for (int i = 0; i < dtJvDetail.Rows.Count; i++)
            {
                filltable();
                DataRow row;
                row = dttbl.NewRow();
                row["Id"] = i + 1;
                row["AccountId"] = Convert.ToInt32(dtJvDetail.Rows[i]["AccountId"]);
                row["AccountDesc"] = Convert.ToString(dtJvDetail.Rows[i]["AccountDesc"]);
                row["DebitAmount"] = Convert.ToDecimal(dtJvDetail.Rows[i]["DebitAmount"]);
                row["CreditAmount"] = Convert.ToDecimal(dtJvDetail.Rows[i]["CreditAmount"]);
                row["Description"] = Convert.ToString(dtJvDetail.Rows[i]["Description"]);
                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
                ViewState.Add("s", dttbl);
            }
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            dgGallery.Visible = true;
            ShowTotal();
        }
        else
        {
            dgGallery.Visible = false;
        }

    }


    #region Fill session Table
    public void filltable()
    {
        if (ViewState["s"] == null)
        {
            DataColumn column = new DataColumn();
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "AccountId";
            column.Caption = "AccountId";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "AccountDesc";
            column.Caption = "AccountDesc";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "DebitAmount";
            column.Caption = "DebitAmount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "CreditAmount";
            column.Caption = "CreditAmount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Description";
            column.Caption = "Description";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


        }
        else
        {
            dttbl = (DataTable)ViewState["s"];
        }
    }
    #endregion


    #region Delete BankReceiptVoucher
    protected void btnDeleteInvoice_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(hdnVoucherSimpleMainId.Value) > 0)
        {
            int result = 0;
            objSalesInvoice.JVMainId = Convert.ToInt32(hdnVoucherSimpleMainId.Value);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.DataFrom = "JV";
            result = objSalesInvoice.deleteAllJV();
            Session["JournalVouchersSummary"] = null;
            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record deleted successfully.');location.href='JournalVouchers.aspx';", true);
            }
        }

    }
    #endregion
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        string urllast = "PrintJournalVouchers.aspx?VoucherSimpleMainId=" + hdnVoucherSimpleMainId.Value;
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,status=yes' );", true);
    }
    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }

            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
            {
                btnDeleteInvoice.Visible = false;
            }
            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

                btnPrint.Visible = false;

            }

            if (Convert.ToString(dt.Rows[0]["IsAdd"]) != "True")
            {
                btnAddNew.Visible = false;
                if (Convert.ToString(Request["addnew"]) == "1")
                {

                    ImageButton2.Visible = false;
                }
            }



            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {
                if (Convert.ToString(Request["addnew"]) != "1")
                {

                    ImageButton2.Visible = false;
                }
            }

        }
    }
    protected void lblbutton_Click(object sender, EventArgs e)
    {
        Response.Redirect("JournalVouchers.aspx?addnew=1");

    } 
}
