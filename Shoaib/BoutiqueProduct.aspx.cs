﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class BoutiqueProduct : System.Web.UI.Page
{
    #region DataMembers & Varriables
    SetCustomers objCust = new SetCustomers();
    int FillGridType ;
    #endregion
    #region Page Load Event

    protected void Page_Load(object sender, EventArgs e)
    {
       
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        txtItemCode.Focus();
        if (Sessions.CustomerCode == "" || Sessions.CustomerCode == null)
        {

            Response.Redirect("Default.aspx");

        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
            if (!IsPostBack)
            {
                btnBarCodePrint.Style.Add("display", "none");
                Session["FillGridType"] = null;
                FilItems();
                FillColor();
                FillSize();
                bindCostCenter();
                FillMainCategory();
                FillArticleType();
                FillArticleName();
            }
        }
    }

    #endregion
    #region Add/ Edit Product Category  Setup

    protected void btn_save_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
        if (rptrCostCenter.Items.Count > 0)
        {

            if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
            {
                objCust.ItemId = int.Parse(hdnItemId.Value);
            }
            else
            {
                objCust.ItemId = 0;
            }
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.BoutiqueItemDescription = txtItemDesc.Text;
            //objCust.BarcodeNo = txtBarCode.Text;
           // objCust.BarcodeNo = txtItemCode.Text;
            objCust.MainCategoryCode = Convert.ToInt32(ddlMainCategory.SelectedValue);
            objCust.SubCategoryCode = Convert.ToInt32(ddlSubCategory.SelectedValue);
            objCust.Unit = txtUnit.Text;
            objCust.Packing = txtPacking.Text;

            Nullable<decimal> nullPurchaseRate;
            nullPurchaseRate = null;
            if (txtPurchaseRate.Text != "")
            {
                nullPurchaseRate = Convert.ToDecimal(txtPurchaseRate.Text);
            }
            objCust.PurchaseRate = nullPurchaseRate;


            Nullable<decimal> nullSaleRate;
            nullSaleRate = null;
            if (txtSaleRate.Text != "")
            {
                nullSaleRate = Convert.ToDecimal(txtSaleRate.Text);
            }
            objCust.SaleRate = nullSaleRate;

            Nullable<decimal> nullSalesTax;
            nullSalesTax = null;
            if (txtSalesTax.Text != "")
            {
                nullSalesTax = Convert.ToDecimal(txtSalesTax.Text);
            }
            objCust.SalesTax = nullSalesTax;


            Nullable<decimal> nullDutyNumber;
            nullDutyNumber = null;
            if (txtDuty.Text != "")
            {
                nullDutyNumber = Convert.ToDecimal(txtDuty.Text);
            }
            objCust.DutyNumber = nullDutyNumber;



            if (chkInvetory.Checked == true)
            {
                objCust.Opbal = Convert.ToDecimal(lblTotal.Text);
            }
            else
            {
                objCust.Opbal = 0;
            }

            objCust.IsInvetory = Convert.ToBoolean(chkInvetory.Checked);
            objCust.ColorId = Convert.ToInt32(ddlColor.SelectedValue);
            objCust.SizeId = Convert.ToInt32(ddlSize.SelectedValue);
            objCust.BoutiqueItemCode = Convert.ToString(txtItemCode.Text).ToUpper();
            objCust.ArticalTypeId = Convert.ToInt32(ddlarticleType.SelectedValue);
            objCust.ArticleNameId = Convert.ToInt32(ddlarticleName.SelectedValue);
            if (lblPurchaseType.Text != "")
            {
                objCust.PurchaseType = lblPurchaseType.Text;
            }
            else
            {
                objCust.PurchaseType = "Contract Purchase";
            }
          

            objCust.ItemDesc = txtItemCode.Text.ToUpper() + " / " + ddlColor.SelectedItem.Text + " / " + ddlSize.SelectedItem.Text  + " / " +  txtItemDesc.Text;



            if (Sessions.UserRole.ToString() != "Admin")
            {
                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
                if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" || Convert.ToString(dt.Rows[0]["IsAdd"]) == "True")
                {

                    if (Convert.ToString(dt.Rows[0]["IsAdd"]) == "True" && (hdnItemId.Value == null || hdnItemId.Value == string.Empty))
                    {
                        int result = objCust.AddEditBoutiqueItems();

                        /* region add Cost center data*/
                        if (result != -1)
                        {
                            /* 1. delete Old records */
                            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            objCust.ItemId = result;
                            objCust.DeleteItemSetupCostCenterByItemId();

                            /* 2. Insert new records */
                            for (int i = 0; i < rptrCostCenter.Items.Count; i++)
                            {
                                //find control
                                HiddenField hdnCostcenterCode = (HiddenField)rptrCostCenter.Items[i].FindControl("hdnCostcenterCode");
                                TextBox txtCostCenter = (TextBox)rptrCostCenter.Items[i].FindControl("txtCostCenter");
                                objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                if (chkInvetory.Checked == true)
                                {
                                    objCust.OpeningQuty = Convert.ToDecimal(txtCostCenter.Text);
                                }
                                else
                                {
                                    objCust.OpeningQuty = 0;
                                }
                                objCust.ItemId = result;
                                objCust.ItemSetupCostCenterId = 0;
                                objCust.CostCenterCode = Convert.ToInt32(hdnCostcenterCode.Value);
                                objCust.SetItemSetupCostCenter();

                            }
                        }

                        if (hdnItemId.Value == null || hdnItemId.Value == string.Empty)
                        {
                            if (result != -1)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Product Setup successful added!');", true);
                                clear();
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                            }
                          
                            bindCostCenter();
                            FilItems();
                        }
                    }
                    else
                    {
                        if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" && hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                        {
                            int result = objCust.AddEditBoutiqueItems();

                            /* region add Cost center data*/
                            if (result != -1)
                            {
                                /* 1. delete Old records */
                                objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                objCust.ItemId = result;
                                objCust.DeleteItemSetupCostCenterByItemId();

                                /* 2. Insert new records */
                                for (int i = 0; i < rptrCostCenter.Items.Count; i++)
                                {
                                    //find control
                                    HiddenField hdnCostcenterCode = (HiddenField)rptrCostCenter.Items[i].FindControl("hdnCostcenterCode");
                                    TextBox txtCostCenter = (TextBox)rptrCostCenter.Items[i].FindControl("txtCostCenter");
                                    objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                    if (chkInvetory.Checked == true)
                                    {
                                        objCust.OpeningQuty = Convert.ToDecimal(txtCostCenter.Text);
                                    }
                                    else
                                    {
                                        objCust.OpeningQuty = 0;
                                    }
                                    objCust.ItemId = result;
                                    objCust.ItemSetupCostCenterId = 0;
                                    objCust.CostCenterCode = Convert.ToInt32(hdnCostcenterCode.Value);
                                    objCust.SetItemSetupCostCenter();

                                }
                            }

                            if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                            {
                                if (result != -1)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Product Setup successful updated!');", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                                }
                                clear();
                                bindCostCenter();
                                if (Convert.ToString(Session["FillGridType"]) == "2")
                                {
                                    FillSerchResult();
                                }
                                else
                                {
                                    FilItems();
                                }
       
                            }

                        }

                        else
                        {
                            if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='ProductSetup.aspx';", true);
                            }

                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='ProductSetup.aspx';", true);
                            }
                        }
                    }
                }
                else
                {
                    if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='ProductSetup.aspx';", true);
                    }

                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='ProductSetup.aspx';", true);
                    }
                }

            }
            else
            {

                int result = objCust.AddEditBoutiqueItems();

                /* region add Cost center data*/
                if (result != -1)
                {
                    /* 1. delete Old records */
                    objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                    objCust.ItemId = result;
                    objCust.DeleteItemSetupCostCenterByItemId();

                    /* 2. Insert new records */
                    for (int i = 0; i < rptrCostCenter.Items.Count; i++)
                    {
                        //find control
                        HiddenField hdnCostcenterCode = (HiddenField)rptrCostCenter.Items[i].FindControl("hdnCostcenterCode");
                        TextBox txtCostCenter = (TextBox)rptrCostCenter.Items[i].FindControl("txtCostCenter");
                        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                        if (chkInvetory.Checked == true)
                        {
                            objCust.OpeningQuty = Convert.ToDecimal(txtCostCenter.Text);
                        }
                        else
                        {
                            objCust.OpeningQuty = 0;
                        }
                        objCust.ItemId = result;
                        objCust.ItemSetupCostCenterId = 0;
                        objCust.CostCenterCode = Convert.ToInt32(hdnCostcenterCode.Value);
                        objCust.SetItemSetupCostCenter();

                    }
                }

                if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                {
                    if (result != -1)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Product Setup successful updated!');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                    }
                }
                else
                {
                    if (result != -1)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Product Setup successful added!');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                    }

                }
                clear();
                bindCostCenter();
                if (Convert.ToString(Session["FillGridType"]) == "2")
                {
                    FillSerchResult();
                }
                else
                {
                    FilItems();
                }
       
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please Add Cost Center First!');", true);
        }

    }

    #endregion
    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        clear();
        bindCostCenter();
    }
    #endregion
    #region Blank Controls
    private void clear()
    {

        txtDuty.Text = "";
        txtItemDesc.Text = "";
        txtBarCode.Text = "";
        txtPacking.Text = "";
        txtPurchaseRate.Text = "";
        txtSaleRate.Text = "";
        txtSalesTax.Text = "";
        txtUnit.Text = "PCS";
        lblTotal.Text = "0.00";
        chkInvetory.Checked = true;
        ddlSubCategory.SelectedValue = "0";
        ddlMainCategory.SelectedValue = "0";
        hdnItemId.Value = string.Empty;
        ddlColor.SelectedValue = "0";
        ddlSize.SelectedValue = "0";
        txtItemCode.Text = "";
        ddlarticleName.SelectedValue = "0";
        ddlarticleType.SelectedValue = "0";
        btnBarCodePrint.Style.Add("display", "none");
        //txtBoutiquePurchaseDiscount.Text = "0.00";
        //txtBoutiqueSalesDiscount.Text = "0.00";
    }
    #endregion
    #region Fill Item Setup
    protected void FillData(int ItemId)
    {
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.ItemId = ItemId;
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        if (ds.Tables[0].Rows.Count > 0)
        {
            btnBarCodePrint.Style.Add("display", " ");
            txtDuty.Text = Convert.ToString(ds.Tables[0].Rows[0]["DutyNumber"].ToString());
            txtItemDesc.Text = Convert.ToString(ds.Tables[0].Rows[0]["BoutiqueItemDescription"].ToString());
            txtBarCode.Text = Convert.ToString(ds.Tables[0].Rows[0]["BarcodeNo"].ToString());
            txtPacking.Text = Convert.ToString(ds.Tables[0].Rows[0]["Packing"].ToString());
            txtPurchaseRate.Text = Convert.ToString(ds.Tables[0].Rows[0]["PurchaseRate"].ToString());
            txtSaleRate.Text = Convert.ToString(ds.Tables[0].Rows[0]["SaleRate"].ToString());
            txtSalesTax.Text = Convert.ToString(ds.Tables[0].Rows[0]["SalesTax"].ToString());
            txtUnit.Text = Convert.ToString(ds.Tables[0].Rows[0]["Unit"].ToString());
            ddlMainCategory.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["MainCategoryCode"].ToString());
            lblTotal.Text = Convert.ToString(ds.Tables[0].Rows[0]["Opbal"].ToString());
            chkInvetory.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsInvetory"].ToString());
            FillSubCategory();
            ddlSubCategory.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["SubCategoryCode"].ToString());
            hdnItemId.Value = ItemId.ToString();
            ddlSize.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["SizeId"].ToString());
            ddlColor.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["ColorId"].ToString());
            txtItemCode.Text = Convert.ToString(ds.Tables[0].Rows[0]["BoutiqueItemCode"].ToString().ToUpper());
            ddlarticleType.SelectedValue= Convert.ToString(ds.Tables[0].Rows[0]["ArticalTypeId"].ToString());
            ddlarticleName.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["ArticleNameId"].ToString());
            //txtBoutiquePurchaseDiscount.Text = Convert.ToString(ds.Tables[0].Rows[0]["BoutiquePurchaseDiscount"].ToString());
            //txtBoutiqueSalesDiscount.Text = Convert.ToString(ds.Tables[0].Rows[0]["BoutiqueSalesDiscount"].ToString());
            lblPurchaseType.Text = Convert.ToString(ds.Tables[0].Rows[0]["PurchaseType"].ToString());
            
        }

        bindCostCenter();
        if (ds.Tables[0].Rows.Count > 0)
        {
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.ItemId = ItemId;
            DataSet dsCostcenter = new DataSet();
            dsCostcenter = objCust.getItemSetupCostCenterByItemId();
            for (int i = 0; i < rptrCostCenter.Items.Count; i++)
            {
                HiddenField hdnCostcenterCode = (HiddenField)rptrCostCenter.Items[i].FindControl("hdnCostcenterCode");
                TextBox txtCostCenter = (TextBox)rptrCostCenter.Items[i].FindControl("txtCostCenter");

                for (int j = 0; j < dsCostcenter.Tables[0].Rows.Count; j++)
                {
                    if (Convert.ToString(hdnCostcenterCode.Value) == Convert.ToString(dsCostcenter.Tables[0].Rows[j]["CostCenterCode"]))
                    {

                        txtCostCenter.Text = Convert.ToString(dsCostcenter.Tables[0].Rows[j]["OpeningQuty"]);
                        break;
                    }

                }

            }

        }

         

    }
    #endregion
    #region Fill Main Category  Setup
    protected void FilItems()
    {
           Session["FillGridType"] = 1;

            objCust.ItemId = 0;
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataSet ds = new DataSet();
            ds = objCust.getItems();
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 30)
                {
                    grdCustomer.AllowPaging = true;
                }
                grdCustomer.Visible = true;
                grdCustomer.DataSource = ds.Tables[0];
                grdCustomer.DataBind();
                lblError.Visible = false;
            }
            else
            {
                grdCustomer.Visible = false;
                lblError.Visible = true;
            }
       

    }
    #endregion
    #region GridView Events
    protected void grdCustomer_RowCommand(object sender, DataGridCommandEventArgs e)
    {

        if (e.CommandName == "Delete")
        {
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.ItemId = int.Parse(e.CommandArgument.ToString());
            objCust.DeleteItemSetupCostCenterByItemId();
            int result= objCust.DeleteBoutiqueItems();
            if (Convert.ToString(Session["FillGridType"]) == "2")
            {
                FillSerchResult();
            }
            else
            {
                FilItems();
            }
       
            if (result == 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Entries found against this Product, cannot delete');", true);
            }
        }

        if (e.CommandName == "Edit")
        {
            FillData(int.Parse(e.CommandArgument.ToString()));

        }
    }
    protected void grdCustomer_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {


            if (Sessions.UserRole.ToString() != "Admin")
            {


                LinkButton lbEdit = (LinkButton)e.Item.FindControl("lbEdit");
                LinkButton lbDelete = (LinkButton)e.Item.FindControl("lbDelete");

                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
                if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
                {
                    Response.Redirect(Request.UrlReferrer.ToString());
                }
                if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
                {

                    lbDelete.Visible = false;

                }
                if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
                {

                    lbEdit.Visible = false;

                }

            }
        }


    }

    protected void grdCustomer_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        grdCustomer.CurrentPageIndex = e.NewPageIndex;
        if (Convert.ToString(  Session["FillGridType"]) == "2")
        {
            FillSerchResult();
        }
        else
        {
            FilItems();
        }
       
    }
    #endregion
    #region Fill Main Category
    protected void FillMainCategory()
    {
       
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.MainCategoryCode = 0;
            DataSet ds = new DataSet();
            ds = objCust.GetMainCategory();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlMainCategory.DataSource = ds.Tables[0];
                ddlMainCategory.DataTextField = "MainCategoryName";
                ddlMainCategory.DataValueField = "MainCategoryCode";
                ddlMainCategory.DataBind();
                ddlMainCategory.Items.Insert(0, new ListItem("Select Brand", "0"));
            }
       

    }
    #endregion
    #region Fill Sub Category

    protected void FillSubCategory()
    {
          objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.MainCategoryCode = Convert.ToInt32(ddlMainCategory.SelectedValue);
            DataSet ds = new DataSet();
            ds = objCust.GetSubCategoryByMainCategory();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlSubCategory.DataSource = ds.Tables[0];
                ddlSubCategory.DataTextField = "SubCategoryName";
                ddlSubCategory.DataValueField = "SubCategoryCode";
                ddlSubCategory.DataBind();
                ddlSubCategory.Items.Insert(0, new ListItem("Select Label", "0"));
            }
       
    }
    #endregion

    protected void ddlMainCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSubCategory();
        ddlSubCategory.Focus();
    }
    protected void bindCostCenter()
    {
        objCust.CostCenterCode = 0;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.GetCostCenter();
        if (ds.Tables[0].Rows.Count > 0)
        {
            rptrCostCenter.DataSource = ds.Tables[0];
            rptrCostCenter.DataBind();

        }
    }
    protected void txtCostCenter_Onchanged(object sender, EventArgs e)
    {
        decimal TotalQuantity = 0;
        for (int i = 0; i < rptrCostCenter.Items.Count; i++)
        {
            //find control
            TextBox txtCostCenter = (TextBox)rptrCostCenter.Items[i].FindControl("txtCostCenter");
            if (txtCostCenter.Text != "")
            {
                TotalQuantity += Convert.ToDecimal(txtCostCenter.Text);
            }
            else
            {
                txtCostCenter.Text = "0.00";
            }


        }
        lblTotal.Text = Convert.ToString(TotalQuantity);


    }



    protected void FillColor()
    {
            objCust.ItemId = 0;
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataSet ds = new DataSet();
            ds = objCust.GetColor();
            if (ds.Tables[0].Rows.Count > 0)
            {

                ddlColor.DataSource = ds.Tables[0];
                ddlColor.DataTextField = "Color";
                ddlColor.DataValueField = "ColorId";
                
                ddlColor.DataBind();
                ddlColor.Items.Insert(0, new ListItem("Select Color", "0"));
            }
    }

    protected void FillSize()
    {
        objCust.ItemId = 0;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.GetSize();
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlSize.DataSource = ds.Tables[0];
            ddlSize.DataTextField = "Size";
            ddlSize.DataValueField = "SizeId";

            ddlSize.DataBind();
            ddlSize.Items.Insert(0, new ListItem("Select Size", "0"));
        }
    }




    protected void FillArticleType()
    {
        objCust.ArticalTypeId = 0;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.GetBoutiqueArticalType();
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlarticleType.DataSource = ds.Tables[0];
            ddlarticleType.DataTextField = "ArticalType";
            ddlarticleType.DataValueField = "ArticalTypeId";

            ddlarticleType.DataBind();
            ddlarticleType.Items.Insert(0, new ListItem("Select article Type", "0"));
        }
    }


    protected void FillArticleName()
    {
        objCust.ArticleNameId = 0;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.GetBoutiqueArticalName();
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlarticleName.DataSource = ds.Tables[0];
            ddlarticleName.DataTextField = "ArticleName";
            ddlarticleName.DataValueField = "ArticleNameId";

            ddlarticleName.DataBind();
            ddlarticleName.Items.Insert(0, new ListItem("Select article Name", "0"));
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        FillSerchResult();       

    }

    protected void FillSerchResult()
    {
        Session["FillGridType"] = 2;
        objCust.ItemDesc = txtSerch.Text;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.SearchBoutiqueItem();
        if (ds.Tables[0].Rows.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 30)
            {
                grdCustomer.AllowPaging = true;
            }
            grdCustomer.Visible = true;
            grdCustomer.DataSource = ds.Tables[0];
            grdCustomer.DataBind();
            lblError.Visible = false;
        }
        else
        {
            grdCustomer.Visible = false;
            lblError.Visible = true;
        }
    }
    protected void btnBarCodePrint_Click(object sender, EventArgs e)
    {
        string urllast = "PrintBarcode.aspx?barcode=" + Convert.ToString(hdnItemId.Value);
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=500,width=320,scrollbars=1,status=yes' );", true);

    }
}