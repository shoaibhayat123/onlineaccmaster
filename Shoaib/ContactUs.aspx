﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaticPages.master" AutoEventWireup="true"
    CodeFile="ContactUs.aspx.cs" Inherits="ContactUs" %>

<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content_wraper5">
        <div id="content_midd5">
            <div class="acc-features-middle-bg">
                <div class="top-heading">
                    <h1>
                        Contact Us</h1>
                </div>
            </div>
            <div class="accmiddle-features-bg-2" style="min-height: 520px">
                <div class="features-wrapper">
                    <div class="features-box">
                        <div style="float: left">
                            <div class="features-left-link-box">
                                <div class="left-link-Content">
                                    <h2 style="text-decoration: underline; font-size: 18px;">
                                        AccMaster</h2>
                                    <br />
                                    <ul style="margin-left: 20px">
                                        <li><a href="index.aspx">Home</a></li>
                                        <li><a href="features.aspx">Features</a></li>
                                        <li><a href="pricing.aspx">Product & Pricing</a></li>
                                        <li><a href="PayOnline.aspx">Pay Online</a></li>
                                        <li><a href="Privacy.aspx">Privacy</a></li>
                                        <li><a href="Terms.aspx">Terms</a></li>
                                        <li><a href="ContactUs.aspx">Contact us</a></li>
                                        <li><a href="AboutUs.aspx">About Us</a></li>
                                        <li><a href="Careers.aspx">Careers</a></li>
                                    </ul>
                                </div>
                                <div class="left-link-bottom">
                                    <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                                </div>
                            </div>
                            <div class="features-left-link-box" style="clear: both; margin-top: 20px;">
                               <div class="left-link-Content" style=" min-height:100px;line-height:0.6">
                                   
                                    <asp:Literal ID="ltrlAddress" runat="server"></asp:Literal>
                                </div>
                                <div class="left-link-bottom">
                                    <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                                </div>
                            </div>
                        </div>
                        <div class="feat-right-part-box">
                            <div class="feta-right-part-content">
                                <div class="feat-right-box-dtls">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" valign="top">
                                                <table width="400" border="0" cellspacing="0" cellpadding="0" style="padding-left: 10px;">
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <table width="280" style="padding: 0 0 0 10px;" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <sup>*</sup> <strong>Your name</strong>:
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtName" runat="server" Style="background: none; border: solid 1px #6b5645;
                                                                            height: 20px; color: Black; width: 300px;"></asp:TextBox>
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:RequiredFieldValidator ID="rfvname" runat="server" ValidationGroup="Respond"
                                                                            ErrorMessage="Please Enter Name " ControlToValidate="txtName" Display="None"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td rowspan="3" align="left" style="padding-left: 10px;">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <table width="280" style="padding: 0 0 0 10px;" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <sup>*</sup> <strong>Your e-mail address:</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtEmail" runat="server" Style="background: none; border: solid 1px #6b5645;
                                                                            height: 20px; color: Black; width: 300px;"></asp:TextBox>
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ValidationGroup="Respond"
                                                                            ControlToValidate="txtEmail" ErrorMessage="! Invalid Email " ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                            Display="None"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Respond"
                                                                            ErrorMessage="Please Enter Your e-mail address " ControlToValidate="txtEmail"
                                                                            Display="None"></asp:RequiredFieldValidator>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <table width="280" style="padding: 0 0 0 10px;" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <strong>Phone: </strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPhone" runat="server" Style="background: none; border: solid 1px #6b5645;
                                                                            height: 20px; color: Black; width: 300px;"></asp:TextBox>
                                                                        <br />
                                                                        &nbsp;
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" style="padding: 0 0 0 10px;" width="280">
                                                                <tr>
                                                                    <td>
                                                                        <strong>Message:</strong>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <label>
                                                                            <asp:TextBox ID="txtComment" runat="server" cols="45" Rows="5" Style="background: none;
                                                                                border: solid 1px #6b5645; height: 100px; color: Black; width: 350px;" TextMode="MultiLine"></asp:TextBox>
                                                                        </label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding: 0 0 0 10px;">
                                                            <br />
                                                            <cc1:CaptchaControl ID="ccJoin" runat="server" CaptchaBackgroundNoise="none" CaptchaLength="6"
                                                                CaptchaHeight="50" CaptchaWidth="220" CaptchaLineNoise="None" CaptchaMinTimeout="5"
                                                                FontColor="Black" BackColor="#E4FCC8" CaptchaMaxTimeout="240" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding: 0 0 0 10px;">
                                                            <br />
                                                            Retype The Text as shown above:
                                                            <div id="divCommentMsg" visible="false" runat="server" style="color: Red;">
                                                                Enter the letters as they are shown in the image below !!!
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 30px">
                                                        <br />
                                                        <td style="padding: 0 0 0 10px;">
                                                            <asp:TextBox ID="txtCap" Style="background: none; border: solid 1px #6b5645; height: 20px;
                                                                color: Black; width: 300px;" runat="server"> </asp:TextBox>
                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" Display="None"
                                                                ControlToValidate="txtCap" ErrorMessage="Please Enter letters as they are shown in the image above"
                                                                ValidationGroup="Respond">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table border="0" cellpadding="0" cellspacing="0" style="padding: 0 0 0 0px;" width="280">
                                                                <tr>
                                                                    <td>
                                                                        <div class="button" style="padding-top: 10px;">
                                                                            <ul>
                                                                                <li>
                                                                                    <asp:LinkButton ID="btnResponse" ValidationGroup="Respond" Text="Submit" runat="server"
                                                                                        OnClick="btnResponse_Click"></asp:LinkButton></li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <asp:ValidationSummary ID="vs1" ShowSummary="false" runat="server" ValidationGroup="Respond"
                                                        ShowMessageBox="true" />
                                                </table>
                                            </td>
                                            <td style="vertical-align: top; width: 250px; text-align: right; padding-right: 0px;
                                                border: solid 0px red; margin-right: 0px">
                                                <img src="images/Contact_Us.png" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="feat-right-part-bottom">
                                <img src="images/feat-right-part-bottom-bg.jpg" alt="" />
                            </div>
                        </div>
                    </div>
</asp:Content>
