﻿

<%@ Page Title="" Language="C#" MasterPageFile="~/StaticPages.master" AutoEventWireup="true" CodeFile="OurClients.aspx.cs" Inherits="OurClients" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content_wraper5">
        <div id="content_midd5">
            <div class="acc-features-middle-bg">
                <div class="top-heading">
                    <h1>List of some of our satisfied customers</h1>
                </div>
            </div>
            <div class="accmiddle-features-bg-2" style="min-height: 520px">
                <div class="features-wrapper">
                    <div class="features-box">
                    <div style=" float:left">
                        <div class="features-left-link-box">
                            <div class="left-link-Content">
                                <h2 style=" text-decoration:underline; font-size:18px;">
                                  AccMaster</h2><br />
                                <ul style=" margin-left:20px">
                                    <li><a href="index.aspx">Home</a></li>
                                    <li><a href="features.aspx">Features</a></li>
                                    <li><a href="pricing.aspx">Product & Pricing</a></li>
                                    <li><a href="PayOnline.aspx">Pay Online</a></li>
                                    <li><a href="Privacy.aspx">Privacy</a></li>
                                    <li><a href="Terms.aspx">Terms</a></li>
                                    <li><a href="ContactUs.aspx">Contact us</a></li>
                                    <li><a href="AboutUs.aspx">About Us</a></li>
                                    <li><a href="Careers.aspx">Careers</a></li>
                                </ul>
                            </div>
                            <div class="left-link-bottom">
                                <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                            </div>
                        </div>
                         <div class="features-left-link-box" style=" clear:both; margin-top:20px;">
                           <div class="left-link-Content" style="min-height: 100px;line-height:0.6">
                                    <asp:Label ID="ltrlAddress" runat="server" Text="Label"></asp:Label>
                                </div>
                            <div class="left-link-bottom">
                                <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                            </div>
                        </div>
                        </div>
                        <h1>Our Clients</h1>
                        <p class="normaltextinpages">Great work can only come from great partnerships. Here are just a few of the amazing clients that allow us to come to work every day and push our boundaries. And theirs. </p>
                        <div class="img-main-box"><!--open img-main-box1-->
                        
                        <div class="img-h3">  
                        <h3 >Boutiques & Shoes</h3>
                        </div>

                        <div class="img-sml-box"><img src="images/tsm&co.png" style="margin:14px 0 0 21px;" /></div>
                        <div class="img-sml-box"><img src="images/humayun_saeed.png" style="margin:19px 0 0 21px;" /></div>
                        <div class="img-sml-box"><img src="images/AlHamd.png" style="margin:13px 0 0 21px;" /></div>
                        <div class="img-sml-box"><img src="images/ary_fashion.PNG" style="margin:14px 0 0 21px;" /></div>
                        <div class="img-sml-box"><img src="images/the_designers_2.PNG" style="margin:9px 0 0 21px;" /></div>
                        <div class="img-sml-box"><img src="images/AAA-Design.png" style="margin:9px 0 0 46px;" /></div>
                        <div class="img-sml-box"><img src="images/everlightfashion.PNG" style="margin:14px 0 0 21px;" /></div>
                        <div class="img-sml-box"><img src="images/taj_Collection.PNG" style="margin:14px 0 0 21px;" /></div>
                        <div class="img-sml-box"><img src="images/TajMuscat.png" style="margin:19px 0 0 21px;" /></div>
                        
                        
                        <div class="img-h3">
                        <h3>Ip Law Firms</h3>
                        </div>

                        <div class="img-sml-box"><img src="images/zallp.png" style="margin:14px 0 0 21px;" /></div>
                        <div class="img-sml-box"><img src="images/AAAssociates.png" style="margin:24px 0 0 21px;" /></div>
                        <div class="img-sml-box"><img src="images/surridgeandbeecheno.png" style="margin:19px 0 0 21px;" /></div>
                       
                       
                        <div class="img-h3">
                        <h3>Manufacturers, Retailers & Wholesellers</h3>
                        </div>
                            
                        <div class="img-sml-box"><img src="images/AM_Industry_2.png" style="margin:9px 0 0 46px;" /></div>
                        <div class="img-sml-box"><img src="images/mehran_metal_ware.png" style="margin:14px 0 0 21px;" /></div>
                        <div class="img-sml-box"><img src="images/munirIndustry.png" style="margin:11px 0 0 21px;" /></div>
                        <div class="img-sml-box"><img src="images/Ahmed-Siddique-Bhalla.png" style="margin:13px 0 0 21px;" /></div>


                        </div><!--close img-main-box-->
    
   
    </div>
</asp:Content>
