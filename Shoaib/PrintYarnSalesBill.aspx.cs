﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintYarnSalesBill : System.Web.UI.Page
{
       SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    User objUser = new User();
    SetCustomers objCust = new SetCustomers();
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (!IsPostBack)
        {
            if (Request["BrokerageId"] != null)
            {
                if (Sessions.CustomerCode != null && Convert.ToString(Sessions.CustomerCode) != "")
                {
                    
                        bindSupplier();
                        FillLogo();
                        BindYarn();
                        lblUser.Text = Sessions.UseLoginName.ToString();
                }
                else
                {
                    Response.Redirect("default.aspx");
                }

            }


        }


    }
    
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            lblSupplierTelephone.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);
            lblSellerRegNo.Text = Convert.ToString(dtSupplier.Rows[0]["SalesTaxNo"]);
            lblSellerNTN.Text = Convert.ToString(dtSupplier.Rows[0]["PanNo"]);

        }
    }

   


    public string NumberToText(int number)
    {
        if (number == 0) return "Zero";

        if (number == -2147483648) return "Minus Two Hundred and Fourteen Crore Seventy Four Lakh Eighty Three Thousand Six Hundred and Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        if (number < 0)
        {
            sb.Append("Minus ");
            number = -number;
        }

        string[] words0 = {"" ,"One ", "Two ", "Three ", "Four ", 
                "Five " ,"Six ", "Seven ", "Eight ", "Nine "};

        string[] words1 = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", 
                "Fifteen ","Sixteen ","Seventeen ","Eighteen ", "Nineteen "};

        string[] words2 = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", 
                "Seventy ","Eighty ", "Ninety "};

        string[] words3 = { "Thousand ", "Lakh ", "Crore " };

        num[0] = number % 1000; // units 
        num[1] = number / 1000;
        num[2] = number / 100000;
        num[1] = num[1] - 100 * num[2]; // thousands 
        num[3] = number / 10000000; // crores 
        num[2] = num[2] - 100 * num[3]; // lakhs 

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }


        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;

            u = num[i] % 10; // ones 
            t = num[i] / 10;
            h = num[i] / 100; // hundreds 
            t = t - 10 * h; // tens 

            if (h > 0) sb.Append(words0[h] + "Hundred ");

            if (u > 0 || t > 0)
            {
                if (h > 0 || i == 0) sb.Append("and ");

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);
            }

            if (i != 0) sb.Append(words3[i - 1]);

        }
        return sb.ToString().TrimEnd();
    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }


    }
    #endregion

    protected void BindYarn()
    {
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(Request["BrokerageId"]);
        DataTable dt = new DataTable();
        objSalesInvoice.DataFrom = "YARNBRKR";
        dt = objSalesInvoice.getYarnbrokerage();

        if (dt.Rows.Count > 0)
        {
            lblSaleDate.Text = Convert.ToDateTime(dt.Rows[0]["SaleDate"]).ToString("dd MMM yyyy");

            lblSaleCrDays.Text = Convert.ToString(dt.Rows[0]["SaleCrdays"]);

           // lblPurchaseCrDays.Text = Convert.ToString(dt.Rows[0]["PurchaseCrDays"]);
            lblSoldTo.Text = Convert.ToString(dt.Rows[0]["SoldPerson"]);
           // lblPurchasedFrom.Text = Convert.ToString(dt.Rows[0]["PurchaseFromPerson"]);
            //lblPL.Text = Convert.ToString(dt.Rows[0]["plName"]);
            lblItem.Text = Convert.ToString(dt.Rows[0]["item"]);
          
            lblDoNo.Text = Convert.ToString(dt.Rows[0]["DoNumber"]);
            lblDeliveredTo.Text = Convert.ToString(dt.Rows[0]["deliverTo"]);
            lblCartons.Text = Convert.ToString(dt.Rows[0]["Cartons"]);
            lblKgs.Text = Convert.ToString(dt.Rows[0]["Kgs"]);
            
            lblLbs.Text = Convert.ToString(dt.Rows[0]["Lbs"]);
            lblSaleRate.Text = Convert.ToString(String.Format("{0:C}", dt.Rows[0]["SaleRate"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
            lblSaleAmount.Text = Convert.ToString(String.Format("{0:C}", dt.Rows[0]["SaleAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
           // lblPurchaseRate.Text = Convert.ToString(String.Format("{0:C}", dt.Rows[0]["PurcheseRate"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
           // lblPurchaseAmount.Text = Convert.ToString(String.Format("{0:C}", dt.Rows[0]["PurchaseAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
            lblCopsamount.Text = Convert.ToString(String.Format("{0:C}", dt.Rows[0]["CopsAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
           // lblPlAmount.Text = Convert.ToString(String.Format("{0:C}", dt.Rows[0]["PLAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
            lblDescription.Text = Convert.ToString(dt.Rows[0]["Description"]);
            lblAgeSale.Text = Convert.ToString(dt.Rows[0]["SalesAge"]); ;
           // lblAgePurchase.Text = Convert.ToString(dt.Rows[0]["PurchaseAge"]); ;
        }

    }
   
}
