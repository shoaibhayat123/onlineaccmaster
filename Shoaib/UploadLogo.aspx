﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="UploadLogo.aspx.cs" Inherits="UploadLogo" Title="" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphead" runat="Server">
    Logo Setup
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainHome" runat="Server">
 
            <table cellspacing="0" style="float: left" border="0" cellpadding="5" cellspacing="5">
                <tr>
                    <td width="100%" valign="top" style="left: 5; padding-top: 10px;">
                        <table style="margin-left: 10px;">
                            <tr>
                                <td align="left" style=" vertical-align:top;">
                                    Upload Logo(210*110px):
                                </td>
                                <td align="left" style=" vertical-align:top;">
                                    <asp:FileUpload ID="flPicture" style=" vertical-align:top;" runat="server" />
                                    <asp:Image ID="imgLogo" style=" vertical-align:top;"  runat="server" 
                                        />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 8px;">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:Button ID="btn_save" runat="server" Text="Update" OnClick="btn_save_Click" ValidationGroup="v" />
                                    <br />
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                        ShowSummary="False" ValidationGroup="v" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
       
</asp:Content>
