﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class CurrencySetup : System.Web.UI.Page
{
    #region DataMembers & varriables
    SetCustomers objCust = new SetCustomers();
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (!IsPostBack)
        {
            txtCurrencyDesc.Focus();
            if (Sessions.CustomerCode != null && Sessions.CustomerCode != "")
            {
                FillData();
            }
        }
        SetUserRight();
    }
    #endregion

    #region Update Date Format
    protected void btn_save_Click(object sender, EventArgs e)
    {
        if (Sessions.CustomerCode != null && Sessions.CustomerCode != "")
        {

            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.CurrencyDesc = txtCurrencyDesc.Text;
            objCust.CurrencySymbol = txtCurrencySymbol.Text;

            objCust.SetCurrency();
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Currency Format Successfully Updated.');", true);


        }
    }
    #endregion

    #region Fill Date Format
    private void FillData()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetCurrency();
        if (ds.Tables[0].Rows.Count > 0)
        {
            txtCurrencyDesc.Text = ds.Tables[0].Rows[0]["CurrencyDesc"].ToString();
            txtCurrencySymbol.Text = ds.Tables[0].Rows[0]["CurrencySymbol"].ToString();
        }

    }
    #endregion
    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }
            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }

            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {

                btn_save.Enabled = false;

            }

        }
    }
}
