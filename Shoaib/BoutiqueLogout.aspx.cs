﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class BoutiqueLogout : System.Web.UI.Page
{
    clsCookie ck = new clsCookie();
    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Abandon();

        ck.DeleteCookie("UserId");
        ck.DeleteCookie("UseLoginName");
        ck.DeleteCookie("CustomerCode");
        ck.DeleteCookie("CustomerName");
        ck.DeleteCookie("CityName");
        ck.DeleteCookie("CountryName");
        ck.DeleteCookie("FromDate");
        ck.DeleteCookie("ToDate");
        Response.Redirect("BoutiqueLogin.aspx");

    }
}