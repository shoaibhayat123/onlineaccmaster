﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class SingleTMSearchClient : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    TmCountry ObjTmcountry = new TmCountry();

    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();
    DataTable dttbl2 = new DataTable();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    int Last = 0;
    int Corrent = 0;
    int FirstRecord = 0;
    int LastRecord = 0;

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessionsClient();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        if (!IsPostBack)
        {


            FillClient();
            FillStatus();
            BindClass();
            Session["Corrent"] = null;
            Session["FirstRecord"] = null;
            Session["LastRecord"] = null;
            Session["Yarnbrokerage"] = null;
            Session["s"] = null;
            Session["s2"] = null;
            if (Convert.ToString(Request["addnew"]) != "1")
            {

                bindpagging();
                if (Request["Id"] == null)
                {
                    //  BindInvoice();
                }
                else
                {
                    DataTable dtInvoiceSummary = new DataTable();
                    dtInvoiceSummary = (DataTable)Session["Yarnbrokerage"];
                    DataView dv = new DataView(dtInvoiceSummary);
                    dv.RowFilter = "Id=" + Request["Id"];
                    dtInvoiceSummary = dv.ToTable();
                    if (dtInvoiceSummary.Rows.Count > 0)
                    {
                        Session["Corrent"] = Convert.ToInt32(dtInvoiceSummary.Rows[0]["row"]);
                        BindInvoice();
                    }
                    else
                    {
                        Response.Redirect("default.aspx");
                    }
                }
                txtApplication.Focus();

            }
            else
            {

                //spanPgging.Style.Add("display", "none");

                //txtSearch.Focus();

            }


        }


    }




    #region Pagging
    private void bindpagging()
    {
        Last = 0;
        Corrent = 0;
        FirstRecord = 0;
        LastRecord = 0;

        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.Id = 0;
        DataSet ds = ObjTmcountry.GetTmTradeMark();
        DataTable dt = new DataTable();

        dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            Session["Yarnbrokerage"] = dt;
            Last = dt.Rows.Count;
            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            //Corrent = Convert.ToInt32(dt.Rows[0]["Row"]);
            Session["FirstRecord"] = FirstRecord;
            Session["LastRecord"] = LastRecord;
            Session["Corrent"] = LastRecord;

        }
        else
        { Response.Redirect("SingleTMSearchClient.aspx?addnew=1"); }
    }
    protected void lnkFirst_Click(object sender, EventArgs e)
    {
        clearOldRecords();
        Session["Corrent"] = Session["FirstRecord"];
        BindInvoice();
        txtApplication.Focus();

    }
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["FirstRecord"])
        {
            clearOldRecords();
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) - 1;
            BindInvoice();
            txtApplication.Focus();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["LastRecord"])
        {
            clearOldRecords();
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) + 1;
            BindInvoice();
            txtApplication.Focus();
        }
    }
    protected void lnLast_Click(object sender, EventArgs e)
    {
        clearOldRecords();
        Session["Corrent"] = Session["LastRecord"];
        BindInvoice();
        txtApplication.Focus();
    }
    #endregion

    protected void clearOldRecords()
    {
        txtApplication.Text = "";
        txtMemo.Text = "";
        txttradeMark.Text = "";
        txtGoods.Text = "";
        txtAcRecDate.Text = "";
        txtFilmSubDate.Text = "";
        txtNoteRecDate.Text = "";
        txtDemandNoteDeposit.Text = "";
        FillClient();
        FillStatus();
        BindClass();
        ddlAcceptenceToClient.SelectedValue = "Yes";
        ddlOpposition.SelectedValue = "Yes";
        ddlNoteSendToClient.SelectedValue = "Yes";
        txtRemark.Text = "";

        txtReminder.Text = "";
        txtReminderDate.Text = "";
        txtDateOfFilling.Text = "";
        txtTakeOverDate.Text = "";
        txtPeriod.Text = "";
        rdbLabel.Checked = true;
        txtTm11Filled.Text = "";
        txtTm12Filled.Text = "";
        txtCertificateRecDate.Text = "";
        ddlShowCase.SelectedValue = "No";
        Session["s"] = null;
        Session["s2"] = null;

    }





    protected void FillClient()
    {
        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.TmCustomerId = 0;
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();

        ds = ObjTmcountry.GetTmCustomerMaindetails();
        dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            ddlClient.DataSource = dt;
            ddlClient.DataTextField = "TmCustomerName";
            ddlClient.DataValueField = "TmCustomerId";
            ddlClient.DataBind();
        }

    }
    protected void FillStatus()
    {
        ObjTmcountry.TmFileStatusId = 0;
        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = ObjTmcountry.GetTmFileStatus();
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlStatus.DataSource = ds.Tables[0];
            ddlStatus.DataTextField = "TmFileStatus";
            ddlStatus.DataValueField = "TmFileStatusId";
            ddlStatus.DataBind();


        }

    }



    #region Fill invoice summary
    protected void BindInvoice()
    {
        if (Session["Yarnbrokerage"] != null)
        {
            DataTable dtYarnbrokerage = (DataTable)Session["Yarnbrokerage"];
            if (dtYarnbrokerage.Rows.Count > 0)
            {

                DataView DvYarnbrokerage = dtYarnbrokerage.DefaultView;
                DvYarnbrokerage.RowFilter = "Row = '" + Session["Corrent"].ToString() + "'";
                dtYarnbrokerage = DvYarnbrokerage.ToTable();
                if (dtYarnbrokerage.Rows.Count > 0)
                {
                    //  clearOldRecords(); 
                    int temp = 0;

                    hdnBrokerageId.Value = Convert.ToString(dtYarnbrokerage.Rows[0]["Id"]);
                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["ApplicationNum"]) != "0")
                    {
                        txtApplication.Text = Convert.ToString(dtYarnbrokerage.Rows[0]["ApplicationNum"]);
                    }
                    txtMemo.Text = Convert.ToString(dtYarnbrokerage.Rows[0]["MemoNumber"]);
                    txttradeMark.Text = Convert.ToString(dtYarnbrokerage.Rows[0]["TradeMark"]);
                    txtGoods.Text = Convert.ToString(dtYarnbrokerage.Rows[0]["Goods"]);
                    ddlClient.SelectedValue = Convert.ToString(dtYarnbrokerage.Rows[0]["TmCustomerId"]);



                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["AccRecDate"]) != "")
                    {
                        txtAcRecDate.Text = Convert.ToDateTime(dtYarnbrokerage.Rows[0]["AccRecDate"].ToString()).ToString("dd MMM yyyy");
                    }
                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["FilmRecDate"]) != "")
                    {
                        txtFilmSubDate.Text = Convert.ToDateTime(dtYarnbrokerage.Rows[0]["FilmRecDate"].ToString()).ToString("dd MMM yyyy");
                    }

                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["DemandNoteRecDate"]) != "")
                    {
                        txtNoteRecDate.Text = Convert.ToDateTime(dtYarnbrokerage.Rows[0]["DemandNoteRecDate"].ToString()).ToString("dd MMM yyyy");
                    }

                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["DemandNoteDeposit"]) != "")
                    {
                        txtDemandNoteDeposit.Text = Convert.ToDateTime(dtYarnbrokerage.Rows[0]["DemandNoteDeposit"].ToString()).ToString("dd MMM yyyy");
                    }
                    else
                    {
                        txtDemandNoteDeposit.Text = "";
                    }

                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["IsAcceptance"]).ToLower() == "true")
                    {
                        ddlAcceptenceToClient.SelectedValue = "Yes";
                    }
                    else
                    {
                        ddlAcceptenceToClient.SelectedValue = "No";
                    }

                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["IsOpposition"]).ToLower() == "true")
                    {
                        ddlOpposition.SelectedValue = "Yes";
                    }
                    else
                    {
                        ddlOpposition.SelectedValue = "No";
                    }

                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["IsNoteSendToClient"]).ToLower() == "true")
                    {
                        ddlNoteSendToClient.SelectedValue = "Yes";
                    }
                    else
                    {
                        ddlNoteSendToClient.SelectedValue = "No";
                    }


                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["CertificateIssued"]).ToLower() == "true")
                    {
                        ddlCertificateIssued.SelectedValue = "Yes";
                    }
                    else
                    {
                        ddlCertificateIssued.SelectedValue = "No";
                    }

                    txtRemark.Text = Convert.ToString(dtYarnbrokerage.Rows[0]["Remark"]);

                    ddlStatus.SelectedValue = Convert.ToString(dtYarnbrokerage.Rows[0]["TmFileStatusId"]);
                    txtReminder.Text = Convert.ToString(dtYarnbrokerage.Rows[0]["Reminder"]);
                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["ReminderDate"]) != "")
                    {
                        txtReminderDate.Text = Convert.ToDateTime(dtYarnbrokerage.Rows[0]["ReminderDate"].ToString()).ToString("dd MMM yyyy");
                    }
                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["FillingDate"]) != "")
                    {
                        txtDateOfFilling.Text = Convert.ToDateTime(dtYarnbrokerage.Rows[0]["FillingDate"].ToString()).ToString("dd MMM yyyy");
                    }

                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["TakeOverDate"]) != "")
                    {
                        txtTakeOverDate.Text = Convert.ToDateTime(dtYarnbrokerage.Rows[0]["TakeOverDate"].ToString()).ToString("dd MMM yyyy");
                    }

                    txtPeriod.Text = Convert.ToString(dtYarnbrokerage.Rows[0]["PeriodOfuse"]);


                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["WorkLabel"]) == "Label")
                    {

                        rdbLabel.Checked = true;

                    }
                    else
                    {
                        rdbWord.Checked = true;

                    }


                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["Tm11FilledOn"]) != "")
                    {
                        txtTm11Filled.Text = Convert.ToDateTime(dtYarnbrokerage.Rows[0]["Tm11FilledOn"].ToString()).ToString("dd MMM yyyy");
                    }
                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["Tm12FilledOn"]) != "")
                    {
                        txtTm12Filled.Text = Convert.ToDateTime(dtYarnbrokerage.Rows[0]["Tm12FilledOn"].ToString()).ToString("dd MMM yyyy");
                    }

                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["CertificateReceiveddate"]) != "")
                    {
                        txtCertificateRecDate.Text = Convert.ToDateTime(dtYarnbrokerage.Rows[0]["CertificateReceiveddate"].ToString()).ToString("dd MMM yyyy");
                    }
                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["IsShowCauseReceived"]).ToLower() == "true")
                    {
                        ddlShowCase.SelectedValue = "Yes";
                    }
                    else
                    {
                        ddlShowCase.SelectedValue = "No";
                    }

                    if (Convert.ToString(dtYarnbrokerage.Rows[0]["TmImage"].ToString()) != "")
                    {
                        imagediv.Style.Add("display", " ");
                        imgTradeMark.Src = "~/images/TMImages/" + dtYarnbrokerage.Rows[0]["TmImage"].ToString();
                        thumb1.HRef = "~/images/TMImages/" + dtYarnbrokerage.Rows[0]["TmImage"].ToString();
                    }
                    else
                    {
                        imgTradeMark.Style.Add("display", "none");
                    }

                    ObjTmcountry.TrademarkId = Convert.ToInt32(hdnBrokerageId.Value);
                    DataSet ds = new DataSet();
                    ds = ObjTmcountry.getTradeMarkDetails();
                    chkClass.DataSource = ds.Tables[0];

                    chkClass.DataTextField = "Class";
                    chkClass.DataValueField = "ClassId";

                    chkClass.DataBind();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (ListItem lt in chkClass.Items)
                        {

                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                if (Convert.ToString(lt.Value) == ds.Tables[0].Rows[i]["ClassId"].ToString())
                                {
                                    lt.Selected = true;
                                    break;
                                }
                                else
                                {
                                    lt.Selected = false;
                                }

                            }

                        }
                    }
                    else
                    {
                        foreach (ListItem lt in chkClass.Items)
                        {
                            lt.Selected = false;

                        }
                    }


                    fillInvoiceDetails(Convert.ToInt32(dtYarnbrokerage.Rows[0]["Id"]));
                    fillInvoiceDetails2(Convert.ToInt32(dtYarnbrokerage.Rows[0]["Id"]));
                    if (ddlOpposition.SelectedValue == "Yes")
                    {
                        txtNoteRecDate.Enabled = false;
                        txtDemandNoteDeposit.Enabled = false;
                        ddlNoteSendToClient.Enabled = false;
                        ddlCertificateIssued.Enabled = false;

                    }
                    else
                    {
                        txtNoteRecDate.Enabled = false;
                        txtDemandNoteDeposit.Enabled = false;
                        ddlNoteSendToClient.Enabled = false;
                        ddlCertificateIssued.Enabled = false;


                    }

                    FilTmForm(Convert.ToInt32(dtYarnbrokerage.Rows[0]["Id"]));
                    FillTmHearing(Convert.ToInt32(dtYarnbrokerage.Rows[0]["Id"]));
                    FillJournal(Convert.ToInt32(dtYarnbrokerage.Rows[0]["Id"]));
                    FillRenewal(Convert.ToInt32(dtYarnbrokerage.Rows[0]["Id"]));
                    FillDocument(Convert.ToInt32(dtYarnbrokerage.Rows[0]["Id"]));
                    fillOpposition(Convert.ToInt32(dtYarnbrokerage.Rows[0]["ApplicationNum"]));

                }



            }
            else
            {
            }





        }
    }

    protected void fillOpposition(int ApplicationNum)
    {
        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.ApplicationNum = ApplicationNum;
        DataTable dtInvoice = ObjTmcountry.getOppositionByApplicationNum();
        if (dtInvoice.Rows.Count > 0)
        {
            dgOpposition.DataSource = dtInvoice;
            dgOpposition.DataBind();
            trOpposition1.Style.Add("display", "");
            trOpposition2.Style.Add("display", "");
        }
        else
        {
            trOpposition1.Style.Add("display", "none");
            trOpposition2.Style.Add("display", "none");

        }
    }

    protected void fillInvoiceDetails(int pid)
    {
        Session["s"] = null;

        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.TrademarkId = pid;
        DataTable dtInvoice = ObjTmcountry.getTrademarkConflict();

        if (dtInvoice.Rows.Count > 0)
        {
            for (int i = 0; i < dtInvoice.Rows.Count; i++)
            {
                filltable();
                DataRow row;
                row = dttbl.NewRow();
                row["Id"] = i + 1;

                row["ConflictingTrademark"] = Convert.ToString(dtInvoice.Rows[i]["ConflictingTrademark"]);
                row["TmNo"] = Convert.ToInt32(dtInvoice.Rows[i]["TmNo"]);
                row["Status"] = Convert.ToString(dtInvoice.Rows[i]["Status"]);
                row["TmFileStatusId"] = Convert.ToInt32(dtInvoice.Rows[i]["TmFileStatusId"]);

                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
                Session.Add("s", dttbl);
            }
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            dgGallery.Visible = true;

        }
        else
        {
            dgGallery.Visible = false;
        }

    }
    protected void fillInvoiceDetails2(int pid)
    {
        Session["s2"] = null;

        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.TrademarkId = pid;
        DataTable dtInvoice = ObjTmcountry.getTradeMarkShowCauseDetails();

        if (dtInvoice.Rows.Count > 0)
        {
            for (int i = 0; i < dtInvoice.Rows.Count; i++)
            {
                filltable2();
                DataRow row;
                row = dttbl2.NewRow();
                row["Id"] = i + 1;

                row["ShowCauseRecDate"] = Convert.ToDateTime(dtInvoice.Rows[i]["ShowCauseRecDate"]);
                if (Convert.ToString(dtInvoice.Rows[i]["ShowCauseReplyDate"]) != "")
                {
                    row["ShowCauseReplyDate"] = Convert.ToDateTime(dtInvoice.Rows[i]["ShowCauseReplyDate"]);
                }

                dttbl2.Rows.Add(row);
                dttbl2.AcceptChanges();
                Session.Add("s2", dttbl2);
            }
            dgGallery2.DataSource = dttbl2;
            dgGallery2.DataBind();
            dgGallery2.Visible = true;

        }
        else
        {
            dgGallery2.Visible = false;
        }

    }
    protected void FilTmForm(int pid)
    {
        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.TmFormId = 0;
        DataSet ds = ObjTmcountry.getTmFormMain();
        DataTable dt = new DataTable();
        dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            DataView dv = new DataView(dt);
            dv.RowFilter = "TmTradeMarkId=" + pid;
            dt = dv.ToTable();

            if (dt.Rows.Count > 0)
            {

                ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                ObjTmcountry.TmFormId = Convert.ToInt32(dt.Rows[0]["TmFormId"]);
                DataSet ds1 = ObjTmcountry.getTmFormDetails();
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    dgTmForm.DataSource = ds1.Tables[0];
                    dgTmForm.DataBind();

                }

            }
            else
            {
                Form1.Style.Add("display", "none");
                Form2.Style.Add("display", "none");

            }
        }
        else
        {
            Form1.Style.Add("display", "none");
            Form2.Style.Add("display", "none");

        }
    }

    protected void FillTmHearing(int pid)
    {
        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.TmHearingId = 0;
        DataSet dsHearing = ObjTmcountry.getTmHearing();

        DataTable dt1 = new DataTable();
        dt1 = dsHearing.Tables[0];
        if (dt1.Rows.Count > 0)
        {
            DataView dv = new DataView(dt1);
            dv.RowFilter = "TmTradeMarkId=" + pid;
            dt1 = dv.ToTable();

            if (dt1.Rows.Count > 0)
            {

                ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                ObjTmcountry.TmHearingId = Convert.ToInt32(dt1.Rows[0]["TmHearingId"]);
                DataSet ds1 = ObjTmcountry.getTmHearingDetails();
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    dgHearing.DataSource = ds1.Tables[0];
                    dgHearing.DataBind();

                }

            }
            else
            {
                Hearing1.Style.Add("display", "none");
                Hearing2.Style.Add("display", "none");

            }
        }
        else
        {
            Hearing1.Style.Add("display", "none");
            Hearing2.Style.Add("display", "none");

        }

    }


    protected void FillJournal(int pid)
    {
        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.TmJournalId = 0;
        DataSet dsJournal = ObjTmcountry.getTmJournal();

        DataTable dt2 = new DataTable();
        dt2 = dsJournal.Tables[0];
        if (dt2.Rows.Count > 0)
        {
            DataView dv = new DataView(dt2);
            dv.RowFilter = "TmTradeMarkId=" + pid;
            dt2 = dv.ToTable();

            if (dt2.Rows.Count > 0)
            {

                ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                ObjTmcountry.TmJournalId = Convert.ToInt32(dt2.Rows[0]["TmJournalId"]);
                DataSet ds1 = ObjTmcountry.getTmJournalDetails();
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    dgJournal.DataSource = ds1.Tables[0];
                    dgJournal.DataBind();
                }
                else
                {
                    Jurnal1.Style.Add("display", "none");
                    Jurnal2.Style.Add("display", "none");
                }

            }
            else
            {
                Jurnal1.Style.Add("display", "none");
                Jurnal2.Style.Add("display", "none");

            }
        }
    }

    protected void FillRenewal(int pid)
    {

        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.TmRenewalId = 0;
        DataSet dsRenewal = ObjTmcountry.getTmRenewal();

        DataTable dt3 = new DataTable();
        dt3 = dsRenewal.Tables[0];
        if (dt3.Rows.Count > 0)
        {
            DataView dv = new DataView(dt3);
            dv.RowFilter = "TmTradeMarkId=" + pid;
            dt3 = dv.ToTable();

            if (dt3.Rows.Count > 0)
            {

                ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                ObjTmcountry.TmRenewalId = Convert.ToInt32(dt3.Rows[0]["TmRenewalId"]);
                DataSet ds1 = ObjTmcountry.getTmRenewalDetails();
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    dgRenewal.DataSource = ds1.Tables[0];
                    dgRenewal.DataBind();

                }

            }
            else
            {


                Renewal1.Style.Add("display", "none");
                Renewal1.Style.Add("display", "none");

            }
        }
        else
        {
            Renewal1.Style.Add("display", "none");
            Renewal1.Style.Add("display", "none");

        }
    }

    protected void FillDocument(int pid)
    {
        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.TmDocumentId = 0;
        DataSet dsRenewal = ObjTmcountry.getTmDocument();
        DataTable dt3 = new DataTable();
        dt3 = dsRenewal.Tables[0];
        if (dt3.Rows.Count > 0)
        {
            DataView dv = new DataView(dt3);
            dv.RowFilter = "TmTradeMarkId=" + pid;
            dt3 = dv.ToTable();
            if (dt3.Rows.Count > 0)
            {
                ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                ObjTmcountry.TmDocumentId = Convert.ToInt32(dt3.Rows[0]["TmDocumentId"]);
                DataSet ds1 = ObjTmcountry.getTmDocumentDetails();


                DataTable dt = new DataTable();
                DataView dv1 = new DataView(ds1.Tables[0]);
                dv1.RowFilter = "IsShowtoClient<>'false'";
                dt = dv1.ToTable();


                if (dt.Rows.Count > 0)
                {
                    dgdocument.DataSource = dt;
                    dgdocument.DataBind();
                    dgdocument.Visible = true;
                }

                else
                {
                    Document1.Style.Add("display", "none");
                    Document2.Style.Add("display", "none");
                }
            }
            else
            {


                Document1.Style.Add("display", "none");
                Document2.Style.Add("display", "none");

            }
        }
        else
        {


            Document1.Style.Add("display", "none");
            Document2.Style.Add("display", "none");

        }

    }


    #endregion





    protected void grdCustomer_RowCommand(object sender, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            Response.Redirect("SingleTMSearchClient.aspx?Id=" + e.CommandArgument.ToString());
        }

    }
    protected void ddgHearing_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Label)e.Item.FindControl("lblHearingDate")).Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "HearingDate").ToString());

        }



    }
    protected void dgTmForm_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Label)e.Item.FindControl("lblFillingDate")).Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "FillingDate").ToString());

        }
    }
    protected void dgJournal_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Label)e.Item.FindControl("lblJournalDate")).Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "JournalDate").ToString());
            ((Label)e.Item.FindControl("lblPurchaseDate")).Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "PurchaseDate").ToString());

        }



    }
    protected void dgRenewal_RowDataBound(object sender, DataGridItemEventArgs e)
    {


        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Label)e.Item.FindControl("lblRenewalDate")).Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "RenewalDate").ToString());
        }

    }
    protected void dgOpposition_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Label)e.Item.FindControl("lblJournalDate")).Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "JournalDate").ToString());
        }
    }

    protected void dgdocument_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string ImageName = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Image"));
            if (ImageName.ToLower().Contains(".jpg") || ImageName.ToLower().Contains(".jpeg") || ImageName.ToLower().Contains(".png") || ImageName.ToLower().Contains(".gif"))
            {

            }
            else
            {
                HtmlGenericControl d1 = ((HtmlGenericControl)e.Item.FindControl("d1"));
                d1.Style.Add("display", "none");

                //d1.Attributes.Remove("class");
                d1.InnerText = "";

                HtmlGenericControl imageDocument = ((HtmlGenericControl)e.Item.FindControl("imageDocument"));
                imageDocument.Style.Add("display", "");
            }




        }



    }

    protected void lblbutton_Click(object sender, EventArgs e)
    {
        Response.Redirect("SingleTMSearchClient.aspx?addnew=1");
    }


    protected void BindClass()
    {
        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.ClassId = 0;
        DataSet ds = new DataSet();
        ds = ObjTmcountry.GetTMClass();

        DataTable dt = new DataTable();
        dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            chkClass.DataSource = dt;
            chkClass.DataTextField = "Class";
            chkClass.DataValueField = "ClassId";
            chkClass.DataBind();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.Id = 0;
        DataSet ds = ObjTmcountry.GetTmTradeMark();
        DataTable dt = new DataTable();

        dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {

            DataView dv = new DataView(dt);
            dv.RowFilter = "CustomerName = '" + Convert.ToString(Sessions.UseLoginName) + "'";

            dt = dv.ToTable();
            dv.Table = dt;
            if (txtSearch.Text != "")
            {
                if (ddlSearchBy.SelectedValue.ToString() == "Application#")
                {
                    dv.RowFilter = "CONVERT( ApplicationNum , System.String) LIKE '%" + txtSearch.Text + "%'";

                }

                if (ddlSearchBy.SelectedValue.ToString() == "TradeMark")
                {

                    dv.RowFilter = "TradeMark LIKE '%" + txtSearch.Text + "%'";
                }

            }

            dt = dv.ToTable();

            dgagentSearch.DataSource = dt;
            dgagentSearch.DataBind();
        }

    }




    #region Add New Fuctionality

    public void filltable()
    {
        if (Session["s"] == null)
        {
            DataColumn column = new DataColumn();
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ConflictingTrademark";
            column.Caption = "ConflictingTrademark";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "TmNo";
            column.Caption = "TmNo";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Status";
            column.Caption = "Status";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "TmFileStatusId";
            column.Caption = "TmFileStatusId";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);




        }
        else
        {
            dttbl = (DataTable)Session["s"];
        }
    }

    protected void dgGallery_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {


        }

    }

    #endregion


    #region Add New2 Fuctionality

    public void filltable2()
    {
        if (Session["s2"] == null)
        {
            DataColumn column = new DataColumn();
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl2.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.DateTime");
            column.ColumnName = "ShowCauseRecDate";
            column.Caption = "ShowCauseRecDate";
            column.ReadOnly = false;
            dttbl2.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.DateTime");
            column.ColumnName = "ShowCauseReplyDate";
            column.Caption = "ShowCauseReplyDate";
            column.ReadOnly = false;
            dttbl2.Columns.Add(column);




        }
        else
        {
            dttbl2 = (DataTable)Session["s2"];
        }
    }

    protected void dgGallery2_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "ShowCauseReplyDate")) != "")
            {
                Label lblShowCauseReplyDate = (Label)e.Item.FindControl("lblShowCauseReplyDate");
                lblShowCauseReplyDate.Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "ShowCauseReplyDate").ToString());
            }

            Label lblShowCauseRecDate = (Label)e.Item.FindControl("lblShowCauseRecDate");
            lblShowCauseRecDate.Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "ShowCauseRecDate").ToString());
        }

    }

    #endregion














    protected void ddlOpposition_SelectedIndexChanged(object sender, EventArgs e)
    {
        SetOpposition();
    }


    protected void SetOpposition()
    {
        if (ddlOpposition.SelectedValue == "Yes")
        {
            txtNoteRecDate.Enabled = false;
            txtDemandNoteDeposit.Enabled = false;
            ddlNoteSendToClient.Enabled = false;
            ddlCertificateIssued.Enabled = false;

            ScriptManager.RegisterStartupScript(this, GetType(), "test", "func('" + txtRemark.ClientID + "');", true);
        }
        else
        {
            txtNoteRecDate.Enabled = true;
            txtDemandNoteDeposit.Enabled = true;
            ddlNoteSendToClient.Enabled = true;
            ddlCertificateIssued.Enabled = true;
            ScriptManager.RegisterStartupScript(this, GetType(), "test", "func('" + txtNoteRecDate.ClientID + "');", true);
        }
    }

}