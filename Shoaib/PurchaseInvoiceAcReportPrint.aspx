﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PurchaseInvoiceAcReportPrint.aspx.cs"
    Inherits="PurchaseInvoiceAcReportPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Tahoma; background: none; font-size: 10px;">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; font-weight: bold;">
            <tr id="trTrialBal" runat="server">
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Purchase
                         Invoice Report<br />
                    </span><span style="font-size: 14px;">From :
                        <asp:Label ID="lblFromDate" runat="server" Style="padding-right: 15px;"></asp:Label>
                        To :
                        <asp:Label ID="lblToDate" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblBuyerName" runat="server"></asp:Label><br />
                    <span style="font-size: 12px; font-weight: normal">
                        <asp:Label ID="lblBuyerAddress" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress2" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress3" runat="server"></asp:Label></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <hr />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <asp:Repeater ID="rptrInvoiceReport" OnItemDataBound="rptrInvoiceReport_OnItemDataBound"
            runat="server">
            <ItemTemplate>
                <table style="width: 800px; font-weight: bold; font-size: 12px;">
                    <tr>
                        <td style="font-weight: bold; width: 15%; text-align: left">
                            Inv # :
                            <asp:Label ID="lblInvNo" runat="server"></asp:Label>
                        </td>
                        <td style="font-weight: bold; width: 20%; text-align: left; margin-left: 50px;">
                            Date :
                            <asp:Label ID="lblInvDate" runat="server"></asp:Label>
                        </td>
                        <td style="font-weight: bold; text-align: left; margin-left: 50px;">
                            <span style="float: left">Party :
                                <asp:Label ID="lblParty" runat="server"></asp:Label></span> <span style="float: right;">
                                    Cost Center :
                                    <asp:Label ID="lblcostCenter" runat="server"></asp:Label>
                                </span>
                        </td>
                    </tr>
                </table>
                <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" FooterStyle-CssClass="gridFooter"
                    HeaderStyle-CssClass="gridheader" ShowFooter="true" AlternatingItemStyle-CssClass="gridAlternateItem"
                    GridLines="none" BorderColor="black" OnItemDataBound="dgGallery_OnItemDataBound"
                    BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem">
                    <Columns>
                        <asp:TemplateColumn HeaderText="S.No.">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="3%" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <%# Container.DataSetIndex + 1%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Item Description">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="45%" />
                            <ItemTemplate>
                                <%# Eval("ItemDesc") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Quantity">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="7%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalQuantity" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Rate">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="8%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Purchase Discount %">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="8%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%#String.Format("{0:C}", Eval("BoutiquePurchaseDiscount")).Replace('$', ' ')%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalPurchaseDiscount" runat="server" style="display:none;"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Gross Amount">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="10%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalGrossAmount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="VAT Amount">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="8%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%#String.Format("{0:C}", Eval("SaleTaxAmount")).Replace('$', ' ')%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalSaleTaxAmount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Amount">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="11%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%#String.Format("{0:C}", Eval("AmountIncludeTaxes")).Replace('$', ' ')%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalAmountIncludeTaxes" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>                      
                    </Columns>
                </asp:DataGrid>
                <table style="width: 800px">
                    <tr>
                        <td style="width: 87%; font-weight: bold; text-align: right">
                            Discount :
                        </td>
                        <td style="width: 15%; font-weight: bold; text-align: right">
                            <asp:Label ID="lblDiscount" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 87%; font-weight: bold; text-align: right">
                            Cartage Charges :
                        </td>
                        <td style="width: 15%; font-weight: bold; text-align: right">
                            <asp:Label ID="lblCartage" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 87%; font-weight: bold; text-align: right">
                            Invoice Amount :
                        </td>
                        <td style="width: 15%; font-weight: bold; text-align: right">
                            <asp:Label ID="lblInvoiceAmount" Style="border-bottom: solid 1px black; border-top: solid 1px black;"
                                runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
            </ItemTemplate>
        </asp:Repeater>
        <table style="width: 800px">
            <tr>
                <td style="width: 45%; font-weight: bold; text-align: right">
                    Grand Total :
                </td>
                <td style="width: 9%; font-weight: bold; text-align: right">
                    <asp:Label ID="GrandlblTotalQuantity" Style="border-bottom: solid 1px black; border-top: solid 1px black;"
                        runat="server"></asp:Label>
                </td>
                <td style="width: 15%; font-weight: bold; text-align: right; display:none">
                    <asp:Label ID="GrandlblTotalPurchaseDiscount" Style="border-bottom: solid 1px black; border-top: solid 1px black;"
                        runat="server"></asp:Label>
                </td>
                <td style="width: 27%; font-weight: bold; text-align: right">
                    <asp:Label ID="GrandlblTotalGrossAmount" Style="border-bottom: solid 1px black; border-top: solid 1px black;"
                        runat="server"></asp:Label>
                </td>
                <td style="width: 8%; font-weight: bold; text-align: right">
                    <asp:Label ID="GrandlblTotalSaleTaxAmount" Style="border-bottom: solid 1px black; border-top: solid 1px black;"
                        runat="server"></asp:Label>
                </td>
                <td style="width: 11%; font-weight: bold; text-align: right">
                    <asp:Label ID="GrandlblTotalAmountIncludeTaxes" Style="border-bottom: solid 1px black; border-top: solid 1px black;"
                        runat="server"></asp:Label>
                </td>
             
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>

                                 <script type="text/javascript">

                                            var currentTime = new Date()
                                            var hours = currentTime.getHours()
                                             var Newhours=0;
                                            var minutes = currentTime.getMinutes()
                                            if (minutes < 10)
                                            {
                                            minutes = "0" + minutes
                                            }                                           
                                            if(hours >12)
                                            {
                                            Newhours= hours-12;
                                            }
                                            else
                                            {
                                              Newhours= hours;
                                            }
                                            document.write(Newhours + ":" + minutes + " ")
                                             if(hours > 11)
                                            {                                           
                                            document.write("PM")
                                            } else 
                                            {
                                            document.write("AM")
                                            }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
