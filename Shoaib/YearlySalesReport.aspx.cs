﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class YearlySalesReport : System.Web.UI.Page
{
    #region properties
    clsCookie ck = new clsCookie();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    Reports objReports = new Reports();   
    #endregion
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        if (!IsPostBack)
        {
            ddlClientAC.Focus();
            FillClientAC();
            FillYear();
            
           

        }
        SetUserRight();
    }
    #endregion
    protected void FillYear()
    {
        ArrayList YearArrayList = new ArrayList();
        for (int i = 0; i < 91; i++)
        {
            int year = 1980 + i;
            YearArrayList.Add(year.ToString());
        }
        ddlYear.DataSource = YearArrayList;
        ddlYear.DataBind();
        ddlYear.Items.Insert(0, (new ListItem("Select Year", "0")));
    }
    #region Fill  Client A/C
    protected void FillClientAC()
    {
       
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtClient = new DataTable();
            dtClient = objSalesInvoice.getAcFileBySundryDebtorOnly();
            if (dtClient.Rows.Count > 0)
            {
                ddlClientAC.DataSource = dtClient;
                ddlClientAC.DataTextField = "Title";
                ddlClientAC.DataValueField = "Id";
                ddlClientAC.DataBind();
                ddlClientAC.Items.Insert(0, new ListItem("Select Client A/c", "0"));

            }

       

    }
    #endregion
    #region btnGenrete
    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        // 

        //// Response.Redirect(url);
        // ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,status=yes' );", true);
        string urllast = "";
        if (rdpQty.Checked == true)
        {
            urllast = "SalesReportPrint.aspx?Client=" + ddlClientAC.SelectedValue + "&year=" + ddlYear.SelectedValue + "&Type=1";
        }
        else
        {
            urllast = "SalesReportAmountPrint.aspx?Client=" + ddlClientAC.SelectedValue + "&year=" + ddlYear.SelectedValue + "&Type=2";
        }
            ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);
        
       
    }
    #endregion
    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }
            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }

            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

                btnGenerate.Enabled = false;

            }

        }
    }
    
}
