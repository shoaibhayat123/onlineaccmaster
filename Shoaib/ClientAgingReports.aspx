﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true" CodeFile="ClientAgingReports.aspx.cs" Inherits="ClientAgingReports" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Aging Receivable Report"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
        width="50%">
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
              Client Account :
            </td>
            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                <asp:DropDownList ID="ddlGroup" runat="server" Style="width: 415px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                    InitialValue="0" ErrorMessage="Please select Client Account " Text="*" ControlToValidate="ddlGroup"
                    ValidationGroup="Ledger"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 180px">
                As On :
            </td>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                <asp:TextBox ID="txtstartDate" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtstartDate'),this);"
                    runat="server" Width="100px"></asp:TextBox>
                <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                    onclick="scwShow(scwID('ctl00_MainHome_txtstartDate'),this);" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                    ErrorMessage="Please Fill Starting  Date." Text="*" ControlToValidate="txtstartDate"
                    ValidationGroup="Ledger"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                  ControlToValidate="txtstartDate" ValidationGroup="Ledger" ErrorMessage="Invalid Invoice Date !!"
                    Display="None"></asp:RegularExpressionValidator>
            </td>
        </tr>
        
        <tr>
            <td colspan="2" style="margin-top: 15px">
                <asp:Button ID="btnGenerate" Text="Generate" ValidationGroup="Ledger" Style="float: left"
                    runat="server" OnClick="btnGenerate_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="Ledger" />
            </td>
        </tr>
    </table>
</asp:Content>

