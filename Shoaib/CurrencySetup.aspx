﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="CurrencySetup.aspx.cs" Inherits="CurrencySetup" Title="" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphead" runat="Server">
    Currency Setup
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <table cellspacing="0" border="0" style=" float:left" cellpadding="5" cellspacing="5">
                <tr>
                    <td width="100%" valign="top" style="left: 5; padding-top: 10px;">
                        <table style="margin-left: 10px;">
                            <tr>
                                <td align="left">
                                    Currency Description :
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtCurrencyDesc" MaxLength="20" runat="server"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Please Fill Currency Description."
                                        Text="*" ControlToValidate="txtCurrencyDesc" ValidationGroup="v"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    Currency Symbol :
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtCurrencySymbol" MaxLength="5" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="req1" runat="server" Display="Dynamic" ErrorMessage="Please Fill Currency Symbol."
                                        Text="*" ControlToValidate="txtCurrencySymbol" ValidationGroup="v"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 8px;">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:Button ID="btn_save" runat="server" Text="Update" OnClick="btn_save_Click" ValidationGroup="v" />
                                    <br />
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                        ShowSummary="False" ValidationGroup="v" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
