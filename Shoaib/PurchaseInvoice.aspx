﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="PurchaseInvoice.aspx.cs" Inherits="PurchaseInvoice" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Purchase Invoice"></asp:Label>
    <script type="text/javascript">

        function Count(text, long) {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }
        function ChangeCartage(obj) {
            if (obj.value != null && !obj.value.toString().match(/^[-]?\d*\.?\d*$/) || obj.value == "") {
                obj.value = "0.00";
                var BillAmount = parseFloat(replaceAll(document.getElementById('<%=txtTotalGrossAmount.ClientID%>').innerHTML, ',', '')) + parseFloat(replaceAll(document.getElementById('<%=txtCartagecharges.ClientID%>').value, ',', '')) - parseFloat(replaceAll(document.getElementById('<%=txtDiscount.ClientID%>').value, ',', ''));
                document.getElementById('<%=lblBillAmount.ClientID%>').value = formatCurrency(BillAmount);
                alert("Please Enter Numeric Value");
            }
            else {
                if (obj.value < 0) {
                    obj.value = "0.00";
                    alert("Please Enter Valid Value");
                }
                var BillAmount = parseFloat(replaceAll(document.getElementById('<%=txtTotalGrossAmount.ClientID%>').innerHTML, ',', '')) + parseFloat(replaceAll(document.getElementById('<%=txtCartagecharges.ClientID%>').value, ',', '')) - parseFloat(replaceAll(document.getElementById('<%=txtDiscount.ClientID%>').value, ',', ''));
                document.getElementById('<%=lblBillAmount.ClientID%>').value = formatCurrency(BillAmount);



            }
        }

        function replaceAll(txt, replace, with_this) {
            return txt.replace(new RegExp(replace, 'g'), with_this);
        }
        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                        num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }

          
    </script>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);

        function BeginRequestHandler(sender, args) {

            if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Save") {

                document.getElementById('<%= ImageButton2.ClientID %>').value = "Saving...";
                args.get_postBackElement().disabled = true;
            }
            if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Update") {

                document.getElementById('<%= btnAddNew.ClientID %>').value = "Updating...";
                args.get_postBackElement().disabled = true;
            }


        }
        function SaveClick() {
            document.getElementById('<%= hdnButtonText.ClientID %>').value = "Save";

        }

        function UpdateClick() {
            document.getElementById('<%= hdnButtonText.ClientID %>').value = "Update";

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
          <asp:HiddenField ID="hdnButtonText" runat="server" />
            <asp:HiddenField ID="hdnInvoiceId" runat="server" />
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                width="100%">
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                        Pur. Inv. No. :
                    </td>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 5px; text-align: left">
                        <table>
                            <tr>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 0px;">
                                    <asp:TextBox ID="txtInvNo" TabIndex="1" CssClass="input_box33" Style="float: left;
                                        text-transform: uppercase;" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="hdnInvoiceSummaryId" runat="server" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill  Pur. Inv. No." Text="*" ControlToValidate="txtInvNo"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <span id="spanPgging" runat="server" style="padding: 0 20px 0 20px; font-weight: bold;
                                        font-size: 18px">
                                        <asp:LinkButton ID="lnkFirst" Text="|<" runat="server" OnClick="lnkFirst_Click"> </asp:LinkButton>
                                        <asp:LinkButton ID="lnkPrevious" Style="margin-left: 10px;" Text="<" runat="server"
                                            OnClick="lnkPrevious_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkNext" Text=">" Style="margin-left: 10px;" runat="server" OnClick="lnkNext_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnLast" Text=">|" Style="margin-left: 10px;" runat="server" OnClick="lnLast_Click"></asp:LinkButton></span>
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    Date :
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    <asp:TextBox ID="txtInvoiceDate" TabIndex="2" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDate'),this);"
                                        runat="server" Width="100px"></asp:TextBox>
                                    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                        onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDate'),this);" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill Invoice Date." Text="*" ControlToValidate="txtInvoiceDate"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                        ControlToValidate="txtInvoiceDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid Invoice Date !!"
                                        Display="None"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    Cr Days :
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    <asp:TextBox ID="txtCrDays" onChange="isNumericPositive(this)" TabIndex="3" MaxLength="25"
                                        runat="server" Width="40px"></asp:TextBox>
                                    <asp:Label ID="Lblcrdate" Style="display: none" runat="server" Width="80px" MaxLength="25"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 111px; padding: 2px 2px 2px 8px;">
                        Cost Center :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                        <asp:DropDownList ID="ddlCostCenter" TabIndex="4" runat="server" Style="width: 550px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select Cost Center" Text="*" ControlToValidate="ddlCostCenter"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Vendor A/c :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        <asp:DropDownList ID="ddlClientAC" TabIndex="5" runat="server" Style="width: 550px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select Vendor A/C " Text="*" ControlToValidate="ddlClientAC"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Purchase A/C :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        <asp:DropDownList ID="ddlSaleAC" TabIndex="6" runat="server" Style="width: 550px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select Purchase A/C " Text="*" ControlToValidate="ddlSaleAC"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="sundryDR" runat="server" style="height: 280px; width: 780px; padding: 10px;
                            overflow: auto; border: solid 2px  #528627">
                            <table>
                                <tr>
                                    <td style="margin-right: 10px; width: 110px;">
                                        Item Description :
                                    </td>
                                    <td colspan="5" style="margin-right: 10px;">
                                        <asp:DropDownList ID="ddlItemDesc" TabIndex="7" runat="server" Style="width: 642px"
                                            OnSelectedIndexChanged="ddlItemDesc_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Display="Dynamic"
                                            InitialValue="0" ErrorMessage="Please select Item Description " Text="*" ControlToValidate="ddlItemDesc"
                                            ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;">
                                        Quantity :
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtQuantity" TabIndex="8" runat="server" onChange="isNumericPositiveValue(this)"
                                            Style="width: 150px;" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill Quantity " Text="*" ControlToValidate="txtQuantity"
                                            ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="margin-right: 10px; width: 80px;">
                                        Rate :
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtRate" runat="server" TabIndex="9" onChange="isNumericDecimal(this)"
                                            Style="width: 150px;" MaxLength="10"></asp:TextBox>
                                        <%--                              <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill Rate " Text="*" ControlToValidate="txtRate" ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td style="margin-right: 10px; width: 80px;">
                                    </td>
                                    <td style="text-align: right">
                                        <asp:Button Style="text-align: right" ID="btnAddNew" TabIndex="10" OnClick="btnAddNew_Click" OnClientClick="UpdateClick()"
                                            runat="server" Text="Update" ValidationGroup="AddInvoiceDetail" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                            ShowSummary="false" ValidationGroup="AddInvoiceDetail" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <asp:HiddenField ID="hdndetail" runat="server" />
                                        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                                            AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                            AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                            BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery_RowDataBound"
                                            OnItemCommand="dgGallery_ItemCommand">
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="S.No.">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataSetIndex + 1%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Item Code">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="8%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Eval("ItemCode")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Item Description">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="50%" />
                                                    <ItemTemplate>
                                                        <%# Eval("ItemDesc") %>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Quantity">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>
                                                        <%--  <%# Convert.ToDecimal(Eval("Quantity")).ToString("0.00")%>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Rate">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                                                        <%--   <%# Convert.ToDecimal(Eval("Rate")).ToString("0.00")%>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Amount">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--     <asp:TemplateColumn HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgDel1" runat="server" ImageUrl="Images/bt-delete.gif" AlternateText="Delete"
                                                            CommandArgument='<%#Eval("Id")%>' CommandName="Delete" OnClientClick="return askDeleteRow();" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>--%>
                                                <asp:TemplateColumn HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="imgDel1" runat="server" Text="Delete" CommandArgument='<%#Eval("Id")%>'
                                                            CommandName="Delete" OnClientClick="return askDeleteRow();" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Edit">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="ImageButton1" runat="server" CommandArgument='<%#Eval("Id")%>'
                                                            Text="Edit" CommandName="Edit" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: left; width: 800px;">
                        <table style="width: 800px;">
                            <tr>
                                <td>
                                    Totals :: &nbsp;&nbsp;&nbsp;&nbsp; <b>Qty:</b><asp:Label ID="txtTotalQuantity" Text="0.00"
                                        runat="server" Style="margin-right: 20px; padding-left: 5px"></asp:Label>
                                    <b>Amount:</b>
                                    <asp:Label ID="txtTotalGrossAmount" runat="server" Text="0.00" Style="margin-right: 20px;
                                        padding-left: 5px"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td style="float: left">
                                    <asp:TextBox ID="txtDescription" TabIndex="11" Width="490px" Style="height: 50px;"
                                        onKeyUp="Count(this,250)" onChange="Count(this,250);capitalizeMe(this)" TextMode="MultiLine"
                                        runat="server"></asp:TextBox>
                                </td>
                                <td align="left" style="">
                                    <table cellpadding="0" cellspacing="0" style="width: 300px; border: solid 0px red;
                                        padding: 5px 0px 0px 0px">
                                        <tr>
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 115px;">
                                                Discount:
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtDiscount" TabIndex="12" onChange="ChangeCartage(this)" Style="text-align: right"
                                                    Text="0.00" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 115px;">
                                                Cartage Charges:
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtCartagecharges" TabIndex="13" onChange="ChangeCartage(this)"
                                                    Style="text-align: right" Text="0.00" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Invoice Amount:
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="lblBillAmount" Enabled="false" Style="text-align: right" Text="0.00"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td style="width: 450px;">
                                    <span>
                                        <%--<asp:Button ID="lblbutton" Style="cursor: pointer" OnClientClick="location.href='PurchaseInvoice.aspx?addnew=1'"
                                            runat="server" Text="Add New Invoice" />--%>
                                        <asp:Button ID="lblbutton" Style="cursor: pointer" runat="server" Text="Add New Invoice"
                                            OnClick="lblbutton_Click" />
                                        <asp:Button Text="Delete Invoice" OnClientClick="return askDeleteInvoice();" ID="btnDeleteInvoice"
                                            runat="server" OnClick="btnDeleteInvoice_Click" />
                                        <%-- <a id="PrintBill"
                                                runat="server" target="_blank">
                                                <asp:Button Text="Print Invoice" ID="btnPrintBill" runat="server" /></a>--%>
                                        <asp:Button Text="Print Invoice" ID="btnPrintBill" runat="server" OnClick="btnPrintBill_Click" />
                                    </span>&nbsp;&nbsp;&nbsp;&nbsp; <span id="spanLastInvoice" runat="server" style="display: none">
                                        <b>Last Inv No:
                                            <asp:Literal ID="litLastInvoiceNo" runat="server"></asp:Literal>&nbsp;&nbsp;<asp:Literal
                                                ID="litLastInvDate" runat="server"></asp:Literal></b></span>
                                </td>
                                <td style="width: 550px">
                                    <table cellpadding="0" cellspacing="0" style="padding: 5px 155px 0px 0px; width: 100%">
                                        <tr>
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 33%;">
                                            </td>
                                            <td align="left" valign="top" style="padding: 2px 0px 2px 5px; text-align: right;
                                                width: 33%;">
                                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                                                    ShowSummary="false" ValidationGroup="AddInvoice" />
                                               <%-- <asp:ImageButton ID="ImageButton2" TabIndex="14" runat="server" ImageUrl="~/Images/btn-save.gif"
                                                    OnClick="btn_saveInvoice_Click" ValidationGroup="AddInvoice" />--%>
                                                    
                                                     <asp:Button ID="ImageButton2" OnClientClick="SaveClick()" TabIndex="14" Text="Save" runat="server"
                                                    ValidationGroup="AddInvoice" OnClick="btn_saveInvoice_Click" />
                                                    
                                                    &nbsp;</div>
                                            </td>
                                            <td style="width: 33%; padding: 2px 2px 20px 3px;">
                                                   <asp:Button ID="ImageButton3" Text="Cancel" runat="server" OnClick="btn_cancel_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
