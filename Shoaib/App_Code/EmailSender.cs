﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Mail;
using System.Net.Mail;
using System.IO;


/// <summary>
/// Summary description for EmailSender
/// </summary>
public class EmailSender
{

    #region Data Members

    public string MailServer;
    public string MailUser;
    public string MailFrom;
    public string MailPassword;
    public string MailTo;
    public string MailBcc;
    public string TemplateDoc;
    public string Message, Subject;
    public string[] ValueArray;
    public string _CC;

    #endregion
    public EmailSender()
    {
        //
        // TODO: Add constructor logic here

        //

    }



    #region Properties

    public string CC
    {
        set { _CC = value; }
    }



    #endregion



    #region SendMail
  
    public void Send()
    {
        MailServer = System.Configuration.ConfigurationManager.AppSettings["MailServer"];
        MailUser = System.Configuration.ConfigurationManager.AppSettings["MailUser"];
        MailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
        MailPassword = System.Configuration.ConfigurationManager.AppSettings["MailPassword"];
        //MailBcc = System.Configuration.ConfigurationManager.AppSettings["BCCMail"];
        ///SendTo = System.Configuration.ConfigurationManager.AppSettings["RootMail"];
      
            SmtpClient smtpClient = new SmtpClient(MailServer);
            System.Net.Mail.MailMessage MlMessage = new System.Net.Mail.MailMessage(MailFrom, MailTo);

            MlMessage.Subject = Subject;
            MlMessage.Bcc.Add(MailBcc);
            MlMessage.IsBodyHtml = true;
            MlMessage.Body = GetHtml(TemplateDoc).ToString();
            MlMessage.Priority = System.Net.Mail.MailPriority.High;

            smtpClient.Credentials = new System.Net.NetworkCredential(MailUser, MailPassword);
            smtpClient.Send(MlMessage);
       
    }
   
    public void SendEmail()
    {
        MailServer = System.Configuration.ConfigurationManager.AppSettings["MailServer"];
        MailUser = System.Configuration.ConfigurationManager.AppSettings["MailUser"];
        MailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
        MailPassword = System.Configuration.ConfigurationManager.AppSettings["MailPassword"];
        //MailBcc = System.Configuration.ConfigurationManager.AppSettings["BCCMail"];
        ///SendTo = System.Configuration.ConfigurationManager.AppSettings["RootMail"];
      
            SmtpClient smtpClient = new SmtpClient(MailServer);
            System.Net.Mail.MailMessage MlMessage = new System.Net.Mail.MailMessage(MailFrom, MailTo);

            MlMessage.Subject = Subject;
          //  MlMessage.Bcc.Add(MailBcc);
            MlMessage.IsBodyHtml = true;
            MlMessage.Body = GetHtml(TemplateDoc).ToString();
            MlMessage.Priority = System.Net.Mail.MailPriority.High;

            smtpClient.Credentials = new System.Net.NetworkCredential(MailUser, MailPassword);
            smtpClient.Send(MlMessage);
       
    }
 
    #endregion


    #region SendMail
  
    public void SendMail()
    {
        MailServer = System.Configuration.ConfigurationManager.AppSettings["MailServer"];
        MailUser = System.Configuration.ConfigurationManager.AppSettings["MailUser"];
        MailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
        MailPassword = System.Configuration.ConfigurationManager.AppSettings["MailPassword"];
        //MailBcc = System.Configuration.ConfigurationManager.AppSettings["BCCMail"];
        ///SendTo = System.Configuration.ConfigurationManager.AppSettings["RootMail"];
      
            SmtpClient smtpClient = new SmtpClient(MailServer);
            System.Net.Mail.MailMessage MlMessage = new System.Net.Mail.MailMessage(MailFrom, MailTo);

            MlMessage.Subject = Subject;
          //  MlMessage.Bcc.Add(MailBcc);
            MlMessage.IsBodyHtml = true;
            MlMessage.Body = Message;
            MlMessage.Priority = System.Net.Mail.MailPriority.High;

            smtpClient.Credentials = new System.Net.NetworkCredential(MailUser, MailPassword);
            smtpClient.Send(MlMessage);
       
    }
   

 
    #endregion

    #region  READ TEMPLATE
    //¦ ROUTINE TO READ TEMPLATE AND RETURN THE MAIL BODY 
    public string GetHtml(string argTemplateDocument)
    {
        int i = 0;
        System.IO.StreamReader filePtr;
        string fileData;
        string path = HttpContext.Current.Server.MapPath("~/EmailTemplates");
        filePtr = File.OpenText(path + "\\" + argTemplateDocument.ToString());
      //  filePtr = File.OpenText(ConfigurationSettings.AppSettings["EMLPath"].ToString() + argTemplateDocument.ToString());
        fileData = filePtr.ReadToEnd();//.ReadToEnd;
     
        if (ValueArray == null)
        {
            return fileData;
        }
        else
        {
            for (i = 0; i < ValueArray.Length; i++)
            {
                fileData = fileData.Replace("@v" + i + "@", ValueArray[i].ToString());
            }
            return fileData;
        }

        filePtr.Close();
        filePtr = null;
    }
    #endregion

}
