﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;


/// <summary>
/// Summary description for Car
/// </summary>
public class Car
{
    #region sqlnew
    SqlConnection con;
    SqlCommand cm;
    SqlDataAdapter da;
    DataTable dt;
    String conStr = ConfigurationManager.ConnectionStrings["ConnectionAccMasterMaster"].ToString();
    #endregion
    #region Data Members

    public int Than,CostingMainId, DailyProductionCapacity, DailyStitches, ConsumptionRatio, MachineSpeed, ProductionHours, DownTime, YarnRate, FabricRate, FabricWidth, OverHeads, OtherInput1, OtherInput2, OtherInput3, OtherInput4, OtherInput5,EmbSalesMainId, DcId, InvoiceNo, EmbRateMainId, ClientId, Thanid, DeliveryChallanId, ClientAcfile, DcNo, SubCategoryCode, ThanId, MendingId, Types, ProductionId, Pcs, QualityId, ClientAcfileId, ThanNum, JobCardNo, MachineNo, Quantity, Orderid, PartyAcfileId, OrderNo, Stitches, StyleType, Repeat, DesignId, LocalDeliveryChalanId, BuyerAcfileId, BrokerAcfileId, SalesPersonacfileId, LocationId, NumberOfContainer, OceanFreight, ContainerBookingId, CarDeliveredVechileId, CarPurchaseCurrencyId1, TransportExpenseAcfileId, WarehouseId, DeliveryId, TransportCompanyAcfileId, VinId, CarTransportOrderId, CarPurchaseCurrencyId2, CarPurchaseCurrencyId3, CarPurchaseCurrencyId4, CarPurchaseCurrencyId5, PurchaseDetailId, OptionId, CarOptionId, SellerAcfileId, CurrencyId5, PurchaseThroughAcfileId, PurchaseLocationId, CountryId, CarTitleId, SaleType, PaymentDays, BrokerExpAcfileId, SalesPersonCreditAcfileId, SalesPersonDebitAcfileId, SalesCurrencyId, BrokerageCurrencyId, SalesAcfileId, InvNo, CarSaleId, CarExpenseOnContainerID, Id, ContainerNoId, DebitAcFileId, CreditAcFileId, ArticleNameId, ArticalTypeId, AccountDrAcfileId, LocalCustomDetailId, AccountCRAcfileId, CustomExpenseId, LocalCustomMainId, NoOfCar, CarLoadingDetailId, LoaderAcfileId, LoadingExpAcfileId, CarLodingId, VendorAcfileId1, ToeingAcfileId1, VendorAcfileId2, ToeingAcfileId2, CarToeingId, CarImageMainid, CarImageId, VendorAcfileId, RepairAcfileId, CarRepairCurrencyId, CarRepairId, VINId, PurchaseAcfileId, CurrencyId1, CurrencyId2, CurrencyId3, CurrencyId4, CarPurchaseId, PartnerAcFileID, CurrencyId, Make, Model, Color, PurchaseId, PurchaseLocation, NoOfUser, ProductId, CarModelId, id, CarLocationId, CarColorId, CarMakeId, CostCenterCode, NoOfMonths, PaymentId;
    public string CustomerName, AddRemark1,AddRemark2,LessRemark2,LessRemark1, OrderForworMachineNo, OrderStatus,LotNumber, Instructions, Units, DesignNo, IdNo, PhoneNo, TruckNo, DeliveryPersonName, Remark, TruckDriverName, SenderName, AgentName, BookingNo, CarPurchaseNotes, PortOfLoading, ShippingCompany, Note, ReciverName, TrackingNo, Agent, TransportOrderNo, Rpresantative, Phone, CarOption, RefNumber, CarPurchaseOtherExp2, CarPurchaseOtherExp3, CarPurchaseOtherExp4, WareHouseSender, ReceivedFromSenderName, MailedSenderName, TrackingNoMailedTo, TrackingNoRecivedFrom, CarColor, DeliveredTo, ExportPaparNo, BranchName, SalesRemark, Descriptiopn, ArticleName, ArticalType, ReceiptNo, DateFrom, DateTo, BillOfEntryNo, ShippingLine, BLNO, CustomExpense, CarLodingInvoiceNO, PortOfLoding, ContainerNo, CarImage, Description, CurrencyCode, Description1, OtherExp2, Description2, OtherExp3, OtherExp4, Notes, CarLot, VechileType, CarUsedStatus, ProductName, VIN, Year, CarLocation, CarMakeName, CarModel, Name, Address, City, Country, Email, ContactNo, TransactionDesc, Product, OrderId;
    public decimal QuotedPrice, ExpectedDailyBilling,StitchRate, CostYard, TotalCostPc, OtherInput, OverHead, Fabric, Yarn, FabricConsumption, YarnConsumption, CalculatedLength, StRate, PieceRate, InvoiceAmount, AddAmount1, AddAmount2, LessAmount1, LessAmount2, EmbLength,  YardRate, PcRate, Yard, Grossweight, NetWeight, Initialweight, Width, Rate, Size, OpStock, Cost5, TransportCharge, ExchangeRate5, TotalCost5, Amount, CostOnOneCar, Charges, SumTotalCost, CostSum, CarRepairCost, CarRepairExchangeRate, CaraRepairTotalCost, TotalCostSum, ExchangeRate, Cost1, ExchangeRate1, TotalCost1, Cost2, ExchangeRate2, TotalCost2, Cost3, ExchangeRate3, TotalCost3, Cost4, ExchangeRate4, TotalCost4, CustomerCode, CarPurchaseCost1, CarPurchaseCost4, CarPurchaseExchangeRate4, CarPurchaseTotalCost4, CarPurchaseCostSum, CarPurchaseTotalCostSum, CarPurchaseCost5, CarPurchaseExchangeRate5, CarPurchaseTotalCost5, CarPurchaseExchangeRate1, CarPurchaseTotalCost1, CarPurchaseCost2, CarPurchaseExchangeRate2, CarPurchaseTotalCost2, CarPurchaseCost3, CarPurchaseExchangeRate3, CarPurchaseTotalCost3, Price, CustomizeAmount, OtherAmount, TotalAmount, BasicPackagPrice;
    public Nullable<DateTime>InvDateFrom,InvDateTo, CostingDate, DcDateTo, DcDateFrom, SalesDateFrom, SalesDateTo, DateOfInvoice, DateOfRateFrom, DateOfRateTo, DateOfRate, DcDate, MendingDateFrom, MendingDateTo, MendingDate, ProductionDateFrom, ProductionDateTo, OrderForwordDateFrom, OrderForwordDateTo, ProductionDate, OrderForwordDate, OrderDate, ActualArrivalDate, BookingDate, CutOffDate, WarehouseDate, TitleSentDate, DeliveredDate, TitleReceivingDate, TitleDeliveredDate, ExpectedDeliveryDate, InventoryDate, TransportDate, SellerDate, MailedToWarehouseDate, RecivedAtWarehouseDate, VoucherDate, ExpPaperDate, ClaimReceivedDate, DeliveryDate, InvDate, BeDate, LoadingDate, ETD, ETA, ContainerReceived, CarToingDate1, CarToingDate2, PurchaseDate, CarRepairDate, AccountOpeningDate, AccountExpiryDate, AccountFromDate, AccountToDate;
    public Boolean TruckKey,IsPickupTitle, IsActiveted, DeliveredKey, DeliveredTitle, TransportKey, TransportNavigation, TransportDVD, IsExportPaperReceived, IsClaimReceived, IsDelivered, IsReceived, IsArrived, IsImage, IsInvetory, Approved;
    public DateTime SalesDate,TransDate, CarExpenseDate;
    public decimal BrokerageCost, SalesPersonAmount, SumCost, BrokerageTotalCost, BrokerageExchangeRate, SalesExchangeRate, SalesTotalCost, SalesCost;
    int retVal = 0;   
  
    #endregion   


    public Car()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataSet GetCarColor()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarColor";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CarColorId", CarColorId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }
    public DataSet GetCarOption()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarOption";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CarOptionId", CarOptionId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }
    public DataSet getCarInventoryDetails()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "getCarInventoryDetails";

        SqlParameter[] param = new SqlParameter[1];

        param[0] = new SqlParameter("@PurchaseId", PurchaseId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public void DeleteCarInventoryDetails()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarInventoryDetails";

        SqlParameter[] param = new SqlParameter[1];

        param[0] = new SqlParameter("@PurchaseId", PurchaseId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



    }
    public DataSet GetCarSaleByVinId()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarSaleByVinId";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@VinId", VinId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public DataSet GetCarSale()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarSale";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CarSaleId", CarSaleId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public DataSet GetCarLocationSetup()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarLocationSetup";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CarLocationId", CarLocationId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public DataSet GetCarMake()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarMake";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CarMakeId", CarMakeId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public DataSet GetCarModel()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarModel";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CarModelId", CarModelId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public DataSet GetCarModelByMake()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarModelByMake";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CarModelId", CarModelId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public int DeleteCarColor()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarColor";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CarColorId", CarColorId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }
    public int DeleteCarOption()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarOption";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CarOptionId", CarOptionId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }
    public int DeleteCarSales()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarSales";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CarSaleId", CarSaleId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }
    public int DeleteCarLocation()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarLocation";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CarLocationId", CarLocationId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }
    public int DeleteCarMake()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarMake";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CarMakeId", CarMakeId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        return result;

    }
    public int DeleteCarModel()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarModel";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CarModelId", CarModelId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }


    public int AddEditCarSale()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditCarSale";
        SqlParameter[] param = new SqlParameter[32];
        param[0] = new SqlParameter("@CarSaleId", CarSaleId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@VinId", VinId);
        param[3] = new SqlParameter("@InvDate", InvDate);
        param[4] = new SqlParameter("@InvNo", InvNo);
        param[5] = new SqlParameter("@SalesCurrencyId", SalesCurrencyId);



        param[6] = new SqlParameter("@SalesCost", SalesCost);
        param[7] = new SqlParameter("@SalesExchangeRate", SalesExchangeRate);
        param[8] = new SqlParameter("@SalesTotalCost", SalesTotalCost);
        param[9] = new SqlParameter("@BuyerAcfileId", BuyerAcfileId);
        param[10] = new SqlParameter("@SalesAcfileId", SalesAcfileId);
        param[11] = new SqlParameter("@SalesRemark", SalesRemark);


        param[12] = new SqlParameter("@BrokerageCurrencyId", BrokerageCurrencyId);
        param[13] = new SqlParameter("@BrokerageCost", BrokerageCost);
        param[14] = new SqlParameter("@BrokerageExchangeRate", BrokerageExchangeRate);
        param[15] = new SqlParameter("@BrokerageTotalCost", BrokerageTotalCost);
        param[16] = new SqlParameter("@BrokerAcfileId", BrokerAcfileId);
        param[17] = new SqlParameter("@BrokerExpAcfileId", BrokerExpAcfileId);

        param[18] = new SqlParameter("@BranchName", BranchName);
        param[19] = new SqlParameter("@IsDelivered", IsDelivered);
        param[20] = new SqlParameter("@DeliveryDate", DeliveryDate);

        param[21] = new SqlParameter("@IsExportPaperReceived", IsExportPaperReceived);
        param[22] = new SqlParameter("@IsClaimReceived", IsClaimReceived);
        param[23] = new SqlParameter("@ExpPaperDate", ExpPaperDate);


        param[24] = new SqlParameter("@ClaimReceivedDate", ClaimReceivedDate);
        param[25] = new SqlParameter("@ExportPaparNo", ExportPaparNo);
        param[26] = new SqlParameter("@SalesPersonCreditAcfileId", SalesPersonCreditAcfileId);
        param[27] = new SqlParameter("@SalesPersonDebitAcfileId", SalesPersonDebitAcfileId);
        param[28] = new SqlParameter("@SalesPersonAmount", SalesPersonAmount);
        param[29] = new SqlParameter("@PaymentDays", PaymentDays);

        param[30] = new SqlParameter("@SaleType", SaleType);
        param[31] = new SqlParameter("@DeliveredTo", DeliveredTo);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarSaleId"]);
        return retVal;

    }



    public int AddEditCarColor()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditCarColor";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CarColorId", CarColorId);
        param[1] = new SqlParameter("@CarColor", CarColor);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarColorId"]);


        return retVal;
    }


    public int AddEditCarOption()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditCarOption";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CarOptionId", CarOptionId);
        param[1] = new SqlParameter("@CarOption", CarOption);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarOptionId"]);

        return retVal;
    }



    public int AddEditCarTitle()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditCarTitle";
        SqlParameter[] param = new SqlParameter[12];

        param[0] = new SqlParameter("@CarTitleId", CarTitleId);
        param[1] = new SqlParameter("@SellerDate", SellerDate);
        param[2] = new SqlParameter("@MailedToWarehouseDate", MailedToWarehouseDate);
        param[3] = new SqlParameter("@RecivedAtWarehouseDate", RecivedAtWarehouseDate);
        param[4] = new SqlParameter("@TrackingNoMailedTo", TrackingNoMailedTo);
        param[5] = new SqlParameter("@TrackingNoRecivedFrom", TrackingNoRecivedFrom);
        param[6] = new SqlParameter("@MailedSenderName", MailedSenderName);
        param[7] = new SqlParameter("@ReceivedFromSenderName", ReceivedFromSenderName);
        param[8] = new SqlParameter("@PurchaseLocationId", PurchaseLocationId);
        param[9] = new SqlParameter("@CustomerCode", CustomerCode);
        param[10] = new SqlParameter("@VinId", VinId);
        param[11] = new SqlParameter("@WareHouseSender", WareHouseSender);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarTitleId"]);


        return retVal;
    }

    public int DeleteCarTitle()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarTitle";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CarTitleId", CarTitleId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        return result;
    }












    public int AddEditCarLocation()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditCarLocation";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@CarLocationId", CarLocationId);
        param[1] = new SqlParameter("@CarLocation", CarLocation);
        param[2] = new SqlParameter("@Address", Address);
        param[3] = new SqlParameter("@Phone", Phone);
        param[4] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarLocationId"]);


        return retVal;
    }



    public int AddEditCarMake()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditCarMake";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CarMakeId", CarMakeId);
        param[1] = new SqlParameter("@CarMakeName", CarMakeName);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarMakeId"]);


        return retVal;
    }


    public int AddEditCarModel()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditCarModel";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@CarModelId", CarModelId);
        param[1] = new SqlParameter("@CarModel", CarModel);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@CarMakeId", CarMakeId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarModelId"]);


        return retVal;
    }




    public int AddEditPaymentDetails()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        //string sqlCommand = "AddEditPaymentDetails";
        //SqlParameter[] param = new SqlParameter[18];





        //ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "AddEditPaymentDetails";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@PaymentId", PaymentId);
        cm.Parameters.AddWithValue("@CustomerCode", CustomerCode);
        cm.Parameters.AddWithValue("@Name", Name);

        cm.Parameters.AddWithValue("@Address", Address);

        cm.Parameters.AddWithValue("@City", City);
        cm.Parameters.AddWithValue("@Country", Country);
        cm.Parameters.AddWithValue("@Email", Email);

        cm.Parameters.AddWithValue("@ContactNo", ContactNo);
        cm.Parameters.AddWithValue("@TransactionDesc", TransactionDesc);


        cm.Parameters.AddWithValue("@CustomizeAmount", CustomizeAmount);



        cm.Parameters.AddWithValue("@OtherAmount", OtherAmount);

        cm.Parameters.AddWithValue("@BasicPackagPrice", BasicPackagPrice);
        cm.Parameters.AddWithValue("@NoOfMonths", NoOfMonths);

        cm.Parameters.AddWithValue("@TotalAmount", TotalAmount);
        cm.Parameters.AddWithValue("@Approved", Approved);
        cm.Parameters.AddWithValue("@OrderId", OrderId);

        cm.Parameters.AddWithValue("@TransDate", TransDate);
        cm.Parameters.AddWithValue("@Product", Product);





        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();

        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["PaymentId"]);

        return retVal;
    }

    public void getPaymentDetails()
    {

        DataSet ds = new DataSet();
        //string sqlCommand = "proc_getPaymentDetails";
        //SqlParameter[] param = new SqlParameter[1];
        //param[0] = new SqlParameter("@OrderId", OrderId);
        //ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "proc_getPaymentDetails";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;

        cm.Parameters.AddWithValue("@OrderId", OrderId);


        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();

    }


    public DataTable GetProdutPrice()
    {

        DataSet ds = new DataSet();

        //string sqlCommand = "GetProdutPrice";


        //SqlParameter[] param = new SqlParameter[1];
        //param[0] = new SqlParameter("@ProductId", ProductId);

        //ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "GetProdutPrice";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;

        cm.Parameters.AddWithValue("@ProductId", ProductId);


        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();


        return ds.Tables[0];

    }

    public void AddEditProdutPrice()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "Proc_AddEditProdutPrice";

        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@ProductId", ProductId);
        param[1] = new SqlParameter("@ProductName", ProductName);
        param[2] = new SqlParameter("@Price", Price);
        param[3] = new SqlParameter("@NoOfUser", NoOfUser);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);




    }


    public DataTable getAccAddress()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "getAccAddress";


        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@id", id);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



        return ds.Tables[0];

    }


    public void setaddress()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "proc_setaddress";


        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Address", Address);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);






    }




    public DataTable GetCarPurchase()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "GetCarPurchase";


        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@PurchaseId", PurchaseId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);




        return ds.Tables[0];

    }

    public DataTable GetCarLoadingVin()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "GetCarLoadingVin";


        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@PurchaseId", PurchaseId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);




        return ds.Tables[0];

    }

    public DataTable GetCarPurchaseSearch()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "GetCarPurchaseSearch";


        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@PurchaseId", PurchaseId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);




        return ds.Tables[0];

    }
    public DataTable GetCarLocalDeliveryChalanSearch()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "GetCarLocalDeliveryChalanSearch";


        SqlParameter[] param = new SqlParameter[2];
     
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);




        return ds.Tables[0];

    }
    public int AddEditCarPurchase()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditCarPurchase";


        SqlParameter[] param = new SqlParameter[46];
        param[0] = new SqlParameter("@PurchaseId", PurchaseId);
        param[1] = new SqlParameter("@CarLot", CarLot);
        param[2] = new SqlParameter("@CarUsedStatus", CarUsedStatus);
        param[3] = new SqlParameter("@VIN", VIN);


        param[4] = new SqlParameter("@CurrencyId1", CurrencyId1);
        param[5] = new SqlParameter("@Cost1", Cost1);
        param[6] = new SqlParameter("@ExchangeRate1", ExchangeRate1);
        param[7] = new SqlParameter("@TotalCost1", TotalCost1);

        param[8] = new SqlParameter("@CurrencyId2", CurrencyId2);
        param[9] = new SqlParameter("@Cost2", Cost2);
        param[10] = new SqlParameter("@ExchangeRate2", ExchangeRate2);
        param[11] = new SqlParameter("@TotalCost2", TotalCost2);


        param[12] = new SqlParameter("@CurrencyId3", CurrencyId3);
        param[13] = new SqlParameter("@Cost3", Cost3);
        param[14] = new SqlParameter("@ExchangeRate3", ExchangeRate3);
        param[15] = new SqlParameter("@TotalCost3", TotalCost3);



        param[16] = new SqlParameter("@CurrencyId4", CurrencyId4);
        param[17] = new SqlParameter("@Cost4", Cost4);
        param[18] = new SqlParameter("@ExchangeRate4", ExchangeRate4);
        param[19] = new SqlParameter("@TotalCost4", TotalCost4);


        param[20] = new SqlParameter("@VendorAcfileId", VendorAcfileId);
        param[21] = new SqlParameter("@PurchaseAcfileId", PurchaseAcfileId);
        param[22] = new SqlParameter("@CostSum", CostSum);
        param[23] = new SqlParameter("@TotalCostSum", TotalCostSum);



        param[24] = new SqlParameter("@Year", Year);
        param[25] = new SqlParameter("@Make", Make);
        param[26] = new SqlParameter("@Model", Model);
        param[27] = new SqlParameter("@Color", Color);

        param[28] = new SqlParameter("@VechileType", VechileType);
        param[29] = new SqlParameter("@PurchaseLocation", PurchaseLocation);
        param[30] = new SqlParameter("@PartnerAcFileID", PartnerAcFileID);
        param[31] = new SqlParameter("@Notes", Notes);
        param[32] = new SqlParameter("@CustomerCode", CustomerCode);
        param[33] = new SqlParameter("@InventoryDate", InventoryDate);


        param[34] = new SqlParameter("@RefNumber", RefNumber);
        param[35] = new SqlParameter("@PurchaseThroughAcfileId", PurchaseThroughAcfileId);
        param[36] = new SqlParameter("@BrokerAcfileId", BrokerAcfileId);

        param[37] = new SqlParameter("@CurrencyId5", CurrencyId5);
        param[38] = new SqlParameter("@Cost5", Cost5);
        param[39] = new SqlParameter("@ExchangeRate5", ExchangeRate5);
        param[40] = new SqlParameter("@TotalCost5", TotalCost5);

        param[41] = new SqlParameter("@BrokerExpAcfileId", BrokerExpAcfileId);


        param[42] = new SqlParameter("@SellerAcfileId", SellerAcfileId);
        param[43] = new SqlParameter("@OtherExp2", OtherExp2);

        param[44] = new SqlParameter("@OtherExp3", OtherExp3);
        param[45] = new SqlParameter("@OtherExp4", OtherExp4);



        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["PurchaseId"]);


        return retVal;


    }

    public int DeleteCarPurchase()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarPurchase";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@PurchaseId", PurchaseId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }

    public int AddEditAddInventoryDetails()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditAddInventoryDetails";



        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@PurchaseDetailId", PurchaseDetailId);
        param[1] = new SqlParameter("@OptionId", OptionId);
        param[2] = new SqlParameter("@PurchaseId", PurchaseId);
        param[3] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["PurchaseDetailId"]);



        return retVal;


    }

    public int AddEditCurrencyCode()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCurrencyCode";



        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@CurrencyId", CurrencyId);
        param[1] = new SqlParameter("@CurrencyCode", CurrencyCode);
        param[2] = new SqlParameter("@ExchangeRate", ExchangeRate);
        param[3] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CurrencyId"]);



        return retVal;


    }

    public DataSet GetCarCurrency()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarCurrency";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CurrencyId", CurrencyId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }



    public int DeleteCurrencyCode()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCurrencyCode";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CurrencyId", CurrencyId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }



    public int AddEditCarPurchaseDetails()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarPurchaseDetails";




        SqlParameter[] param = new SqlParameter[30];
        param[0] = new SqlParameter("@CarPurchaseId", CarPurchaseId);
        param[1] = new SqlParameter("@VinId", VinId);
        param[2] = new SqlParameter("@PurchaseDate", PurchaseDate);
        param[3] = new SqlParameter("@CarPurchaseTotalCost5", CarPurchaseTotalCost5);
        param[4] = new SqlParameter("@CarPurchaseCurrencyId1", CarPurchaseCurrencyId1);
        param[5] = new SqlParameter("@CarPurchaseCost1", CarPurchaseCost1);
        param[6] = new SqlParameter("@CarPurchaseExchangeRate1", CarPurchaseExchangeRate1);
        param[7] = new SqlParameter("@CarPurchaseTotalCost1", CarPurchaseTotalCost1);

        param[8] = new SqlParameter("@CarPurchaseCurrencyId2", CarPurchaseCurrencyId2);
        param[9] = new SqlParameter("@CarPurchaseCost2", CarPurchaseCost2);
        param[10] = new SqlParameter("@CarPurchaseExchangeRate2", CarPurchaseExchangeRate2);
        param[11] = new SqlParameter("@CarPurchaseTotalCost2", CarPurchaseTotalCost2);


        param[12] = new SqlParameter("@CarPurchaseCurrencyId3", CarPurchaseCurrencyId3);
        param[13] = new SqlParameter("@CarPurchaseCost3", CarPurchaseCost3);
        param[14] = new SqlParameter("@CarPurchaseExchangeRate3", CarPurchaseExchangeRate3);
        param[15] = new SqlParameter("@CarPurchaseTotalCost3", CarPurchaseTotalCost3);



        param[16] = new SqlParameter("@CarPurchaseCurrencyId4", CarPurchaseCurrencyId4);
        param[17] = new SqlParameter("@CarPurchaseCost4", CarPurchaseCost4);
        param[18] = new SqlParameter("@CarPurchaseExchangeRate4", CarPurchaseExchangeRate4);
        param[19] = new SqlParameter("@CarPurchaseTotalCost4", CarPurchaseTotalCost4);


        param[20] = new SqlParameter("@CarPurchaseCurrencyId5", CarPurchaseCurrencyId5);
        param[21] = new SqlParameter("@CarPurchaseCost5", CarPurchaseCost5);

        param[22] = new SqlParameter("@CarPurchaseCostSum", CarPurchaseCostSum);
        param[23] = new SqlParameter("@CarPurchaseTotalCostSum", CarPurchaseTotalCostSum);
        param[24] = new SqlParameter("@CustomerCode", CustomerCode);

        param[25] = new SqlParameter("@CarPurchaseOtherExp2", CarPurchaseOtherExp2);
        param[26] = new SqlParameter("@CarPurchaseOtherExp3", CarPurchaseOtherExp3);
        param[27] = new SqlParameter("@CarPurchaseOtherExp4", CarPurchaseOtherExp4);

        param[28] = new SqlParameter("@CarPurchaseNotes", CarPurchaseNotes);
        param[29] = new SqlParameter("@CarPurchaseExchangeRate5", CarPurchaseExchangeRate5);




        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarPurchaseId"]);



        return retVal;


    }

    public DataSet GetCarPurchaseDetails()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarPurchaseDetails";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CarPurchaseId", CarPurchaseId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }



    public int DeleteCarPurchaseDetails()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarPurchaseDetails";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CarPurchaseId", CarPurchaseId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }


    public DataSet GetCarTitle()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarTitle";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CarTitleId", CarTitleId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public DataTable getInventoryDetails()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "getInventoryDetails";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@PurchaseId", PurchaseId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }

    public DataTable getInventoryDetailsCarWarehouse()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "getInventoryDetailsCarWarehouse";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@PurchaseId", PurchaseId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }


    public DataSet GetCarExpenseOnVinNo()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarExpenseOnVinNo";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public DataSet GetCarExpenseOnContainer()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarExpenseOnContainer";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }
    public DataSet GetCarRepair()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarRepair";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CarRepairId", CarRepairId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }
    public int DeleteCarRepair()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarRepair";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CarRepairId", CarRepairId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }


    public int DeleteCarExpenseOnContainer()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarExpenseOnContainer";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }

    public int DeleteCarExpenseOnVinNO()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarExpenseOnVinNO";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }

    public int AddEditCarRepair()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarRepair";

        SqlParameter[] param = new SqlParameter[12];
        param[0] = new SqlParameter("@CarRepairId", CarRepairId);
        param[1] = new SqlParameter("@VinId", VINId);
        param[2] = new SqlParameter("@CarRepairDate", CarRepairDate);

        param[4] = new SqlParameter("@CarRepairCurrencyId", CarRepairCurrencyId);
        param[5] = new SqlParameter("@CarRepairCost", CarRepairCost);
        param[6] = new SqlParameter("@CarRepairExchangeRate", CarRepairExchangeRate);
        param[7] = new SqlParameter("@CaraRepairTotalCost", CaraRepairTotalCost);

        param[8] = new SqlParameter("@VendorAcfileId", VendorAcfileId);
        param[9] = new SqlParameter("@RepairAcfileId", RepairAcfileId);
        param[10] = new SqlParameter("@Description", Description);
        param[11] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarRepairId"]);
        return retVal;


    }

    public int AddEditCarExpenseOnContainer()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarExpenseOnContainer";

        SqlParameter[] param = new SqlParameter[12];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@CarExpenseDate", CarExpenseDate);
        param[2] = new SqlParameter("@ContainerNoId", ContainerNoId);

        param[4] = new SqlParameter("@DebitAcFileId", DebitAcFileId);
        param[5] = new SqlParameter("@CreditAcFileId", CreditAcFileId);
        param[6] = new SqlParameter("@Amount", Amount);
        param[7] = new SqlParameter("@CostOnOneCar", CostOnOneCar);

        param[8] = new SqlParameter("@Description", Description);
        param[9] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
        return retVal;


    }
    public int AddEditCarExpenseOnVinNo()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarExpenseOnVinNo";

        SqlParameter[] param = new SqlParameter[12];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@CarExpenseDate", CarExpenseDate);
        param[2] = new SqlParameter("@VinId", VinId);

        param[4] = new SqlParameter("@DebitAcFileId", DebitAcFileId);
        param[5] = new SqlParameter("@CreditAcFileId", CreditAcFileId);
        param[6] = new SqlParameter("@Amount", Amount);

        param[7] = new SqlParameter("@Description", Description);
        param[8] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
        return retVal;


    }


    public DataSet getCarImages()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "proc_getCarImages";
        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CarImageMainid", CarImageMainid);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public DataSet getCarImagesDetail()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "proc_getCarImagesDetail";
        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@CarImageMainid", CarImageMainid);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }



    public int DeleteCarImage()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarImage";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CarImageMainid", CarImageMainid);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }





    public int AddEditCarImageDetail()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarImageDetail";

        SqlParameter[] param = new SqlParameter[6];

        param[0] = new SqlParameter("@CarImageId", CarImageId);
        param[1] = new SqlParameter("@VinId", VinId);
        param[2] = new SqlParameter("@CarImage", CarImage);
        param[3] = new SqlParameter("@IsImage", IsImage);
        param[4] = new SqlParameter("@CustomerCode", CustomerCode);
        param[5] = new SqlParameter("@CarImageMainid", CarImageMainid);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarImageId"]);
        return retVal;


    }

    public int AddEditCarImageMain()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditCarImageMain";

        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@CarImageMainid", CarImageMainid);
        param[1] = new SqlParameter("@VinId", VinId);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarImageMainid"]);
        return retVal;


    }






    public DataSet GetCarToeing()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarToeing";
        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@CarToeingId", CarToeingId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }



    public int DeleteCarToeing()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarToeing";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CarToeingId", CarToeingId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }



    public int AddEditCarToeing()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarToeing";

        SqlParameter[] param = new SqlParameter[22];

        param[0] = new SqlParameter("@CarToeingId", CarToeingId);
        param[1] = new SqlParameter("@VinId", VinId);
        param[2] = new SqlParameter("@CarToingDate1", CarToingDate1);

        param[4] = new SqlParameter("@CurrencyId1", CurrencyId1);
        param[5] = new SqlParameter("@Cost1", Cost1);
        param[6] = new SqlParameter("@ExchangeRate1", ExchangeRate1);
        param[7] = new SqlParameter("@TotalCost1", TotalCost1);
        param[8] = new SqlParameter("@VendorAcfileId1", VendorAcfileId1);
        param[9] = new SqlParameter("@ToeingAcfileId1", ToeingAcfileId1);
        param[10] = new SqlParameter("@Description1", Description1);


        param[11] = new SqlParameter("@CurrencyId2", CurrencyId2);
        param[12] = new SqlParameter("@Cost2", Cost2);
        param[13] = new SqlParameter("@ExchangeRate2", ExchangeRate2);
        param[14] = new SqlParameter("@TotalCost2", TotalCost2);
        param[15] = new SqlParameter("@VendorAcfileId2", VendorAcfileId2);
        param[16] = new SqlParameter("@ToeingAcfileId2", ToeingAcfileId2);
        param[17] = new SqlParameter("@Description2", Description2);

        param[18] = new SqlParameter("@SumCost", SumCost);
        param[19] = new SqlParameter("@SumTotalCost", SumTotalCost);
        param[20] = new SqlParameter("@CustomerCode", CustomerCode);

        param[21] = new SqlParameter("@CarToingDate2", CarToingDate2);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarToeingId"]);
        return retVal;


    }

    public DataTable GetCarLoadingMain()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarLoadingMain";
        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@CarLodingId", CarLodingId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        DataTable dt = new DataTable();
        dt = ds.Tables[0];
        return dt;
    }

    public DataTable GetCarLoadingDetails()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarLoadingDetails";
        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@CarLodingId", CarLodingId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        DataTable dt = new DataTable();
        dt = ds.Tables[0];
        return dt;
    }



    public int AddEditCarLoadingMain()
    {

        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarLoadingMain";


        SqlParameter[] param = new SqlParameter[40];
        param[0] = new SqlParameter("@CarLodingId", CarLodingId);
        param[1] = new SqlParameter("@ContainerNo", ContainerNo);
        param[2] = new SqlParameter("@LoadingDate", LoadingDate);

        param[3] = new SqlParameter("@CustomerCode", CustomerCode);

        param[4] = new SqlParameter("@CurrencyId1", CurrencyId1);
        param[5] = new SqlParameter("@Cost1", Cost1);
        param[6] = new SqlParameter("@ExchangeRate1", ExchangeRate1);
        param[7] = new SqlParameter("@TotalCost1", TotalCost1);

        param[8] = new SqlParameter("@CurrencyId2", CurrencyId2);
        param[9] = new SqlParameter("@Cost2", Cost2);
        param[10] = new SqlParameter("@ExchangeRate2", ExchangeRate2);
        param[11] = new SqlParameter("@TotalCost2", TotalCost2);


        param[12] = new SqlParameter("@CurrencyId3", CurrencyId3);
        param[13] = new SqlParameter("@Cost3", Cost3);
        param[14] = new SqlParameter("@ExchangeRate3", ExchangeRate3);
        param[15] = new SqlParameter("@TotalCost3", TotalCost3);



        param[16] = new SqlParameter("@CurrencyId4", CurrencyId4);
        param[17] = new SqlParameter("@Cost4", Cost4);
        param[18] = new SqlParameter("@ExchangeRate4", ExchangeRate4);
        param[19] = new SqlParameter("@TotalCost4", TotalCost4);

        param[20] = new SqlParameter("@OtherExp2", OtherExp2);
        param[21] = new SqlParameter("@OtherExp3", OtherExp3);
        param[22] = new SqlParameter("@OtherExp4", OtherExp4);
        param[23] = new SqlParameter("@Description", Description);

        param[24] = new SqlParameter("@SumCost", SumCost);
        param[25] = new SqlParameter("@SumTotalCost", SumTotalCost);
        param[26] = new SqlParameter("@NoOfCar", NoOfCar);
        param[27] = new SqlParameter("@CarLodingInvoiceNO", CarLodingInvoiceNO);
        param[28] = new SqlParameter("@PortOfLoding", PortOfLoding);


        param[29] = new SqlParameter("@ETD", ETD);
        param[30] = new SqlParameter("@ETA", ETA);
        param[31] = new SqlParameter("@IsArrived", IsArrived);
        param[32] = new SqlParameter("@ContainerReceived", ContainerReceived);

        param[33] = new SqlParameter("@IsReceived", IsReceived);
        param[34] = new SqlParameter("@LoaderAcfileId", LoaderAcfileId);
        param[35] = new SqlParameter("@LoadingExpAcfileId", LoadingExpAcfileId);
        param[36] = new SqlParameter("@ShippingLine", ShippingLine);
        param[37] = new SqlParameter("@BLNO", BLNO);

        param[38] = new SqlParameter("@BookingNo", BookingNo);
        param[39] = new SqlParameter("@ActualArrivalDate", ActualArrivalDate);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarLodingId"]);



        return retVal;


    }


    public int DeleteCarLoadingMain()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarLoadingMain";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CarLodingId", CarLodingId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }

    public int AddEditCarLoadingDetail()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditCarLoadingDetail";

        SqlParameter[] param = new SqlParameter[6];

        param[0] = new SqlParameter("@CarLoadingDetailId", CarLoadingDetailId);
        param[1] = new SqlParameter("@VinId", VinId);
        param[3] = new SqlParameter("@Charges", Charges);
        param[4] = new SqlParameter("@CarLodingId", CarLodingId);
        param[5] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarLoadingDetailId"]);
        return retVal;



    }


    public int getExists()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "proc_getExists";
        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@CarLodingId", CarLodingId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@VinId", VinId);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        return retVal;
    }



    public DataSet GetCustomExpense()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCustomExpense";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CustomExpenseId", CustomExpenseId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public int DeleteCustomExpense()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCustomExpense";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomExpenseId", CustomExpenseId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }

    public int AddEditCustomExpense()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCustomExpense";

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CustomExpenseId", CustomExpenseId);
        param[1] = new SqlParameter("@CustomExpense", CustomExpense);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CustomExpenseId"]);
        return retVal;

    }











    public int AddEditLocalCustomByVinId()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditLocalCustomByVinId";


        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@VinId", VinId);
        param[2] = new SqlParameter("@Amount", Amount);
        param[3] = new SqlParameter("@LocalCustomMainId", LocalCustomMainId);
        param[4] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["id"]);
        return retVal;

    }

    public int AddEditCarExpenseOnContainerByVinId()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarExpenseOnContainerByVinId";


        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@VinId", VinId);
        param[2] = new SqlParameter("@Amount", Amount);
        param[3] = new SqlParameter("@CarExpenseOnContainerID", CarExpenseOnContainerID);
        param[4] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["id"]);
        return retVal;

    }

    public int AddEditLocalCustomMain()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditLocalCustomMain";


        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@LocalCustomMainId", LocalCustomMainId);
        param[1] = new SqlParameter("@VoucherDate", VoucherDate);
        param[2] = new SqlParameter("@ContainerNoId", ContainerNoId);
        param[3] = new SqlParameter("@BillOfEntryNo", BillOfEntryNo);
        param[4] = new SqlParameter("@BeDate", BeDate);
        param[5] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["LocalCustomMainId"]);
        return retVal;

    }
    public int AddEditLocalCustomDetail()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditLocalCustomDetail";


        SqlParameter[] param = new SqlParameter[10];
        param[0] = new SqlParameter("@LocalCustomDetailId", LocalCustomDetailId);
        param[1] = new SqlParameter("@CustomExpenseId", CustomExpenseId);
        param[2] = new SqlParameter("@ReceiptNo", ReceiptNo);
        param[3] = new SqlParameter("@DateFrom", DateFrom);
        param[4] = new SqlParameter("@DateTo", DateTo);

        param[5] = new SqlParameter("@Amount", Amount);
        param[6] = new SqlParameter("@AccountDrAcfileId", AccountDrAcfileId);
        param[7] = new SqlParameter("@AccountCRAcfileId", AccountCRAcfileId);

        param[8] = new SqlParameter("@LocalCustomMainId", LocalCustomMainId);
        param[9] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["LocalCustomDetailId"]);
        return retVal;



    }



    public DataSet GetLocalCustomMain()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetLocalCustomMain";
        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@LocalCustomMainId", LocalCustomMainId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public void DeleteCarExpenseOnContainerByVinId()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "proc_DeleteCarExpenseOnContainerByVinId";
        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@CarExpenseOnContainerID", CarExpenseOnContainerID);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



    }



    public DataSet GetLocalCustomDetail()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetLocalCustomDetail";
        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@LocalCustomMainId", LocalCustomMainId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }



    public DataSet getContainer()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "proc_getContainer";
        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@CarLodingId", LocalCustomMainId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }


    public int DeleteLocalCustomsMain()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteLocalCustomsMain";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@LocalCustomMainId", LocalCustomMainId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }
    public DataTable getAllVinByContainer()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "proc_getAllVinByContainer";
        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@ContainerNoId", ContainerNoId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }




















    public int AddEditCarTransportOrder()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarTransportOrder";



        SqlParameter[] param = new SqlParameter[19];
        param[0] = new SqlParameter("@CarTransportOrderId", CarTransportOrderId);
        param[1] = new SqlParameter("@TransportDate", TransportDate);
        param[2] = new SqlParameter("@TransportOrderNo", TransportOrderNo);
        param[3] = new SqlParameter("@VinId", VinId);
        param[4] = new SqlParameter("@Rpresantative", Rpresantative);

        param[5] = new SqlParameter("@TransportCompanyAcfileId", TransportCompanyAcfileId);
        param[6] = new SqlParameter("@Agent", Agent);
        param[7] = new SqlParameter("@TransportCharge", TransportCharge);

        param[8] = new SqlParameter("@TransportExpenseAcfileId", TransportExpenseAcfileId);
        param[9] = new SqlParameter("@WarehouseId", WarehouseId);


        param[10] = new SqlParameter("@DeliveryId", DeliveryId);
        param[11] = new SqlParameter("@ExpectedDeliveryDate", ExpectedDeliveryDate);
        param[12] = new SqlParameter("@TransportKey", TransportKey);
        param[13] = new SqlParameter("@TransportNavigation", TransportNavigation);

        param[14] = new SqlParameter("@TransportDVD", TransportDVD);
        param[15] = new SqlParameter("@Note", Note);
        param[16] = new SqlParameter("@CustomerCode", CustomerCode);

        param[17] = new SqlParameter("@IsPickupTitle", IsPickupTitle);
        param[18] = new SqlParameter("@LocationId", LocationId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarTransportOrderId"]);



        return retVal;


    }

    public DataTable GetCarTransportOrder()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarTransportOrder";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CarTransportOrderId", CarTransportOrderId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }



    public int DeleteCarTransportOrder()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarTransportOrder";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CarTransportOrderId", CarTransportOrderId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }


    public int AddEditCarTitleReceiving()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarTitleReceiving";



        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@TitleReceivingDate", TitleReceivingDate);
        param[2] = new SqlParameter("@TrackingNo", TrackingNo);
        param[3] = new SqlParameter("@VinId", VinId);
        param[4] = new SqlParameter("@ReciverName", ReciverName);
        param[5] = new SqlParameter("@CustomerCode", CustomerCode);

     

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);



        return retVal;


    }


    public DataTable GetCarTitleRecivedFromSeller()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarTitleRecivedFromSeller";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }



    public int DeleteCarTitleRecivedFromSeller()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarTitleRecivedFromSeller";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }















    public int AddEditCarTitleMailedToLoader()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarTitleMailedToLoader";



        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@TitleSentDate", TitleSentDate);
        param[2] = new SqlParameter("@TrackingNo", TrackingNo);
        param[3] = new SqlParameter("@VinId", VinId);
        param[4] = new SqlParameter("@SenderName", SenderName);
        param[5] = new SqlParameter("@CustomerCode", CustomerCode);

     

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);



        return retVal;


    }


    public DataTable GetCarTitleMailedTolader()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarTitleMailedTolader";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }



    public int DeleteCarTitleMailedTolader()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarTitleMailedTolader";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }





































    public int AddEditCarTitleRecivedByWarehouse()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarTitleRecivedByWarehouse";



        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@WarehouseDate", WarehouseDate);
        param[2] = new SqlParameter("@TrackingNo", TrackingNo);
        param[3] = new SqlParameter("@VinId", VinId);
        param[4] = new SqlParameter("@ReciverName", ReciverName);
        param[5] = new SqlParameter("@CustomerCode", CustomerCode);



        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);



        return retVal;


    }


    public DataTable GetCarTitleRecivedByWarehouse()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarTitleRecivedByWarehouse";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }

    public DataTable getContainerBookingByBookinNo()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "proc_getContainerBookingByBookinNo";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@BookingNo", BookingNo);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }

    public int DeleteCarTitleRecivedByWarehouse()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarTitleRecivedByWarehouse";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }

































    public DataTable getVINSerchForCarTransport()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getVINSerchForCarTransport";
        SqlParameter[] param = new SqlParameter[2];
        
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
      
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }




    public int AddEditDeliveredVehicle()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditDeliveredVehicle";


        SqlParameter[] param = new SqlParameter[17];
        param[0] = new SqlParameter("@CarDeliveredVechileId", CarDeliveredVechileId);
        param[1] = new SqlParameter("@VinId", VinId);
        param[2] = new SqlParameter("@DeliveredDate", DeliveredDate);
        param[3] = new SqlParameter("@Agent", Agent);
        param[4] = new SqlParameter("@Phone", Phone);

        param[5] = new SqlParameter("@WarehouseId", WarehouseId);
        param[6] = new SqlParameter("@DeliveredKey", DeliveredKey);
        param[7] = new SqlParameter("@DeliveredTitle", DeliveredTitle);

        param[8] = new SqlParameter("@TitleDeliveredDate", TitleDeliveredDate);
        param[9] = new SqlParameter("@Note", Note);


        param[10] = new SqlParameter("@CustomerCode", CustomerCode);
      


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarDeliveredVechileId"]);



        return retVal;


    }

    public DataTable GetCarDeliveredVehicle()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarDeliveredVehicle";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CarDeliveredVechileId", CarDeliveredVechileId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }



    public int DeleteCarDeliveredVehicle()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarDeliveredVehicle";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CarDeliveredVechileId", CarDeliveredVechileId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }















    public int AddEditCarcontainerBooking()
    {
        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarcontainerBooking";
        SqlParameter[] param = new SqlParameter[12];        


        param[0] = new SqlParameter("@ContainerBookingId", ContainerBookingId);
        param[1] = new SqlParameter("@BookingDate", BookingDate);
        param[2] = new SqlParameter("@BookingNo", BookingNo);
        param[3] = new SqlParameter("@PortOfLoading", PortOfLoading);
        param[4] = new SqlParameter("@ShippingCompany", ShippingCompany);
        param[5] = new SqlParameter("@AgentName", AgentName);
        param[6] = new SqlParameter("@ETA", ETA);
        param[7] = new SqlParameter("@ETD", ETD);
        param[8] = new SqlParameter("@CutOffDate", CutOffDate);
        param[9] = new SqlParameter("@NumberOfContainer", NumberOfContainer);
        param[10] = new SqlParameter("@OceanFreight", OceanFreight);
        param[11] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["ContainerBookingId"]);
        return retVal;
    }
    public DataTable GetCarcontainerBooking()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetCarcontainerBooking";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ContainerBookingId", ContainerBookingId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }



    public int DeleteCarcontainerBooking()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarcontainerBooking";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@ContainerBookingId", ContainerBookingId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }






    public int AddEditCarLocalDeliveryChalan()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarLocalDeliveryChalan";
    


        SqlParameter[] param = new SqlParameter[13];
        param[0] = new SqlParameter("@LocalDeliveryChalanId", LocalDeliveryChalanId);
        param[1] = new SqlParameter("@BuyerAcfileId", BuyerAcfileId);
        param[2] = new SqlParameter("@BrokerAcfileId", BrokerAcfileId);
        param[3] = new SqlParameter("@SalesPersonacfileId", SalesPersonacfileId);
        param[4] = new SqlParameter("@TruckDriverName", TruckDriverName);
        param[5] = new SqlParameter("@IdNo", IdNo);
        param[6] = new SqlParameter("@PhoneNo", PhoneNo);
        param[7] = new SqlParameter("@TruckNo", TruckNo);
        param[8] = new SqlParameter("@DeliveryPersonName", DeliveryPersonName);
        param[9] = new SqlParameter("@TruckKey", TruckKey);
        param[10] = new SqlParameter("@Remark", Remark);
        param[11] = new SqlParameter("@CustomerCode", CustomerCode);
        param[12] = new SqlParameter("@VinId", VinId);



        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["LocalDeliveryChalanId"]);



        return retVal;


    }

    public DataSet GetCarLocalDeliveryChalan()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCarLocalDeliveryChalan";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@LocalDeliveryChalanId", LocalDeliveryChalanId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }



    public int DeleteCarLocalDeliveryChalan()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarLocalDeliveryChalan";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@LocalDeliveryChalanId", LocalDeliveryChalanId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }



    public int AddEditDesignNo()
    {
        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditDesignNo";
        SqlParameter[] param = new SqlParameter[8];

        param[0] = new SqlParameter("@DesignId", DesignId);
        param[1] = new SqlParameter("@DesignNo", DesignNo);
        param[2] = new SqlParameter("@Type", StyleType);
        param[3] = new SqlParameter("@Stitches", Stitches);
        param[4] = new SqlParameter("@Size", Size);
        param[5] = new SqlParameter("@OpStock", OpStock);
        param[6] = new SqlParameter("@CustomerCode", CustomerCode);
        param[7] = new SqlParameter("@Repeat", Repeat);
        

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["DesignId"]);
        return retVal;
    }
    public DataSet GetDesign()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetDesign";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@DesignId", DesignId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet GetDesignByType()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetDesignByType";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@Type", Types);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    
    public int DeleteDesign()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteDesign";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@DesignId", DesignId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        return result;
    }









    public int AddEditOrderNo()
    {
        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditOrderNo";
        SqlParameter[] param = new SqlParameter[12];
        
        param[0] = new SqlParameter("@Orderid", Orderid);
        param[1] = new SqlParameter("@OrderNo", OrderNo);
        param[2] = new SqlParameter("@OrderDate", OrderDate);
        param[3] = new SqlParameter("@PartyAcfileId", PartyAcfileId);
        param[4] = new SqlParameter("@DesignId", DesignId);
        param[5] = new SqlParameter("@Rate", Rate);
        param[6] = new SqlParameter("@Quantity", Quantity);
        param[7] = new SqlParameter("@Unit", Units);

        param[8] = new SqlParameter("@LotNumber", LotNumber);
        param[9] = new SqlParameter("@Instructions", Instructions);
        param[10] = new SqlParameter("@CustomerCode", CustomerCode);
        param[11] = new SqlParameter("@OrderStatus", OrderStatus);
        

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Orderid"]);
        return retVal;
    }
    public DataSet GetEmbOrder()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetEmbOrder";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@Orderid", Orderid);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public int DeleteEmbOrder()
    {

        int result = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "DeleteEmbOrder";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Orderid", Orderid);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        return result;

    }


















    public int AddEditEmbSalesOrderForward()
    {
        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditEmbSalesOrderForward";
        SqlParameter[] param = new SqlParameter[6];

        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@Orderid", Orderid);
        param[2] = new SqlParameter("@JobCardNo", JobCardNo);
        param[3] = new SqlParameter("@OrderForwordDate", OrderForwordDate);
        param[4] = new SqlParameter("@MachineNo", OrderForworMachineNo);
        param[5] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
        return retVal;
    }
    public DataSet GetmbSalesOrderForward()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetmbSalesOrderForward";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public int DeleteEmbSalesOrderForward()
    {
        int result = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "DeleteEmbSalesOrderForward";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        return result;

    }

    public DataSet SearchSalesOrderForward()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "SearchSalesOrderForward";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet SearchSalesOrderForwardReport()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "SearchSalesOrderForwardReport";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@OrderForwordDateFrom", OrderForwordDateFrom);
        param[3] = new SqlParameter("@OrderForwordDateTo", OrderForwordDateTo);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet EmbDailyProductionSummaryReport()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbDailyProductionSummaryReport";
        SqlParameter[] param = new SqlParameter[3];     
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@ProductionDateFrom", ProductionDateFrom);
        param[2] = new SqlParameter("@ProductionDateTo", ProductionDateTo);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet EmbSalesInvoiceSummaryReport()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbSalesInvoiceSummaryReport";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@SalesDateFrom", SalesDateFrom);
        param[2] = new SqlParameter("@SalesDateTo", SalesDateTo);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet getSaleInvoiceByMobile()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "Proc_getSaleInvoiceByMobile";
        SqlParameter[] param = new SqlParameter[3];
 
        param[0] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[1] = new SqlParameter("@InvDateTo", InvDateTo);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet EmbSalesInvoiceSummaryReportByClient()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbSalesInvoiceSummaryReportByClient";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@SalesDateFrom", SalesDateFrom);
        param[2] = new SqlParameter("@SalesDateTo", SalesDateTo);
        param[3] = new SqlParameter("@ClientAcfile", ClientAcfile);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet EmbSalesInvoiceDetailClient()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbSalesInvoiceDetailClient";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@SalesDateFrom", SalesDateFrom);
        param[2] = new SqlParameter("@SalesDateTo", SalesDateTo);
        param[3] = new SqlParameter("@ClientAcfile", ClientAcfile);
       
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet EmbDailyProductionDetailReport()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbDailyProductionDetailReport";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@ProductionDateFrom", ProductionDateFrom);
        param[2] = new SqlParameter("@ProductionDateTo", ProductionDateTo);
        param[3] = new SqlParameter("@QualityId", QualityId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet EmbDailyProductionDetailReportByWeight()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbDailyProductionDetailReportByWeight";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@ProductionDateFrom", ProductionDateFrom);
        param[2] = new SqlParameter("@ProductionDateTo", ProductionDateTo);
        param[3] = new SqlParameter("@QualityId", QualityId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet EmbDailyProductionDetailReportUnDeliver()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbDailyProductionDetailReportUnDeliver";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@ProductionDateFrom", ProductionDateFrom);
        param[2] = new SqlParameter("@ProductionDateTo", ProductionDateTo);
        param[3] = new SqlParameter("@QualityId", QualityId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet EmbDailyProductionDesignDetails()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbDailyProductionDesignDetails";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@ProductionDate", ProductionDate);      
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet EmbDailyProductionDesignDetailReport()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbDailyProductionDesignDetailReport";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@ProductionDateFrom", ProductionDateFrom);
        param[2] = new SqlParameter("@ProductionDateTo", ProductionDateTo);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet EmbDailyMendingDetailReport()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbDailyMendingDetailReport";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@MendingDateFrom", MendingDateFrom);
        param[2] = new SqlParameter("@MendingDateTo", MendingDateTo);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet EmbDailyMendingDetailDate()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbDailyMendingDetailDate";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@MendingDateFrom", MendingDateFrom);
        param[2] = new SqlParameter("@MendingDateTo", MendingDateTo);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet EmbDeliveryChallanReport()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbDeliveryChallanReport";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@DcDateFrom", DcDateFrom);
        param[2] = new SqlParameter("@DcDateTo", DcDateTo);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet EmbRateClient()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbRateClient";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@DateOfRateFrom", DateOfRateFrom);
        param[2] = new SqlParameter("@DateOfRateTo", DateOfRateTo);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet EmbRateByClientId()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbRateByClientId";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@DateOfRateFrom", DateOfRateFrom);
        param[2] = new SqlParameter("@DateOfRateTo", DateOfRateTo);
        param[3] = new SqlParameter("@ClientId", ClientId);
        
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet getEmbClientForProduction()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getEmbClientForProduction";
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@ProductionDateFrom", ProductionDateFrom);
        param[1] = new SqlParameter("@ProductionDateTo", ProductionDateTo);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@ClientAcfileId", ClientAcfileId);
        param[4] = new SqlParameter("@MachineNo", MachineNo);
        param[5] = new SqlParameter("@QualityId", QualityId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet getEmbQualityForProduction()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getEmbQualityForProduction";
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@ProductionDateFrom", ProductionDateFrom);
        param[1] = new SqlParameter("@ProductionDateTo", ProductionDateTo);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@ClientAcfileId", ClientAcfileId);
        param[4] = new SqlParameter("@MachineNo", MachineNo);
        param[5] = new SqlParameter("@QualityId", QualityId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet getEmbMachineForProduction()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getEmbMachineForProduction";
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@ProductionDateFrom", ProductionDateFrom);
        param[1] = new SqlParameter("@ProductionDateTo", ProductionDateTo);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@ClientAcfileId", ClientAcfileId);
        param[4] = new SqlParameter("@MachineNo", MachineNo);
        param[5] = new SqlParameter("@QualityId", QualityId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet getEmbProductionValueReport()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getEmbProductionValueReport";
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@ProductionDateFrom", ProductionDateFrom);
        param[1] = new SqlParameter("@ProductionDateTo", ProductionDateTo);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@ClientAcfileId", ClientAcfileId);
        param[4] = new SqlParameter("@MachineNo", MachineNo);
        param[5] = new SqlParameter("@QualityId", QualityId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet getEmbProductionValueReportDelivered()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getEmbProductionValueReportDelivered";
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@ProductionDateFrom", ProductionDateFrom);
        param[1] = new SqlParameter("@ProductionDateTo", ProductionDateTo);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@ClientAcfileId", ClientAcfileId);
        param[4] = new SqlParameter("@MachineNo", MachineNo);
        param[5] = new SqlParameter("@QualityId", QualityId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet getEmbProductionValueUnDelivered()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getEmbProductionValueUnDelivered";
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@ProductionDateFrom", ProductionDateFrom);
        param[1] = new SqlParameter("@ProductionDateTo", ProductionDateTo);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@ClientAcfileId", ClientAcfileId);
        param[4] = new SqlParameter("@MachineNo", MachineNo);
        param[5] = new SqlParameter("@QualityId", QualityId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet GetEmbRateByDesignId()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetEmbRateByDesignId";
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@ProductionDateFrom", ProductionDateFrom);
        param[1] = new SqlParameter("@ProductionDateTo", ProductionDateTo);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@ClientAcfileId", ClientAcfileId);
        param[4] = new SqlParameter("@DesignId", DesignId);   
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    
    public DataSet GetEmbRateClientDetail()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetEmbRateClientDetail";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@DateOfRateFrom", DateOfRateFrom);
        param[2] = new SqlParameter("@DateOfRateTo", DateOfRateTo);
        param[3] = new SqlParameter("@ClientId", ClientId);
        
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet GetEmbRateClientDetailUniqueRecord()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetEmbRateClientDetailUniqueRecord";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@DateOfRateFrom", DateOfRateFrom);
        param[2] = new SqlParameter("@DateOfRateTo", DateOfRateTo);
        param[3] = new SqlParameter("@ClientId", ClientId);
        param[4] = new SqlParameter("@DesignNo", DesignNo);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet GetEmbRateClientDetailUniqueRecordDesignNo()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetEmbRateClientDetailUniqueRecordDesignNo";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@DateOfRateFrom", DateOfRateFrom);
        param[2] = new SqlParameter("@DateOfRateTo", DateOfRateTo);
        param[3] = new SqlParameter("@ClientId", ClientId);
       

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet getEmbDailyProductionSheetDetailsByDate()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getEmbDailyProductionSheetDetailsByDate";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@ProductionDate", ProductionDate);
   
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }



    public DataSet getEmbDailyProductionSheetStitches()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getEmbDailyProductionSheetStitches";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@ProductionDate", ProductionDate);
        param[2] = new SqlParameter("@MachineNo", MachineNo);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet getEmbDailyProductionSheetStitchesDetails()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getEmbDailyProductionSheetStitchesDetails";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@ProductionDate", ProductionDate);
        param[2] = new SqlParameter("@MachineNo", MachineNo);
        param[3] = new SqlParameter("@ThanId", ThanId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public int AddEditEmbDailyProductionSheet()
    {

  
        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditEmbDailyProductionSheet";
        SqlParameter[] param = new SqlParameter[13];

        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@ThanNum", ThanNum);
        param[2] = new SqlParameter("@ProductionDate", ProductionDate);
        param[3] = new SqlParameter("@MachineNo", MachineNo);
        param[4] = new SqlParameter("@ClientAcfileId", ClientAcfileId);
        param[5] = new SqlParameter("@Width", Width);
        param[6] = new SqlParameter("@Initialweight", Initialweight);
        param[7] = new SqlParameter("@Grossweight", Grossweight);
        param[8] = new SqlParameter("@NetWeight", NetWeight);
        param[9] = new SqlParameter("@Orderid", Orderid);
        param[10] = new SqlParameter("@CustomerCode", CustomerCode);
        param[11] = new SqlParameter("@QualityId", QualityId);
        param[12] = new SqlParameter("@JobCardNo", JobCardNo);
        
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
        return retVal;
    }

    public int AddEditEmbDailyProductionSheetDetails()
    {
          
        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditEmbDailyProductionSheetDetails";
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@DesignId", DesignId);
        param[2] = new SqlParameter("@Pcs", Pcs);
        param[3] = new SqlParameter("@Yard", Yard);
        param[4] = new SqlParameter("@Remark", Remark);
        param[5] = new SqlParameter("@ProductionId", ProductionId);
        param[6] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
        return retVal;
    }

    public DataSet getEmbDailyProductionSheet()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getEmbDailyProductionSheet";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet getEmbDailyProductionSheetDetails()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getEmbDailyProductionSheetDetails";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ProductionId", ProductionId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public int DeleteEmbDailyProductionSheet()
    {
        int result = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "DeleteEmbDailyProductionSheet";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        return result;
    }

    public int AddEditEmbMending()
    {

        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditEmbMending";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@ThanNum", ThanNum);
        param[2] = new SqlParameter("@MendingDate", MendingDate);
        param[3] = new SqlParameter("@CustomerCode", CustomerCode);
    
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
        return retVal;
    }
    public int DeleteEmbMending()
    {
        int result = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "DeleteEmbMending";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        return result;
    }


    public DataSet getEmbMendingByDate()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getEmbMendingByDate";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@MendingDate", MendingDate);      
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet GetEmbMendingDetailByDate()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetEmbMendingDetailByDate";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@MendingDate", MendingDate);      
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    
    public DataSet getEmbMendingByThan()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getEmbMendingByThan";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ThanNum", ThanNum);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet GetMendingDetails()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetMendingDetails";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@MendingId", MendingId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public int AddEditEmbMendingDetails()
    {

        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditEmbMendingDetails";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@ThanNum", ThanNum);
        param[2] = new SqlParameter("@MendingId", MendingId);
        param[3] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
        return retVal;
    }

    
    public DataSet GetEmbMending()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetEmbMending";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@MendingId", Id);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet getEmbDailyProductionSheetByThanNo()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getEmbDailyProductionSheetByThanNo";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ThanNum", ThanNum);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }



    public DataSet EmbDailyMendingDetailTableReport()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbDailyMendingDetailTableReport";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@MendingDate", MendingDate);
        param[1] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet EmbDeliveryChallanDetailTableReport()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbDeliveryChallanDetailTableReport";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@DcNo", DcNo);
     
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }


    public DataSet EmbMendingThanDetails()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "EmbMendingThanDetails";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ThanNum", ThanNum);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);   
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet SearchEmbMendingThan()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "SearchEmbMendingThan";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ThanNum", ThanNum);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }







    public int AddEditEmbDeliveryChallan()
    {

        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditEmbDeliveryChallan";
        SqlParameter[] param = new SqlParameter[6];

        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@DcNo", DcNo);
        param[2] = new SqlParameter("@DcDate", DcDate);
        param[3] = new SqlParameter("@QualityId", QualityId);
        param[4] = new SqlParameter("@ClientAcfile", ClientAcfile);
        param[5] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
        return retVal;
    }
    public int DeleteEmbDeliveryChallan()
    {
        int result = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "DeleteEmbDeliveryChallan";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        return result;
    }

    public int AddEditEmbDeliveryChallanDetails()
    {

        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditEmbDeliveryChallanDetails";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@ThanNum", ThanNum);
        param[2] = new SqlParameter("@DeliveryChallanId", DeliveryChallanId);   
        param[3] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
        return retVal;
    }


    
    public DataSet GetEmbDeliveryChallan()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetEmbDeliveryChallan";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet getSalesInvoiceDcDetils()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getSalesInvoiceDcDetils";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@DeliveryChallanId", DeliveryChallanId);
       
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet getDeliveryChallanDesignNo()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getDeliveryChallanDesignNo";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@DeliveryChallanId", DeliveryChallanId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet getDeliveryChallanThanNo()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getDeliveryChallanThanNo";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@DesignId", DesignId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DeliveryChallanId", DeliveryChallanId);  
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet getDeliveryChallanThanNoDetails()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getDeliveryChallanThanNoDetails";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@DesignId", DesignId);
        param[1] = new SqlParameter("@Thanid", Thanid);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    
    
    public DataSet GetEmbDeliveryChallanDetail()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetEmbDeliveryChallanDetail";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@DeliveryChallanId", DeliveryChallanId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet getEmbDeliveryChallanByDate()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getEmbDeliveryChallanByDate";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@DcDate", DcDate);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet SearchDeliveryChallanDetail()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "SearchDeliveryChallanDetail";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ThanNum", ThanNum);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet SearchDeliveryChallanByDcNo()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "SearchDeliveryChallanByDcNo";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@DcNo", DcNo);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet SearchEmbSaleByDcNo()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "SearchEmbSaleByDcNo";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@DcNo", DcNo);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet SearchDeliveryChallanByThanNum()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "SearchDeliveryChallanByThanNum";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ThanNum", ThanNum);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }



















    public DataSet GetEmbRateDetail()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetEmbRateDetail";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@EmbRateMainId", EmbRateMainId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet GetEmbRateMain()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetEmbRateMain";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public int AddEditEmbRateDetail()
    {

        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditEmbRateDetail";
        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@ClientId", ClientId);
        param[2] = new SqlParameter("@DesignId", DesignId);
        param[3] = new SqlParameter("@StitchRate", StitchRate);
        param[4] = new SqlParameter("@YardRate", YardRate);
        param[5] = new SqlParameter("@PcRate", PcRate);
        param[6] = new SqlParameter("@EmbRateMainId", EmbRateMainId);
        param[7] = new SqlParameter("@CustomerCode", CustomerCode);
        param[8] = new SqlParameter("@EmbLength", EmbLength);
        
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
        return retVal;
    }

   
    public int AddEditEmbRate()
    {

        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditEmbRate";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@DateOfRate", DateOfRate);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
        return retVal;
    }
    public int DeleteEmbRate()
    {
        int result = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "DeleteEmbRate";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        return result;
    }
    public DataSet getEmbRateByDate()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getEmbRateByDate";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@DateOfRate", DateOfRate);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }











    public int AddEditEmbSalesMain()
    {

        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditEmbSalesMain";
        SqlParameter[] param = new SqlParameter[16];

        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@DcId", DcId);
        param[2] = new SqlParameter("@InvoiceNo", InvoiceNo);
        param[3] = new SqlParameter("@SalesDate", SalesDate);
        param[4] = new SqlParameter("@SalesAcfileId", SalesAcfileId);
        param[5] = new SqlParameter("@AddAmount1", AddAmount1);

        param[6] = new SqlParameter("@AddAmount2", AddAmount2);
        param[7] = new SqlParameter("@LessAmount1", LessAmount1);
        param[8] = new SqlParameter("@LessAmount2", LessAmount2);
        param[9] = new SqlParameter("@AddRemark1", AddRemark1);
        param[10] = new SqlParameter("@AddRemark2", AddRemark2);
        param[11] = new SqlParameter("@LessRemark1", LessRemark1);

        param[12] = new SqlParameter("@LessRemark2", LessRemark2);
        param[13] = new SqlParameter("@InvoiceAmount", InvoiceAmount);
        param[14] = new SqlParameter("@CustomerCode", CustomerCode);
        param[15] = new SqlParameter("@Description", Description);
       
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["id"]);
        return retVal;
    }


    public int AddEditSalesDetail()
    {

        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditSalesDetail";
        SqlParameter[] param = new SqlParameter[15];


        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@DesignNo", DesignNo);
        param[2] = new SqlParameter("@Stitches", Stitches);
        param[3] = new SqlParameter("@Pcs", Pcs);
        param[4] = new SqlParameter("@Yard", Yard);
        param[5] = new SqlParameter("@StRate", StRate);

        param[6] = new SqlParameter("@YardRate", YardRate);
        param[7] = new SqlParameter("@PieceRate", PieceRate);
        param[8] = new SqlParameter("@Amount", Amount);
        param[9] = new SqlParameter("@CustomerCode", CustomerCode);
        param[10] = new SqlParameter("@EmbSalesMainId", EmbSalesMainId);
        param[11] = new SqlParameter("@EmbLength", EmbLength);
      


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
        return retVal;
    }



    public DataSet GetEmbSales()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetEmbSales";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataTable getEmbRateDetails()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "Proc_getEmbRateDetails";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@DesignNo", DesignNo);
        param[1] = new SqlParameter("@DateOfInvoice", DateOfInvoice);
        param[2] = new SqlParameter("@ClientAcfile", ClientAcfile);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public DataSet GetEmbSalesDetail()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetEmbSalesDetail";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@EmbSalesMainId", EmbSalesMainId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public int DeleteEmbSales()
    {
        int result = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "DeleteEmbSales";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        return result;
    }




    public int AddEditEmbCostingMain()
    {

        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditEmbCostingMain";
        SqlParameter[] param = new SqlParameter[31];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@CostingDate", CostingDate);
        param[2] = new SqlParameter("@DesignId", DesignId);
        param[3] = new SqlParameter("@ConsumptionRatio", ConsumptionRatio);
        param[4] = new SqlParameter("@CalculatedLength", CalculatedLength);
        param[5] = new SqlParameter("@MachineSpeed", MachineSpeed);
        param[6] = new SqlParameter("@ProductionHours", ProductionHours);
        param[7] = new SqlParameter("@DownTime", DownTime);
        param[8] = new SqlParameter("@YarnRate", YarnRate);
        param[9] = new SqlParameter("@FabricRate", FabricRate);
        param[10] = new SqlParameter("@FabricWidth", FabricWidth);
        param[11] = new SqlParameter("@OverHeads", OverHeads);
        param[12] = new SqlParameter("@OtherInput1", OtherInput1);
        param[13] = new SqlParameter("@OtherInput2", OtherInput2);
        param[14] = new SqlParameter("@OtherInput3", OtherInput3);
        param[15] = new SqlParameter("@OtherInput4", OtherInput4);       
        param[16] = new SqlParameter("@OtherInput5", OtherInput5);
        param[17] = new SqlParameter("@DailyStitches", DailyStitches);
        param[18] = new SqlParameter("@DailyProductionCapacity", DailyProductionCapacity);
        param[19] = new SqlParameter("@Than", Than);
        param[20] = new SqlParameter("@YarnConsumption", YarnConsumption);
        param[21] = new SqlParameter("@FabricConsumption", FabricConsumption);
        param[22] = new SqlParameter("@Yarn", Yarn);
        param[23] = new SqlParameter("@Fabric", Fabric);
        param[24] = new SqlParameter("@OverHead", OverHead);
        param[25] = new SqlParameter("@OtherInput", OtherInput);
        param[26] = new SqlParameter("@TotalCostPc", TotalCostPc);
        param[27] = new SqlParameter("@CostYard", CostYard);
        param[28] = new SqlParameter("@StitchRate", StitchRate);
        param[29] = new SqlParameter("@ExpectedDailyBilling", ExpectedDailyBilling);
        param[30] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["id"]);
        return retVal;
    }

    public int DeleteEmbCosting()
    {
        int result = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "DeleteEmbCosting";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        return result;
    }
    public DataSet GetEmbCostingDetail()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetEmbCostingDetail";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CostingMainId", CostingMainId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet GetCostingMain()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "GetCostingMain";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@Id", Id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public int AddEditEmbCostingDetail()
    {

        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditEmbCostingDetail";
        SqlParameter[] param = new SqlParameter[15];


        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@CostingMainId", CostingMainId);
        param[2] = new SqlParameter("@CostingDate", CostingDate);
        param[3] = new SqlParameter("@QuotedPrice", QuotedPrice);
        param[4] = new SqlParameter("@CustomerName", CustomerName);
        param[5] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
        return retVal;
    }





}