﻿

#region Namespaces...
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


#endregion

#region User Class

/// <summary>
/// Summary description for City
/// </summary>

public class User
{

    #region sqlnew
        SqlConnection con;
        SqlCommand cm;
        SqlDataAdapter da;
        DataTable dt;
        String conStr = ConfigurationManager.ConnectionStrings["ConnectionAccMasterMaster"].ToString();
    #endregion
    clsCookie ck = new clsCookie();
    #region Data Members
    public string TmUsername,TmPassword,SortBy, Remark, UserID, ClientIp, PageName, SalesManName, Address, UserRole, OldPassword, CountryName, Password, SalesTaxNo, Username, PanNo, CountryLang, UserPassword, FirstName, LastName, DateOfBirth, Email, Address1, Address2, City, ContactNo, Zipcode, State, Counrty, Gender, CustomerName, ContactPerson, AddressLine1, AddressLine2, AddressLine3, Country, PhoneNumber, CellNumber, PrimaryEmail, SecondryEmail, SalesTaxNumber, NationalTaxNumber, AccountBlocked, Currency, NewUserID;
    public int  MenuId,ID, CountryId, SalesMan,id, IsActive, IsEdit, ProvinceId, UserId, RoleId, CreatedBy, CustomerId, userid, IndId;
    public Nullable<decimal> CustomerCode,Userid_UserRole, SetupCommision, MonthlyCommision,SetupCharges, MonthlyCharges, NumberOfUsers;
    public Nullable<DateTime> AccountOpeningDate, AccountExpiryDate, FromDate, ToDate;
    public DateTime AccountExpiryDateFrom, AccountExpiryDateTo;
    int retVal = 0;
    public Boolean ChkAll, IsAccess, IsAdd, IsDelete, IsEditRight, IsPrint, IsActiveStatus;
    #endregion
    
		




    public User()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region Login Check
    public DataSet CheckLoginUser()
    {
        DataSet ds = new DataSet();
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@UserName", Username);       
        param[1] = new SqlParameter("@Password", UserPassword);      

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "CheckLogin", param);      
        if (ds.Tables[0].Rows.Count > 0)
        {
            if (ds.Tables[0].Rows[0][0].ToString() != "0")
            {
                Sessions.AdminUserId = int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
                Sessions.AdminUser = ds.Tables[0].Rows[0]["UserName"].ToString();
            }
        }  
        
        return ds;
    }
    #endregion

    #region Get Login
    public DataSet GetLogin()
    {
        int retVal = 0;
        DataSet ds = new DataSet();
       


        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "GetLogin";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;

        cm.Parameters.AddWithValue("@Username", Username);
        cm.Parameters.AddWithValue("@Password", UserPassword);
        cm.Parameters.AddWithValue("@CustomerCode", CustomerCode);        

        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();



            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0][0].ToString() != "0")
                {
                    Sessions.UserId = int.Parse(ds.Tables[0].Rows[0]["Id"].ToString());
                    Sessions.UseLoginName = ds.Tables[0].Rows[0]["UserId"].ToString();
                    Sessions.CustomerCode = ds.Tables[0].Rows[0]["CustomerCode"].ToString();
                    Sessions.CustomerName = ds.Tables[0].Rows[0]["CustomerName"].ToString();
                    Sessions.CityName = ds.Tables[0].Rows[0]["City"].ToString();
                    Sessions.CountryName = ds.Tables[0].Rows[0]["Country"].ToString();
                    Sessions.IndId = ds.Tables[0].Rows[0]["IndId"].ToString();
                    Sessions.UserRole = ds.Tables[0].Rows[0]["UserRole"].ToString();
                    ck.InsertCookie10Hrs("UserId", ds.Tables[0].Rows[0]["Id"].ToString());
                    ck.InsertCookie10Hrs("UseLoginName", ds.Tables[0].Rows[0]["UserId"].ToString());
                    ck.InsertCookie10Hrs("CustomerCode", ds.Tables[0].Rows[0]["CustomerCode"].ToString());
                    ck.InsertCookie10Hrs("CustomerName", ds.Tables[0].Rows[0]["CustomerName"].ToString());
                    ck.InsertCookie10Hrs("CityName", ds.Tables[0].Rows[0]["City"].ToString());
                    ck.InsertCookie10Hrs("CountryName", ds.Tables[0].Rows[0]["Country"].ToString());
                    ck.InsertCookie10Hrs("IndId", ds.Tables[0].Rows[0]["IndId"].ToString());
                    ck.InsertCookie10Hrs("UserRole", ds.Tables[0].Rows[0]["UserRole"].ToString());
                    


                    con = new SqlConnection();
                    con.ConnectionString = conStr;
                    con.Open();
                    cm = new SqlCommand();
                    cm.CommandText = "GetCustomerDetails";
                    cm.CommandType = CommandType.StoredProcedure;
                    cm.Connection = con;
                    cm.Parameters.AddWithValue("@CustomerCode", CustomerCode);
                    da = new SqlDataAdapter();
                    da.SelectCommand = cm;
                    da.Fill(ds);
                    con.Close();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        Sessions.CustomerDetails = ds.Tables[0];
                    }
                               
                }

                getAccountingYear();
                //Sessions.FromDate = ds.Tables[1].Rows[0]["FromDate"].ToString();
                //Sessions.ToDate = ds.Tables[1].Rows[0]["ToDate"].ToString(); 
                //ck.InsertCookie10Hrs("FromDate", ds.Tables[1].Rows[0]["FromDate"].ToString());
                //ck.InsertCookie10Hrs("ToDate", ds.Tables[1].Rows[0]["ToDate"].ToString());



            }

             

        
        return ds;
    }


    public DataSet GetRemainingDays()
    {
        int retVal = 0;
        DataSet ds = new DataSet();



        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "GetRemainingDays";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;

        cm.Parameters.AddWithValue("@Username", Username);
        cm.Parameters.AddWithValue("@Password", UserPassword);
        cm.Parameters.AddWithValue("@CustomerCode", CustomerCode);

        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();
        return ds;


    }


    public void updatecustomerAccountBlocked()
    {
        DataSet ds = new DataSet();


        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "updatecustomerAccountBlocked";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;      
        cm.Parameters.AddWithValue("@CustomerCode", CustomerCode);     
        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();
     
    }

    public void getAccountingYear()
    {
       
        DataSet ds = new DataSet();
        
        SqlParameter[] param = new SqlParameter[1];       
        param[0] = new SqlParameter("@CustomerCode",Convert.ToDecimal(Sessions.CustomerCode));
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "proc_getAccountingYear", param);
        Sessions.FromDate = ds.Tables[0].Rows[0]["FromDate"].ToString();
        Sessions.ToDate = ds.Tables[0].Rows[0]["ToDate"].ToString();
        ck.InsertCookie10Hrs("FromDate", ds.Tables[0].Rows[0]["FromDate"].ToString());
        ck.InsertCookie10Hrs("ToDate", ds.Tables[0].Rows[0]["ToDate"].ToString());

    }
    #endregion

    #region Get Login
    public DataSet GetBoutiqueLogin()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        Sessions.CustomerCode = CustomerCode.ToString();
        

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@Username", Username);
            param[1] = new SqlParameter("@Password", UserPassword);
            param[2] = new SqlParameter("@CustomerCode", CustomerCode);
            ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetBoutiqueLogin", param);
            Sessions.CustomerCode = "";
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0][0].ToString() != "0")
                {
                    Sessions.UserId = int.Parse(ds.Tables[0].Rows[0]["BoutiqueUserId"].ToString());
                    ck.InsertCookie10Hrs("UserId", ds.Tables[0].Rows[0]["BoutiqueUserId"].ToString());

                    Sessions.UseLoginName = ds.Tables[0].Rows[0]["UserName"].ToString();
                    ck.InsertCookie10Hrs("UseLoginName", ds.Tables[0].Rows[0]["UserName"].ToString());

                    Sessions.CustomerCode = ds.Tables[0].Rows[0]["CustomerCode"].ToString();
                    ck.InsertCookie10Hrs("CustomerCode", ds.Tables[0].Rows[0]["CustomerCode"].ToString());

                    Sessions.Brand = Convert.ToString(ds.Tables[0].Rows[0]["BrandId"].ToString());
                    ck.InsertCookie10Hrs("Brand", ds.Tables[0].Rows[0]["BrandId"].ToString());

                    DataSet ds1 = new DataSet();
                    con = new SqlConnection();
                    con.ConnectionString = conStr;
                    con.Open();
                    cm = new SqlCommand();
                    cm.CommandText = "GetBoutiqueUserMaster";
                    cm.CommandType = CommandType.StoredProcedure;
                    cm.Connection = con;
                    cm.Parameters.AddWithValue("@CustomerCode", CustomerCode);
                    da = new SqlDataAdapter();
                    da.SelectCommand = cm;
                    da.Fill(ds1);
                    con.Close();


                    Sessions.CityName = ds1.Tables[0].Rows[0]["City"].ToString();
                    ck.InsertCookie10Hrs("CityName", ds1.Tables[0].Rows[0]["City"].ToString());

                    Sessions.CountryName = ds1.Tables[0].Rows[0]["Country"].ToString();
                    ck.InsertCookie10Hrs("CountryName", ds1.Tables[0].Rows[0]["Country"].ToString());

                    Sessions.CustomerName = ds1.Tables[0].Rows[0]["CustomerName"].ToString();
                    ck.InsertCookie10Hrs("CustomerName", ds1.Tables[0].Rows[0]["CustomerName"].ToString());



                   

                }
                //Sessions.FromDate = ds.Tables[1].Rows[0]["FromDate"].ToString();
                //Sessions.ToDate = ds.Tables[1].Rows[0]["ToDate"].ToString();

                //ck.InsertCookie10Hrs("FromDate", ds.Tables[1].Rows[0]["FromDate"].ToString());
                //ck.InsertCookie10Hrs("ToDate", ds.Tables[1].Rows[0]["ToDate"].ToString());

            }



        
        return ds;
    }

    public DataSet GetTmLogin()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        Sessions.CustomerCode = CustomerCode.ToString();


        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@TmUsername", TmUsername);
        param[1] = new SqlParameter("@TmPassword", TmPassword);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetTmLogin", param);
        Sessions.CustomerCode = "";
        if (ds.Tables[0].Rows.Count > 0)
        {
            if (ds.Tables[0].Rows[0][0].ToString() != "0")
            {
                Sessions.TmCustomerId = int.Parse(ds.Tables[0].Rows[0]["TmCustomerId"].ToString());
                ck.InsertCookie10Hrs("TmCustomerId", ds.Tables[0].Rows[0]["TmCustomerId"].ToString());

                Sessions.TmCustomerName = ds.Tables[0].Rows[0]["TmCustomerName"].ToString();
                ck.InsertCookie10Hrs("TmCustomerName", ds.Tables[0].Rows[0]["TmCustomerName"].ToString());

                Sessions.TmCustomerName = ds.Tables[0].Rows[0]["TmCustomerName"].ToString();
                ck.InsertCookie10Hrs("TmCustomerName", ds.Tables[0].Rows[0]["TmCustomerName"].ToString());

                Sessions.UseLoginName = ds.Tables[0].Rows[0]["TmCustomerName"].ToString();
                ck.InsertCookie10Hrs("UseLoginName", ds.Tables[0].Rows[0]["TmCustomerName"].ToString());

                

                Sessions.CustomerCode = ds.Tables[0].Rows[0]["CustomerCode"].ToString();
                ck.InsertCookie10Hrs("CustomerCode", ds.Tables[0].Rows[0]["CustomerCode"].ToString());
              

                DataSet ds1 = new DataSet();
                con = new SqlConnection();
                con.ConnectionString = conStr;
                con.Open();
                cm = new SqlCommand();
                cm.CommandText = "GetBoutiqueUserMaster";
                cm.CommandType = CommandType.StoredProcedure;
                cm.Connection = con;
                cm.Parameters.AddWithValue("@CustomerCode", CustomerCode);
                da = new SqlDataAdapter();
                da.SelectCommand = cm;
                da.Fill(ds1);
                con.Close();
                
                Sessions.CityName = ds1.Tables[0].Rows[0]["City"].ToString();
                ck.InsertCookie10Hrs("CityName", ds1.Tables[0].Rows[0]["City"].ToString());

                Sessions.CountryName = ds1.Tables[0].Rows[0]["Country"].ToString();
                ck.InsertCookie10Hrs("CountryName", ds1.Tables[0].Rows[0]["Country"].ToString());

                Sessions.CustomerName = ds1.Tables[0].Rows[0]["CustomerName"].ToString();
                ck.InsertCookie10Hrs("CustomerName", ds1.Tables[0].Rows[0]["CustomerName"].ToString());
                
            }
          

        }



        return ds;
    }
    #endregion

    public DataTable getForgetPassword()
    {
       
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@UserID", UserID);          
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "proc_getForgetPassword", param);
            return ds.Tables[0];
    }

    #region Get Customer
    public DataSet GetCustomer()
    {
        DataSet ds = new DataSet();
       
        //    string sqlCommand = "GetCustomer";          
        //    SqlParameter[] param = new SqlParameter[1];
        //    param[0] = new SqlParameter("@CustomerId", CustomerId);
        //    ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param); 


        
        //return ds;

        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "GetCustomer";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@CustomerId", CustomerId);
        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();
        return ds;
    }
    public DataSet GetCustomerUserDetails()
    {
        DataSet ds = new DataSet();
      
        //    string sqlCommand = "GetCustomerUserDetails";
        //    SqlParameter[] param = new SqlParameter[1];
        //    param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        //    ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        
        //return ds;

        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "GetCustomerUserDetails";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@CustomerCode", CustomerCode);
        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();
        return ds;
    }

    public DataSet GetEnquiryCustomer()
    {
        DataSet ds = new DataSet();
       
            string sqlCommand = "GetEnquiryCustomer";
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ID", ID);
            ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }
    #endregion


    #region Get Customer Search
    public DataSet GetCustomerSearch()
    {
        DataSet ds = new DataSet();
       
            string sqlCommand = "GetCustomerSearch";
           
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@CustomerCode", CustomerCode);
            param[1] = new SqlParameter("@CustomerName", CustomerName);
            param[2] = new SqlParameter("@City", City);
            param[3] = new SqlParameter("@Country", Country);
            param[4] = new SqlParameter("@AccountBlocked", AccountBlocked);
            param[5] = new SqlParameter("@AccountExpiryDateFrom", AccountExpiryDateFrom);
            param[6] = new SqlParameter("@AccountExpiryDateTo", AccountExpiryDateTo);
            param[7] = new SqlParameter("@ChkAll", ChkAll);
            param[8] = new SqlParameter("@SalesMan", SalesMan);
            param[9] = new SqlParameter("@IndId", IndId);
            
            ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);         
        
        return ds;
    }
    #endregion


    #region Set User
    public int SetUser()
    {
        int retVal = 0;
        DataSet ds = new DataSet();
       
        //    string sqlCommand = "SetUsers";
          

        //    SqlParameter[] param = new SqlParameter[4];
        //    param[0] = new SqlParameter("@UserPassword", UserPassword);
        //    param[1] = new SqlParameter("@UserID", NewUserID);
        //    param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        //    param[3] = new SqlParameter("@UserRole", UserRole);

        //    ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);        
        //    if (ds.Tables[0].Rows.Count > 0)
        //    {
        //        retVal = int.Parse(ds.Tables[0].Rows[0][0].ToString());

        //    }

       
        //return retVal;


        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "SetUsers";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@UserPassword", UserPassword);
        cm.Parameters.AddWithValue("@UserID", NewUserID);
        cm.Parameters.AddWithValue("@CustomerCode", CustomerCode);
        cm.Parameters.AddWithValue("@UserRole", UserRole);
        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();
        if (ds.Tables[0].Rows.Count > 0)
        {
            retVal = int.Parse(ds.Tables[0].Rows[0][0].ToString());

        }


        return retVal;
    }

    public void UpdateUsers()
    {
        int retVal = 0;
        DataSet ds = new DataSet();
        
            //string sqlCommand = "Proc_UpdateUsers";


            //SqlParameter[] param = new SqlParameter[4];
            //param[0] = new SqlParameter("@UserPassword", UserPassword);
            //param[1] = new SqlParameter("@UserID", NewUserID);
            //param[2] = new SqlParameter("@CustomerCode", CustomerCode);
            //param[3] = new SqlParameter("@id", id);
            //ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "Proc_UpdateUsers";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@UserPassword", UserPassword);
        cm.Parameters.AddWithValue("@UserID", NewUserID);
        cm.Parameters.AddWithValue("@CustomerCode", CustomerCode);
        cm.Parameters.AddWithValue("@id", id);
        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();
       

        
    }
    #endregion

    #region Get User
    public DataTable getAllUser()
    {
        DataSet ds = new DataSet();
        //string sqlCommand = "proc_getAllUser";
        //SqlParameter[] param = new SqlParameter[2];    
        //param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        //param[1] = new SqlParameter("@id", id);
        //ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        //return ds.Tables[0];
        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "proc_getAllUser";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@CustomerCode", CustomerCode);
        cm.Parameters.AddWithValue("@id", id);
        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();
        return ds.Tables[0];

    }
     #endregion

    #region Delete User
    public DataTable DeleteUser()
    {
       DataSet ds = new DataSet();
        //string sqlCommand = "proc_DeleteUser";
        //SqlParameter[] param = new SqlParameter[2];
        //param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        //param[1] = new SqlParameter("@id", id);
        //ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        //return ds.Tables[0];
        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "proc_DeleteUser";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@CustomerCode", CustomerCode);
        cm.Parameters.AddWithValue("@id", id);
        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();
        return ds.Tables[0];
    }
    #endregion
    #region Update User Password
    public DataTable UpdateUserPassword()
    {
        DataSet ds = new DataSet();
        
      
        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "UpdateUserPassword";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@userid", userid);
        cm.Parameters.AddWithValue("@Password", UserPassword);
        cm.Parameters.AddWithValue("@Username", Username);
        cm.Parameters.AddWithValue("@CustomerCode", CustomerCode);
        cm.Parameters.AddWithValue("@OldPassword", OldPassword);
        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();       
        return ds.Tables[0];
    }
    #endregion




    #region SetCustomerInformation
    public DataTable SetCustomerInformation()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "SetCustomerInformation";       
        SqlParameter[] param = new SqlParameter[29];
        param[0] = new SqlParameter("@UserId", UserId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@CustomerName", CustomerName);
        param[3] = new SqlParameter("@ContactPerson", ContactPerson);
        param[4] = new SqlParameter("@AddressLine1", AddressLine1);
        param[5] = new SqlParameter("@AddressLine2", AddressLine2);
        param[6] = new SqlParameter("@AddressLine3", AddressLine3);
        param[7] = new SqlParameter("@City", City);
        param[8] = new SqlParameter("@Country", Country);
        param[9] = new SqlParameter("@PhoneNumber", PhoneNumber);
        param[10] = new SqlParameter("@CellNumber", CellNumber);
        param[11] = new SqlParameter("@PrimaryEmail", PrimaryEmail);
        param[12] = new SqlParameter("@SecondryEmail", SecondryEmail);
        param[13] = new SqlParameter("@AccountOpeningDate", AccountOpeningDate);
        param[14] = new SqlParameter("@AccountExpiryDate", AccountExpiryDate);
        param[15] = new SqlParameter("@NumberOfUsers", NumberOfUsers);
        param[16] = new SqlParameter("@AccountBlocked", AccountBlocked);
        param[17] = new SqlParameter("@Currency", Currency);
        param[18] = new SqlParameter("@SetupCharges", SetupCharges);
        param[19] = new SqlParameter("@MonthlyCharges", MonthlyCharges);
        param[20] = new SqlParameter("@NewUserID", NewUserID);
        param[21] = new SqlParameter("@UserPassword", UserPassword);
        param[22] = new SqlParameter("@IndId", IndId);
        param[23] = new SqlParameter("@PanNo", PanNo);
        param[24] = new SqlParameter("@SalesTaxNo", SalesTaxNo);
        param[25] = new SqlParameter("@FromDate", FromDate);
        param[26] = new SqlParameter("@ToDate", ToDate);
        param[27] = new SqlParameter("@SalesMan", SalesMan);
        param[28] = new SqlParameter("@Remark", Remark); 
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public DataTable AddEditEnquiryInfo()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_AddEditEnquiryInfo";
        SqlParameter[] param = new SqlParameter[19];
        param[0] = new SqlParameter("@ID", ID);
        param[1] = new SqlParameter("@CustomerName", CustomerName);
        param[2] = new SqlParameter("@ContactPerson", ContactPerson);       
        param[3] = new SqlParameter("@AddressLine1", AddressLine1);
        param[4] = new SqlParameter("@AddressLine2", AddressLine2);
        param[5] = new SqlParameter("@AddressLine3", AddressLine3);
        param[6] = new SqlParameter("@City", City);
        param[7] = new SqlParameter("@Country", Country);
        param[8] = new SqlParameter("@PhoneNumber", PhoneNumber);
        param[9] = new SqlParameter("@CellNumber", CellNumber);
        param[10] = new SqlParameter("@PrimaryEmail", PrimaryEmail);
        param[11] = new SqlParameter("@SecondryEmail", SecondryEmail);      
        param[12] = new SqlParameter("@IndId", IndId);
        param[13] = new SqlParameter("@PanNo", PanNo);
        param[14] = new SqlParameter("@SalesTaxNo", SalesTaxNo);
        param[15] = new SqlParameter("@NumberOfUsers", NumberOfUsers);
        param[16] = new SqlParameter("@IsActive", IsActiveStatus);
        param[17] = new SqlParameter("@EnquiryDate", System.DateTime.Now.ToString());

        param[18] = new SqlParameter("@ClientIp",ClientIp );
       
       
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable AddEditSalesMan()
    {
      
        DataSet ds = new DataSet();
        string sqlCommand = "Proc_AddEditSalesMan";
        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@SalesManName", SalesManName);
        param[2] = new SqlParameter("@Address", Address);
        param[3] = new SqlParameter("@City", City);
        param[4] = new SqlParameter("@Country", Country);
        param[5] = new SqlParameter("@CellNumber", CellNumber);
        param[6] = new SqlParameter("@Email", Email);
        param[7] = new SqlParameter("@SetupCommision", SetupCommision);
        param[8] = new SqlParameter("@MonthlyCommision", MonthlyCommision);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public DataTable getSalesMan()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "Proc_getSalesMan";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@id", id);       
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    #endregion



    #region Update Password
    public DataTable UpdatePassword()
    {
        DataSet ds = new DataSet();
      
        string sqlCommand = "Proc_UpdatePassword";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@userid", userid);
        param[1] = new SqlParameter("@Username", Username);
        param[2] = new SqlParameter("@Password", Password);
        param[3] = new SqlParameter("@OldPassword", OldPassword);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }
    #endregion



    public DataTable AddEditAccessRight()
    {

        DataSet ds = new DataSet();
        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "proc_AddEditAccessRight";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@id", id);
        cm.Parameters.AddWithValue("@Userid", Userid_UserRole);
        cm.Parameters.AddWithValue("@MenuId", MenuId);
        cm.Parameters.AddWithValue("@IsAccess", IsAccess);
        cm.Parameters.AddWithValue("@IsAdd", IsAdd);
        cm.Parameters.AddWithValue("@IsDelete", IsDelete);
        cm.Parameters.AddWithValue("@IsEdit", IsEditRight);
        cm.Parameters.AddWithValue("@IsPrint", IsPrint);

        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();

        return ds.Tables[0];
       
        //string sqlCommand = "proc_AddEditAccessRight";
        //SqlParameter[] param = new SqlParameter[8];
        //param[0] = new SqlParameter("@id", id);
        //param[1] = new SqlParameter("@Userid", Userid_UserRole);
        //param[2] = new SqlParameter("@MenuId", MenuId);
        //param[3] = new SqlParameter("@IsAccess", IsAccess);
        //param[4] = new SqlParameter("@IsAdd", IsAdd);
        //param[5] = new SqlParameter("@IsDelete", IsDelete);
        //param[6] = new SqlParameter("@IsEdit", IsEditRight);
        //param[7] = new SqlParameter("@IsPrint", IsPrint);
     
        //ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
       
    }

    public DataTable GetaccessRight()
    {

        //DataSet ds = new DataSet();
        //string sqlCommand = "proc_GetaccessRight";
        //SqlParameter[] param = new SqlParameter[1];
        //param[0] = new SqlParameter("@Userid", Userid_UserRole);    
     
        //ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        //return ds.Tables[0];

        DataSet ds = new DataSet();
        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "proc_GetaccessRight";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;       
        cm.Parameters.AddWithValue("@Userid", Userid_UserRole);  
        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();

        return ds.Tables[0];
    }

    public void deleteaccessRight()
    {

        //DataSet ds = new DataSet();
        //string sqlCommand = "proc_deleteaccessRight";
        //SqlParameter[] param = new SqlParameter[1];
        //param[0] = new SqlParameter("@Userid", Userid_UserRole);    
     
        //ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        DataSet ds = new DataSet();
        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "proc_deleteaccessRight";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@Userid", Userid_UserRole);
        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();

       
       
    }


    public DataTable getPageUserRights()
    {

        //DataSet ds = new DataSet();
        //string sqlCommand = "proc_getPageUserRights";
        //SqlParameter[] param = new SqlParameter[2];
        //param[0] = new SqlParameter("@Userid", Userid_UserRole);
        //param[1] = new SqlParameter("@PageName", PageName);

        //ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        //return ds.Tables[0];

        DataSet ds = new DataSet();
        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "proc_getPageUserRights";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@Userid", Userid_UserRole);
        cm.Parameters.AddWithValue("@PageName", PageName);
        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();
        return ds.Tables[0];
    }

    #region Delete User
    public void deleteEnquiry()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_deleteEnquiry";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@ID", ID);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
       
    }
    #endregion



    public void DoOdit()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_DoOdit";
        SqlParameter[] param = new SqlParameter[0];   

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

    }

}
#endregion