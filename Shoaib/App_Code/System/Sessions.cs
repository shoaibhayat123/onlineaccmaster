﻿#region NameSpace Used....
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


#endregion

#region Class....
/// <summary>
/// Class is Used to Maintain the InProcess Sessions State
/// </summary>

public class Sessions
{
    clsCookie ck = new clsCookie();
    private static string _PageUrl;
    #region Class Constructor....
    public Sessions()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #endregion

    #region Userd Defined Static Functions....

    #region Get or Set _UserId Login Id ....
    public static int UseLoginId
    {
        get
        {
            if (HttpContext.Current.Session["_UserId"] == null || HttpContext.Current.Session["_UserId"].ToString() == string.Empty)
                return 0;
            else
                return int.Parse(HttpContext.Current.Session["_UserId"].ToString());
        }
        set
        {
            HttpContext.Current.Session["_UserId"] = value;
        }
    }
    #endregion

    #region Get or Set _UserId Login Name
    public static string UseLoginName
    {
        get
        {
            if (HttpContext.Current.Session["Username"] == null || HttpContext.Current.Session["Username"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["Username"].ToString();
        }
        set
        {
            HttpContext.Current.Session["Username"] = value;
        }
    }
    #endregion

    #region Get or Set RoleId  ....
    public static int UserRoleId
    {
        get
        {
            if (HttpContext.Current.Session["RoleId"] == null || HttpContext.Current.Session["RoleId"].ToString() == string.Empty)
                return 0;
            else
                return int.Parse(HttpContext.Current.Session["RoleId"].ToString());
        }
        set
        {
            HttpContext.Current.Session["RoleId"] = value;
        }
    }
    #endregion

    #region Get or Set Admin User
    public static string AdminUser
    {
        get
        {
            if (HttpContext.Current.Session["AdminUser"] == null || HttpContext.Current.Session["AdminUser"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["AdminUser"].ToString();
        }
        set
        {
            HttpContext.Current.Session["AdminUser"] = value;
        }
    }
    #endregion

    #region Get or Set Admin User Id....
    public static int AdminUserId
    {
        get
        {
            if (HttpContext.Current.Session["AdminUserId"] == null || HttpContext.Current.Session["AdminUserId"].ToString() == string.Empty)
                return 0;
            else
                return int.Parse(HttpContext.Current.Session["AdminUserId"].ToString());
        }
        set
        {
            HttpContext.Current.Session["AdminUserId"] = value;
        }
    }
    #endregion

    #region Get or Set Admin RoleId  ....
    public static int AdminUserRoleId
    {
        get
        {
            if (HttpContext.Current.Session["AdminUserRoleId"] == null || HttpContext.Current.Session["AdminUserRoleId"].ToString() == string.Empty)
                return 0;
            else
                return int.Parse(HttpContext.Current.Session["AdminUserRoleId"].ToString());
        }
        set
        {
            HttpContext.Current.Session["AdminUserRoleId"] = value;
        }
    }
    #endregion

    #region Get or Set Admin User Country Id....
    public static int AdminUserCountryId
    {
        get
        {
            if (HttpContext.Current.Session["AdminUserCountryId"] == null || HttpContext.Current.Session["AdminUserCountryId"].ToString() == string.Empty)
                return 0;
            else
                return int.Parse(HttpContext.Current.Session["AdminUserCountryId"].ToString());
        }
        set
        {
            HttpContext.Current.Session["AdminUserCountryId"] = value;
        }
    }
    #endregion

    #region Get or Set Admin Country Name....
    public static string AdminCountryName
    {
        get
        {
            if (HttpContext.Current.Session["AdminCountryName"] == null || HttpContext.Current.Session["AdminCountryName"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["AdminCountryName"].ToString();
        }
        set
        {
            HttpContext.Current.Session["AdminCountryName"] = value;
        }
    }
    #endregion


    #region Get or Set PropertyId....
    public static int PropertyId
    {

        get
        {
            if (HttpContext.Current.Session["PropertyId"] == null || HttpContext.Current.Session["PropertyId"].ToString() == string.Empty)
                return 0;
            else
                return int.Parse(HttpContext.Current.Session["PropertyId"].ToString());
        }
        set
        {
            HttpContext.Current.Session["PropertyId"] = value;
        }
    }
    #endregion

    #region Get or Set CurrencyId....
    public static int CurrencyId
    {

        get
        {
            if (HttpContext.Current.Session["CurrencyId"] == null || HttpContext.Current.Session["CurrencyId"].ToString() == string.Empty)
                return 0;
            else
                return int.Parse(HttpContext.Current.Session["CurrencyId"].ToString());
        }
        set
        {
            HttpContext.Current.Session["CurrencyId"] = value;
        }
    }
    #endregion

    #region Get or Set CompanyId....
    public static int CompanyId
    {

        get
        {
            if (HttpContext.Current.Session["CompanyId"] == null || HttpContext.Current.Session["CompanyId"].ToString() == string.Empty)
                return 0;
            else
                return int.Parse(HttpContext.Current.Session["CompanyId"].ToString());
        }
        set
        {
            HttpContext.Current.Session["CompanyId"] = value;
        }
    }
    #endregion

    #region Get or Set Company Name
    public static string CompanyName
    {

        get
        {
            if (HttpContext.Current.Session["CompanyName"] == null || HttpContext.Current.Session["CompanyName"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["CompanyName"].ToString();
        }
        set
        {
            HttpContext.Current.Session["CompanyName"] = value;

        }
    }
    #endregion

    #region Get or Set PostUrl
    public static string PostUrl
    {

        get
        {
            if (HttpContext.Current.Session["PostUrl"] == null || HttpContext.Current.Session["PostUrl"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["PostUrl"].ToString();
        }
        set
        {
            HttpContext.Current.Session["PostUrl"] = value;

        }
    }
    #endregion

    #region Get or Set Admin User Email  ....
    public static string AdminUserEmail
    {

        get
        {
            if (HttpContext.Current.Session["AdminUserEmail"] == null || HttpContext.Current.Session["AdminUserEmail"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["AdminUserEmail"].ToString();
        }
        set
        {
            HttpContext.Current.Session["AdminUserEmail"] = value;
        }
    }
    #endregion

    #region Get or Set  Email  ....
    public static string Email
    {

        get
        {
            if (HttpContext.Current.Session["Email"] == null || HttpContext.Current.Session["Email"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["Email"].ToString();
        }
        set
        {
            HttpContext.Current.Session["Email"] = value;
        }
    }
    #endregion

    #region Get or Set Search Value ....
    public static DataTable SearchValue
    {

        get
        {
            if (HttpContext.Current.Session["SearchValue"] == null || HttpContext.Current.Session["SearchValue"].ToString() == string.Empty)
                return null;
            else
                return (DataTable)(HttpContext.Current.Session["SearchValue"]);
        }
        set
        {
            HttpContext.Current.Session["SearchValue"] = value;
        }
    }
    #endregion

    //#region Get or Set Cart ....
    //public static DataTable Cart
    //{

    //    get
    //    {
    //        if (HttpContext.Current.Session["Cart"] == null || HttpContext.Current.Session["Cart"].ToString() == string.Empty)
    //            return null;
    //        else
    //            return (DataTable)(HttpContext.Current.Session["Cart"]);
    //    }
    //    set
    //    {
    //        HttpContext.Current.Session["Cart"] = value;
    //    }
    //}
    //#endregion

    #region Get or Set Product Name
    public static string ProductName
    {

        get
        {
            if (HttpContext.Current.Session["ProductName"] == null || HttpContext.Current.Session["ProductName"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["ProductName"].ToString();
        }
        set
        {
            HttpContext.Current.Session["ProductName"] = value;

        }
    }
    #endregion

    #region Get or Set UserGuid
    public static string UserGuid
    {

        get
        {
            if (HttpContext.Current.Session["UserGuid"] == null || HttpContext.Current.Session["UserGuid"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["UserGuid"].ToString();
        }
        set
        {
            HttpContext.Current.Session["UserGuid"] = value;

        }
    }
    #endregion

    #region Get or Set Html Content
    public static string HtmlContent
    {

        get
        {
            if (HttpContext.Current.Session["HtmlContent"] == null || HttpContext.Current.Session["HtmlContent"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["HtmlContent"].ToString();
        }
        set
        {
            HttpContext.Current.Session["HtmlContent"] = value;

        }
    }
    #endregion

    #region Get or Set Html Footer
    public static string HtmlFooter
    {

        get
        {
            if (HttpContext.Current.Session["HtmlFooter"] == null || HttpContext.Current.Session["HtmlFooter"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["HtmlFooter"].ToString();
        }
        set
        {
            HttpContext.Current.Session["HtmlFooter"] = value;

        }
    }
    #endregion

    #region Get or Set Html Middle
    public static string HtmlMiddle
    {

        get
        {
            if (HttpContext.Current.Session["HtmlMiddle"] == null || HttpContext.Current.Session["HtmlMiddle"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["HtmlMiddle"].ToString();
        }
        set
        {
            HttpContext.Current.Session["HtmlMiddle"] = value;

        }
    }
    #endregion

    #region Get or Set PropertyList ....
    public static DataTable PropertyList
    {

        get
        {
            if (HttpContext.Current.Session["PropertyList"] == null || HttpContext.Current.Session["PropertyList"].ToString() == string.Empty)
                return null;
            else
                return (DataTable)(HttpContext.Current.Session["PropertyList"]);
        }
        set
        {
            HttpContext.Current.Session["PropertyList"] = value;
        }
    }
    #endregion

    #region Get or Set PropertyListing ....
    public static DataSet PropertyListing
    {

        get
        {
            if (HttpContext.Current.Session["PropertyListing"] == null || HttpContext.Current.Session["PropertyListing"].ToString() == string.Empty)
                return null;
            else
                return (DataSet)(HttpContext.Current.Session["PropertyListing"]);
        }
        set
        {
            HttpContext.Current.Session["PropertyListing"] = value;
        }
    }
    #endregion

    #region LogOut
    public void LogOut()
    {
        throw new System.NotImplementedException();
    }
    #endregion

    #endregion

    public void SessionClear()
    {
        throw new System.NotImplementedException();
    }

    #region Get or Set  Country Id....
    public static int CountryId
    {
        get
        {
            if (HttpContext.Current.Session["CountryId"] == null || HttpContext.Current.Session["CountryId"].ToString() == string.Empty)
                return 0;
            else
                return int.Parse(HttpContext.Current.Session["CountryId"].ToString());
        }
        set
        {
            HttpContext.Current.Session["CountryId"] = value;
        }
    }
    #endregion

    #region Get or Set  User Id....
    public static int UserId
    {
        get
        {
            if (HttpContext.Current.Session["UserId"] == null || HttpContext.Current.Session["UserId"].ToString() == string.Empty)
                return 0;
            else
                return int.Parse(HttpContext.Current.Session["UserId"].ToString());
        }
        set
        {
            HttpContext.Current.Session["UserId"] = value;
        }
    }
    #endregion

    #region Get or Set  User Name....
    public static string UserName
    {
        get
        {
            if (HttpContext.Current.Session["UserName"] == null || HttpContext.Current.Session["UserName"].ToString() == string.Empty)
                return "0";
            else
                return HttpContext.Current.Session["UserName"].ToString();
        }
        set
        {
            HttpContext.Current.Session["UserName"] = value;
        }
    }
    #endregion
    

    #region Get or Set  OrderNo
    public static string OrderNo
    {
        get
        {
            if (HttpContext.Current.Session["OrderNo"] == null || HttpContext.Current.Session["OrderNo"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["OrderNo"].ToString();
        }
        set
        {
            HttpContext.Current.Session["OrderNo"] = value;
        }
    }
    #endregion

    #region Get or Set  Category Id....
    public static int CategoryId
    {
        get
        {
            if (HttpContext.Current.Session["CategoryId"] == null || HttpContext.Current.Session["CategoryId"].ToString() == string.Empty)
                return 0;
            else
                return int.Parse(HttpContext.Current.Session["CategoryId"].ToString());
        }
        set
        {
            HttpContext.Current.Session["CategoryId"] = value;
        }
    }
    #endregion

    public static string PageUrl
    {
        get
        {
            return _PageUrl;
        }
        set
        {
            _PageUrl = value;
        }
    }

    #region Get or Set Category Name
    public static string CategoryName
    {

        get
        {
            if (HttpContext.Current.Session["CategoryName"] == null || HttpContext.Current.Session["CategoryName"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["CategoryName"].ToString();
        }
        set
        {
            HttpContext.Current.Session["CategoryName"] = value;

        }
    }
    #endregion


    #region Get or Set Customer Code
    public static string CustomerCode
    {

        get
        {
            if (HttpContext.Current.Session["CustomerCode"] == null || HttpContext.Current.Session["CustomerCode"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["CustomerCode"].ToString();
        }
        set
        {
            HttpContext.Current.Session["CustomerCode"] = value;

        }
    }
    #endregion

    #region Get or Set Customer Name
    public static string CustomerName
    {

        get
        {
            if (HttpContext.Current.Session["CustomerName"] == null || HttpContext.Current.Session["CustomerName"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["CustomerName"].ToString();
        }
        set
        {
            HttpContext.Current.Session["CustomerName"] = value;

        }
    }
    #endregion




    #region Get or Set SubCategory Name
    public static string SubCategoryName
    {

        get
        {
            if (HttpContext.Current.Session["SubCategoryName"] == null || HttpContext.Current.Session["SubCategoryName"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["SubCategoryName"].ToString();
        }
        set
        {
            HttpContext.Current.Session["SubCategoryName"] = value;

        }
    }
    #endregion


    #region Get or Set  SubCategory Id....
    public static int SubCategoryId
    {
        get
        {
            if (HttpContext.Current.Session["SubCategoryId"] == null || HttpContext.Current.Session["SubCategoryId"].ToString() == string.Empty)
                return 0;
            else
                return int.Parse(HttpContext.Current.Session["SubCategoryId"].ToString());
        }
        set
        {
            HttpContext.Current.Session["SubCategoryId"] = value;
        }
    }
    #endregion

    #region Get or Set Country Name
    public static string CountryName
    {

        get
        {
            if (HttpContext.Current.Session["CountryName"] == null || HttpContext.Current.Session["CountryName"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["CountryName"].ToString();
        }
        set
        {
            HttpContext.Current.Session["CountryName"] = value;

        }
    }
    #endregion

    #region Get or Set City Name
    public static string CityName
    {

        get
        {
            if (HttpContext.Current.Session["CityName"] == null || HttpContext.Current.Session["CityName"].ToString() == string.Empty)
                return "";
            else
                return HttpContext.Current.Session["CityName"].ToString();
        }
        set
        {
            HttpContext.Current.Session["CityName"] = value;

        }
    }
    #endregion

    public static object FromDate
    {
        get { return HttpContext.Current.Session["FromDate"]; }
        set { HttpContext.Current.Session["FromDate"] = value; }
    }
    public static object ToDate
    {
        get { return HttpContext.Current.Session["ToDate"]; }
        set { HttpContext.Current.Session["ToDate"] = value; }
    }

    public static object IndId
    {
        get { return HttpContext.Current.Session["IndId"]; }
        set { HttpContext.Current.Session["IndId"] = value; }
    }

    public static object UserRole
    {
        get { return HttpContext.Current.Session["UserRole"]; }
        set { HttpContext.Current.Session["UserRole"] = value; }
    }
    public static object Brand
    {
        get { return HttpContext.Current.Session["Brand"]; }
        set { HttpContext.Current.Session["Brand"] = value; }
    }
    public static object CustomerDetails
    {
        get { return HttpContext.Current.Session["CustomerDetails"]; }
        set { HttpContext.Current.Session["CustomerDetails"] = value; }
    }
    
    public static object TmCustomerId
    {
        get { return HttpContext.Current.Session["TmCustomerId"]; }
        set { HttpContext.Current.Session["TmCustomerId"] = value; }
    }
    public static object TmCustomerName
    {
        get { return HttpContext.Current.Session["TmCustomerName"]; }
        set { HttpContext.Current.Session["TmCustomerName"] = value; }
    }

}





#endregion
