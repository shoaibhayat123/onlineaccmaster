using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for SystemLabels
/// </summary>
public class SystemLabels
{
	public SystemLabels()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static string lblAddContentPage = "Page successfully added.";
    public static string lblUpdateContentPage = "Page successfully updated.";

    public static string lblNoRecord = "No record found.";

    public static string lblUserActivation = "Registration";
    public static string lblloginCheck = "First you have to login to view our Product Collection";
    public static string lblloginFailed = "Invalid Username or Password.";


}
