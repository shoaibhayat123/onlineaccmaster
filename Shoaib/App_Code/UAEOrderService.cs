﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UAEOrderService2
{
    using System.Runtime.Serialization;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="OrderDetails", Namespace="http://schemas.datacontract.org/2004/07/UAEOrderService2")]
    public partial class OrderDetails : object, System.Runtime.Serialization.IExtensibleDataObject
    {
        
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private string Category_CodeField;
        
        private string Product_NameField;
        
        private int QuantityField;
        
        private decimal ServiceChargesField;
        
        private decimal UnitPriceField;
        
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Category_Code
        {
            get
            {
                return this.Category_CodeField;
            }
            set
            {
                this.Category_CodeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Product_Name
        {
            get
            {
                return this.Product_NameField;
            }
            set
            {
                this.Product_NameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Quantity
        {
            get
            {
                return this.QuantityField;
            }
            set
            {
                this.QuantityField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal ServiceCharges
        {
            get
            {
                return this.ServiceChargesField;
            }
            set
            {
                this.ServiceChargesField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public decimal UnitPrice
        {
            get
            {
                return this.UnitPriceField;
            }
            set
            {
                this.UnitPriceField = value;
            }
        }
    }
}


[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
[System.ServiceModel.ServiceContractAttribute(ConfigurationName="IUAEOrderService")]
public interface IUAEOrderService
{
    
    [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUAEOrderService/SetCategory", ReplyAction="http://tempuri.org/IUAEOrderService/SetCategoryResponse")]
    string SetCategory(long Vendor_Id, long Branch_Id, string category_name, string category_code, decimal margin, string EncryptedView);
    
    [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUAEOrderService/SetCategory", ReplyAction="http://tempuri.org/IUAEOrderService/SetCategoryResponse")]
    System.Threading.Tasks.Task<string> SetCategoryAsync(long Vendor_Id, long Branch_Id, string category_name, string category_code, decimal margin, string EncryptedView);
    
    [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUAEOrderService/SetProduct", ReplyAction="http://tempuri.org/IUAEOrderService/SetProductResponse")]
    string SetProduct(long Vendor_Id, long Branch_Id, string product_name, string category_code, decimal margin, string EncryptedView);
    
    [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUAEOrderService/SetProduct", ReplyAction="http://tempuri.org/IUAEOrderService/SetProductResponse")]
    System.Threading.Tasks.Task<string> SetProductAsync(long Vendor_Id, long Branch_Id, string product_name, string category_code, decimal margin, string EncryptedView);
    
    [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUAEOrderService/SetOrder", ReplyAction="http://tempuri.org/IUAEOrderService/SetOrderResponse")]
    string SetOrder(long Vendor_Id, long Branch_Id, string terminal_code, string CellNo, string Name, string City, UAEOrderService2.OrderDetails[] order_details, string EncryptedView);
    
    [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUAEOrderService/SetOrder", ReplyAction="http://tempuri.org/IUAEOrderService/SetOrderResponse")]
    System.Threading.Tasks.Task<string> SetOrderAsync(long Vendor_Id, long Branch_Id, string terminal_code, string CellNo, string Name, string City, UAEOrderService2.OrderDetails[] order_details, string EncryptedView);
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
public interface IUAEOrderServiceChannel : IUAEOrderService, System.ServiceModel.IClientChannel
{
}

[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
public partial class UAEOrderServiceClient : System.ServiceModel.ClientBase<IUAEOrderService>, IUAEOrderService
{
    
    public UAEOrderServiceClient()
    {
    }
    
    public UAEOrderServiceClient(string endpointConfigurationName) : 
            base(endpointConfigurationName)
    {
    }
    
    public UAEOrderServiceClient(string endpointConfigurationName, string remoteAddress) : 
            base(endpointConfigurationName, remoteAddress)
    {
    }
    
    public UAEOrderServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
            base(endpointConfigurationName, remoteAddress)
    {
    }
    
    public UAEOrderServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
            base(binding, remoteAddress)
    {
    }
    
    public string SetCategory(long Vendor_Id, long Branch_Id, string category_name, string category_code, decimal margin, string EncryptedView)
    {
        return base.Channel.SetCategory(Vendor_Id, Branch_Id, category_name, category_code, margin, EncryptedView);
    }
    
    public System.Threading.Tasks.Task<string> SetCategoryAsync(long Vendor_Id, long Branch_Id, string category_name, string category_code, decimal margin, string EncryptedView)
    {
        return base.Channel.SetCategoryAsync(Vendor_Id, Branch_Id, category_name, category_code, margin, EncryptedView);
    }
    
    public string SetProduct(long Vendor_Id, long Branch_Id, string product_name, string category_code, decimal margin, string EncryptedView)
    {
        return base.Channel.SetProduct(Vendor_Id, Branch_Id, product_name, category_code, margin, EncryptedView);
    }
    
    public System.Threading.Tasks.Task<string> SetProductAsync(long Vendor_Id, long Branch_Id, string product_name, string category_code, decimal margin, string EncryptedView)
    {
        return base.Channel.SetProductAsync(Vendor_Id, Branch_Id, product_name, category_code, margin, EncryptedView);
    }
    
    public string SetOrder(long Vendor_Id, long Branch_Id, string terminal_code, string CellNo, string Name, string City, UAEOrderService2.OrderDetails[] order_details, string EncryptedView)
    {
        return base.Channel.SetOrder(Vendor_Id, Branch_Id, terminal_code, CellNo, Name, City, order_details, EncryptedView);
    }
    
    public System.Threading.Tasks.Task<string> SetOrderAsync(long Vendor_Id, long Branch_Id, string terminal_code, string CellNo, string Name, string City, UAEOrderService2.OrderDetails[] order_details, string EncryptedView)
    {
        return base.Channel.SetOrderAsync(Vendor_Id, Branch_Id, terminal_code, CellNo, Name, City, order_details, EncryptedView);
    }
}
