﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;


/// <summary>
/// Summary description for TmCountry
/// </summary>
public class TmCountry
{
    	 
     
    #region Data Members
    public string FileNo, OtherSideGoods, SearchText,TmUsername, OtherSideMemoNumber, TmPassword, RNumber, Classes, DetailsRemark, OtherSideApplicationUser, OtherSideClientAddress, OtherSideClientAdvocateName, OthersideClientName, OtherSideClientAdvocateAddress, Country, OtherSideTmTradeMark, OtherSideTmClass, OppositionType, OppositionStatus, OppositionRemark, OppositionNo, PaymentMode, PageNo, FormName, Priorty, CustomerWebAddress, TmWebAddress, PaymentMetod, Before, Image, ConflictingTrademark, Comment, AdvOrAgentAppeared, PeriodOfuse, WorkLabel, TmImage, Reminder, Remark, TmFileStatus, MemoNumber, TradeMark, Goods, Description, BusinessType, Party, TmAgentName, TmPhoneNum, TmMobileNo, TmFax, TmEmail, CityName, TmCustomerName, TmAgentAddress, TmCustomerAddress, ContactPerson, Designation, Email, Mobile, Fax;
    public int ClientId , DebitAcId, ApplicationNo, OtherSideJournalNo, AccountId, NoOfCharacter, JournalPageNo, TmOppositionFileStatusId, TmOppositionFormId, TmOppositionFormDetailId, TmOppositionDocumentDetailId, TmOppositionDocumentId, TmOppositionDetailId, TmOppositionHearingDetailId, OtherSideTmApplicationNum, Year, TmJounalNo, Id, TmOppositionId, FormId, TmShowCauseId, TmFormId, TmFormDetailId, ClassTo, ClassFrom, CurrencyId1, TmTrademarkConflictId, TmNo, PurchaseId, VinId, VendorAcfileId, CarPaymentDetailId, CreditAcfileId, CarPaymentId, TmRenewalDetailId, TmDocumentId, TmDocumentDetailId, TmRenewalId, TmJournalDetailId, TmJournalId, JournalNo, TmHearingDetailId, CountryId, TmHearingId, TmTradeMarkId, ApplicationNum, TmFileStatusId, Class, ClassId, BusinessTypeId, TmCityId, id, TmCountryId, CityId, TmAgentId, TmCustomerId, TradeMarkDetailId, TrademarkId;
    public decimal CustomerCode, TotalAmount,Cost1,ExchangeRate1,TotalCost1;
    public Nullable<DateTime>OtherSideJournalDate,OtherSidePublicationDate, DateFrom,OppositionDate, DateTo, DetailsDate, OtherSideTmFilledOn, PublicationDate, Extension1, Extension2, LastDateOfOpposition, ShowCauseRecDateFrom, ShowCauseReplyDateFrom, ShowCauseReplyDateTo, ShowCauseRecDateTo, AccRecDateFrom, DemandNoteDepositTo, DemandNoteDepositFrom,DemandNoteRecDateTo,DemandNoteRecDateFrom, AccRecDateTo, TakeOverDateTo, TakeOverDateFrom, ReminderDateFrom, ReminderDateTo, RenewalDateTo, RenewalDateFrom, CertificateReceiveddate, HearingDate, DemandNoteDeposit, FillingDateFrom, FillingDateTo, CarPaymentDate, RenewalDate, JournalDate, PurchaseDate, Tm11FilledOn, Tm12FilledOn, TakeOverDate, FillingDate, ReminderDate, ShowCauseRecDate, AccRecDate, FilmRecDate, DemandNoteRecDate, ShowCauseReplyDate;
    public Boolean IsShowtoClient,IsAcceptance, CertificateIssued, IsShowCauseReceived, IsOpposition, IsNoteSendToClient;
    int retVal = 0;
        
    #endregion
	public TmCountry()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    #region Country
    #region Set MainCategory
    public int AddEditTmCountry()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_AddEditTmCountry";


        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CountryId", CountryId);
        param[1] = new SqlParameter("@Country", Country);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CountryId"]);

        return retVal;
    }
    #endregion
    #region Get Country
    public DataSet GetCountry()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCountry";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@CountryId", CountryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds;
    }
    #endregion
    #region Delete Country
    public int DeleteTmCountry()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteTmCountry";

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CountryId", CountryId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        
        return result;

    }
    #endregion
    #endregion


    #region MainCategory
    #region Set City
    public int AddEditTMCity()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_AddEditTMCity";


        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@CityId", CityId);
        param[1] = new SqlParameter("@CityName", CityName);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@CountryId", CountryId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CityId"]);

        return retVal;
    }
    #endregion
    #region Get City
    public DataSet GetCity()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetTmCity";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CityId", CityId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@CountryId", CountryId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }
    #endregion
    #region Delete City
    public int DeleteTmCity()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteTmCity";

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CityId", CityId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }
    #endregion
    #endregion


    public DataTable AddEditTmAgentMain()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditTmAgentMain";

        SqlParameter[] param = new SqlParameter[11];
        param[0] = new SqlParameter("@TmAgentId", TmAgentId);
        param[1] = new SqlParameter("@TmAgentName", TmAgentName);
        param[2] = new SqlParameter("@TmAgentAddress", TmAgentAddress);
        param[3] = new SqlParameter("@TmCityId", TmCityId);
        param[4] = new SqlParameter("@TmCountryId", TmCountryId);
        param[5] = new SqlParameter("@TmPhoneNum", TmPhoneNum);
        param[6] = new SqlParameter("@TmFax", TmFax);
        param[7] = new SqlParameter("@TmMobileNo", TmMobileNo);
        param[8] = new SqlParameter("@TmEmail", TmEmail);
        param[9] = new SqlParameter("@CustomerCode", CustomerCode);
        param[10] = new SqlParameter("@TmWebAddress", TmWebAddress);
        

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }




    public DataTable AddEditTmAgentDetails()
    {


        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditTmAgentDetails";

        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@ContactPerson", ContactPerson);
        param[2] = new SqlParameter("@Designation", Designation);
        param[3] = new SqlParameter("@Email", Email);
        param[4] = new SqlParameter("@Mobile", Mobile);
        param[5] = new SqlParameter("@Fax", Fax);
        param[6] = new SqlParameter("@TmAgentId", TmAgentId);
        param[7] = new SqlParameter("@CustomerCode", CustomerCode);
        

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public int DeleteTmAgentMain()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteTmAgentMain";

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@TmAgentId", TmAgentId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }
    public DataSet GetTmAgentMain()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetTmAgentMain";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@TmAgentId", TmAgentId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }

    public DataSet GetTmAgentDetails()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetTmAgentDetails";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@TmAgentId", TmAgentId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }


    public DataTable AddEditTmCustomerMain()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditTmCustomerMain";

        SqlParameter[] param = new SqlParameter[16];
        param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
        param[1] = new SqlParameter("@TmCustomerName", TmCustomerName);
        param[2] = new SqlParameter("@TmCustomerAddress", TmCustomerAddress);
        param[3] = new SqlParameter("@TmCityId", TmCityId);
        param[4] = new SqlParameter("@TmCountryId", TmCountryId);
        param[5] = new SqlParameter("@TmPhoneNum", TmPhoneNum);
        param[6] = new SqlParameter("@TmFax", TmFax);
        param[7] = new SqlParameter("@TmMobileNo", TmMobileNo);
        param[8] = new SqlParameter("@TmEmail", TmEmail);
        param[9] = new SqlParameter("@CustomerCode", CustomerCode);
        param[10] = new SqlParameter("@TmAgentId", TmAgentId);
        param[11] = new SqlParameter("@Party", Party);
        param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
        param[13] = new SqlParameter("@CustomerWebAddress", CustomerWebAddress);
        param[14] = new SqlParameter("@TmUsername", TmUsername);
        param[15] = new SqlParameter("@TmPassword", TmPassword);
        
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }




    public DataTable AddEditTmCustomerDetails()
    {
        
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditTmCustomerDetails";

        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@ContactPerson", ContactPerson);
        param[2] = new SqlParameter("@Designation", Designation);
        param[3] = new SqlParameter("@Email", Email);
        param[4] = new SqlParameter("@Mobile", Mobile);
        param[5] = new SqlParameter("@Fax", Fax);
        param[6] = new SqlParameter("@TmCustomerId", TmCustomerId);
        param[7] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public int DeleteTmCustomerMain()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteTmCustomerMain";

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }
    public DataSet GetTmCustomerMain()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetTmCustomerMain";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }

    public DataSet GetTmCustomerMaindetails()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetTmCustomerMaindetails";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }

    public DataSet GetTmCustomerDetails()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetTmCustomerDetails";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }

        public int AddEditTmBusinessType()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_AddEditTmBusinessType";


        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
        param[1] = new SqlParameter("@BusinessType", BusinessType);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["BusinessTypeId"]);

        return retVal;
    }

    #region Get BusinessType
    public DataSet GetBusinessType()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetTmBusinessType";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds;
    }
    #endregion
    #region Delete BusinessType
    public int DeleteTmBusinessType()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteTmBusinessType";

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        
        return result;

    }
    #endregion


    public int DeleteClass()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteClass";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@ClassId", ClassId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }

    public DataSet GetTMClass()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetTMClass";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@ClassId", ClassId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public int AddEditTMClass()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditTMClass";
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@ClassId", ClassId);
        param[1] = new SqlParameter("@Class", Class);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@Description", Description);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["ClassId"]);


        return retVal;
    }

    public DataSet GetTmFileStatus()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetTmFileStatus";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public int DeleteTmFileStatus()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteTmFileStatus";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }

    public int AddEditTmFileStatus()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditTmFileStatus";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
        param[1] = new SqlParameter("@TmFileStatus", TmFileStatus);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmFileStatusId"]);


        return retVal;
    }
     


    public DataSet GetTmTradeMark()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetTmTradeMark";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }
    public DataSet getApplicationdata()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getApplicationdata";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;

    }

    public int DeleteTmTradeMark()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteTmTradeMark";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@Id", Id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }


    public int AddEditTmTradeMark()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditTmTradeMark";
        SqlParameter[] param = new SqlParameter[30];
        

        param[0] = new SqlParameter("@Id",   Id);
        param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);
        param[2] = new SqlParameter("@MemoNumber", MemoNumber);
        param[3] = new SqlParameter("@TradeMark", TradeMark);
        param[4] = new SqlParameter("@Goods", Goods);
        param[5] = new SqlParameter("@TmCustomerId", TmCustomerId);
        param[6] = new SqlParameter("@ShowCauseRecDate", ShowCauseRecDate);
        param[7] = new SqlParameter("@AccRecDate", AccRecDate);
        param[8] = new SqlParameter("@FilmRecDate", FilmRecDate);
        param[9] = new SqlParameter("@DemandNoteRecDate", DemandNoteRecDate);
        param[10] = new SqlParameter("@ShowCauseReplyDate", ShowCauseReplyDate);
        param[11] = new SqlParameter("@IsAcceptance", IsAcceptance);
        param[12] = new SqlParameter("@IsOpposition", IsOpposition);

        param[13] = new SqlParameter("@IsNoteSendToClient", IsNoteSendToClient);
        param[14] = new SqlParameter("@Remark", Remark);
        param[15] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
        param[16] = new SqlParameter("@Reminder", Reminder);
        param[17] = new SqlParameter("@ReminderDate", ReminderDate);
        param[18] = new SqlParameter("@FillingDate", FillingDate);
        param[19] = new SqlParameter("@TakeOverDate", TakeOverDate);
        param[20] = new SqlParameter("@PeriodOfuse", PeriodOfuse);
        param[21] = new SqlParameter("@WorkLabel", WorkLabel);
        param[22] = new SqlParameter("@TmImage", TmImage);
        param[23] = new SqlParameter("@Tm11FilledOn", Tm11FilledOn);
        param[24] = new SqlParameter("@Tm12FilledOn", Tm12FilledOn);
        param[25] = new SqlParameter("@CustomerCode", CustomerCode);

        param[26] = new SqlParameter("@DemandNoteDeposit", DemandNoteDeposit);
        param[27] = new SqlParameter("@CertificateIssued", CertificateIssued);

        param[28] = new SqlParameter("@CertificateReceiveddate", CertificateReceiveddate);
        param[29] = new SqlParameter("@IsShowCauseReceived", IsShowCauseReceived);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);


        return retVal;
    }

    public int AddEditTmTradeMarkDetails()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditTmTradeMarkDetails";


        


        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@TradeMarkDetailId", TradeMarkDetailId);
        param[1] = new SqlParameter("@ClassId", ClassId);
        param[2] = new SqlParameter("@TrademarkId", TrademarkId);
        param[3] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TradeMarkDetailId"]);



        return retVal;


    }

    public DataSet getTradeMarkDetails()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "getTradeMarkDetails";

        SqlParameter[] param = new SqlParameter[1];

        param[0] = new SqlParameter("@TrademarkId", TrademarkId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }
    public void DeleteTradeMarkDetails()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteTradeMarkDetails";

        SqlParameter[] param = new SqlParameter[1];

        param[0] = new SqlParameter("@TrademarkId", TrademarkId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


    }




    
    public int AddEditTmHearing()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditTmHearing";





        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@TmHearingId", TmHearingId);
        param[1] = new SqlParameter("@TmTradeMarkId", TmTradeMarkId);       
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmHearingId"]);



        return retVal;


    }

    public int AddEditTmHearingDetails()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditTmHearingDetails";



      
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@TmHearingDetailId", TmHearingDetailId);
        param[1] = new SqlParameter("@HearingDate", HearingDate);       
        param[2] = new SqlParameter("@Before", Before);
         param[3] = new SqlParameter("@Comment", Comment);
         param[4] = new SqlParameter("@AdvOrAgentAppeared", AdvOrAgentAppeared);
         param[5] = new SqlParameter("@TmHearingId", TmHearingId);
         param[6] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmHearingDetailId"]);



        return retVal;


    }

    public int DeleteTmHearing()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteTmHearing";

        SqlParameter[] param = new SqlParameter[1];

        param[0] = new SqlParameter("@TmHearingId", TmHearingId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;


    }

    public DataSet getTmHearing()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "proc_getTmHearing";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@TmHearingId", TmHearingId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public DataSet getTmHearingDetails()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getTmHearingDetails";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@TmHearingId", TmHearingId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet SearchHearing()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "SearchHearing";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
       
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }




    public int AddEditTmJournal()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditTmJournal";





        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@TmJournalId", TmJournalId);
        param[1] = new SqlParameter("@TmTradeMarkId", TmTradeMarkId);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmJournalId"]);



        return retVal;


    }

    public int AddEditTmJournalDetails()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditTmJournalDetails";




        SqlParameter[] param = new SqlParameter[8];
        param[0] = new SqlParameter("@TmJournalDetailId", TmJournalDetailId);
        param[1] = new SqlParameter("@JournalDate", JournalDate);
        param[2] = new SqlParameter("@JournalNo", JournalNo);
        param[3] = new SqlParameter("@Comment", Comment);
        param[4] = new SqlParameter("@PurchaseDate", PurchaseDate);
        param[5] = new SqlParameter("@TmJournalId", TmJournalId);
        param[6] = new SqlParameter("@CustomerCode", CustomerCode);
        param[7] = new SqlParameter("@PageNo", PageNo);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmJournalDetailId"]);



        return retVal;


    }

    public int DeleteTmJournal()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteTmJournal";

        SqlParameter[] param = new SqlParameter[1];

        param[0] = new SqlParameter("@TmJournalId", TmJournalId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;


    }

    public DataSet getTmJournal()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "proc_getTmJournal";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@TmJournalId", TmJournalId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public DataSet getTmJournalDetails()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getTmJournalDetails";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@TmJournalId", TmJournalId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet SearchJournal()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "SearchJournal";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }



    public int AddEditTmRenewal()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditTmRenewal";





        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@TmRenewalId", TmRenewalId);
        param[1] = new SqlParameter("@TmTradeMarkId", TmTradeMarkId);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmRenewalId"]);



        return retVal;


    }

    public int AddEditTmRenewalDetails()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditTmRenewalDetails";




        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@TmRenewalDetailId", TmRenewalDetailId);
        param[1] = new SqlParameter("@RenewalDate", RenewalDate);
        param[2] = new SqlParameter("@Remark", Remark);
        param[3] = new SqlParameter("@TmRenewalId", TmRenewalId);
        param[4] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmRenewalDetailId"]);



        return retVal;


    }

    public int DeleteTmRenewal()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteTmRenewal";

        SqlParameter[] param = new SqlParameter[1];

        param[0] = new SqlParameter("@TmRenewalId", TmRenewalId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;


    }

    public DataSet getTmRenewal()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "proc_getTmRenewal";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@TmRenewalId", TmRenewalId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
     


    public DataSet getTmRenewalDetails()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getTmRenewalDetails";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@TmRenewalId", TmRenewalId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }


    public DataSet SearchRenewal()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "SearchRenewal";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }



    public int AddEditTmDocument()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditTmDocument";





        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@TmDocumentId", TmDocumentId);
        param[1] = new SqlParameter("@TmTradeMarkId", TmTradeMarkId);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmDocumentId"]);



        return retVal;


    }

    public int AddEditTmDocumentDetails()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditTmDocumentDetails";
        
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@TmDocumentDetailId", TmDocumentDetailId);
        param[1] = new SqlParameter("@Image", Image);
        param[2] = new SqlParameter("@Remark", Remark);
        param[3] = new SqlParameter("@TmDocumentId", TmDocumentId);
        param[4] = new SqlParameter("@CustomerCode", CustomerCode);
        param[5] = new SqlParameter("@IsShowtoClient", IsShowtoClient);
        
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmDocumentDetailId"]);



        return retVal;


    }

    public int DeleteTmDocument()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteTmDocument";

        SqlParameter[] param = new SqlParameter[1];

        param[0] = new SqlParameter("@TmDocumentId", TmDocumentId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;


    }

    public DataSet getTmDocument()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "proc_getTmDocument";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@TmDocumentId", TmDocumentId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }
    
    public DataSet getTmDocumentDetails()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getTmDocumentDetails";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@TmDocumentId", TmDocumentId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet SearchDocument()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "SearchDocument";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet SearchTmOpposition()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "SearchTmOpposition";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }

    public DataSet SearchTmOppositionDocument()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "SearchTmOppositionDocument";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataTable getCarPurchaseVendor()
    {
        DataSet ds = new DataSet();
       
        string sqlCommand = "getCarPurchaseVendor";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }

    public DataSet getCarPaymentDetails()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getCarPaymentDetails";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@CarPaymentId", CarPaymentId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public DataSet getCarPayment()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "proc_getCarPayment";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CarPaymentId", CarPaymentId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public int DeleteCarPayment()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCarPayment";

        SqlParameter[] param = new SqlParameter[1];

        param[0] = new SqlParameter("@CarPaymentId", CarPaymentId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;


    }
    public int AddEditCarPaymentDetail()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarPaymentDetail";



        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@CarPaymentDetailId", CarPaymentDetailId);
        param[1] = new SqlParameter("@CarPaymentId", CarPaymentId);
        param[2] = new SqlParameter("@VinId", VinId);
        param[3] = new SqlParameter("@CurrencyId1", CurrencyId1);
        param[4] = new SqlParameter("@Cost1", Cost1);

         param[5] = new SqlParameter("@ExchangeRate1", ExchangeRate1);
        param[6] = new SqlParameter("@TotalCost1", TotalCost1);
        param[7] = new SqlParameter("@CustomerCode", CustomerCode);
        param[8] = new SqlParameter("@PaymentMode", PaymentMode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarPaymentDetailId"]);



        return retVal;


    }


    public int AddEditCarPayment()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditCarPayment";

    



        SqlParameter[] param = new SqlParameter[8];
        param[0] = new SqlParameter("@CarPaymentId", CarPaymentId);
        param[1] = new SqlParameter("@CarPaymentDate", CarPaymentDate);
        param[2] = new SqlParameter("@VendorAcfileId", VendorAcfileId);

          param[3] = new SqlParameter("@CreditAcfileId", CreditAcfileId);
        param[4] = new SqlParameter("@PaymentMetod", PaymentMetod);
        param[5] = new SqlParameter("@TotalAmount", TotalAmount);
        param[6] = new SqlParameter("@Remark", Remark);
        param[7] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CarPaymentId"]);



        return retVal;


    }

    public DataTable getVINSerchForCarPurchase()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getVINSerchForCarPurchase";
        SqlParameter[] param = new SqlParameter[2];
        
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@VendorAcfileId", VendorAcfileId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public DataTable getVinPurchaseDetail()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getVinPurchaseDetail";
        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@PurchaseId", PurchaseId);
        param[2] = new SqlParameter("@CarPaymentDate", CarPaymentDate);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public int AddEditTmTrademarkConflict()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditTmTrademarkConflict";



        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@TmTrademarkConflictId", TmTrademarkConflictId);
        param[1] = new SqlParameter("@TrademarkId", TrademarkId);
        param[2] = new SqlParameter("@ConflictingTrademark", ConflictingTrademark);
        param[3] = new SqlParameter("@TmNo", TmNo);
        param[4] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
        param[5] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmTrademarkConflictId"]);



        return retVal;


    }
    public int AddEditTmTrademarkShowCauseDetails()
    {


        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditTmTrademarkShowCauseDetails";


  
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@TmShowCauseId", TmShowCauseId);
        param[1] = new SqlParameter("@ShowCauseRecDate", ShowCauseRecDate);
        param[2] = new SqlParameter("@ShowCauseReplyDate", ShowCauseReplyDate);
        param[3] = new SqlParameter("@TrademarkId", TrademarkId);
        param[4] = new SqlParameter("@CustomerCode", CustomerCode);
    

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmShowCauseId"]);



        return retVal;


    }
    public DataTable getOppositionByApplicationNum()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getOppositionByApplicationNum";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ApplicationNum", ApplicationNum);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
      public DataTable getTrademarkConflict()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getTrademarkConflict";
        SqlParameter[] param = new SqlParameter[2];        
        param[0] = new SqlParameter("@TrademarkId", TrademarkId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
      public DataTable getTradeMarkShowCauseDetails()
      {
          DataSet ds = new DataSet();
          string sqlCommand = "getTradeMarkShowCauseDetails";
          SqlParameter[] param = new SqlParameter[2];
          param[0] = new SqlParameter("@TrademarkId", TrademarkId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds.Tables[0];
      }


      public DataSet getRenewalSearch()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getRenewalSearch";
          SqlParameter[] param = new SqlParameter[3];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[1] = new SqlParameter("@RenewalDateFrom", RenewalDateFrom);
          param[2] = new SqlParameter("@RenewalDateTo", RenewalDateTo);
         
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

          
          return ds;
      }

      public DataSet getDebitNotGroup()
      {
          DataSet ds = new DataSet();
          string sqlCommand = "proc_getDebitNotGroup";
          SqlParameter[] param = new SqlParameter[5];
          param[0] = new SqlParameter("@DebitAcId", DebitAcId);
          param[1] = new SqlParameter("@DateFrom", DateFrom);
          param[2] = new SqlParameter("@DateTo", DateTo);        
          param[3] = new SqlParameter("@CustomerCode", CustomerCode);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


          return ds;
      }

      public DataSet getDebitNoteDetails()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getDebitNoteDetails";
          SqlParameter[] param = new SqlParameter[5];
          param[0] = new SqlParameter("@DebitAcId", DebitAcId);
          param[1] = new SqlParameter("@DateFrom", DateFrom);
          param[2] = new SqlParameter("@DateTo", DateTo);
          param[3] = new SqlParameter("@CustomerCode", CustomerCode);
        
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }
      public DataSet getDebitNoteReceivedAmount()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getDebitNoteReceivedAmount";
          SqlParameter[] param = new SqlParameter[5];
          param[0] = new SqlParameter("@AccountId", AccountId);
          param[1] = new SqlParameter("@DateFrom", DateFrom);
          param[2] = new SqlParameter("@DateTo", DateTo);
          param[3] = new SqlParameter("@CustomerCode", CustomerCode);
          param[4] = new SqlParameter("@RNumber", RNumber);
           
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }
      public DataSet getReminderSearch()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getReminderSearch";
          SqlParameter[] param = new SqlParameter[3];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[1] = new SqlParameter("@ReminderDateFrom", ReminderDateFrom);
          param[2] = new SqlParameter("@ReminderDateTo", ReminderDateTo);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


          return ds;
      }
      public DataSet getOppositionReminderSearch()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getOppositionReminderSearch";
          SqlParameter[] param = new SqlParameter[3];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[1] = new SqlParameter("@ReminderDateFrom", ReminderDateFrom);
          param[2] = new SqlParameter("@ReminderDateTo", ReminderDateTo);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


          return ds;
      }

      public DataSet getbasicResult()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getbasicResult";
          SqlParameter[] param = new SqlParameter[14];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);        
          param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);         
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[4] = new SqlParameter("@TmCountryId", TmCountryId);
          param[5] = new SqlParameter("@TmCityId", TmCityId);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@TradeMark", TradeMark);
          param[8] = new SqlParameter("@FillingDateFrom", FillingDateFrom);
          param[9] = new SqlParameter("@FillingDateTo", FillingDateTo);
          param[10] = new SqlParameter("@ClassFrom", ClassFrom);
          param[11] = new SqlParameter("@ClassTo", ClassTo);
          param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
          param[13] = new SqlParameter("@TmAgentId", TmAgentId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



          return ds;
      }

      public DataSet getbasicResultByTakeOverDate()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getbasicResultByTakeOverDate";
          SqlParameter[] param = new SqlParameter[14];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);        
          param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);         
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[4] = new SqlParameter("@TmCountryId", TmCountryId);
          param[5] = new SqlParameter("@TmCityId", TmCityId);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@TradeMark", TradeMark);
          param[8] = new SqlParameter("@TakeOverDateFrom", TakeOverDateFrom);
          param[9] = new SqlParameter("@TakeOverDateTo", TakeOverDateTo);
          param[10] = new SqlParameter("@ClassFrom", ClassFrom);
          param[11] = new SqlParameter("@ClassTo", ClassTo);
          param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
          param[13] = new SqlParameter("@TmAgentId", TmAgentId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



          return ds;
      }
      public DataSet getbasicResultDetailsByTakeOverDate()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getbasicResultDetailsByTakeOverDate";
          SqlParameter[] param = new SqlParameter[14];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);        
          param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);         
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[4] = new SqlParameter("@TmCountryId", TmCountryId);
          param[5] = new SqlParameter("@TmCityId", TmCityId);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@TradeMark", TradeMark);
          param[8] = new SqlParameter("@TakeOverDateFrom", TakeOverDateFrom);
          param[9] = new SqlParameter("@TakeOverDateTo", TakeOverDateTo);
          param[10] = new SqlParameter("@ClassFrom", ClassFrom);
          param[11] = new SqlParameter("@ClassTo", ClassTo);
          param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
          param[13] = new SqlParameter("@TmAgentId", TmAgentId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



          return ds;
      }


    
      public DataSet getbasicResultByAccRecDate()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getbasicResultByAccRecDate";
          SqlParameter[] param = new SqlParameter[14];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[4] = new SqlParameter("@TmCountryId", TmCountryId);
          param[5] = new SqlParameter("@TmCityId", TmCityId);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@TradeMark", TradeMark);
          param[8] = new SqlParameter("@AccRecDateFrom", AccRecDateFrom);
          param[9] = new SqlParameter("@AccRecDateTo", AccRecDateTo);
          param[10] = new SqlParameter("@ClassFrom", ClassFrom);
          param[11] = new SqlParameter("@ClassTo", ClassTo);
          param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
          param[13] = new SqlParameter("@TmAgentId", TmAgentId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



          return ds;
      }

      public DataSet getbasicResultDetailsByAccRecDate()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getbasicResultDetailsByAccRecDate";
          SqlParameter[] param = new SqlParameter[14];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[4] = new SqlParameter("@TmCountryId", TmCountryId);
          param[5] = new SqlParameter("@TmCityId", TmCityId);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@TradeMark", TradeMark);
          param[8] = new SqlParameter("@AccRecDateFrom", AccRecDateFrom);
          param[9] = new SqlParameter("@AccRecDateTo", AccRecDateTo);
          param[10] = new SqlParameter("@ClassFrom", ClassFrom);
          param[11] = new SqlParameter("@ClassTo", ClassTo);
          param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
          param[13] = new SqlParameter("@TmAgentId", TmAgentId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



          return ds;
      }

      public DataSet getbasicResultByDemandNoteDeposit()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getbasicResultByDemandNoteDeposit";
          SqlParameter[] param = new SqlParameter[14];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[4] = new SqlParameter("@TmCountryId", TmCountryId);
          param[5] = new SqlParameter("@TmCityId", TmCityId);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@TradeMark", TradeMark);
          param[8] = new SqlParameter("@DemandNoteDepositFrom", DemandNoteDepositFrom);
          param[9] = new SqlParameter("@DemandNoteDepositTo", DemandNoteDepositTo);
          param[10] = new SqlParameter("@ClassFrom", ClassFrom);
          param[11] = new SqlParameter("@ClassTo", ClassTo);
          param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
          param[13] = new SqlParameter("@TmAgentId", TmAgentId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



          return ds;
      }

      public DataSet getbasicResultDetailsByDemandNoteDeposit()
      {
          DataSet ds = new DataSet();
          string sqlCommand = "proc_getbasicResultDetailsByDemandNoteDeposit";
          SqlParameter[] param = new SqlParameter[14];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[4] = new SqlParameter("@TmCountryId", TmCountryId);
          param[5] = new SqlParameter("@TmCityId", TmCityId);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@TradeMark", TradeMark);
          param[8] = new SqlParameter("@DemandNoteDepositFrom", DemandNoteDepositFrom);
          param[9] = new SqlParameter("@DemandNoteDepositTo", DemandNoteDepositTo);
          param[10] = new SqlParameter("@ClassFrom", ClassFrom);
          param[11] = new SqlParameter("@ClassTo", ClassTo);
          param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
          param[13] = new SqlParameter("@TmAgentId", TmAgentId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }



      public DataSet getbasicResultByDemandNoteRecDate()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getbasicResultByDemandNoteRecDate";
          SqlParameter[] param = new SqlParameter[14];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[4] = new SqlParameter("@TmCountryId", TmCountryId);
          param[5] = new SqlParameter("@TmCityId", TmCityId);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@TradeMark", TradeMark);
          param[8] = new SqlParameter("@DemandNoteRecDateFrom", DemandNoteRecDateFrom);
          param[9] = new SqlParameter("@DemandNoteRecDateTo", DemandNoteRecDateTo);
          param[10] = new SqlParameter("@ClassFrom", ClassFrom);
          param[11] = new SqlParameter("@ClassTo", ClassTo);
          param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
          param[13] = new SqlParameter("@TmAgentId", TmAgentId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



          return ds;
      }

      public DataSet getbasicResultDetailsByDemandNoteRecDate()
      {
          DataSet ds = new DataSet();
          string sqlCommand = "proc_getbasicResultDetailsByDemandNoteRecDate";
          SqlParameter[] param = new SqlParameter[14];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[4] = new SqlParameter("@TmCountryId", TmCountryId);
          param[5] = new SqlParameter("@TmCityId", TmCityId);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@TradeMark", TradeMark);
          param[8] = new SqlParameter("@DemandNoteRecDateFrom", DemandNoteRecDateFrom);
          param[9] = new SqlParameter("@DemandNoteRecDateTo", DemandNoteRecDateTo);
          param[10] = new SqlParameter("@ClassFrom", ClassFrom);
          param[11] = new SqlParameter("@ClassTo", ClassTo);
          param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
          param[13] = new SqlParameter("@TmAgentId", TmAgentId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }
    













      public DataSet getbasicResultByShowCauseRecDate()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getbasicResultByShowCauseRecDate";
          SqlParameter[] param = new SqlParameter[14];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[4] = new SqlParameter("@TmCountryId", TmCountryId);
          param[5] = new SqlParameter("@TmCityId", TmCityId);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@TradeMark", TradeMark);
          param[8] = new SqlParameter("@ShowCauseRecDateFrom", ShowCauseRecDateFrom);
          param[9] = new SqlParameter("@ShowCauseRecDateTo ", ShowCauseRecDateTo);
          param[10] = new SqlParameter("@ClassFrom", ClassFrom);
          param[11] = new SqlParameter("@ClassTo", ClassTo);
          param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
          param[13] = new SqlParameter("@TmAgentId", TmAgentId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



          return ds;
      }
      public DataSet getbasicResultDetailsByShowCauseRecDate()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getbasicResultDetailsByShowCauseRecDate";
          SqlParameter[] param = new SqlParameter[14];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[4] = new SqlParameter("@TmCountryId", TmCountryId);
          param[5] = new SqlParameter("@TmCityId", TmCityId);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@TradeMark", TradeMark);
          param[8] = new SqlParameter("@ShowCauseRecDateFrom", ShowCauseRecDateFrom);
          param[9] = new SqlParameter("@ShowCauseRecDateTo ", ShowCauseRecDateTo);
          param[10] = new SqlParameter("@ClassFrom", ClassFrom);
          param[11] = new SqlParameter("@ClassTo", ClassTo);
          param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
          param[13] = new SqlParameter("@TmAgentId", TmAgentId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



          return ds;
      }



      public DataSet getbasicResultByShowCauseReplyDate()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getbasicResultByShowCauseReplyDate";
          SqlParameter[] param = new SqlParameter[14];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[4] = new SqlParameter("@TmCountryId", TmCountryId);
          param[5] = new SqlParameter("@TmCityId", TmCityId);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@TradeMark", TradeMark);
          param[8] = new SqlParameter("@ShowCauseReplyDateFrom", ShowCauseReplyDateFrom);
          param[9] = new SqlParameter("@ShowCauseReplyDateTo", ShowCauseReplyDateTo);
          param[10] = new SqlParameter("@ClassFrom", ClassFrom);
          param[11] = new SqlParameter("@ClassTo", ClassTo);
          param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
          param[13] = new SqlParameter("@TmAgentId", TmAgentId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



          return ds;
      }

      public DataSet getbasicResultDetailsByShowCauseReplyDate()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getbasicResultDetailsByShowCauseReplyDate";
          SqlParameter[] param = new SqlParameter[14];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[4] = new SqlParameter("@TmCountryId", TmCountryId);
          param[5] = new SqlParameter("@TmCityId", TmCityId);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@TradeMark", TradeMark);
          param[8] = new SqlParameter("@ShowCauseReplyDateFrom", ShowCauseReplyDateFrom);
          param[9] = new SqlParameter("@ShowCauseReplyDateTo", ShowCauseReplyDateTo);
          param[10] = new SqlParameter("@ClassFrom", ClassFrom);
          param[11] = new SqlParameter("@ClassTo", ClassTo);
          param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
          param[13] = new SqlParameter("@TmAgentId", TmAgentId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



          return ds;
      }



      public DataSet getbasicResultDetails()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getbasicResultDetails";
          SqlParameter[] param = new SqlParameter[14];
          param[0] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[1] = new SqlParameter("@ApplicationNum", ApplicationNum);
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[4] = new SqlParameter("@TmCountryId", TmCountryId);
          param[5] = new SqlParameter("@TmCityId", TmCityId);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@TradeMark", TradeMark);
          param[8] = new SqlParameter("@FillingDateFrom", FillingDateFrom);
          param[9] = new SqlParameter("@FillingDateTo", FillingDateTo);
          param[10] = new SqlParameter("@ClassFrom", ClassFrom);
          param[11] = new SqlParameter("@ClassTo", ClassTo);
          param[12] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
          param[13] = new SqlParameter("@TmAgentId", TmAgentId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



          return ds;
      }

      public DataSet getTmOppositionbasicResult()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getTmOppositionbasicResult";
          SqlParameter[] param = new SqlParameter[9];
          param[0] = new SqlParameter("@OppositionNo", OppositionNo);
          param[1] = new SqlParameter("@OppositionType", OppositionType);
          param[2] = new SqlParameter("@TmOppositionFileStatusId", TmOppositionFileStatusId);
          param[3] = new SqlParameter("@TmJounalNo", TmJounalNo);
          param[4] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[5] = new SqlParameter("@TradeMark", TradeMark);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          param[7] = new SqlParameter("@DateFrom", DateFrom);
          param[8] = new SqlParameter("@DateTo", DateTo);
          
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



          return ds;
      }

      public DataTable proc_getTradeMarkDetails()
      {
          DataSet ds = new DataSet();
          string sqlCommand = "proc_getTradeMarkDetails";
          SqlParameter[] param = new SqlParameter[4];
          param[0] = new SqlParameter("@TrademarkId", TrademarkId);
          param[1] = new SqlParameter("@CustomerCode", CustomerCode);
          param[2] = new SqlParameter("@ClassFrom", ClassFrom);
          param[3] = new SqlParameter("@ClassTo", ClassTo);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds.Tables[0];
      }


      public int DeleteTMForm()
      {
          int result = 0;
          DataSet ds = new DataSet();

          string sqlCommand = "DeleteTMForm";
          SqlParameter[] param = new SqlParameter[1];
          param[0] = new SqlParameter("@FormId", FormId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

          return result;

      }

      public DataSet GetTMForm()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "GetTMForm";

          SqlParameter[] param = new SqlParameter[2];

          param[0] = new SqlParameter("@CustomerCode", CustomerCode);
          param[1] = new SqlParameter("@FormId", FormId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


          return ds;
      }

      public int AddEditTMForm()
      {
          int retVal = 0;
          DataSet ds = new DataSet();

          string sqlCommand = "AddEditTMForm";
          SqlParameter[] param = new SqlParameter[5];
          param[0] = new SqlParameter("@FormId", FormId);
          param[1] = new SqlParameter("@FormName", FormName);
          param[2] = new SqlParameter("@CustomerCode", CustomerCode);
          param[3] = new SqlParameter("@Description", Description);
          param[4] = new SqlParameter("@Priorty", Priorty);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["FormId"]);


          return retVal;
      }





      public int AddEditTmFormMain()
      {
          int retVal = 0;
          DataSet ds = new DataSet();
          string sqlCommand = "AddEditTmFormMain";
          SqlParameter[] param = new SqlParameter[4];
          param[0] = new SqlParameter("@TmFormId", TmFormId);
          param[1] = new SqlParameter("@TmTradeMarkId", TmTradeMarkId);
          param[2] = new SqlParameter("@CustomerCode", CustomerCode);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmFormId"]);
          return retVal;
      }

      public int AddEditTmFormDetails()
      {


          int retVal = 0;
          DataSet ds = new DataSet();
          string sqlCommand = "AddEditTmFormDetails";
          SqlParameter[] param = new SqlParameter[7];
          param[0] = new SqlParameter("@TmFormDetailId", TmFormDetailId);
          param[1] = new SqlParameter("@FillingDate", FillingDate);        
          param[2] = new SqlParameter("@Comment", Comment);
          param[3] = new SqlParameter("@FormId", FormId);
          param[4] = new SqlParameter("@TmFormId", TmFormId);
          param[5] = new SqlParameter("@CustomerCode", CustomerCode);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmFormDetailId"]);

          return retVal;


      }

      public int DeleteTmFormEntries()
      {
          int result = 0;
          DataSet ds = new DataSet();

          string sqlCommand = "DeleteTmFormEntries";

          SqlParameter[] param = new SqlParameter[1];

          param[0] = new SqlParameter("@TmFormId", TmFormId);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

          return result;


      }

      public DataSet getTmFormMain()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getTmForm";

          SqlParameter[] param = new SqlParameter[2];

          param[0] = new SqlParameter("@TmFormId", TmFormId);
          param[1] = new SqlParameter("@CustomerCode", CustomerCode);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


          return ds;
      }

      public DataSet getTmFormDetails()
      {
          DataSet ds = new DataSet();
          string sqlCommand = "proc_getTmFormDetails";
          SqlParameter[] param = new SqlParameter[2];
          param[0] = new SqlParameter("@TmFormId", TmFormId);
          param[1] = new SqlParameter("@CustomerCode", CustomerCode);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }
      public DataSet SearchForm()
      {
          DataSet ds = new DataSet();
          string sqlCommand = "SearchForm";
          SqlParameter[] param = new SqlParameter[1];
          param[0] = new SqlParameter("@CustomerCode", CustomerCode);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }


















      public DataTable AddEditTmOpposition()
      {

          DataSet ds = new DataSet();
          string sqlCommand = "AddEditTmOpposition";

          SqlParameter[] param = new SqlParameter[38];
          param[0] = new SqlParameter("@TmOppositionId", TmOppositionId);
          param[1] = new SqlParameter("@OppositionNo", OppositionNo);
          param[2] = new SqlParameter("@Year", Year);
          param[3] = new SqlParameter("@OppositionType", OppositionType);
          param[4] = new SqlParameter("@OppositionStatus", OppositionStatus);
          param[5] = new SqlParameter("@OppositionRemark", OppositionRemark);
          param[6] = new SqlParameter("@TmJounalNo", TmJounalNo);
          param[7] = new SqlParameter("@JournalDate", JournalDate);
          param[8] = new SqlParameter("@PublicationDate", PublicationDate);
          param[9] = new SqlParameter("@Extension1", Extension1);
          param[10] = new SqlParameter("@Extension2", Extension2);


          param[11] = new SqlParameter("@LastDateOfOpposition", LastDateOfOpposition);
          param[12] = new SqlParameter("@TmCustomerId", TmCustomerId);
          param[13] = new SqlParameter("@ApplicationNum", ApplicationNum);
          param[14] = new SqlParameter("@TradeMark", TradeMark);
          param[15] = new SqlParameter("@TmFileStatusId", TmFileStatusId);
          param[16] = new SqlParameter("@OtherSideTmApplicationNum", OtherSideTmApplicationNum);
          param[17] = new SqlParameter("@OtherSideTmTradeMark", OtherSideTmTradeMark);
          param[18] = new SqlParameter("@OtherSideTmClass", OtherSideTmClass);
          param[19] = new SqlParameter("@OtherSideTmFilledOn", OtherSideTmFilledOn);
          param[20] = new SqlParameter("@OtherSideApplicationUser", OtherSideApplicationUser);


          param[21] = new SqlParameter("@OthersideClientName", OthersideClientName);
          param[22] = new SqlParameter("@OtherSideClientAddress", OtherSideClientAddress);
          param[23] = new SqlParameter("@OtherSideClientAdvocateName", OtherSideClientAdvocateName);
          param[24] = new SqlParameter("@OtherSideClientAdvocateAddress", OtherSideClientAdvocateAddress);
          param[25] = new SqlParameter("@CustomerCode", CustomerCode);
          param[26] = new SqlParameter("@Reminder", Reminder);
          param[27] = new SqlParameter("@ReminderDate", ReminderDate);
          param[28] = new SqlParameter("@TmOppositionFileStatusId", TmOppositionFileStatusId);


          param[29] = new SqlParameter("@OtherSideJournalNo", OtherSideJournalNo);
          param[30] = new SqlParameter("@OtherSideMemoNumber", OtherSideMemoNumber);

          param[31] = new SqlParameter("@Goods", Goods);


          param[32] = new SqlParameter("@OtherSideGoods", OtherSideGoods);
          param[33] = new SqlParameter("@PeriodOfuse", PeriodOfuse);


          param[34] = new SqlParameter("@FileNo", FileNo);
          param[35] = new SqlParameter("@OppositionDate", OppositionDate);

          param[36] = new SqlParameter("@OtherSideJournalDate", OtherSideJournalDate);
          param[37] = new SqlParameter("@OtherSidePublicationDate", OtherSidePublicationDate);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds.Tables[0];
      }



      public int AddEditTmOppositionDetails()
      {

          int retVal = 0;
          DataSet ds = new DataSet();
          string sqlCommand = "AddEditTmOppositionDetails";
          SqlParameter[] param = new SqlParameter[7];
          param[0] = new SqlParameter("@TmOppositionDetailId", TmOppositionDetailId);
          param[1] = new SqlParameter("@TmOppositionId", TmOppositionId);
          param[2] = new SqlParameter("@DetailsDate", DetailsDate);
          param[3] = new SqlParameter("@DetailsRemark", DetailsRemark);
          param[4] = new SqlParameter("@CustomerCode", CustomerCode);      

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmOppositionDetailId"]);

          return retVal;


      }


      public DataSet getTmOpposition()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getTmOpposition";

          SqlParameter[] param = new SqlParameter[2];

          param[0] = new SqlParameter("@TmOppositionId", TmOppositionId);
          param[1] = new SqlParameter("@CustomerCode", CustomerCode);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


          return ds;
      }

      public DataSet getTmOppositionDetails()
      {
          DataSet ds = new DataSet();
          string sqlCommand = "proc_getTmOppositionDetails";
          SqlParameter[] param = new SqlParameter[2];
          param[0] = new SqlParameter("@TmOppositionId", TmOppositionId);
          param[1] = new SqlParameter("@CustomerCode", CustomerCode);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }

      public int DeleteTmOpposition()
      {
          int result = 0;
          DataSet ds = new DataSet();

          string sqlCommand = "DeleteTmOpposition";
          SqlParameter[] param = new SqlParameter[1];
          param[0] = new SqlParameter("@TmOppositionId", TmOppositionId);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

          return result;

      }

      public int AddEditTmOppositionHearingDetails()
      {
          int retVal = 0;
          DataSet ds = new DataSet();
          string sqlCommand = "AddEditTmOppositionHearingDetails";

          SqlParameter[] param = new SqlParameter[7];
          param[0] = new SqlParameter("@TmOppositionHearingDetailId", TmOppositionHearingDetailId);
          param[1] = new SqlParameter("@TmOppositionId", TmOppositionId);
          param[2] = new SqlParameter("@Before", Before);
          param[3] = new SqlParameter("@Comment", Comment);
          param[5] = new SqlParameter("@HearingDate", HearingDate);
          param[6] = new SqlParameter("@CustomerCode", CustomerCode);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmOppositionHearingDetailId"]);
          return retVal;
      }
      public DataSet getTmOppositionHearingDetails()
      {
          DataSet ds = new DataSet();
          string sqlCommand = "proc_getTmOppositionHearingDetails";
          SqlParameter[] param = new SqlParameter[2];
          param[0] = new SqlParameter("@TmOppositionId", TmOppositionId);
          param[1] = new SqlParameter("@CustomerCode", CustomerCode);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }



      public int AddEditTmOppositionDocument()
      {


          int retVal = 0;
          DataSet ds = new DataSet();
          string sqlCommand = "AddEditTmOppositionDocument";





          SqlParameter[] param = new SqlParameter[4];
          param[0] = new SqlParameter("@TmOppositionDocumentId", TmOppositionDocumentId);
          param[1] = new SqlParameter("@TmOppositionId", TmOppositionId);
          param[2] = new SqlParameter("@CustomerCode", CustomerCode);


          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmOppositionDocumentId"]);



          return retVal;


      }

      public int AddEditTmOppositionDocumentDetails()
      {


          int retVal = 0;
          DataSet ds = new DataSet();
          string sqlCommand = "AddEditTmOppositionDocumentDetails";

          SqlParameter[] param = new SqlParameter[6];
          param[0] = new SqlParameter("@TmOppositionDocumentDetailId", TmOppositionDocumentDetailId);
          param[1] = new SqlParameter("@Image", Image);
          param[2] = new SqlParameter("@Remark", Remark);
          param[3] = new SqlParameter("@TmOppositionDocumentId", TmOppositionDocumentId);
          param[4] = new SqlParameter("@CustomerCode", CustomerCode);
          param[5] = new SqlParameter("@IsShowtoClient", IsShowtoClient);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmOppositionDocumentDetailId"]);



          return retVal;


      }

      public int DeleteTmOppositionDocument()
      {
          int result = 0;
          DataSet ds = new DataSet();

          string sqlCommand = "DeleteTmOppositionDocument";

          SqlParameter[] param = new SqlParameter[1];

          param[0] = new SqlParameter("@TmOppositionDocumentId", TmOppositionDocumentId);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

          return result;


      }

      public DataSet getTmOppositionDocument()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getTmOppositionDocument";

          SqlParameter[] param = new SqlParameter[2];

          param[0] = new SqlParameter("@TmOppositionDocumentId", TmOppositionDocumentId);
          param[1] = new SqlParameter("@CustomerCode", CustomerCode);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


          return ds;
      }

      public DataSet getTmOppositionDocumentDetails()
      {
          DataSet ds = new DataSet();
          string sqlCommand = "proc_getTmOppositionDocumentDetails";
          SqlParameter[] param = new SqlParameter[2];
          param[0] = new SqlParameter("@TmOppositionDocumentId", TmOppositionDocumentId);
          param[1] = new SqlParameter("@CustomerCode", CustomerCode);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }




      public int AddEditTmOppositionFormMain()
      {
          int retVal = 0;
          DataSet ds = new DataSet();
          string sqlCommand = "AddEditTmOppositionFormMain";
          SqlParameter[] param = new SqlParameter[4];
          param[0] = new SqlParameter("@TmOppositionFormId", TmOppositionFormId);
          param[1] = new SqlParameter("@TmOppositionId", TmOppositionId);
          param[2] = new SqlParameter("@CustomerCode", CustomerCode);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmOppositionFormId"]);
          return retVal;
      }

      public int AddEditTmOppositionFormDetails()
      {


          int retVal = 0;
          DataSet ds = new DataSet();
          string sqlCommand = "AddEditTmOppositionFormDetails";
          SqlParameter[] param = new SqlParameter[7];
          param[0] = new SqlParameter("@TmOppositionFormDetailId", TmOppositionFormDetailId);
          param[1] = new SqlParameter("@FillingDate", FillingDate);
          param[2] = new SqlParameter("@Comment", Comment);
          param[3] = new SqlParameter("@FormId", FormId);
          param[4] = new SqlParameter("@TmOppositionFormId", TmOppositionFormId);
          param[5] = new SqlParameter("@CustomerCode", CustomerCode);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["TmOppositionFormDetailId"]);

          return retVal;


      }

      public int DeleteTmOppositionFormEntries()
      {
          int result = 0;
          DataSet ds = new DataSet();

          string sqlCommand = "DeleteTmOppositionFormEntries";

          SqlParameter[] param = new SqlParameter[1];

          param[0] = new SqlParameter("@TmOppositionFormId", TmOppositionFormId);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

          return result;


      }

      public DataSet getTmOppositionFormMain()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getTmOppositionForm";

          SqlParameter[] param = new SqlParameter[2];

          param[0] = new SqlParameter("@TmOppositionFormId", TmOppositionFormId);
          param[1] = new SqlParameter("@CustomerCode", CustomerCode);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


          return ds;
      }

      public DataSet getTmOppositionFormDetails()
      {
          DataSet ds = new DataSet();
          string sqlCommand = "proc_getTmOppositionFormDetails";
          SqlParameter[] param = new SqlParameter[2];
          param[0] = new SqlParameter("@TmOppositionFormId", TmOppositionFormId);
          param[1] = new SqlParameter("@CustomerCode", CustomerCode);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }
      public DataTable getTradeMarkByTradeMark()
      {
          DataSet ds = new DataSet();
          string sqlCommand = "proc_getTradeMarkByTradeMark";
          SqlParameter[] param = new SqlParameter[2];
          param[0] = new SqlParameter("@ApplicationNum", ApplicationNum);
          param[1] = new SqlParameter("@CustomerCode", CustomerCode);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds.Tables[0];
      }

    


      public DataSet SearchTmOppositionForm()
      {
          DataSet ds = new DataSet();
          string sqlCommand = "SearchTmOppositionForm";
          SqlParameter[] param = new SqlParameter[1];
          param[0] = new SqlParameter("@CustomerCode", CustomerCode);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }





























      public int AddEditTmMonthlyJournal()
      {


          int retVal = 0;
          DataSet ds = new DataSet();
          string sqlCommand = "AddEditTmMonthlyJournal";
          SqlParameter[] param = new SqlParameter[7];
          param[0] = new SqlParameter("@Id", Id);
          param[1] = new SqlParameter("@JournalDate", JournalDate);
          param[2] = new SqlParameter("@JournalNo", JournalNo);
          param[3] = new SqlParameter("@PageNo", JournalPageNo);
          param[4] = new SqlParameter("@TradeMark", TradeMark);
          param[5] = new SqlParameter("@CustomerCode", CustomerCode);
          param[6] = new SqlParameter("@Class", Classes);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);

          return retVal;


      }

     public int DeleteTmMonthlyJournal()
      {
          int result = 0;
          DataSet ds = new DataSet();

          string sqlCommand = "DeleteTmMonthlyJournal";

          SqlParameter[] param = new SqlParameter[1];

          param[0] = new SqlParameter("@Id", Id);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

          return result;


      }

      public DataSet GetTmMonthlyJournal()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "GetTmMonthlyJournal";

          SqlParameter[] param = new SqlParameter[2];
          param[0] = new SqlParameter("@Id", Id);
          param[1] = new SqlParameter("@CustomerCode", CustomerCode);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }
      public DataSet SearchTmMonthlyJournal()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "SearchTmMonthlyJournal";

          SqlParameter[] param = new SqlParameter[3];
          param[0] = new SqlParameter("@JournalNo", JournalNo);
          param[1] = new SqlParameter("@TradeMark", TradeMark);
          param[2] = new SqlParameter("@CustomerCode", CustomerCode);

          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }


      public DataSet getTmMonthlyJournalReportPrefix()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getTmMonthlyJournalReportPrefix";
          SqlParameter[] param = new SqlParameter[4];
          param[0] = new SqlParameter("@JournalNo", JournalNo);
          param[1] = new SqlParameter("@NoOfCharacter", NoOfCharacter);
          param[2] = new SqlParameter("@CustomerCode", CustomerCode);
          param[3] = new SqlParameter("@ClientId", ClientId);      
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }
      public DataSet getTmMonthlyManualSearch()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getTmMonthlyManualSearch";
          SqlParameter[] param = new SqlParameter[3];
          param[0] = new SqlParameter("@JournalNo", JournalNo);
          param[1] = new SqlParameter("@SearchText", SearchText);
          param[2] = new SqlParameter("@CustomerCode", CustomerCode);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }

      public DataSet getTmMonthlyManualSearchByApplication()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getTmMonthlyManualSearchByApplication ";
          SqlParameter[] param = new SqlParameter[3];
          param[0] = new SqlParameter("@PageNo", ApplicationNo);          
          param[1] = new SqlParameter("@CustomerCode", CustomerCode);
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }
      public DataSet getTmMonthlyJournalReportPostFix()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getTmMonthlyJournalReportPostFix";
          SqlParameter[] param = new SqlParameter[4];
          param[0] = new SqlParameter("@JournalNo", JournalNo);
          param[1] = new SqlParameter("@NoOfCharacter", NoOfCharacter);
          param[2] = new SqlParameter("@CustomerCode", CustomerCode);
          param[3] = new SqlParameter("@ClientId", ClientId);      
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }


      public DataSet getTmMonthlyJournalReportEqual()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getTmMonthlyJournalReportEqual";
          SqlParameter[] param = new SqlParameter[3];
          param[0] = new SqlParameter("@JournalNo", JournalNo);        
          param[1] = new SqlParameter("@CustomerCode", CustomerCode);
          param[2] = new SqlParameter("@ClientId", ClientId);      
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }
      public DataSet getTmMonthlyJournalReportAll()
      {
          DataSet ds = new DataSet();

          string sqlCommand = "proc_getTmMonthlyJournalReportAll";
          SqlParameter[] param = new SqlParameter[3];
          param[0] = new SqlParameter("@JournalNo", JournalNo);
          param[1] = new SqlParameter("@CustomerCode", CustomerCode); 
          param[2] = new SqlParameter("@ClientId", ClientId);      
          ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
          return ds;
      }


    
}