﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UpdateSession
/// </summary>
public class UpdateSession
{
    clsCookie ck = new clsCookie();
	public UpdateSession()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public void UpdateSessions()
    {
        if (ck.GetCookieValue("CustomerCode") != "")
        {
            Sessions.UserId = int.Parse(Convert.ToString(ck.GetCookieValue("UserId")));
            Sessions.UseLoginName = Convert.ToString(ck.GetCookieValue("UseLoginName"));
            Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            Sessions.CustomerName = Convert.ToString(ck.GetCookieValue("CustomerName"));
            Sessions.CityName = Convert.ToString(ck.GetCookieValue("CityName"));
            Sessions.CountryName = Convert.ToString(ck.GetCookieValue("CountryName"));
            Sessions.FromDate = Convert.ToString(ck.GetCookieValue("FromDate"));
            Sessions.ToDate = Convert.ToString(ck.GetCookieValue("ToDate"));
            Sessions.IndId = Convert.ToString(ck.GetCookieValue("IndId"));  
            Sessions.UserRole = Convert.ToString(ck.GetCookieValue("UserRole"));
        }

    }

    public void UpdateSessionsClient()    
    {
        if (ck.GetCookieValue("CustomerCode") != "")
        {
            Sessions.TmCustomerId = int.Parse(Convert.ToString(ck.GetCookieValue("TmCustomerId")));
            Sessions.UseLoginName = Convert.ToString(ck.GetCookieValue("TmCustomerName"));
            Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            Sessions.CustomerName = Convert.ToString(ck.GetCookieValue("CustomerName"));
            Sessions.CityName = Convert.ToString(ck.GetCookieValue("CityName"));
            Sessions.CountryName = Convert.ToString(ck.GetCookieValue("CountryName"));
            Sessions.FromDate = Convert.ToString(ck.GetCookieValue("FromDate"));
            Sessions.ToDate = Convert.ToString(ck.GetCookieValue("ToDate"));
            Sessions.IndId = Convert.ToString(ck.GetCookieValue("IndId"));
            Sessions.UserRole = Convert.ToString(ck.GetCookieValue("UserRole"));
        }

    }
}