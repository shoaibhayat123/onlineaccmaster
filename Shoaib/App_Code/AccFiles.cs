﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;


/// <summary>
/// Summary description for AccFiles
/// </summary>
public class AccFiles
{
    #region Data Members
    public string Title,EmailId, PhoneNo, OpeningBalType, Address1, Address2, Address3, PanNo, InvoiceNo, SalesTaxNo, DataFrom, DebitCredit;
    public int id, ParrentId, CreditDays, AcFileId, InvoiceID, InvoiceSummaryId;
    public Nullable<int> Creditperiod;
    public decimal CustomerCode, Amount;
    public DateTime InvoiceDate;
    public Nullable<decimal> OpeningBal;
    int retVal = 0;
    #endregion
	public AccFiles()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public DataTable getTreeData()
    {
        DataSet ds1 = new DataSet();
        DataTable dt = new DataTable();
       
            string sqlCommand = "getTreeData";
            
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@id", id);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);     
                 
            dt=ds1.Tables[0];

       
       
        return dt;
    }
    public DataTable getMenueDetail()
    {
        DataSet ds1 = new DataSet();
        DataTable dt = new DataTable();
         string sqlCommand = "getMenueDetail";        

            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@id", id);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);  
           
            dt = ds1.Tables[0];

       
        
        return dt;
    }
    public DataTable CheckCustomer()
    {
        DataSet ds1 = new DataSet();
        DataTable dt = new DataTable();
       
            string sqlCommand = "CheckCustomer";
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@id", id);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param); 
            dt = ds1.Tables[0];

        
       
        return dt;
    }

    #region AddEditSubMenu
    public DataTable PositionSundryDebitors()
    {
        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "proc_PositionSundryDebitors";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@id", id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param); 
        return ds.Tables[0];
    }

    public DataTable AddEditSubMenu()
    {
        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditSubMenu";       
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@Title", Title);
        param[2] = new SqlParameter("@ParrentId", ParrentId);
        param[3] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param); 
        return ds.Tables[0];
    }


    public DataTable AddEditLedger()
    {
        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "AddEditLedger";

        SqlParameter[] param = new SqlParameter[14];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@Title", Title);
        param[2] = new SqlParameter("@ParrentId", ParrentId);
        param[3] = new SqlParameter("@CustomerCode", CustomerCode);
        param[4] = new SqlParameter("@PhoneNo", PhoneNo);
        param[5] = new SqlParameter("@Address1", Address1);
        param[6] = new SqlParameter("@Address2", Address2);
        param[7] = new SqlParameter("@Address3", Address3);
        param[8] = new SqlParameter("@OpeningBal", OpeningBal);
        param[9] = new SqlParameter("@OpeningBalType", OpeningBalType);
        param[10] = new SqlParameter("@Creditperiod", Creditperiod);
        param[11] = new SqlParameter("@PanNo", PanNo);
        param[12] = new SqlParameter("@SalesTaxNo", SalesTaxNo);
        param[13] = new SqlParameter("@EmailId", EmailId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param); 

        return ds.Tables[0];
    }
    #endregion

    #region deleteGroup
    public DataTable deleteGroup()
    {

            int retVal = 0;
            DataSet ds = new DataSet();
            string sqlCommand = "proc_deleteGroup";
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@id", id);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);        
           // retVal=Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
            return ds.Tables[0];


    }
    #endregion
    public DataTable AddeditInvoice()
    {
        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "Proc_AddeditInvoice";     


        SqlParameter[] param = new SqlParameter[11];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@InvoiceDate", InvoiceDate);
        param[2] = new SqlParameter("@InvoiceNo", InvoiceNo);
        param[3] = new SqlParameter("@CreditDays", CreditDays);
        param[4] = new SqlParameter("@Amount", Amount);
        param[5] = new SqlParameter("@AcFileId", AcFileId);
        param[6] = new SqlParameter("@CustomerCode", CustomerCode);
        param[7] = new SqlParameter("@DebitCredit", DebitCredit);
        param[8] = new SqlParameter("@InvoiceID", InvoiceID);
        param[9] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[10] = new SqlParameter("@DataFrom", DataFrom);
       
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param); 


        return ds.Tables[0];
    }
    public DataTable BoutiqueAddeditInvoice()
    {
        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "Proc_BoutiqueAddeditInvoice";


        SqlParameter[] param = new SqlParameter[11];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@InvoiceDate", InvoiceDate);
        param[2] = new SqlParameter("@InvoiceNo", InvoiceNo);
        param[3] = new SqlParameter("@CreditDays", CreditDays);
        param[4] = new SqlParameter("@Amount", Amount);
        param[5] = new SqlParameter("@AcFileId", AcFileId);
        param[6] = new SqlParameter("@CustomerCode", CustomerCode);
        param[7] = new SqlParameter("@DebitCredit", DebitCredit);
        param[8] = new SqlParameter("@InvoiceID", InvoiceID);
        param[9] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[10] = new SqlParameter("@DataFrom", DataFrom);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }
    public int deleteInvoice()
    {

        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "Proc_deleteInvoice";       
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);        
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["id"]);
        return retVal;


    }

    public int deleteInvoiceFromInvoiceTax()
    {

        int retVal = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "Proc_deleteInvoiceFromInvoiceTax";      
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);      
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["id"]);
        return retVal;


    }



    public DataTable getInvoice()
    {
        DataSet ds1 = new DataSet();
        DataTable dt = new DataTable();
       
            string sqlCommand = "Proc_getInvoice";
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@id", id);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);       
            dt = ds1.Tables[0];

        return dt;
    }
    public DataTable getDebitCredit()
    {
        DataSet ds1 = new DataSet();
        DataTable dt = new DataTable();
       
            string sqlCommand = "proc_getDebitCredit ";          
            SqlParameter[] param = new SqlParameter[1];          
            param[0] = new SqlParameter("@CustomerCode", CustomerCode);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);       
         
            dt = ds1.Tables[0];

        
        return dt;
    }


    public DataTable getTreeLevel()
    {
        DataSet ds1 = new DataSet();
        DataTable dt = new DataTable();
      
            string sqlCommand = "proc_getTreeLevel ";
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@id", id);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);     
            dt = ds1.Tables[0];

        
       
        return dt;
    }

}
