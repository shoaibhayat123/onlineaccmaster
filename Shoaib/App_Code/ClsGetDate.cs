﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for ClsGetDate
/// </summary>
public class ClsGetDate
{
   
	public ClsGetDate()
	{
		//
		// TODO: Add constructor logic here
		//

   
	}

    public static string FillFromDate(string date)
    {
        clsCookie ck = new clsCookie();
       string FromDate = "";
       if (Convert.ToString(Sessions.CustomerCode) == "")
       {
           if (ck.GetCookieValue("CustomerCode") != "")
           {
               Sessions.UserId = int.Parse(Convert.ToString(ck.GetCookieValue("UserId")));
               Sessions.UseLoginName = Convert.ToString(ck.GetCookieValue("UseLoginName"));
               Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
               Sessions.CustomerName = Convert.ToString(ck.GetCookieValue("CustomerName"));
               Sessions.CityName = Convert.ToString(ck.GetCookieValue("CityName"));
               Sessions.CountryName = Convert.ToString(ck.GetCookieValue("CountryName"));
               Sessions.FromDate = Convert.ToString(ck.GetCookieValue("FromDate"));
               Sessions.ToDate = Convert.ToString(ck.GetCookieValue("ToDate"));



           }
        
       }

        if (Convert.ToString( Sessions.CustomerCode) != "")
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@CustomerCode", Convert.ToDecimal(Sessions.CustomerCode));            
            ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GetDateFormat", param);
            if (ds.Tables[0].Rows.Count > 0)
            {
                FromDate = Convert.ToDateTime(date).ToString(ds.Tables[0].Rows[0]["Dateformat"].ToString());
            }

        }

        return FromDate;
    }
}
