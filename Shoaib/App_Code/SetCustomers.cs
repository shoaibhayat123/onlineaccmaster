﻿#region Namespaces...

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

#endregion

#region SetCustomers



/// <summary>
/// Summary description for SetCustomers
/// </summary>
public class SetCustomers
{

    #region sqlnew
    SqlConnection con;
    SqlCommand cm;
    SqlDataAdapter da;
    DataTable dt;
    String conStr = ConfigurationManager.ConnectionStrings["ConnectionAccMasterMaster"].ToString();
    #endregion

    #region Data Members

    public string DataFrom, Categoryname,TransporterName, CompanyName, Address3, TelephoneNumber, NIC, NTN, GST, PurchaseType, CountryName, BoutiqueItemDescription, ArticleName, ArticalType, BoutiqueItemCode, CompanyLogo, CountryLang, UserName, UserPassword, FirstName, Description, IndustrialName, LastName, DateOfBirth, Email, Address1, Address2, City, ContactNo, Zipcode, State, Counrty, Gender, CustomerName, ContactPerson, AddressLine1, AddressLine2, AddressLine3, Country, PhoneNumber, CellNumber, PrimaryEmail, SecondryEmail, SalesTaxNumber, NationalTaxNumber, AccountBlocked, Currency, NewUserID, MainCategoryName, CostCenterName;
    public int ClientAccountId,CategoryId, ClientAcfileId,id, ACFileID, ArticleNameId, ArticalTypeId, CountryId, BoutiqueUserId, BrandId, SizeId, ColorId, BusinessTypeId, SubCatid, CatId, Position, IsActive, SubCategoryCode, IsEdit, ItemId, ProvinceId, UserId, RoleId, CreatedBy, ItemSetupCostCenterId, CustomerId, userid, IndustrialTypeId, AccountId, MainCategoryCode, CostCenterCode;
    public string Dateformat, Address, Size, Password, DesignerName, MobileNo, ItemCode, SizeAddress, Color, Title, SubCatTitle, PageName, SubcategoryName, CurrencySymbol, CurrencyDesc, ItemDesc, Unit, Packing, BarcodeNo;
    public Nullable<decimal>MinOrderLevel, price,Discount, BoutiquePurchaseDiscount, BoutiqueSalesDiscount, CustomerCode, Userid_SubMenu, SetupCharges, MonthlyCharges, OpeningQuty, NumberOfUsers, PurchaseRate, SaleRate, SalesTax, DutyNumber, Opbal;
    public Nullable<DateTime> AccountOpeningDate, AccountExpiryDate, AccountFromDate, AccountToDate;
    public Boolean IsActiveted, IsHeader, IsInvetory;
    int retVal = 0;
    #endregion

    public SetCustomers()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #region Set IndustrialType
    public DataTable SetIndustrialType()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "SetIndustrialType";


        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@IndId", IndustrialTypeId);
        param[1] = new SqlParameter("@IndustrialName", IndustrialName);
        param[2] = new SqlParameter("@Description", Description);
        param[3] = new SqlParameter("@IsActive", IsActive);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



        return ds.Tables[0];

    }
    #endregion

    #region AccountYear
    #region Set AccountingYearSetup
    public int SetAccountingYearSetup()
    {
        int retVal = 0;
        DataSet ds = new DataSet();


        string sqlCommand = "SetAccountingYearSetup";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@AccId", AccountId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@FromDate", AccountFromDate);
        param[3] = new SqlParameter("@ToDate", AccountToDate);
        param[4] = new SqlParameter("@IsActive", IsActiveted);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["AccId"]);





        return retVal;

    }
    #endregion
    #region get AccountyearsetupActive
    public void GetAccountyearsetupActive()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "proc_getAccountyearsetupActive";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        Sessions.FromDate = ds.Tables[0].Rows[0]["FromDate"].ToString();
        Sessions.ToDate = ds.Tables[0].Rows[0]["ToDate"].ToString();


    }
    #endregion
    #region  Update Date Format
    public void UpdateDateFormat()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "UpdateDateFormat";

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@Dateformat", Dateformat);
        param[2] = new SqlParameter("@CompanyLogo", CompanyLogo);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);




    }
    public void UpdateHeader()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "UpdateHeader";

        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@IsHeader", IsHeader);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);




    }

    #endregion
    #region Get Accounting Year Setup
    public DataSet GetAccountingYearSetup()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetAccountingYearSetup";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@AccId", AccountId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }
    #endregion
    #region Delete Accounting Year Setup
    public int DeleteAccountingYearSetup()
    {
        int retVal = 0;
        DataSet ds = new DataSet();


        string sqlCommand = "DeleteAccountingYearSetup";

        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@AccId", AccountId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["AccId"]);

        return retVal;

    }
    #endregion
    #endregion
    #region Get IndustrialType
    public DataSet GetIndustrialType()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetIndustrialType";

        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@IndId", IndustrialTypeId);
        param[1] = new SqlParameter("@IsActive", IsActive);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



        return ds;
    }
    public DataSet GetIndustrialTypeActive()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetIndustrialTypeActive";

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@IndId", IndustrialTypeId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



        return ds;
    }



    #endregion


    #region Get Date Format
    public DataSet GetDateFormat()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetDateFormat";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);




        return ds;
    }
    #endregion

    #region HambalGST
   
    public int AddEditHambalGSTCompanies()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditHambalGSTCompanies";
        SqlParameter[] param = new SqlParameter[10];
        param[0] = new SqlParameter("@id", id);      
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@CompanyName", CompanyName);
        param[3] = new SqlParameter("@Address1", Address1);
        param[4] = new SqlParameter("@Address2", Address2);
        param[5] = new SqlParameter("@Address3", Address3);
        param[6] = new SqlParameter("@TelephoneNumber", TelephoneNumber);
        param[7] = new SqlParameter("@NIC", NIC);
        param[8] = new SqlParameter("@NTN", NTN);
        param[9] = new SqlParameter("@GST", GST);       
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["id"]);

        return retVal;
    }
    public DataSet GetHambalGSTCompanies()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetHambalGSTCompanies";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
                return ds;
    }
    public int DeleteHambalGSTCompanies()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteHambalGSTCompanies";

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@id", id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);


        return result;

    }
  
    #endregion


    #region Hambal Customer Discount Setup

    public int AddEditHambalCustomerDiscountSetup()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditHambalCustomerDiscountSetup";
        SqlParameter[] param = new SqlParameter[10];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@ClientAcfileId", ClientAcfileId);
        param[3] = new SqlParameter("@Discount", Discount);
       
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["id"]);

        return retVal;
    }
    public DataSet GetHambalCustomerDiscountSetup()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetHambalCustomerDiscountSetup";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public int DeleteHambalCustomerDiscountSetup()
    {
        int result = 0;
        DataSet ds = new DataSet();
        string sqlCommand = "DeleteHambalCustomerDiscountSetup";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@id", id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);
        return result;
    }

    #endregion

    #region HambalTransporter

    public int AddEditHambalTransporterSetup()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditHambalTransporterSetup";
        SqlParameter[] param = new SqlParameter[10];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@TransporterName", TransporterName);
      
        param[3] = new SqlParameter("@TelephoneNumber", TelephoneNumber);
       
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["id"]);

        return retVal;
    }
    public DataSet GetHambalTransporterSetup()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetHambalTransporterSetup";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds;
    }
    public int DeleteHambalTransporterSetup()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteHambalTransporterSetup";

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@id", id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);


        return result;

    }

    #endregion
    #region MainCategory
    #region Set MainCategory
    public int SetMainCategory()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "SetMainCategory";


        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        param[1] = new SqlParameter("@MainCategoryName", MainCategoryName);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        //  param[3] = new SqlParameter("@ACFileID", ACFileID);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["MainCategoryCode"]);

        return retVal;
    }
    #endregion

    #region Set hambal price MainCategory
    public int AddEdithumbalpricelistcategorySetup()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEdithumbalpricelistcategorySetup";


        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CategoryId", CategoryId);
        param[1] = new SqlParameter("@Categoryname", Categoryname);
       
      
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CategoryId"]);

        return retVal;
    }
    #endregion

    #region Set BoutiqueMainCategory
    public int BoutiqueSetMainCategory()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "BoutiqueSetMainCategory";


        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        param[1] = new SqlParameter("@MainCategoryName", MainCategoryName);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@ACFileID", ACFileID);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["MainCategoryCode"]);

        return retVal;
    }
    #endregion


    #region Get MainCategory
    public DataSet GetMainCategory()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetMainCategory";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);




        return ds;
    }
    #endregion
    #region Get hambal MainCategory
    public DataSet GetHambalMainCategory()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetHambalMainCategory";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@CategoryId", CategoryId);
      
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);




        return ds;
    }
    #endregion
    #region Get MainCategory
    public DataSet GethumbalPricelistcategory()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GethumbalPricelistcategory";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@CategoryId", CategoryId);
      
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }
    #endregion

    #region Delete MainCategory
    public int DeleteMainCategory()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteMainCategoryCode";

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);


        return result;

    }
    #endregion
    #region Delete Price Category
    public int DeletehumbalpricelistcategorySetup()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeletehumbalpricelistcategorySetup";

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CategoryId", CategoryId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);


        return result;

    }
    #endregion

    
    #endregion

    #region Set SubCategory
    public int SetHambalPriceList()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEdithumbalpricelistSetup";

        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@CategoryId", CategoryId);
        param[2] = new SqlParameter("@price", price);
        param[3] = new SqlParameter("@ItemId", ItemId);
      


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["id"]);

        return retVal;
    }
    #endregion
    #region SubCategory

    #region Set SubCategory
    public int SetSubCategory()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "SetSubCategory";

        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
        param[1] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        param[2] = new SqlParameter("@SubcategoryName", SubcategoryName);
        param[3] = new SqlParameter("@CustomerCode", CustomerCode);
        param[4] = new SqlParameter("@BoutiquePurchaseDiscount", BoutiquePurchaseDiscount);
        param[5] = new SqlParameter("@BoutiqueSalesDiscount", BoutiqueSalesDiscount);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["SubCategoryCode"]);

        return retVal;
    }
    #endregion

    public DataSet GethumbalPricelist()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GethumbalPricelist";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@CategoryId", CategoryId);
        param[1] = new SqlParameter("@id", id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }


    #region Get SubCategory
    public DataSet GetSubCategory()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetSubCategory";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public DataSet GetEMBSubCategory()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetEMBSubCategory";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }

    public DataSet GetHambalSubCategory()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetHambalSubCategory";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CategoryId", CategoryId);
        param[2] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }
    #endregion
    #region Get SubCategory
    public DataSet GetSubCategoryByMainCategory()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "GetSubCategoryByMainCategory";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }
    #endregion

    #region getItemByBrand
    public DataSet getItemByBrand()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "proc_getItemByBrand";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@BrandId", BrandId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@SubCategoryCode", SubCategoryCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }
    #endregion


    #region Delete Subcategory
    public int DeleteSubcategoryCode()
    {
        int result = 0;
        DataSet ds;

        string sqlCommand = "DeleteSubcategoryCode";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["SubCategoryCode"]);

        return result;

    }
    #endregion
    #region Delete humbal price list
    public int DeletehumbalpricelistSetup()
    {
        int result = 0;
        DataSet ds;

        string sqlCommand = "DeletehumbalpricelistSetup";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@id", id);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["id"]);

        return result;

    }
    #endregion
    #endregion
    #region CostCenter
    #region Set CostCenter
    public int SetCostCenter()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "SetCostCenter";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CostCenterName", CostCenterName);
        param[2] = new SqlParameter("@CostCenterCode", CostCenterCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["CostCenterCode"]);


        return retVal;
    }
    #endregion
    #region Set Color
    public int SetColor()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditColor";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ColorId", ColorId);
        param[1] = new SqlParameter("@Color", Color);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["ColorId"]);


        return retVal;
    }
    #endregion

    #region Set Size
    public int SetSize()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditSize";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@SizeId", SizeId);
        param[1] = new SqlParameter("@Size", Size);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["SizeId"]);


        return retVal;
    }
    #endregion

    #region Get CostCenter
    public DataSet GetCostCenter()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCostCenter";

        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@CostCenterCode", CostCenterCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }
    #endregion
    #region Get Color
    public DataSet GetColor()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetColor";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@ColorId", ColorId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }
    #endregion
    #region Get SIze
    public DataSet GetSize()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetSize";

        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);

        param[1] = new SqlParameter("@SizeId", SizeId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }
    #endregion

    #region Delete CostCenter
    public int DeleteCostCenterCode()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteCostCenterCode";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CostCenterCode", CostCenterCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }

    public int DeleteSize()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteSize";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@SizeId", SizeId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }

    public int DeleteColor()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteColor";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@ColorId", ColorId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }
    #endregion
    #endregion

    #region Set Currency
    public void SetCurrency()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "SetCurrency";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CurrencyDesc", CurrencyDesc);
        param[1] = new SqlParameter("@CurrencySymbol", CurrencySymbol);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


    }
    #endregion
    #region Get Currency
    public DataSet GetCurrency()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetCurrency";
        //Database db = DatabaseFactory.CreateDatabase();
        //DBCommandWrapper dbCw = db.GetStoredProcCommandWrapper(sqlCommand);
        //dbCw.AddInParameter("@CustomerCode", DbType.Int32, CustomerCode);

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds;
    }
    #endregion
    #region Item
    #region Set Item
    public int SetItem()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "proc_AddEditItems";
        SqlParameter[] param = new SqlParameter[21];
        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@ItemDesc", ItemDesc);
        param[2] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        param[3] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
        param[4] = new SqlParameter("@Unit", Unit);
        param[5] = new SqlParameter("@Packing", Packing);
        param[6] = new SqlParameter("@PurchaseRate", PurchaseRate);
        param[7] = new SqlParameter("@SaleRate", SaleRate);
        param[8] = new SqlParameter("@SalesTax", SalesTax);
        param[9] = new SqlParameter("@DutyNumber", DutyNumber);
        param[10] = new SqlParameter("@Opbal", Opbal);
        param[11] = new SqlParameter("@CustomerCode", CustomerCode);
        param[12] = new SqlParameter("@BarcodeNo", BarcodeNo);
        param[13] = new SqlParameter("@IsInvetory", IsInvetory);
        param[14] = new SqlParameter("@ColorId", ColorId);
        param[15] = new SqlParameter("@SizeId", SizeId);
        param[16] = new SqlParameter("@BoutiqueItemCode", BoutiqueItemCode);

        param[17] = new SqlParameter("@ArticalTypeId", ArticalTypeId);
        param[18] = new SqlParameter("@ArticleNameId", ArticleNameId);


        //   param[19] = new SqlParameter("@BoutiquePurchaseDiscount", BoutiquePurchaseDiscount);

        param[19] = new SqlParameter("@BoutiqueItemDescription", BoutiqueItemDescription);

        param[20] = new SqlParameter("@MinOrderLevel", MinOrderLevel);
        // param[21] = new SqlParameter("@BoutiqueSalesDiscount", BoutiqueSalesDiscount);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["ItemId"]);

        return retVal;
    }
    public int AddEditBoutiqueItems()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "proc_AddEditBoutiqueItems";
        SqlParameter[] param = new SqlParameter[21];
        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@ItemDesc", ItemDesc);
        param[2] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        param[3] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
        param[4] = new SqlParameter("@Unit", Unit);
        param[5] = new SqlParameter("@Packing", Packing);
        param[6] = new SqlParameter("@PurchaseRate", PurchaseRate);
        param[7] = new SqlParameter("@SaleRate", SaleRate);
        param[8] = new SqlParameter("@SalesTax", SalesTax);
        param[9] = new SqlParameter("@DutyNumber", DutyNumber);
        param[10] = new SqlParameter("@Opbal", Opbal);
        param[11] = new SqlParameter("@CustomerCode", CustomerCode);
        param[12] = new SqlParameter("@BarcodeNo", BarcodeNo);
        param[13] = new SqlParameter("@IsInvetory", IsInvetory);
        param[14] = new SqlParameter("@ColorId", ColorId);
        param[15] = new SqlParameter("@SizeId", SizeId);
        param[16] = new SqlParameter("@BoutiqueItemCode", BoutiqueItemCode);
        param[17] = new SqlParameter("@ArticalTypeId", ArticalTypeId);
        param[18] = new SqlParameter("@ArticleNameId", ArticleNameId);
        param[19] = new SqlParameter("@BoutiqueItemDescription", BoutiqueItemDescription);
        param[20] = new SqlParameter("@PurchaseType", PurchaseType);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["ItemId"]);

        return retVal;
    }
    #endregion
    public DataSet getClientDiscount()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "proc_getClientDiscount";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ClientAcfileId", ClientAcfileId);
    
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }
    #region Get Item

    public DataSet getItems()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_getItems";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }

    public DataSet getLastItemRate()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "proc_getLastItemRate";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ItemCode", ItemId);
        param[1] = new SqlParameter("@ClientAccountId", ClientAccountId);
       
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }

    public DataTable getItemsSearch()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_getItemsSearch";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        param[1] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }
    public DataTable getSearchSubcategory()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_getSearchSubcategory";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        param[1] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }
    public DataSet getItemsByItemCode()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_getItemsByItemCode";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@BoutiqueItemCode", BoutiqueItemCode);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }
    public DataSet getPurchasePP()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_getPurchasePP";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }

    public DataSet getInventoryItems()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_getInventoryItems";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }

    public DataSet getItemsByBarcode()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_getItemsByBarcode";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@BarcodeNo", BarcodeNo);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }


    public DataSet SearchBoutiqueItem()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "SearchBoutiqueItem";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ItemDesc", ItemDesc);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }
    #endregion
    #region Delete Items
    public void DeleteItems()
    {

        string sqlCommand = "Proc_deleteItems";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        DataTier.SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, sqlCommand, param);


    }

    public int DeleteBoutiqueItems()
    {
        DataSet ds;
        int retVal = 0;

        string sqlCommand = "Proc_deleteItems";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["ItemId"]);

        return retVal;

    }
    #endregion
    #region Set Item Setup CostCenter
    public void SetItemSetupCostCenter()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "proc_AddEditItemSetupCostCenter";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@OpeningQuty", OpeningQuty);
        param[1] = new SqlParameter("@CostCenterCode", CostCenterCode);
        param[2] = new SqlParameter("@ItemSetupCostCenterId", ItemSetupCostCenterId);
        param[3] = new SqlParameter("@ItemId", ItemId);
        param[4] = new SqlParameter("@CustomerCode", CustomerCode);
        DataTier.SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, sqlCommand, param);



    }
    #endregion
    #region Get ItemSetup CostCenterBy ItemId
    public DataSet getItemSetupCostCenterByItemId()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_getItemSetupCostCenterByItemId";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }

    #endregion
    #endregion
    #region Delete ItemSetup CostCenterBy ItemId
    public DataSet DeleteItemSetupCostCenterByItemId()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_DeleteItemSetupCostCenterByItemId";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }

    #endregion
    #region Set MainMenue
    public void AddEditMenuMainCategory()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_AddEditMenuMainCategory";


        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CatId", CatId);
        param[1] = new SqlParameter("@Title", Title);
        param[2] = new SqlParameter("@Position", Position);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);




    }


    public void AddEditMenuSubCategory()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_AddEditMenuSubCategory";


        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@SubCatid ", SubCatid);
        param[1] = new SqlParameter("@SubCatTitle", SubCatTitle);
        param[2] = new SqlParameter("@CatId", CatId);
        param[3] = new SqlParameter("@PageName", PageName);
        param[4] = new SqlParameter("@Position", Position);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);




    }

    public DataTable GetMenuMainCategory()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "GetMenu_MainCategory";


        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CatId", CatId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);




        return ds.Tables[0];



    }

    public DataTable GetMenuSubCategory()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "GetMenu_SubCategory";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@SubCatid", SubCatid);
        param[1] = new SqlParameter("@CatId", CatId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];

    }


    public void AddEditMenu()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "proc_AddEditMenu";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
        param[1] = new SqlParameter("@SubCatid", SubCatid);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        //  return ds.Tables[0];

    }
    public void DeleteMenu()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "proc_DeleteMenu";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@BusinessTypeId", BusinessTypeId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



    }
    public DataTable GetMenu()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "proc_GetMenu";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@BusinessTypeId", BusinessTypeId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];

    }


    public DataTable getSubMenuFront()
    {

        DataSet ds = new DataSet();

        //string sqlCommand = "proc_getSubMenuFront";
        //SqlParameter[] param = new SqlParameter[2];
        //param[0] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
        //param[1] = new SqlParameter("@CatId", CatId);


        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "proc_getSubMenuFront";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@BusinessTypeId", BusinessTypeId);
        cm.Parameters.AddWithValue("@CatId", CatId);
        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();

        //  ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];

    }

    public DataTable getMainMenuFront()
    {

        DataSet ds = new DataSet();

        //string sqlCommand = "proc_getMainMenuFront";
        //SqlParameter[] param = new SqlParameter[1];
        //param[0] = new SqlParameter("@BusinessTypeId", BusinessTypeId);
        //ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "proc_getMainMenuFront";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@BusinessTypeId", BusinessTypeId);
        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();

        return ds.Tables[0];
    }

    public DataTable getMainMenuFront_UserRole()
    {

        DataSet ds = new DataSet();

        //string sqlCommand = "proc_getMainMenuFront_UserRole";
        //SqlParameter[] param = new SqlParameter[1];
        //param[0] = new SqlParameter("@Userid", Userid_SubMenu);
        //ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "proc_getMainMenuFront_UserRole";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@Userid", Userid_SubMenu);

        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();

        return ds.Tables[0];
    }


    public DataTable getSubMenuFront_UserRole()
    {

        DataSet ds = new DataSet();

        //string sqlCommand = "proc_getSubMenuFront_UserRole";
        //SqlParameter[] param = new SqlParameter[2];
        //param[0] = new SqlParameter("@Userid", Userid_SubMenu);
        //param[1] = new SqlParameter("@CatId", CatId);
        //ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        con = new SqlConnection();
        con.ConnectionString = conStr;
        con.Open();
        cm = new SqlCommand();
        cm.CommandText = "proc_getSubMenuFront_UserRole";
        cm.CommandType = CommandType.StoredProcedure;
        cm.Connection = con;
        cm.Parameters.AddWithValue("@Userid", Userid_SubMenu);
        cm.Parameters.AddWithValue("@CatId", CatId);
        da = new SqlDataAdapter();
        da.SelectCommand = cm;
        da.Fill(ds);
        con.Close();

        return ds.Tables[0];

    }

    #endregion


    #region Add Edit BoutiqueUser

    public int AddEditBoutiqueUser()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditBoutiqueUser";
        SqlParameter[] param = new SqlParameter[12];
        param[0] = new SqlParameter("@BoutiqueUserId", BoutiqueUserId);
        param[1] = new SqlParameter("@UserName", UserName);
        param[2] = new SqlParameter("@Password", Password);

        param[3] = new SqlParameter("@DesignerName", DesignerName);
        param[4] = new SqlParameter("@ContactNo", ContactNo);
        param[5] = new SqlParameter("@MobileNo", MobileNo);

        param[6] = new SqlParameter("@Country", Country);
        param[7] = new SqlParameter("@City", City);
        param[8] = new SqlParameter("@Address", Address);

        param[9] = new SqlParameter("@BrandId", BrandId);
        param[10] = new SqlParameter("@CustomerCode", CustomerCode);

        param[11] = new SqlParameter("@Email", Email);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["BoutiqueUserId"]);


        return retVal;
    }

    public DataSet getBoutiqueUserSetup()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "Proc_getBoutiqueUserSetup";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@BoutiqueUserId", BoutiqueUserId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }

    public void DeleteBoutiqueUser()
    {

        DataSet ds = new DataSet();

        string sqlCommand = "DeleteBoutiqueUserSetup";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@BoutiqueUserId", BoutiqueUserId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);



    }

    #endregion

    #region Get Boutique Artical Name
    public DataSet GetBoutiqueArticalName()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetBoutiqueArticalName";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@ArticleNameId", ArticleNameId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }
    #endregion
    #region Get Boutique Artical Type
    public DataSet GetBoutiqueArticalType()
    {
        DataSet ds = new DataSet();

        string sqlCommand = "GetBoutiqueArticalType";

        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@ArticalTypeId", ArticalTypeId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds;
    }
    #endregion




    #region Delete Boutique Artical Name
    public int DeleteBoutiqueArticalName()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteBoutiqueArticalName";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@ArticleNameId", ArticleNameId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        result = Convert.ToInt32(ds.Tables[0].Rows[0]["result"]);

        return result;

    }
    #endregion


    #region Delete Boutique Artical Type
    public int DeleteBoutiqueArticalType()
    {
        int result = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "DeleteBoutiqueArticalType";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@ArticalTypeId", ArticalTypeId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return result;

    }
    #endregion


    #region AddEdit Boutique Artical Name
    public int AddEditBoutiqueArticalName()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditBoutiqueArticalName";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ArticleNameId", ArticleNameId);
        param[1] = new SqlParameter("@ArticleName", ArticleName);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["ArticleNameId"]);


        return retVal;
    }
    #endregion

    #region AddEdit Boutique Artical Type
    public int AddEditBoutiqueArticalType()
    {
        int retVal = 0;
        DataSet ds = new DataSet();

        string sqlCommand = "AddEditBoutiqueArticalType";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ArticalTypeId", ArticalTypeId);
        param[1] = new SqlParameter("@ArticalType", ArticalType);
        param[2] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["ArticalTypeId"]);


        return retVal;
    }
    #endregion
}
#endregion