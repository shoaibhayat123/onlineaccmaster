﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.Mail;
using System.Net.Mail;
using System.IO;


/// <summary>
/// Summary description for EmailSender
/// </summary>
public class EmailSenderNew
{

    #region Data Members

    public string MailServer;
    public string MailUser;
    public string MailFrom;
    public string MailPassword;
    public string MailTo;
    public string MailBcc;
    public string TemplateDoc;
    public string Message, Subject;
    public string[] ValueArray;
    public string _CC;

    #endregion
    public EmailSenderNew()
    {
        //
        // TODO: Add constructor logic here

        //

    }



    #region Properties

    public string CC
    {
        set { _CC = value; }
    }



    #endregion



    #region SendMail

    public void Send(string msg, string mailfrom, string MailTo, string Subject)
    {
        MailServer = System.Configuration.ConfigurationManager.AppSettings["MailServer"];
        MailUser = System.Configuration.ConfigurationManager.AppSettings["MailUser"];
        MailTo = MailTo;
        //MailFrom = System.Configuration.ConfigurationManager.AppSettings["EmailFrom"];
        MailFrom = mailfrom;
        MailPassword = System.Configuration.ConfigurationManager.AppSettings["MailPassword"];
        //MailBcc = System.Configuration.ConfigurationManager.AppSettings["BCCMail"];
        ///SendTo = System.Configuration.ConfigurationManager.AppSettings["RootMail"];
      
            SmtpClient smtpClient = new SmtpClient(MailServer);
            System.Net.Mail.MailMessage MlMessage = new System.Net.Mail.MailMessage(MailFrom, MailTo);
            Subject = Subject;

            MlMessage.Subject = Subject;
          //  MlMessage.Bcc.Add(MailBcc);
           // MlMessage.IsBodyHtml = true;
            //MlMessage.Body = GetHtml(TemplateDoc).ToString();
            MlMessage.Body = msg;
            MlMessage.Priority = System.Net.Mail.MailPriority.High;

            smtpClient.Credentials = new System.Net.NetworkCredential(MailUser, MailPassword);
            smtpClient.Send(MlMessage);
       
    }
   

 
    #endregion

    #region  READ TEMPLATE
    //¦ ROUTINE TO READ TEMPLATE AND RETURN THE MAIL BODY 
    public string GetHtml(string argTemplateDocument)
    {
        int i = 0;
        System.IO.StreamReader filePtr;
        string fileData;
        string path = HttpContext.Current.Server.MapPath("~/EmailTemplate");
        filePtr = File.OpenText(path + "\\" + argTemplateDocument.ToString());
      //  filePtr = File.OpenText(ConfigurationSettings.AppSettings["EMLPath"].ToString() + argTemplateDocument.ToString());
        fileData = filePtr.ReadToEnd();//.ReadToEnd;
     
        if (ValueArray == null)
        {
            return fileData;
        }
        else
        {
            for (i = 0; i < ValueArray.Length; i++)
            {
                fileData = fileData.Replace("@v" + i + "@", ValueArray[i].ToString());
            }
            return fileData;
        }

        filePtr.Close();
        filePtr = null;
    }
    #endregion

}
