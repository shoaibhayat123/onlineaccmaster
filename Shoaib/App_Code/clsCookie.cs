using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


/// <summary>
/// Summary description for clsCookie
/// </summary>
public class clsCookie
{
    public clsCookie()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void InsertCookie(string CookieName, string CookieValue)
    {
        HttpCookie oCookie2 = new HttpCookie(CookieName);
        oCookie2.Value = CookieValue;
        oCookie2.Expires = DateTime.Now.AddYears(5);
        HttpContext.Current.Response.Cookies.Add(oCookie2);
    }

    public string GetCookieValue(string CookieName)
    {
        if (HttpContext.Current.Request.Cookies[CookieName] != null)
        {
            return HttpContext.Current.Request.Cookies[CookieName].Value;
        }
        else
        {
            return "";
        }
    }

    public void DeleteCookie(string CookieName)
    {
        HttpContext.Current.Response.Cookies[CookieName].Expires = System.DateTime.Now.AddDays(-1);
    }

    public void InsertCookie10Hrs(string CookieName, string CookieValue)
    {
        HttpCookie oCookie2 = new HttpCookie(CookieName);
        oCookie2.Value = CookieValue;
        oCookie2.Expires = DateTime.Now.AddHours(1);
        HttpContext.Current.Response.Cookies.Add(oCookie2);
    }

}
