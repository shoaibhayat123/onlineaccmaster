﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for SalesInvoices
/// </summary>
public class SalesInvoices
{
    #region sqlnew
    SqlConnection con;
    SqlCommand cm;
    SqlDataAdapter da;
    DataTable dt;
    String conStr = ConfigurationManager.ConnectionStrings["ConnectionAccMasterMaster"].ToString();
    #endregion
    #region Data Members
    public string BuiltyNumber, SalesInfoRefNo,ReceiveNumber, SalesPersonAbbr, OrderStatus, PurchaseType, CustomerName, ContactNumber, EmailAddress, DataFrom, POSSaleType, CreditCardNo, Approval, VIN, QuantityType, DescriptionPurcahse, ReceiptNo, AccounTitle, InvNo, RNumber, DoNumber, AccountCode, deliverTo, ItemDesc, Description, InvoiceNo, ChqNo;
        public int TransporterNo, DRID, CRID, CardChargesAcfileId, ContainerNoId, BuyerAcfileId, BrokerAcfileId, SalesPersonCreditAcfileId, VendorAcfileId1, ToeingAcfileId1, VendorAcfileId2, ToeingAcfileId2, LoaderAcfileId, LoadingExpAcfileId, CarLodingId, ProdNConsMainId, RepairAcfileId, VendorAcfileId, PurchaseAcfileId, PartnerAcFileID, CashReceivedAcFileID, VinId, CostCenterCode, AcfileID, StoreId, InvoiceSummaryId, StoreFromId, StoreToId, TransferSummaryId, JVMainId, DebitAcId, InvoiceNos, CreditAcId, ServiceInvoiceMainId, CashBookMainId, AccountId, BankAcId, CashBookAcfileID, id, ServiceInvoiceNo, SaleCrdays, VoucherSimpleMainId, ItemId, Cartons, PurchaseCrDays, SoldToId, BrokerageId, PurchaseFromId, PLId, CrDays, storeId, ClientAccountId, Crdays, SalesAccountId, SaleTexAccountId, ItemCode, SaleTaxAccountId, TransId, AcFileId;
        public Nullable<int> Creditperiod;
        public decimal BoutiqueAmountAfterDis, BoutiqueDiscount, AlterCharges, CardChargesRate, CardChargesAmount, FCRateDebit, BoutiqueConvRate, BoutiqueGrossAmountFC, BoutiquePurchaseDiscount, BoutiqueCostPrice, BoutiqueSalesDiscount, BoutiqueSalePrice, CashReceived, CashReturned, FCRateCredit, FCAmountCredit, FCAmountDebit, TotalAmountFC, FCAmount, FCRate, TotalQuantity, TotalquantityProduction, TotalquantityConsumption, TransQty, Totalquantity, Receive, DebitAmount, Payment, Amount, VoucherNo, CreditAmount, TotalAmount, rate, SaleAmount, SaleRate, QuantityKg, QuantityLbs, PurcheseRate, PurchaseAmount, CopsAmount, PLAmount, TotalGrossAmount, TransAmt, InvoiceDiscount, Kgs, Lbs, TotalSaleTaxAmount, TotalAmountIncludeTax, CartageAmount, BillAmount, CustomerCode, Quantity, Rate, GrossAmount, SaleTax, SaleTaxAmount, AmountIncludeTaxes;
        public DateTime InvDate,AsOn, DateFrom, DateTo,CashBookDateFrom, CashBookDateTo, InvDateFrom, VoucherDateFrom, VoucherDateTo, invDateTo, VoucherDate, SaleDate, PuchaseDate, TransactionDate, CashBookDate;
        public Nullable<DateTime> ChqDate, DueDate;
        public Nullable<decimal> OpeningBal,ConversionRate, PurchaseRate,BoutiqueTotalGrossAmountFC, SalesAge, PurchaseAge;
        public Nullable<Boolean> Rounding;
			
        int retVal = 0;



  
    #endregion
       
	public SalesInvoices()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataTable AddEditInvoiceSummary()
    {      
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditInvoiceSummary";        


               SqlParameter[] param = new SqlParameter[28];
                param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
                param[1] = new SqlParameter("@InvNo", InvNo);
                param[2] = new SqlParameter("@InvDate", InvDate);
                param[3] = new SqlParameter("@CrDays", CrDays);
                param[4] = new SqlParameter("@storeId", storeId);
                param[5] = new SqlParameter("@ClientAccountId", ClientAccountId);
                param[6] = new SqlParameter("@SalesAccountId", SalesAccountId);
                param[7] = new SqlParameter("@SaleTaxAccountId", SaleTaxAccountId);
                param[8] = new SqlParameter("@TotalQuantity", TotalQuantity);
                param[9] = new SqlParameter("@TotalGrossAmount", TotalGrossAmount);
                param[10] = new SqlParameter("@TotalSaleTaxAmount", TotalSaleTaxAmount);
                param[11] = new SqlParameter("@TotalAmountIncludeTax", TotalAmountIncludeTax);
                param[12] = new SqlParameter("@CartageAmount", CartageAmount);
                param[13] = new SqlParameter("@BillAmount", BillAmount);
                param[14] = new SqlParameter("@CustomerCode", CustomerCode);
                param[15] = new SqlParameter("@DataFrom", DataFrom);
                param[16] = new SqlParameter("@Description", Description);
                param[17] = new SqlParameter("@InvoiceDiscount", InvoiceDiscount);

                param[18] = new SqlParameter("@CashReceived", CashReceived);
                param[19] = new SqlParameter("@CashReturned", CashReturned);
                param[20] = new SqlParameter("@CashReceivedAcFileID", CashReceivedAcFileID);


                param[21] = new SqlParameter("@POSSaleType", POSSaleType);
                param[22] = new SqlParameter("@CreditCardNo", CreditCardNo);
                param[23] = new SqlParameter("@Approval", Approval);
                param[24] = new SqlParameter("@BoutiqueTotalGrossAmountFC", BoutiqueTotalGrossAmountFC);

             
              
                      
      ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);     


        return ds.Tables[0];
    }

    public DataTable AddEditBoutiqueSalesReturnInvoiceSummary()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditBoutiqueSalesReturnInvoiceSummary";

        SqlParameter[] param = new SqlParameter[28];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@InvNo", InvNo);
        param[2] = new SqlParameter("@InvDate", InvDate);
        param[3] = new SqlParameter("@CrDays", CrDays);
        param[4] = new SqlParameter("@storeId", storeId);
        param[5] = new SqlParameter("@ClientAccountId", ClientAccountId);
        param[6] = new SqlParameter("@SalesAccountId", SalesAccountId);
        param[7] = new SqlParameter("@SaleTaxAccountId", SaleTaxAccountId);
        param[8] = new SqlParameter("@TotalQuantity", TotalQuantity);
        param[9] = new SqlParameter("@TotalGrossAmount", TotalGrossAmount);
        param[10] = new SqlParameter("@TotalSaleTaxAmount", TotalSaleTaxAmount);
        param[11] = new SqlParameter("@TotalAmountIncludeTax", TotalAmountIncludeTax);
        param[12] = new SqlParameter("@CartageAmount", CartageAmount);
        param[13] = new SqlParameter("@BillAmount", BillAmount);
        param[14] = new SqlParameter("@CustomerCode", CustomerCode);
        param[15] = new SqlParameter("@DataFrom", DataFrom);
        param[16] = new SqlParameter("@Description", Description);
        param[17] = new SqlParameter("@InvoiceDiscount", InvoiceDiscount);

        param[18] = new SqlParameter("@CashReceived", CashReceived);
        param[19] = new SqlParameter("@CashReturned", CashReturned);
        param[20] = new SqlParameter("@CashReceivedAcFileID", CashReceivedAcFileID);


        param[21] = new SqlParameter("@POSSaleType", POSSaleType);
        param[22] = new SqlParameter("@CreditCardNo", CreditCardNo);
        param[23] = new SqlParameter("@Approval", Approval);
        param[24] = new SqlParameter("@BoutiqueTotalGrossAmountFC", BoutiqueTotalGrossAmountFC);
        param[25] = new SqlParameter("@ConversionRate", ConversionRate);
        param[26] = new SqlParameter("@SalesInfoRefNo", SalesInfoRefNo);



        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }
    public DataTable HambalAddEditInvoiceSummary()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_HambalAddEditInvoiceSummary";


        SqlParameter[] param = new SqlParameter[30];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@InvNo", InvNo);
        param[2] = new SqlParameter("@InvDate", InvDate);
        param[3] = new SqlParameter("@CrDays", CrDays);
        param[4] = new SqlParameter("@storeId", storeId);
        param[5] = new SqlParameter("@ClientAccountId", ClientAccountId);
        param[6] = new SqlParameter("@SalesAccountId", SalesAccountId);
        param[7] = new SqlParameter("@SaleTaxAccountId", SaleTaxAccountId);
        param[8] = new SqlParameter("@TotalQuantity", TotalQuantity);
        param[9] = new SqlParameter("@TotalGrossAmount", TotalGrossAmount);
        param[10] = new SqlParameter("@TotalSaleTaxAmount", TotalSaleTaxAmount);
        param[11] = new SqlParameter("@TotalAmountIncludeTax", TotalAmountIncludeTax);
        param[12] = new SqlParameter("@CartageAmount", CartageAmount);
        param[13] = new SqlParameter("@BillAmount", BillAmount);
        param[14] = new SqlParameter("@CustomerCode", CustomerCode);
        param[15] = new SqlParameter("@DataFrom", DataFrom);
        param[16] = new SqlParameter("@Description", Description);
        param[17] = new SqlParameter("@InvoiceDiscount", InvoiceDiscount);

        param[18] = new SqlParameter("@CashReceived", CashReceived);
        param[19] = new SqlParameter("@CashReturned", CashReturned);
        param[20] = new SqlParameter("@CashReceivedAcFileID", CashReceivedAcFileID);


        param[21] = new SqlParameter("@POSSaleType", POSSaleType);
        param[22] = new SqlParameter("@CreditCardNo", CreditCardNo);
        param[23] = new SqlParameter("@Approval", Approval);
        param[24] = new SqlParameter("@BoutiqueTotalGrossAmountFC", BoutiqueTotalGrossAmountFC);
        param[25] = new SqlParameter("@TransporterNo", TransporterNo);
        param[26] = new SqlParameter("@BuiltyNumber", BuiltyNumber);
        param[27] = new SqlParameter("@ReceiveNumber", ReceiveNumber);
        param[28] = new SqlParameter("@CardChargesRate", CardChargesRate);



        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }
    public DataTable BoutiqueAddEditInvoiceSummary()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_BoutiqueAddEditInvoiceSummary";


        SqlParameter[] param = new SqlParameter[27];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@InvNo", InvNo);
        param[2] = new SqlParameter("@InvDate", InvDate);
        param[3] = new SqlParameter("@CrDays", CrDays);
        param[4] = new SqlParameter("@storeId", storeId);
        param[5] = new SqlParameter("@ClientAccountId", ClientAccountId);
        param[6] = new SqlParameter("@SalesAccountId", SalesAccountId);
        param[7] = new SqlParameter("@SaleTaxAccountId", SaleTaxAccountId);
        param[8] = new SqlParameter("@TotalQuantity", TotalQuantity);
        param[9] = new SqlParameter("@TotalGrossAmount", TotalGrossAmount);
        param[10] = new SqlParameter("@TotalSaleTaxAmount", TotalSaleTaxAmount);
        param[11] = new SqlParameter("@TotalAmountIncludeTax", TotalAmountIncludeTax);
        param[12] = new SqlParameter("@CartageAmount", CartageAmount);
        param[13] = new SqlParameter("@BillAmount", BillAmount);
        param[14] = new SqlParameter("@CustomerCode", CustomerCode);
        param[15] = new SqlParameter("@DataFrom", DataFrom);
        param[16] = new SqlParameter("@Description", Description);
        param[17] = new SqlParameter("@InvoiceDiscount", InvoiceDiscount);

        param[18] = new SqlParameter("@CashReceived", CashReceived);
        param[19] = new SqlParameter("@CashReturned", CashReturned);
        param[20] = new SqlParameter("@CashReceivedAcFileID", CashReceivedAcFileID);


        param[21] = new SqlParameter("@POSSaleType", POSSaleType);
        param[22] = new SqlParameter("@CreditCardNo", CreditCardNo);
        param[23] = new SqlParameter("@Approval", Approval);
        param[24] = new SqlParameter("@BoutiqueTotalGrossAmountFC", BoutiqueTotalGrossAmountFC);
        param[25] = new SqlParameter("@PurchaseType", PurchaseType);
        param[26] = new SqlParameter("@ConversionRate", ConversionRate);



        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }
    public DataTable AddEditBoutiqueInvoiceSummary()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditBoutiqueInvoiceSummary";


        SqlParameter[] param = new SqlParameter[28];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@InvNo", InvNo);
        param[2] = new SqlParameter("@InvDate", InvDate);
        param[3] = new SqlParameter("@CrDays", CrDays);
        param[4] = new SqlParameter("@storeId", storeId);
        param[5] = new SqlParameter("@ClientAccountId", ClientAccountId);
        param[6] = new SqlParameter("@SalesAccountId", SalesAccountId);
        param[7] = new SqlParameter("@SaleTaxAccountId", SaleTaxAccountId);
        param[8] = new SqlParameter("@TotalQuantity", TotalQuantity);
        param[9] = new SqlParameter("@TotalGrossAmount", TotalGrossAmount);
        param[10] = new SqlParameter("@TotalSaleTaxAmount", TotalSaleTaxAmount);
        param[11] = new SqlParameter("@TotalAmountIncludeTax", TotalAmountIncludeTax);
        param[12] = new SqlParameter("@CartageAmount", CartageAmount);
        param[13] = new SqlParameter("@BillAmount", BillAmount);
        param[14] = new SqlParameter("@CustomerCode", CustomerCode);
        param[15] = new SqlParameter("@DataFrom", DataFrom);
        param[16] = new SqlParameter("@Description", Description);
        param[17] = new SqlParameter("@InvoiceDiscount", InvoiceDiscount);

        param[18] = new SqlParameter("@CashReceived", CashReceived);
        param[19] = new SqlParameter("@CashReturned", CashReturned);
        param[20] = new SqlParameter("@CashReceivedAcFileID", CashReceivedAcFileID);


        param[21] = new SqlParameter("@POSSaleType", POSSaleType);
        param[22] = new SqlParameter("@CreditCardNo", CreditCardNo);
        param[23] = new SqlParameter("@Approval", Approval);
        param[24] = new SqlParameter("@BoutiqueTotalGrossAmountFC", BoutiqueTotalGrossAmountFC);
        param[25] = new SqlParameter("@PurchaseType", PurchaseType);



        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }


    public DataTable AddEditInvoiceSummaryPos()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditInvoiceSummaryPos";


        SqlParameter[] param = new SqlParameter[33];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@InvNo", InvNo);
        param[2] = new SqlParameter("@InvDate", InvDate);
        param[3] = new SqlParameter("@CrDays", CrDays);
        param[4] = new SqlParameter("@storeId", storeId);
        param[5] = new SqlParameter("@ClientAccountId", ClientAccountId);
        param[6] = new SqlParameter("@SalesAccountId", SalesAccountId);
        param[7] = new SqlParameter("@SaleTaxAccountId", SaleTaxAccountId);
        param[8] = new SqlParameter("@TotalQuantity", TotalQuantity);
        param[9] = new SqlParameter("@TotalGrossAmount", TotalGrossAmount);
        param[10] = new SqlParameter("@TotalSaleTaxAmount", TotalSaleTaxAmount);
        param[11] = new SqlParameter("@TotalAmountIncludeTax", TotalAmountIncludeTax);
        param[12] = new SqlParameter("@CartageAmount", CartageAmount);
        param[13] = new SqlParameter("@BillAmount", BillAmount);
        param[14] = new SqlParameter("@CustomerCode", CustomerCode);
        param[15] = new SqlParameter("@DataFrom", DataFrom);
        param[16] = new SqlParameter("@Description", Description);
        param[17] = new SqlParameter("@InvoiceDiscount", InvoiceDiscount);

        param[18] = new SqlParameter("@CashReceived", CashReceived);
        param[19] = new SqlParameter("@CashReturned", CashReturned);
        param[20] = new SqlParameter("@CashReceivedAcFileID", CashReceivedAcFileID);


        param[21] = new SqlParameter("@POSSaleType", POSSaleType);
        param[22] = new SqlParameter("@CreditCardNo", CreditCardNo);
        param[23] = new SqlParameter("@Approval", Approval);
        param[24] = new SqlParameter("@BoutiqueTotalGrossAmountFC", BoutiqueTotalGrossAmountFC);

        param[25] = new SqlParameter("@CardChargesRate", CardChargesRate);
        param[26] = new SqlParameter("@CardChargesAmount", CardChargesAmount);
        param[27] = new SqlParameter("@CardChargesAcfileId", CardChargesAcfileId);

        param[28] = new SqlParameter("@CustomerName", CustomerName);
        param[29] = new SqlParameter("@ContactNumber", ContactNumber);
        param[30] = new SqlParameter("@EmailAddress", EmailAddress);
        param[31] = new SqlParameter("@SalesPersonAbbr", SalesPersonAbbr);
        param[32] = new SqlParameter("@ConversionRate", ConversionRate);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }
    public DataTable AddEditBoutiqueOrderSummary()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditBoutiqueOrderSummary";


        SqlParameter[] param = new SqlParameter[32];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@InvNo", InvNo);
        param[2] = new SqlParameter("@InvDate", InvDate);
        param[3] = new SqlParameter("@CrDays", CrDays);
        param[4] = new SqlParameter("@storeId", storeId);
        param[5] = new SqlParameter("@ClientAccountId", ClientAccountId);
        param[6] = new SqlParameter("@SalesAccountId", SalesAccountId);
        param[7] = new SqlParameter("@SaleTaxAccountId", SaleTaxAccountId);
        param[8] = new SqlParameter("@TotalQuantity", TotalQuantity);
        param[9] = new SqlParameter("@TotalGrossAmount", TotalGrossAmount);
        param[10] = new SqlParameter("@TotalSaleTaxAmount", TotalSaleTaxAmount);
        param[11] = new SqlParameter("@TotalAmountIncludeTax", TotalAmountIncludeTax);
        param[12] = new SqlParameter("@CartageAmount", CartageAmount);
        param[13] = new SqlParameter("@BillAmount", BillAmount);
        param[14] = new SqlParameter("@CustomerCode", CustomerCode);
        param[15] = new SqlParameter("@DataFrom", DataFrom);
        param[16] = new SqlParameter("@Description", Description);
        param[17] = new SqlParameter("@InvoiceDiscount", InvoiceDiscount);

        param[18] = new SqlParameter("@CashReceived", CashReceived);
        param[19] = new SqlParameter("@CashReturned", CashReturned);
        param[20] = new SqlParameter("@CashReceivedAcFileID", CashReceivedAcFileID);


        param[21] = new SqlParameter("@POSSaleType", POSSaleType);
        param[22] = new SqlParameter("@CreditCardNo", CreditCardNo);
        param[23] = new SqlParameter("@Approval", Approval);
        param[24] = new SqlParameter("@BoutiqueTotalGrossAmountFC", BoutiqueTotalGrossAmountFC);

        param[25] = new SqlParameter("@CardChargesRate", CardChargesRate);
        param[26] = new SqlParameter("@CardChargesAmount", CardChargesAmount);
        param[27] = new SqlParameter("@CardChargesAcfileId", CardChargesAcfileId);

        param[28] = new SqlParameter("@CustomerName", CustomerName);
        param[29] = new SqlParameter("@ContactNumber", ContactNumber);
        param[30] = new SqlParameter("@EmailAddress", EmailAddress);

        param[31] = new SqlParameter("@OrderStatus", OrderStatus);
        
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds.Tables[0];
    }


    public DataTable AddEditTransferSummary()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditTransferSummary";

        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@TransferSummaryId", TransferSummaryId);
        param[1] = new SqlParameter("@VoucherNo", VoucherNo);
        param[2] = new SqlParameter("@VoucherDate", VoucherDate);
        param[3] = new SqlParameter("@StoreFromId", StoreFromId);
        param[4] = new SqlParameter("@StoreToId", StoreToId);       
        param[5] = new SqlParameter("@Description", Description);
        param[6] = new SqlParameter("@Totalquantity", Totalquantity);
        param[7] = new SqlParameter("@DataFrom", DataFrom);
        param[8] = new SqlParameter("@CustomerCode", CustomerCode); 
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }



    public DataTable AddEditProdNConsDetail()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditProdNConsDetail";

        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@ProdNConsMainId", ProdNConsMainId);
        param[1] = new SqlParameter("@VoucherNo", VoucherNo);
        param[2] = new SqlParameter("@VoucherDate", VoucherDate);
        param[3] = new SqlParameter("@StoreId", StoreId);
        param[4] = new SqlParameter("@TotalquantityProduction", TotalquantityProduction);
        param[5] = new SqlParameter("@Description", Description);
        param[6] = new SqlParameter("@TotalquantityConsumption", TotalquantityConsumption);
        param[7] = new SqlParameter("@DataFrom", DataFrom);
        param[8] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }







    public DataTable AddEditVoucherSimpleMain()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditVoucherSimpleMain";        

        SqlParameter[] param = new SqlParameter[11];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@VoucherNo", VoucherNo);
        param[2] = new SqlParameter("@VoucherDate", VoucherDate);
        param[3] = new SqlParameter("@BankAcId", BankAcId);
        param[4] = new SqlParameter("@ChqNo", ChqNo);
        param[5] = new SqlParameter("@ChqDate", ChqDate);
        param[6] = new SqlParameter("@DataFrom", DataFrom);
        param[7] = new SqlParameter("@CustomerCode", CustomerCode);
        param[8] = new SqlParameter("@TotalAmount", TotalAmount);
        param[9] = new SqlParameter("@Description", Description);
        param[10] = new SqlParameter("@TotalAmountFC", TotalAmountFC);
      
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);     
        return ds.Tables[0];
    }

    public DataTable AddEditVoucherSimpleDetail()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditVoucherSimpleDetail";      

        SqlParameter[] param = new SqlParameter[10];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@AccountId", AccountId);
        param[2] = new SqlParameter("@Amount", Amount);
        param[3] = new SqlParameter("@RNumber", RNumber);
        param[4] = new SqlParameter("@Description", Description);
        param[5] = new SqlParameter("@VoucherSimpleMainId", VoucherSimpleMainId);
        param[6] = new SqlParameter("@DataFrom", DataFrom);
        param[7] = new SqlParameter("@CustomerCode", CustomerCode);
        param[8] = new SqlParameter("@FCAmount", FCAmount);
        param[9] = new SqlParameter("@FCRate", FCRate); 

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);     
        return ds.Tables[0];
    }

    public DataTable AddEditCashbookMain()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditCashbookMain";        

        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@AcFileId", AcFileId);
        param[2] = new SqlParameter("@CashBookDate", CashBookDate);      
        param[3] = new SqlParameter("@DataFrom", DataFrom);
        param[4] = new SqlParameter("@CustomerCode", CustomerCode);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);     
        return ds.Tables[0];
    }
    public DataTable AddeditCashBookDetail()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "Proc_AddeditCashBookDetail";     

        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@Description", Description);
        param[2] = new SqlParameter("@Receive", Receive);
        param[3] = new SqlParameter("@Payment", Payment);
        param[4] = new SqlParameter("@CashBookAcfileID", CashBookAcfileID);
        param[5] = new SqlParameter("@CashBookDate", CashBookDate);
        param[6] = new SqlParameter("@DataFrom", DataFrom);
        param[7] = new SqlParameter("@CustomerCode", CustomerCode);
        param[8] = new SqlParameter("@CashBookMainId", CashBookMainId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param); 
        return ds.Tables[0];
    }

    public DataTable GetBoutiquePurchaseinvoiceDetailByItem()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_GetBoutiquePurchaseinvoiceDetailByItem";

        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@ItemCode", ItemCode);
        param[1] = new SqlParameter("@PurchaseType", PurchaseType);       
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        param[3] = new SqlParameter("@CustomerCode", CustomerCode);       

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable AddEditYarnbrokerage()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditYarnbrokerage";
  

        SqlParameter[] param = new SqlParameter[27];
        param[0] = new SqlParameter("@BrokerageId", BrokerageId);
        param[1] = new SqlParameter("@SaleDate", SaleDate);
        param[2] = new SqlParameter("@SaleCrdays", SaleCrdays);
        param[3] = new SqlParameter("@PurchaseCrDays", PurchaseCrDays);
        param[4] = new SqlParameter("@SoldToId", SoldToId);
        param[5] = new SqlParameter("@PurchaseFromId", PurchaseFromId);
        param[6] = new SqlParameter("@PLId", PLId);
        param[7] = new SqlParameter("@ItemId", ItemId);
        param[8] = new SqlParameter("@DoNumber", DoNumber);
        param[9] = new SqlParameter("@deliverTo", deliverTo);
        param[10] = new SqlParameter("@Cartons", Cartons);
        param[11] = new SqlParameter("@Kgs", Kgs);
        param[12] = new SqlParameter("@Lbs", Lbs);
        param[13] = new SqlParameter("@SaleAmount", SaleAmount);
        param[14] = new SqlParameter("@PurchaseAmount", PurchaseAmount);
        param[15] = new SqlParameter("@CopsAmount", CopsAmount);
        param[16] = new SqlParameter("@PLAmount", PLAmount);
        param[17] = new SqlParameter("@CustomerCode", CustomerCode);
        param[18] = new SqlParameter("@DataFrom", DataFrom);
        param[19] = new SqlParameter("@SaleRate", SaleRate);
        param[20] = new SqlParameter("@PurcheseRate", PurcheseRate);
        param[21] = new SqlParameter("@Description", Description);
        param[22] = new SqlParameter("@DescriptionPurcahse", DescriptionPurcahse);
        param[23] = new SqlParameter("@QuantityType", QuantityType);
        param[24] = new SqlParameter("@Rounding", Rounding);
        param[25] = new SqlParameter("@PurchaseAge", PurchaseAge);
        param[26] = new SqlParameter("@SalesAge", SalesAge);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param); 

        return ds.Tables[0];
    }


    public DataTable AddEditBoutiqueInvoiceDetail()
    {
        SqlParameter[] param = new SqlParameter[26];
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditBoutiqueInvoiceDetail";

        if (DataFrom == "SALESINV")
        {
           param = new SqlParameter[24];
        }

        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@ItemCode", ItemCode);
        param[2] = new SqlParameter("@Quantity", Quantity);
        param[3] = new SqlParameter("@Rate", Rate);
        param[4] = new SqlParameter("@GrossAmount", GrossAmount);
        param[5] = new SqlParameter("@SaleTax", SaleTax);
        param[6] = new SqlParameter("@SaleTaxAmount", SaleTaxAmount);
        param[7] = new SqlParameter("@AmountIncludeTaxes", AmountIncludeTaxes);
        param[8] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[9] = new SqlParameter("@CustomerCode", CustomerCode);
        param[10] = new SqlParameter("@DataFrom", DataFrom);
    

        if (DataFrom == "GSTINVSALE" || DataFrom == "SALESINV" || DataFrom == "GSTINVPURRET" || DataFrom == "PURRETINV")
        {
            Quantity = 0 - Quantity;
            param[11] = new SqlParameter("@TransQty", Quantity);
         
        }
        else
        {
            param[11] = new SqlParameter("@TransQty", Quantity);          
        }
        param[12] = new SqlParameter("@storeId", storeId);

        param[13] = new SqlParameter("@BoutiquePurchaseDiscount", BoutiquePurchaseDiscount);
        param[14] = new SqlParameter("@BoutiqueCostPrice", BoutiqueCostPrice);
        param[15] = new SqlParameter("@BoutiqueSalesDiscount", BoutiqueSalesDiscount);
        param[16] = new SqlParameter("@BoutiqueSalePrice", BoutiqueSalePrice);


        param[17] = new SqlParameter("@BoutiqueConvRate", BoutiqueConvRate);
        param[18] = new SqlParameter("@BoutiqueGrossAmountFC", BoutiqueGrossAmountFC);

        //if (DataFrom == "SALESINV")
        //{
            param[19] = new SqlParameter("@AlterCharges", AlterCharges);
            param[20] = new SqlParameter("@DRID", DRID);
            param[21] = new SqlParameter("@CRID", CRID);
            param[22] = new SqlParameter("@BoutiqueDiscount", BoutiqueDiscount);
            param[23] = new SqlParameter("@BoutiqueAmountAfterDis", BoutiqueAmountAfterDis);
            
      //  }

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param); 

        return ds.Tables[0];
    }
    public DataTable AddEditBoutiqueOrderDetail()
    {
        SqlParameter[] param = new SqlParameter[19];
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditBoutiqueOrderDetail";
        if (DataFrom == "BOUTORDER")
        {
            param = new SqlParameter[24];
        }
        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@ItemCode", ItemCode);
        param[2] = new SqlParameter("@Quantity", Quantity);
        param[3] = new SqlParameter("@Rate", Rate);
        param[4] = new SqlParameter("@GrossAmount", GrossAmount);
        param[5] = new SqlParameter("@SaleTax", SaleTax);
        param[6] = new SqlParameter("@SaleTaxAmount", SaleTaxAmount);
        param[7] = new SqlParameter("@AmountIncludeTaxes", AmountIncludeTaxes);
        param[8] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[9] = new SqlParameter("@CustomerCode", CustomerCode);
        param[10] = new SqlParameter("@DataFrom", DataFrom);
        if (DataFrom == "GSTINVSALE" || DataFrom == "BOUTORDER" || DataFrom == "GSTINVPURRET" || DataFrom == "PURRETINV")
        {
            Quantity = 0 - Quantity;
            param[11] = new SqlParameter("@TransQty", Quantity);
        }
        else
        {
            param[11] = new SqlParameter("@TransQty", Quantity);
        }
        param[12] = new SqlParameter("@storeId", storeId);
        param[13] = new SqlParameter("@BoutiquePurchaseDiscount", BoutiquePurchaseDiscount);
        param[14] = new SqlParameter("@BoutiqueCostPrice", BoutiqueCostPrice);
        param[15] = new SqlParameter("@BoutiqueSalesDiscount", BoutiqueSalesDiscount);
        param[16] = new SqlParameter("@BoutiqueSalePrice", BoutiqueSalePrice);
        param[17] = new SqlParameter("@BoutiqueConvRate", BoutiqueConvRate);
        param[18] = new SqlParameter("@BoutiqueGrossAmountFC", BoutiqueGrossAmountFC);
        if (DataFrom == "BOUTORDER")
        {
            param[19] = new SqlParameter("@AlterCharges", AlterCharges);
            param[20] = new SqlParameter("@DRID", DRID);
            param[21] = new SqlParameter("@CRID", CRID);
            param[22] = new SqlParameter("@BoutiqueDiscount", BoutiqueDiscount);
            param[23] = new SqlParameter("@BoutiqueAmountAfterDis", BoutiqueAmountAfterDis);
        }

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }

    public DataTable AddEditInvoiceDetail ()
    {
        SqlParameter[] param = new SqlParameter[19];
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditInvoiceDetail";

       

        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@ItemCode", ItemCode);
        param[2] = new SqlParameter("@Quantity", Quantity);
        param[3] = new SqlParameter("@Rate", Rate);
        param[4] = new SqlParameter("@GrossAmount", GrossAmount);
        param[5] = new SqlParameter("@SaleTax", SaleTax);
        param[6] = new SqlParameter("@SaleTaxAmount", SaleTaxAmount);
        param[7] = new SqlParameter("@AmountIncludeTaxes", AmountIncludeTaxes);
        param[8] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[9] = new SqlParameter("@CustomerCode", CustomerCode);
        param[10] = new SqlParameter("@DataFrom", DataFrom);


        if (DataFrom == "GSTINVSALE" || DataFrom == "SALESINV" || DataFrom == "GSTINVPURRET" || DataFrom == "PURRETINV")
        {
            Quantity = 0 - Quantity;
            param[11] = new SqlParameter("@TransQty", Quantity);

        }
        else
        {
            param[11] = new SqlParameter("@TransQty", Quantity);
        }
        param[12] = new SqlParameter("@storeId", storeId);

        param[13] = new SqlParameter("@BoutiquePurchaseDiscount", BoutiquePurchaseDiscount);
        param[14] = new SqlParameter("@BoutiqueCostPrice", BoutiqueCostPrice);
        param[15] = new SqlParameter("@BoutiqueSalesDiscount", BoutiqueSalesDiscount);
        param[16] = new SqlParameter("@BoutiqueSalePrice", BoutiqueSalePrice);


        param[17] = new SqlParameter("@BoutiqueConvRate", BoutiqueConvRate);
        param[18] = new SqlParameter("@BoutiqueGrossAmountFC", BoutiqueGrossAmountFC);

       
          
      

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }
    public DataTable BoutiqueAddEditInvoiceDetail()
    {
        SqlParameter[] param = new SqlParameter[19];
        DataSet ds = new DataSet();
        string sqlCommand = "proc_BoutiqueAddEditInvoiceDetail";



        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@ItemCode", ItemCode);
        param[2] = new SqlParameter("@Quantity", Quantity);
        param[3] = new SqlParameter("@Rate", Rate);
        param[4] = new SqlParameter("@GrossAmount", GrossAmount);
        param[5] = new SqlParameter("@SaleTax", SaleTax);
        param[6] = new SqlParameter("@SaleTaxAmount", SaleTaxAmount);
        param[7] = new SqlParameter("@AmountIncludeTaxes", AmountIncludeTaxes);
        param[8] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[9] = new SqlParameter("@CustomerCode", CustomerCode);
        param[10] = new SqlParameter("@DataFrom", DataFrom);


        if (DataFrom == "GSTINVSALE" || DataFrom == "SALESINV" || DataFrom == "GSTINVPURRET" || DataFrom == "PURRETINV")
        {
            Quantity = 0 - Quantity;
            param[11] = new SqlParameter("@TransQty", Quantity);

        }
        else
        {
            param[11] = new SqlParameter("@TransQty", Quantity);
        }
        param[12] = new SqlParameter("@storeId", storeId);

        param[13] = new SqlParameter("@BoutiquePurchaseDiscount", BoutiquePurchaseDiscount);
        param[14] = new SqlParameter("@BoutiqueCostPrice", BoutiqueCostPrice);
        param[15] = new SqlParameter("@BoutiqueSalesDiscount", BoutiqueSalesDiscount);
        param[16] = new SqlParameter("@BoutiqueSalePrice", BoutiqueSalePrice);


        param[17] = new SqlParameter("@BoutiqueConvRate", BoutiqueConvRate);
        param[18] = new SqlParameter("@BoutiqueGrossAmountFC", BoutiqueGrossAmountFC);





        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }

    public DataTable AddEditBoutiquePurchaseInvoiceDetail()
    {
        SqlParameter[] param = new SqlParameter[20];
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditBoutiquePurchaseInvoiceDetail";



        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@ItemCode", ItemCode);
        param[2] = new SqlParameter("@Quantity", Quantity);
        param[3] = new SqlParameter("@Rate", Rate);
        param[4] = new SqlParameter("@GrossAmount", GrossAmount);
        param[5] = new SqlParameter("@SaleTax", SaleTax);
        param[6] = new SqlParameter("@SaleTaxAmount", SaleTaxAmount);
        param[7] = new SqlParameter("@AmountIncludeTaxes", AmountIncludeTaxes);
        param[8] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[9] = new SqlParameter("@CustomerCode", CustomerCode);
        param[10] = new SqlParameter("@DataFrom", DataFrom);


        if (DataFrom == "GSTINVSALE" || DataFrom == "SALESINV" || DataFrom == "GSTINVPURRET" || DataFrom == "PURRETINV")
        {
            Quantity = 0 - Quantity;
            param[11] = new SqlParameter("@TransQty", Quantity);

        }
        else
        {
            param[11] = new SqlParameter("@TransQty", Quantity);
        }
        param[12] = new SqlParameter("@storeId", storeId);

        param[13] = new SqlParameter("@BoutiquePurchaseDiscount", BoutiquePurchaseDiscount);
        param[14] = new SqlParameter("@BoutiqueCostPrice", BoutiqueCostPrice);
        param[15] = new SqlParameter("@BoutiqueSalesDiscount", BoutiqueSalesDiscount);
        param[16] = new SqlParameter("@BoutiqueSalePrice", BoutiqueSalePrice);


        param[17] = new SqlParameter("@BoutiqueConvRate", BoutiqueConvRate);
        param[18] = new SqlParameter("@BoutiqueGrossAmountFC", BoutiqueGrossAmountFC);
        param[19] = new SqlParameter("@PurchaseType", PurchaseType);




        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }

    public DataTable addBoutiqueSalePrice()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_addBoutiqueSalePrice";


        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@ItemId", ItemId);
        param[2] = new SqlParameter("@SaleRate", SaleRate);
        param[3] = new SqlParameter("@PurchaseRate", PurchaseRate);
        param[4] = new SqlParameter("@PurchaseType", PurchaseType);

        
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }
    public DataTable AddEditInvoiceDetailStock()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditInvoiceDetailStock";


        SqlParameter[] param = new SqlParameter[13];
        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@ItemCode", ItemCode);
        param[2] = new SqlParameter("@Quantity", Quantity);
        param[3] = new SqlParameter("@Rate", Rate);
        param[4] = new SqlParameter("@GrossAmount", GrossAmount);
        param[5] = new SqlParameter("@SaleTax", SaleTax);
        param[6] = new SqlParameter("@SaleTaxAmount", SaleTaxAmount);
        param[7] = new SqlParameter("@AmountIncludeTaxes", AmountIncludeTaxes);
        param[8] = new SqlParameter("@InvoiceSummaryId", ProdNConsMainId);
        param[9] = new SqlParameter("@CustomerCode", CustomerCode);
        param[10] = new SqlParameter("@DataFrom", DataFrom);
        param[11] = new SqlParameter("@TransQty", TransQty);
        param[12] = new SqlParameter("@storeId", storeId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }

    public DataTable AddEditInvoiceDetailStockTransfer()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditInvoiceDetailStockTransfer";


        SqlParameter[] param = new SqlParameter[13];
        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@ItemCode", ItemCode);
        param[2] = new SqlParameter("@Quantity", Quantity);
        param[3] = new SqlParameter("@Rate", Rate);
        param[4] = new SqlParameter("@GrossAmount", GrossAmount);
        param[5] = new SqlParameter("@SaleTax", SaleTax);
        param[6] = new SqlParameter("@SaleTaxAmount", SaleTaxAmount);
        param[7] = new SqlParameter("@AmountIncludeTaxes", AmountIncludeTaxes);
        param[8] = new SqlParameter("@InvoiceSummaryId", TransferSummaryId);
        param[9] = new SqlParameter("@CustomerCode", CustomerCode);
        param[10] = new SqlParameter("@DataFrom", DataFrom);
        param[11] = new SqlParameter("@TransQty", TransQty);
        param[12] = new SqlParameter("@storeId", storeId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }


    public DataTable getCharges()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getCharges";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@ItemId", ItemId);
        param[1] = new SqlParameter("@InvoiceNo", InvoiceNo);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public DataTable AddEditTrans()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_AddEditTrans";       
        SqlParameter[] param = new SqlParameter[21];
        param[0] = new SqlParameter("@TransId", TransId);
        param[1] = new SqlParameter("@AcFileId", AcFileId);
        param[2] = new SqlParameter("@TransactionDate", TransactionDate);
        param[3] = new SqlParameter("@TransAmt", TransAmt);
        param[4] = new SqlParameter("@Description", Description);
        param[5] = new SqlParameter("@DataFrom", DataFrom);
        param[6] = new SqlParameter("@InvoiceNo", InvoiceNo);
        param[7] = new SqlParameter("@ChqNo", ChqNo);
        param[8] = new SqlParameter("@ChqDate", ChqDate);
        param[9] = new SqlParameter("@Crdays", Crdays);
        param[10] = new SqlParameter("@DueDate", DueDate);
        param[11] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[12] = new SqlParameter("@CustomerCode", CustomerCode);
        param[13] = new SqlParameter("@rate", rate);
        param[14] = new SqlParameter("@QuantityKg", QuantityKg);
        param[15] = new SqlParameter("@QuantityLbs", QuantityLbs);
        param[16] = new SqlParameter("@ItemId", ItemId);
        param[17] = new SqlParameter("@ReceiptNo", ReceiptNo);
        param[18] = new SqlParameter("@FCAmount", FCAmount);
        param[19] = new SqlParameter("@FCRate", FCRate);
        param[20] = new SqlParameter("@VinId", VinId); 
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }








    public DataTable DeleteTrans()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "Proc_deleteTrans";  
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }




    public DataTable getInvoiceSummary()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getInvoiceSummary";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param); 
        return ds.Tables[0];


    }
    public DataTable HambalgetInvoiceSummary()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_HambalgetInvoiceSummary";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param); 
        return ds.Tables[0];

    }

    
    public DataTable getBoutiqueInvoiceSummary()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getBoutiqueInvoiceSummary";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];


    }
    public DataTable getBoutiqueInvoiceInvNo()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getBoutiqueInvoiceInvNo";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];


    }
    public DataTable getBoutiqueOrderSummary()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getBoutiqueOrderSummary";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param); 
        return ds.Tables[0];


    }


    
    public DataTable getCustomerSummeryByContactNumber()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getCustomerSummeryByContactNumber";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ContactNumber", ContactNumber);
      
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];


    }

    public DataTable getTransferSummary()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getTransferSummary";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@TransferSummaryId", TransferSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];


    }

    public DataTable getProdNConsMain()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getProdNConsMain";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ProdNConsMainId", ProdNConsMainId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];


    }

    public DataTable SearchInvoice()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchInvoice";
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@InvNo", InvNo);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        param[3] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[4] = new SqlParameter("@invDateTo", invDateTo);
        param[5] = new SqlParameter("@ClientAccountId", ClientAccountId);
        param[6] = new SqlParameter("@CostCenterCode", CostCenterCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }
    public DataTable SearchBoutiqueOrder()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchBoutiqueOrder";
        SqlParameter[] param = new SqlParameter[8];
        param[0] = new SqlParameter("@InvNo", InvNo);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        param[3] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[4] = new SqlParameter("@invDateTo", invDateTo);
        param[5] = new SqlParameter("@ClientAccountId", ClientAccountId);
        param[6] = new SqlParameter("@CostCenterCode", CostCenterCode);
        param[7] = new SqlParameter("@OrderStatus", OrderStatus);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }


    public DataTable SearchCarInventory()
    {

   
        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchCarInventory";
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@VinId", VinId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);      
        param[3] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[4] = new SqlParameter("@invDateTo", invDateTo);
        param[5] = new SqlParameter("@PartnerAcFileID", PartnerAcFileID);
        
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }
    public DataTable SearchCarPurchase()
    {


        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchCarPurchase";
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@VinId", VinId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[4] = new SqlParameter("@invDateTo", invDateTo);
        param[5] = new SqlParameter("@VendorAcfileId", VendorAcfileId);
        param[6] = new SqlParameter("@PurchaseAcfileId", PurchaseAcfileId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }

    public DataTable SearchCarRepair()
    {


        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchCarRepair";
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@VinId", VinId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[4] = new SqlParameter("@invDateTo", invDateTo);
        param[5] = new SqlParameter("@VendorAcfileId", VendorAcfileId);
        param[6] = new SqlParameter("@RepairAcfileId", RepairAcfileId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }






    public DataTable SearchCarLoding()
    {


        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchCarCarLoding";
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@CarLodingId", CarLodingId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[4] = new SqlParameter("@invDateTo", invDateTo);
        param[5] = new SqlParameter("@LoaderAcfileId", LoaderAcfileId);
        param[6] = new SqlParameter("@LoadingExpAcfileId", LoadingExpAcfileId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }


    public DataTable SearchCarSale()
    {


        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchCarSale";
        SqlParameter[] param = new SqlParameter[8];
        param[0] = new SqlParameter("@VinId", VinId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[4] = new SqlParameter("@invDateTo", invDateTo);
        param[5] = new SqlParameter("@BuyerAcfileId", BuyerAcfileId);
        param[6] = new SqlParameter("@BrokerAcfileId", BrokerAcfileId);   
        param[7] = new SqlParameter("@SalesPersonCreditAcfileId", SalesPersonCreditAcfileId);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }


    public DataTable SearchCarLocalCustoms()
    {


        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchCarLocalCustoms";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@ContainerNoId", ContainerNoId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[4] = new SqlParameter("@invDateTo", invDateTo);
       

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }


    public DataTable SearchCarExpenseOnContainer()
    {


        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchCarExpenseOnContainer";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@ContainerNoId", ContainerNoId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[4] = new SqlParameter("@invDateTo", invDateTo);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }

    public DataTable SearchCarExpenseOnVinNo()
    {


        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchCarExpenseOnVinNo";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@VinId", VinId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[4] = new SqlParameter("@invDateTo", invDateTo);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }


    public DataTable SearchCarTitle()
    {


        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchCarTitle";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@VinId", VinId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[4] = new SqlParameter("@invDateTo", invDateTo);


        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }
    public DataTable SearchCarToeing()
    {


        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchCarCarToeing";
        SqlParameter[] param = new SqlParameter[9];
        param[0] = new SqlParameter("@VinId", VinId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[3] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[4] = new SqlParameter("@invDateTo", invDateTo);
        param[5] = new SqlParameter("@VendorAcfileId1", VendorAcfileId1);
        param[6] = new SqlParameter("@ToeingAcfileId1", ToeingAcfileId1);
        param[7] = new SqlParameter("@VendorAcfileId2", VendorAcfileId2);
        param[8] = new SqlParameter("@ToeingAcfileId2", ToeingAcfileId2);



        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }


    public DataTable SearchCashbook()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchCashbook";
        SqlParameter[] param = new SqlParameter[5];
      
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@DataFrom", DataFrom);
        param[2] = new SqlParameter("@CashBookDateFrom", CashBookDateFrom);
        param[3] = new SqlParameter("@CashBookDateTo", CashBookDateTo);
        param[4] = new SqlParameter("@AcFileId", AcFileId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }


    public DataTable SearchVoucher()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchVoucher";
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@VoucherNo", VoucherNo);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        param[3] = new SqlParameter("@VoucherDateFrom", VoucherDateFrom);
        param[4] = new SqlParameter("@VoucherDateTo", VoucherDateTo);
        param[5] = new SqlParameter("@BankAcId", BankAcId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds.Tables[0];
    }


    public DataTable SearchServiceInvoice()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchServiceInvoice";
        SqlParameter[] param = new SqlParameter[8];
        param[0] = new SqlParameter("@InvoiceNo", ServiceInvoiceNo);/*---*/
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        param[3] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[4] = new SqlParameter("@invDateTo", invDateTo);
        param[5] = new SqlParameter("@DebitAcId", DebitAcId);
        param[6] = new SqlParameter("@CreditAcId", CreditAcId);
        param[7] = new SqlParameter("@CostCenterCode", CostCenterCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];

    }

    public DataTable SearchJournalVoucher()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchJournalVoucher";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@VoucherNo", VoucherNo);/*---*/
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        param[3] = new SqlParameter("@VoucherDateFrom", VoucherDateFrom);
        param[4] = new SqlParameter("@VoucherDateTo", VoucherDateTo);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];

    }

    public DataTable SearchStockTransferSummary()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchStockTransferSummary";
        SqlParameter[] param = new SqlParameter[7];
        param[0] = new SqlParameter("@VoucherNo", VoucherNo);/*---*/
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        param[3] = new SqlParameter("@VoucherDateFrom", VoucherDateFrom);
        param[4] = new SqlParameter("@VoucherDateTo", VoucherDateTo);
        param[5] = new SqlParameter("@StoreFromId", StoreFromId);
        param[6] = new SqlParameter("@StoreToId", StoreToId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];

    }
    public DataTable SearchStockProdNConsMain()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_SearchStockProdNConsMain";
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@VoucherNo", VoucherNo);/*---*/
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        param[3] = new SqlParameter("@VoucherDateFrom", VoucherDateFrom);
        param[4] = new SqlParameter("@VoucherDateTo", VoucherDateTo);
        param[5] = new SqlParameter("@StoreId", StoreId);
       
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];

    }
    

    public DataTable Searchbrokerage()
    {


        DataSet ds = new DataSet();
        string sqlCommand = "proc_Searchbrokerage";
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@SoldToId", SoldToId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        param[3] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[4] = new SqlParameter("@invDateTo", invDateTo);
        param[5] = new SqlParameter("@ItemId", ItemId);    
    
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];

    }


    public DataTable getVoucherSimpleDetail()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getVoucherSimpleDetail";
       
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        param[3] = new SqlParameter("@VoucherSimpleMainId", VoucherSimpleMainId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable getVoucherSimpleMain()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getVoucherSimpleMain";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);      
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }


    public DataTable BoutiqueDayBookReport()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_BoutiqueDayBookReport";      
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@DateTo", invDateTo);
        param[1] = new SqlParameter("@DateFrom", InvDateFrom);  
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];

    }


    public DataTable getCashBookDetail()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getCashBookDetail";

        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        param[3] = new SqlParameter("@CashBookMainId", CashBookMainId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    //public DataTable getCashBookDetailOld()
    //{

    //    DataSet ds = new DataSet();
    //    string sqlCommand = "proc_getCashBookDetailOld";

    //    SqlParameter[] param = new SqlParameter[5];
    //    param[0] = new SqlParameter("@id", id);
    //    param[1] = new SqlParameter("@CustomerCode", CustomerCode);
    //    param[2] = new SqlParameter("@DataFrom", DataFrom);
    //    param[3] = new SqlParameter("@CashBookMainId", CashBookMainId);
    //    param[4] = new SqlParameter("@CashBookAcfileID", CashBookAcfileID);
    //    ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
    //    return ds.Tables[0];
    //}

    public DataTable getCashBook()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getCashBook";       
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@id", id);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }



    public DataTable getYarnbrokerage()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getYarnbrokerage";       
       
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@BrokerageId", InvoiceSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }


    public DataTable GetCustomerDetails()
    {

       // DataSet ds = new DataSet();
        //string sqlCommand = "GetCustomerDetails";        
        //SqlParameter[] param = new SqlParameter[1];
        //param[0] = new SqlParameter("@CustomerCode", CustomerCode);       
        //ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        //return ds.Tables[0];
        DataTable dt = new DataTable();
        //if (Sessions.CustomerDetails != null)
        //{
        //    dt = (DataTable)Sessions.CustomerDetails;
        //}
        //else
        //{GetCustomerDetails
            DataSet ds = new DataSet();
            con = new SqlConnection();
            con.ConnectionString = conStr;
            con.Open();
            cm = new SqlCommand();
            cm.CommandText = "GetCustomerDetails";
            cm.CommandType = CommandType.StoredProcedure;
            cm.Connection = con;
            cm.Parameters.AddWithValue("@CustomerCode", Convert.ToDecimal(Sessions.CustomerCode) );
            da = new SqlDataAdapter();
            da.SelectCommand = cm;
            da.Fill(ds);
            con.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                Sessions.CustomerDetails = ds.Tables[0];
                dt = (DataTable)Sessions.CustomerDetails;
            }
        //}
        return dt;

    }
    
    public DataTable getInvoiceDetails()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getInvoiceDetails";       
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable getInvoiceDetailsCrccount()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getInvoiceDetailsCrccount";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable getBoutiqueOrderDetails()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getBoutiqueOrderDetails";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public DataTable getInvoiceDetailsStock()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getInvoiceDetailsStock";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", TransferSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public DataTable getInvoiceDetailsStockAdjustment()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getInvoiceDetailsStockAdjustment";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", TransferSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public DataTable getInvoiceDetailsConsumption()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getInvoiceDetailsConsumption";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public DataTable getInvoiceDetailsProduction()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getInvoiceDetailsProduction";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }



    public DataTable getAcFileBySundryDebtor()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getAcFileBySundryDebtor";      
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable getAcFileBySundryDebtorOnly()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getAcFileBySundryDebtorOnly";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public DataTable getAcFileBySaleTaxAccount()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getAcFileBySaleTaxAccount";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable getAcFileBySaleAccount()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getAcFileBySaleAccount";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public DataTable getAcFileByDirectIncome()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getAcFileByDirectIncome";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public DataTable getAcFileByCreditorDebtorOnly()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getAcFileByCreditorDebtorOnly";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable getAcFileByCashBankAccount()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getAcFileByCashBankAccount";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable getAcFileByCashInHand()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getAcFileByCashInHand";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable getAcFileByBankaccount()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getAcFileByBankaccount";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable getAcFileAllLedger()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getAcFileAllLedger";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable getAcFileBySundryCreditor()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getAcFileBySundryCreditor";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable getAcFileByPurchase()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getAcFileByPurchase";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable getAcFileBySales()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_getAcFileBySales";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public int deleteAllInvoiceData()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_deleteAllInvoiceData";      
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return  Convert.ToInt32(ds.Tables[0].Rows[0]["InvoiceSummaryId"]);

    }
    public int deleteAllBoutiqueOrderData()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_deleteAllBoutiqueOrderData";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return Convert.ToInt32(ds.Tables[0].Rows[0]["InvoiceSummaryId"]);

    }

    public int deleteAllTransferSummary()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_deleteAllTransferSummary";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@TransferSummaryId", TransferSummaryId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return Convert.ToInt32(ds.Tables[0].Rows[0]["TransferSummaryId"]);

    }

    public int deleteAllProdNConsMain()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_deleteAllProdNConsMain";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ProdNConsMainId", ProdNConsMainId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return Convert.ToInt32(ds.Tables[0].Rows[0]["ProdNConsMainId"]);

    }
     public int deleteAllbrokerageData()
    {

        DataSet ds = new DataSet();
        string sqlCommand = "proc_deleteAllbrokerageData";       
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@BrokerageId", BrokerageId);
        param[1] = new SqlParameter("@CustomerCode", CustomerCode);
        param[2] = new SqlParameter("@DataFrom", DataFrom);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return  Convert.ToInt32(ds.Tables[0].Rows[0]["BrokerageId"]);

    }

     public int deleteAllCashBook()
     {

         DataSet ds = new DataSet();
         string sqlCommand = "proc_deleteAllCashBook";         
         SqlParameter[] param = new SqlParameter[3];
         param[0] = new SqlParameter("@CashBookMainId", CashBookMainId);
         param[1] = new SqlParameter("@CustomerCode", CustomerCode);
         param[2] = new SqlParameter("@DataFrom", DataFrom);
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return Convert.ToInt32(ds.Tables[0].Rows[0]["CashBookMainId"]);

     }
     public int deleteAllVoucher()
     {

         DataSet ds = new DataSet();
         string sqlCommand = "proc_deleteAllVoucher";
        
         SqlParameter[] param = new SqlParameter[3];
         param[0] = new SqlParameter("@VoucherSimpleMainId", VoucherSimpleMainId);
         param[1] = new SqlParameter("@CustomerCode", CustomerCode);
         param[2] = new SqlParameter("@DataFrom", DataFrom);
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return Convert.ToInt32(ds.Tables[0].Rows[0]["VoucherSimpleMainId"]);

     }





     public DataSet GetTrialBalance()
     {
         DataSet ds = new DataSet();
         string sqlCommand = "proc_GetTrialBalance";        
         SqlParameter[] param = new SqlParameter[3];
         param[0] = new SqlParameter("@TransactionDate", TransactionDate);
         param[1] = new SqlParameter("@CustomerCode", CustomerCode);
         param[2] = new SqlParameter("@id", id);
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds;
     }
     public DataSet GetProfitAndLoss()
     {
         DataSet ds = new DataSet();
         string sqlCommand = "proc_GetProfitAndLoss";
         SqlParameter[] param = new SqlParameter[4];
         param[0] = new SqlParameter("@DateFrom", DateFrom);
         param[1] = new SqlParameter("@CustomerCode", CustomerCode);
         param[2] = new SqlParameter("@id", id);
         param[3] = new SqlParameter("@DateTo", DateTo);
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds;
     }
     public DataSet GetProfitAndLossList()
     {
         DataSet ds = new DataSet();
         string sqlCommand = "proc_GetProfitAndLossList";
         SqlParameter[] param = new SqlParameter[3];
         param[0] = new SqlParameter("@AsOn", AsOn);
         param[1] = new SqlParameter("@CustomerCode", CustomerCode);
         param[2] = new SqlParameter("@id", id);
        
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds;
     }


     public DataSet GetCharOfAccountreport()
     {
         DataSet ds = new DataSet();
         string sqlCommand = "proc_GetCharOfAccountreport";        
         SqlParameter[] param = new SqlParameter[2];       
         param[0] = new SqlParameter("@CustomerCode", CustomerCode);
         param[1] = new SqlParameter("@id", id);
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds;
     }

     public DataTable getClient()
     {
         DataSet ds = new DataSet();
         string sqlCommand = "proc_getClient";
         SqlParameter[] param = new SqlParameter[2];      
         param[0] = new SqlParameter("@id", id);
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds.Tables[0];
     }
     public DataSet GetSequenceTrialBalance()
     {
         DataSet ds = new DataSet();
         string sqlCommand = "proc_GetSequenceTrialBalance";       
         SqlParameter[] param = new SqlParameter[1];
         param[0] = new SqlParameter("@CustomerCode", Convert.ToDecimal(Sessions.CustomerCode));
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds;
     }

     public DataSet GetSequenceTrialBalanceByGroup()
     {
         DataSet ds = new DataSet();
         string sqlCommand = "proc_GetSequenceTrialBalanceByGroup";
         SqlParameter[] param = new SqlParameter[2];
         param[0] = new SqlParameter("@CustomerCode", Convert.ToDecimal(Sessions.CustomerCode));
         param[1] = new SqlParameter("@id", id);
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds;
     }


     public DataSet GetSequenceProfitAndLoss()
     {
         DataSet ds = new DataSet();
         string sqlCommand = "proc_GetSequenceProfitAndLoss";
         SqlParameter[] param = new SqlParameter[1];
         param[0] = new SqlParameter("@CustomerCode", Convert.ToDecimal(Sessions.CustomerCode));
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds;
     }

     public DataSet GetSequencePeriodicTransaction()
     {
         DataSet ds = new DataSet();
         string sqlCommand = "proc_GetSequencePeriodicTransaction";
         SqlParameter[] param = new SqlParameter[2];
         param[0] = new SqlParameter("@CustomerCode", Convert.ToDecimal(Sessions.CustomerCode));
         param[1] = new SqlParameter("@AcfileId", AcfileID);
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds;
     }

     public DataSet GetSequenceChartOfAccount()
     {
         DataSet ds = new DataSet();
         string sqlCommand = "proc_GetSequenceChartOfAccount";       
         SqlParameter[] param = new SqlParameter[1];
         param[0] = new SqlParameter("@CustomerCode", Convert.ToDecimal(Sessions.CustomerCode));
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds;
     }


    
     public DataSet getBrodForwordBal()
     {
         DataSet ds = new DataSet();
         string sqlCommand = "proc_getBrodForwordBal";         
         SqlParameter[] param = new SqlParameter[3];
         param[0] = new SqlParameter("@AcFileId", AcFileId);
         param[1] = new SqlParameter("@CustomerCode", Convert.ToDecimal(Sessions.CustomerCode));
         param[2] = new SqlParameter("@CashBookDate", CashBookDate);
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds;
     }


     public DataSet CashbookDetails()
     {
         DataSet ds = new DataSet();
         string sqlCommand = "proc_CashbookDetails";
         
         SqlParameter[] param = new SqlParameter[4];
         param[0] = new SqlParameter("@DataFrom", DataFrom);
         param[1] = new SqlParameter("@CustomerCode", Convert.ToDecimal(Sessions.CustomerCode));
         param[2] = new SqlParameter("@CashBookDate", CashBookDate);
         param[3] = new SqlParameter("@AcfileID", AcfileID);
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

         return ds;
     }




     #region ServiceInvoice

     public DataTable AddEditServiceInvoiceMain()
     {

     
         DataSet ds = new DataSet();
         string sqlCommand = "proc_AddEditServiceInvoiceMain";         

         SqlParameter[] param = new SqlParameter[10];
         param[0] = new SqlParameter("@id", id);
         param[1] = new SqlParameter("@InvoiceNo", InvoiceNos);
         param[2] = new SqlParameter("@InvDate", InvDate);
         param[3] = new SqlParameter("@DebitAcId", DebitAcId);
         param[4] = new SqlParameter("@CreditAcId", CreditAcId);
         param[5] = new SqlParameter("@TotalAmount", TotalAmount);
         param[6] = new SqlParameter("@DataFrom", DataFrom);
         param[7] = new SqlParameter("@CustomerCode", CustomerCode);
         param[8] = new SqlParameter("@Description", Description);
         param[9] = new SqlParameter("@storeId", storeId);

         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds.Tables[0];
     }

     public DataTable AddEditServiceInvoiceDetail()
     {

         DataSet ds = new DataSet();
         string sqlCommand = "proc_AddEditServiceInvoiceDetail";
         SqlParameter[] param = new SqlParameter[7];
         param[0] = new SqlParameter("@id", id);
         param[1] = new SqlParameter("@Amount", Amount);
         param[2] = new SqlParameter("@Description", Description);
         param[3] = new SqlParameter("@ServiceInvoiceMainId", ServiceInvoiceMainId);
         param[4] = new SqlParameter("@DataFrom", DataFrom);
         param[5] = new SqlParameter("@CustomerCode", CustomerCode);
       
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds.Tables[0];
     }

     public int deleteAllServiceInvoice()
     {

         DataSet ds = new DataSet();
         string sqlCommand = "proc_deleteAllServiceInvoice";   

         SqlParameter[] param = new SqlParameter[3];
         param[0] = new SqlParameter("@ServiceInvoiceMainId", ServiceInvoiceMainId);
         param[1] = new SqlParameter("@CustomerCode", CustomerCode);
         param[2] = new SqlParameter("@DataFrom", DataFrom);
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

         return Convert.ToInt32(ds.Tables[0].Rows[0]["ServiceInvoiceMainId"]);

     }

     public DataTable getServiceInvoiceDetail()
     {

         DataSet ds = new DataSet();
         string sqlCommand = "proc_getServiceInvoiceDetail";
         SqlParameter[] param = new SqlParameter[4];
         param[0] = new SqlParameter("@ServiceInvoiceMainId", ServiceInvoiceMainId);
         param[1] = new SqlParameter("@CustomerCode", Sessions.CustomerCode);
         param[2] = new SqlParameter("@DataFrom", DataFrom);
         param[3] = new SqlParameter("@id", id);
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds.Tables[0];
     }

     public DataTable getServiceInvoiceMain()
     {

         DataSet ds = new DataSet();
         string sqlCommand = "proc_getServiceInvoiceMain";
        
         SqlParameter[] param = new SqlParameter[3];
         param[0] = new SqlParameter("@id", id);
         param[1] = new SqlParameter("@CustomerCode", Sessions.CustomerCode);
         param[2] = new SqlParameter("@DataFrom", DataFrom);
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds.Tables[0];
     }

     #endregion






     public DataTable AddEditJvMain()
     {


         DataSet ds = new DataSet();
         string sqlCommand = "proc_AddEditJvMain";        

         SqlParameter[] param = new SqlParameter[8];
         param[0] = new SqlParameter("@id", id);
         param[1] = new SqlParameter("@VoucherNo", VoucherNo);
         param[2] = new SqlParameter("@VoucherDate", VoucherDate);
         param[3] = new SqlParameter("@TotalAmount", TotalAmount);
         param[4] = new SqlParameter("@DataFrom", DataFrom);
         param[5] = new SqlParameter("@CustomerCode", CustomerCode);
         param[6] = new SqlParameter("@FCAmountDebit ", FCAmountDebit);
         param[7] = new SqlParameter("@FCAmountCredit ", FCAmountCredit);

         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds.Tables[0];
     }

     public DataTable AddEditJvDetails()
     {

         DataSet ds = new DataSet();
         string sqlCommand = "proc_AddEditJvDetails ";
         SqlParameter[] param = new SqlParameter[12];
         param[0] = new SqlParameter("@id", id);
         param[1] = new SqlParameter("@AccountId", AccountId);
         param[2] = new SqlParameter("@DebitAmount", DebitAmount);
         param[3] = new SqlParameter("@CreditAmount", CreditAmount);
         param[4] = new SqlParameter("@Description", Description);
         param[5] = new SqlParameter("@JVMainId", JVMainId);
         param[6] = new SqlParameter("@DataFrom", DataFrom);
         param[7] = new SqlParameter("@CustomerCode", CustomerCode);
         param[8] = new SqlParameter("@FCAmountDebit ", FCAmountDebit);
         param[9] = new SqlParameter("@FCAmountCredit ", FCAmountCredit);
         param[10] = new SqlParameter("@FCRateDebit ", FCRateDebit);
         param[11] = new SqlParameter("@FCRateCredit ", FCRateCredit);


         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds.Tables[0];
     }
     public DataTable AddEditTmJvDetails()
     {

         DataSet ds = new DataSet();
         string sqlCommand = "proc_AddEditTmJvDetails ";
         SqlParameter[] param = new SqlParameter[13];
         param[0] = new SqlParameter("@id", id);
         param[1] = new SqlParameter("@AccountId", AccountId);
         param[2] = new SqlParameter("@DebitAmount", DebitAmount);
         param[3] = new SqlParameter("@CreditAmount", CreditAmount);
         param[4] = new SqlParameter("@Description", Description);
         param[5] = new SqlParameter("@JVMainId", JVMainId);
         param[6] = new SqlParameter("@DataFrom", DataFrom);
         param[7] = new SqlParameter("@CustomerCode", CustomerCode);
         param[8] = new SqlParameter("@FCAmountDebit ", FCAmountDebit);
         param[9] = new SqlParameter("@FCAmountCredit ", FCAmountCredit);
         param[10] = new SqlParameter("@FCRateDebit ", FCRateDebit);
         param[11] = new SqlParameter("@FCRateCredit ", FCRateCredit);
         param[12] = new SqlParameter("@RNumber ", RNumber);


         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds.Tables[0];
     }
     public int deleteAllJV()
     {

         DataSet ds = new DataSet();
         string sqlCommand = "proc_deleteAllJV";
       
         SqlParameter[] param = new SqlParameter[3];
         param[0] = new SqlParameter("@JVMainId", JVMainId);
         param[1] = new SqlParameter("@CustomerCode", CustomerCode);
         param[2] = new SqlParameter("@DataFrom", DataFrom);
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return Convert.ToInt32(ds.Tables[0].Rows[0]["JVMainId"]);

     }

     public DataTable getJvDetails()
     {

         DataSet ds = new DataSet();
         string sqlCommand = "proc_getJvDetails";        

         SqlParameter[] param = new SqlParameter[4];
         param[0] = new SqlParameter("@id", id);
         param[1] = new SqlParameter("@CustomerCode", CustomerCode);
         param[2] = new SqlParameter("@DataFrom", DataFrom);
         param[3] = new SqlParameter("@JVMainId", JVMainId);       
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds.Tables[0];
     }

     public DataTable getJvMain()
     {
         DataSet ds = new DataSet();
         string sqlCommand = "proc_getJvMain";
        
         SqlParameter[] param = new SqlParameter[3];
         param[0] = new SqlParameter("@id", id);
         param[1] = new SqlParameter("@CustomerCode", CustomerCode);
         param[2] = new SqlParameter("@DataFrom", DataFrom);
  
         ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         return ds.Tables[0];
     }

     public DataSet GetComparisionTrialBalance()
     {
         DataSet ds1 = new DataSet();

        
             string sqlCommand = "proc_GetComparisionTrialBalance";
             SqlParameter[] param = new SqlParameter[4];
             param[0] = new SqlParameter("@AcFileId", AcFileId);
             param[1] = new SqlParameter("@CustomerCode", CustomerCode);
             param[2] = new SqlParameter("@DateFrom", DateFrom);
             param[3] = new SqlParameter("@DateTo", DateTo);
             ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
         
         return ds1;
     }

}
