﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class ServiceInvoice : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();
    int Last = 0;
    int Corrent = 0;
    int FirstRecord = 0;
    int LastRecord = 0;
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();

        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        if (!IsPostBack)
        {
            txtInvNo.Focus();
            ViewState["s"] = null;
            FillAllLedgerAc();
            FillCostCenter();
            DataTable dt = new DataTable();
            dgGallery.DataSource = dt;
            dgGallery.DataBind();
            Session["Corrent"] = null;
            Session["FirstRecord"] = null;
            Session["LastRecord"] = null;
            Session["ServiceInvoiceSummary"] = null;
            if (Convert.ToString(Request["addnew"]) != "1")
            {
                bindpagging();

                if (Request["InvoiceSummaryId"] == null)
                {
                    BindServiceInvoice();
                }
                else
                {
                    DataTable dtInvoiceSummary = new DataTable();
                    dtInvoiceSummary = (DataTable)Session["ServiceInvoiceSummary"];
                    DataView dv = new DataView(dtInvoiceSummary);
                    dv.RowFilter = "id=" + Request["InvoiceSummaryId"];
                    dtInvoiceSummary = dv.ToTable();
                    if (dtInvoiceSummary.Rows.Count > 0)
                    {
                        Session["Corrent"] = Convert.ToInt32(dtInvoiceSummary.Rows[0]["row"]);
                        BindServiceInvoice();
                    }
                    else
                    {
                        Response.Redirect("default.aspx");
                    }
                }
            }
            else
            {
                objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objSalesInvoice.ServiceInvoiceMainId = 0;
                DataTable dtSERINV = new DataTable();
                objSalesInvoice.DataFrom = "SERINV";
                dtSERINV = objSalesInvoice.getServiceInvoiceMain();
                if (dtSERINV.Rows.Count > 0)
                {
                    int Last = Convert.ToInt32(dtSERINV.Rows.Count) - 1;
                    ddlCredit.SelectedValue = Convert.ToString(dtSERINV.Rows[Last]["CreditAcId"]);
                    txtInvNo.Text = Convert.ToString(Convert.ToDecimal(dtSERINV.Rows[Last]["InvoiceNo"]) + 1);
                    txtInvDate.Text = Convert.ToString(Convert.ToDateTime(dtSERINV.Rows[Last]["InvDate"]).ToString("dd MMM yyyy"));

                    spanLastInvoice.Style.Add("display", "");
                    litLastInvoiceNo.Text = Convert.ToString(dtSERINV.Rows[Last]["InvoiceNo"]);
                    litLastInvDate.Text = ClsGetDate.FillFromDate(Convert.ToString(dtSERINV.Rows[Last]["InvDate"]));
                }
                else
                {
                    txtInvNo.Text = "1";
                    txtInvDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
                }
                spanPgging.Style.Add("display", "none");
                btnDeleteInvoice.Style.Add("display", "none");
                btnPrint.Style.Add("display", "none");
            }
        }
        SetUserRight();
    }


    #endregion
    #region Fill Accounts

    protected void FillAllLedgerAc()
    {
      
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtLedger = new DataTable();
            dtLedger = objSalesInvoice.getAcFileBySundryDebtorOnly();
            if (dtLedger.Rows.Count > 0)
            {
              

                ddlDebit.DataSource = dtLedger;
                ddlDebit.DataTextField = "Title";
                ddlDebit.DataValueField = "Id";
                ddlDebit.DataBind();
                ddlDebit.Items.Insert(0, new ListItem("Select Debit A/C ", "0"));


            }

            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtLedgerAll = new DataTable();
            dtLedgerAll = objSalesInvoice.getAcFileByDirectIncome();
            if (dtLedgerAll.Rows.Count > 0)
            {
                ddlCredit.DataSource = dtLedgerAll;
                ddlCredit.DataTextField = "Title";
                ddlCredit.DataValueField = "Id";
                ddlCredit.DataBind();
                ddlCredit.Items.Insert(0, new ListItem("Select Credit A/C ", "0"));
            }
        

    }
    #endregion

    #region Add Details
    protected void btn_saveInvoice_Click(object sender, EventArgs   e)
    {
        hdnButtonText.Value = "";
        if (Sessions.FromDate != null)
        {
            if (ViewState["s"] != null)
            {
                if ((Convert.ToDateTime(txtInvDate.Text) >= Convert.ToDateTime(Sessions.FromDate)) && (Convert.ToDateTime(txtInvDate.Text) <= Convert.ToDateTime(Sessions.ToDate)))
                {
                    if (Convert.ToDecimal(lblAmount.Text) >= 0)
                    {
                        if (hdnServiceInvoiceMainId.Value == "")
                        {
                            hdnServiceInvoiceMainId.Value = "0";
                        }
                        /************ Add Invoice Summary **************/
                        if (Convert.ToInt32(hdnServiceInvoiceMainId.Value) > 0)
                        {
                            objSalesInvoice.id = Convert.ToInt32(hdnServiceInvoiceMainId.Value);
                        }
                        else
                        {
                            objSalesInvoice.id = 0;
                        }
                        objSalesInvoice.InvoiceNos = Convert.ToInt32(txtInvNo.Text);
                        objSalesInvoice.InvDate = Convert.ToDateTime(txtInvDate.Text);
                        objSalesInvoice.DebitAcId = Convert.ToInt32(ddlDebit.SelectedValue);
                        objSalesInvoice.CreditAcId = Convert.ToInt32(ddlCredit.SelectedValue);
                        objSalesInvoice.DataFrom = "SERINV";
                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                        objSalesInvoice.TotalAmount = Convert.ToDecimal(lblAmount.Text);
                        objSalesInvoice.Description = txtDescription.Text;
                        objSalesInvoice.storeId = Convert.ToInt32(ddlCostCenter.SelectedValue);
                        DataTable dtresult = new DataTable();
                        dtresult = objSalesInvoice.AddEditServiceInvoiceMain();

                        /************ Add Trans ***************/

                        objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["id"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                        objSalesInvoice.DataFrom = "SERINV";
                        objSalesInvoice.DeleteTrans();

                        for (int i = 0; i < 2; i++)
                        {
                            objSalesInvoice.TransId = 0;
                            objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvDate.Text);
                            if (i == 0)
                            {
                                objSalesInvoice.AcFileId = Convert.ToInt32(ddlDebit.SelectedValue);
                                objSalesInvoice.TransAmt = Convert.ToDecimal(lblAmount.Text);
                            }
                            if (i == 1)
                            {
                                objSalesInvoice.AcFileId = Convert.ToInt32(ddlCredit.SelectedValue);
                                objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(lblAmount.Text);
                            }
                            objSalesInvoice.Description = txtDescription.Text;
                            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            objSalesInvoice.DataFrom = "SERINV";
                            objSalesInvoice.InvoiceNo = txtInvNo.Text;
                            objSalesInvoice.AddEditTrans();

                        }




                        /************ Add Invoice Detail **************/
                        if (dtresult.Rows.Count > 0)
                        {
                            if (ViewState["s"] != null)
                            {
                                DataTable dt = new DataTable();
                                dt = (DataTable)ViewState["s"];
                                if (dt.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        objSalesInvoice.id = 0;
                                        objSalesInvoice.Amount = Convert.ToDecimal(dt.Rows[i]["Amount"].ToString());
                                        objSalesInvoice.Description = Convert.ToString(dt.Rows[i]["Description"].ToString());
                                        objSalesInvoice.ServiceInvoiceMainId = Convert.ToInt32(dtresult.Rows[0]["id"].ToString()); /***** dtresult.Rows[0] is for ServiceInvoiceMainId which is new****/
                                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                        objSalesInvoice.DataFrom = "SERINV";
                                        DataTable dtInvoiceSummary = new DataTable();
                                        dtInvoiceSummary = objSalesInvoice.AddEditServiceInvoiceDetail();
                                    }
                                }
                            }

                        }


                        if (Convert.ToInt32(hdnServiceInvoiceMainId.Value) > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Updated.');location.href='ServiceInvoice.aspx';", true);

                        }

                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Saved.');location.href='ServiceInvoice.aspx';", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Invalid Invoice Amount.');", true);

                    }
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Date out of accouting year.');", true);

                }

            }


            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please Fill  Details.');", true);
            }
        }
    }

    #endregion

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {

        Response.Redirect("ServiceInvoice.aspx");



    }
    #endregion
    #region Add New Bill details
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
        filltable();
        if (hdndetail.Value == "")
        {
            DataRow row;
            if (dttbl.Rows.Count > 0)
            {

                int maxid = 0;
                for (int i = 0; i < dttbl.Rows.Count; i++)
                {
                    if (dttbl.Rows[i]["Id"].ToString() != "")
                    {
                        if (maxid < Convert.ToInt32(dttbl.Rows[i]["Id"].ToString()))
                        {
                            maxid = Convert.ToInt32(dttbl.Rows[i]["Id"].ToString());
                        }
                    }
                }

                row = dttbl.NewRow();
                row["Id"] = maxid + 1;
                row["Amount"] = Convert.ToDecimal(txtAmount.Text);
                row["Description"] = Convert.ToString(txtDescriptionDetail.Text);


            }
            else
            {

                row = dttbl.NewRow();
                row["Id"] = 1;
                row["Amount"] = Convert.ToDecimal(txtAmount.Text);
                row["Description"] = Convert.ToString(txtDescriptionDetail.Text);

            }
            dttbl.Rows.Add(row);
            dttbl.AcceptChanges();
        }

        else
        {
          
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == hdndetail.Value)
                {

                    dttbl.Rows[i]["Amount"] = txtAmount.Text;
                    dttbl.Rows[i]["Description"] = txtDescriptionDetail.Text;
                   
                }
            }
        }
        ViewState.Add("s", dttbl);
        dgGallery.DataSource = dttbl;
        dgGallery.DataBind();
        ShowTotal();
        if (dttbl.Rows.Count > 0)
        {
            dgGallery.Visible = true;
        }

        ClearInvoiceDetail();
    }
    #endregion

    #region Grid Event
    protected void dgGallery_RowDataBound(object sender, DataGridItemEventArgs e)
    {




    }
    protected void dgGallery_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        dttbl = (DataTable)ViewState["s"];
        if (e.CommandName == "Delete")
        {
            int j = 0;
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    dttbl.Rows.RemoveAt(i);
                }
            }
            dttbl.AcceptChanges();
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            ViewState.Add("s", dttbl);
            ShowTotal();

        }
        if (e.CommandName == "Edit")
        {
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {


                    //ddlItemDesc.SelectedValue = dttbl.Rows[i]["ItemCode"].ToString();
                    //txtQuantity.Text = dttbl.Rows[i]["Quantity"].ToString();
                    //txtRate.Text = dttbl.Rows[i]["Rate"].ToString();
                    txtAmount.Text = dttbl.Rows[i]["Amount"].ToString();
                    txtDescriptionDetail.Text = dttbl.Rows[i]["Description"].ToString();
                    hdndetail.Value = dttbl.Rows[i]["Id"].ToString();


                }
            }
        }




    }
    #endregion

    #region ClearInvoiceDetail
    protected void ClearInvoiceDetail()
    {

        txtDescriptionDetail.Text = "";
        txtAmount.Text = "";
        txtDescriptionDetail.Text = "";
        hdndetail.Value = "";
    }
    #endregion

    #region Show Total
    protected void ShowTotal()
    {
        Decimal Amount = 0;

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["s"];
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Amount += Convert.ToDecimal(dt.Rows[i]["Amount"]);

            }
        }
        lblAmount.Text = String.Format("{0:C}", Amount).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');



    }
    #endregion
    #region Pagging
    private void bindpagging()
    {
        Last = 0;
        Corrent = 0;
        FirstRecord = 0;
        LastRecord = 0;
        //  int row = Convert.ToInt32(Request["row"]);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.ServiceInvoiceMainId = 0;
        DataTable dt = new DataTable();
        objSalesInvoice.DataFrom = "SERINV";
        dt = objSalesInvoice.getServiceInvoiceMain();
        if (dt.Rows.Count > 0)
        {
            Session["ServiceInvoiceSummary"] = dt;
            Last = dt.Rows.Count;
            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            Session["FirstRecord"] = FirstRecord;
            Session["LastRecord"] = LastRecord;
            Session["Corrent"] = LastRecord;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "location.href='ServiceInvoice.aspx?addnew=1';", true);
        }


    }
    protected void lnkFirst_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["FirstRecord"];
        BindServiceInvoice();

    }
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["FirstRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) - 1;
            BindServiceInvoice();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["LastRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) + 1;
            BindServiceInvoice();
        }
    }
    protected void lnLast_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["LastRecord"];
        BindServiceInvoice();
    }
    #endregion

    #region Fill Bank Receipt summary
    protected void BindServiceInvoice()
    {
        if (Session["ServiceInvoiceSummary"] != null)
        {
            DataTable dtServiceInvoiceSummary = (DataTable)Session["ServiceInvoiceSummary"];
            if (dtServiceInvoiceSummary.Rows.Count > 0)
            {

                DataView DvServiceInvoiceSummary = dtServiceInvoiceSummary.DefaultView;
                DvServiceInvoiceSummary.RowFilter = "Row = '" + Session["Corrent"].ToString() + "'";
                dtServiceInvoiceSummary = DvServiceInvoiceSummary.ToTable();
                if (dtServiceInvoiceSummary.Rows.Count > 0)
                {

                    txtInvNo.Text = Convert.ToString(dtServiceInvoiceSummary.Rows[0]["InvoiceNo"]);
                    txtInvDate.Text = Convert.ToDateTime(dtServiceInvoiceSummary.Rows[0]["InvDate"].ToString()).ToString("dd MMM yyyy");
                    ddlDebit.SelectedValue = Convert.ToString(dtServiceInvoiceSummary.Rows[0]["DebitAcId"]);
                    ddlCredit.SelectedValue = Convert.ToString(dtServiceInvoiceSummary.Rows[0]["CreditAcId"]);
                    hdnServiceInvoiceMainId.Value = Convert.ToString(dtServiceInvoiceSummary.Rows[0]["id"]);
                    fillServiceInvoiceDetails(Convert.ToInt32(dtServiceInvoiceSummary.Rows[0]["id"]));
                    lblAmount.Text = Convert.ToString(dtServiceInvoiceSummary.Rows[0]["TotalAmount"]);
                    txtDescription.Text = Convert.ToString(dtServiceInvoiceSummary.Rows[0]["Description"]);
                    ddlCostCenter.SelectedValue = Convert.ToString(dtServiceInvoiceSummary.Rows[0]["storeId"]);
                }
            }

        }

    }
    #endregion
    protected void fillServiceInvoiceDetails(int pid)
    {
        ViewState["s"] = null;

        DataTable dtSERINVSimpleDetail = new DataTable();
        objSalesInvoice.ServiceInvoiceMainId = pid;
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objSalesInvoice.DataFrom = "SERINV";
        dtSERINVSimpleDetail = objSalesInvoice.getServiceInvoiceDetail();

        if (dtSERINVSimpleDetail.Rows.Count > 0)
        {
            for (int i = 0; i < dtSERINVSimpleDetail.Rows.Count; i++)
            {
                filltable();
                DataRow row;
                row = dttbl.NewRow();
                row["Id"] = i + 1;
                row["Amount"] = Convert.ToDecimal(dtSERINVSimpleDetail.Rows[i]["Amount"]);
                row["Description"] = Convert.ToString(dtSERINVSimpleDetail.Rows[i]["Description"]);
                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
                ViewState.Add("s", dttbl);
            }
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            dgGallery.Visible = true;

        }
        else
        {
            dgGallery.Visible = false;
        }

    }


    #region Fill session Table
    public void filltable()
    {
        if (ViewState["s"] == null)
        {
            DataColumn column = new DataColumn();
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Amount";
            column.Caption = "Amount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);



            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Description";
            column.Caption = "Description";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


        }
        else
        {
            dttbl = (DataTable)ViewState["s"];
        }
    }
    #endregion


    #region Delete BankReceiptVoucher
    protected void btnDeleteInvoice_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(hdnServiceInvoiceMainId.Value) > 0)
        {
            int result = 0;
            objSalesInvoice.ServiceInvoiceMainId = Convert.ToInt32(hdnServiceInvoiceMainId.Value);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.DataFrom = "SERINV";
            result = objSalesInvoice.deleteAllServiceInvoice();
            Session["ServiceInvoiceSummary"] = null;
            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record deleted successfully.');location.href='ServiceInvoice.aspx';", true);
            }
        }

    }
    #endregion
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        string urllast = "PrintServiceInvoice.aspx?ServiceInvoiceMainId=" + hdnServiceInvoiceMainId.Value;
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=800,status=yes' );", true);
    }

    #region Fill Cost Center
    protected void FillCostCenter()
    {
       
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.CostCenterCode = 0;
            DataSet ds = new DataSet();
            ds = objCust.GetCostCenter();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlCostCenter.DataSource = ds.Tables[0];
                ddlCostCenter.DataTextField = "CostCenterName";
                ddlCostCenter.DataValueField = "CostCenterCode";
                ddlCostCenter.DataBind();
                ddlCostCenter.Items.Insert(0, new ListItem("Select cost center", "0"));

            }

        
    }
    #endregion


    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }

            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
            {
                btnDeleteInvoice.Enabled = false;
            }
            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

                btnPrint.Visible= false;

            }

            if (Convert.ToString(dt.Rows[0]["IsAdd"]) != "True")
            {
                lblbutton.Visible = false;
                if (Convert.ToString(Request["addnew"]) == "1")
                {

                    ImageButton2.Visible = false;
                }
            }



            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {
                if (Convert.ToString(Request["addnew"]) != "1")
                {

                    ImageButton2.Visible = false;
                }
            }

        }
    }
    protected void lblbutton_Click(object sender, EventArgs e)
    {
        Response.Redirect("ServiceInvoice.aspx?addnew=1");

    }

}
