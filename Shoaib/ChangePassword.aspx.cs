﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Manage_ChangePassword : System.Web.UI.Page
{
    clsCookie ck = new clsCookie();
   User objUser = new User();
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (!IsPostBack)
        {
            if (Sessions.CustomerCode == "")
            {
                if (ck.GetCookieValue("CustomerCode") != "")
                {
                    Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
            txtOldPassword.Focus();
        }
        SetUserRight();
    }

    protected void btn_save_Click(object sender, EventArgs e)
    {
        if (txtnpwd.Text != "")
        {
            DataSet ds = new DataSet();
            objUser.userid = Sessions.UserId;
            objUser.Username = Sessions.UseLoginName.ToString().Trim().Replace("'", "").ToString();
            objUser.UserPassword = txtnpwd.Text.Trim().Replace("'", "").ToString();
            objUser.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objUser.OldPassword =txtOldPassword.Text.Trim().Replace("'", "").ToString();
            DataTable dt = objUser.UpdateUserPassword();
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["result"].ToString() == "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Password successfully changed !!!');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Old Password is incorrect !!!');", true);
                }
            }        
          


        }
    }

    private void clear()
    {
      
        txtnpwd.Text = "";
        txtnpwd1.Text = "";
      
    }


    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }
            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }

            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {

              btn_save.Enabled = false;

            }

        }
    }
}
