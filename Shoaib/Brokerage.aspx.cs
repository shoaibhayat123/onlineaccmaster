﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Brokerage : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();
    int Last = 0;
    int Corrent = 0;
    int FirstRecord = 0;
    int LastRecord = 0;
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        if (!IsPostBack)
        {
          
            txtSaleDate.Focus();
            FillSoldTo();
            FillPL();
            FillItem();
            Session["Corrent"] = null;
            Session["FirstRecord"] = null;
            Session["LastRecord"] = null;
            Session["Yarnbrokerage"] = null;
            if (Convert.ToString(Request["addnew"]) != "1")
            {

                bindpagging();
                if (Request["InvoiceSummaryId"] == null)
                {
                    BindInvoice();
                }
                else
                {
                    DataTable dtInvoiceSummary = new DataTable();
                    dtInvoiceSummary = (DataTable)Session["Yarnbrokerage"];
                    DataView dv = new DataView(dtInvoiceSummary);
                    dv.RowFilter = "BrokerageId=" + Request["InvoiceSummaryId"];
                    dtInvoiceSummary = dv.ToTable();
                    if (dtInvoiceSummary.Rows.Count > 0)
                    {
                        Session["Corrent"] = Convert.ToInt32(dtInvoiceSummary.Rows[0]["row"]);
                        BindInvoice();
                    }
                    else
                    {
                        Response.Redirect("default.aspx");
                    }
                }

            }
            else
            {
                objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objSalesInvoice.InvoiceSummaryId = 0;
                DataTable dt = new DataTable();
                objSalesInvoice.DataFrom = "GENBRKR";
                dt = objSalesInvoice.getYarnbrokerage();
                if (dt.Rows.Count > 0)
                {
                    int Last = Convert.ToInt32(dt.Rows.Count) - 1;
                    ddlPL.SelectedValue = Convert.ToString(dt.Rows[Last]["PLId"]);
                    rdbRounding.Checked = Convert.ToBoolean(dt.Rows[Last]["Rounding"]);
                    txtSaleDate.Text = Convert.ToDateTime(dt.Rows[Last]["SaleDate"]).ToString("dd MMM yyyy"); ;
                }
                else
                {
                    txtSaleDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
                }
                spanPgging.Style.Add("display", "none");
                btnDeleteInvoice.Style.Add("display", "none");
               
            }

        }
        SetUserRight();

    }
    #region Pagging
    private void bindpagging()
    {
        Last = 0;
        Corrent = 0;
        FirstRecord = 0;
        LastRecord = 0;
        //  int row = Convert.ToInt32(Request["row"]);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.InvoiceSummaryId = 0;
        DataTable dt = new DataTable();
        objSalesInvoice.DataFrom = "GENBRKR";
        dt = objSalesInvoice.getYarnbrokerage();
        if (dt.Rows.Count > 0)
        {
            Session["Yarnbrokerage"] = dt;
            Last = dt.Rows.Count;
            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            //Corrent = Convert.ToInt32(dt.Rows[0]["Row"]);
            Session["FirstRecord"] = FirstRecord;
            Session["LastRecord"] = LastRecord;
            Session["Corrent"] = LastRecord;

        }
        else
        {
            Response.Redirect("Brokerage.aspx?addnew=1");
        
        }


    }
    protected void lnkFirst_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["FirstRecord"];
        BindInvoice();

    }
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["FirstRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) - 1;
            BindInvoice();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["LastRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) + 1;
            BindInvoice();
        }
    }
    protected void lnLast_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["LastRecord"];
        BindInvoice();
    }
    #endregion

    #region Fill  Sold To
    protected void FillPL()
    {
       
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtLedger = new DataTable();
            dtLedger = objSalesInvoice.getAcFileAllLedger();
            if (dtLedger.Rows.Count > 0)
            {
                ddlPL.DataSource = dtLedger;
                ddlPL.DataTextField = "Title";
                ddlPL.DataValueField = "Id";
                ddlPL.DataBind();
                ddlPL.Items.Insert(0, new ListItem("Select Profit & Loss  A/C", "0"));

            }

       
    }
    #endregion

    #region Fill Item
    protected void FillItem()
    {
        objCust.ItemId = 0;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlItem.DataSource = ds.Tables[0];
            ddlItem.DataTextField = "ItemDesc";
            ddlItem.DataValueField = "ItemId";
            ddlItem.DataBind();
            ddlItem.Items.Insert(0, new ListItem("Select Item", "0"));
        }

    }
    #endregion

    #region Fill  Sold To
    protected void FillSoldTo()
    {
         objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtClient = new DataTable();
            dtClient = objSalesInvoice.getAcFileBySundryDebtor();
            if (dtClient.Rows.Count > 0)
            {
                ddlSlodTo.DataSource = dtClient;
                ddlSlodTo.DataTextField = "Title";
                ddlSlodTo.DataValueField = "Id";
                ddlSlodTo.DataBind();
                ddlSlodTo.Items.Insert(0, new ListItem("Select Sold To A/c", "0"));

                ddlPurchasedFrom.DataSource = dtClient;
                ddlPurchasedFrom.DataTextField = "Title";
                ddlPurchasedFrom.DataValueField = "Id";
                ddlPurchasedFrom.DataBind();
                ddlPurchasedFrom.Items.Insert(0, new ListItem("Select Purchased From A/c", "0"));

            }

        

    }
    #endregion

    #region Fill invoice summary
    protected void BindInvoice()
    {
        if (Session["Yarnbrokerage"] != null)
        {
            DataTable dtYarnbrokerage = (DataTable)Session["Yarnbrokerage"];
            if (dtYarnbrokerage.Rows.Count > 0)
            {
                DataView DvYarnbrokerage = dtYarnbrokerage.DefaultView;
                DvYarnbrokerage.RowFilter = "Row = '" + Session["Corrent"].ToString() + "'";
                dtYarnbrokerage = DvYarnbrokerage.ToTable();
                if (dtYarnbrokerage.Rows.Count > 0)
                {

                    txtSaleDate.Text = Convert.ToDateTime(dtYarnbrokerage.Rows[0]["SaleDate"]).ToString("dd MMM yyyy");
                    txtSaleCrDays.Text = Convert.ToString(dtYarnbrokerage.Rows[0]["SaleCrdays"]);                  
                    txtPurchaseCrDays.Text = Convert.ToString(dtYarnbrokerage.Rows[0]["PurchaseCrDays"]);
                    ddlSlodTo.SelectedValue = Convert.ToString(dtYarnbrokerage.Rows[0]["SoldToId"]);
                    ddlPurchasedFrom.SelectedValue = Convert.ToString(dtYarnbrokerage.Rows[0]["PurchaseFromId"]);
                    ddlPL.SelectedValue = Convert.ToString(dtYarnbrokerage.Rows[0]["PLId"]);
                    ddlItem.SelectedValue = Convert.ToString(dtYarnbrokerage.Rows[0]["ItemId"]);
                    txtDoNo.Text = Convert.ToString(dtYarnbrokerage.Rows[0]["DoNumber"]);
                    txtDeliveredTo.Text = Convert.ToString(dtYarnbrokerage.Rows[0]["deliverTo"]);                  
                    txtLbs.Text = Convert.ToString(dtYarnbrokerage.Rows[0]["Lbs"]);
                    txtSaleRate.Text = Convert.ToString(String.Format("{0:C}", dtYarnbrokerage.Rows[0]["SaleRate"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    txtSaleAmount.Text = Convert.ToString(String.Format("{0:C}", dtYarnbrokerage.Rows[0]["SaleAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    txtPurcheseRate.Text = Convert.ToString(String.Format("{0:C}", dtYarnbrokerage.Rows[0]["PurcheseRate"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    txtPurchaseAmount.Text = Convert.ToString(String.Format("{0:C}", dtYarnbrokerage.Rows[0]["PurchaseAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    txtPlAmount.Text = Convert.ToString(String.Format("{0:C}", dtYarnbrokerage.Rows[0]["PLAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    txtDescriptionSale.Text = Convert.ToString(dtYarnbrokerage.Rows[0]["Description"]);
                    txtDescriptionPurchese.Text = Convert.ToString(dtYarnbrokerage.Rows[0]["DescriptionPurcahse"]);
                    if (rdbDo.Text == Convert.ToString(dtYarnbrokerage.Rows[0]["DescriptionPurcahse"]))
                    {
                        rdbDo.Checked = true;
                    }
                    if (rdbGP.Text == Convert.ToString(dtYarnbrokerage.Rows[0]["QuantityType"]))
                    {
                        rdbGP.Checked = true;
                    }

                    /* For invoice Update */
                    hdnBrokerageId.Value = Convert.ToString(dtYarnbrokerage.Rows[0]["BrokerageId"]);
                    rdbRounding.Checked = Convert.ToBoolean(dtYarnbrokerage.Rows[0]["Rounding"]);

                    //PrintBill.HRef = "PrintSalesInvoiceBill.aspx?InvoiceSummaryId=" + Convert.ToString(hdnInvoiceSummaryId.Value);
                }
            }

        }
    }


    #endregion


    protected void txtKgs_TextChanged(object sender, EventArgs e)
    {
        //if (txtKgs.Text == "")
        //{
        //    txtKgs.Text = "0.00";
        //}

        //txtLbs.Text = Convert.ToDecimal(Convert.ToDecimal(txtKgs.Text) * Convert.ToDecimal(2.2046)).ToString("0.00"); ;
        //txtSaleAmount.Text = Convert.ToDecimal(Convert.ToDecimal(txtSaleRate.Text) * Convert.ToDecimal(txtLbs.Text)).ToString("0.00"); ;
        //txtPurchaseAmount.Text = Convert.ToDecimal(Convert.ToDecimal(txtPurcheseRate.Text) * Convert.ToDecimal(txtLbs.Text)).ToString("0.00"); ;
        // lblBillAmount.Text = Convert.ToString(String.Format("{0:C}", (Convert.ToDecimal(txtTotalGrossAmount.Text) + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

    }
    protected void txtSaleRate_TextChanged(object sender, EventArgs e)
    {
        if (txtSaleRate.Text == "")
        {
            txtSaleRate.Text = "0.00";
        }

        txtSaleAmount.Text = Convert.ToDecimal(Convert.ToDecimal(txtSaleRate.Text) * Convert.ToDecimal(txtLbs.Text)).ToString("0.00");
        txtPlAmount.Text = Convert.ToDecimal(Convert.ToDecimal(txtSaleAmount.Text) - Convert.ToDecimal(txtPurchaseAmount.Text)).ToString("0.00");

    }
    protected void txtPurcheseRate_TextChanged(object sender, EventArgs e)
    {
        if (txtPurcheseRate.Text == "")
        {
            txtPurcheseRate.Text = "0.00";
        }

        txtPurchaseAmount.Text = Convert.ToDecimal(Convert.ToDecimal(txtPurcheseRate.Text) * Convert.ToDecimal(txtLbs.Text)).ToString("0.00");
        txtPlAmount.Text = Convert.ToDecimal(Convert.ToDecimal(txtSaleAmount.Text) - Convert.ToDecimal(txtPurchaseAmount.Text)).ToString("0.00");
    }

    #region Add Details
    protected void btn_saveInvoice_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";

        if (Sessions.FromDate != null)
        {

            if ((Convert.ToDateTime(txtSaleDate.Text) >= Convert.ToDateTime(Sessions.FromDate)) && (Convert.ToDateTime(txtSaleDate.Text) <= Convert.ToDateTime(Sessions.ToDate)))
            {



                if (hdnBrokerageId.Value == "")
                {
                    hdnBrokerageId.Value = "0";
                }
                /************ Add bROKERAGE **************/
                if (Convert.ToInt32(hdnBrokerageId.Value) > 0)
                {
                    objSalesInvoice.BrokerageId = Convert.ToInt32(hdnBrokerageId.Value);
                }
                else
                {
                    objSalesInvoice.BrokerageId = 0;
                }


                objSalesInvoice.SaleDate = Convert.ToDateTime(txtSaleDate.Text);
                objSalesInvoice.SaleCrdays = Convert.ToInt32(txtSaleCrDays.Text);
                objSalesInvoice.PurchaseCrDays = Convert.ToInt32(txtPurchaseCrDays.Text);
                objSalesInvoice.SoldToId = Convert.ToInt32(ddlSlodTo.SelectedValue);
                objSalesInvoice.PurchaseFromId = Convert.ToInt32(ddlPurchasedFrom.SelectedValue);
                objSalesInvoice.PLId = Convert.ToInt32(ddlPL.SelectedValue);
                objSalesInvoice.ItemId = Convert.ToInt32(ddlItem.SelectedValue);
                objSalesInvoice.DoNumber = Convert.ToString(txtDoNo.Text);
                objSalesInvoice.deliverTo = Convert.ToString(txtDeliveredTo.Text);
                objSalesInvoice.Lbs = Convert.ToDecimal(txtLbs.Text);
                objSalesInvoice.Kgs = Convert.ToDecimal(txtLbs.Text);
                objSalesInvoice.SaleRate = Convert.ToDecimal(txtSaleRate.Text);
                objSalesInvoice.SaleAmount = Convert.ToDecimal(txtSaleAmount.Text);
                objSalesInvoice.PurcheseRate = Convert.ToDecimal(txtPurcheseRate.Text);
                objSalesInvoice.PurchaseAmount = Convert.ToDecimal(txtPurchaseAmount.Text);
                objSalesInvoice.PLAmount = Convert.ToDecimal(txtPlAmount.Text);
                objSalesInvoice.DataFrom = "GENBRKR";
                objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objSalesInvoice.Description = Convert.ToString(txtDescriptionSale.Text);
                objSalesInvoice.DescriptionPurcahse = Convert.ToString(txtDescriptionPurchese.Text);
                if (rdbDo.Checked == true)
                {
                    objSalesInvoice.QuantityType = Convert.ToString(rdbDo.Text);
                }

                else
                {
                    objSalesInvoice.QuantityType = Convert.ToString(rdbGP.Text);
                }
                objSalesInvoice.Rounding = Convert.ToBoolean(rdbRounding.Checked);
                DataTable dtresult = new DataTable();
                dtresult = objSalesInvoice.AddEditYarnbrokerage();

                /************ Add Trans ***************/

                objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["BrokerageId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objSalesInvoice.DataFrom = "GENBRKR";
                objSalesInvoice.DeleteTrans();

                for (int i = 0; i < 3; i++)
                {
                    objSalesInvoice.TransId = 0;

                    objSalesInvoice.TransactionDate = Convert.ToDateTime(txtSaleDate.Text);
                    objSalesInvoice.DataFrom = "GENBRKR";
                    objSalesInvoice.ItemId = Convert.ToInt32(ddlItem.SelectedValue);
                    objSalesInvoice.QuantityKg = Convert.ToDecimal(txtLbs.Text);
                    objSalesInvoice.QuantityLbs = Convert.ToDecimal(txtLbs.Text);
                    objSalesInvoice.ItemId = Convert.ToInt32(ddlItem.SelectedValue);
                    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                    objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["BrokerageId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                    if (i == 0)  /* For Sale account */
                    {
                        objSalesInvoice.AcFileId = Convert.ToInt32(ddlSlodTo.SelectedValue);
                        objSalesInvoice.TransAmt = Convert.ToDecimal(txtSaleAmount.Text);
                        objSalesInvoice.AddEditTrans();
                        objSalesInvoice.Description = txtDescriptionSale.Text;
                    }

                    if (i == 1) /* For Purchase  account */
                    {
                        objSalesInvoice.AcFileId = Convert.ToInt32(ddlPurchasedFrom.SelectedValue);
                        objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(txtPurchaseAmount.Text);
                        objSalesInvoice.AddEditTrans();
                        objSalesInvoice.Description = txtDescriptionPurchese.Text;
                    }
                    if (i == 2) /* For P&l  account */
                    {
                        objSalesInvoice.AcFileId = Convert.ToInt32(ddlPL.SelectedValue);
                        objSalesInvoice.TransAmt = Convert.ToDecimal(txtPurchaseAmount.Text) - Convert.ToDecimal(txtSaleAmount.Text);
                        objSalesInvoice.AddEditTrans();
                        objSalesInvoice.Description = txtDescriptionSale.Text;
                    }




                }
                if (Convert.ToInt32(hdnBrokerageId.Value) > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Updated.');location.href='Brokerage.aspx';", true);

                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Saved.');location.href='Brokerage.aspx';", true);
                }
            }


            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Date out of accouting year.');", true);

            }
        }
    }
    #endregion

    #region Delete Brokerag
    protected void btnDeleteInvoice_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(hdnBrokerageId.Value) > 0)
        {
            int result = 0;
            objSalesInvoice.BrokerageId = Convert.ToInt32(hdnBrokerageId.Value);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.DataFrom = "GENBRKR";
            result = objSalesInvoice.deleteAllbrokerageData();
            Session["Yarnbrokerage"] = null;
            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record deleted successfully.');location.href='Brokerage.aspx';", true);
            }
        }

    }
    #endregion
    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }

            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
            {
                btnDeleteInvoice.Visible= false;
            }
            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

                //btnPrintSales.Enabled = false;
                //btnPrintPurchase.Enabled = false;
            }

            if (Convert.ToString(dt.Rows[0]["IsAdd"]) != "True")
            {
                lblbutton.Enabled = false;
                if (Convert.ToString(Request["addnew"]) == "1")
                {

                    ImageButton2.Visible = false;
                }
            }



            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {
                if (Convert.ToString(Request["addnew"]) != "1")
                {

                    ImageButton2.Visible = false;
                }
            }

        }
    }
    protected void lblbutton_Click(object sender, EventArgs e)
    {


        Response.Redirect("Brokerage.aspx?addnew=1");


    }

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {

        Response.Redirect("Brokerage.aspx");



    }
    #endregion
}
