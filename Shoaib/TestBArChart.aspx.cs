﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class TestBArChart : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //DataTable dttbl = new DataTable();

        //DataColumn column = new DataColumn();
        //column.DataType = System.Type.GetType("System.String");
        //column.ColumnName = "Month";
        //column.Caption = "Month";
        //column.ReadOnly = false;
        //dttbl.Columns.Add(column);

        //column.DataType = System.Type.GetType("System.String");
        //column.ColumnName = "Amount";
        //column.Caption = "Amount";
        //column.ReadOnly = false;
        //dttbl.Columns.Add(column);

        //DataRow row;
        //row = dttbl.NewRow();
        //row["Month"] ="Jan";
        //row["Amount"] = "25000";
        //dttbl.Rows.Add(row);
        //dttbl.AcceptChanges();
        //row = dttbl.NewRow();
        //row["Month"] = "Feb";
        //row["Amount"] = "50,000";
        //dttbl.Rows.Add(row);
        //dttbl.AcceptChanges();
        //row = dttbl.NewRow();
        //row["Month"] = "March";
        //row["Amount"] = "600000";
        //dttbl.Rows.Add(row);
        //dttbl.AcceptChanges();
        //row = dttbl.NewRow();
        //row["Month"] = "Apr";
        //row["Amount"] = "700000";
        //dttbl.Rows.Add(row);
        //dttbl.AcceptChanges();

        //Chart1.DataBindTable("Month"); 

        double[] array1 = { 2.8, 4.4, 6.5, 8.3, 3.6, 5.6, 7.3,8.0,9.5,10,11,10 };
        double[] array2 = { 2.0, 4.0, 6.1, 7.8, 2.5, 5.0, 6.2, 8.0, 9.5, 10, 11, 12 };
        double[] array4 = { 1, 2, 3, 4, 5, 6, 7,8,9,10,11,12 };

        Chart1.Series.Add("Year");
        Chart1.Series["Year"].Points.DataBindXY(array4, array1);


        Chart1.Series.Add("Sales V/s Recovery");
        Chart1.Series["Sales V/s Recovery"].Points.DataBindXY(array4, array2);
       
        Chart1.DataBind();
       
        

    }
}