﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class CostCenter : System.Web.UI.Page
{
    #region DataMembers & Varriables
    SetCustomers objCust = new SetCustomers();
    #endregion


    #region Page Load Event

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillCostCenter();
        }
    }

    #endregion

    #region Add/ Edit Cost Center Setup

    protected void btn_save_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
        if (hdnCostCenterCode.Value != null && hdnCostCenterCode.Value != string.Empty)
        {
            objCust.CostCenterCode = int.Parse(hdnCostCenterCode.Value);
        }
        else
        {
            objCust.CostCenterCode = 0;
        }
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.CostCenterName = Convert.ToString(txtCostCenterName.Text);
        if (Sessions.UserRole.ToString() != "Admin")
        {
            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }
            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" || Convert.ToString(dt.Rows[0]["IsAdd"]) == "True")
            {

                //int result = objCust.SetCostCenter();

                //if (objCust.CostCenterCode != 0)
                //{
                //    if (result != -1)
                //    {
                //        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Cost Center setup successful updated!');", true);
                //    }
                //    else
                //    {
                //        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                //    }
                //}
                //else
                //{
                //    if (result != -1)
                //    {
                //        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Cost Center setup successful added!');", true);
                //    }
                //    else
                //    {
                //        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                //    }
                //}
                //clear();
                //FillCostCenter();



                if (Convert.ToString(dt.Rows[0]["IsAdd"]) == "True" && (hdnCostCenterCode.Value == null || hdnCostCenterCode.Value == string.Empty))
                {
                    int result = objCust.SetCostCenter();
                    if (result != -1)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Cost Center setup successful added!');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                    }
                    clear();
                    FillCostCenter();
                }
                else
                {
                    if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" && hdnCostCenterCode.Value != null && hdnCostCenterCode.Value != string.Empty)
                    {
                        int result = objCust.SetCostCenter();
                        if (result != -1)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Cost Center setup successful updated!');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                        }
                        clear();
                        FillCostCenter();

                   }

                   else
                    {
                        if (hdnCostCenterCode.Value != null && hdnCostCenterCode.Value != string.Empty)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='CostCenter.aspx';", true);
                        }

                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='CostCenter.aspx';", true);
                        }
                    }

                }
            }
            else
            {
                if (hdnCostCenterCode.Value != null && hdnCostCenterCode.Value != string.Empty)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='CostCenter.aspx';", true);
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='CostCenter.aspx';", true);
                }
            }
           




        }
        else
        {

            int result = objCust.SetCostCenter();

            if (objCust.CostCenterCode != 0)
            {
                if (result != -1)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Cost Center setup successful updated!');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                }
            }
            else
            {
                if (result != -1)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Cost Center setup successful added!');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                }
            }
            clear();
            FillCostCenter();
        }





















    }

    #endregion

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        clear();
    }
    #endregion

    #region Blank Controls
    private void clear()
    {

        txtCostCenterName.Text = string.Empty;
        hdnCostCenterCode.Value = string.Empty;

    }
    #endregion


    #region Fill Cost Center Setup
    protected void FillData(int CostCenterCode)
    {
        objCust.CostCenterCode = CostCenterCode;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.GetCostCenter();
        if (ds.Tables[0].Rows.Count > 0)
        {
            txtCostCenterName.Text = Convert.ToString(ds.Tables[0].Rows[0]["CostCenterName"].ToString());


            hdnCostCenterCode.Value = CostCenterCode.ToString();
        }

    }
    #endregion

    #region Fill Cost Center Setup
    protected void FillCostCenter()
    {
       
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.CostCenterCode = 0;
            DataSet ds = new DataSet();
            ds = objCust.GetCostCenter();
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 30)
                {
                    grdCustomer.AllowPaging = true;
                }
                grdCustomer.Visible = true;
                grdCustomer.DataSource = ds.Tables[0];
                grdCustomer.DataBind();
                lblError.Visible = false;
            }
            else
            {
                grdCustomer.Visible = false;
                lblError.Visible = true;
            }
       

    }
    #endregion

    #region GridView Events
    protected void grdCustomer_RowCommand(object sender, DataGridCommandEventArgs e)
    {


        if (e.CommandName == "Delete")
        {
            objCust.CostCenterCode = int.Parse(e.CommandArgument.ToString());
            int result = objCust.DeleteCostCenterCode();
            FillCostCenter();
            if (result == 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Entries found against this cost center, cannot delete');", true);

            }

        }

        if (e.CommandName == "Edit")
        {
            FillData(int.Parse(e.CommandArgument.ToString()));

        }
    }
    protected void grdCustomer_RowDataBound(object sender, DataGridItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {


            if (Sessions.UserRole.ToString() != "Admin")
            {


                LinkButton lbEdit = (LinkButton)e.Item.FindControl("lbEdit");
                LinkButton lbDelete = (LinkButton)e.Item.FindControl("lbDelete");

                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
                if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
                {
                    Response.Redirect(Request.UrlReferrer.ToString());
                }
                if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
                {

                    lbDelete.Visible = false;

                }
                if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
                {

                    lbEdit.Visible = false;

                }

            }
        }

    }

    protected void grdCustomer_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        grdCustomer.CurrentPageIndex = e.NewPageIndex;
        FillCostCenter();

    }
    #endregion


}
