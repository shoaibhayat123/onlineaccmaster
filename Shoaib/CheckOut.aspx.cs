﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CheckOut : System.Web.UI.Page
{
    public String Amount;
    public String CustomerCode;
    protected void Page_Load(object sender, EventArgs e)
    {
        Amount = Convert.ToString( Session["Amount"]);
        CustomerCode = Convert.ToString( Session["CustomerCode"]);
    }
}