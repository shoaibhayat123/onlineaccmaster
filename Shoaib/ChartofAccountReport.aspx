﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="ChartofAccountReport.aspx.cs" Inherits="ChartofAccountReport" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
 <span style="float: left">
        <asp:Label ID="lblTitle" runat="server" Text="Chart Of Account Report"></asp:Label>
    </span>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <table>
        <tr>
            <td colspan="2" style="margin-top: 15px">
                <asp:Button ID="btnGenerate" Text="Generate" ValidationGroup="Ledger" Style="float: left"
                    runat="server" OnClick="btnGenerate_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="Ledger" />
            </td>
        </tr>
    </table>
</asp:Content>
