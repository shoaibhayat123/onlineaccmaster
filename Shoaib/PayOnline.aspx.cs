﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PayOnline : System.Web.UI.Page
{
    Car objcar = new Car();
    protected void Page_Load(object sender, EventArgs e)
    {
        FillAddress();
    }
    protected void btnResponse_Click(object sender, EventArgs e)
    {
  
        if (Convert.ToDecimal(txtTotal.Text) > 0)
        {
            Session["Amount"] = txtTotal.Text;
            Session["CustomerCode"] = txtCustomerCode.Text;
                            objcar.CustomerCode= Convert.ToDecimal(txtCustomerCode.Text);                           
							objcar.Name= txtName.Text;
							objcar.Address= txtAddress.Text;
							objcar.City= txtCity.Text;
							objcar.Country= txtCountry.Text;
							objcar.Email= txtEmail.Text;
							objcar.ContactNo= txtContactNumber.Text;
							objcar.TransactionDesc= txtTransaction.Text;
							objcar.Product= ddlPorduct.SelectedItem.Text;
							objcar.CustomizeAmount= Convert.ToDecimal(txtCustomizeAmt.Text);
							objcar.OtherAmount=Convert.ToDecimal( txtOtheramount.Text);
							objcar.BasicPackagPrice= Convert.ToDecimal( txtBasicPackagePrice.Text);
							objcar.NoOfMonths= Convert.ToInt32(ddlMonth.SelectedValue);
							objcar.TotalAmount=Convert.ToDecimal( txtTotal.Text);
                            objcar.Approved = false;
							objcar.OrderId="";
                            objcar.TransDate = Convert.ToDateTime(System.DateTime.Now.ToString());
                            int OrderId = objcar.AddEditPaymentDetails();

           Response.Redirect("CheckOut.aspx");
        }
        else
        {
            //Page.RegisterStartupScript("msg", "<script>alert('Please Enter valid amount');</script>");
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please Enter valid amount');", true);
        }
    }


    protected void ddlPorduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        decimal ProductAmount = 0;
        objcar.ProductId = Convert.ToInt32(ddlPorduct.SelectedValue);
        DataTable dt = new DataTable();
        dt = objcar.GetProdutPrice();
        if (dt.Rows.Count > 0)
        {
            ProductAmount = Convert.ToDecimal(dt.Rows[0]["Price"].ToString());
            txtBasicPackagePrice.Text = Convert.ToString(dt.Rows[0]["Price"].ToString());

        }

        decimal TotalAmount = (Convert.ToDecimal(txtBasicPackagePrice.Text) * Convert.ToDecimal(ddlMonth.SelectedValue)) + (Convert.ToDecimal(txtCustomizeAmt.Text) + Convert.ToDecimal( txtOtheramount.Text));

        txtTotal.Text = Convert.ToString(TotalAmount);

    }
    protected void FillAddress()
    {
        objcar.id = 1;
        DataTable dt = objcar.getAccAddress();
        if (dt.Rows.Count > 0)
        {
            ltrlAddress.Text = dt.Rows[0]["Address"].ToString();
        }
    }
}