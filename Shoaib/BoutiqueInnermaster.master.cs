﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;

public partial class BoutiqueInnermaster : System.Web.UI.MasterPage
{
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {


            if (Convert.ToString(Sessions.CustomerCode) == "")
            {

                if (ck.GetCookieValue("CustomerCode") != "")
                {
                    Sessions.UserId = int.Parse(Convert.ToString(ck.GetCookieValue("UserId")));
                    Sessions.UseLoginName = Convert.ToString(ck.GetCookieValue("UseLoginName"));
                    Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
                    Sessions.CustomerName = Convert.ToString(ck.GetCookieValue("CustomerName"));
                    Sessions.CityName = Convert.ToString(ck.GetCookieValue("CityName"));
                    Sessions.CountryName = Convert.ToString(ck.GetCookieValue("CountryName"));
                    Sessions.FromDate = Convert.ToString(ck.GetCookieValue("FromDate"));
                    Sessions.ToDate = Convert.ToString(ck.GetCookieValue("ToDate"));
                    Sessions.IndId = Convert.ToString(ck.GetCookieValue("IndId"));
                    Sessions.UserRole = Convert.ToString(ck.GetCookieValue("UserRole"));
                    Sessions.Brand = Convert.ToString(ck.GetCookieValue("Brand"));
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }

            }

            if (Sessions.CustomerCode != null && Sessions.CustomerName != null)
            {

                if (ck.GetCookieValue("CustomerCode") != "")
                {

                    Sessions.UserId = int.Parse(Convert.ToString(ck.GetCookieValue("UserId")));
                    Sessions.UseLoginName = Convert.ToString(ck.GetCookieValue("UseLoginName"));
                    Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
                    Sessions.CustomerName = Convert.ToString(ck.GetCookieValue("CustomerName"));
                    Sessions.CityName = Convert.ToString(ck.GetCookieValue("CityName"));
                    Sessions.CountryName = Convert.ToString(ck.GetCookieValue("CountryName"));
                    Sessions.FromDate = Convert.ToString(ck.GetCookieValue("FromDate"));
                    Sessions.ToDate = Convert.ToString(ck.GetCookieValue("ToDate"));
                    Sessions.IndId = Convert.ToString(ck.GetCookieValue("IndId"));
                    Sessions.UserRole = Convert.ToString(ck.GetCookieValue("UserRole"));
                    Sessions.Brand = Convert.ToString(ck.GetCookieValue("Brand"));
                }

          
                //lblaccountYear.Text = "Account Year From  " + ClsGetDate.FillFromDate(Convert.ToString(Sessions.FromDate)) + "To " + ClsGetDate.FillFromDate(Convert.ToString(Sessions.ToDate));
                lblTitle.Text = "<span style='font-family:Verdana'> Customer No :  " + Sessions.CustomerCode + "</span><span style='font-family:Verdana; padding-left:20px'> User :  " + Sessions.UseLoginName + "</span><br><span style='font-size:30px;'>" + Sessions.CustomerName + "</span><br><span style='font-size:18px;'>" + Sessions.CityName + ", " + Sessions.CountryName + ".</span>";

            }


            
     
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            CompanyLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            CompanyLogo.Style.Add("display", "none");
        }


   

        }
        //lblaccountYearFrom.Text = ClsGetDate.FillFromDate(Convert.ToString(Sessions.FromDate));
        //lblaccountYearTo.Text = ClsGetDate.FillFromDate(Convert.ToString(Sessions.ToDate));

    }
}
