﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CommonLib;
using System.Web.Mail;
using System.Net.Mail;
using System.IO;
using System.Data;

public partial class SendEmail : System.Web.UI.Page
{
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    User objUser = new User();
    SetCustomers objCust = new SetCustomers();

    #region Data Members

    public string MailServer;
    public string MailUser;
    public string MailFrom;
    public string MailPassword;
    public string MailTo;
    public string MailBcc;
    public string TemplateDoc;
    public string Message, Subject;
    public string[] ValueArray;
    public string _CC;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["LastUrl"] = Request.UrlReferrer.ToString();
            bindSupplier();
        }
    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        AdvancedWebFormPostBack objpostreq = new AdvancedWebFormPostBack();
        string strhtml = "";
        objpostreq.FormMethod = "get";

        objpostreq.Uri = Session["LastUrl"].ToString() + "&CustomerCode=" + Sessions.CustomerCode.ToString(); ;
        strhtml = objpostreq.GetHtml();
        string LastUrl=Session["LastUrl"].ToString();
        Session.Remove("LastUrl");


        MailServer = System.Configuration.ConfigurationManager.AppSettings["MailServer"];
        MailUser = System.Configuration.ConfigurationManager.AppSettings["MailUser"];
        MailFrom = System.Configuration.ConfigurationManager.AppSettings["MailFrom"];
        MailPassword = System.Configuration.ConfigurationManager.AppSettings["MailPassword"];
        SmtpClient smtpClient = new SmtpClient(MailServer);
        System.Net.Mail.MailMessage MlMessage = new System.Net.Mail.MailMessage(txtEmaiFrom.Text, txtEmailTo.Text);
        MlMessage.Subject = txtSubject.Text;
        MlMessage.IsBodyHtml = true;
        MlMessage.Body = strhtml.Replace("Print","").Replace("Email","").Replace("User :","");
        MlMessage.Priority = System.Net.Mail.MailPriority.High;
        smtpClient.Credentials = new System.Net.NetworkCredential(MailUser, MailPassword);
        smtpClient.Send(MlMessage);

        Page.RegisterStartupScript("msg", "<script>alert('Email has been sent Successfully');location.href='" + LastUrl + "'</script>");
       


    }


    
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();

        if (dtSupplier.Rows.Count > 0)
        {
            txtEmaiFrom.Text = dtSupplier.Rows[0]["PrimaryEmail"].ToString();          

        }
    }




}