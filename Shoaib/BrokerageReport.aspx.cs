﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class BrokerageReport : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        if (!IsPostBack)
        {
            ddlItem.Focus();
            FillClientAC();
            FillItem();
            txtInvoiceDateFrom.Text = Convert.ToDateTime(Sessions.FromDate).ToString("dd MMM yyyy");
            txtInvoiceDateTo.Text = System.DateTime.Now.ToString("dd MMM yyyy");

        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        objSalesInvoice.invDateTo = Convert.ToDateTime(txtInvoiceDateTo.Text);
        objSalesInvoice.InvDateFrom = Convert.ToDateTime(txtInvoiceDateFrom.Text);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.SoldToId = Convert.ToInt32(ddlSoldTo.SelectedValue);
        objSalesInvoice.DataFrom = "GENBRKR";
        objSalesInvoice.ItemId = Convert.ToInt32(ddlItem.SelectedValue);


        DataTable dt = new DataTable();
        dt = objSalesInvoice.Searchbrokerage();

        dgGallery.DataSource = dt;
        dgGallery.DataBind();




    }

    #region Fill  Client A/C
    protected void FillClientAC()
    {
       
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtClient = new DataTable();
            dtClient = objSalesInvoice.getAcFileBySundryDebtor();
            if (dtClient.Rows.Count > 0)
            {
                ddlSoldTo.DataSource = dtClient;
                ddlSoldTo.DataTextField = "Title";
                ddlSoldTo.DataValueField = "Id";
                ddlSoldTo.DataBind();
                ddlSoldTo.Items.Insert(0, new ListItem("Select Sold To A/c", "0"));

            }
       

    }
    #endregion

    protected void dgGallery_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            if (DataBinder.Eval(e.Row.DataItem, "SaleDate").ToString() != "")
            {
                Label lblInvDate = (Label)e.Row.FindControl("lblinvDate");
                lblInvDate.Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Row.DataItem, "SaleDate").ToString());
            }

            if (Sessions.UserRole.ToString() != "Admin")
            {


                Button btnEdit = (Button)e.Row.FindControl("btnEdit");

                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
                if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
                {
                    Response.Redirect(Request.UrlReferrer.ToString());
                }

                if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
                {

                   // btnEdit.Visible = false;

                }

            }

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {

            objSalesInvoice.invDateTo = Convert.ToDateTime(txtInvoiceDateTo.Text);
            objSalesInvoice.InvDateFrom = Convert.ToDateTime(txtInvoiceDateFrom.Text);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.SoldToId = Convert.ToInt32(ddlSoldTo.SelectedValue);
            objSalesInvoice.DataFrom = "GENBRKR";
            objSalesInvoice.ItemId = Convert.ToInt32(ddlItem.SelectedValue);
            DataTable dt = new DataTable();
            dt = objSalesInvoice.Searchbrokerage();


            //Label lblTotalKgs = (Label)e.Row.FindControl("lblTotalKgs");
            //decimal Kgs = (decimal)dt.Compute("SUM(Kgs)", "");
            //lblTotalKgs.Text = String.Format("{0:C}", Kgs).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            Label lblTotalLbs = (Label)e.Row.FindControl("lblTotalLbs");
            decimal Lbs = (decimal)dt.Compute("SUM(Lbs)", "");
            lblTotalLbs.Text = String.Format("{0:C}", Lbs).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblTotalSaleAmount = (Label)e.Row.FindControl("lblTotalSaleAmount");
            decimal SaleAmount = (decimal)dt.Compute("SUM(SaleAmount)", "");
            lblTotalSaleAmount.Text = String.Format("{0:C}", SaleAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            Label lblTotalPurchaseAmount = (Label)e.Row.FindControl("lblTotalPurchaseAmount");
            decimal PurchaseAmount = (decimal)dt.Compute("SUM(PurchaseAmount)", "");
            lblTotalPurchaseAmount.Text = String.Format("{0:C}", PurchaseAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblPLAmount = (Label)e.Row.FindControl("lblPLAmount");
            decimal PLAmount = (decimal)dt.Compute("SUM(PLAmount)", "");
            lblPLAmount.Text = String.Format("{0:C}", PLAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


           

        }

    }

    #region Fill Item
    protected void FillItem()
    {
        objCust.ItemId = 0;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlItem.DataSource = ds.Tables[0];
            ddlItem.DataTextField = "ItemDesc";
            ddlItem.DataValueField = "ItemId";
            ddlItem.DataBind();
            ddlItem.Items.Insert(0, new ListItem("Select Item", "0"));
        }

    }
    #endregion

    protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditCustomer")
        {
            Response.Redirect("Brokerage.aspx?InvoiceSummaryId=" + e.CommandArgument.ToString());
        }

    }
}
