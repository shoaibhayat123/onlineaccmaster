﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SendEmail.aspx.cs" Inherits="SendEmail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/loginform.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="loginformwindow">
        <div class="heading">
            Email This <span style="color: #33ccff">Report</span> !!
        </div>
        <div class="loginformwindowform">
            <div>
                <img src="images/loginformtop.jpg" /></div>
            <div class="loginformwindowformmiddle">
                <div class="logintable">
                    <div>
                        E-mail to:
                    </div>
                    <div>
                        <asp:TextBox ID="txtEmailTo" class="logintextimg" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator Text="*" Display="None" ID="RequiredFieldValidator1"
                            runat="server" ControlToValidate="txtEmailTo" ErrorMessage="Please Enter  E-mail to"
                            SetFocusOnError="True" ValidationGroup="Reg"></asp:RequiredFieldValidator>
                    <%--    <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmailTo"
                            Display="None" ErrorMessage="Please Enter Valid Email To." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ValidationGroup="Reg"></asp:RegularExpressionValidator>--%>
                    </div>
                    <div>
                        Subject:
                    </div>
                    <div>
                        <asp:TextBox ID="txtSubject" class="logintextimg" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator Text="*" Display="None" ID="RequiredFieldValidator3"
                            runat="server" ControlToValidate="txtSubject" ErrorMessage="Please Enter  Subject"
                            SetFocusOnError="True" ValidationGroup="Reg"></asp:RequiredFieldValidator>
                    </div>
                    <div>
                        E-mail From:
                    </div>
                    <div>
                        <asp:TextBox ID="txtEmaiFrom" class="logintextimg" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator Text="*" Display="None" ID="RequiredFieldValidator2"
                            runat="server" ControlToValidate="txtEmaiFrom" ErrorMessage="Please Enter  E-mail From"
                            SetFocusOnError="True" ValidationGroup="Reg"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmaiFrom"
                            Display="None" ErrorMessage="Please Enter Valid Email From." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            ValidationGroup="Reg"></asp:RegularExpressionValidator>
                    </div>
                    <div style="margin: 15px 0 15px 0px">
                        <div class="float_left" style="margin-right: 15px">
                            <asp:Button ID="btnSend" Text="Send" runat="server" ValidationGroup="Reg" OnClick="btnSend_Click" />
                            <asp:ValidationSummary ID="vs" runat="server" ShowMessageBox="true" ShowSummary="false"
                                ValidationGroup="Reg" />
                        </div>
                        <div style="clear: both">
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <img src="images/loginformbottom.jpg" width="390" height="11" /></div>
        </div>
    </div>
    </form>
</body>
</html>
