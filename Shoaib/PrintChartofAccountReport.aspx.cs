﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintChartofAccountReport : System.Web.UI.Page
{

    #region properties
    clsCookie ck = new clsCookie();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    decimal TotalDebit = 0;
    decimal TotalCredit = 0;
    decimal TotalDebitRow = 0;
    decimal TotalCreditRow = 0;
    decimal TotalAmount = 0;
    SetCustomers objCust = new SetCustomers();
    DataTable dttbl = new DataTable();
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        if (!IsPostBack)
        {

           
                Session["s"] = null;
                bindSupplier();
                GetSequenceTrialBalance();
                FillLogo();
                lblDate.Text = ClsGetDate.FillFromDate( System.DateTime.Now.ToString());
                lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());

                lblUser.Text = Sessions.UseLoginName.ToString();
               // lblFromDate.Text = ClsGetDate.FillFromDate(Request["AsOn"].ToString());
           
        }
    }
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }

        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }


    }
    #endregion
    protected void GetSequenceTrialBalance()
    {

       
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objSalesInvoice.GetSequenceChartOfAccount();

        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "depth = '1'";
        DataTable dt1 = new DataTable();
        dt1 = dv.ToTable();

        DataView dv2 = ds.Tables[0].DefaultView;
        dv2.RowFilter = "depth = '2'";
        DataTable dt2 = new DataTable();
        dt2 = dv2.ToTable();


        DataView dv3 = ds.Tables[0].DefaultView;
        dv.RowFilter = "depth = '3'";
        DataTable dt3 = new DataTable();
        dt3 = dv3.ToTable();


        DataView dv4 = ds.Tables[0].DefaultView;
        dv4.RowFilter = "depth = '4'";
        DataTable dt4 = new DataTable();
        dt4 = dv4.ToTable();

        DataView dv5 = ds.Tables[0].DefaultView;
        dv5.RowFilter = "depth = '5'";
        DataTable dt5 = new DataTable();
        dt5 = dv5.ToTable();


        DataView dv6 = ds.Tables[0].DefaultView;
        dv6.RowFilter = "depth = '6'";
        DataTable dt6 = new DataTable();
        dt6 = dv6.ToTable();

        filltable();
        string test = "";
        for (int i = 0; i < dt1.Rows.Count; i++)
        {
            // test += + ",";
            AddRecord(Convert.ToInt32(dt1.Rows[i]["Id"]), Convert.ToString(dt1.Rows[i]["Title"]), Convert.ToInt32(dt1.Rows[i]["ParrentId"]), Convert.ToInt32(dt1.Rows[i]["depth"]));
            for (int j = 0; j < dt2.Rows.Count; j++)
            {
                if (dt2.Rows[j]["ParrentId"].ToString() == dt1.Rows[i]["Id"].ToString())
                {
                    // test += dt2.Rows[j]["Title"].ToString() + ",";
                    AddRecord(Convert.ToInt32(dt2.Rows[j]["Id"]), Convert.ToString(dt2.Rows[j]["Title"]), Convert.ToInt32(dt2.Rows[j]["ParrentId"]), Convert.ToInt32(dt2.Rows[j]["depth"]));
                    for (int k = 0; k < dt3.Rows.Count; k++)
                    {
                        if (dt3.Rows[k]["ParrentId"].ToString() == dt2.Rows[j]["Id"].ToString())
                        {
                            // test += dt3.Rows[k]["Title"].ToString() + ",";
                            AddRecord(Convert.ToInt32(dt3.Rows[k]["Id"]), Convert.ToString(dt3.Rows[k]["Title"]), Convert.ToInt32(dt3.Rows[k]["ParrentId"]), Convert.ToInt32(dt3.Rows[k]["depth"]));
                            for (int l = 0; l < dt4.Rows.Count; l++)
                            {

                                if (dt4.Rows[l]["ParrentId"].ToString() == dt3.Rows[k]["Id"].ToString())
                                {

                                    // test += dt4.Rows[l]["Title"].ToString() + ",";
                                    AddRecord(Convert.ToInt32(dt4.Rows[l]["Id"]), Convert.ToString(dt4.Rows[l]["Title"]), Convert.ToInt32(dt4.Rows[l]["ParrentId"]), Convert.ToInt32(dt4.Rows[l]["depth"]));
                                    for (int m = 0; m < dt5.Rows.Count; m++)
                                    {
                                        if (dt5.Rows[m]["ParrentId"].ToString() == dt4.Rows[l]["Id"].ToString())
                                        {

                                            // test += dt5.Rows[m]["Title"].ToString() + ",";
                                            AddRecord(Convert.ToInt32(dt5.Rows[m]["Id"]), Convert.ToString(dt5.Rows[m]["Title"]), Convert.ToInt32(dt5.Rows[m]["ParrentId"]), Convert.ToInt32(dt5.Rows[m]["depth"]));
                                            for (int n = 0; n < dt6.Rows.Count; n++)
                                            {
                                                if (dt6.Rows[n]["ParrentId"].ToString() == dt5.Rows[m]["Id"].ToString())
                                                {
                                                    AddRecord(Convert.ToInt32(dt6.Rows[n]["Id"]), Convert.ToString(dt6.Rows[n]["Title"]), Convert.ToInt32(dt6.Rows[n]["ParrentId"]), Convert.ToInt32(dt6.Rows[n]["depth"]));
                                                    // test += dt6.Rows[n]["Title"].ToString() + ",";
                                                }
                                            }

                                        }

                                    }

                                }

                            }
                        }


                    }
                }
            }

        }

        grdInvoice.DataSource = dttbl;
        grdInvoice.DataBind();
       // grandTotalDebit.Text = String.Format("{0:C}", TotalDebit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
       // grandTotalCredit.Text = String.Format("{0:C}", TotalCredit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
    }




    #region Grid RowDataBound
    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {
      
    }
    #endregion
    #region Fill session Table
    public void filltable()
    {
        if (Session["s"] == null)
        {
            DataColumn column = new DataColumn();
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Title";
            column.Caption = "Title";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "ParrentId";
            column.Caption = "ParrentId";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "depth";
            column.Caption = "depth";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

        }
        else
        {
            dttbl = (DataTable)Session["s"];
        }
    }

    protected void AddRecord(int Id, string Title, int ParrentId, int depth)
    {
        DataRow row;
        row = dttbl.NewRow();
        row["Id"] = Id;
        row["Title"] = Title;
        row["ParrentId"] = ParrentId;
        row["depth"] = depth;


        dttbl.Rows.Add(row);
        dttbl.AcceptChanges();
        Session.Add("s", dttbl);
    }
    #endregion



    protected void grdInvoice_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnid = ((HiddenField)e.Row.FindControl("hdnid"));
            hdnid.Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Id"));
            objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);            
            objSalesInvoice.id = Convert.ToInt32(hdnid.Value);

            DataSet ds = objSalesInvoice.GetCharOfAccountreport();
            GridView grdLedger = (GridView)e.Row.FindControl("grdLedger");
            if (ds.Tables[0].Rows.Count > 0)
            {
                TotalDebitRow = 0;
                TotalCreditRow = 0;
                grdLedger.DataSource = ds.Tables[0];
                grdLedger.DataBind();
            }
        }

    }
}
