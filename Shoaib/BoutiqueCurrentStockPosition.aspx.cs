﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class BoutiqueCurrentStockPosition : System.Web.UI.Page
{
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        if (!IsPostBack)
        {
            BindBrand();
            if (Sessions.FromDate != null)
            {
                txtstartDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");

            }
            else
            {
                Response.Redirect("default.aspx");
            }

        }
       
    }
    #region btnGenrete
    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        string urllast;

        if (rdWith.Checked == true)
        {
            urllast = "PrintBoutiqueCurrentStockPosition.aspx?AsOn=" + txtstartDate.Text + "&Type=1&MainCategoryCode=" + ddlBrand.SelectedValue;
        }
        else
        {
            urllast = "PrintBoutiqueCurrentStockPosition.aspx?AsOn=" + txtstartDate.Text + "&Type=2&MainCategoryCode=" + ddlBrand.SelectedValue;
        }

        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);

    }
    #endregion
    #region BindBrand
    protected void BindBrand()
    {

        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.MainCategoryCode = 0;
        DataSet ds = new DataSet();
        ds = objCust.GetMainCategory();
        ddlBrand.DataSource = ds.Tables[0];
        ddlBrand.DataTextField = "MainCategoryName";
        ddlBrand.DataValueField = "MainCategoryCode";
        ddlBrand.DataBind();
        ddlBrand.Items.Insert(0, new ListItem("Select Brand", "0"));
        ddlBrand.SelectedValue = Convert.ToString(Sessions.Brand);

    }
    #endregion
}