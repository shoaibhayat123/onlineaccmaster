﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintEmbDailyProductionValueReport.aspx.cs"
    Inherits="PrintEmbDailyProductionValueReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Tahoma; background: none; font-size: 10px;">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; font-weight: bold;">
          <tr id="trTrialBal" runat="server">
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <br />
                    <br />
                    <br />
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Daily
                        Production Value Report
                        <br />
                    </span><span style="font-size: 14px;">From :
                        <asp:Label ID="lblFromDate" runat="server" Style="padding-right: 15px;"></asp:Label>
                        To :
                        <asp:Label ID="lblToDate" runat="server"></asp:Label><br />
                    </span><span style="font-size: 14px;">Report Type :
                        <asp:Label ID="lblReportType" Style="text-decoration: underline" runat="server"></asp:Label>
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <hr />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <asp:Repeater ID="rptMending" OnItemDataBound="RptReport_ItemDataBound" runat="server">
            <ItemTemplate>
                <table>
                    <tr>
                        <td>
                            <b>Client :
                                <asp:Label ID="lblClient" runat="server"></asp:Label></b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Repeater ID="rptQuality" OnItemDataBound="rptQuality_ItemDataBound" runat="server">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <b>Quality :
                                                    <asp:Label ID="lblQuality" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Repeater ID="rptMachine" OnItemDataBound="rptMachine_ItemDataBound" runat="server">
                                                    <ItemTemplate>
                                                        <b>Machine # :
                                                            <asp:Label ID="lblMachine" runat="server"></asp:Label></b>
                                                        <asp:DataGrid ID="dgGallery" Width="800px" runat="server" AutoGenerateColumns="False"
                                                            AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                                            AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                                            FooterStyle-CssClass="gridFooter" ShowFooter="true" BorderStyle="Dotted" BorderWidth="1"
                                                            HeaderStyle-ForeColor="Black" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery_RowDataBound">
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderText="S.No.">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <%# Container.DataSetIndex + 1%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Design No.">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="left" Width="10%" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDesign" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Stitches">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStitches" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Total Stitches">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTotalStitches" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblMachineTotalStitches" runat="server"></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Pcs">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                                    <ItemTemplate>
                                                                        <%# Eval("Pcs")%>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblMachineTotalPcs" runat="server"></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Yards">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                                    <ItemTemplate>
                                                                        <%# Eval("yard")%>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblMachineTotalyard" runat="server"></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Stitches Rate">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblStitchesRate" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Yard Rate">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblYardRate" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Pcs Rate">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPcsRate" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:TemplateColumn HeaderText="Amount">
                                                                    <HeaderStyle HorizontalAlign="Center" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="15%" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblAmount" runat="server"></asp:Label>
                                                                    </ItemTemplate>
                                                                    <FooterTemplate>
                                                                        <asp:Label ID="lblMachineTotalAmount" runat="server"></asp:Label>
                                                                    </FooterTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                        </td>
                    </tr>
                </table>
                <table style="width: 800px; text-align: right; border: solid 1px black; font-size: 14px;
                    font-weight: bold; margin-top: 5px; margin-bottom: 15px; color: black;">
                    <tr>
                        <td style="text-align: left; width: 20%">
                            Group Total
                        </td>
                        <td style="text-align: center; width: 20%">
                            Stitches:
                            <asp:Label ID="lblClientTotalStitches" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center; width: 20%">
                            Pcs:
                            <asp:Label ID="lblClientTotalPcs" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center; width: 20%">
                            Yards:
                            <asp:Label ID="lblClientTotalYards" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center; width: 20%">
                            Amount:
                            <asp:Label ID="lblClientTotalAmount" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:Repeater>
       
        <table border="1" style="width: 800px; text-align: right; border: solid 1px black;
            font-weight: bold; border-collapse: collapse; float: left; margin-top: 15px;
            margin-bottom: 15px; color: black;">
            <tr>
                <td colspan="3" style="font-size: 18px; text-align: center; font-weight: bold">
                    Total Billing Details
                </td>
            </tr>
            <tr>
                <td style="width: 50%; text-align: center; font-size: 14px;">
                    Total Billing Value Machine #
                </td>
                <td style="width: 25%; text-align: center; font-size: 14px;">
                    Total Stitches
                </td>
                <td style="width: 25%; text-align: center; font-size: 14px;">
                    Amount
                </td>
            </tr>
            <tr id="tr1" runat="server">
                <td style="width: 50%; text-align: center; font-size: 14px;">
                    1
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblTSt1" runat="server"></asp:Label>
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblAmount1" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="tr2" runat="server">
                <td style="width: 50%; text-align: center; font-size: 14px;">
                    2
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblTSt2" runat="server"></asp:Label>
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblAmount2" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="tr3" runat="server">
                <td style="width: 50%; text-align: center; font-size: 14px;">
                    3
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblTSt3" runat="server"></asp:Label>
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblAmount3" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="tr4" runat="server">
                <td style="width: 50%; text-align: center; font-size: 14px;">
                    4
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblTSt4" runat="server"></asp:Label>
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblAmount4" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="tr5" runat="server">
                <td style="width: 50%; text-align: center; font-size: 14px;">
                    5
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblTSt5" runat="server"></asp:Label>
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblAmount5" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="tr6" runat="server">
                <td style="width: 50%; text-align: center; font-size: 14px;">
                    6
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblTSt6" runat="server"></asp:Label>
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblAmount6" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="tr7" runat="server">
                <td style="width: 50%; text-align: center; font-size: 14px;">
                    7
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblTSt7" runat="server"></asp:Label>
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblAmount7" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="tr8" runat="server">
                <td style="width: 50%; text-align: center; font-size: 14px;">
                    8
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblTSt8" runat="server"></asp:Label>
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblAmount8" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="tr9" runat="server">
                <td style="width: 50%; text-align: center; font-size: 14px;">
                    9
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblTSt9" runat="server"></asp:Label>
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblAmount9" runat="server"></asp:Label>
                </td>
            </tr>
            <tr id="tr10" runat="server">
                <td style="width: 50%; text-align: center; font-size: 14px;">
                    10
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblTSt10" runat="server"></asp:Label>
                </td>
                <td style="width: 25%; font-size: 14px;">
                    <asp:Label ID="lblAmount10" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
         <table style="width: 800px; text-align: right; border: solid 1px black;background: none repeat scroll 0 0 #85A68C; font-size: 14px;
            font-weight: bold; float: left; margin-top: 15px; margin-bottom: 15px; color: black;">
            <tr>
                <td style="text-align: left; width: 20%">
                    Grand Total
                </td>
                <td style="text-align: center; width: 20%">
                    Stitches:
                    <asp:Label ID="lblClientTotalStitches" runat="server"></asp:Label>
                </td>
                <td style="text-align: center; width: 20%">
                   Pcs: <asp:Label ID="lblClientTotalPcs" runat="server"></asp:Label>
                </td>
                <td style="text-align: center; width: 20%">
                    Yards:
                    <asp:Label ID="lblClientTotalYards" runat="server"></asp:Label>
                </td>
                <td style="text-align: center; width: 20%">
                    Amount:
                    <asp:Label ID="lblClientTotalAmount" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>
                                <script type="text/javascript">

                                    var currentTime = new Date()
                                    var hours = currentTime.getHours()
                                    var Newhours = 0;
                                    var minutes = currentTime.getMinutes()
                                    if (minutes < 10) {
                                        minutes = "0" + minutes
                                    }
                                    if (hours > 12) {
                                        Newhours = hours - 12;
                                    }
                                    else {
                                        Newhours = hours;
                                    }
                                    document.write(Newhours + ":" + minutes + " ")
                                    if (hours > 11) {
                                        document.write("PM")
                                    } else {
                                        document.write("AM")
                                    }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
