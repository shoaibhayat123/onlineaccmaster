﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class CashBankTransactionsAcReportPrint : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();

    AccFiles objFile = new AccFiles();
    Reports objReports = new Reports();
    decimal TotalBalance = 0;
    decimal TotalDebit = 0;
    decimal TotalCredit = 0;
    decimal balance = 0;
    decimal bfamount = 0;
    decimal ToatlReceive = 0;
    decimal TotalPayment = 0;
    //public string attachment = "attachment; filename=Dresses27_Report_" + DateTime.Now.ToString("dd_MM_yyyy") + ".xls";
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if ((Sessions.CustomerCode == "") || (Request["From"].ToString() == "") || (Request["To"].ToString() == "") || (Request["Client"].ToString() == ""))
        {
            Response.Redirect("default.aspx");
        }
        if (!IsPostBack)
        {


            bindSupplier();
            bindCashBook();
            lblFromDate.Text = ClsGetDate.FillFromDate(Request["From"].ToString());
            lblToDate.Text = ClsGetDate.FillFromDate(Request["To"].ToString());
            bindBuyer();
            FillLogo();

            lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
            lblUser.Text = Sessions.UseLoginName.ToString();
            //PrepareGridViewForExport(grdLedger);
            //ExportGridView();


        }
    }

    protected void bindBuyer()
    {
        objFile.id = Convert.ToInt32(Request["Client"].ToString()); ;
        objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtBuyer = new DataTable();
        dtBuyer = objFile.getMenueDetail();


        if (dtBuyer.Rows.Count > 0)
        {
            lblBuyerName.Text = Convert.ToString(dtBuyer.Rows[0]["Title"]);
            lblBuyerAddress.Text = Convert.ToString(dtBuyer.Rows[0]["Address1"]);
            if (Convert.ToString(dtBuyer.Rows[0]["Address2"]) != "")
            {
                lblBuyerAddress2.Text = "," + Convert.ToString(dtBuyer.Rows[0]["Address2"]);
            }
            if (Convert.ToString(dtBuyer.Rows[0]["Address3"]) != "")
            {
                lblBuyerAddress3.Text = "," + Convert.ToString(dtBuyer.Rows[0]["Address3"]);
            }
        }


    }


    #region Bind ledger grid
    protected void bindCashBook()
    {
        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);

        objReports.DateFrom = Convert.ToDateTime(Request["from"]);
        objReports.DateTo = Convert.ToDateTime(Request["to"]);
        objReports.AcFileId = Convert.ToInt32(Request["client"]);
        objReports.DataFrom = "CashBook";
        DataSet ds = new DataSet();
        ds = objReports.Getcashbankbookreport();


        ///********** Add new row to result for Opening Balance**********/
        DataTable dtCashBookSimpleDetail = new DataTable();
        objSalesInvoice.AcFileId = Convert.ToInt32(Request["client"]);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.CashBookDate = Convert.ToDateTime(Request["from"]);
        DataSet ds1 = new DataSet();
        ds1 = objSalesInvoice.getBrodForwordBal();
        decimal tottrans = 0;
        decimal OpeningBal = 0;
        if (Convert.ToString(ds1.Tables[0].Rows[0]["OpeningBal"]) != "")
        {
            OpeningBal = Convert.ToDecimal(ds1.Tables[0].Rows[0]["OpeningBal"]);
        }
        if (Convert.ToString(ds1.Tables[0].Rows[0]["tottrans"]) != "")
        {
            tottrans = Convert.ToDecimal(ds1.Tables[0].Rows[0]["tottrans"]);
        }
        bfamount = OpeningBal + tottrans;

        DataRow dr = ds.Tables[0].NewRow(); //table 2 for detail trans
        dr["Description"] = "Opening Balance";
        dr["TransAmt"] = bfamount;

        ds.Tables[0].Rows.InsertAt(dr, 0);
        if (ds.Tables[0].Rows.Count > 0)
        {
            grdLedger.DataSource = ds.Tables[0];
            grdLedger.DataBind();
        }


    }

    #endregion
    #region Grid RowDataBound
    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbltransDate = ((Label)e.Row.FindControl("lbltransDate"));
            string transDate = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "CashBookDate"));
            if (DataBinder.Eval(e.Row.DataItem, "CashBookDate").ToString() != "")
            {
                lbltransDate.Text = ClsGetDate.FillFromDate(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "CashBookDate")));
            }

            Label lblBalance = ((Label)e.Row.FindControl("lblBalance"));
            if (e.Row.RowIndex.ToString() == "0")
            {
                balance += bfamount;
            }
            else
            {
                balance += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Receive")) - Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Payment"));
                ToatlReceive += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Receive"));
                TotalPayment += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Payment"));
            }
            lblBalance.Text = String.Format("{0:C}", balance).Replace('-', ' ').Replace('$', ' ').Replace('(', '-').Replace(')', ' ');
         

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblTotalReceive = ((Label)e.Row.FindControl("lblTotalReceive"));
            Label lblTotalPayment = ((Label)e.Row.FindControl("lblTotalPayment"));

            lblTotalReceive.Text = String.Format("{0:C}", ToatlReceive).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblTotalPayment.Text = String.Format("{0:C}", TotalPayment).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        }


    }


    #endregion

    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }

    }
    #endregion
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }



}
