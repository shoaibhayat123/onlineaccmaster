﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


using System.Globalization;
using System.Data.SqlClient;


using System.Xml;
using System.Web.Security;


using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;

public partial class POSSaleInvoice : System.Web.UI.Page
{
    

    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();
    UAEOrderServiceClient uAEClient = new UAEOrderServiceClient();          // Shoaib Working
    int Last = 0;
    int Corrent = 0;
    int FirstRecord = 0;
    int LastRecord = 0;

    // Shoaib Working Start
    int vender_id = 7; int branch_id = 7; string category_Name = "Ladies / Gents Suits"; string category_Code = "001";
    double margin = 11.11; string terminal_Code = "EC086B1A355D"; string city = "Dubai"; string prefixNumber = "00971";
    string[] customerAccCodeArray = { "7860020", "7860021", "7860022", "7860023", "7860030",
        "7860031", "7860032", "7860039", "7860040", "7860041", "7860043", "7860027" };

    // Shoaib Working End

    #endregion


    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {

        //HttpContext.Current.Session["CustomerCode"] = value;
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' " + Sessions.CustomerCode + "');location.href='POSSaleInvoice.aspx';", true);
                
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        string strx;
        strx = Convert.ToString(HttpContext.Current.Session["CustomerCode"]);
        if (strx == "7860043")
        {
            lblDes1.Text = "Wallet Card #";
            lblDes2.Text = "Transaction #";
            rdbcashCredit.Text = "ARY Wallet";
        }
          else
        {
            lblDes1.Text = "Credit Card #";
            lblDes2.Text = "Approval No. ";
            rdbcashCredit.Text = "Credit Card";
        }
        //
        //
        //
        //
        //



        if (!IsPostBack)
        {
            ViewState["s"] = null;

            FillAllLedgerAc();
            FillCostCenter();
            FillClientAC();
            FillSalesAc();
            FillItemDesc();
            FillCashReceived(1);
            FillCardChargesaccount();
            txtInvNo.Focus();
            DataTable dt = new DataTable();
            dgGallery.DataSource = dt;
            dgGallery.DataBind();
            Session["Corrent"] = null;
            Session["FirstRecord"] = null;
            Session["LastRecord"] = null;
            Session["InvoiceSummary"] = null;
            if (Convert.ToString(Request["addnew"]) != "1")
            {

                bindpagging();
                if (Request["InvoiceSummaryId"] == null)
                {
                   BindInvoice(); 
                  
                }
                else
                {
                    DataTable dtInvoiceSummary = new DataTable();
                    dtInvoiceSummary = (DataTable)Session["InvoiceSummary"];
                    DataView dv = new DataView(dtInvoiceSummary);
                    dv.RowFilter = "InvoiceSummaryId=" + Request["InvoiceSummaryId"];
                    dtInvoiceSummary = dv.ToTable();
                    if (dtInvoiceSummary.Rows.Count > 0)
                    {
                        Session["Corrent"] = Convert.ToInt32(dtInvoiceSummary.Rows[0]["row"]);
                        BindInvoice();
                    }
                    else
                    {
                        Response.Redirect("default.aspx");
                    }
                }

            }
            else
            {
                objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objSalesInvoice.InvoiceSummaryId = 0;
                objSalesInvoice.DataFrom = "SALESINV";
                DataTable dtInvoiceNewDetails = new DataTable();
                dtInvoiceNewDetails = objSalesInvoice.getBoutiqueInvoiceInvNo();
                if (dtInvoiceNewDetails.Rows.Count > 0)
                {
                    int Last = Convert.ToInt32(dtInvoiceNewDetails.Rows.Count) - 1;
                    ddlSaleAC.SelectedValue = Convert.ToString(dtInvoiceNewDetails.Rows[Last]["SalesAccountId"]);
                    ddlCostCenter.SelectedValue = Convert.ToString(dtInvoiceNewDetails.Rows[Last]["storeId"]);
                    ddlClientAC.SelectedValue = Convert.ToString(dtInvoiceNewDetails.Rows[Last]["ClientAccountId"]);
                    // ddlSaleTaxAC.SelectedValue = Convert.ToString(dtInvoiceNewDetails.Rows[Last]["SaleTaxAccountId"]);
                    spanLastInvoice.Style.Add("display", "");
                    litLastInvoiceNo.Text = Convert.ToString(dtInvoiceNewDetails.Rows[Last]["InvNo"]);
                    litLastInvDate.Text = ClsGetDate.FillFromDate(Convert.ToString(dtInvoiceNewDetails.Rows[Last]["InvDate"]));
                    txtInvoiceDate.Text = Convert.ToDateTime(dtInvoiceNewDetails.Rows[Last]["InvDate"]).ToString("dd MMM yyyy");
                    string Str = Convert.ToString(dtInvoiceNewDetails.Rows[Last]["InvNo"]);
                    double Num;
                    bool isNum = double.TryParse(Str, out Num);
                    if (isNum)
                    {
                        txtInvNo.Text = (Convert.ToInt32(dtInvoiceNewDetails.Rows[Last]["InvNo"]) + 1).ToString();
                    }


                    DataTable dtInvoice = new DataTable();
                    objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtInvoiceNewDetails.Rows[Last]["InvoiceSummaryId"]);
                    objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
                    objSalesInvoice.DataFrom = "SALESINV";
                    dtInvoice = objSalesInvoice.getInvoiceDetailsCrccount();
                    if (dtInvoice.Rows.Count > 0)
                    {
                        ddlCR.SelectedValue = Convert.ToString(dtInvoice.Rows[dtInvoice.Rows.Count-1]["CRID"]);                        
                    }


                }
                else
                {
                    txtInvoiceDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
                    txtInvNo.Text = "1";
                }
                spanPgging.Style.Add("display", "none");
                btnDeleteInvoice.Style.Add("display", "none");
                //btnPrint.Style.Add("display", "none");
                btnPrintBill.Style.Add("display", "none");
            }
        }

        SetUserRight();

    }
    #endregion


    #region Fill Categories



    #endregion
    // Shoaib Working Start
    #region Encrypt
    public static string Encrypt(string toEncrypt, bool useHashing)
    {
        byte[] keyArray;
        byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

        //System.Configuration.AppSettingsReader settingsReader =
        //                                    new AppSettingsReader();
        //// Get the key from config file

        //string key = (string)settingsReader.GetValue("testKey",
        //                                                 typeof(String));
        //System.Windows.Forms.MessageBox.Show(key);
        //If hashing use get hashcode regards to your key

        string key = "`@%u&&**a#$";

        if (useHashing)
        {
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
            //Always release the resources and flush data
            // of the Cryptographic service provide. Best Practice

            hashmd5.Clear();
        }
        else
            keyArray = UTF8Encoding.UTF8.GetBytes(key);

        TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
        //set the secret key for the tripleDES algorithm
        tdes.Key = keyArray;
        //mode of operation. there are other 4 modes.
        //We choose ECB(Electronic code Book)
        tdes.Mode = CipherMode.ECB;
        //padding mode(if any extra byte added)

        tdes.Padding = PaddingMode.PKCS7;

        ICryptoTransform cTransform = tdes.CreateEncryptor();
        //transform the specified region of bytes array to resultArray
        byte[] resultArray =
          cTransform.TransformFinalBlock(toEncryptArray, 0,
          toEncryptArray.Length);
        //Release resources held by TripleDes Encryptor
        tdes.Clear();
        //Return the encrypted data into unreadable string format
        return Convert.ToBase64String(resultArray, 0, resultArray.Length);
    }
    #endregion
    // Shoaib Working End


    #region Fill Item Description

    protected void FillItemDesc()
    {
        
        
        
        objCust.ItemId = 0;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlItemDesc.DataSource = ds.Tables[0];
            ddlItemDesc.DataTextField = "ItemDesc";
            ddlItemDesc.DataValueField = "ItemId";
            ddlItemDesc.DataBind();
            ddlItemDesc.Items.Insert(0, new ListItem("--Select Item--", "0"));

          }
                
    }


    #endregion

    #region Fill Cost Center
    protected void FillCostCenter()
    {

        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.CostCenterCode = 0;
        DataSet ds = new DataSet();
        ds = objCust.GetCostCenter();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCostCenter.DataSource = ds.Tables[0];
            ddlCostCenter.DataTextField = "CostCenterName";
            ddlCostCenter.DataValueField = "CostCenterCode";
            ddlCostCenter.DataBind();
            ddlCostCenter.Items.Insert(0, new ListItem("Select cost center", "0"));

        }



    }
    #endregion

    #region Fill  Client A/C
    protected void FillClientAC()
    {

        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtClient = new DataTable();
        dtClient = objSalesInvoice.getAcFileAllLedger();
        if (dtClient.Rows.Count > 0)
        {
            ddlClientAC.DataSource = dtClient;
            ddlClientAC.DataTextField = "Title";
            ddlClientAC.DataValueField = "Id";
            ddlClientAC.DataBind();
            ddlClientAC.Items.Insert(0, new ListItem("Select Client A/c",  "0"));

        }



    }
    protected void FillCardChargesaccount()
    {

        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtClient = new DataTable();
        dtClient = objSalesInvoice.getAcFileAllLedger();
        if (dtClient.Rows.Count > 0)
        {
            ddlCardChargesaccount.DataSource = dtClient;
            ddlCardChargesaccount.DataTextField = "Title";
            ddlCardChargesaccount.DataValueField = "Id";
            ddlCardChargesaccount.DataBind();
            ddlCardChargesaccount.Items.Insert(0, new ListItem("Select Card Charges A/c", "0"));

        }



    }



    #endregion

    #region Fill  FillSales A/c
    protected void FillSalesAc()
    {

        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtLedger = new DataTable();
        dtLedger = objSalesInvoice.getAcFileBySales();
        if (dtLedger.Rows.Count > 0)
        {
            ddlSaleAC.DataSource = dtLedger;
            ddlSaleAC.DataTextField = "Title";
            ddlSaleAC.DataValueField = "Id";
            ddlSaleAC.DataBind();
            ddlSaleAC.Items.Insert(0, new ListItem("Select Sales A/C", "0"));


            //ddlSaleTaxAC.DataSource = dtLedger;
            //ddlSaleTaxAC.DataTextField = "Title";
            //ddlSaleTaxAC.DataValueField = "Id";
            //ddlSaleTaxAC.DataBind();
            //ddlSaleTaxAC.Items.Insert(0, new ListItem("Select Sales A/C", "0"));

        }



    }
    #endregion

    #region Add Details

    public void DataforTbltrans(Int32 InvoiceSummaryID)
    {
        for (int i = 0; i < 2; i++)
        {
            objSalesInvoice.TransId = 0;

            objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvoiceDate.Text);

            objSalesInvoice.DataFrom = "SALESINV";
            objSalesInvoice.InvoiceNo = txtInvNo.Text.ToUpper();

            if (txtCrDays.Text != "")
            {
                objSalesInvoice.Crdays = Convert.ToInt32(txtCrDays.Text);
            }
            else
            {
                objSalesInvoice.Crdays = 0;
            }

            /**** Add CR Days *******/
            System.DateTime today = Convert.ToDateTime(txtInvoiceDate.Text);
            if (txtCrDays.Text != "")
            {
                System.DateTime answer = today.AddDays(Convert.ToInt32(txtCrDays.Text));
                objSalesInvoice.DueDate = answer;
            }
            else
            {
                System.DateTime answer = Convert.ToDateTime(txtInvoiceDate.Text);
                objSalesInvoice.DueDate = answer;
            }

            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.InvoiceSummaryId = InvoiceSummaryID; /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
            if (i == 0)  /* For Client account */
            {
                objSalesInvoice.AcFileId = Convert.ToInt32(ddlClientAC.SelectedValue);
                objSalesInvoice.TransAmt = Convert.ToDecimal(lblBillAmount.Text);
                objSalesInvoice.Description = txtDescription.Text;
                if (txtconversionRate.Text != "")
                {
                    objSalesInvoice.FCAmount = Convert.ToDecimal(lblBillAmount.Text) *
                           Convert.ToDecimal(txtconversionRate.Text);
                }


            }

            if (i == 1) /* For Sales  account */
            {
                objSalesInvoice.AcFileId = Convert.ToInt32(ddlSaleAC.SelectedValue);
                objSalesInvoice.TransAmt = 0 - (Convert.ToDecimal(txtSaleTax.Text) + Convert.ToDecimal(txtTotalGrossAmount.Text) + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text));
                objSalesInvoice.Description = txtDescription.Text;
                if (txtconversionRate.Text != "")
                {
                    objSalesInvoice.FCAmount =0-( Convert.ToDecimal(lblBillAmount.Text) *
                           Convert.ToDecimal(txtconversionRate.Text));
                }
            }


            objSalesInvoice.AddEditTrans();

        }
    }

    protected void btn_saveInvoice_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
        if (Sessions.FromDate != null)
        {
            if (ViewState["s"] != null)
            {
                if ((Convert.ToDateTime(txtInvoiceDate.Text) >= Convert.ToDateTime(Sessions.FromDate)) && (Convert.ToDateTime(txtInvoiceDate.Text) <= Convert.ToDateTime(Sessions.ToDate)))
                {
                    if (Convert.ToDecimal(lblBillAmount.Text) >= 0)
                    {
                        int Flag = 0;
                        if (((rdbCash.Checked == true) || (rdbcashCredit.Checked == true)) && (Convert.ToDecimal(txtCashReceived.Text) >= Convert.ToDecimal(lblBillAmount.Text)))
                        {
                            // /******/
                            if (ddlCashReceipt.SelectedValue.ToString() != "0")
                            {
                                Flag = 1;
                            }

                        }
                        else
                        {
                            if (rdbCredit.Checked == true)       
                            {
                                Flag = 1;
                            }
                            else if (rdbARYSahulatCard.Checked == true)     // Shoaib Working "rdbARYSahulatCard.Checked == true"
                            {
                                if (String.IsNullOrEmpty(txtCustomerName.Text) || String.IsNullOrWhiteSpace(txtCustomerName.Text)
                                    || String.IsNullOrEmpty(txtContactNumber.Text) || String.IsNullOrEmpty(txtContactNumber.Text))
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Customer Name (OR) Contact Number Should Not be Empty.');", true);
                                    return;
                                }
                                else
                                {
                                    string customerAccCode = Convert.ToString(HttpContext.Current.Session["CustomerCode"]);
                                    if (Array.IndexOf(customerAccCodeArray, customerAccCode) >= 0)
                                    {
                                        if (ARYSahulatCardInvoice() == false)
                                        {
                                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Order Registration Failed.');", true);
                                            return;
                                        }
                                        else
                                        {
                                            Flag = 1;
                                        }
                                    }
                                    else
                                    {
                                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Account Number Not Valid For ARY Sahulat Card.');", true);
                                        return;
                                    }                                    
                                }

                            }

                        }
                        if (Flag == 1)
                        {

                            if (hdnInvoiceSummaryId.Value == "")
                            {
                                hdnInvoiceSummaryId.Value = "0";
                            }
                            /************ Add Invoice Summary **************/
                            if (Convert.ToInt32(hdnInvoiceSummaryId.Value) > 0)
                            {
                                objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(hdnInvoiceSummaryId.Value);
                            }
                            else
                            {
                                objSalesInvoice.InvoiceSummaryId = 0;
                            }
                            objSalesInvoice.InvNo = txtInvNo.Text.ToUpper();
                            objSalesInvoice.InvDate = Convert.ToDateTime(txtInvoiceDate.Text);
                            if (txtCrDays.Text != "")
                            {
                                objSalesInvoice.CrDays = Convert.ToInt32(txtCrDays.Text);
                            }
                            else
                            {
                                objSalesInvoice.CrDays = 0;
                            }
                            objSalesInvoice.storeId = Convert.ToInt32(ddlCostCenter.SelectedValue);
                            objSalesInvoice.ClientAccountId = Convert.ToInt32(ddlClientAC.SelectedValue);
                            objSalesInvoice.SalesAccountId = Convert.ToInt32(ddlSaleAC.SelectedValue);
                            //objSalesInvoice.SaleTaxAccountId = Convert.ToInt32(ddlSaleTaxAC.SelectedValue);
                            objSalesInvoice.TotalQuantity = Convert.ToDecimal(txtTotalQuantity.Text);
                            objSalesInvoice.TotalGrossAmount = Convert.ToDecimal(txtTotalGrossAmount.Text);
                            objSalesInvoice.TotalSaleTaxAmount = Convert.ToDecimal(txtSaleTax.Text);
                            objSalesInvoice.TotalAmountIncludeTax = Convert.ToDecimal(txtTotalAmountIncludingTax.Text);
                            objSalesInvoice.CartageAmount = Convert.ToDecimal(txtCartagecharges.Text);
                            objSalesInvoice.BillAmount = Convert.ToDecimal(lblBillAmount.Text);
                            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            objSalesInvoice.Description = txtDescription.Text;
                            objSalesInvoice.InvoiceDiscount = Convert.ToDecimal(txtDiscount.Text);
                            objSalesInvoice.DataFrom = "SALESINV";
                            objSalesInvoice.CashReceived = Convert.ToDecimal(txtCashReceived.Text);
                            objSalesInvoice.CashReturned = Convert.ToDecimal(txtCashReturned.Text);
                            objSalesInvoice.CashReceivedAcFileID = Convert.ToInt32(ddlCashReceipt.SelectedValue);
                            if (rdbCash.Checked == true)
                            {
                                objSalesInvoice.POSSaleType = rdbCash.Text;
                            }
                            if (rdbcashCredit.Checked == true)
                            {
                                objSalesInvoice.POSSaleType = rdbcashCredit.Text;
                                objSalesInvoice.CreditCardNo = txtCreditCarNo.Text;
                                objSalesInvoice.Approval = txtApprovelNO.Text;
                            }
                            if (rdbCredit.Checked == true)
                            {
                                objSalesInvoice.POSSaleType = rdbCredit.Text;
                            }

                            // Shoaib Working Start
                            if (rdbARYSahulatCard.Checked == true)
                            {
                                objSalesInvoice.POSSaleType = rdbARYSahulatCard.Text;
                                objSalesInvoice.ContactNumber = System.String.Concat(prefixNumber, txtContactNumber.Text);
                            }
                            // Shoaib Working End

                            if (Convert.ToString(txtCardCharge.Text) != "")
                            {
                                objSalesInvoice.CardChargesRate = Convert.ToDecimal(txtCardCharge.Text);
                            }
                            if (Convert.ToString(hdnCardamount.Value) != "")
                            {
                                objSalesInvoice.CardChargesAmount = Convert.ToDecimal(hdnCardamount.Value);
                            }

                            ////////Add 3 new column
                            if (txtCustomerName.Text != "")
                            {
                                objSalesInvoice.CustomerName = txtCustomerName.Text;
                            }
                            else
                            {
                                objSalesInvoice.CustomerName = "-";
                            }
                            if (txtContactNumber.Text != "")
                            {   
                                //objSalesInvoice.ContactNumber = txtContactNumber.Text;                                        // Comit By Shoaib
                                objSalesInvoice.ContactNumber = System.String.Concat(prefixNumber, txtContactNumber.Text);      // Shoaib Working
                            }
                            else
                            {
                                objSalesInvoice.ContactNumber = "-";
                            }
                            if (txtEmailAddress.Text != "")
                            {
                                objSalesInvoice.EmailAddress = txtEmailAddress.Text;
                            }
                            else
                            {
                                objSalesInvoice.EmailAddress = "-";
                            }
                            objSalesInvoice.SalesPersonAbbr = txtSalesPerson.Text;

                            objSalesInvoice.CardChargesAcfileId = Convert.ToInt32(ddlCardChargesaccount.SelectedValue);
                            if (txtconversionRate.Text != "")
                            {
                                objSalesInvoice.ConversionRate = Convert.ToDecimal(txtconversionRate.Text);
                            }
                            DataTable dtresult = new DataTable();
                            dtresult = objSalesInvoice.AddEditInvoiceSummaryPos();

                            //BasicDetailClear();               // Comit By Shoaib

                            /////
                            /************ Add Trans ***************/

                            objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            objSalesInvoice.DataFrom = "SALESINV";
                            objSalesInvoice.DeleteTrans();


                            ///////////////////////Modify date 24 Nov 2012 /////////
                            if (rdbCredit.Checked == true) //for credit
                            {
                                DataforTbltrans(Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()));
                            }
                            else if (rdbCash.Checked == true) //for Cash
                            {
                                DataforTbltrans(Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()));

                                //add new 2 rows data for cash
                                for (int i = 0; i < 2; i++)
                                {
                                    objSalesInvoice.TransId = 0;

                                    objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvoiceDate.Text);

                                    objSalesInvoice.DataFrom = "SALESINV";
                                    objSalesInvoice.InvoiceNo = txtInvNo.Text.ToUpper();

                                    if (txtCrDays.Text != "")
                                    {
                                        objSalesInvoice.Crdays = Convert.ToInt32(txtCrDays.Text);
                                    }
                                    else
                                    {
                                        objSalesInvoice.Crdays = 0;
                                    }

                                    /**** Add CR Days *******/
                                    System.DateTime today = Convert.ToDateTime(txtInvoiceDate.Text);
                                    if (txtCrDays.Text != "")
                                    {
                                        System.DateTime answer = today.AddDays(Convert.ToInt32(txtCrDays.Text));
                                        objSalesInvoice.DueDate = answer;
                                    }
                                    else
                                    {
                                        System.DateTime answer = Convert.ToDateTime(txtInvoiceDate.Text);
                                        objSalesInvoice.DueDate = answer;
                                    }

                                    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                    objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                                    if (i == 0)  /* For Client account */
                                    {
                                        objSalesInvoice.AcFileId = Convert.ToInt32(ddlClientAC.SelectedValue);
                                        objSalesInvoice.TransAmt = 0 - (Convert.ToDecimal(txtCashReceived.Text) - Convert.ToDecimal(txtCashReturned.Text));
                                        objSalesInvoice.Description = txtDescription.Text;
                                        if (txtconversionRate.Text != "")
                                        {
                                            objSalesInvoice.FCAmount = 0 - ((Convert.ToDecimal(txtCashReceived.Text) - Convert.ToDecimal(txtCashReturned.Text)) *
                                                Convert.ToDecimal(txtconversionRate.Text));
                                        }

                                    }

                                    if (i == 1) /* For CashReceipt  account */
                                    {
                                        objSalesInvoice.AcFileId = Convert.ToInt32(ddlCashReceipt.SelectedValue);
                                        objSalesInvoice.TransAmt = Convert.ToDecimal(txtCashReceived.Text) - Convert.ToDecimal(txtCashReturned.Text);
                                        objSalesInvoice.Description = txtDescription.Text;
                                        if (txtconversionRate.Text != "")
                                        {
                                            objSalesInvoice.FCAmount = ((Convert.ToDecimal(txtCashReceived.Text) - Convert.ToDecimal(txtCashReturned.Text)) *
                                                   Convert.ToDecimal(txtconversionRate.Text));
                                        }
                                    }


                                    objSalesInvoice.AddEditTrans();

                                }

                            }
                            else if (rdbcashCredit.Checked == true) //Credit card
                            {
                                DataforTbltrans(Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()));

                                //add new 2 rows data 
                                for (int i = 0; i < 2; i++)
                                {
                                    objSalesInvoice.TransId = 0;

                                    objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvoiceDate.Text);

                                    objSalesInvoice.DataFrom = "SALESINV";
                                    objSalesInvoice.InvoiceNo = txtInvNo.Text.ToUpper();

                                    if (txtCrDays.Text != "")
                                    {
                                        objSalesInvoice.Crdays = Convert.ToInt32(txtCrDays.Text);
                                    }
                                    else
                                    {
                                        objSalesInvoice.Crdays = 0;
                                    }

                                    /**** Add CR Days *******/
                                    System.DateTime today = Convert.ToDateTime(txtInvoiceDate.Text);
                                    if (txtCrDays.Text != "")
                                    {
                                        System.DateTime answer = today.AddDays(Convert.ToInt32(txtCrDays.Text));
                                        objSalesInvoice.DueDate = answer;
                                    }
                                    else
                                    {
                                        System.DateTime answer = Convert.ToDateTime(txtInvoiceDate.Text);
                                        objSalesInvoice.DueDate = answer;
                                    }

                                    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                    objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                                    if (i == 0)  /* For Client account */
                                    {
                                        objSalesInvoice.AcFileId = Convert.ToInt32(ddlCashReceipt.SelectedValue);
                                        objSalesInvoice.TransAmt = Convert.ToDecimal(txtCashReceived.Text) - Convert.ToDecimal(txtCashReturned.Text);
                                        objSalesInvoice.Description = txtDescription.Text;
                                        if (txtconversionRate.Text != "")
                                        {
                                            objSalesInvoice.FCAmount = (Convert.ToDecimal(txtCashReceived.Text) - Convert.ToDecimal(txtCashReturned.Text)) *
                                                   Convert.ToDecimal(txtconversionRate.Text);
                                        }
                                    }

                                    if (i == 1) /* For CashReceipt  account */
                                    {
                                        objSalesInvoice.AcFileId = Convert.ToInt32(ddlClientAC.SelectedValue);
                                        objSalesInvoice.TransAmt = 0 - (Convert.ToDecimal(txtCashReceived.Text) - Convert.ToDecimal(txtCashReturned.Text));
                                        objSalesInvoice.Description = txtDescription.Text;
                                        if (txtconversionRate.Text != "")
                                        {
                                            objSalesInvoice.FCAmount = (Convert.ToDecimal(txtCashReceived.Text) - Convert.ToDecimal(txtCashReturned.Text)) *
                                                   Convert.ToDecimal(txtconversionRate.Text);
                                        }
                                    }


                                    objSalesInvoice.AddEditTrans();
                                }

                                //add new 2 rows data for Credit card txtCardCharge
                                if (txtCardCharge.Text != "0" && txtCardCharge.Text != "" && txtCardCharge.Text != "0.00")
                                {
                                    for (int i = 0; i < 2; i++)
                                    {
                                        objSalesInvoice.TransId = 0;
                                        objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvoiceDate.Text);
                                        objSalesInvoice.DataFrom = "SALESINV";
                                        objSalesInvoice.InvoiceNo = txtInvNo.Text.ToUpper();
                                        if (txtCrDays.Text != "")
                                        {
                                            objSalesInvoice.Crdays = Convert.ToInt32(txtCrDays.Text);
                                        }
                                        else
                                        {
                                            objSalesInvoice.Crdays = 0;
                                        }
                                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                        objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/

                                        if (i == 0)  /* For Client account */
                                        {
                                            objSalesInvoice.AcFileId = Convert.ToInt32(ddlCardChargesaccount.SelectedValue);
                                            if (Convert.ToString(hdnCardamount.Value) != "")
                                            {
                                                objSalesInvoice.TransAmt = Convert.ToDecimal(hdnCardamount.Value);
                                            }
                                            if (txtconversionRate.Text != "")
                                            {
                                                objSalesInvoice.FCAmount = Convert.ToDecimal(hdnCardamount.Value) *
                                                       Convert.ToDecimal(txtconversionRate.Text);
                                            }
                                        }
                                        if (i == 1)  /* For Client account */
                                        {

                                            objSalesInvoice.AcFileId = Convert.ToInt32(ddlCashReceipt.SelectedValue);
                                            if (Convert.ToString(hdnCardamount.Value) != "")
                                            {
                                                objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(hdnCardamount.Value);
                                            }

                                            if (txtconversionRate.Text != "")
                                            {
                                                objSalesInvoice.FCAmount = 0 - (Convert.ToDecimal(hdnCardamount.Value) *
                                                       Convert.ToDecimal(txtconversionRate.Text));
                                            }
                                        }
                                        objSalesInvoice.Description = "Credit Card Charges On  " + txtDescription.Text; //Credit Card Charges On 
                                        objSalesInvoice.AddEditTrans();
                                    }
                                }

                            }
                            // Shoaib Working Start
                            else if (rdbARYSahulatCard.Checked == true) //ARY sahulat Card
                            {
                                //if (String.IsNullOrEmpty(txtCustomerName.Text) || String.IsNullOrWhiteSpace(txtCustomerName.Text)
                                //    || String.IsNullOrEmpty(txtContactNumber.Text) || String.IsNullOrEmpty(txtContactNumber.Text))
                                //{
                                //    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Customer Name (OR) Contact Number Should Not be Empty.');", true);
                                //    return;
                                //}
                                //else
                                //{
                                //    string customerAccCode = Convert.ToString(HttpContext.Current.Session["CustomerCode"]);
                                //    if (Array.IndexOf(customerAccCodeArray, customerAccCode) >= 0)
                                //    {
                                //        if (ARYSahulatCardInvoice() == false)
                                //        {
                                //            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Order Registration Failed.');", true);
                                //            return;
                                //        }
                                //        else
                                //        {
                                            DataforTbltrans(Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()));
                                //        }
                                //    }
                                //    else
                                //    {
                                //        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Account Number Not Valid For ARY Sahulat Card.');", true);
                                //        return;
                                //    }                                    
                                //}
                            }
                            // Shoaib Working End

                            /////add new records in trans according to alter charges

                            DataTable dtTransData = (DataTable)ViewState["s"];

                            if (dtTransData.Rows.Count > 0)
                            {
                                for (int x = 0; x < dtTransData.Rows.Count; x++)
                                {
                                    if (Convert.ToString(dtTransData.Rows[x]["AlterCharges"]) != "0.00" && Convert.ToString(dtTransData.Rows[x]["AlterCharges"]) != "0")
                                    {

                                        for (int i = 0; i < 2; i++)
                                        {
                                            objSalesInvoice.TransId = 0;

                                            objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvoiceDate.Text);

                                            objSalesInvoice.DataFrom = "SALESINV";
                                            objSalesInvoice.InvoiceNo = txtInvNo.Text.ToUpper();

                                            if (txtCrDays.Text != "")
                                            {
                                                objSalesInvoice.Crdays = Convert.ToInt32(txtCrDays.Text);
                                            }
                                            else
                                            {
                                                objSalesInvoice.Crdays = 0;
                                            }

                                            /**** Add CR Days *******/
                                            System.DateTime today = Convert.ToDateTime(txtInvoiceDate.Text);
                                            if (txtCrDays.Text != "")
                                            {
                                                System.DateTime answer = today.AddDays(Convert.ToInt32(txtCrDays.Text));
                                                objSalesInvoice.DueDate = answer;
                                            }
                                            else
                                            {
                                                System.DateTime answer = Convert.ToDateTime(txtInvoiceDate.Text);
                                                objSalesInvoice.DueDate = answer;
                                            }

                                            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                            objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                                            if (i == 0)  /* For Dr. account */
                                            {
                                                objSalesInvoice.AcFileId = Convert.ToInt32(dtTransData.Rows[x]["DRID"].ToString()); //Convert.ToInt32(ddlCashReceipt.SelectedValue);
                                                objSalesInvoice.TransAmt = Convert.ToDecimal(dtTransData.Rows[x]["AlterCharges"].ToString()); //Convert.ToDecimal(txtCashReceived.Text) - Convert.ToDecimal(txtCashReturned.Text);
                                                if (txtconversionRate.Text != "")
                                                {
                                                    objSalesInvoice.FCAmount = Convert.ToDecimal(dtTransData.Rows[x]["AlterCharges"].ToString()) *
                                                        Convert.ToDecimal(txtconversionRate.Text);
                                                }
                                                objSalesInvoice.Description = "Charges on Inv. No. " + txtInvNo.Text.ToUpper() + " Item : " + Convert.ToString(dtTransData.Rows[x]["ItemDesc"]);
                                            }

                                            if (i == 1) /* For Cr.  account */
                                            {
                                                objSalesInvoice.AcFileId = Convert.ToInt32(dtTransData.Rows[x]["CRID"].ToString()); // Convert.ToInt32(ddlClientAC.SelectedValue);
                                                objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(dtTransData.Rows[x]["AlterCharges"].ToString()); // 0 - (Convert.ToDecimal(txtCashReceived.Text) - Convert.ToDecimal(txtCashReturned.Text));
                                                if (txtconversionRate.Text != "")
                                                {
                                                    objSalesInvoice.FCAmount = 0 - (Convert.ToDecimal(dtTransData.Rows[x]["AlterCharges"].ToString()) *
                                                        Convert.ToDecimal(txtconversionRate.Text));
                                                }
                                             
                                                objSalesInvoice.Description = "Charges on Inv. No. " + txtInvNo.Text.ToUpper() + " Item : " + Convert.ToString(dtTransData.Rows[x]["ItemDesc"]);
                                            }
                                            objSalesInvoice.AddEditTrans();
                                        }

                                    }
                                }
                            }

                            ////////////END///////////////////


                            /************ Add Invoice**************/
                            if (hdnInvoiceId.Value != "")
                            {
                                objFile.id = Convert.ToInt32(hdnInvoiceId.Value);
                                objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
                                objFile.DataFrom = "SALESINV";
                                objFile.deleteInvoiceFromInvoiceTax();
                            }

                            objFile.InvoiceDate = Convert.ToDateTime(txtInvoiceDate.Text);
                            objFile.InvoiceNo = Convert.ToString(txtInvNo.Text).ToUpper();
                            if (txtCrDays.Text != "")
                            {
                                objFile.CreditDays = Convert.ToInt32(txtCrDays.Text);
                            }
                            else
                            {
                                objFile.CreditDays = 0;
                            }
                            objFile.Amount = Convert.ToDecimal(lblBillAmount.Text);
                            objFile.AcFileId = Convert.ToInt32(ddlClientAC.SelectedValue);
                            objFile.DataFrom = "SALESINV";
                            objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
                            objFile.DebitCredit = "Debit";

                            if (hdnInvoiceId.Value != "")
                            {
                                objFile.InvoiceID = Convert.ToInt32(hdnInvoiceId.Value);
                            }
                            else
                            {
                                objFile.InvoiceID = 0;
                            }
                            objFile.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString());

                            objFile.AddeditInvoice();

                            /************ Add Invoice Detail **************/
                            if (dtresult.Rows.Count > 0)
                            {
                                if (ViewState["s"] != null)
                                {
                                    int intEmpty = 0;
                                    DataTable dt = new DataTable();
                                    dt = (DataTable)ViewState["s"];
                                    if (dt.Rows.Count > 0)
                                    {
                                        for (int i = 0; i < dt.Rows.Count; i++)
                                        {
                                            objSalesInvoice.ItemId = 0;
                                            objSalesInvoice.ItemCode = Convert.ToInt32(dt.Rows[i]["ItemCode"].ToString());
                                            objSalesInvoice.Quantity = Convert.ToDecimal(dt.Rows[i]["Quantity"].ToString());
                                            objSalesInvoice.Rate = Convert.ToDecimal(dt.Rows[i]["Rate"].ToString());
                                            objSalesInvoice.GrossAmount = Convert.ToDecimal(dt.Rows[i]["GrossAmount"].ToString());
                                            objSalesInvoice.SaleTax = Convert.ToDecimal(dt.Rows[i]["SaleTax"].ToString());
                                            objSalesInvoice.SaleTaxAmount = Convert.ToDecimal(dt.Rows[i]["SaleTaxAmount"].ToString());
                                            objSalesInvoice.AmountIncludeTaxes = Convert.ToDecimal(dt.Rows[i]["AmountIncludeTaxes"].ToString());
                                            objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                                            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                            objSalesInvoice.DataFrom = "SALESINV";
                                            objSalesInvoice.storeId = Convert.ToInt32(ddlCostCenter.SelectedValue);

                                            //Add Alter charges ,Dr. , Cr.
                                            if (dt.Rows[i]["AlterCharges"].ToString() != "")
                                            {
                                                objSalesInvoice.AlterCharges = Convert.ToDecimal(dt.Rows[i]["AlterCharges"].ToString());
                                            }
                                            if (dt.Rows[i]["DRID"].ToString() != "")
                                            {
                                                objSalesInvoice.DRID = Convert.ToInt32(dt.Rows[i]["DRID"].ToString());
                                            }
                                            else
                                            {
                                                objSalesInvoice.DRID = intEmpty;
                                            }

                                            if (dt.Rows[i]["CRID"].ToString() != "")
                                            {
                                                objSalesInvoice.CRID = Convert.ToInt32(dt.Rows[i]["CRID"].ToString());
                                            }
                                            else
                                            {
                                                objSalesInvoice.CRID = intEmpty;
                                            }

                                            if (dt.Rows[i]["BoutiqueDiscount"].ToString() != "")
                                            {
                                                objSalesInvoice.BoutiqueDiscount = Convert.ToDecimal(dt.Rows[i]["BoutiqueDiscount"].ToString());
                                            }

                                            if (dt.Rows[i]["BoutiqueAmountAfterDis"].ToString() != "")
                                            {
                                                objSalesInvoice.BoutiqueAmountAfterDis = Convert.ToDecimal(dt.Rows[i]["BoutiqueAmountAfterDis"].ToString());
                                            }

                                            DataTable dtInvoiceSummary = new DataTable();
                                            dtInvoiceSummary = objSalesInvoice.AddEditBoutiqueInvoiceDetail();






                                            // Trans Entry For Contract Purchase //


                                            objCust.ItemId = Convert.ToInt32(dt.Rows[i]["ItemCode"].ToString());
                                            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                            DataSet ds = new DataSet();
                                            ds = objCust.getItems();
                                            if (ds.Tables[0].Rows.Count > 0)
                                            {
                                                if (Convert.ToString(ds.Tables[0].Rows[0]["PurchaseType"]) == "Contract Purchase")
                                                {
                                                    for (int k = 0; k < 2; k++)
                                                    {
                                                        objSalesInvoice.ItemCode = Convert.ToInt32(dt.Rows[i]["ItemCode"].ToString());
                                                        objSalesInvoice.PurchaseType = "Contract Purchase";
                                                        objSalesInvoice.DataFrom = "PURINV";
                                                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                                        DataTable DtPurchase = new DataTable();
                                                        DtPurchase = objSalesInvoice.GetBoutiquePurchaseinvoiceDetailByItem();
                                                        if (DtPurchase.Rows.Count > 0)
                                                        {

                                                            objSalesInvoice.TransId = 0;
                                                            objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvoiceDate.Text);
                                                            objSalesInvoice.DataFrom = "SALESINV";
                                                            objSalesInvoice.InvoiceNo = txtInvNo.Text.ToUpper();
                                                            if (txtCrDays.Text != "")
                                                            {
                                                                objSalesInvoice.Crdays = Convert.ToInt32(txtCrDays.Text);
                                                            }
                                                            else
                                                            {
                                                                objSalesInvoice.Crdays = 0;
                                                            }

                                                            /**** Add CR Days *******/
                                                            System.DateTime today = Convert.ToDateTime(txtInvoiceDate.Text);
                                                            if (txtCrDays.Text != "")
                                                            {
                                                                System.DateTime answer = today.AddDays(Convert.ToInt32(txtCrDays.Text));
                                                                objSalesInvoice.DueDate = answer;
                                                            }
                                                            else
                                                            {
                                                                System.DateTime answer = Convert.ToDateTime(txtInvoiceDate.Text);
                                                                objSalesInvoice.DueDate = answer;
                                                            }
                                                            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                                            objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString());
                                                            if (k == 0)
                                                            {
                                                                objSalesInvoice.AcFileId = Convert.ToInt32(DtPurchase.Rows[0]["ClientAccountId"].ToString());
                                                                if (dt.Rows[i]["BoutiqueDiscount"].ToString() != "")
                                                                {
                                                                    Decimal purChaseNetAmount = Convert.ToDecimal(DtPurchase.Rows[0]["Rate"].ToString()) / Convert.ToDecimal(DtPurchase.Rows[0]["BoutiqueConvRate"].ToString());

                                                                    //Decimal PurchaseAmount1 = (
                                                                    //  purChaseNetAmount -
                                                                    //    (
                                                                    //        purChaseNetAmount
                                                                    //       * (Convert.ToDecimal(dt.Rows[i]["BoutiqueDiscount"]) / 100)
                                                                    //    )
                                                                    //    - Convert.ToDecimal(dt.Rows[i]["AlterCharges"]));


                                                                    Decimal PurchaseAmount1 = (
                                                           purChaseNetAmount -
                                                             (
                                                                 purChaseNetAmount
                                                                * (Convert.ToDecimal(dt.Rows[i]["BoutiqueDiscount"]) / 100)
                                                             ) );
                                                                    /////new 
                                                                  PurchaseAmount1=PurchaseAmount1-Convert.ToDecimal(dt.Rows[i]["AlterCharges"].ToString());
                                                                    Decimal PurchaseAmount2 = 0;
                                                                    if (Convert.ToString(DtPurchase.Rows[0]["BoutiquePurchaseDiscount"]) != "")
                                                                    {
                                                                        PurchaseAmount2 = PurchaseAmount1 - (PurchaseAmount1 * (Convert.ToDecimal(DtPurchase.Rows[0]["BoutiquePurchaseDiscount"]) / 100));
                                                                    }
                                                                    else
                                                                    {
                                                                        PurchaseAmount2 = PurchaseAmount1;
                                                                    }

                                                                    PurchaseAmount2 = PurchaseAmount2+Convert.ToDecimal(dt.Rows[i]["AlterCharges"].ToString());

                                                                    Decimal TransAmount = (PurchaseAmount2 * Convert.ToDecimal(dt.Rows[i]["Quantity"].ToString()));
                                                                    objSalesInvoice.TransAmt = 0 - TransAmount;

                                                                    if (txtconversionRate.Text != "")
                                                                    {
                                                                        //Decimal FCAmount = (TransAmount * Convert.ToDecimal(DtPurchase.Rows[0]["BoutiqueConvRate"].ToString()));

                                                                        Decimal FCAmount = (TransAmount * Convert.ToDecimal(txtconversionRate.Text));
                                                                        objSalesInvoice.FCAmount = 0 - FCAmount;
                                                                    }
                                                                   

                                                                }

                                                                else
                                                                {
                                                                    Decimal PurchaseAmount2 = 0;
                                                                    if (Convert.ToString(DtPurchase.Rows[0]["BoutiquePurchaseDiscount"]) != "")
                                                                    {
                                                                        PurchaseAmount2 =
                                                                           Convert.ToDecimal(DtPurchase.Rows[0]["BoutiqueCostPrice"].ToString()) -
                                                                          (Convert.ToDecimal(DtPurchase.Rows[0]["BoutiqueCostPrice"].ToString()) * (Convert.ToDecimal(DtPurchase.Rows[0]["BoutiquePurchaseDiscount"]) / 100));
                                                                    }
                                                                    else
                                                                    {
                                                                        PurchaseAmount2 = Convert.ToDecimal(DtPurchase.Rows[0]["BoutiqueCostPrice"].ToString());

                                                                    }


                                                                    Decimal TransAmount = (PurchaseAmount2 * Convert.ToDecimal(dt.Rows[i]["Quantity"].ToString()));
                                                                    objSalesInvoice.TransAmt = 0 - TransAmount;

                                                                    if (txtconversionRate.Text != "")
                                                                    {
                                                                        //Decimal FCAmount = (TransAmount * Convert.ToDecimal(DtPurchase.Rows[0]["BoutiqueConvRate"].ToString()));

                                                                        Decimal FCAmount = (TransAmount * Convert.ToDecimal(txtconversionRate.Text));
                                                                        objSalesInvoice.FCAmount = 0 - FCAmount;
                                                                    }
                                                                }

                                                            }
                                                            else
                                                            {

                                                                if (dt.Rows[i]["BoutiqueDiscount"].ToString() != "")
                                                                {
                                                                    objSalesInvoice.AcFileId = Convert.ToInt32(DtPurchase.Rows[0]["SalesAccountId"].ToString());

                                                                    Decimal purChaseNetAmount = Convert.ToDecimal(DtPurchase.Rows[0]["Rate"].ToString()) / Convert.ToDecimal(DtPurchase.Rows[0]["BoutiqueConvRate"].ToString());

                                                                    Decimal PurchaseAmount1 = (
                                                                      purChaseNetAmount -
                                                                        (
                                                                            purChaseNetAmount
                                                                           * (Convert.ToDecimal(dt.Rows[i]["BoutiqueDiscount"]) / 100)
                                                                        )
                                                                        - Convert.ToDecimal(dt.Rows[i]["AlterCharges"]));


                                                                    Decimal PurchaseAmount2 = 0;
                                                                    if (Convert.ToString(DtPurchase.Rows[0]["BoutiquePurchaseDiscount"]) != "")
                                                                    {
                                                                        PurchaseAmount2 = PurchaseAmount1 - (PurchaseAmount1 * (Convert.ToDecimal(DtPurchase.Rows[0]["BoutiquePurchaseDiscount"]) / 100));
                                                                    }
                                                                    else
                                                                    {
                                                                        PurchaseAmount2 = PurchaseAmount1;
                                                                    }
                                                                    PurchaseAmount2 = PurchaseAmount2 + Convert.ToDecimal(dt.Rows[i]["AlterCharges"].ToString());
                                                                    Decimal TransAmount = (PurchaseAmount2 * Convert.ToDecimal(dt.Rows[i]["Quantity"].ToString()));
                                                                    objSalesInvoice.TransAmt = TransAmount;

                                                                    Decimal FCAmount = (TransAmount * Convert.ToDecimal(DtPurchase.Rows[0]["BoutiqueConvRate"].ToString()));
                                                                    objSalesInvoice.FCAmount = FCAmount;


                                                                }
                                                                else
                                                                {
                                                                    Decimal purChaseNetAmount = Convert.ToDecimal(DtPurchase.Rows[0]["Rate"].ToString()) / Convert.ToDecimal(DtPurchase.Rows[0]["BoutiqueConvRate"].ToString());
                                                                    Decimal PurchaseAmount2 = 0;
                                                                    if (Convert.ToString(DtPurchase.Rows[0]["BoutiquePurchaseDiscount"]) != "")
                                                                    {
                                                                        PurchaseAmount2 =
                                                                           purChaseNetAmount -
                                                                          (purChaseNetAmount * (Convert.ToDecimal(DtPurchase.Rows[0]["BoutiquePurchaseDiscount"]) / 100));
                                                                    }
                                                                    else
                                                                    {
                                                                        PurchaseAmount2 = purChaseNetAmount;

                                                                    }


                                                                    Decimal TransAmount = (PurchaseAmount2 * Convert.ToDecimal(dt.Rows[i]["Quantity"].ToString()));
                                                                    objSalesInvoice.TransAmt = TransAmount;

                                                                    if (txtconversionRate.Text != "")
                                                                    {
                                                                        //Decimal FCAmount = (TransAmount * Convert.ToDecimal(DtPurchase.Rows[0]["BoutiqueConvRate"].ToString()));
                                                                        Decimal FCAmount = (TransAmount * Convert.ToDecimal(txtconversionRate.Text));
                                                                        objSalesInvoice.FCAmount = FCAmount;
                                                                    }

                                                                    objSalesInvoice.AcFileId = Convert.ToInt32(DtPurchase.Rows[0]["SalesAccountId"].ToString());

                                                                }
                                                            }
                                                            objSalesInvoice.Description = Convert.ToString(ds.Tables[0].Rows[0]["ItemDesc"]) + " , Sale Discount " + (dt.Rows[i]["BoutiqueDiscount"].ToString()) + " %";
                                                            objSalesInvoice.AddEditTrans();

                                                        }
                                                    }
                                                }
                                            }


                                        }
                                    }
                                }

                            }




                            if (Convert.ToInt32(hdnInvoiceSummaryId.Value) > 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Updated.');location.href='POSSaleInvoice.aspx';", true);

                            }

                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Saved.');location.href='POSSaleInvoice.aspx';", true);
                            }
                        }
                        else
                        {
                            if (ddlCashReceipt.SelectedValue.ToString() == "0" && rdbcashCredit.Checked == true)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please select Credit Card A/C.');", true);
                            }
                            else if (ddlCashReceipt.SelectedValue.ToString() == "0" && rdbCash.Checked == true)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please select Cash Receipt A/C.');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Invalid Cash Received Amount.');", true);
                            }
                        }

                    }

                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Invalid Invoice Amount.');", true);

                    }
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Date out of accouting year.');", true);

                }
            }

            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please Fill Items Details.');", true);
            }
        }
    }
    #endregion

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("POSSaleInvoice.aspx");
    }
    #endregion

    #region Fill session Table
    public void filltable()
    {
        if (ViewState["s"] == null)
        {

            DataColumn column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "ItemCode";
            column.Caption = "ItemCode";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ItemDesc";
            column.Caption = "ItemDesc";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Quantity";
            column.Caption = "Quantity";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Rate";
            column.Caption = "Rate";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "GrossAmount";
            column.Caption = "GrossAmount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "SaleTax";
            column.Caption = "SaleTax";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "SaleTaxAmount";
            column.Caption = "SaleTaxAmount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "AmountIncludeTaxes";
            column.Caption = "AmountIncludeTaxes";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            // /**********/

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "AlterCharges";
            column.Caption = "AlterCharges";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "DRID";
            column.Caption = "DRID";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "DRTitle";
            column.Caption = "DRTitle";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "CRID";
            column.Caption = "CRID";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "CRTitle";
            column.Caption = "CRTitle";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "BoutiqueDiscount";
            column.Caption = "BoutiqueDiscount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "BoutiqueAmountAfterDis";
            column.Caption = "BoutiqueAmountAfterDis";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            /**********/


        }
        else
        {
            dttbl = (DataTable)ViewState["s"];
        }
    }
    #endregion

    #region Add New Bill details
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        //AddDiscount();
        //// alter charges validation
        hdnButtonText.Value = "";
        int MsgFlag = 0;

        if (txtAlterCharges.Text == "" || txtAlterCharges.Text == "0.00" || txtAlterCharges.Text == "0")
        {

            ddlDR.SelectedValue = "0";
            ddlCR.SelectedValue = "0";
            MsgFlag = 1;
        }
        else
        {
            if (ddlDR.SelectedValue.ToString() == "0" && ddlCR.SelectedValue.ToString() == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please select Dr. and Cr.');", true);
            }
            else if (ddlDR.SelectedValue.ToString() == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please select Dr.');", true);
            }
            else if (ddlCR.SelectedValue.ToString() == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please select Cr.');", true);
            }
            else
            {
                MsgFlag = 1;
            }

            //RequiredFieldValidator10.Enabled = true;
            //RequiredFieldValidator11.Enabled = true;

        }

        if (MsgFlag == 1)
        {

            filltable();
            if (hdndetail.Value == "")
            {
                DataRow row;
                if (dttbl.Rows.Count > 0)
                {

                    int maxid = 0;
                    for (int i = 0; i < dttbl.Rows.Count; i++)
                    {
                        if (dttbl.Rows[i]["Id"].ToString() != "")
                        {
                            if (maxid < Convert.ToInt32(dttbl.Rows[i]["Id"].ToString()))
                            {
                                maxid = Convert.ToInt32(dttbl.Rows[i]["Id"].ToString());
                            }
                        }
                    }
                    decimal Rate = 0;
                    decimal SaleTex = 0;

                    /**********/
                    decimal AlterCharges = 0;

                    /**********/

                    row = dttbl.NewRow();
                    row["Id"] = maxid + 1;
                    row["ItemCode"] = Convert.ToInt32(ddlItemDesc.SelectedValue.ToString());
                    row["ItemDesc"] = Convert.ToString(ddlItemDesc.SelectedItem.Text);
                    row["Quantity"] = Convert.ToDecimal(txtQuantity.Text);
                    if (lblRateAfterDiscount.Text != "")
                    {
                        Rate = Convert.ToDecimal(lblRateAfterDiscount.Text);
                    }
                    if (txtSt.Text != "")
                    {
                        SaleTex = Convert.ToDecimal(txtSt.Text);
                    }


                    row["Rate"] = Convert.ToDecimal(txtRate.Text);
                    row["GrossAmount"] = Rate * Convert.ToDecimal(txtQuantity.Text);
                    row["SaleTax"] = SaleTex;
                    row["SaleTaxAmount"] = (SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100;
                    row["AmountIncludeTaxes"] = (Rate * Convert.ToDecimal(txtQuantity.Text)) + ((SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100);

                    // /**********/
                    if (txtAlterCharges.Text != "")
                    {
                        AlterCharges = Convert.ToDecimal(txtAlterCharges.Text);
                    }

                    row["AlterCharges"] = AlterCharges;
                    if (ddlDR.SelectedValue.ToString() != "0")
                    {
                        row["DRID"] = Convert.ToInt32(ddlDR.SelectedValue.ToString());
                        row["DRTitle"] = ddlDR.SelectedItem.Text;
                    }
                    else
                    {
                        row["DRTitle"] = "";
                    }

                    if (ddlCR.SelectedValue.ToString() != "0")
                    {
                        row["CRID"] = Convert.ToInt32(ddlCR.SelectedValue.ToString());
                        row["CRTitle"] = ddlCR.SelectedItem.Text;
                    }
                    else
                    {
                        row["CRTitle"] = "";
                    }
                    if (txtBoutiqueDiscount.Text != "")
                    {

                        row["BoutiqueDiscount"] = Convert.ToDecimal(txtBoutiqueDiscount.Text);
                    }
                    if (lblRateAfterDiscount.Text != "")
                    {

                        row["BoutiqueAmountAfterDis"] = Convert.ToDecimal(lblRateAfterDiscount.Text);
                    }


                    /**********/

                }
                else
                {
                    decimal Rate = 0;
                    decimal SaleTex = 0;
                    /**********/
                    decimal AlterCharges = 0;

                    /**********/
                    row = dttbl.NewRow();
                    row["Id"] = 1;
                    row["ItemCode"] = Convert.ToInt32(ddlItemDesc.SelectedValue.ToString());
                    row["ItemDesc"] = Convert.ToString(ddlItemDesc.SelectedItem.Text);
                    row["Quantity"] = Convert.ToDecimal(txtQuantity.Text);
                    if (lblRateAfterDiscount.Text != "")
                    {
                        Rate = Convert.ToDecimal(lblRateAfterDiscount.Text);
                    }
                    if (txtSt.Text != "")
                    {
                        SaleTex = Convert.ToDecimal(txtSt.Text);
                    }
                    row["Rate"] = txtRate.Text;
                    row["GrossAmount"] = Rate * Convert.ToDecimal(txtQuantity.Text);
                    row["SaleTax"] = SaleTex;
                    row["SaleTaxAmount"] = (SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100;
                    row["AmountIncludeTaxes"] = (Rate * Convert.ToDecimal(txtQuantity.Text)) + ((SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100);

                    ///  /**********/
                    if (txtAlterCharges.Text != "")
                    {
                        AlterCharges = Convert.ToDecimal(txtAlterCharges.Text);
                    }

                    row["AlterCharges"] = AlterCharges;
                    if (ddlDR.SelectedValue.ToString() != "0")
                    {
                        row["DRID"] = Convert.ToInt32(ddlDR.SelectedValue.ToString());
                        row["DRTitle"] = ddlDR.SelectedItem.Text;
                    }
                    //else
                    //{
                    //    row["DRID"] = null;
                    //}

                    if (ddlCR.SelectedValue.ToString() != "0")
                    {
                        row["CRID"] = Convert.ToInt32(ddlCR.SelectedValue.ToString());
                        row["CRTitle"] = ddlCR.SelectedItem.Text;
                    }
                    //else
                    //{
                    //    row["CRID"] = null;
                    //}

                    /**********/
                    if (txtBoutiqueDiscount.Text != "")
                    {

                        row["BoutiqueDiscount"] = Convert.ToDecimal(txtBoutiqueDiscount.Text);
                    }
                    if (lblRateAfterDiscount.Text != "")
                    {

                        row["BoutiqueAmountAfterDis"] = Convert.ToDecimal(lblRateAfterDiscount.Text);
                    }


                }
                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
            }

            else
            {
                decimal Rate = 0;
                decimal AlterCharges = 0;
                decimal SaleTex = 0;
                for (int i = 0; i < dttbl.Rows.Count; i++)
                {
                    if (dttbl.Rows[i]["Id"].ToString() == hdndetail.Value)
                    {
                        dttbl.Rows[i]["ItemCode"] = Convert.ToInt32(ddlItemDesc.SelectedValue.ToString());
                        dttbl.Rows[i]["ItemDesc"] = Convert.ToString(ddlItemDesc.SelectedItem.Text);
                        dttbl.Rows[i]["Quantity"] = Convert.ToDecimal(txtQuantity.Text);
                        if (lblRateAfterDiscount.Text != "")
                        {
                            Rate = Convert.ToDecimal(lblRateAfterDiscount.Text);
                        }
                        if (txtSt.Text != "")
                        {
                            SaleTex = Convert.ToDecimal(txtSt.Text);
                        }
                        dttbl.Rows[i]["Rate"] = txtRate.Text;
                        dttbl.Rows[i]["GrossAmount"] = Rate * Convert.ToDecimal(txtQuantity.Text);
                        dttbl.Rows[i]["SaleTax"] = SaleTex;
                        dttbl.Rows[i]["SaleTaxAmount"] = (SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100;
                        dttbl.Rows[i]["AmountIncludeTaxes"] = (Rate * Convert.ToDecimal(txtQuantity.Text)) + ((SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100);

                        /**********/
                        //dttbl.Rows[i]["CCCharges"] = ""; // Convert.ToDecimal(txtCCCharge.Text);
                        //dttbl.Rows[i]["AlterCharges"] = Convert.ToDecimal(txtAlterCharges.Text);
                        //dttbl.Rows[i]["OtherCharges"] = Convert.ToDecimal(txtOtherCharges.Text);

                        ///  /**********/
                        if (txtAlterCharges.Text != "")
                        {
                            AlterCharges = Convert.ToDecimal(txtAlterCharges.Text);
                        }

                        dttbl.Rows[i]["AlterCharges"] = AlterCharges;

                        dttbl.Rows[i]["AlterCharges"] = AlterCharges;
                        if (ddlDR.SelectedValue.ToString() != "0")
                        {
                            dttbl.Rows[i]["DRID"] = Convert.ToInt32(ddlDR.SelectedValue.ToString());
                            dttbl.Rows[i]["DRTitle"] = ddlDR.SelectedItem.Text;
                        }
                        //else
                        //{
                        //    row["DRID"] = null;
                        //}

                        if (ddlCR.SelectedValue.ToString() != "0")
                        {
                            dttbl.Rows[i]["CRID"] = Convert.ToInt32(ddlCR.SelectedValue.ToString());
                            dttbl.Rows[i]["CRTitle"] = ddlCR.SelectedItem.Text;
                        }
                        //else
                        //{
                        //    row["CRID"] = null;
                        //}

                        /**********/
                        if (txtBoutiqueDiscount.Text != "")
                        {

                            dttbl.Rows[i]["BoutiqueDiscount"] = Convert.ToDecimal(txtBoutiqueDiscount.Text);
                        }

                        if (lblRateAfterDiscount.Text != "")
                        {

                            dttbl.Rows[i]["BoutiqueAmountAfterDis"] = Convert.ToDecimal(lblRateAfterDiscount.Text);
                        }



                    }
                }
            }
            ViewState.Add("s", dttbl);
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            ShowTotal();
            if (dttbl.Rows.Count > 0)
            {
                dgGallery.Visible = true;
            }

            ClearInvoiceDetail();
            txtBarCodeNo.Focus();
        }
    }
    #endregion

    #region Grid Event
    protected void dgGallery_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{
        //    ((Label)e.Item.FindControl("lblInvoiceDate")).Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "InvoiceDate").ToString());

        //}



    }
    protected void dgGallery_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        dttbl = (DataTable)ViewState["s"];
        if (e.CommandName == "Delete")
        {
            int j = 0;
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    dttbl.Rows.RemoveAt(i);
                }
            }
            dttbl.AcceptChanges();
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            ViewState.Add("s", dttbl);
            ShowTotal();

        }

        if (e.CommandName == "Edit")
        {
            lblPurchaseRate.Text = "";

            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    ddlItemDesc.SelectedValue = dttbl.Rows[i]["ItemCode"].ToString();
                    txtQuantity.Text = dttbl.Rows[i]["Quantity"].ToString();
                    txtRate.Text = dttbl.Rows[i]["Rate"].ToString();
                    hdndetail.Value = dttbl.Rows[i]["Id"].ToString();
                    txtSt.Text = dttbl.Rows[i]["SaleTax"].ToString();

                    objCust.ItemId = Convert.ToInt32(ddlItemDesc.SelectedValue);
                    objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                    DataSet ds = new DataSet();
                    ds = objCust.getItems();
                    if (ds.Tables[0].Rows.Count == 1)
                    {                      
                        txtBarCodeNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["BoutiqueItemCode"]);                    
                    }
                    //txtBarCodeNo.Text = dttbl.Rows[i]["ItemCode"].ToString();
                    //
                    /*******/
                    txtAlterCharges.Text = dttbl.Rows[i]["AlterCharges"].ToString();

                    if (dttbl.Rows[i]["DRID"].ToString() != "")
                    {
                        ddlDR.SelectedValue = dttbl.Rows[i]["DRID"].ToString();
                    }
                    if (dttbl.Rows[i]["CRID"].ToString() != "")
                    {
                        ddlCR.SelectedValue = dttbl.Rows[i]["CRID"].ToString();
                    }
                    txtBoutiqueDiscount.Text = dttbl.Rows[i]["BoutiqueDiscount"].ToString();
                    lblRateAfterDiscount.Text = dttbl.Rows[i]["BoutiqueAmountAfterDis"].ToString();

                    objCust.ItemId = Convert.ToInt32(ddlItemDesc.SelectedValue);
                    objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                    DataSet ds1 = new DataSet();
                    ds1 = objCust.getPurchasePP();
                    if (ds1.Tables[0].Rows.Count > 0)
                    {
                        lblPurchaseRate.Text = "P/P : " + (Convert.ToDecimal(ds1.Tables[0].Rows[0]["Rate"]) / Convert.ToDecimal(ds1.Tables[0].Rows[0]["BoutiqueConvRate"])).ToString("0.00");
                    }


                }
            }
        }
    }
    #endregion

    #region ClearInvoiceDetail
    protected void ClearInvoiceDetail()
    {
        ddlItemDesc.SelectedValue = "0";
        txtQuantity.Text = "";
        txtRate.Text = "";
        lblPurchaseRate.Text = "";
        hdndetail.Value = "";

        txtAlterCharges.Text = "";
        ddlDR.SelectedValue = "0";
     
        RequiredFieldValidator10.Enabled = false;
        RequiredFieldValidator11.Enabled = false;
        txtBoutiqueDiscount.Text = "0.00";
        txtBarCodeNo.Text = "";
        lblRateAfterDiscount.Text = "0.00";
        txtSt.Text = "5.00";
    }

    public void BasicDetailClear()
    {
        txtCustomerName.Text = "";
        txtContactNumber.Text = "";
        txtEmailAddress.Text = "";
    }

    #endregion

    #region Show Total
    protected void ShowTotal()
    {
        Decimal Quantity = 0;
        Decimal TotalGrossAmount = 0;
        Decimal TotalStAmount = 0;
        Decimal amountIncludingTax = 0;

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["s"];
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Quantity += Convert.ToDecimal(dt.Rows[i]["Quantity"]);
                TotalGrossAmount += Convert.ToDecimal(dt.Rows[i]["GrossAmount"]);
                TotalStAmount += Convert.ToDecimal(dt.Rows[i]["SaleTaxAmount"]);
                amountIncludingTax += (Convert.ToDecimal(dt.Rows[i]["SaleTaxAmount"]) + Convert.ToDecimal(dt.Rows[i]["GrossAmount"]));

            }
        }

        txtTotalQuantity.Text = String.Format("{0:C}", Quantity).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        txtTotalGrossAmount.Text = String.Format("{0:C}", TotalGrossAmount).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        // Shoaib Working Start
        txtSaleTax.Text = Convert.ToString(String.Format("{0:C}", TotalStAmount).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
        txtTotalAmountIncludingTax.Text = Convert.ToString(String.Format("{0:C}", amountIncludingTax).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
        // Shoaib Working End
        lblBillAmount.Text = String.Format("{0:C}", (amountIncludingTax + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        if (rdbCredit.Checked == true)
        {
            txtCashReturned.Text = "0.00";
        }
        else
        {
            txtCashReturned.Text = String.Format("{0:C}", (Convert.ToDecimal(txtCashReceived.Text) - (amountIncludingTax + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text)))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        }



        if (txtCardCharge.Text != "")
        {
            hdnCardamount.Value = Convert.ToString((amountIncludingTax + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text)) * (Convert.ToDecimal(txtCardCharge.Text) / 100));
            lblCardamount.Text = Convert.ToDecimal((amountIncludingTax + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text)) * (Convert.ToDecimal(txtCardCharge.Text) / 100)).ToString("0.00");
        }



    }
    #endregion

    #region Pagging
    private void bindpagging()
    {
        Last = 0;
        Corrent = 0;
        FirstRecord = 0;
        LastRecord = 0;
        //  int row = Convert.ToInt32(Request["row"]);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.InvoiceSummaryId = 0;
        DataTable dt = new DataTable();
        objSalesInvoice.DataFrom = "SALESINV";
        dt = objSalesInvoice.getInvoiceSummary();
        if (dt.Rows.Count > 0)
        {
            Session["InvoiceSummary"] = dt;
            Last = dt.Rows.Count;
            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            //Corrent = Convert.ToInt32(dt.Rows[0]["Row"]);
            Session["FirstRecord"] = FirstRecord;
            Session["LastRecord"] = LastRecord;
            Session["Corrent"] = LastRecord;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "location.href='POSSaleInvoice.aspx?addnew=1';", true);
        }


    }
    protected void lnkFirst_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["FirstRecord"];
        BindInvoice();

    }
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["FirstRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) - 1;
            BindInvoice();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["LastRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) + 1;
            BindInvoice();
        }
    }
    protected void lnLast_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["LastRecord"];
        BindInvoice();
    }
    #endregion

    #region Fill invoice summary
    protected void BindInvoice()
    {
        if (Session["InvoiceSummary"] != null)
        {
            DataTable dtInvoiceSummary = (DataTable)Session["InvoiceSummary"];
            if (dtInvoiceSummary.Rows.Count > 0)
            {

                DataView DvInvoiceSummary = dtInvoiceSummary.DefaultView;
                DvInvoiceSummary.RowFilter = "Row = '" + Session["Corrent"].ToString() + "'";
                dtInvoiceSummary = DvInvoiceSummary.ToTable();
                if (dtInvoiceSummary.Rows.Count > 0)
                {
                    txtconversionRate.Text = "";
                    txtInvNo.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["InvNo"]);
                    txtInvoiceDate.Text = Convert.ToDateTime(dtInvoiceSummary.Rows[0]["InvDate"].ToString()).ToString("dd MMM yyyy");

                    if (Convert.ToString(dtInvoiceSummary.Rows[0]["CrDays"]) != "0")
                    {
                        txtCrDays.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CrDays"]);
                    }
                    else
                    {
                        txtCrDays.Text = "";
                    }

                    //// bind details
                    if (dtInvoiceSummary.Rows[0]["CustomerName"].ToString() != "-")
                    {
                        txtCustomerName.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CustomerName"]);
                    }
                    else
                    {
                        txtCustomerName.Text = "";
                    }
                    if (dtInvoiceSummary.Rows[0]["ContactNumber"].ToString() != "-")
                    {
                        //txtContactNumber.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["ContactNumber"]); // Comit by Shoaib

                        // Shoaib Working Start
                        string contact = Convert.ToString(dtInvoiceSummary.Rows[0]["ContactNumber"]);
                        txtContactNumber.Text = contact.StartsWith("00971") ?
                           contact.Substring(contact.IndexOf(contact) + prefixNumber.Length, contact.Length - prefixNumber.Length) : contact;
                        // Shoaib Working End
                    }
                    else
                    {
                        txtContactNumber.Text = "";
                    }
                    if (dtInvoiceSummary.Rows[0]["EmailAddress"].ToString() != "-")
                    {
                        txtEmailAddress.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["EmailAddress"]);
                    }
                    else
                    {
                        txtEmailAddress.Text = "";
                    }
                    if (Convert.ToString(dtInvoiceSummary.Rows[0]["ConversionRate"]) != "")
                    {
                        txtconversionRate.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["ConversionRate"]);
                    }
                    ddlCostCenter.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["storeId"]);
                    ddlClientAC.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["ClientAccountId"]);
                    ddlSaleAC.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["SalesAccountId"]);
                    txtTotalQuantity.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["TotalQuantity"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    txtTotalGrossAmount.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["TotalGrossAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

                    // Shoaib Working Start  
                    txtSaleTax.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["TotalSaleTaxAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    txtSaleTax.Text = txtSaleTax.Text.Trim();
                    txtTotalAmountIncludingTax.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["TotalAmountIncludeTax"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    txtTotalAmountIncludingTax.Text = txtTotalAmountIncludingTax.Text.Trim();
                    // Shoaib Working End

                    txtCartagecharges.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["CartageAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    txtCartagecharges.Text = txtCartagecharges.Text.Trim();

                    lblBillAmount.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["BillAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

                    hdnInvoiceSummaryId.Value = Convert.ToString(dtInvoiceSummary.Rows[0]["InvoiceSummaryId"]);

                    txtDiscount.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["InvoiceDiscount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    txtDiscount.Text = txtDiscount.Text.Trim();

                    txtDescription.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["Description"]);
                    txtCardCharge.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CardChargesRate"]);
                    lblCardamount.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CardChargesAmount"]);
                    hdnCardamount.Value = Convert.ToString(dtInvoiceSummary.Rows[0]["CardChargesAmount"]);
                    ddlCardChargesaccount.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["CardChargesAcfileId"]);
                    txtSalesPerson.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["SalesPersonAbbr"]).ToUpper(); ;

                    fillInvoiceDetails(Convert.ToInt32(dtInvoiceSummary.Rows[0]["InvoiceSummaryId"]));
                    if (Convert.ToString(dtInvoiceSummary.Rows[0]["POSSaleType"]) == "Cash")
                    {
                        rdbCash.Checked = true;
                        rdbcashCredit.Checked = false;
                        rdbCredit.Checked = false;
                        rdbARYSahulatCard.Checked = false;          // Shoaib Working

                        txtCreditCarNo.Enabled = false;
                        txtApprovelNO.Enabled = false;

                        FillCashReceived(1);
                        txtCashReceived.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CashReceived"]);
                        txtCashReturned.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CashReturned"]);

                    }
                    if (Convert.ToString(dtInvoiceSummary.Rows[0]["POSSaleType"]) == "Credit Card")
                    {
                        rdbCash.Checked = false;
                        rdbcashCredit.Checked = true;
                        rdbCredit.Checked = false;
                        rdbARYSahulatCard.Checked = false;          // Shoaib Working

                        //rdbcashCredit.Checked = true;
                        txtCreditCarNo.Enabled = true;
                        txtApprovelNO.Enabled = true;
                        txtCreditCarNo.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CreditCardNo"]);
                        txtApprovelNO.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["Approval"]);
                        txtCashReceived.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CashReceived"]);
                        txtCashReturned.Text = "0.00";
                        FillCashReceived(2);

                    }
                    if (Convert.ToString(dtInvoiceSummary.Rows[0]["POSSaleType"]) == "CR / CR Note / Order")
                    {
                        rdbCash.Checked = false;
                        rdbcashCredit.Checked = false;
                        rdbCredit.Checked = true;
                        rdbARYSahulatCard.Checked = false;          // Shoaib Working

                        txtCreditCarNo.Enabled = false;
                        txtApprovelNO.Enabled = false;
                        txtCashReceived.Text = "0.00";
                        txtCashReturned.Text = "0.00";
                        txtCashReceived.Enabled = false;
                        txtCashReturned.Enabled = false;
                        FillCashReceived(3);

                    }
                    // Shoaib Working Start
                    if (Convert.ToString(dtInvoiceSummary.Rows[0]["POSSaleType"]) == "ARY Sahulat Card")
                    {
                        rdbCash.Checked = false;
                        rdbcashCredit.Checked = false;
                        rdbCredit.Checked = false;
                        rdbARYSahulatCard.Checked = true;

                        txtCreditCarNo.Enabled = false;
                        txtApprovelNO.Enabled = false;
                        txtCashReceived.Text = "0.00";
                        txtCashReturned.Text = "0.00";
                        txtCashReceived.Enabled = false;
                        txtCashReturned.Enabled = false;
                        FillCashReceived(4);

                    }
                    // Shoaib Working End

                    ddlCashReceipt.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["CashReceivedAcFileID"]);



                    /* For invoice Update */
                    hdnInvoiceId.Value = Convert.ToString(dtInvoiceSummary.Rows[0]["InvoiceID"]);


                    //objSalesInvoice.CashReceived = Convert.ToDecimal(txtCashReceived.Text);
                    //objSalesInvoice.CashReturned = Convert.ToDecimal(txtCashReturned.Text);
                    //objSalesInvoice.CashReceivedAcFileID = 0;

                }
            }

        }
    }
    #endregion

    #region fillInvoiceDetails
    protected void fillInvoiceDetails(int pid)
    {
        ViewState["s"] = null;

        DataTable dtInvoice = new DataTable();
        objSalesInvoice.InvoiceSummaryId = pid;
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objSalesInvoice.DataFrom = "SALESINV";
        dtInvoice = objSalesInvoice.getInvoiceDetails();

        if (dtInvoice.Rows.Count > 0)
        {
            for (int i = 0; i < dtInvoice.Rows.Count; i++)
            {
                filltable();
                DataRow row;
                row = dttbl.NewRow();
                row["Id"] = i + 1;

                row["ItemCode"] = Convert.ToInt32(dtInvoice.Rows[i]["ItemCode"]);
                row["ItemDesc"] = Convert.ToString(dtInvoice.Rows[i]["ItemDesc"]);
                row["Quantity"] = Convert.ToDecimal(dtInvoice.Rows[i]["Quantity"]);
                row["Rate"] = Convert.ToDecimal(dtInvoice.Rows[i]["Rate"]);
                row["GrossAmount"] = Convert.ToDecimal(dtInvoice.Rows[i]["GrossAmount"]);
                row["SaleTax"] = Convert.ToDecimal(dtInvoice.Rows[i]["SaleTax"]);
                row["SaleTaxAmount"] = Convert.ToDecimal(dtInvoice.Rows[i]["SaleTaxAmount"]);
                row["AmountIncludeTaxes"] = Convert.ToDecimal(dtInvoice.Rows[i]["AmountIncludeTaxes"]);

                //get altercharges, dr, cr
                if (dtInvoice.Rows[i]["AlterCharges"].ToString() != "")
                {
                    row["AlterCharges"] = Convert.ToDecimal(dtInvoice.Rows[i]["AlterCharges"]);
                }

                if (dtInvoice.Rows[i]["DRID"].ToString() != "")
                {
                    row["DRID"] = Convert.ToInt32(dtInvoice.Rows[i]["DRID"]);
                    row["DRTitle"] = Convert.ToString(dtInvoice.Rows[i]["DRTitle"]);
                }


                if (dtInvoice.Rows[i]["CRID"].ToString() != "")
                {
                    row["CRID"] = Convert.ToInt32(dtInvoice.Rows[i]["CRID"]);
                    row["CRTitle"] = Convert.ToString(dtInvoice.Rows[i]["CRTitle"]);
                }
                row["BoutiqueDiscount"] = Convert.ToDecimal(dtInvoice.Rows[i]["BoutiqueDiscount"]);
                row["BoutiqueAmountAfterDis"] = Convert.ToDecimal(dtInvoice.Rows[i]["BoutiqueAmountAfterDis"]);


                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
                ViewState.Add("s", dttbl);
            }
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            dgGallery.Visible = true;

        }
        else
        {
            dgGallery.Visible = false;
        }

    }
    #endregion

    #region Fill item rate
    protected void ddlItemDesc_SelectedIndexChanged(object sender, EventArgs e)
    {

        objCust.ItemId = Convert.ToInt32(ddlItemDesc.SelectedValue);
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        if (ds.Tables[0].Rows.Count == 1)
        {
            txtRate.Text = Convert.ToString(ds.Tables[0].Rows[0]["SaleRate"]);
            //  lblPurchaseRate.Text = "P/P : " + Convert.ToString(ds.Tables[0].Rows[0]["PurchaseRate"].ToString()); 
            txtBarCodeNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["BoutiqueItemCode"]);// ddlItemDesc.SelectedValue;
            lblRateAfterDiscount.Text = Convert.ToString(ds.Tables[0].Rows[0]["SaleRate"]);
        }
        else
        {
            txtRate.Text = "";
            lblPurchaseRate.Text = "";
        }
        objCust.ItemId = Convert.ToInt32(ddlItemDesc.SelectedValue);
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.DataFrom = "";
        DataSet ds1 = new DataSet();
        ds1 = objCust.getPurchasePP();
        if (ds1.Tables[0].Rows.Count > 0)
        {

            lblPurchaseRate.Text = "P/P : " + (Convert.ToDecimal(ds1.Tables[0].Rows[0]["Rate"]) / Convert.ToDecimal(ds1.Tables[0].Rows[0]["BoutiqueConvRate"])).ToString("0.00");
        }

        //  AddDiscount();

        ScriptManager.RegisterStartupScript(this, GetType(), "test", "func('" + txtQuantity.ClientID + "');", true);
    }
    protected void txtBarCodeNo_TextChanged(object sender, EventArgs e)
    {
        objCust.BoutiqueItemCode = txtBarCodeNo.Text;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.getItemsByItemCode();
        if (ds.Tables[0].Rows.Count == 1)
        {
            txtRate.Text = Convert.ToString(ds.Tables[0].Rows[0]["SaleRate"]);
            lblRateAfterDiscount.Text = Convert.ToString(ds.Tables[0].Rows[0]["SaleRate"]);
            //  lblPurchaseRate.Text = "P/P : " + Convert.ToString(ds.Tables[0].Rows[0]["PurchaseRate"].ToString()); ;
            ddlItemDesc.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["ItemId"]);
            objCust.ItemId = Convert.ToInt32(ddlItemDesc.SelectedValue);
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataSet ds1 = new DataSet();
            ds1 = objCust.getPurchasePP();
            if (ds1.Tables[0].Rows.Count > 0)
            {
                lblPurchaseRate.Text = "P/P : " + (Convert.ToDecimal(ds1.Tables[0].Rows[0]["Rate"]) / Convert.ToDecimal(ds1.Tables[0].Rows[0]["BoutiqueConvRate"])).ToString("0.00");
            }
            txtQuantity.Focus();
        }
        else
        {
            txtBarCodeNo.Text = "";
            txtRate.Text = "";
            lblPurchaseRate.Text = "";
            ddlItemDesc.SelectedValue = "0";
            txtBarCodeNo.Focus();
        }



        //AddDiscount();
        ScriptManager.RegisterStartupScript(this, GetType(), "test", "func('" + txtQuantity.ClientID + "');", true);
    }



    #endregion

    #region Cartage
    // Commit Remove by Shoaib Start
    protected void txtDiscount_TextChanged(object sender, EventArgs e)
    {
        if (txtDiscount.Text == "")
        {
            txtDiscount.Text = "0.00";
        }
        lblBillAmount.Text = Convert.ToString(String.Format("{0:C}", (Convert.ToDecimal(txtSaleTax.Text) + Convert.ToDecimal(txtTotalGrossAmount.Text) + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
        txtCashReturned.Text = Convert.ToString(String.Format("{0:C}",Convert.ToDecimal(lblBillAmount.Text) - Convert.ToDecimal(txtCashReceived.Text)).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
    }

    protected void txtCartagecharges_TextChanged(object sender, EventArgs e)
    {
        if (txtCartagecharges.Text == "")
        {
            txtCartagecharges.Text = "0.00";
        }
        lblBillAmount.Text = Convert.ToString(String.Format("{0:C}", (Convert.ToDecimal(txtSaleTax.Text) + Convert.ToDecimal(txtTotalGrossAmount.Text) + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
        txtCashReturned.Text = Convert.ToString(String.Format("{0:C}", Convert.ToDecimal(lblBillAmount.Text) - Convert.ToDecimal(txtCashReceived.Text)).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
    }
    // Commit Remove by Shoaib End
    #endregion

    #region Delete Invoice
    protected void btnDeleteInvoice_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(hdnInvoiceSummaryId.Value) > 0)
        {
            int result = 0;
            objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(hdnInvoiceSummaryId.Value);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.DataFrom = "SALESINV";
            result = objSalesInvoice.deleteAllInvoiceData();
            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record deleted successfully.');location.href='POSSaleInvoice.aspx';", true);
            }
        }

    }
    #endregion

    protected void btnPrintBill_Click(object sender, EventArgs e)
    {
        string urllast = "POSPrintSaleInvoiceBill.aspx?InvoiceSummaryId=" + Convert.ToString(hdnInvoiceSummaryId.Value);
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=750,width=270,scrollbars=1,status=yes' );", true);

    }
    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }

            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
            {
                btnDeleteInvoice.Visible = false;
            }
            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

                btnPrintBill.Visible = false;
            }

            if (Convert.ToString(dt.Rows[0]["IsAdd"]) != "True")
            {
                lblbutton.Visible = false;
                if (Convert.ToString(Request["addnew"]) == "1")
                {

                    ImageButton2.Visible = false;
                }
            }



            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {
                if (Convert.ToString(Request["addnew"]) != "1")
                {

                    ImageButton2.Visible = false;
                }
            }

        }
    }
    protected void lblbutton_Click(object sender, EventArgs e)
    {
        Response.Redirect("POSSaleInvoice.aspx?addnew=1");
    }
    protected void OnCheckChangedCash(object sender, EventArgs e)
    {
        FillCashReceived(1);
        ScriptManager.RegisterStartupScript(this, GetType(), "test", "func('" + ddlCashReceipt.ClientID + "');", true);
    }
    protected void OnCheckChangedCreditCard(object sender, EventArgs e)
    {
        FillCashReceived(2);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.InvoiceSummaryId = 0;
        DataTable dt = new DataTable();
        objSalesInvoice.DataFrom = "SALESINV";
        dt = objSalesInvoice.getInvoiceSummary();
        DataView DvInvoice = dt.DefaultView;
        DvInvoice.RowFilter = "POSSaleType = 'Credit Card'";
        dt = DvInvoice.ToTable();

        if (dt.Rows.Count > 0)
        {
            DataView DvSortInvoice = dt.DefaultView;
            DvSortInvoice.Sort = "InvoiceSummaryId DESC";
            dt = DvSortInvoice.ToTable();
            if (dt.Rows.Count > 0)
            {
                ddlCardChargesaccount.SelectedValue = Convert.ToString(dt.Rows[0]["CardChargesAcfileId"]);
            }
        }
        ScriptManager.RegisterStartupScript(this, GetType(), "test", "func('" + txtCreditCarNo.ClientID + "');", true);
    }
    protected void OnCheckChangedCredit(object sender, EventArgs e)
    {
        FillCashReceived(3);
        ScriptManager.RegisterStartupScript(this, GetType(), "test", "func('" + txtApprovelNO.ClientID + "');", true);

        if (rdbCredit.Checked == true)
        {
            //  txtDescription.Text = "Inv. # : " + txtInvNo.Text + ", Dated : " + ClsGetDate.FillFromDate(txtInvoiceDate.Text) + ", Credit Sale";
            txtDescription.Text = "Credit Sale Inv. # : " + txtInvNo.Text + ", Dated : " + ClsGetDate.FillFromDate(txtInvoiceDate.Text);
        }
        txtDescription.Focus();

    }
    #region Fill  Cash received A/c
    protected void FillCashReceived(int Type)
    {

        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtLedger = new DataTable();
        if (Type == 1)
        {
            dtLedger = objSalesInvoice.getAcFileByCashInHand();
            txtCreditCarNo.Enabled = false;
            txtApprovelNO.Enabled = false;
            txtCashReceived.Enabled = true;
            // ddlCashReceipt.Focus();
            lblCashReceipt.Text = "Cash Receipt A/C ";
            trApproval.Style.Add("display", "none");
            trCashReceipt.Style.Add("display", "");
            trCreditCard.Style.Add("display", "none");
            trCard1.Style.Add("display", "none");
            trCard2.Style.Add("display", "none");

            RequiredFieldValidator7.Enabled = false;
        }
        if (Type == 2)
        {
            dtLedger = objSalesInvoice.getAcFileBySundryDebtorOnly();
            txtCreditCarNo.Enabled = true;
            txtApprovelNO.Enabled = true;
            txtCashReceived.Enabled = true;
            // txtCreditCarNo.Focus();
            lblCashReceipt.Text = "Card A/C ";
            trApproval.Style.Add("display", "");
            trCashReceipt.Style.Add("display", "");
            trCreditCard.Style.Add("display", "");
            trCard1.Style.Add("display", "");
            trCard2.Style.Add("display", "");
            txtCashReceived.Text = lblBillAmount.Text;
            txtCashReturned.Text = "0.00";
            RequiredFieldValidator7.Enabled = true;
        }
        if (Type == 3)
        {
            trApproval.Style.Add("display", "none");
            trCashReceipt.Style.Add("display", "none");
            trCreditCard.Style.Add("display", "none");
            trCard1.Style.Add("display", "none");
            trCard2.Style.Add("display", "none");
            dtLedger = objSalesInvoice.getAcFileBySundryDebtorOnly();
            txtCreditCarNo.Enabled = false;
            txtApprovelNO.Enabled = false;
            txtCashReceived.Text = "0.00";
            txtCashReturned.Text = "0.00";
            txtCashReceived.Enabled = false;
            txtCashReturned.Enabled = false;
            //  ddlCashReceipt.Focus();
            lblCashReceipt.Text = "Credit A/C ";

            RequiredFieldValidator7.Enabled = false;
            RequiredFieldValidator1.Enabled = false;
        }
        // Shoaib Working Start
        if (Type == 4)
        {
            trApproval.Style.Add("display", "none");
            trCashReceipt.Style.Add("display", "none");
            trCreditCard.Style.Add("display", "none");
            trCard1.Style.Add("display", "none");
            trCard2.Style.Add("display", "none");
            dtLedger = objSalesInvoice.getAcFileBySundryDebtorOnly();
            txtCreditCarNo.Enabled = false;
            txtApprovelNO.Enabled = false;
            txtCashReceived.Text = "0.00";
            txtCashReturned.Text = "0.00";
            txtCashReceived.Enabled = false;
            txtCashReturned.Enabled = false;
            //  ddlCashReceipt.Focus();
            lblCashReceipt.Text = "Sahulat Card A/C ";

            RequiredFieldValidator7.Enabled = false;
            RequiredFieldValidator1.Enabled = false;
        }
        // Shoaib Working End

        if (dtLedger.Rows.Count > 0)
        {
            ddlCashReceipt.DataSource = dtLedger;
            ddlCashReceipt.DataTextField = "Title";
            ddlCashReceipt.DataValueField = "Id";
            ddlCashReceipt.DataBind();
            ddlCashReceipt.Items.Insert(0, new ListItem("Select  A/C", "0"));

        }



    }
    #endregion
    protected void ddlCashReceipt_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (rdbCash.Checked == true)
        {
            txtDescription.Text = "Inv. # : " + txtInvNo.Text + ", Dated : " + ClsGetDate.FillFromDate(txtInvoiceDate.Text) + ", Cash Sale";
            txtDescription.Focus();
        }

        if (rdbcashCredit.Checked == true)
        {
            txtDescription.Text = "C.C. Inv. #  : " + txtInvNo.Text + ", Dated : " + ClsGetDate.FillFromDate(txtInvoiceDate.Text) + ", Credit Card Sale. Credit Card # :" + txtCreditCarNo.Text + " Apr # " + txtApprovelNO.Text;
            txtCardCharge.Focus();
        }

        if (rdbCredit.Checked == true)
        {
            txtDescription.Text = "Inv. # : " + txtInvNo.Text + ", Dated : " + ClsGetDate.FillFromDate(txtInvoiceDate.Text) + ", Credit Sale";
            txtDescription.Focus();
        }

        if (rdbARYSahulatCard.Checked == true)
        {
            txtDescription.Text = "Inv. # : " + txtInvNo.Text + ", Dated : " + ClsGetDate.FillFromDate(txtInvoiceDate.Text) + ", Sahulat Card Sale";
            txtDescription.Focus();
        }

    }

    /// <summary>
    /// Bind Dr and cr
    /// </summary>
    #region Fill Accounts

    protected void FillAllLedgerAc()
    {

        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtLedger = new DataTable();
        dtLedger = objSalesInvoice.getAcFileAllLedger();
        if (dtLedger.Rows.Count > 0)
        {

            ddlDR.DataSource = dtLedger;
            ddlDR.DataTextField = "Title";
            ddlDR.DataValueField = "Id";
            ddlDR.DataBind();
            ddlDR.Items.Insert(0, new ListItem("Select", "0"));

            ddlCR.DataSource = dtLedger;
            ddlCR.DataTextField = "Title";
            ddlCR.DataValueField = "Id";
            ddlCR.DataBind();
            ddlCR.Items.Insert(0, new ListItem("Select", "0"));



        }



    }
    #endregion

    # region commented code

    //protected void txtAlterCharges_TextChanged(object sender, EventArgs e)
    //{
    //    if (txtAlterCharges.Text == "" || txtAlterCharges.Text == "0.00" || txtAlterCharges.Text == "0")
    //    {
    //        RequiredFieldValidator10.Enabled = false;
    //        RequiredFieldValidator11.Enabled = false;
    //        ddlDR.SelectedValue = "0";
    //        ddlCR.SelectedValue = "0";
    //    }
    //    else
    //    {
    //        RequiredFieldValidator10.Enabled = true;
    //        RequiredFieldValidator11.Enabled = true;

    //    }
    //}


    # endregion

    protected void txtContactNumber_OnChanged(object sender, EventArgs e)
    {
        if (txtContactNumber.Text != "")
        {
            //objSalesInvoice.ContactNumber = txtContactNumber.Text;                                          // Comit By Shoaib
            objSalesInvoice.ContactNumber = System.String.Concat(prefixNumber, txtContactNumber.Text);        // Shoaib Working
            DataTable dt = new DataTable();

            dt = objSalesInvoice.getCustomerSummeryByContactNumber();
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["CustomerName"].ToString() != "-")
                {
                    txtCustomerName.Text = Convert.ToString(dt.Rows[0]["CustomerName"]);
                }
                else
                {
                    txtCustomerName.Text = "";
                }
                if (dt.Rows[0]["ContactNumber"].ToString() != "-")
                {
                    //txtContactNumber.Text = Convert.ToString(dt.Rows[0]["ContactNumber"]);     // Comit by Shoaib 

                    // Shoaib Working Start
                    string contact = Convert.ToString(dt.Rows[0]["ContactNumber"]);
                    txtContactNumber.Text = contact.StartsWith("00971") ?
                       contact.Substring(contact.IndexOf(contact) + prefixNumber.Length, contact.Length - prefixNumber.Length) : contact;
                    // Shoaib Working End                
                }
                else
                {
                    txtContactNumber.Text = "";
                }
                if (dt.Rows[0]["EmailAddress"].ToString() != "-")
                {
                    txtEmailAddress.Text = Convert.ToString(dt.Rows[0]["EmailAddress"]);
                }
                else
                {
                    txtEmailAddress.Text = "";
                }
            }
            else
            {
                txtCustomerName.Text = "";
                // txtContactNumber.Text = "";
                txtEmailAddress.Text = "";
                txtCustomerName.Focus();
            }
            mpeAddDetails.Show();
            // AjaxControlToolkit.ModalPopupExtender mpepopup = (AjaxControlToolkit.ModalPopupExtender)rptReseller.Items[rpitem.ItemIndex].FindControl("mpeAddDoc");

        }


    }

    protected void txtRate_changed(object sender, EventArgs e)
    {

        AddDiscount();

        txtBoutiqueDiscount.Focus();
    }
    protected void txtBoutiqueDiscount_changed(object sender, EventArgs e)
    {

        AddDiscount();

        lblRateAfterDiscount.Focus();
    }
    protected void AddDiscount()
    {
        if (txtRate.Text != "")
        {
            if (Convert.ToDecimal(txtRate.Text) > 0)
            {
                lblRateAfterDiscount.Text = (Convert.ToDecimal(txtRate.Text) - (Convert.ToDecimal(txtRate.Text) * (Convert.ToDecimal(txtBoutiqueDiscount.Text) / 100))).ToString("0.00");
                txtAlterCharges.Focus();

            }

            else
            {
                lblRateAfterDiscount.Text = (Convert.ToDecimal(txtRate.Text)).ToString("0.00");
            }
        }
    }
    protected void txtInvNo_TextChanged(object sender, EventArgs e)
    {


        if (Convert.ToString(hdnInvoiceSummaryId.Value) != "")
        {
            txtInvoiceDate.Focus();
        }
        else
        {
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.InvoiceSummaryId = 0;
            objSalesInvoice.DataFrom = "SALESINV";
            DataTable dtInvoiceNewDetails = new DataTable();
            dtInvoiceNewDetails = objSalesInvoice.getBoutiqueInvoiceSummary();
            if (dtInvoiceNewDetails.Rows.Count > 0)
            {

                DataTable dt = new DataTable();
                DataView dv = new DataView(dtInvoiceNewDetails);
                dv.RowFilter = "InvNo='" + Convert.ToString(txtInvNo.Text) + "'";
                dt = dv.ToTable();
                if (dt.Rows.Count > 0)
                {
                    int Last = Convert.ToInt32(dtInvoiceNewDetails.Rows.Count) - 1;
                    txtInvNo.Text = Convert.ToString(Convert.ToInt32(dtInvoiceNewDetails.Rows[Last]["InvNo"]) + 1);
                }

            }
            txtInvNo.Focus();
        }

      
       

    }

    protected void hdnInvoiceSummaryId_ValueChanged(object sender, EventArgs e)
    {

    }
    protected void txtInvoiceDate_TextChanged(object sender, EventArgs e)
    {

    }
    protected void ddlDR_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    // Shoaib Working Start
    protected void rdbARYSahulatCard_CheckedChanged(object sender, EventArgs e)
    {
        FillCashReceived(4);
        if (rdbARYSahulatCard.Checked == true)
        {
            txtDescription.Text = "Sahulat Card Sale Inv. # : " + txtInvNo.Text + ", Dated : " + ClsGetDate.FillFromDate(txtInvoiceDate.Text);
        }
        txtDescription.Focus();
    }
    // Shoaib Working End  
    
    // Shoaib Working Start
    protected bool ARYSahulatCardInvoice()
    {
        if (rdbARYSahulatCard.Checked == true) //ARY sahulat Card
        {
            List<UAEOrderService2.OrderDetails> orderDetails = new List<UAEOrderService2.OrderDetails>();
            string categoryConcat = System.String.Concat(vender_id, branch_id, category_Name, category_Code);
            string categoryEncrypt = Encrypt(categoryConcat, true);          // Encrpty Static Method Call                               
            string categoryResponse = uAEClient.SetCategory(vender_id, branch_id,
                category_Name, category_Code, Convert.ToDecimal(margin), categoryEncrypt);

            if (categoryResponse.Contains("000") || categoryResponse.Contains("201") || categoryResponse.Contains("202"))
            {
                DataTable dtSahulat = new DataTable();
                dtSahulat = (DataTable)ViewState["s"];
                if (dtSahulat.Rows.Count > 0)
                {
                    for (int i = 0; i < dtSahulat.Rows.Count; i++)
                    {
                        string productConcat = System.String.Concat(vender_id, branch_id, dtSahulat.Rows[i]["ItemDesc"].ToString(), category_Code);
                        string productEncrypt = Encrypt(productConcat, true);
                        string productResponse = uAEClient.SetProduct(vender_id, branch_id, dtSahulat.Rows[i]["ItemDesc"].ToString(), category_Code, Convert.ToDecimal(margin), productEncrypt);
                        if (productResponse.Contains("000") || productResponse.Contains("201") || productResponse.Contains("202"))
                        {
                            UAEOrderService2.OrderDetails order = new UAEOrderService2.OrderDetails();
                            order.Category_Code = category_Code;
                            order.Product_Name = dtSahulat.Rows[i]["ItemDesc"].ToString().ToString();
                            order.Quantity = (int)Math.Round(Convert.ToDouble(dtSahulat.Rows[i]["Quantity"].ToString()), 0);
                            order.UnitPrice = Convert.ToDecimal(dtSahulat.Rows[i]["Rate"].ToString());
                            order.ServiceCharges = Convert.ToDecimal(dtSahulat.Rows[i]["AlterCharges"].ToString());
                            orderDetails.Add(order);
                        }
                        else if (productResponse.Contains("101"))
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Invalid vendor id.');", true);
                            return false;
                        }
                        else if (productResponse.Contains("102"))
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Invalid branch id.');", true);
                            return false;
                        }
                        else if (productResponse.Contains("103"))
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Vendor and Branch mismatch.');", true);
                            return false;
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Unauthorized access (Encryption mismatch).');", true);
                            return false;
                        }
                    }
                }

                if (orderDetails.Count > 0)
                {
                    string cellNo = System.String.Concat(prefixNumber, txtContactNumber.Text);
                    string orderConcat = System.String.Concat(vender_id, branch_id, terminal_Code, cellNo, txtCustomerName.Text, city);
                    string orderEncrypt = Encrypt(orderConcat, true);          // Encrpty Static Method Call                               
                    string orderResponse = uAEClient.SetOrder(vender_id, branch_id,
                        terminal_Code, cellNo, txtCustomerName.Text, city, orderDetails.ToArray(), orderEncrypt);

                    if (orderResponse.Contains("000"))
                    {
                        //ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Order Succefuly Register.');", true);
                        return true;
                    }
                    else if (orderResponse.Contains("101"))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Invalid vendor id.');", true);
                        return false;
                    }
                    else if (orderResponse.Contains("102"))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Invalid branch id.');", true);
                        return false;
                    }
                    else if (orderResponse.Contains("103"))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Vendor and Branch mismatch.');", true);
                        return false;
                    }
                    else if (orderResponse.Contains("201"))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Category ID not found.');", true);
                        return false;
                    }
                    else if (orderResponse.Contains("202"))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Product ID not found.');", true);
                        return false;
                    }
                    else if (orderResponse.Contains("300"))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Device not registered.');", true);
                        return false;
                    }
                    else if (orderResponse.Contains("301"))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Device inactive.');", true);
                    }
                    else if (orderResponse.Contains("400"))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Incorrect cell number format.');", true);
                        return false;
                    }
                    else if (orderResponse.Contains("600"))
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Order Detail are empty.');", true);
                        return false;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Unauthorized access (Encryption mismatch).');", true);
                        return false;
                    }
                }
            }
            else if(categoryResponse.Contains("101"))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Invalid vendor id.');", true);
                return false;
            }
            else if (categoryResponse.Contains("102"))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Invalid branch id.');", true);
                return false;
            }
            else if (categoryResponse.Contains("103"))
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Vendor and Branch mismatch.');", true);
                return false;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Unauthorized access (Encryption mismatch).');", true);
                return false;
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert(' Please Select ARY Sahulat Card Option.');", true);
            return false;
        }
        return true;
    }
    // Shoaib Working End 
}