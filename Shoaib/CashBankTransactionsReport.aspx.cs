﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class CashBankTransactionsReport : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        if (!IsPostBack)
        {
            txtInvoiceDateFrom.Focus();
            FillClientAC();

            txtInvoiceDateFrom.Text = Convert.ToDateTime(Sessions.FromDate).ToString("dd MMM yyyy");
            txtInvoiceDateTo.Text = System.DateTime.Now.ToString("dd MMM yyyy");

        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        objSalesInvoice.CashBookDateTo = Convert.ToDateTime(txtInvoiceDateTo.Text);
        objSalesInvoice.CashBookDateFrom = Convert.ToDateTime(txtInvoiceDateFrom.Text);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.AcFileId = Convert.ToInt32(ddlClientAC.SelectedValue);
        objSalesInvoice.DataFrom = "CashBook";  

        DataTable dt = new DataTable();
        dt = objSalesInvoice.SearchCashbook();
        dgGallery.DataSource = dt;
        dgGallery.DataBind();




    }
    #region Fill  Client A/C
    protected void FillClientAC()
    {
     
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtClient = new DataTable();
            dtClient = objSalesInvoice.getAcFileByCashBankAccount();
            if (dtClient.Rows.Count > 0)
            {
                ddlClientAC.DataSource = dtClient;
                ddlClientAC.DataTextField = "Title";
                ddlClientAC.DataValueField = "Id";
                ddlClientAC.DataBind();
                ddlClientAC.Items.Insert(0, new ListItem("Select Cash/Bank A/c", "0"));

            }
      

    }
    #endregion

    protected void dgGallery_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (DataBinder.Eval(e.Row.DataItem, "CashBookDate").ToString() != "")
            {
                Label lblInvDate = (Label)e.Row.FindControl("lblinvDate");
                lblInvDate.Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Row.DataItem, "CashBookDate").ToString());
            }

            if (Sessions.UserRole.ToString() != "Admin")
            {


                Button btnEdit = (Button)e.Row.FindControl("btnEdit");

                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
                if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
                {
                    Response.Redirect(Request.UrlReferrer.ToString());
                }

                if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
                {

                   // btnEdit.Visible = false;

                }

            }
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            objSalesInvoice.CashBookDateTo = Convert.ToDateTime(txtInvoiceDateTo.Text);
            objSalesInvoice.CashBookDateFrom = Convert.ToDateTime(txtInvoiceDateFrom.Text);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.AcFileId = Convert.ToInt32(ddlClientAC.SelectedValue);
            objSalesInvoice.DataFrom = "CashBook";       
            DataTable dt = new DataTable();
            dt = objSalesInvoice.SearchCashbook();

            Label lblTotalTotalReceive = (Label)e.Row.FindControl("lblTotalTotalReceive");
            decimal TotalReceive = (decimal)dt.Compute("SUM(TotalReceive)", "");
            lblTotalTotalReceive.Text = String.Format("{0:C}", TotalReceive).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblTotalPayment = (Label)e.Row.FindControl("lblTotalPayment");
            decimal TotalPayment = (decimal)dt.Compute("SUM(TotalPayment)", "");
            lblTotalPayment.Text = String.Format("{0:C}", TotalPayment).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            //Label lblTotalAmountIncludeTax = (Label)e.Row.FindControl("lblTotalAmountIncludeTax");
            //decimal TotalAmountIncludeTax = (decimal)dt.Compute("SUM(TotalAmountIncludeTax)", "");
            //lblTotalAmountIncludeTax.Text = String.Format("{0:C}", TotalAmountIncludeTax).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            //Label lblBillAmount = (Label)e.Row.FindControl("lblBillAmount");
            //decimal BillAmount = (decimal)dt.Compute("SUM(BillAmount)", "");
            //lblBillAmount.Text = String.Format("{0:C}", BillAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            //Label lblCartageAmount = (Label)e.Row.FindControl("lblCartageAmount");
            //decimal CartageAmount = (decimal)dt.Compute("SUM(CartageAmount)", "");
            //lblCartageAmount.Text = String.Format("{0:C}", CartageAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            //Label lblInvoiceDiscount = (Label)e.Row.FindControl("lblInvoiceDiscount");
            //decimal InvoiceDiscount = (decimal)dt.Compute("SUM(InvoiceDiscount)", "");
            //lblInvoiceDiscount.Text = String.Format("{0:C}", InvoiceDiscount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


        }

    }

    protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditCustomer")
        {
            Response.Redirect("CashBankTransactions.aspx?InvoiceSummaryId=" + e.CommandArgument.ToString());
        }

    } 
}
