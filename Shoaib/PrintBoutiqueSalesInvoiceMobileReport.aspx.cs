﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintBoutiqueSalesInvoiceMobileReport : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    clsCookie ck = new clsCookie();
    decimal TotalOpbal = 0;
    decimal TotalTransections = 0;
    decimal TotalOpeningQuantity = 0;
    decimal TotalPurchaseRate = 0;
    decimal TotalProduction = 0;
    decimal TotalSalesReturn = 0;
    decimal TotalPurchaseRateAdjustment = 0;
    decimal AllTotalStockReceived = 0;
    decimal TotalSales = 0;
    decimal TotalConsumption = 0;
    decimal TotalPurchaseRateReturn = 0;
    decimal TotalSalesAdjustment = 0;
    decimal AllTotalStockSale = 0;
    decimal TotalClosingStock = 0;
    decimal TotalAmount = 0;

    decimal AllTotalOpbal = 0;
    decimal AllTotalTransections = 0;
    decimal AllTotalOpeningQuantity = 0;
    decimal AllTotalPurchaseRate = 0;
    decimal AllTotalProduction = 0;
    decimal AllTotalSalesReturn = 0;
    decimal AllTotalPurchaseRateAdjustment = 0;
    decimal CompleteTotalStockReceived = 0;
    decimal AllTotalSales = 0;
    decimal AllTotalConsumption = 0;
    decimal AllTotalPurchaseRateReturn = 0;
    decimal AllTotalSalesAdjustment = 0;
    decimal CompleteTotalStockSale = 0;
    decimal AllTotalClosingStock = 0;
    decimal AllTotalAmount = 0;

    decimal GroupTotalOpeningQuantity = 0;
    decimal GroupTotalPurchaseRate = 0;
    decimal GroupTotalAmount = 0;
    Reports objReports = new Reports();
    SetCustomers objCust = new SetCustomers();
    DataTable dttbl = new DataTable();
    Car objCar = new Car();
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {
            
                Session["s"] = null;
                bindSupplier();
               BindBasicSearch();
                FillLogo();
                lblFromDate.Text = ClsGetDate.FillFromDate(Request["From"].ToString());
                lblToDate.Text = ClsGetDate.FillFromDate(Request["To"].ToString());
                lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
                lblUser.Text = Sessions.UseLoginName.ToString();

              

           
        }
    }

    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }

        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }

    }
    #endregion

    protected void dgGallery_OnItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            
            TotalAmount += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "totalBillamount"));

        }
        if (e.Item.ItemType == ListItemType.Footer)
        {
          
            ((Label)e.Item.FindControl("lblTotalAmountSum")).Text = TotalAmount.ToString("0.00");
        }

    }


    protected void BindBasicSearch()
    {
     
        objCar.InvDateFrom = Convert.ToDateTime(Request["From"]);
        objCar.InvDateTo = Convert.ToDateTime(Request["To"]);
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        ds = objCar.getSaleInvoiceByMobile();

        if (ds.Tables[0].Rows.Count > 0)
        {
            dgGallery.DataSource = ds.Tables[0];
            dgGallery.DataBind();

        }
    }
    protected void lnkExport_Click(object sender, EventArgs e)
    {
        System.IO.StringWriter tw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
       DataGrid dgGrid = new DataGrid();
        DataTable dt = new DataTable();
        objCar.InvDateFrom = Convert.ToDateTime(Request["From"]);
        objCar.InvDateTo = Convert.ToDateTime(Request["To"]);        
        DataSet ds = new DataSet();
        ds = objCar.getSaleInvoiceByMobile();
        dt = ds.Tables[0];
        dgGrid.DataSource = dt;
        dgGrid.HeaderStyle.Font.Bold = true;
        dgGrid.DataBind();
        dgGrid.RenderControl(hw);
        string attachment = null;
        attachment = "attachment; filename=SalesMobileReport.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/vnd.ms-excel";
        this.EnableViewState = false;
        Response.Write(tw.ToString());
        Response.End();

    }
}