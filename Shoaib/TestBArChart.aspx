﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestBArChart.aspx.cs" Inherits="TestBArChart" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Chart ID="Chart1" runat="server" BorderlineWidth="0" Height="500px" Width="60px">
            <Legends>
                <asp:Legend BackColor="DarkMagenta">
              
                </asp:Legend>
            </Legends>
            <Titles>
                <asp:Title Name="Naveen" Text="Sales V/s Recovery" TextStyle="Default" ForeColor="Green">
                </asp:Title>
            </Titles>
            <Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" BorderColor="White" BackColor="Green">
                    <AxisX Title="Months" IsLabelAutoFit="False" Minimum="0" Interval="1" Maximum="13">
                        <LabelStyle Format="MMM" />
                    </AxisX>
                    <AxisY Title="Sales V/s Recovery" IsLabelAutoFit="False" Minimum="0" Maximum="15"
                        Interval="1" IsStartedFromZero="False">
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>
    </form>
</body>
</html>
