﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="BoutiqueOrderSummaryReport.aspx.cs" Inherits="BoutiqueOrderSummaryReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Boutique Order Report"></asp:Label>
    <script type="text/javascript">

        function Count(text, long) {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }
          
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnInvoiceId" runat="server" />
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Order No :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:TextBox ID="txtInvNo" CssClass="input_box33" Width="100px" Style="float: left;
                            width: 264px; text-transform: uppercase;" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Cost Center :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:DropDownList ID="ddlCostCenter" runat="server" Style="width: 270px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Client Name :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:DropDownList ID="ddlClientAC" runat="server" Style="width: 270px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Status:
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        <asp:DropDownList ID="ddlStatus" runat="server">
                        <asp:ListItem Text="All" Value="All"></asp:ListItem>
                            <asp:ListItem Text="Open" Value="Open"></asp:ListItem>
                            <asp:ListItem Text="Closed" Value="Closed"></asp:ListItem>
                            <asp:ListItem Text="Cancel" Value="Cancel"></asp:ListItem>
                        </asp:DropDownList>
                       
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Date From:
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:TextBox ID="txtInvoiceDateFrom" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateFrom'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateFrom'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill Invoice Date." Text="*" ControlToValidate="txtInvoiceDateFrom"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtInvoiceDateFrom" ValidationGroup="AddInvoice" ErrorMessage="Invalid Invoice Date !!"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Date To :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:TextBox ID="txtInvoiceDateTo" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateTo'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateTo'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill Invoice Date." Text="*" ControlToValidate="txtInvoiceDateTo"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtInvoiceDateTo" ValidationGroup="AddInvoice" ErrorMessage="Invalid Invoice Date !!"
                            Display="None"></asp:RegularExpressionValidator>
                        <span style="float: right; padding-right: 65px">
                            <asp:Button ID="btnsearch" Text="Search" runat="server" OnClick="btnsearch_Click" /></span>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" style="float: left; padding: 0 10px 10px 10px;" border="0"
                cellpadding="0" width="90%">
                <tr>
                    <td colspan="3">
                        <asp:GridView ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                            AllowPaging="true" PageSize="500000" PagerSettings-Visible="false" PagerSettings-Mode="Numeric"
                            HeaderStyle-CssClass="gridheader" AlternatingRowStyle-CssClass="gridAlternateItem"
                            OnRowDataBound="dgGallery_RowDataBound" GridLines="none" BorderColor="black"
                            OnRowCommand="grdCustomer_RowCommand" FooterStyle-CssClass="gridFooter" BorderStyle="Dotted"
                            BorderWidth="1" RowStyle-CssClass="gridItem" ShowFooter="true">
                            <EmptyDataTemplate>
                                <div style="padding-bottom: 10px; width: 100%; text-align: center; font-family: Gisha;
                                    font-size: 20px; background-color: #E1F5FD; color: #110F0F;">
                                    <div style="padding-top: 10px; padding-bottom: 5px;">
                                        No record found
                                    </div>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order No">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="8%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Eval("InvNo")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Client Name">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Eval("Title")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cost Center">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Eval("CostCenterName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order Date">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblinvDate" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Gross Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("TotalGrossAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalGrossAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <%-- <asp:TemplateField HeaderText="Sale Tax Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("TotalSaleTaxAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalSaleTaxAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amount Including Tax">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("TotalAmountIncludeTax")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalAmountIncludeTax" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField> --%>
                                <asp:TemplateField HeaderText="Discount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("InvoiceDiscount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblInvoiceDiscount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cartage">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("CartageAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblCartageAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Order Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("BillAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblBillAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Eval("OrderStatus")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Edit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%--  <a href='SalesInvoice.aspx?InvoiceSummaryId=<%#Eval("InvoiceSummaryId") %>'>
                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" /></a>--%>
                                        <asp:Button ID="btnEdit" CommandName="EditCustomer" CommandArgument='<%#Eval("InvoiceSummaryId") %>'
                                            runat="server" Text="Edit" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
