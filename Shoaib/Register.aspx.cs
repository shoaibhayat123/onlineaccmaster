﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class Register : System.Web.UI.Page
{
    SetCustomers objcustomer = new SetCustomers();
    User objUser = new User();
    Car objCar = new Car();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillIndustry();
            FillAddress();
        }
    }


    protected void FillIndustry()
    {
        DataSet ds = objcustomer.GetIndustrialTypeActive();
        ddlIndustry.DataTextField = "IndustrialName";
        ddlIndustry.DataValueField = "IndId";
        ddlIndustry.DataSource = ds.Tables[0];
        ddlIndustry.DataBind();
        ddlIndustry.Items.Insert(0, new ListItem("--Select Business Type--", "0"));
    }





    protected void SendMail()
    {
        string[] strArray = new string[1];
        strArray[0] = txtCustomerName.Text.Trim();

        EmailSender objMail = new EmailSender();
        objMail.ValueArray = strArray;
        objMail.MailTo = txtPrimaryEmail.Text;
        objMail.Subject = "Enquiry acknowledgement";
        objMail.MailBcc = "support@accmaster.com";
        objMail.TemplateDoc = "Enquiry.htm";
        objMail.Send();

    }
    protected void btnAdd_Click1(object sender, EventArgs e)
    {

        ccJoin.ValidateCaptcha(txtCap.Text);
        if (!ccJoin.UserValidated)
        {
            Page.RegisterStartupScript("Msg1", "<script>alert('Enter the letters as they are shown in the image below !!!');</script>");
            //Page.RegisterStartupScript("Msg1", "<script>alert('Enter the letters as they are shown in the image below !!!');</script>");
            divCommentMsg.Visible = true;

        }
        else
        {
            if (chkTermAccept.Checked)
            {

                if ((Convert.ToString(Request.QueryString["UserId"]) != "") && (Request.QueryString["UserId"] != null))
                {
                    objUser.UserId = Convert.ToInt32(Request["UserId"]);
                }
                else
                {
                    objUser.UserId = 0;
                }
                objUser.CustomerCode = 786001;
                objUser.CustomerName = txtCustomerName.Text;
                objUser.ContactPerson = txtContactPerson.Text;
                objUser.AddressLine1 = txtAddress1.Text;
                objUser.AddressLine2 = txtAddress2.Text;
                objUser.AddressLine3 = txtAddress3.Text;
                objUser.City = txtCity.Text;
                objUser.Country = txtCountry.Text;
                objUser.PhoneNumber = txtPhoneNumber.Text;
                objUser.CellNumber = txtCellNumber.Text;
                objUser.PrimaryEmail = txtPrimaryEmail.Text;
                objUser.SecondryEmail = txtSecondryEmail.Text;
                objUser.NumberOfUsers = Convert.ToDecimal(ddlNoCustomer.SelectedValue.ToString());
                objUser.IndId = Convert.ToInt32(ddlIndustry.SelectedValue);
                objUser.PanNo = txtMtno.Text;
                objUser.SalesTaxNo = txtstRegNo.Text;
                objUser.IsActiveStatus = false;
                objUser.ClientIp = Request.UserHostAddress.ToString();
                DataTable dt = objUser.AddEditEnquiryInfo();
                if ((Convert.ToString(Request.QueryString["UserId"]) != "") && (Request.QueryString["UserId"] != null))
                {

                    Page.RegisterStartupScript("msg", "<script> alert('Customer detail has been Updated Successfully'); location.href='default.aspx'  </script>");
                }
                else
                {
                    //if (dt.Rows.Count > 0)
                    //{
                    //    SendMail(Convert.ToDecimal(dt.Rows[0]["Customercode"]));
                    //}

                    Page.RegisterStartupScript("msg", "<script> alert('Customer detail has been Submitted Successfully'); location.href='default.aspx'  </script>");

                }
            }

            else
            {
                Page.RegisterStartupScript("msg", "<script> alert('First accept Terms of Service And Privacy Policy');   </script>");
            }
            SendMail();
        }
    }
    protected void FillAddress()
    {
        objCar.id = 1;
        DataTable dt = objCar.getAccAddress();
        if (dt.Rows.Count > 0)
        {
            ltrlAddress.Text = dt.Rows[0]["Address"].ToString();
        }
    }
}