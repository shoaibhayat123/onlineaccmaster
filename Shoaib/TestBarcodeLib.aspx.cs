﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BarcodeLib;

public partial class TestBarcodeLib : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Barcode barcode = new Barcode();
        BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39;
        BarcodeLib.SaveTypes y = SaveTypes.JPG;
        barcode.Encode(type, "944");
        barcode.SaveImage(Server.MapPath("~/images/Barcode\\123.jpg"), y);
        imgBarcode.ImageUrl = "~/images/Barcode/123.jpg";
       

    }
    protected void Page_LoadComplete(object sender, EventArgs e)
    {
      
    }
}