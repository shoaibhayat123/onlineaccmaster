﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class BoutiqueSalesInvoiceMobileReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtInvoiceDateFrom.Text = Convert.ToDateTime(Sessions.FromDate).ToString("dd MMM yyyy");
            txtInvoiceDateTo.Text = System.DateTime.Now.ToString("dd MMM yyyy");
        }
    }
    #region btnGenrete
    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        string urllast = "PrintBoutiqueSalesInvoiceMobileReport.aspx?From=" + txtInvoiceDateFrom.Text + "&To=" + txtInvoiceDateTo.Text;
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open('" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);
    }
    #endregion
}