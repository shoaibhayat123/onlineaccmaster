﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintHambalProductPricelist.aspx.cs"
    Inherits="PrintHambalProductPricelist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .Level1
        {
            margin: 5px 0 0 30px;
            font-weight: bold;
            color: Green;
        }
        .Level2
        {
            margin: 5px 0 0 60px;
            font-weight: bold;
            color: Green;
        }
        .Level3
        {
            margin: 5px 0 0 90px;
            font-weight: bold;
            color: Green;
        }
        .Level4
        {
            margin: 5px 0 0 120px;
            font-weight: bold;
            color: Green;
        }
        .Level5
        {
            margin: 5px 0 0 150px;
            font-weight: bold;
            color: Green;
        }
        .Level6
        {
            margin: 5px 0 0 180px;
            font-weight: bold;
            color: Green;
        }
        .Ledger
        {
            margin: 5px 0 0 60px;
        }
    </style>
</head>
<body style="font-family: Tahoma; background: none; font-size: 12px">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; font-weight: bold;">
            <tr id="trTrialBal" runat="server">
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Product
                        Price List<br />
                        <asp:Label ID="lblDate" runat="server"></asp:Label>
                    </span>
                    <asp:Label ID="lblBuyerName" runat="server"></asp:Label><br />
                    <span style="font-size: 12px; font-weight: normal">
                        <asp:Label ID="lblBuyerAddress" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress2" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress3" runat="server"></asp:Label></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <hr />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 755px; float: left">
        <asp:GridView ID="grdMainCategory" AutoGenerateColumns="false" GridLines="None" BorderWidth="0"
            ShowHeader="false" ShowFooter="false" CellPadding="0" CellSpacing="0" runat="server"
            OnRowDataBound="grdMainCategory_RowDataBound">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <div class="Level1">
                            <asp:Label ID="lblTitle" Text='<%#Eval("MainCategoryName")%>' Style="text-decoration: underline;
                                font-size: 16px" runat="server"></asp:Label>
                            <asp:HiddenField Value='<%#Eval("MainCategoryCode")%>' ID="hdnMainCategoryCode" runat="server" />
                        </div>
                        <asp:GridView ID="grdSubCategory" AutoGenerateColumns="false" ShowHeader="false"
                            ShowFooter="false" GridLines="None" runat="server" OnRowDataBound="grdSubCategory_RowDataBound">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div class="Level2">
                                            <asp:Label ID="Label1" Text='<%#Eval("SubcategoryName")%>' Style="text-decoration: underline;
                                                font-size: 16px" runat="server"></asp:Label>
                                            <asp:HiddenField Value='<%#Eval("SubCategoryCode")%>' ID="hdnSubCategoryCode" runat="server" />
                                            <asp:HiddenField Value='<%#Eval("MainCategoryCode")%>' ID="hdnMainCategoryCode2"
                                                runat="server" />
                                        </div>
                                        <div class="Ledger">
                                            <asp:GridView ID="grdLedger" Width="700px" runat="server" ShowFooter="False" AutoGenerateColumns="False"
                                                AllowPaging="true" PageSize="500000" PagerSettings-Visible="false" PagerSettings-Mode="Numeric"
                                                HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                                                FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                                                FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                                                BorderColor="black" OnRowDataBound="grdLedger_RowDataBound">
                                                <EmptyDataTemplate>
                                                    <div style="padding-bottom: 10px; width: 100%; text-align: center; font-family: Gisha;
                                                        font-size: 20px; background-color: #E1F5FD; color: #110F0F;">
                                                        <div style="padding-top: 10px; padding-bottom: 5px;">
                                                            No record found
                                                        </div>
                                                    </div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Barcode">
                                                        <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                                        <ItemStyle HorizontalAlign="Center" Width="15%" CssClass=" gridpadding" />
                                                        <ItemTemplate>
                                                            <%#Eval("BarcodeNo")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="SKU NO">
                                                        <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                                        <ItemStyle HorizontalAlign="Center" Width="15%" CssClass=" gridpadding" />
                                                        <ItemTemplate>
                                                            <%#Eval("BoutiqueItemCode")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                                        <ItemStyle HorizontalAlign="Left" Width="55%" CssClass=" gridpadding" />
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <ItemTemplate>
                                                            <%#Eval("ItemDesc")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Sale Price">
                                                        <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                                        <ItemStyle HorizontalAlign="Right" Width="15%" CssClass=" gridpadding" />
                                                        <ItemTemplate>
                                                            <%#Eval("price")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>
                                <script type="text/javascript">

                                    var currentTime = new Date()
                                    var hours = currentTime.getHours()
                                    var Newhours = 0;
                                    var minutes = currentTime.getMinutes()
                                    if (minutes < 10) {
                                        minutes = "0" + minutes
                                    }
                                    if (hours > 12) {
                                        Newhours = hours - 12;
                                    }
                                    else {
                                        Newhours = hours;
                                    }
                                    document.write(Newhours + ":" + minutes + " ")
                                    if (hours > 11) {
                                        document.write("PM")
                                    } else {
                                        document.write("AM")
                                    }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;">
                                <asp:LinkButton Text="Print" ID="lnkPrint" OnClientClick="window.print();" runat="server"></asp:LinkButton>
                                <a href="SendEmail.aspx?page=SendEmail.aspx?page" style="margin-left: 15px" title="AccMaster"
                                    rel="gb_page_center[580, 440]">Email</a>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
