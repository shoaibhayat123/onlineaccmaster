﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintAgingReport : System.Web.UI.Page
{
    Reports objreport = new Reports();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Sessions.CustomerCode == "") || (Request["AsOn"].ToString() == "") || (Request["Acfile"].ToString() == ""))
        {
            Response.Redirect("default.aspx");
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {
            bindSupplier();
            lblFromDate.Text = ClsGetDate.FillFromDate(Request["AsOn"].ToString());
            FillLogo();
            BindAgingResult();
        }
    }

    protected void BindAgingResult()
    {
        objreport.AcFileId = Convert.ToInt32(Request["Acfile"]);
        objreport.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtLedger = new DataTable();
        dtLedger = objreport.getChildLedger();
        DataTable dtAgentReport = new DataTable();
        if (dtLedger.Rows.Count > 0)
        {
            for (int i = 0; i < dtLedger.Rows.Count; i++)
            {
                objreport.AcFileId = Convert.ToInt32(dtLedger.Rows[i]["id"]);
                objreport.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
                objreport.EndDate = Convert.ToDateTime(Request["AsOn"]);
                DataTable dtRusult = new DataTable();
                dtRusult = objreport.getAgingReport();
                if (i == 0)
                {
                    dtAgentReport = dtRusult.Copy();
                }
                else
                {
                    dtAgentReport.Merge(dtRusult);
                }
            }
        }
        if(dtAgentReport.Rows.Count>0)
        {
            grdLedger.DataSource = dtAgentReport;
            grdLedger.DataBind();
        }
            
    }
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }


    }
    #endregion
}
