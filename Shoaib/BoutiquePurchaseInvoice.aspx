﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="BoutiquePurchaseInvoice.aspx.cs" Inherits="BoutiquePurchaseInvoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Purchase Invoice"></asp:Label>
    <script type="text/javascript">

        function Count(text, long) {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }
        function ChangeCartage(obj) {
            if (obj.value != null && !obj.value.toString().match(/^[-]?\d*\.?\d*$/) || obj.value == "") {
                obj.value = "0.00";
                var BillAmount = parseFloat(replaceAll(document.getElementById('<%=txtTotalGrossAmount.ClientID%>').innerHTML, ',', '')) + parseFloat(replaceAll(document.getElementById('<%=txtCartagecharges.ClientID%>').value, ',', '')) - parseFloat(replaceAll(document.getElementById('<%=txtDiscount.ClientID%>').value, ',', ''));
                document.getElementById('<%=lblBillAmount.ClientID%>').value = formatCurrency(BillAmount);
                alert("Please Enter Numeric Value");
            }
            else {
                if (obj.value < 0) {
                    obj.value = "0.00";
                    alert("Please Enter Valid Value");
                }
                var BillAmount = parseFloat(replaceAll(document.getElementById('<%=txtTotalGrossAmount.ClientID%>').innerHTML, ',', '')) + parseFloat(replaceAll(document.getElementById('<%=txtCartagecharges.ClientID%>').value, ',', '')) - parseFloat(replaceAll(document.getElementById('<%=txtDiscount.ClientID%>').value, ',', ''));
                document.getElementById('<%=lblBillAmount.ClientID%>').value = formatCurrency(BillAmount);



            }
        }

        function replaceAll(txt, replace, with_this) {
            return txt.replace(new RegExp(replace, 'g'), with_this);
        }
        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                        num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }

        function ChangeConvRate() {

        
            var Rate = document.getElementById('<%=txtRate.ClientID %>').value;
          
            var SalesDiscount = document.getElementById('<%=hdnBoutiqueSalesDiscount.ClientID %>').value;
          
            var Convrate = document.getElementById('<%=txtConvrate.ClientID %>').value;

            var ConversRate =parseFloat(Rate)/ parseFloat(Convrate)
            var SalePrice = (ConversRate) + parseFloat(ConversRate * (SalesDiscount / 100));
            if (isNaN(SalePrice)) {
                SalePrice = 0.00;
            }


            document.getElementById('<%=txtSalePrice.ClientID %>').value = roundNumber(parseFloat(SalePrice), 3); 

          }
          function roundNumber(num, dec) {
              var result = Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
              return result;
          }

          
    </script>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);

        function BeginRequestHandler(sender, args) {

            if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Save") {

                document.getElementById('<%= ImageButton2.ClientID %>').value = "Saving...";
                args.get_postBackElement().disabled = true;
            }
            if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Update") {

                document.getElementById('<%= btnAddNew.ClientID %>').value = "Updating...";
                args.get_postBackElement().disabled = true;
            }


        }
        function SaveClick() {
            document.getElementById('<%= hdnButtonText.ClientID %>').value = "Save";

        }

        function UpdateClick() {
            document.getElementById('<%= hdnButtonText.ClientID %>').value = "Update";

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
          <asp:HiddenField ID="hdnButtonText" runat="server" />
            <asp:HiddenField ID="hdnInvoiceId" runat="server" />
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                width="100%">
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                        Pur. Inv. No. :
                    </td>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 5px; text-align: left">
                        <table>
                            <tr>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 0px;">
                                    <asp:TextBox ID="txtInvNo" Enabled="false" TabIndex="1" CssClass="input_box33" Style="float: left;
                                        text-transform: uppercase;" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="hdnInvoiceSummaryId" runat="server" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill  Pur. Inv. No." Text="*" ControlToValidate="txtInvNo"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <span id="spanPgging" runat="server" style="padding: 0 20px 0 20px; font-weight: bold;
                                        font-size: 18px">
                                        <asp:LinkButton ID="lnkFirst" Text="|<" runat="server" OnClick="lnkFirst_Click"> </asp:LinkButton>
                                        <asp:LinkButton ID="lnkPrevious" Style="margin-left: 10px;" Text="<" runat="server"
                                            OnClick="lnkPrevious_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkNext" Text=">" Style="margin-left: 10px;" runat="server" OnClick="lnkNext_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnLast" Text=">|" Style="margin-left: 10px;" runat="server" OnClick="lnLast_Click"></asp:LinkButton></span>
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    Date :
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    <asp:TextBox ID="txtInvoiceDate" TabIndex="2" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDate'),this);"
                                        runat="server" Width="100px"></asp:TextBox>
                                    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                        onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDate'),this);" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill Invoice Date." Text="*" ControlToValidate="txtInvoiceDate"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                        ControlToValidate="txtInvoiceDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid Invoice Date !!"
                                        Display="None"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    Cr Days :
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    <asp:TextBox ID="txtCrDays" onChange="isNumericPositive(this)" TabIndex="3" MaxLength="25"
                                        runat="server" Width="40px"></asp:TextBox>
                                    <asp:Label ID="Lblcrdate" Style="display: none" runat="server" Width="80px" MaxLength="25"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 111px; padding: 2px 2px 2px 8px;">
                       Purchase Type :
                       
                      
                    </td>
                      <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                        <asp:RadioButton ID="rbdContactPerson" TabIndex="4" GroupName="ClientType" Checked="true" Text="Contract Purchase"
                            runat="server" />
                        <asp:RadioButton ID="rbdDirectPurchase" TabIndex="4" GroupName="ClientType" Text="Direct Purchase"
                            runat="server" />
                      </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 111px; padding: 2px 2px 2px 8px;">
                        Cost Center :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                        <asp:DropDownList ID="ddlCostCenter" TabIndex="4" runat="server" Style="width: 596px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select Cost Center" Text="*" ControlToValidate="ddlCostCenter"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Vendor A/c :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        <asp:DropDownList ID="ddlClientAC" TabIndex="5" runat="server" Style="width: 596px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select Vendor A/C " Text="*" ControlToValidate="ddlClientAC"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Purchase A/C :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        <asp:DropDownList ID="ddlSaleAC" TabIndex="6" runat="server" Style="width: 596px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select Purchase A/C " Text="*" ControlToValidate="ddlSaleAC"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                       <%-- <asp:HiddenField ID="hdnBoutiquePurchaseDiscount" runat="server" />--%>
                        <asp:HiddenField ID="hdnBoutiqueSalesDiscount" runat="server" />
                        <div id="sundryDR" runat="server" style="height: 280px; width: 1000px; padding: 10px;
                            overflow: auto; border: solid 2px  #528627">
                            <table>
                                <tr>
                                    <td colspan="2" style="margin-right: 10px; width: 110px;">
                                        Item Description :
                                    </td>
                                    <td colspan="7" style="margin-right: 10px;">
                                        <asp:DropDownList ID="ddlItemDesc" TabIndex="7" runat="server" Style="width: 678px"
                                            OnSelectedIndexChanged="ddlItemDesc_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Display="Dynamic"
                                            InitialValue="0" ErrorMessage="Please select Item Description " Text="*" ControlToValidate="ddlItemDesc"
                                            ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;">
                                        Quantity:
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtQuantity" TabIndex="8" runat="server" onChange="isNumericPositiveValue(this)"
                                            Style="width: 60px;" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill Quantity " Text="*" ControlToValidate="txtQuantity"
                                            ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="margin-right: 10px;">
                                        Rate:
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtRate" runat="server" Text="0.00" TabIndex="9" onBlur="ChangeConvRate()" onChange="isNumericDecimal(this)"
                                            Style="width: 80px;" MaxLength="10"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill Rate " Text="*" ControlToValidate="txtRate" ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td style="margin-right: 10px;">
                                        Conv. Rate :
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtConvrate" runat="server" Text="1.00" TabIndex="10" onBlur="ChangeConvRate()" onChange="isNumericDecimal(this)"
                                            Style="width: 80px;" MaxLength="10" Enabled="false"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill Rate " Text="*" ControlToValidate="txtConvrate" ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                    </td>
                                     <td style="margin-right: 10px;">
                                        Pur. Discount %:
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtPurchaseDiscount" runat="server" Text="0.00" TabIndex="10"  onChange="isNumericDecimal(this)"
                                            Style="width: 80px;" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill Rate " Text="*" ControlToValidate="txtPurchaseDiscount" ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                    </td>
                                    <%--<td style="margin-right: 10px;">
                                       Sale Price:
                                    </td>--%>
                                    <td style="margin-right: 10px;">
                                        Sale Price:
                                        <asp:TextBox ID="txtSalePrice" runat="server" Text="0.00" TabIndex="10" onChange="isNumericDecimal(this)"
                                            Style="width: 70px;" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill  Sale Price " Text="*" ControlToValidate="txtSalePrice" ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="margin-right: 10px;">
                                      VAT % :
                                     </td>
                                    <td style="margin-right: 10px;">
                                        <%-- OnTextChanged="txtAlterCharges_TextChanged" AutoPostBack="true"--%>
                                        <asp:TextBox ID="txtSt" TabIndex="11" runat="server" onChange="isNumericDecimal(this)" 
                                                   Text="5.00" Style="width: 70px;" MaxLength="10"></asp:TextBox>
                                    </td>
                                   <%-- <td style="margin-right: 10px; width: 80px;">
                                    </td>--%>
                                    <td style="text-align: right">
                                        <asp:Button Style="text-align: right" ID="btnAddNew" TabIndex="12" OnClick="btnAddNew_Click" OnClientClick="UpdateClick()" 
                                            runat="server" Text="Update" ValidationGroup="AddInvoiceDetail" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                            ShowSummary="false" ValidationGroup="AddInvoiceDetail" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="12">
                                        <asp:HiddenField ID="hdndetail" runat="server" />
                                        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                                            AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                            AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                            BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery_RowDataBound"
                                            OnItemCommand="dgGallery_ItemCommand">
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="S.No.">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataSetIndex + 1%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Item Description">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="38%" />
                                                    <ItemTemplate>
                                                        <%# Eval("ItemDesc") %>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Quantity">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Rate">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Purc. Dis. % ">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("BoutiquePurchaseDiscount")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Cost Price">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("BoutiqueCostPrice")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Sales Markup % ">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("BoutiqueSalesDiscount")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Conv. Rate ">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("BoutiqueConvRate")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Sale Price">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("BoutiqueSalePrice")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Amount">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Amount (Dhs) ">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("BoutiqueGrossAmountFC")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="VAT %">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="7%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("SaleTax")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="VAT Amount">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="7%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("SaleTaxAmount")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Amt Inc Taxes">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="16%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("AmountIncludeTaxes")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="imgDel1" runat="server" Text="Delete" CommandArgument='<%#Eval("Id")%>'
                                                            CommandName="Delete" OnClientClick="return askDeleteRow();" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Edit">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="ImageButton1" runat="server" CommandArgument='<%#Eval("Id")%>'
                                                            Text="Edit" CommandName="Edit" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: left; width: 800px;">
                        <table style="width: 800px;">
                            <tr>
                                <td>
                                    Totals :: &nbsp;&nbsp;&nbsp;&nbsp; <b>Qty:</b><asp:Label ID="txtTotalQuantity" Text="0.00"
                                        runat="server" Style="margin-right: 20px; padding-left: 5px"></asp:Label>
                                    <b>Gross Amount: </b>
                                    <asp:Label ID="txtTotalGrossAmount" runat="server" Text="0.00" Style="margin-right: 20px;
                                        padding-left: 5px"></asp:Label>
                                    <%--<b>VAT Tax:</b>
                                    <asp:Label ID="txtSaleTax" runat="server" Text="0.00" Style="margin-right: 20px;
                                        padding-left: 5px"></asp:Label>--%>
                                    <%--<b>ToTal Amount (Including VAT)</b>--%>
                                    <asp:Label ID="txtTotalAmountIncludingTax" runat="server" Text="0.00" Style="margin-right: 20px;
                                        padding-left: 5px; display:none;"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td style="float: left">
                                    <asp:TextBox ID="txtDescription" TabIndex="12" Width="490px" Style="height: 50px;"
                                        onKeyUp="Count(this,250)" onChange="Count(this,250);capitalizeMe(this)" TextMode="MultiLine"
                                        runat="server"></asp:TextBox>
                                </td>
                                <td align="left" style="">
                                    <table cellpadding="0" cellspacing="0" style="width: 300px; border: solid 0px red;
                                        padding: 5px 0px 0px 0px">
                                        <tr style="display: none">
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 115px;">
                                                Discount:
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtDiscount" TabIndex="13" onChange="ChangeCartage(this)" Style="text-align: right"
                                                    Text="0.00" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 115px;">
                                                Cartage Charges:
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtCartagecharges" TabIndex="14" onChange="ChangeCartage(this)"
                                                    Style="text-align: right" Text="0.00" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Gross Amount : <asp:Label ID="Currencytype" runat="server" style="display:none"></asp:Label>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="lblBillAmountFC" Enabled="false" Style="text-align: right" Text="0.00"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                VAT Amount:
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtSaleTax" Enabled="false" Style="text-align: right" Text="0.00"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Invoice Amount:
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="lblBillAmount" Enabled="false" Style="text-align: right" Text="0.00"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>                                        
                                        <%-- <tr>
                                            <td align="right">
                                                Invoice Amount FC:
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtInvoiceamountFC" Enabled="false" Style="text-align: right" Text="0.00"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>--%>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td style="width: 450px;">
                                    <span>
                                        <%--<asp:Button ID="lblbutton" Style="cursor: pointer" OnClientClick="location.href='PurchaseInvoice.aspx?addnew=1'"
                                            runat="server" Text="Add New Invoice" />--%>
                                        <asp:Button ID="lblbutton" Style="cursor: pointer" runat="server" Text="Add New Invoice"
                                            OnClick="lblbutton_Click" />
                                        <asp:Button Text="Delete Invoice" OnClientClick="return askDeleteInvoice();" ID="btnDeleteInvoice"
                                            runat="server" OnClick="btnDeleteInvoice_Click" />
                                        <%-- <a id="PrintBill"
                                                runat="server" target="_blank">
                                                <asp:Button Text="Print Invoice" ID="btnPrintBill" runat="server" /></a>--%>
                                        <asp:Button Text="Print Invoice" ID="btnPrintBill" runat="server" OnClick="btnPrintBill_Click" />
                                    </span>&nbsp;&nbsp;&nbsp;&nbsp; <span id="spanLastInvoice" runat="server" style="display: none">
                                        <b>Last Inv No:
                                            <asp:Literal ID="litLastInvoiceNo" runat="server"></asp:Literal>&nbsp;&nbsp;<asp:Literal
                                                ID="litLastInvDate" runat="server"></asp:Literal></b></span>
                                </td>
                                <td style="width: 550px">
                                    <table cellpadding="0" cellspacing="0" style="padding: 5px 155px 0px 0px; width: 100%">
                                        <tr>
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 33%;">
                                            </td>
                                            <td align="left" valign="top" style="padding: 2px 0px 2px 5px; text-align: right;
                                                width: 33%;">
                                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                                                    ShowSummary="false" ValidationGroup="AddInvoice" />
                                              <%--  <asp:ImageButton ID="ImageButton2" TabIndex="15" runat="server" ImageUrl="~/Images/btn-save.gif"
                                                    OnClick="btn_saveInvoice_Click" ValidationGroup="AddInvoice" />--%>
                                                    
                                                      <asp:Button ID="ImageButton2" OnClientClick="SaveClick()" TabIndex="15" Text="Save" runat="server"
                                                    ValidationGroup="AddInvoice" OnClick="btn_saveInvoice_Click" />
                                                    &nbsp;</div>
                                            </td>
                                            <td style="width: 33%; padding: 2px 2px 20px 3px;">
                                               <asp:Button ID="ImageButton3" Text="Cancel" runat="server" OnClick="btn_cancel_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
