﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.UI.DataVisualization.Charting;

public partial class PrintYearlyTransactionReport : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    clsCookie ck = new clsCookie();
    decimal TotalDebit = 0;
    decimal TotalCredit = 0;
    decimal TotalDebitRow = 0;
    decimal TotalCreditRow = 0;
    decimal TotalAmount = 0;
    Reports objReports = new Reports();
    SetCustomers objCust = new SetCustomers();
    DataTable dttbl = new DataTable();
    decimal Total = 0;
    decimal TotalMonth1 = 0;
    decimal TotalMonth2 = 0;
    decimal TotalMonth3 = 0;
    decimal TotalMonth4 = 0;
    decimal TotalMonth5 = 0;
    decimal TotalMonth6 = 0;
    decimal TotalMonth7 = 0;
    decimal TotalMonth8 = 0;
    decimal TotalMonth9 = 0;
    decimal TotalMonth10 = 0;
    decimal TotalMonth11 = 0;
    decimal TotalMonth12 = 0;

      decimal GridTotal =0;
      decimal GridTotalMonth1 =0;
      decimal GridTotalMonth2=0 ;
      decimal GridTotalMonth3=0 ;
      decimal GridTotalMonth4=0 ;
      decimal GridTotalMonth5=0 ;
      decimal GridTotalMonth6=0 ;
      decimal GridTotalMonth7=0 ;
      decimal GridTotalMonth8=0 ;
      decimal GridTotalMonth9=0 ;
      decimal GridTotalMonth10 =0;
      decimal GridTotalMonth11 =0;
      decimal GridTotalMonth12=0;
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {
            if (Request["Year"].ToString() != "" ||  Request["AcfileId"].ToString() != "")
            {
                Session["s"] = null;
                bindSupplier();
                GetSequenceTrialBalance();
                FillLogo();
                lblYear.Text = Request["Year"].ToString();
                lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
                lblUser.Text = Sessions.UseLoginName.ToString();
                bindAccount();
                lblGrandTotal.Text = String.Format("{0:C}", Total).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblGrandTotalMonth1.Text = String.Format("{0:C}", TotalMonth1).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblGrandTotalMonth2.Text = String.Format("{0:C}", TotalMonth2).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblGrandTotalMonth3.Text = String.Format("{0:C}", TotalMonth3).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblGrandTotalMonth4.Text = String.Format("{0:C}", TotalMonth4).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblGrandTotalMonth5.Text = String.Format("{0:C}", TotalMonth5).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblGrandTotalMonth6.Text = String.Format("{0:C}", TotalMonth6).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblGrandTotalMonth7.Text = String.Format("{0:C}", TotalMonth7).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblGrandTotalMonth8.Text = String.Format("{0:C}", TotalMonth8).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblGrandTotalMonth9.Text = String.Format("{0:C}", TotalMonth9).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblGrandTotalMonth10.Text = String.Format("{0:C}", TotalMonth10).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblGrandTotalMonth11.Text = String.Format("{0:C}", TotalMonth11).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblGrandTotalMonth12.Text = String.Format("{0:C}", TotalMonth12).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                BindGraph();
            }
        }
    }
    protected void bindAccount()
    {

        objReports.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objReports.Id = Convert.ToInt32(Request["AcfileId"].ToString());
        DataSet ds = new DataSet();
        ds = objReports.getAcfileGroup();
        if (ds.Tables[0].Rows.Count > 0)
        {
            lblAccount.Text = Convert.ToString(ds.Tables[0].Rows[0]["Title"]);

        }

    }
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }


    }
    #endregion
    protected void GetSequenceTrialBalance()
    {


        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objSalesInvoice.AcfileID = Convert.ToInt32(Request["AcfileId"].ToString());
        DataSet ds = new DataSet();
        ds = objSalesInvoice.GetSequencePeriodicTransaction();
        DataTable dtResult = new DataTable();
        dtResult = ds.Tables[0].Copy();
        dtResult.Merge(ds.Tables[1]);

        if (dtResult.Rows.Count > 0)
        {

            DataView dv = dtResult.DefaultView;
            dv.Sort = "ParrentId,Id";
            DataTable dt1 = new DataTable();
            dt1 = dv.ToTable();



            grdInvoice.DataSource = dt1;
            grdInvoice.DataBind();
         
            dvNoRecord.Style.Add("display", "none");
            grandTotal.Style.Add("display", " ");

        }

        else
        {
            dvNoRecord.Style.Add("display", " ");
            grandTotal.Style.Add("display", "none");
        }
    }


   
    #region Fill session Table
    public void filltable()
    {
        if (Session["s"] == null)
        {
            DataColumn column = new DataColumn();
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Title";
            column.Caption = "Title";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "ParrentId";
            column.Caption = "ParrentId";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "depth";
            column.Caption = "depth";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);




        }
        else
        {
            dttbl = (DataTable)Session["s"];
        }
    }

    protected void AddRecord(int Id, string Title, int ParrentId, int depth)
    {
        DataRow row;
        row = dttbl.NewRow();
        row["Id"] = Id;
        row["Title"] = Title;
        row["ParrentId"] = ParrentId;
        row["depth"] = depth;


        dttbl.Rows.Add(row);
        dttbl.AcceptChanges();
        Session.Add("s", dttbl);
    }
    #endregion

    protected void grdInvoice_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnid = ((HiddenField)e.Row.FindControl("hdnid"));
            hdnid.Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Id"));

            objReports.year = Convert.ToInt32(Convert.ToInt32(Request["year"]));
            objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objReports.AcFileId = Convert.ToInt32(hdnid.Value);
            DataSet ds = new DataSet();
            ds = objReports.getYearlyExpenses();
            DataGrid dgGallery = (DataGrid)e.Row.FindControl("dgGallery");
            if (ds.Tables[0].Rows.Count > 0)
            {
                GridTotal = 0;
                GridTotalMonth1 = 0;
                GridTotalMonth2 = 0;
                GridTotalMonth3 = 0;
                GridTotalMonth4 = 0;
                GridTotalMonth5 = 0;
                GridTotalMonth6 = 0;
                GridTotalMonth7 = 0;
                GridTotalMonth8 = 0;
                GridTotalMonth9 = 0;
                GridTotalMonth10 = 0;
                GridTotalMonth11 = 0;
                GridTotalMonth12 = 0;
                dgGallery.DataSource = ds.Tables[0];
                dgGallery.DataBind();
            }
            else
            {
                Label lblTitle = (Label)e.Row.FindControl("lblTitle");
                lblTitle.Style.Add("display", "none");
            }

           
        }

    }


    protected void dgGallery_OnItemDataBound(object sender, DataGridItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            GridTotal +=       Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "total").ToString()) ;
            GridTotalMonth1 += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Month1").ToString());
            GridTotalMonth2 += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Month2").ToString()); 
            GridTotalMonth3 += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Month3").ToString()); 
            GridTotalMonth4 += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Month4").ToString()); 
            GridTotalMonth5 += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Month5").ToString()); 
            GridTotalMonth6 += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Month6").ToString()); 
            GridTotalMonth7 += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Month7").ToString()); 
            GridTotalMonth8 += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Month8").ToString()); 
            GridTotalMonth9 += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Month9").ToString()); 
            GridTotalMonth10 += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Month10").ToString()); 
            GridTotalMonth11 += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Month10").ToString()); 
            GridTotalMonth12 += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Month12").ToString()); 
        }

        if (e.Item.ItemType == ListItemType.Footer)
        {
            Label lblTotalMonth1 = ((Label)e.Item.FindControl("lblTotalMonth1"));
            Label lblTotalMonth2 = ((Label)e.Item.FindControl("lblTotalMonth2"));
            Label lblTotalMonth3 = ((Label)e.Item.FindControl("lblTotalMonth3"));
            Label lblTotalMonth4 = ((Label)e.Item.FindControl("lblTotalMonth4"));
            Label lblTotalMonth5 = ((Label)e.Item.FindControl("lblTotalMonth5"));
            Label lblTotalMonth6 = ((Label)e.Item.FindControl("lblTotalMonth6"));
            Label lblTotalMonth7 = ((Label)e.Item.FindControl("lblTotalMonth7"));
            Label lblTotalMonth8 = ((Label)e.Item.FindControl("lblTotalMonth8"));
            Label lblTotalMonth9 = ((Label)e.Item.FindControl("lblTotalMonth9"));
            Label lblTotalMonth10 = ((Label)e.Item.FindControl("lblTotalMonth10"));
            Label lblTotalMonth11 = ((Label)e.Item.FindControl("lblTotalMonth11"));
            Label lblTotalMonth12 = ((Label)e.Item.FindControl("lblTotalMonth12"));
            Label lblTotalMonth = ((Label)e.Item.FindControl("lblTotalMonth"));
           

                lblTotalMonth1.Text = String.Format("{0:C}", GridTotalMonth1).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth2.Text = String.Format("{0:C}", GridTotalMonth2).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth3.Text = String.Format("{0:C}", GridTotalMonth3).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth4.Text = String.Format("{0:C}", GridTotalMonth4).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth5.Text = String.Format("{0:C}", GridTotalMonth5).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

                lblTotalMonth6.Text = String.Format("{0:C}", GridTotalMonth6).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth7.Text = String.Format("{0:C}", GridTotalMonth7).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth8.Text = String.Format("{0:C}", GridTotalMonth8).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth9.Text = String.Format("{0:C}", GridTotalMonth9).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth10.Text = String.Format("{0:C}", GridTotalMonth10).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth11.Text = String.Format("{0:C}", GridTotalMonth11).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth12.Text = String.Format("{0:C}", GridTotalMonth12).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth.Text = String.Format("{0:C}", GridTotal).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');




                Total += GridTotal;
                TotalMonth1 += GridTotalMonth1;
                TotalMonth2 += GridTotalMonth2;
                TotalMonth3 += GridTotalMonth3;
                TotalMonth4 += GridTotalMonth4;
                TotalMonth5 += GridTotalMonth5;
                TotalMonth6 += GridTotalMonth6;
                TotalMonth7 += GridTotalMonth7;
                TotalMonth8 += GridTotalMonth8;
                TotalMonth9 += GridTotalMonth9;
                TotalMonth10 += GridTotalMonth10;
                TotalMonth11 += GridTotalMonth11;
                TotalMonth12 += GridTotalMonth12;


        }

    }


    protected void BindGraph()
    {

      

        DataTable dt = default(DataTable);
        dt = CreateDataTable();

        //Set the DataSource property of the Chart control to the DataTabel
        Chart1.DataSource = dt;

        //Give two columns of data to Y-axle
        Chart1.Series[0].LegendText = "Amount";
        Chart1.Series[0].YValueMembers = "Amount";
      

        //Set the X-axle as date value
        Chart1.Series[0].XValueMember = "Month";

        //Chart1.Series[0].ChartType = SeriesChartType.Line;
        //Bind the Chart control with the setting above

        Chart1.DataBind();

    }


    private DataTable CreateDataTable()
    {
        //Create a DataTable as the data source of the Chart control
        DataTable dt = new DataTable();

        //Add three columns to the DataTable
        dt.Columns.Add("Month");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Recovery");

        DataRow dr;

        //Add rows to the table which contains some random data for demonstration
        dr = dt.NewRow();
        dr["Month"] = "Jan";
        if (TotalMonth1 >= 0)
        {
            dr["Amount"] = TotalMonth1;
        }
        else
        {
            dr["Amount"] = TotalMonth1*-1;
        }
        dt.Rows.Add(dr);


        dr = dt.NewRow();
        dr["Month"] = "Feb";
        if (TotalMonth2 >= 0)
        {
            dr["Amount"] = TotalMonth2;
        }
        else
        {
            dr["Amount"] = TotalMonth2 * -1;
        }
        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Month"] = "Mar";
        if (TotalMonth3 >= 0)
        {
            dr["Amount"] = TotalMonth3;
        }
        else
        {
            dr["Amount"] = TotalMonth3 * -1;
        }
        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Month"] = "Apr";
        if (TotalMonth4 >= 0)
        {
            dr["Amount"] = TotalMonth4;
        }
        else
        {
            dr["Amount"] = TotalMonth4 * -1;
        }
        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Month"] = "May";
        if (TotalMonth5 >= 0)
        {
            dr["Amount"] = TotalMonth5;
        }
        else
        {
            dr["Amount"] = TotalMonth5 * -1;
        }
        dt.Rows.Add(dr);


        dr = dt.NewRow();
        dr["Month"] = "Jun";
        if (TotalMonth6 >= 0)
        {
            dr["Amount"] = TotalMonth6;
        }
        else
        {
            dr["Amount"] = TotalMonth6 * -1;
        }
        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Month"] = "Jul";
        if (TotalMonth7 >= 0)
        {
            dr["Amount"] = TotalMonth7;
        }
        else
        {
            dr["Amount"] = TotalMonth7 * -1;
        }
        dt.Rows.Add(dr);


        dr = dt.NewRow();
        dr["Month"] = "Aug";
        if (TotalMonth8 >= 0)
        {
            dr["Amount"] = TotalMonth8;
        }
        else
        {
            dr["Amount"] = TotalMonth8 * -1;
        }
        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Month"] = "Sep";
        if (TotalMonth9 >= 0)
        {
            dr["Amount"] = TotalMonth9;
        }
        else
        {
            dr["Amount"] = TotalMonth9 * -1;
        }
        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Month"] = "Oct";
        if (TotalMonth10 >= 0)
        {
            dr["Amount"] = TotalMonth10;
        }
        else
        {
            dr["Amount"] = TotalMonth10 * -1;
        }
        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Month"] = "Nov";
        if (TotalMonth11 >= 0)
        {
            dr["Amount"] = TotalMonth11;
        }
        else
        {
            dr["Amount"] = TotalMonth11 * -1;
        }
        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Month"] = "Dec";
        if (TotalMonth12 >= 0)
        {
            dr["Amount"] = TotalMonth12;
        }
        else
        {
            dr["Amount"] = TotalMonth12 * -1;
        }
        dt.Rows.Add(dr);
        dr = dt.NewRow();

      
        return dt;
    }
}
