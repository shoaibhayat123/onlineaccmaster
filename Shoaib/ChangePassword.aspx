﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="ChangePassword.aspx.cs" Inherits="Manage_ChangePassword" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphead" runat="Server">
    Change Password
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <table cellspacing="0" style="float: left" border="0" cellpadding="5" cellspacing="5">
                <tr>
                    <td width="100%" valign="top" style="left: 5; padding-top: 10px;">
                        <table style="margin-left: 10px;">
                            <tr>
                                <td align="left">
                                    Old Password :
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtOldPassword" TextMode="Password" runat="server" TabIndex="1"
                                        MaxLength="10" CssClass="input_box2"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtOldPassword"
                                        Display="Dynamic" ErrorMessage="Please enter Old Password" SetFocusOnError="True"
                                        ValidationGroup="v">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    Enter New Password :
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtnpwd" TextMode="Password" runat="server" MaxLength="15" TabIndex="2"
                                        CssClass="input_box2"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtnpwd"
                                        Display="Dynamic" ErrorMessage="Please enter new password" SetFocusOnError="True"
                                        ValidationGroup="v">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 8px;">
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    Retype New Password :
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtnpwd1" TextMode="Password" runat="server" TabIndex="3" CssClass="input_box2"
                                        MaxLength="15"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtnpwd1"
                                        Display="Dynamic" ErrorMessage="Please enter retype new  password" SetFocusOnError="True"
                                        ValidationGroup="v">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 8px;">
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtnpwd"
                                        ControlToValidate="txtnpwd1" Display="Dynamic" ErrorMessage="Retype password does not match"
                                        SetFocusOnError="True" ValidationGroup="v">*</asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <%-- <asp:ImageButton ID="btn_save" runat="server" ImageUrl="~/admin/images/bt-update.gif"
                                    OnClick="btn_save_Click" ValidationGroup="v" />--%>
                                    <asp:Button ID="btn_save" runat="server" TabIndex="4" Text="Update" OnClick="btn_save_Click"
                                        ValidationGroup="v" />
                                    <br />
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                        ShowSummary="False" ValidationGroup="v" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
