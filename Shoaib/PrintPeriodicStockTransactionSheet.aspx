﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintPeriodicStockTransactionSheet.aspx.cs"
    Inherits="PrintPeriodicStockTransactionSheet" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .Level1
        {
            margin: 5px 0 0 30px;
            font-weight: bold;
            color: Green;
        }
        .Level2
        {
            margin: 5px 0 0 60px;
            font-weight: bold;
            color: Green;
        }
        .Level3
        {
            margin: 5px 0 0 90px;
            font-weight: bold;
            color: Green;
        }
        .Level4
        {
            margin: 5px 0 0 120px;
            font-weight: bold;
            color: Green;
        }
        .Level5
        {
            margin: 5px 0 0 150px;
            font-weight: bold;
            color: Green;
        }
        .Level6
        {
            margin: 5px 0 0 180px;
            font-weight: bold;
            color: Green;
        }
        .Ledger
        {
            margin: 5px 0 0 60px;
        }
    </style>
</head>
<body style="font-family: Tahoma; background: none; font-size: 12px">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 1200px; float: left">
        <table style="width: 1200px; float: left; font-size: 14px; font-weight: bold;">
            <tr id="trTrialBal" runat="server">
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Periodic
                        Stock Transaction Sheet<br />
                    </span><span style="font-size: 14px;"><span style="font-size: 14px;">From :
                        <asp:Label ID="lblFromDate" runat="server" Style="padding-right: 15px;"></asp:Label>
                        To :
                        <asp:Label ID="lblToDate" runat="server"></asp:Label><br />
                    </span>
                        <asp:Label ID="lblAccount" runat="server"></asp:Label><br />
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    
                    <hr />
                   
                </td>
            </tr>
        </table>
    </div>
    <div id="dvResult" runat="server" style="text-align: left; width: 1200px; float: left">
        <asp:GridView ID="grdMainCategory" AutoGenerateColumns="false" GridLines="None" BorderWidth="0"
            ShowHeader="false" ShowFooter="false" CellPadding="0" CellSpacing="0" runat="server"
            OnRowDataBound="grdMainCategory_RowDataBound">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <div class="Level1">
                            <asp:Label ID="lblTitle" Text='<%#Eval("MainCategoryName")%>' Style="text-decoration: underline;
                                font-size: 16px" runat="server"></asp:Label>
                            <asp:HiddenField Value='<%#Eval("MainCategoryCode")%>' ID="hdnMainCategoryCode" runat="server" />
                        </div>
                        <asp:GridView ID="grdSubCategory" AutoGenerateColumns="false" ShowHeader="false"
                            ShowFooter="false" GridLines="None" runat="server" OnRowDataBound="grdSubCategory_RowDataBound">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div class="Level2">
                                            <asp:Label ID="Label1" Text='<%#Eval("SubcategoryName")%>' Style="text-decoration: underline;
                                                font-size: 16px" runat="server"></asp:Label>
                                            <asp:HiddenField Value='<%#Eval("SubCategoryCode")%>' ID="hdnSubCategoryCode" runat="server" />
                                            <asp:HiddenField Value='<%#Eval("MainCategoryCode")%>' ID="hdnMainCategoryCode2"
                                                runat="server" />
                                        </div>
                                      
                                        <div class="Ledger">
                                            <asp:GridView ID="grdLedger" Width="1140px" runat="server" ShowFooter="True" AutoGenerateColumns="False"
                                                ShowHeader="true" AllowPaging="true" PageSize="500000" PagerSettings-Visible="false"
                                                PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader" FooterStyle-CssClass="gridFooter"
                                                AlternatingRowStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                                BorderStyle="Dotted" OnRowDataBound="grdLedger_RowDataBound" BorderWidth="1"
                                                RowStyle-CssClass="gridItem">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Item">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="12%" HorizontalAlign="left" CssClass=" gridpadding" />
                                                        <FooterStyle HorizontalAlign="Right" Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <%# Eval("ItemDesc")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Opening Quantity">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="7%" HorizontalAlign="right" CssClass=" gridpadding" />
                                                        <FooterStyle HorizontalAlign="Right" Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOpeningQuantity" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="TotallblOpeningQuantity" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Purchase">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="7%" HorizontalAlign="right" CssClass=" gridpadding" />
                                                        <FooterStyle HorizontalAlign="Right" Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPurchase" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="TotallblPurchase" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                       <asp:TemplateField HeaderText="Purchase Return">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="7%" HorizontalAlign="right" CssClass=" gridpadding" />
                                                        <FooterStyle HorizontalAlign="Right" Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPurchaseReturn" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="TotallblPurchaseReturn" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Production">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="7%" HorizontalAlign="right" CssClass=" gridpadding" />
                                                        <FooterStyle HorizontalAlign="Right" Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblProduction" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="TotallblProduction" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                   
                                                    <asp:TemplateField HeaderText="Adjustment">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="7%" HorizontalAlign="right" CssClass=" gridpadding" />
                                                        <FooterStyle HorizontalAlign="Right" Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPurchaseAdjustment" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="TotallblPurchaseAdjustment" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total Stock Received">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="7%" HorizontalAlign="right" CssClass=" gridpadding" />
                                                        <FooterStyle HorizontalAlign="Right" Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTotalStockReceived" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="TotallblTotalStockReceived" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Sales">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="7%" HorizontalAlign="right" CssClass=" gridpadding" />
                                                        <FooterStyle HorizontalAlign="Right" Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSales" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="TotallblSales" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Sales Return">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="7%" HorizontalAlign="right" CssClass=" gridpadding" />
                                                        <FooterStyle HorizontalAlign="Right" Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSalesReturn" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="TotallblSalesReturn" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Consumption">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="7%" HorizontalAlign="right" CssClass=" gridpadding" />
                                                        <FooterStyle HorizontalAlign="Right" Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblConsumption" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="TotallblConsumption" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                 
                                                    <asp:TemplateField HeaderText="Adjustment">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="7%" HorizontalAlign="right" CssClass=" gridpadding" />
                                                        <FooterStyle HorizontalAlign="Right" Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSalesAdjustment" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="TotallblSalesAdjustment" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Total Stock Sale">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="7%" HorizontalAlign="right" CssClass=" gridpadding" />
                                                        <FooterStyle HorizontalAlign="Right" Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTotalStockSale" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="TotallblTotalStockSale" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Closing Stock ">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="7%" HorizontalAlign="right" CssClass=" gridpadding" />
                                                        <FooterStyle HorizontalAlign="Right" Font-Size="11px" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblClosingStock" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="TotallblClosingStock" runat="server"></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div id="dvNoRecord" runat="server" style="padding-bottom: 10px; width: 1200px; text-align: center;
        font-family: Gisha; font-size: 20px; background-color: #E1F5FD; display: none;
        float: left; color: #110F0F;">
        <div style="padding-top: 10px; padding-bottom: 5px;">
            No record found
        </div>
    </div>
    <div style="text-align: left; width: 1200px; float: left">
        <table style="width: 1200px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
              <tr>
                <td style="text-align: right">
                    <asp:LinkButton Text="Print" ID="lnkPrint" OnClientClick="window.print();" runat="server"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 1200px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>

                                <script type="text/javascript">

                                            var currentTime = new Date()
                                            var hours = currentTime.getHours()
                                             var Newhours=0;
                                            var minutes = currentTime.getMinutes()
                                            if (minutes < 10)
                                            {
                                            minutes = 0 + minutes
                                            }                                           
                                            if(hours >12)
                                            {
                                            Newhours= hours-12;
                                            }
                                            else
                                            {
                                              Newhours= hours;
                                            }
                                            document.write(Newhours + ":" + minutes + " ")
                                             if(hours > 11)
                                            {                                           
                                            document.write("PM")
                                            } else 
                                            {
                                            document.write("AM")
                                            }

                                </script>

                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
