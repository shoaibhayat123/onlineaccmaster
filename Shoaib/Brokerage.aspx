﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="Brokerage.aspx.cs" Inherits="Brokerage" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Brokerage"></asp:Label>
    <script type="text/javascript">

        function Count(text, long) {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }

        function replaceAll(txt, replace, with_this) {
            return txt.replace(new RegExp(replace, 'g'), with_this);
        }
        function ChangeKg(obj) {
            if (obj.value != null && !obj.value.toString().match(/^[-]?\d*\.?\d*$/) || obj.value == "") {
                obj.value = "0.00";
                alert("Please Enter Numeric Value");

            }
            else {
                if (parseFloat(obj.value) <= 0) {
                    obj.value = "0.00";
                    alert("Invalid Quantity");
                }
                {

                    var CalLb = parseFloat(obj.value);
                    document.getElementById('<%=txtLbs.ClientID %>').value = parseFloat(CalLb).toFixed(3);

                    var SaleRate = replaceAll(document.getElementById('<%=txtSaleRate.ClientID %>').value, ',', '');
                    var PurchaseAmount = replaceAll(document.getElementById('<%=txtPurcheseRate.ClientID %>').value, ',', '');


                    if (document.getElementById('<%=rdbRounding.ClientID %>').checked == true) {
                        var CalSaleAmount = parseFloat(SaleRate) * parseFloat(CalLb);
                        var CalPurchaseAmount = parseFloat(PurchaseAmount) * parseFloat(CalLb);
                        var pl = Math.round(CalSaleAmount) - Math.round(CalPurchaseAmount);

                        document.getElementById('<%=txtSaleAmount.ClientID %>').value = formatCurrency(Math.round(CalSaleAmount));
                        document.getElementById('<%=txtPurchaseAmount.ClientID %>').value = formatCurrency(Math.round(CalPurchaseAmount));
                        document.getElementById('<%=txtPlAmount.ClientID %>').value = formatCurrency(Math.round(pl));
                    }
                    else {
                        var CalSaleAmount = parseFloat(SaleRate) * parseFloat(CalLb);
                        var CalPurchaseAmount = parseFloat(PurchaseAmount) * parseFloat(CalLb);
                        var pl = CalSaleAmount - CalPurchaseAmount;

                        document.getElementById('<%=txtSaleAmount.ClientID %>').value = formatCurrency(CalSaleAmount);
                        document.getElementById('<%=txtPurchaseAmount.ClientID %>').value = formatCurrency(CalPurchaseAmount);
                        document.getElementById('<%=txtPlAmount.ClientID %>').value = formatCurrency(parseFloat(replaceAll(document.getElementById('<%=txtSaleAmount.ClientID %>').value, ',', '')) - parseFloat(replaceAll(document.getElementById('<%=txtPurchaseAmount.ClientID %>').value, ',', '')));
                    }
                }

            }
        }

        function ChangeSaleRate(obj) {
            if (obj.value != null && !obj.value.toString().match(/^[-]?\d*\.?\d*$/) || obj.value == "") {
                obj.value = "0.00";
                alert("Please Enter Numeric Value");

            }
            else {
                if (document.getElementById('<%=rdbRounding.ClientID %>').checked == true) {
                    var Lb = replaceAll(document.getElementById('<%=txtLbs.ClientID %>').value, ',', '');
                    document.getElementById('<%=txtSaleAmount.ClientID %>').value = formatCurrency(Math.round(parseFloat(Lb) * parseFloat(obj.value)));
                    document.getElementById('<%=txtPlAmount.ClientID %>').value = formatCurrency(Math.round(parseFloat(replaceAll(document.getElementById('<%=txtSaleAmount.ClientID %>').value, ',', '')) - parseFloat(replaceAll(document.getElementById('<%=txtPurchaseAmount.ClientID %>').value, ',', ''))));
                }
                else {
                    var Lb = replaceAll(document.getElementById('<%=txtLbs.ClientID %>').value, ',', '');
                    document.getElementById('<%=txtSaleAmount.ClientID %>').value = formatCurrency(parseFloat(Lb) * parseFloat(obj.value));
                    document.getElementById('<%=txtPlAmount.ClientID %>').value = formatCurrency(parseFloat(replaceAll(document.getElementById('<%=txtSaleAmount.ClientID %>').value, ',', '')) - parseFloat(replaceAll(document.getElementById('<%=txtPurchaseAmount.ClientID %>').value, ',', '')));
                }
            }
        }


        function ChangePurchaseRate(obj) {
            if (obj.value != null && !obj.value.toString().match(/^[-]?\d*\.?\d*$/) || obj.value == "") {
                obj.value = "0.00";
                alert("Please Enter Numeric Value");

            }
            else {
                if (document.getElementById('<%=rdbRounding.ClientID %>').checked == true) {
                    var Lb = replaceAll(document.getElementById('<%=txtLbs.ClientID %>').value, ',', '');
                    document.getElementById('<%=txtPurchaseAmount.ClientID %>').value = formatCurrency(Math.round(parseFloat(Lb) * parseFloat(obj.value)));
                    document.getElementById('<%=txtPlAmount.ClientID %>').value = formatCurrency(Math.round(parseFloat(replaceAll(document.getElementById('<%=txtSaleAmount.ClientID %>').value, ',', '')) - parseFloat(replaceAll(document.getElementById('<%=txtPurchaseAmount.ClientID %>').value, ',', ''))));
                }
                else {
                    var Lb = replaceAll(document.getElementById('<%=txtLbs.ClientID %>').value, ',', '');
                    document.getElementById('<%=txtPurchaseAmount.ClientID %>').value = formatCurrency(parseFloat(Lb) * parseFloat(obj.value));
                    document.getElementById('<%=txtPlAmount.ClientID %>').value = formatCurrency(parseFloat(replaceAll(document.getElementById('<%=txtSaleAmount.ClientID %>').value, ',', '')) - parseFloat(replaceAll(document.getElementById('<%=txtPurchaseAmount.ClientID %>').value, ',', '')));

                }

            }
        }

        function ChangeRound() {

            var CalLb = parseFloat(document.getElementById('<%=txtLbs.ClientID %>').value);
            document.getElementById('<%=txtLbs.ClientID %>').value = parseFloat(CalLb).toFixed(3);


            var SaleRate = replaceAll(document.getElementById('<%=txtSaleRate.ClientID %>').value, ',', '');
            var PurchaseAmount = replaceAll(document.getElementById('<%=txtPurcheseRate.ClientID %>').value, ',', '');



            if (document.getElementById('<%=rdbRounding.ClientID %>').checked == true) {

                var CalSaleAmount = parseFloat(SaleRate) * parseFloat(CalLb);
                var CalPurchaseAmount = parseFloat(PurchaseAmount) * parseFloat(CalLb);
                var pl = Math.round(CalSaleAmount) - Math.round(CalPurchaseAmount);
                document.getElementById('<%=txtSaleAmount.ClientID %>').value = formatCurrency(Math.round(CalSaleAmount));
                document.getElementById('<%=txtPurchaseAmount.ClientID %>').value = formatCurrency(Math.round(CalPurchaseAmount));
                document.getElementById('<%=txtPlAmount.ClientID %>').value = formatCurrency(Math.round(pl));
            }
            else {
                var CalSaleAmount = parseFloat(SaleRate) * parseFloat(CalLb);
                var CalPurchaseAmount = parseFloat(PurchaseAmount) * parseFloat(CalLb);
                var pl = CalSaleAmount - CalPurchaseAmount;
                document.getElementById('<%=txtSaleAmount.ClientID %>').value = formatCurrency(CalSaleAmount);
                document.getElementById('<%=txtPurchaseAmount.ClientID %>').value = formatCurrency(CalPurchaseAmount);
                document.getElementById('<%=txtPlAmount.ClientID %>').value = formatCurrency(parseFloat(replaceAll(document.getElementById('<%=txtSaleAmount.ClientID %>').value, ',', '')) - parseFloat(replaceAll(document.getElementById('<%=txtPurchaseAmount.ClientID %>').value, ',', '')));
            }
        }


        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }

          
    </script>

      <script type="text/javascript">
          Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);

          function BeginRequestHandler(sender, args) {

              if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Save") {

                  document.getElementById('<%= ImageButton2.ClientID %>').value = "Saving...";
                  args.get_postBackElement().disabled = true;
              }


          }
          function SaveClick() {
              document.getElementById('<%= hdnButtonText.ClientID %>').value = "Save";

          }

       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
        <asp:HiddenField ID="hdnButtonText" runat="server" />
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                width="100%">
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 13%">
                        Transaction Date :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                        <table>
                            <tr>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                    <asp:TextBox ID="txtSaleDate" TabIndex="1" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtSaleDate'),this);"
                                        runat="server" Width="100px"></asp:TextBox>
                                    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                        onclick="scwShow(scwID('ctl00_MainHome_txtSaleDate'),this);" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill Transection Date." Text="*" ControlToValidate="txtSaleDate"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                        ControlToValidate="txtSaleDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid Transection Date !!"
                                        Display="None"></asp:RegularExpressionValidator>
                                    <asp:HiddenField ID="hdnBrokerageId" runat="server" />
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                    <span id="spanPgging" runat="server" style="padding: 0 20px 0 20px; font-weight: bold;
                                        font-size: 18px">
                                        <asp:LinkButton ID="lnkFirst" Text="|<" runat="server" OnClick="lnkFirst_Click"> </asp:LinkButton>
                                        <asp:LinkButton ID="lnkPrevious" Style="margin-left: 10px;" Text="<" runat="server"
                                            OnClick="lnkPrevious_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkNext" Text=">" Style="margin-left: 10px;" runat="server" OnClick="lnkNext_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnLast" Text=">|" Style="margin-left: 10px;" runat="server" OnClick="lnLast_Click"></asp:LinkButton></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                        Sale Cr Days :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                        <table>
                            <tr>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                    <asp:TextBox ID="txtSaleCrDays" TabIndex="2" OnChange="isNumericPositive(this)" MaxLength="4"
                                        runat="server" Width="40px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill Sale Cr Days." Text="*" ControlToValidate="txtSaleCrDays"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                    Purchase Cr Days :
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                    <asp:TextBox ID="txtPurchaseCrDays" TabIndex="2" OnChange="isNumericPositive(this)"
                                        MaxLength="4" runat="server" Width="40px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill Purchase Cr Days ." Text="*" ControlToValidate="txtPurchaseCrDays"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                        Purchased from :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                        <asp:DropDownList ID="ddlPurchasedFrom" TabIndex="3" runat="server" Style="width: 415px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select  Purchased from" Text="*" ControlToValidate="ddlPurchasedFrom"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                        Sold To :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                        <asp:DropDownList ID="ddlSlodTo" TabIndex="4" runat="server" Style="width: 415px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select Sold To" Text="*" ControlToValidate="ddlSlodTo"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                        Profit &amp; Loss A/c :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                        <asp:DropDownList ID="ddlPL" TabIndex="5" runat="server" Style="width: 415px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select  Profit & Loss A/c :" Text="*" ControlToValidate="ddlPL"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                        Item :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                        <asp:DropDownList ID="ddlItem" TabIndex="6" runat="server" Style="width: 415px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select Item :" Text="*" ControlToValidate="ddlItem"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                        D.O.NO. :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                        <table>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:TextBox ID="txtDoNo" TabIndex="7" MaxLength="10" onChange="isNumeric(this)"
                                        runat="server"></asp:TextBox>
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 0px; width: 115px">
                                    Delivered To:
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                    <asp:TextBox ID="txtDeliveredTo" TabIndex="8" MaxLength="10" onChange="capitalizeMe(this)"
                                        runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                        Quantity:
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                        <table>
                            <tr>
                                <td align="left" colspan="2" valign="top">
                                    <asp:TextBox ID="txtLbs" MaxLength="10" TabIndex="9" Text="0" onChange="ChangeKg(this)"
                                        Style="text-align: right" runat="server"></asp:TextBox>
                                    <asp:RadioButton ID="rdbDo" Text="DO" TabIndex="10" Checked="true" GroupName="Quantity"
                                        runat="server" />
                                    <asp:RadioButton ID="rdbGP" Text="GP" TabIndex="11" GroupName="Quantity" runat="server" />
                                </td>
                                <td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="rdbRounding" TabIndex="12" onClick="ChangeRound()" Text="Rounding"
                                        runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                        Purchase Rate :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                        <table>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:TextBox ID="txtPurcheseRate" MaxLength="10" Text="0.00" TabIndex="13" onChange="isNumericDecimal(this);ChangePurchaseRate(this)"
                                        Style="text-align: right" runat="server"></asp:TextBox>
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 0px; width: 115px">
                                    Sale Rate :
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                    <asp:TextBox ID="txtSaleRate" MaxLength="10" Text="0.00" TabIndex="14" onChange="isNumericDecimal(this);ChangeSaleRate(this)"
                                        Style="text-align: right" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                        Purchase Amount :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                        <table>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:TextBox ID="txtPurchaseAmount" Text="0.00" TabIndex="15" MaxLength="10" Enabled="false"
                                        onChange="isNumeric(this)" Style="text-align: right" runat="server"></asp:TextBox>
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 0px; width: 110px">
                                    Sale Amount :
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                    <asp:TextBox AutoPostBack="true" ID="txtSaleAmount" Text="0.00" MaxLength="10" Enabled="false"
                                        onChange="isNumeric(this)" Style="text-align: right" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                </tr>
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                        Profit/Loss Amount :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                        <asp:TextBox ID="txtPlAmount" Text="0.00" MaxLength="10" Enabled="false" onChange="isNumeric(this)"
                            Style="text-align: right" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 10%">
                        Description For Sale:
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                        <asp:TextBox ID="txtDescriptionSale" TabIndex="16" Width="490px" Style="height: 50px;"
                            onKeyUp="Count(this,250)" onChange="Count(this,250);capitalizeMe(this)" TextMode="MultiLine"
                            runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 10%">
                        Description For Purchase:
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                        <asp:TextBox ID="txtDescriptionPurchese" TabIndex="17" Width="490px" Style="height: 50px;"
                            onKeyUp="Count(this,250)" onChange="Count(this,250);capitalizeMe(this)" TextMode="MultiLine"
                            runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: left">
                        <table cellpadding="0" cellspacing="0" style="padding: 5px 155px 0px 0px">
                            <tr>
                                <td align="right" valign="top" style="padding: 2px 2px 2px 8px; text-align: left;
                                    width: 350px;">
                                    <a>
                                        <%--   <asp:Button ID="Button1" Style="cursor: pointer" OnClientClick="location.href='Brokerage.aspx?addnew=1'"
                                            runat="server" Text="Add New " />--%>
                                        <asp:Button ID="lblbutton" Style="cursor: pointer" runat="server" Text="Add New Invoice"
                                            OnClick="lblbutton_Click" />
                                        <asp:Button Text="Delete" OnClientClick="return askDeleteEntry();" ID="btnDeleteInvoice"
                                            runat="server" OnClick="btnDeleteInvoice_Click" />
                                </td>
                                <td style="width: 470px">
                                    <table cellpadding="0" cellspacing="0" style="padding: 5px 155px 0px 0px; width: 100%">
                                        <tr>
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 33%;">
                                            </td>
                                            <td align="left" valign="top" style="padding: 2px 0px 2px 5px; text-align: right;
                                                width: 33%;">
                                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                                                    ShowSummary="false" ValidationGroup="AddInvoice" />
                                             <%--   <asp:ImageButton ID="ImageButton2" TabIndex="18" runat="server" ImageUrl="~/Images/btn-save.gif"
                                                    OnClick="btn_saveInvoice_Click" ValidationGroup="AddInvoice" />--%>
                                                     <asp:Button ID="ImageButton2" OnClientClick="SaveClick()" TabIndex="18" Text="Save"
                                                    runat="server" ValidationGroup="AddInvoice" OnClick="btn_saveInvoice_Click" />
                                                    
                                                    &nbsp;</div>
                                            </td>
                                            <td style="width: 33%; padding: 2px 2px 20px 3px;">
                                          <asp:Button ID="ImageButton3" Text="Cancel" runat="server" OnClick="btn_cancel_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
