﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="BoutiqueSalesInvoiceMobileReport.aspx.cs" Inherits="BoutiqueSalesInvoiceMobileReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
  <asp:Label ID="lblTitle" runat="server" Text="Sales Invoice Report By Mobile Number"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnInvoiceId" runat="server" />
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Inv.Date From:
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:TextBox ID="txtInvoiceDateFrom" TabIndex="2" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateFrom'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateFrom'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill Invoice Date." Text="*" ControlToValidate="txtInvoiceDateFrom"
                            ValidationGroup="Ledger"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtInvoiceDateFrom" ValidationGroup="Ledger" ErrorMessage="Invalid Invoice Date !!"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Inv.Date To :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:TextBox ID="txtInvoiceDateTo" MaxLength="25" TabIndex="3" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateTo'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateTo'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill Invoice Date." Text="*" ControlToValidate="txtInvoiceDateTo"
                            ValidationGroup="Ledger"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtInvoiceDateTo" ValidationGroup="Ledger" ErrorMessage="Invalid Invoice Date !!"
                            Display="None"></asp:RegularExpressionValidator>
                        <span style="float: right; padding-right: 65px">
                            <asp:Button ID="btnGenerate" Text="Generate" ValidationGroup="Ledger" runat="server"
                                OnClick="btnGenerate_Click" />
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                ShowSummary="false" ValidationGroup="Ledger" />
                        </span>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
