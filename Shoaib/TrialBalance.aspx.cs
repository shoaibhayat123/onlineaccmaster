﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;



public partial class TrialBalance : System.Web.UI.Page
{
    clsCookie ck = new clsCookie();
       protected void Page_Load(object sender, EventArgs e)
       {
           UpdateSession objUpdateSession = new UpdateSession();
           objUpdateSession.UpdateSessions();
           if (Sessions.CustomerCode == "")
           {
               if (ck.GetCookieValue("CustomerCode") != "")
               {
                   Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
               }
               else
               {
                   Response.Redirect("Default.aspx");
               }
           }
        if (!IsPostBack)
        {
            FillGroup();
            txtstartDate.Focus();
            txtstartDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
        }
        SetUserRight();
    }
  
    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        string urllast;
        if (rdWith.Checked == true)
        {
            urllast = "PrintTrialBalance.aspx?AsOn="+txtstartDate.Text+"&AcfileId="+ddlAcfile.SelectedValue.ToString()+ "&Type=1";
        }
        else
        {
            urllast = "PrintTrialBalance.aspx?AsOn=" + txtstartDate.Text + "&AcfileId="+ddlAcfile.SelectedValue.ToString()+"&Type=2";
        }
      
     
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);
     
    }
    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }
            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }

            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

                btnGenerate.Enabled = false;

            }

        }
    }

    #region Fill Item Description
    protected void FillGroup()
    {
        Reports objReports = new Reports();
        objReports.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objReports.Id = 0;
        DataSet ds = new DataSet();
        ds = objReports.getAcfileGroup();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlAcfile.DataSource = ds.Tables[0];
            ddlAcfile.DataTextField = "Title";
            ddlAcfile.DataValueField = "id";
            ddlAcfile.DataBind();
            ddlAcfile.Items.Insert(0, new ListItem("Select Group", "0"));


        }

    }
    #endregion

   
}
   
