﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="ChartofAccount.aspx.cs" Inherits="ChartofAccount" Title="" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <span style="float: left">
        <asp:Label ID="lblTitle" runat="server" Text="Chart Of Account"></asp:Label>
    </span>
    <script type="text/javascript">
        function askDeleteChartOfAccount() {
            var agree;
            agree = confirm("Are you sure to delete this account?");
            if (agree) {
                return true;
            }
            return false;

        }
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div style="width: 100%;">
        <asp:UpdatePanel ID="updatepanel1" runat="server">
            <ContentTemplate>
                <div style="float: left; border-right: 1px solid #B1B89B; min-height: 590px; min-width: 25%">
                    <table cellpadding="0" style="float: left" cellspacing="0">
                        <tr>
                            <td align="left" valign="top" style="width: 100%; padding: 10px 0 2px 10px;">
                                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                    <tr>
                                        <td align="left" valign="top">
                                            <div style="height: 500px; min-width: 400px; overflow: auto; width: 100%">
                                                <asp:TreeView ID="tvcat" runat="server" DataSourceID="XmlDataSource1" OnSelectedNodeChanged="tvcat_SelectedNodeChanged"
                                                    ImageSet="BulletedList" ShowExpandCollapse="true" ExpandDepth="1" ShowLines="true"
                                                    OnDataBinding="tvcat_DataBinding" OnDataBound="tvcat_DataBound" OnTreeNodeDataBound="tvcat_TreeNodeDataBound">
                                                    <NodeStyle CssClass="treelist" />
                                                    <ParentNodeStyle Font-Bold="False" />
                                                    <HoverNodeStyle />
                                                    <SelectedNodeStyle Font-Underline="True" CssClass="treeactive" ForeColor="#0168b3"
                                                        HorizontalPadding="0px" VerticalPadding="0px" />
                                                    <DataBindings>
                                                        <asp:TreeNodeBinding DataMember="MenuItem" SelectAction="Select" ValueField="Categoryid"
                                                            NavigateUrlField="NavigateUrl" TextField="CategoryName" ToolTipField="Tooltip" />
                                                    </DataBindings>
                                                    <NodeStyle Font-Names="Tahoma" ForeColor="Black" HorizontalPadding="5px" NodeSpacing="0px"
                                                        VerticalPadding="0px" />
                                                </asp:TreeView>
                                                <asp:XmlDataSource ID="XmlDataSource1" EnableCaching="false" TransformFile="transform.xslt"
                                                    XPath="MenuItems/MenuItem" runat="server"></asp:XmlDataSource>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 100%;">
                                <asp:Button ID="btnExpand" Text="Expand" runat="server" OnClick="btnExpand_Click" />
                                <asp:Button ID="btnUnExpand" Text="Un-Expand" runat="server" OnClick="btnUnExpand_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td style="border-top: solid 1px black; text-align: center; width: 100%; font-weight: bold;">
                                <asp:Label ID="lblGroup" runat="server"></asp:Label>&nbsp;&nbsp; Group(s)&nbsp;&nbsp;<span
                                    style="font-weight: normal">and</span>&nbsp;&nbsp;
                                <asp:Label ID="lblLedger" runat="server"></asp:Label>&nbsp;&nbsp;Ledger(s) <span
                                    style="padding-left: 10px;">Selected A/c. Level :
                                    <asp:Label ID="lblLevel" runat="server"></asp:Label>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center; width: 100%; font-weight: bold;">
                                Total Dr:<asp:Label ID="lblDebits" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                                Total Cr:<asp:Label ID="lblCredits" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                                Diff:
                                <asp:Label ID="lbldifferences" runat="server"></asp:Label>&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width: 40%; float: left; min-height: 560px;">
                    <table>
                        <tr>
                            <td align="left" valign="top" style="width: 70%">
                                <asp:Panel ID="pnl_menu" runat="server" Visible="false">
                                    <table cellpadding="0px" style="float: right" cellspacing="0px">
                                        <tr>
                                            <td align="left" valign="top" style="padding: 5px 0 2px 5px">
                                                <asp:Label ID="lbl_menu" runat="server" Style="font-size: 14px" Font-Bold="true"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top">
                                                <table cellpadding="0px" cellspacing="0px">
                                                    <tr>
                                                        <td align="left" valign="top" style="padding: 5px 0 2px 5px">
                                                            <asp:ImageButton ID="btn_menu_addNew" runat="server" ImageUrl="~/Images/btnGroup.png"
                                                                OnClick="btn_menu_addNew_Click" />
                                                            <asp:ImageButton ID="btn_menu_Ladger" runat="server" ImageUrl="~/Images/btnLadger.png"
                                                                 OnClick="btn_menu_Ladger_Click" />
                                                            <asp:ImageButton ID="btn_menu_EditSubCategory" runat="server" ImageUrl="~/Images/edit.png"
                                                                OnClick="btn_menu_EditSubCategory_Click" />
                                                            <asp:ImageButton ID="imgbtn_deletecontent" runat="server" ImageUrl="~/Images/delete.png"
                                                                 Visible="false" OnClick="imgbtn_deletecontent_Click" OnClientClick="return askDeleteChartOfAccount()" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnl_MenuOption" runat="server" Visible="false">
                                    <asp:MultiView ID="multiview_1" runat="server">
                                        <asp:View ID="view1" runat="server">
                                            <table cellpadding="0" cellspacing="0" style="width: 500px; padding: 20px 0px 0px 0px">
                                                <tr>
                                                    <td align="left" valign="top" style="padding: 10px 0 10px 10px; width: 150px">
                                                        <asp:Label ID="Label1" runat="server" Style="font-size: 14px; font-weight: bold"
                                                            Text="Add Group"></asp:Label>
                                                        <asp:HiddenField ID="hid_submenuid" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblParrentGroup" Style="font-size: 18px; font-weight: bold" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 100px;">
                                                        Group
                                                    </td>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="400px">
                                                        <asp:TextBox ID="txt_subcate" onChange="capitalizeMe(this)" runat="server" Width="300px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="req1" runat="server" Display="Dynamic" ErrorMessage="Please Fill Group."
                                                            Text="*" ControlToValidate="txt_subcate" ValidationGroup="AddSubCat"></asp:RequiredFieldValidator>
                                                        <asp:ValidationSummary ID="vs_AddSubCat" runat="server" ShowMessageBox="true" ShowSummary="false"
                                                            ValidationGroup="AddSubCat" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" valign="top">
                                                    </td>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px">
                                                        <asp:ImageButton ID="btn_save" runat="server" ImageUrl="~/Images/btn-save.gif" OnClick="btn_save_Click"
                                                            ValidationGroup="AddSubCat" />&nbsp;
                                                        <asp:ImageButton ID="btn_cancel" runat="server" ImageUrl="~/Images/bt-cancal.gif"
                                                            OnClick="btn_cancel_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                        <asp:View ID="view2" runat="server">
                                            <table cellpadding="0" cellspacing="0" style="width: 500px; padding: 20px 0px 0px 0px">
                                                <tr>
                                                    <td align="left" valign="top" style="padding: 10px 0 10px 10px; width: 100px">
                                                        <asp:Label ID="lblLadgerTitle" runat="server" Style="font-size: 14px; font-weight: bold"
                                                            Text="Add Ledger"></asp:Label>
                                                        <asp:HiddenField ID="hid_Ladgerid" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblParrent" Style="font-size: 18px; font-weight: bold;" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 100px;">
                                                        Title
                                                    </td>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="400px">
                                                        <asp:TextBox ID="txtTitle" onChange="capitalizeMe(this)" runat="server" MaxLength="100"
                                                            Width="300px"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                                            ErrorMessage="Please Fill Title." Text="*" ControlToValidate="txtTitle" ValidationGroup="AddLadger"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 100px;">
                                                        Phone No.
                                                    </td>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="400px">
                                                        <asp:TextBox ID="txtPhone" MaxLength="50" runat="server" Width="300px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 150px;">
                                                        Address
                                                    </td>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="400px">
                                                        <asp:TextBox ID="txtAddress1" onChange="capitalizeMe(this)" MaxLength="100" runat="server"
                                                            Width="300px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 150px;">
                                                    </td>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="400px">
                                                        <asp:TextBox ID="txtAddress2" onChange="capitalizeMe(this)" MaxLength="100" runat="server"
                                                            Width="300px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 150px;">
                                                    </td>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="400px">
                                                        <asp:TextBox ID="txtAddress3" onChange="capitalizeMe(this)" MaxLength="100" runat="server"
                                                            Width="300px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tblSundyCredit" runat="server" cellpadding="0" cellspacing="0" style="width: 500px;
                                                display: none; padding: 0px 0px 0px 0px">
                                                <tr>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 150px;">
                                                        Email Address
                                                    </td>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="400px">
                                                        <asp:TextBox ID="txtEmail" runat="server" MaxLength="100" Width="300px"></asp:TextBox>
                                                    <%--<asp:RequiredFieldValidator Text="*" Display="None" ID="RequiredFieldValidator6"
                                                            runat="server" ControlToValidate="txtEmail" ErrorMessage="Please Enter Email Address"
                                                            SetFocusOnError="True" ValidationGroup="AddLadger"></asp:RequiredFieldValidator>--%>
                                                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                                            Display="None" ErrorMessage="Please Enter Valid Email Address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                            ValidationGroup="AddLadger"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 150px;">
                                                        Credit period
                                                    </td>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="400px">
                                                        <asp:TextBox ID="txtCreditperiod" onChange="isNumeric(this)" MaxLength="25" runat="server"
                                                            Width="300px"></asp:TextBox>
                                                        <asp:RangeValidator runat="server" ID="RangeValidator1" ValidationGroup="AddLadger"
                                                            ControlToValidate="txtCreditperiod" Display="None" MaximumValue="9999999999"
                                                            MinimumValue="0" Type="Double" ErrorMessage="Please enter valid Credit period"></asp:RangeValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 150px;">
                                                        Sales Tax No.
                                                    </td>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="400px">
                                                        <asp:TextBox ID="txtSalesTax" onChange="capitalizeMe(this)" MaxLength="25" runat="server"
                                                            Width="300px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 150px;">
                                                        PAN/NTN No.
                                                    </td>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="400px">
                                                        <asp:TextBox ID="txtPan" onChange="capitalizeMe(this)" MaxLength="25" runat="server"
                                                            Width="300px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table cellpadding="0" cellspacing="0" style="width: 500px; padding: 0px 0px 0px 0px">
                                                <tr>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 130px;">
                                                        Opening Balance
                                                    </td>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="400px">
                                                        <span style="float: left">
                                                            <asp:TextBox ID="txtOpeningBalance" onChange="isNumeric(this)" runat="server" MaxLength="15"
                                                                Width="223px"></asp:TextBox></span> <span style="float: left">
                                                                    <asp:RadioButton ID="rdbDR" runat="server" GroupName="rdbCategory" Checked="true"
                                                                        Text="Dr." />
                                                                    <asp:RadioButton ID="rdbCR" runat="server" GroupName="rdbCategory" Text="Cr." /></span>
                                                        <asp:RangeValidator runat="server" ID="range1" ValidationGroup="AddLadger" ControlToValidate="txtOpeningBalance"
                                                            Display="None" MaximumValue="9999999999.00" MinimumValue="0.00" Type="Double"
                                                            ErrorMessage="Please enter valid numeric Opening Balance"></asp:RangeValidator>
                                                        <asp:RangeValidator runat="server" ID="RangeValidator4" ValidationGroup="Invoice"
                                                            ControlToValidate="txtOpeningBalance" Display="None" MaximumValue="9999999999.00"
                                                            MinimumValue="0.00" Type="Double" ErrorMessage="Please enter valid numeric Opening Balance"></asp:RangeValidator>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                                                            ErrorMessage="Please Fill  Opening Balance" Text="*" ControlToValidate="txtOpeningBalance"
                                                            ValidationGroup="Invoice"></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div id="sundryDR" runat="server" style="height: 200px; width: 520px; display: none;
                                                overflow: auto; border: solid 2px  #528627">
                                                <table runat="server" cellpadding="0" cellspacing="0" style="width: 500px; padding: 10px;">
                                                    <tr>
                                                        <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 100px;">
                                                            Invoice No.
                                                        </td>
                                                        <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="200px">
                                                            <asp:TextBox ID="txtInvoiceNo" onChange="capitalizeMe(this)" MaxLength="25" runat="server"
                                                                Width="100px"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                                                ErrorMessage="Please Fill  Invoice No." Text="*" ControlToValidate="txtInvoiceNo"
                                                                ValidationGroup="Invoice"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 100px;">
                                                            Invoice Date
                                                        </td>
                                                        <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="200px">
                                                            <asp:TextBox ID="txtBillDate" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtBillDate'),this);"
                                                                runat="server" Width="100px"></asp:TextBox>
                                                            <img src="images/Calendar.gif" style="vertical-align: top" title='Click Here' alt='Click Here'
                                                                onclick="scwShow(scwID('ctl00_MainHome_txtBillDate'),this);" />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                                                ErrorMessage="Please Fill Invoice Date." Text="*" ControlToValidate="txtBillDate"
                                                                ValidationGroup="Invoice"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                                                ControlToValidate="txtBillDate" ValidationGroup="Invoice" ErrorMessage="Invalid Invoice Date !!"
                                                                Display="None"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 100px;">
                                                            Credit Days
                                                        </td>
                                                        <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="200px">
                                                            <asp:TextBox ID="txtCreditDays" onChange="isNumeric(this)" MaxLength="10" runat="server"
                                                                Width="100px"></asp:TextBox>
                                                            <asp:RangeValidator runat="server" ID="RangeValidator3" ValidationGroup="Invoice"
                                                                ControlToValidate="txtCreditDays" Display="None" MaximumValue="9999999999" MinimumValue="0"
                                                                Type="Double" ErrorMessage="Please enter valid Credit Days"></asp:RangeValidator>
                                                        </td>
                                                        <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 150px;">
                                                            Invoice Amount
                                                        </td>
                                                        <td align="left" valign="top" style="padding: 2px 2px 2px 5px;" width="200px">
                                                            <asp:TextBox ID="txtInvoiceAmount" onChange="isNumeric(this)" MaxLength="25" runat="server"
                                                                Width="100px"></asp:TextBox>
                                                            <asp:RangeValidator runat="server" ID="RangeValidator2" ValidationGroup="Invoice"
                                                                ControlToValidate="txtInvoiceAmount" Display="None" MaximumValue="9999999999.00"
                                                                MinimumValue="0.00" Type="Double" ErrorMessage="Please enter valid Invoice Amount"></asp:RangeValidator>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                                                                ErrorMessage="Please Fill Invoice Amount." Text="*" ControlToValidate="txtInvoiceAmount"
                                                                ValidationGroup="Invoice"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="text-align: right">
                                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                                                ShowSummary="false" ValidationGroup="Invoice" />
                                                            <asp:Button ID="btnAddNew" Text="Add New" ValidationGroup="Invoice" OnClick="btnAddNew_Click"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">
                                                            <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                                                                AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                                                AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                                                BorderStyle="Dotted" BorderWidth="1" OnItemDataBound="dgGallery_RowDataBound"
                                                                OnItemCommand="dgGallery_ItemCommand" ItemStyle-CssClass="gridItem">
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderText="S.No.">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <%# Container.DataSetIndex + 1%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Invoice Date">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:Label runat="server" ID="lblInvoiceDate"></asp:Label>
                                                                            <%--  <%#String.Format("{0:dd-MMM-yyyy}", Convert.ToDateTime(Eval("InvoiceDate")))%>--%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Invoice No.">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <%# Eval("InvoiceNo")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Credit Days">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <%# Eval("CreditDays")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Amount">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <ItemStyle Width="15%" HorizontalAlign="Right" />
                                                                        <ItemTemplate>
                                                                            <%# Eval("Amount")%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn HeaderText="Delete">
                                                                        <HeaderStyle HorizontalAlign="Center" />
                                                                        <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="imgDel1" runat="server" ImageUrl="Images/bt-delete.gif" AlternateText="Delete"
                                                                                CommandArgument='<%#Eval("Id")%>' CommandName="Delete" OnClientClick="return askDelete();" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" id="tdTotal" runat="server" style="text-align: right; display: none">
                                                            Total amount:
                                                            <asp:Label ID="lblTotalamount" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" id="tdDiscount" style="text-align: right; display: none">
                                                            Difference amount:
                                                            <asp:Label ID="lblDifference" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <table cellpadding="0" cellspacing="0" style="width: 500px; padding: 20px 0px 0px 0px">
                                                <tr>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 115px;">
                                                    </td>
                                                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px">
                                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                                                            ShowSummary="false" ValidationGroup="AddLadger" />
                                                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/btn-save.gif"
                                                            OnClick="btn_saveLedger_Click" ValidationGroup="AddLadger" />&nbsp;
                                                        <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/bt-cancal.gif"
                                                            OnClick="btn_cancel_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:View>
                                    </asp:MultiView>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
