﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="F_Sql.aspx.cs" Inherits="F_Sql" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Query</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table cellpadding="5" cellspacing="5" runat="server" id="tblLogin">
         <tr runat="server" id="trMessage" visible="false">
                <td>
                </td>
                <td>
                <asp:Label runat="server" ID="lblMessage" ForeColor="Red" Text="Please enter valid user name & password" ></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td>Connection String
                </td>
                <td>
                <asp:TextBox runat="server" ID="txtConStr" Width="500"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="req1" ControlToValidate="txtConStr" ErrorMessage="Please enter connection string" Display="None" ValidationGroup="Login" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            
            <tr>
                <td>User Name
                </td>
                <td>
                <asp:TextBox runat="server" ID="txtUserName" Width="300"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="req2" ControlToValidate="txtUserName" ErrorMessage="Please enter username" Display="None" ValidationGroup="Login" ></asp:RequiredFieldValidator>
                </td>
            </tr>
             <tr>
                <td>Password
                </td>
                <td>
                <asp:TextBox runat="server" ID="txtPassword"  TextMode="Password"  Width="200"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtPassword" ErrorMessage="Please enter password" Display="None" ValidationGroup="Login" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            
             <tr>
                <td>
                </td>
                <td>
                <asp:Button runat="server" ID="btnLogin"  Text="Login"  OnClick="btnLogin_Click" ValidationGroup="Login"></asp:Button>
                <asp:ValidationSummary runat="server" ID="val1" ShowMessageBox="true" ShowSummary="false" ValidationGroup="Login" />
                </td>
            </tr>
        </table>
        <table cellpadding="5" cellspacing="5" runat="server" id="tblQuery" visible="false">
            <tr runat="server" id="trError" visible="false">
                <td>
                </td>
                <td>
                <asp:Label runat="server" ID="lblError" ForeColor="Red" ></asp:Label>
                </td>
            </tr>
            
            <tr>
                <td colspan="2">Query
                </td></tr><tr>
                <td colspan="2">
                <asp:TextBox runat="server" ID="txtQuery" TextMode="MultiLine"  Width="600" Height="400"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="req4" ControlToValidate="txtQuery" ErrorMessage="Please enter query" Display="None" ValidationGroup="Query" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            
             <tr>
                <td>
                </td>
                <td>
                <asp:Button runat="server" ID="btnSub"  Text="Execute Query" OnClick="btnSub_Click" ValidationGroup="Query"  ></asp:Button>
                <asp:ValidationSummary runat="server" ID="val2" ShowMessageBox="true" ShowSummary="false" ValidationGroup="Query" />
                </td>
            </tr>
            
            <tr><td colspan="2">
            <asp:DataGrid runat="server" AutoGenerateColumns="true" ID="dgResult" ></asp:DataGrid>
            
            
            </td></tr>
        </table>
    </div>
    </form>
</body>
</html>
