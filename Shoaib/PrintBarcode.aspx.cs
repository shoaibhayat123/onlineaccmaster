﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintBarcode : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    clsCookie ck = new clsCookie();

    Reports objReports = new Reports();
    SetCustomers objCust = new SetCustomers();
    DataTable dttbl = new DataTable();
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request["barcode"].ToString() != "")
            {
                Session["s"] = null;
                bindSupplier();
                FillLogo();
                FillItemDetail();
            }
        }
    }
    [
    System.Web.Services.WebMethod()
    ]
    public static string GetBarcode(string valueToEncode, string symbology)
    {
        //Use Codeless Barcode Generation in v5.0
        string addChecksum = "&AC=T";
        string code39Ext = "&EXT=T";

        //If barcode symbology is Code 39
        if (symbology == "Code39")
        {
            //Disable checksum    
            addChecksum = "";
        }

        //HTTP Handler for Codeless Barcode Generation
        //IMPORTANT: Look for the following setting in the web.config file to learn how to configure it
        //<add verb="*" path="BarcodeGen.axd" type="Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional, Neodynamic.WebControls.BarcodeProfessional"/>

        //Return url for barcode generation
        return "BarcodeGen.axd?S=" + symbology + "&C=" + valueToEncode + "&DC=T" + addChecksum + code39Ext;
    }

    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }


    }
    #endregion

    protected void FillItemDetail()
    {
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        if (Convert.ToString(Request["barcode"]) != "")
        {
            objCust.ItemId = Convert.ToInt32(Request["barcode"]);
        }
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        if (ds.Tables[0].Rows.Count > 0)
        {

            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.MainCategoryCode = Convert.ToInt32(ds.Tables[0].Rows[0]["MainCategoryCode"].ToString());
            DataSet dsBrand = new DataSet();
            dsBrand = objCust.GetMainCategory();
            if (dsBrand.Tables[0].Rows.Count > 0)
            {
               // lblBrand.Text = Convert.ToString(dsBrand.Tables[0].Rows[0]["MainCategoryName"].ToString());
            }


            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.MainCategoryCode = Convert.ToInt32(ds.Tables[0].Rows[0]["SubCategoryCode"].ToString());
            DataSet dsLabel = new DataSet();
            dsLabel = objCust.GetSubCategory();
            if (dsLabel.Tables[0].Rows.Count > 0)
            {
              // lblLabel.Text = Convert.ToString(dsLabel.Tables[0].Rows[0]["SubCategoryName"].ToString());
            }
            if (Convert.ToString(ds.Tables[0].Rows[0]["SaleRate"]) != "")
            {
                lblPrice.Text = Convert.ToString(Convert.ToInt32(ds.Tables[0].Rows[0]["SaleRate"]));

            }

        }
    }
}