﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class BoutiqueUserSetup : System.Web.UI.Page
{
    #region DataMembers & Varriables
    SetCustomers objCust = new SetCustomers();
    #endregion
    #region Page Load Event

    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "" || Sessions.CustomerCode == null)
        {

            Response.Redirect("Default.aspx");

        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
            if (!IsPostBack)
            {

                FilItems();
                BindUser();
            }
        }
    }

    #endregion
    #region Add/ Edit Product Category  Setup

    protected void btn_save_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
            if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
            {
                objCust.BoutiqueUserId = int.Parse(hdnItemId.Value);
            }
            else
            {
                objCust.BoutiqueUserId = 0;
            }

            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.Country = txtCountry.Text;
            objCust.City = txtCity.Text;
            objCust.Address = txtAddress.Text;
            objCust.UserName = txtUser.Text;
            objCust.Password = txtPassword.Text;
            objCust.DesignerName = txtDesigner.Text;
            objCust.MobileNo = txtMobileNo.Text;
            objCust.ContactNo = txtContactNo.Text;
            objCust.BrandId = Convert.ToInt32(ddlBrand.SelectedValue);
            objCust.Email = txtEmail.Text;
            if (Sessions.UserRole.ToString() != "Admin")
            {
                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
               
                
                if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" || Convert.ToString(dt.Rows[0]["IsAdd"]) == "True")
                {

                    if (Convert.ToString(dt.Rows[0]["IsAdd"]) == "True" && (hdnItemId.Value == null || hdnItemId.Value == string.Empty))
                    {
                        int result = objCust.AddEditBoutiqueUser();

                        /* region add Cost center data*/
                        if (result != -1)
                        {
                            /* 1. delete Old records */
                            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            objCust.userid = result;
                          
                        }

                        if (hdnItemId.Value == null || hdnItemId.Value == string.Empty)
                        {
                            if (result != -1)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('User  successful added!');", true);
                                clear();             
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                            }
                                          
                            FilItems();
                        }
                    }
                    else
                    {
                        if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" && hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                        {
                            int result = objCust.AddEditBoutiqueUser();

                           
                            if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                            {
                                if (result != -1)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('User  successful updated!');", true);
                                    clear();              
                                }

                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                                }
                                         
                                FilItems();
                            }

                        }

                        else
                        {
                            if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='BoutiqueUserSetup.aspx';", true);
                            }

                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='BoutiqueUserSetup.aspx';", true);
                            }
                        }
                    }
                }
                else
                {
                    if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='BoutiqueUserSetup.aspx';", true);
                    }

                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='BoutiqueUserSetup.aspx';", true);
                    }
                }

            }
            else
            {

                int result = objCust.AddEditBoutiqueUser();

               

                if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                {
                    if (result != -1)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('User  successful updated!');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                    }
                }
                else
                {
                    if (result != -1)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('User  successful added!');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                    }

                }
                clear();
              
                FilItems();
            }
       

    }

    #endregion
    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        clear();
       
    }
    #endregion
    #region Blank Controls
    private void clear()
    {

        txtCity.Text = "";
        txtContactNo.Text = "";
        txtCountry.Text = "";
        txtDesigner.Text = "";
        txtMobileNo.Text = "";
        txtPassword.Text = "";
        txtUser.Text = "";
        txtAddress.Text = "";
        ddlBrand.SelectedValue = "0";
        hdnItemId.Value = "";
        txtEmail.Text = "";

      

    }
    #endregion
    #region Fill Item Setup
    protected void FillData(int ItemId)
    {
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.BoutiqueUserId = ItemId;
        DataSet ds = new DataSet();
        ds = objCust.getBoutiqueUserSetup();
        if (ds.Tables[0].Rows.Count > 0)
        {
            
            txtCity.Text = Convert.ToString(ds.Tables[0].Rows[0]["City"].ToString());
            txtAddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["Address"].ToString());
            txtContactNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["ContactNo"].ToString());
            txtCountry.Text = Convert.ToString(ds.Tables[0].Rows[0]["Country"].ToString());
            txtDesigner.Text = Convert.ToString(ds.Tables[0].Rows[0]["DesignerName"].ToString());
            txtMobileNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["MobileNo"].ToString());
            txtPassword.Text = Convert.ToString(ds.Tables[0].Rows[0]["Password"].ToString());
            txtUser.Text = Convert.ToString(ds.Tables[0].Rows[0]["UserName"].ToString());
            ddlBrand.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["BrandId"].ToString());          
            hdnItemId.Value = ItemId.ToString();
            txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["Email"].ToString());          


        }





    }
    #endregion
    #region Fill Main Category  Setup
    protected void FilItems()
    {
      

            objCust.BoutiqueUserId = 0;
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataSet ds = new DataSet();
            ds = objCust.getBoutiqueUserSetup();
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 30)
                {
                    grdCustomer.AllowPaging = true;
                }
                grdCustomer.Visible = true;
                grdCustomer.DataSource = ds.Tables[0];
                grdCustomer.DataBind();
                lblError.Visible = false;
            }
            else
            {
                grdCustomer.Visible = false;
                lblError.Visible = true;
            }
       

    }
    #endregion
    #region GridView Events
    protected void grdCustomer_RowCommand(object sender, DataGridCommandEventArgs e)
    {

        if (e.CommandName == "Delete")
        {
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.BoutiqueUserId = int.Parse(e.CommandArgument.ToString());          
            objCust.DeleteBoutiqueUser();
            FilItems();
        }

        if (e.CommandName == "Edit")
        {
            FillData(int.Parse(e.CommandArgument.ToString()));

        }
    }
    protected void grdCustomer_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {


            if (Sessions.UserRole.ToString() != "Admin")
            {


                LinkButton lbEdit = (LinkButton)e.Item.FindControl("lbEdit");
                LinkButton lbDelete = (LinkButton)e.Item.FindControl("lbDelete");

                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
                if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
                {
                    Response.Redirect(Request.UrlReferrer.ToString());
                }
                if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
                {

                    lbDelete.Visible = false;

                }
                if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
                {

                    lbEdit.Visible = false;

                }

            }
        }


    }

    protected void grdCustomer_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        grdCustomer.CurrentPageIndex = e.NewPageIndex;
        FilItems();
    }
    #endregion
    #region BindUser
    protected void BindUser()
    {
        
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.MainCategoryCode = 0;
        DataSet ds = new DataSet();
        ds = objCust.GetMainCategory();
        ddlBrand.DataSource = ds.Tables[0];
        ddlBrand.DataTextField = "MainCategoryName";
        ddlBrand.DataValueField = "MainCategoryCode";
        ddlBrand.DataBind();
        ddlBrand.Items.Insert(0, new ListItem("Select Brand", "0"));

    }
    #endregion
}