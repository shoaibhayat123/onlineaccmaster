﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaticPages.master" AutoEventWireup="true"
    CodeFile="Register.aspx.cs" Inherits="Register" %>

<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content_wraper5">
        <div id="content_midd5">
            <div class="acc-features-middle-bg">
                <div class="top-heading">
                    <h1>
                        Online Registration Form</h1>
                </div>
            </div>
            <div class="accmiddle-features-bg-2" style="min-height: 520px">
                <div class="features-wrapper">
                    <div class="features-box">
                        <%-- <div class="features-left-link-box">
                            <div class="left-link-Content">
                                <h2>
                                    Privacy Topics:</h2>
                                <ul>
                                    <li><a href="#">Privacy at AccMaster</a></li>
                                    <li><a href="#">Data Stewardship</a></li>
                                    <li><a href="#">AccMaster Services</a>
                                        <ul>
                                            <li><a href="#">Online Services</a></li>
                                            <li><a href="#">Mobile Services</a></li>
                                            <li><a href="#">On Your Computer</a></li>
                                            <li><a href="#">For Individuals</a></li>
                                            <li><a href="#">For Businesses</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="left-link-bottom">
                                <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                            </div>
                        </div>--%>
                        <div style="float: left">
                            <div class="features-left-link-box">
                                <div class="left-link-Content">
                                    <h2 style="text-decoration: underline; font-size: 18px;">
                                        AccMaster</h2>
                                    <br />
                                    <ul style="margin-left: 20px">
                                        <li><a href="index.aspx">Home</a></li>
                                        <li><a href="features.aspx">Features</a></li>
                                        <li><a href="pricing.aspx">Product & Pricing</a></li>
                                        <li><a href="PayOnline.aspx">Pay Online</a></li>
                                        <li><a href="Privacy.aspx">Privacy</a></li>
                                        <li><a href="Terms.aspx">Terms</a></li>
                                        <li><a href="ContactUs.aspx">Contact us</a></li>
                                        <li><a href="AboutUs.aspx">About Us</a></li>
                                        <li><a href="Careers.aspx">Careers</a></li>
                                    </ul>
                                </div>
                                <div class="left-link-bottom">
                                    <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                                </div>
                            </div>
                            <div class="features-left-link-box" style="clear: both; margin-top: 20px;">
                                <div class="left-link-Content" style="min-height: 100px; line-height: 0.6">
                                    <asp:Literal ID="ltrlAddress" runat="server"></asp:Literal>
                                </div>
                                <div class="left-link-bottom">
                                    <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                                </div>
                            </div>
                        </div>
                        <div class="feat-right-part-box">
                            <div class="feta-right-part-content">
                                <div class="feat-right-box-dtls">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="left" valign="top">
                                                <table cellpadding="5" style="margin: auto;" cellspacing="5">
                                                    <tr>
                                                        <td>
                                                            Customer Name :<span style="color: Red;">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="input_box2" onChange="capitalizeMe(this)" ID="txtCustomerName"
                                                                runat="server" MaxLength="100"></asp:TextBox>
                                                            <asp:RequiredFieldValidator Text="*" Display="None" ID="RequiredFieldValidator2"
                                                                runat="server" ControlToValidate="txtCustomerName" ErrorMessage="Please Enter Customer Name"
                                                                SetFocusOnError="True" ValidationGroup="Reg"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Contact Person :
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="input_box2" onChange="capitalizeMe(this)" ID="txtContactPerson"
                                                                runat="server" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Address :
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="input_box2" onChange="capitalizeMe(this)" ID="txtAddress1"
                                                                runat="server" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="input_box2" onChange="capitalizeMe(this)" ID="txtAddress2"
                                                                runat="server" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="input_box2" onChange="capitalizeMe(this)" ID="txtAddress3"
                                                                runat="server" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            City :
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="input_box2" onChange="capitalizeMe(this)" ID="txtCity" runat="server"
                                                                MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Country :
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="input_box2" onChange="capitalizeMe(this)" ID="txtCountry"
                                                                runat="server" MaxLength="50"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Phone Number :
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="input_box2" onChange="capitalizeMe(this)" ID="txtPhoneNumber"
                                                                runat="server" MaxLength="30"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Cell Number :
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="input_box2" ID="txtCellNumber" runat="server" MaxLength="30"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Primary Email :<span style="color: Red;">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="input_box2" ID="txtPrimaryEmail" runat="server" MaxLength="255"></asp:TextBox>
                                                            <asp:RequiredFieldValidator Text="*" Display="None" ID="RequiredFieldValidator1"
                                                                runat="server" ControlToValidate="txtPrimaryEmail" ErrorMessage="Please Enter Primary Email"
                                                                SetFocusOnError="True" ValidationGroup="Reg"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtPrimaryEmail"
                                                                Display="None" ErrorMessage="Please Enter Valid Primary Email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                ValidationGroup="Reg"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Secondry Email :
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="input_box2" ID="txtSecondryEmail" runat="server" MaxLength="255"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSecondryEmail"
                                                                Display="None" ErrorMessage="Please Enter Valid  Secondry Email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                ValidationGroup="Reg"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Number Of Users<span style="color: Red;">*</span>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlNoCustomer" Style="width: 270px" runat="server">
                                                                <asp:ListItem Text="Select Number Of Users" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator Text="*" Display="None" ID="RequiredFieldValidator5"
                                                                runat="server" ControlToValidate="ddlNoCustomer" InitialValue="0" ErrorMessage="Please Select Number Of Users"
                                                                SetFocusOnError="True" ValidationGroup="Reg"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            ST Reg. No. :
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="input_box2" ID="txtstRegNo" onChange="capitalizeMe(this)"
                                                                runat="server" MaxLength="20"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            NTN No. :
                                                        </td>
                                                        <td>
                                                            <asp:TextBox CssClass="input_box2" ID="txtMtno" onChange="capitalizeMe(this)" runat="server"
                                                                MaxLength="20"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Business Type :
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlIndustry" Style="width: 270px" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="rfv2" runat="server" ControlToValidate="ddlIndustry"
                                                                ErrorMessage="Please Select Business Type" Display="None" SetFocusOnError="True"
                                                                InitialValue="0" ValidationGroup="Reg"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:CheckBox ID="chkTermAccept" runat="server" />
                                                            I agree to AccMaster <a href="Terms.aspx" target="_blank">Terms of Service And Privacy
                                                                Policy</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td style="">
                                                            <cc1:CaptchaControl ID="ccJoin" runat="server" CaptchaBackgroundNoise="none" CaptchaLength="6"
                                                                CaptchaHeight="50" CaptchaWidth="220" CaptchaLineNoise="None" CaptchaMinTimeout="5"
                                                                FontColor="Black" BackColor="#E4FCC8" CaptchaMaxTimeout="240" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            Retype The Text as shown above:
                                                            <div id="divCommentMsg" visible="false" runat="server" style="color: Red;">
                                                                Enter the letters as they are shown in the image below !!!
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr style="height: 30px">
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtCap" CssClass="input_box2" runat="server"> </asp:TextBox>
                                                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" Display="None"
                                                                ControlToValidate="txtCap" ErrorMessage="Please Enter letters as they are shown in the image above"
                                                                ValidationGroup="Reg">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <div class="start-button">
                                                                <%--  <a href="#"></a>--%>
                                                                <asp:LinkButton Text="Start Free Trial" ID="btnAdd" ValidationGroup="Reg" runat="server"
                                                                    OnClick="btnAdd_Click1"></asp:LinkButton>
                                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                                                    ShowSummary="false" ValidationGroup="Reg" />
                                                            </div>
                                                            <div class="secur-image-lock">
                                                                <img src="images/login_bg-sprite.png" alt="" />
                                                            </div>
                                                            <div id="vs" style="color: Red; display: none;">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="vertical-align: top; width: 223px; text-align: right; padding-right: 0px;
                                                border: solid 0px red; margin-right: 0px">
                                                <img src="images/Online_Registration.jpg" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="feat-right-part-bottom">
                                <img src="images/feat-right-part-bottom-bg.jpg" alt="" />
                            </div>
                        </div>
                    </div>
</asp:Content>
