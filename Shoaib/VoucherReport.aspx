﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="VoucherReport.aspx.cs" Inherits="VoucherReport" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Voucher Report"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
        width="50%">
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 100px">
               Voucher Type :
            </td>
            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                <asp:DropDownList ID="ddlReportType" runat="server">
                    <asp:ListItem Text="Select Voucher Type" Value="0"></asp:ListItem>
                    <asp:ListItem Text="CRV" Value="CRV"></asp:ListItem>
                    <asp:ListItem Text="CPV" Value="CPV"></asp:ListItem>
                    <asp:ListItem Text="BRV" Value="BRV"></asp:ListItem>
                    <asp:ListItem Text="BPV" Value="BPV"></asp:ListItem>
                    <asp:ListItem Text="JV" Value="JV"></asp:ListItem>
                    <asp:ListItem Text="CRVFC" Value="CRVFC"></asp:ListItem>
                    <asp:ListItem Text="CPVFC" Value="CPVFC"></asp:ListItem>
                    <asp:ListItem Text="BRVFC" Value="BRVFC"></asp:ListItem>
                    <asp:ListItem Text="BPVFC" Value="BPVFC"></asp:ListItem>
                    <asp:ListItem Text="JVFC" Value="JVFC"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                    InitialValue="0" ErrorMessage="Please Select Voucher Type" Text="*" ControlToValidate="ddlReportType"
                    ValidationGroup="AddCashBook"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;  tex">
                Starting Date :
            </td>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                <asp:TextBox ID="txtstartDate" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtstartDate'),this);"
                    runat="server" Width="100px"></asp:TextBox>
                <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                    onclick="scwShow(scwID('ctl00_MainHome_txtstartDate'),this);" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                    ErrorMessage="Please Fill Starting  Date." Text="*" ControlToValidate="txtstartDate"
                    ValidationGroup="AddCashBook"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                    ControlToValidate="txtstartDate" ValidationGroup="AddCashBook" ErrorMessage="Invalid Invoice Date !!"
                    Display="None"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                End Date :
            </td>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                <asp:TextBox ID="txtEndDate" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtEndDate'),this);"
                    runat="server" Width="100px"></asp:TextBox>
                <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                    onclick="scwShow(scwID('ctl00_MainHome_txtEndDate'),this);" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                    ErrorMessage="Please Fill  End  Date ." Text="*" ControlToValidate="txtEndDate"
                    ValidationGroup="AddCashBook"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                    ControlToValidate="txtEndDate" ValidationGroup="AddCashBook" ErrorMessage="Invalid Invoice Date !!"
                    Display="None"></asp:RegularExpressionValidator>
                       <asp:Button ID="btnGenerate" Text="Generate" ValidationGroup="AddCashBook" 
                    runat="server" OnClick="btnGenerate_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="AddCashBook" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
             
            </td>
        </tr>
        <%-- <tr>
                    <td colspan="2">
                        <asp:RadioButton ID="rdb" GroupName="reporttype" runat="server" Text="View" />
                        <asp:RadioButton ID="RadioButton1" GroupName="reporttype" runat="server" Text="Excel Formate" />
                        <asp:RadioButton ID="RadioButton2" GroupName="reporttype" runat="server" Text="Printable" />
                    </td>
                </tr>--%>
    </table>
</asp:Content>
