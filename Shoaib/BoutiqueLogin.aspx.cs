﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class BoutiqueLogin : System.Web.UI.Page
{
    User objUser = new User();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnLogin_Click(object sender, ImageClickEventArgs e)
    {
        if (txtUsername.Text != "" && txtPassword.Text != "")
        {
            DataSet ds = new DataSet();
            objUser.Username = txtUsername.Text.Trim().Replace("'", "").ToString();
            objUser.UserPassword = txtPassword.Text.Trim().Replace("'", "").ToString();
            objUser.CustomerCode = decimal.Parse(txtCustomerCode.Text);
            ds = objUser.GetBoutiqueLogin();
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblError.Visible = false;
                Response.Redirect("BoutiqueWelcome.aspx");
            }
            else
            {
                lblError.Visible = true;
            }
        }
    }
}