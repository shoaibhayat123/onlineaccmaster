﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintPeriodicStockTransactionSheet : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    clsCookie ck = new clsCookie();
    decimal TotalOpbal = 0;
    decimal TotaloldBalance = 0;
    decimal TotalOpeningQuantity = 0;
    decimal TotalPurchase = 0;
    decimal TotalProduction = 0;
    decimal TotalSalesReturn = 0;
    decimal TotalPurchaseAdjustment = 0;
    decimal AllTotalStockReceived = 0;
    decimal TotalSales = 0;
    decimal TotalConsumption = 0;
    decimal TotalPurchaseReturn = 0;
    decimal TotalSalesAdjustment = 0;
    decimal AllTotalStockSale = 0;
    decimal TotalClosingStock = 0;


    decimal AllTotalOpbal = 0;
    decimal AllTotaloldBalance = 0;
    decimal AllTotalOpeningQuantity = 0;
    decimal AllTotalPurchase = 0;
    decimal AllTotalProduction = 0;
    decimal AllTotalSalesReturn = 0;
    decimal AllTotalPurchaseAdjustment = 0;
    decimal CompleteTotalStockReceived = 0;
    decimal AllTotalSales = 0;
    decimal AllTotalConsumption = 0;
    decimal AllTotalPurchaseReturn = 0;
    decimal AllTotalSalesAdjustment = 0;
    decimal CompleteTotalStockSale = 0;
    decimal AllTotalClosingStock = 0;

    Reports objReports = new Reports();
    SetCustomers objCust = new SetCustomers();
    DataTable dttbl = new DataTable();
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {
            if (Request["From"].ToString() != "" || Request["To"].ToString() != "" || Request["AcfileId"].ToString() != "")
            {
                Session["s"] = null;
                bindSupplier();
                FillMainCategory();
                FillLogo();
                lblFromDate.Text = ClsGetDate.FillFromDate(Request["From"].ToString());
                lblToDate.Text = ClsGetDate.FillFromDate(Request["To"].ToString());
                lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
                lblUser.Text = Sessions.UseLoginName.ToString();

            }
        }
    }

    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }

    }
    #endregion

    #region Grid RowDataBound
    protected void grdMainCategory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnMainCategoryCode = ((HiddenField)e.Row.FindControl("hdnMainCategoryCode"));

            //objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            //objCust.MainCategoryCode = Convert.ToInt32(hdnMainCategoryCode.Value);
            //DataSet ds = new DataSet();
            //ds = objCust.GetSubCategoryByMainCategory();

            objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objReports.DateFrom = Convert.ToDateTime(Request["From"]);
            objReports.DateTo = Convert.ToDateTime(Request["To"]);
            objReports.MainCategoryCode = Convert.ToInt32(hdnMainCategoryCode.Value);
            DataSet ds = new DataSet();
            ds = objReports.GetSubCategoryPeriodicStock();

            GridView grdSubCategory = (GridView)e.Row.FindControl("grdSubCategory");
            if (ds.Tables[0].Rows.Count > 0)
            {

                grdSubCategory.DataSource = ds.Tables[0];
                grdSubCategory.DataBind();
            }

        }


    }

    protected void grdSubCategory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnMainCategoryCode2 = ((HiddenField)e.Row.FindControl("hdnMainCategoryCode2"));
            HiddenField hdnSubCategoryCode = ((HiddenField)e.Row.FindControl("hdnSubCategoryCode"));

            objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objReports.DateFrom = Convert.ToDateTime(Request["From"]);
            objReports.DateTo = Convert.ToDateTime(Request["To"]);
            objReports.MainCategoryCode = Convert.ToInt32(hdnMainCategoryCode2.Value);
            objReports.SubCategoryCode = Convert.ToInt32(hdnSubCategoryCode.Value);

            DataSet ds = new DataSet();
            ds = objReports.getPeriodicStock();

            GridView grdLedger = (GridView)e.Row.FindControl("grdLedger");
            if (ds.Tables[0].Rows.Count > 0)
            {
                 TotalOpbal = 0;
                 TotaloldBalance = 0;
                 TotalOpeningQuantity = 0;
                 TotalPurchase = 0;
                 TotalProduction = 0;
                 TotalSalesReturn = 0;
                 TotalPurchaseAdjustment = 0;
                 AllTotalStockReceived = 0;
                 TotalSales = 0;
                 TotalConsumption = 0;
                 TotalPurchaseReturn = 0;
                 TotalSalesAdjustment = 0;
                 AllTotalStockSale = 0;
                 TotalClosingStock = 0;
                 grdLedger.DataSource = ds.Tables[0];
                 grdLedger.DataBind();
            }

        }

        if (e.Row.RowType == DataControlRowType.Footer)  // For footer totals
        {


        }
    }

    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblOpeningQuantity = ((Label)e.Row.FindControl("lblOpeningQuantity"));
            Label lblPurchase = ((Label)e.Row.FindControl("lblPurchase"));

            Label lblProduction = ((Label)e.Row.FindControl("lblProduction"));
            Label lblSalesReturn = ((Label)e.Row.FindControl("lblSalesReturn"));

            Label lblPurchaseAdjustment = ((Label)e.Row.FindControl("lblPurchaseAdjustment"));
            Label lblTotalStockReceived = ((Label)e.Row.FindControl("lblTotalStockReceived"));

            Label lblSales = ((Label)e.Row.FindControl("lblSales"));
            Label lblConsumption = ((Label)e.Row.FindControl("lblConsumption"));

            Label lblPurchaseReturn = ((Label)e.Row.FindControl("lblPurchaseReturn"));
            Label lblSalesAdjustment = ((Label)e.Row.FindControl("lblSalesAdjustment"));

            Label lblTotalStockSale = ((Label)e.Row.FindControl("lblTotalStockSale"));
            Label lblClosingStock = ((Label)e.Row.FindControl("lblClosingStock"));

            decimal Opbal = 0;
            decimal oldBalance = 0;
            decimal OpeningQuantity = 0;
            decimal Purchase = 0;
            decimal Production = 0;
            decimal SalesReturn = 0;
            decimal PurchaseAdjustment = 0;
            decimal TotalStockReceived = 0;
            decimal Sales = 0;
            decimal Consumption = 0;
            decimal PurchaseReturn = 0;
            decimal SalesAdjustment = 0;
            decimal TotalStockSale = 0;
            decimal ClosingStock = 0;
            decimal oldBalanceInvoice = 0;
            decimal oldBalanceProdNCons = 0;
            decimal oldBalanceTransferSummary = 0;
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Opbal")) != "")
            {
                Opbal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Opbal"));
               
            }
            //if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "oldBalance")) != "")
            //{
            //    oldBalance = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "oldBalance"));

            //}
            //OpeningQuantity = Opbal + oldBalance;

            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "oldBalanceInvoice")) != "")
            {
                oldBalanceInvoice = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "oldBalanceInvoice"));

            }
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "oldBalanceProdNCons")) != "")
            {
                oldBalanceProdNCons = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "oldBalanceProdNCons"));

            }
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "oldBalanceTransferSummary")) != "")
            {
                oldBalanceTransferSummary = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "oldBalanceTransferSummary"));

            }


           
            OpeningQuantity = Opbal + oldBalanceInvoice + oldBalanceProdNCons + oldBalanceTransferSummary;
            TotalOpeningQuantity += OpeningQuantity;
            lblOpeningQuantity.Text = String.Format("{0:C}", OpeningQuantity).Replace('$', ' ');



            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Purchase")) != "")
            {
                Purchase = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Purchase"));
                TotalPurchase += Purchase;
                AllTotalPurchase += Purchase;
            }
            lblPurchase.Text = String.Format("{0:C}", Purchase).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PurchaseReturn")) != "")
            {
                PurchaseReturn = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PurchaseReturn"));
                TotalPurchaseReturn += PurchaseReturn;
                AllTotalPurchaseReturn += PurchaseReturn;
            }
            lblPurchaseReturn.Text = String.Format("{0:C}", PurchaseReturn).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Production")) != "")
            {
                Production = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Production"));
                TotalProduction += Production;
                AllTotalProduction += Production;
            }
            lblProduction.Text = String.Format("{0:C}", Production).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
          
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PurchaseAdjustment")) != "")
            {
                PurchaseAdjustment = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PurchaseAdjustment"));
                TotalPurchaseAdjustment += PurchaseAdjustment;
                AllTotalPurchaseAdjustment += PurchaseAdjustment;
            }
            lblPurchaseAdjustment.Text = String.Format("{0:C}", PurchaseAdjustment).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            TotalStockReceived = Purchase + PurchaseReturn+ Production + PurchaseAdjustment;
            AllTotalStockReceived += TotalStockReceived;
            CompleteTotalStockReceived += TotalStockReceived;
            lblTotalStockReceived.Text = String.Format("{0:C}", TotalStockReceived).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Sales")) != "")
            {
                Sales = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Sales"));
                TotalSales += Sales;
                AllTotalSales += Sales;

            }
            lblSales.Text = String.Format("{0:C}", Sales).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

         

            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SalesReturn")) != "")
            {
                SalesReturn = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "SalesReturn"));
                TotalSalesReturn += SalesReturn;
                AllTotalSalesReturn += SalesReturn;
            }
            lblSalesReturn.Text = String.Format("{0:C}", SalesReturn).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Consumption")) != "")
            {
                Consumption = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Consumption"));
                TotalConsumption += Consumption;
                AllTotalConsumption += Consumption;
            }
            lblConsumption.Text = String.Format("{0:C}", Consumption).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SalesAdjustment")) != "")
            {
                SalesAdjustment = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "SalesAdjustment"));
                TotalSalesAdjustment += SalesAdjustment;
                AllTotalSalesAdjustment += SalesAdjustment;
            }
            lblSalesAdjustment.Text = String.Format("{0:C}", SalesAdjustment).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            TotalStockSale =Sales + SalesReturn + Consumption  + SalesAdjustment;
            AllTotalStockSale += TotalStockSale;
            CompleteTotalStockSale += TotalStockSale;
            lblTotalStockSale.Text = String.Format("{0:C}", TotalStockSale).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            ClosingStock = TotalStockReceived + TotalStockSale + OpeningQuantity;
            TotalClosingStock += ClosingStock;
            AllTotalClosingStock += ClosingStock;

            //if (ClosingStock >= 0)
            //{
            //    lblClosingStock.Text = String.Format("{0:C}", ClosingStock).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            //}
            //else
            //{
            //    lblClosingStock.Text = "-"+String.Format("{0:C}", ClosingStock).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            //}
           
                lblClosingStock.Text = String.Format("{0:C}", ClosingStock).Replace('$', ' ');
            

        }

        if (e.Row.RowType == DataControlRowType.Footer)  // For footer totals
        {
            Label TotallblOpeningQuantity = ((Label)e.Row.FindControl("TotallblOpeningQuantity"));
            Label TotallblPurchase = ((Label)e.Row.FindControl("TotallblPurchase"));

            Label TotallblProduction = ((Label)e.Row.FindControl("TotallblProduction"));
            Label TotallblSalesReturn = ((Label)e.Row.FindControl("TotallblSalesReturn"));

            Label TotallblPurchaseAdjustment = ((Label)e.Row.FindControl("TotallblPurchaseAdjustment"));
            Label TotallblTotalStockReceived = ((Label)e.Row.FindControl("TotallblTotalStockReceived"));

            Label TotallblSales = ((Label)e.Row.FindControl("TotallblSales"));
            Label TotallblConsumption = ((Label)e.Row.FindControl("TotallblConsumption"));

            Label TotallblPurchaseReturn = ((Label)e.Row.FindControl("TotallblPurchaseReturn"));
            Label TotallblSalesAdjustment = ((Label)e.Row.FindControl("TotallblSalesAdjustment"));

            Label TotallblTotalStockSale = ((Label)e.Row.FindControl("TotallblTotalStockSale"));
            Label TotallblClosingStock = ((Label)e.Row.FindControl("TotallblClosingStock"));

           
            TotallblPurchase.Text = String.Format("{0:C}", TotalPurchase).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            TotallblProduction.Text = String.Format("{0:C}", TotalProduction).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            TotallblSalesReturn.Text = String.Format("{0:C}", TotalSalesReturn).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            TotallblPurchaseAdjustment.Text = String.Format("{0:C}", TotalPurchaseAdjustment).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            TotallblTotalStockReceived.Text = String.Format("{0:C}", AllTotalStockReceived).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            TotallblSales.Text = String.Format("{0:C}", TotalSales).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            TotallblConsumption.Text = String.Format("{0:C}", TotalConsumption).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            TotallblPurchaseReturn.Text = String.Format("{0:C}", TotalPurchaseReturn).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            TotallblSalesAdjustment.Text = String.Format("{0:C}", TotalPurchaseAdjustment).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            TotallblTotalStockSale.Text = String.Format("{0:C}", AllTotalStockSale).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            //if (TotalClosingStock >= 0)
            //{

            //    TotallblClosingStock.Text = String.Format("{0:C}", TotalClosingStock).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            //}
            //else
            //{
            //    TotallblClosingStock.Text = "-" + String.Format("{0:C}", TotalClosingStock).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            //}

            //if (TotalOpeningQuantity >= 0)
            //{
            //    TotallblOpeningQuantity.Text = String.Format("{0:C}", TotalOpeningQuantity).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            //}
            //else
            //{
            //    TotallblOpeningQuantity.Text ="-"+ String.Format("{0:C}", TotalOpeningQuantity).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            //}

           

                TotallblClosingStock.Text = String.Format("{0:C}", TotalClosingStock).Replace('$', ' ');
           

           
                TotallblOpeningQuantity.Text = String.Format("{0:C}", TotalOpeningQuantity).Replace('$', ' ');
           
        }
    }
    #endregion





    #region Fill Main Category  Setup
    protected void FillMainCategory()
    {

        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.DateFrom = Convert.ToDateTime(Request["From"]);
        objReports.DateTo = Convert.ToDateTime(Request["To"]);
        DataSet ds = new DataSet();
        ds = objReports.GetMainCategoryPeriodicStock();

        //objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        //objCust.MainCategoryCode = 0;
        //DataSet ds = new DataSet();
        //ds = objCust.GetMainCategory();

        if (ds.Tables[0].Rows.Count > 0)
        {

            grdMainCategory.DataSource = ds.Tables[0];
            grdMainCategory.DataBind();

        }


    }
    #endregion
}

