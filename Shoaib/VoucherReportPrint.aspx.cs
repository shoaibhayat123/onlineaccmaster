﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class VoucherReportPrint : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();

    AccFiles objFile = new AccFiles();
    Reports objReports = new Reports();
   
    decimal TotalDebit = 0;
    decimal TotalCredit = 0;
    decimal TotalDateDebit = 0;
    decimal TotalDateCredit = 0;   
    decimal ToatlVoucherDebit = 0;
    decimal ToatlVoucherCredit = 0;
    //public string attachment = "attachment; filename=Dresses27_Report_" + DateTime.Now.ToString("dd_MM_yyyy") + ".xls";
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if ((Sessions.CustomerCode == "") || (Request["From"].ToString() == "") || (Request["To"].ToString() == "") || (Request["VoucherType"].ToString() == ""))
        {
            Response.Redirect("default.aspx");
        }
        if (!IsPostBack)
        {


            bindSupplier();
            bindCashBook();
            lblFromDate.Text = ClsGetDate.FillFromDate(Request["From"].ToString());
            lblToDate.Text = ClsGetDate.FillFromDate(Request["To"].ToString());
            bindBuyer();
            FillLogo();
            lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
            lblUser.Text = Sessions.UseLoginName.ToString();
            bindrptrvoucherReport();
            lblvoucherDebit.Text = String.Format("{0:C}", ToatlVoucherDebit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            lblvoucherCredit.Text = String.Format("{0:C}", ToatlVoucherCredit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            //PrepareGridViewForExport(grdLedger);
            //ExportGridView();


        }
    }

    protected void bindBuyer()
    {
        //objFile.id = Convert.ToInt32(Request["Client"].ToString()); ;
        //objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        //DataTable dtBuyer = new DataTable();
        //dtBuyer = objFile.getMenueDetail();


        //if (dtBuyer.Rows.Count > 0)
        //{
        //    lblBuyerName.Text = Convert.ToString(dtBuyer.Rows[0]["Title"]);
        //    lblBuyerAddress.Text = Convert.ToString(dtBuyer.Rows[0]["Address1"]);
        //    if (Convert.ToString(dtBuyer.Rows[0]["Address2"]) != "")
        //    {
        //        lblBuyerAddress2.Text = "," + Convert.ToString(dtBuyer.Rows[0]["Address2"]);
        //    }
        //    if (Convert.ToString(dtBuyer.Rows[0]["Address3"]) != "")
        //    {
        //        lblBuyerAddress3.Text = "," + Convert.ToString(dtBuyer.Rows[0]["Address3"]);
        //    }
        //}


    }


    #region Bind ledger grid
    protected void bindCashBook()
    {
        //objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);

        //objReports.DateFrom = Convert.ToDateTime(Request["from"]);
        //objReports.DateTo = Convert.ToDateTime(Request["to"]);
        //objReports.AcFileId = Convert.ToInt32(Request["client"]);
        //objReports.DataFrom = "CashBook";
        //DataSet ds = new DataSet();
        //ds = objReports.Getcashbankbookreport();


        /////********** Add new row to result for Opening Balance**********/
        //DataTable dtCashBookSimpleDetail = new DataTable();
        //objSalesInvoice.AcFileId = Convert.ToInt32(Request["client"]);
        //objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        //objSalesInvoice.CashBookDate = Convert.ToDateTime(Request["from"]);
        //DataSet ds1 = new DataSet();
        //ds1 = objSalesInvoice.getBrodForwordBal();
        //decimal tottrans = 0;
        //decimal OpeningBal = 0;
        //if (Convert.ToString(ds1.Tables[0].Rows[0]["OpeningBal"]) != "")
        //{
        //    OpeningBal = Convert.ToDecimal(ds1.Tables[0].Rows[0]["OpeningBal"]);
        //}
        //if (Convert.ToString(ds1.Tables[0].Rows[0]["tottrans"]) != "")
        //{
        //    tottrans = Convert.ToDecimal(ds1.Tables[0].Rows[0]["tottrans"]);
        //}
        //bfamount = OpeningBal + tottrans;

        //DataRow dr = ds.Tables[0].NewRow(); //table 2 for detail trans
        //dr["Description"] = "Opening Balance";
        //dr["TransAmt"] = bfamount;

        //ds.Tables[0].Rows.InsertAt(dr, 0);

        //grdLedger.DataSource = ds.Tables[0];
        //grdLedger.DataBind();


    }

    #endregion
    #region Grid RowDataBound
    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    Label lbltransDate = ((Label)e.Row.FindControl("lbltransDate"));
        //    string transDate = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "CashBookDate"));
        //    if (DataBinder.Eval(e.Row.DataItem, "CashBookDate").ToString() != "")
        //    {
        //        lbltransDate.Text = ClsGetDate.FillFromDate(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "CashBookDate")));
        //    }

        //    Label lblBalance = ((Label)e.Row.FindControl("lblBalance"));
        //    if (e.Row.RowIndex.ToString() == "0")
        //    {
        //        balance += bfamount;
        //    }
        //    else
        //    {
        //        balance += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Receive")) - Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Payment"));
        //        ToatlReceive += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Receive"));
        //        TotalPayment += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Payment"));
        //    }
        //    lblBalance.Text = String.Format("{0:C}", balance).Replace('-', ' ').Replace('$', ' ').Replace('(', '-').Replace(')', ' ');


        //}
        //if (e.Row.RowType == DataControlRowType.Footer)
        //{
        //    Label lblTotalReceive = ((Label)e.Row.FindControl("lblTotalReceive"));
        //    Label lblTotalPayment = ((Label)e.Row.FindControl("lblTotalPayment"));

        //    lblTotalReceive.Text = String.Format("{0:C}", ToatlReceive).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        //    lblTotalPayment.Text = String.Format("{0:C}", TotalPayment).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        //}


    }


    #endregion

    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }

    }
    #endregion
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }


    protected void rptrvoucherReport_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Label lblVucherDate = ((Label)e.Item.FindControl("lblVucherDate"));
        lblVucherDate.Text = ClsGetDate.FillFromDate(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "TransactionDate")));
        bindrptrVoucherMain(Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "TransactionDate") ));
        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.Date = Convert.ToDateTime(DataBinder.Eval(e.Item.DataItem, "TransactionDate"));
        objReports.DataFrom = Convert.ToString(Request["VoucherType"]);
        DataSet ds = new DataSet();
        ds = objReports.getTransInvoiceSummaryId();

        TotalDateDebit = 0;
        TotalDateCredit = 0;
        Repeater rptrVoucherMain = (Repeater)e.Item.FindControl("rptrVoucherMain");
        if (ds.Tables[0].Rows.Count > 0)
        {
            rptrVoucherMain.DataSource = ds.Tables[0];
            rptrVoucherMain.DataBind();
        }

        Label lblDateDebit = ((Label)e.Item.FindControl("lblDateDebit"));
        lblDateDebit.Text = String.Format("{0:C}", TotalDateDebit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        Label lblDateCredit = ((Label)e.Item.FindControl("lblDateCredit"));
        lblDateCredit.Text = String.Format("{0:C}", TotalDateCredit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        
    
    }
    protected void rptrVoucherMain_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        DataGrid dgGallery = ((DataGrid)e.Item.FindControl("dgGallery"));

        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.InvoiceSummaryId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "InvoiceSummaryId"));
        objReports.DataFrom = Convert.ToString(Request["VoucherType"]);
        DataSet ds = new DataSet();
        TotalDebit  = 0;
        TotalCredit = 0;
        ds = objReports.getVoucherMainDetail();
        if (ds.Tables[0].Rows.Count > 0)
        {
            dgGallery.DataSource = ds.Tables[0];
            dgGallery.DataBind();
        }

    }
    protected void bindrptrvoucherReport()
    {
        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.DateFrom = Convert.ToDateTime(Request["from"]);
        objReports.DateTo = Convert.ToDateTime(Request["to"]);
        objReports.DataFrom = Convert.ToString(Request["VoucherType"]);      
        DataSet ds = new DataSet();
        ds = objReports.getAccountsDate();
        if (ds.Tables[0].Rows.Count > 0)
        {
            rptrvoucherReport.DataSource = ds.Tables[0];
            rptrvoucherReport.DataBind();
        }
    }
    protected void bindrptrVoucherMain( DateTime Date)
    {
       

    }

    protected void dgGallery_OnItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt")) > 0)
            {
                TotalDebit +=  Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt"));
                TotalDateDebit += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt"));
                ToatlVoucherDebit += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt"));
            }
            else
            {
                TotalCredit += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt"));
                TotalDateCredit += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt"));
                ToatlVoucherCredit += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt"));
            }

        }

        if (e.Item.ItemType == ListItemType.Footer)
        {
            Label lblTotalDebit = ((Label)e.Item.FindControl("lblTotalDebit"));
            lblTotalDebit.Text = String.Format("{0:C}", TotalDebit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            Label lblTotalCredit = ((Label)e.Item.FindControl("lblTotalCredit"));
            lblTotalCredit.Text = String.Format("{0:C}", TotalCredit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        }

    }
    
}
