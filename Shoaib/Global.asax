﻿<%@ Application Language="C#" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup

    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

   

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started
              //Session.Timeout = 120;
    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }



    protected void Application_BeginRequest(object sender, EventArgs e)
    {
        string strURL = Context.Request.Url.ToString();
        string strNewURL;


        if (strURL.ToLower().Contains("http://online.accmaster.com"))
        {
            strNewURL = strURL.Replace("http://online.accmaster.com", "https://www.online.accmaster.com");
            Response.Redirect(strNewURL);
        }
        else if (strURL.ToLower().Contains("http://www.online.accmaster.com"))
        {
            strNewURL = strURL.Replace("http://www.online.accmaster.com", "https://www.online.accmaster.com");
            Response.Redirect(strNewURL);
        }
        else
            if (strURL.ToLower().Contains("https://online.accmaster.com"))
        {
            strNewURL = strURL.Replace("https://online.accmaster.com", "https://www.online.accmaster.com");
            Response.Redirect(strNewURL);
        }

    }



    void Application_Error(object sender, EventArgs e)
    {

        //Exception exc = Server.GetLastError();
        //string msg = exc.ToString();

        //string[] strArray = new string[1];
        //strArray[0] = msg;
        //try
        //{
        //    EmailSender objMail = new EmailSender();
        //    objMail.ValueArray = strArray;
        //    objMail.MailTo = "support@accmaster.com";
        //    objMail.Subject = "Error Report";
        //    objMail.MailBcc = "naveen@ubwebs.com";
        //    objMail.TemplateDoc = "ErrorMsg.htm";
        //    objMail.Send();

        //}
        //catch
        //{

        //}
        //Server.Transfer("Exception.aspx");

    }

       
</script>

