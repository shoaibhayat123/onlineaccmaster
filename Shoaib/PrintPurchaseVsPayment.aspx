﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintPurchaseVsPayment.aspx.cs" Inherits="PrintPurchaseVsPayment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Tahoma; background: none; font-size: 10px;">
    <form id="form1" runat="server">
      <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; font-weight: bold;">
            <tr id="trTrialBal" runat="server">
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Purchase
                        Vs Payment Report<br />
                    </span><span style="font-size: 14px;">For the Year :
                        <asp:Label ID="lblFromDate" runat="server" Style="padding-right: 15px;"></asp:Label>
                    </span>
                    <asp:Label ID="lblBuyerName" runat="server"></asp:Label><br />
                    <span style="font-size: 12px; font-weight: normal">
                        <asp:Label ID="lblBuyerAddress" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress2" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress3" runat="server"></asp:Label></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <hr />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <b style="float: left; font-size: 14px; text-decoration: underline">Customer Wise</b><br />
        <br />
        <asp:GridView ID="grdLedger" Width="800px" runat="server" ShowFooter="True" AutoGenerateColumns="False"
            AllowPaging="true" PageSize="500000" PagerSettings-Visible="false" PagerSettings-Mode="Numeric"
            HeaderStyle-CssClass="gridheader" FooterStyle-CssClass="gridFooter" AlternatingRowStyle-CssClass="gridAlternateItem"
            GridLines="none" BorderColor="black" BorderStyle="Dotted" BorderWidth="1" OnRowDataBound="dgGallery_RowDataBound"
            RowStyle-CssClass="gridItem">
            <EmptyDataTemplate>
                <div style="padding-bottom: 10px; width: 100%; text-align: center; font-family: Gisha;
                    font-size: 20px; background-color: #E1F5FD; color: #110F0F;">
                    <div style="padding-top: 10px; padding-bottom: 5px;">
                        No record found
                    </div>
                </div>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="Title Of Account">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" CssClass=" gridpadding" />
                    <FooterStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        <%#Eval("Title")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total Purchase ">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" CssClass=" gridpadding" Width="15%" />
                    <FooterStyle HorizontalAlign="right" />
                    <ItemTemplate>
                        <asp:Label ID="lblSalesAmt" runat="server"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblSumSales" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="% of Purchase">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" CssClass=" gridpadding" Width="10%" />
                    <FooterStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <asp:Label ID="lblAgeOfSale" runat="server"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblSumAgeOfSale" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Payment">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" CssClass=" gridpadding" Width="15%" />
                    <FooterStyle HorizontalAlign="right" />
                    <ItemTemplate>
                        <asp:Label ID="lblRecoveryAmt" runat="server"></asp:Label>
                        <%--   <%#Eval("TotalRecoveryAmt")%>--%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblSumRecovery" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="% of Payment">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" CssClass=" gridpadding" Width="10%" />
                    <FooterStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <asp:Label ID="lblAgeOfRecovery" runat="server"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblSumAgeOfRecovery" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <br />
        <br />
        <b style="float: left; font-size: 14px; text-decoration: underline">Year Wise</b><br />
        <br />
        <asp:GridView ID="grdMonth" Width="800px" runat="server" ShowFooter="True" AutoGenerateColumns="False"
            AllowPaging="true" PageSize="500000" PagerSettings-Visible="false" PagerSettings-Mode="Numeric"
            HeaderStyle-CssClass="gridheader" FooterStyle-CssClass="gridFooter" AlternatingRowStyle-CssClass="gridAlternateItem"
            GridLines="none" BorderColor="black" BorderStyle="Dotted" BorderWidth="1" OnRowDataBound="grdMonth_RowDataBound"
            RowStyle-CssClass="gridItem">
            <EmptyDataTemplate>
                <div style="padding-bottom: 10px; width: 100%; text-align: center; font-family: Gisha;
                    font-size: 20px; background-color: #E1F5FD; color: #110F0F;">
                    <div style="padding-top: 10px; padding-bottom: 5px;">
                        No record found
                    </div>
                </div>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="Month">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" CssClass=" gridpadding" />
                    <FooterStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        <asp:Label ID="lblMonth" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total Purchase ">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" CssClass=" gridpadding" Width="15%" />
                    <FooterStyle HorizontalAlign="right" />
                    <ItemTemplate>
                        <asp:Label ID="lblSalesAmt" runat="server"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblSumSales" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="% of Purchase">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" CssClass=" gridpadding" Width="10%" />
                    <FooterStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <asp:Label ID="lblAgeOfSale" runat="server"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblSumAgeOfSale" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Recovery">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" CssClass=" gridpadding" Width="15%" />
                    <FooterStyle HorizontalAlign="right" />
                    <ItemTemplate>
                        <asp:Label ID="lblRecoveryAmt" runat="server"></asp:Label>
                        <%--   <%#Eval("TotalRecoveryAmt")%>--%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblSumRecovery" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="% of Payment">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" CssClass=" gridpadding" Width="10%" />
                    <FooterStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <asp:Label ID="lblAgeOfRecovery" runat="server"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblSumAgeOfRecovery" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div style="clear: both">
        <asp:Chart ID="Chart1" runat="server" Height="400px" Width="800px">
            <Legends>
                <asp:Legend BackColor="GreenYellow">
                </asp:Legend>
            </Legends>
            <Titles>
                <asp:Title Name="Naveen" Text="Purchase V/s Payment" TextStyle="Default">
                </asp:Title>
            </Titles>
            <Series>
                <asp:Series Name="Series1" ChartType="Column" ChartArea="ChartArea1">
                </asp:Series>
                <asp:Series Name="Series2" ChartType="Column" ChartArea="ChartArea1">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" BackColor="#DDEEDC">
                    <AxisX Title="Months" IsLabelAutoFit="False" Interval="1">
                        <LabelStyle Format="MMM" />
                    </AxisX>
                    <AxisY Title="Amount">
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
             <tr>
                <td style="text-align: right">
                    <asp:LinkButton Text="Print" ID="lnkPrint" OnClientClick="window.print();" runat="server"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>
                                <script type="text/javascript">

                                    var currentTime = new Date()
                                    var hours = currentTime.getHours()
                                    var Newhours = 0;
                                    var minutes = currentTime.getMinutes()
                                    if (minutes < 10) {
                                        minutes = "0" + minutes
                                    }
                                    if (hours > 12) {
                                        Newhours = hours - 12;
                                    }
                                    else {
                                        Newhours = hours;
                                    }
                                    document.write(Newhours + ":" + minutes + " ")
                                    if (hours > 11) {
                                        document.write("PM")
                                    } else {
                                        document.write("AM")
                                    }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
