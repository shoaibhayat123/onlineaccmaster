﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="JournalVouchersFc.aspx.cs" Inherits="JournalVouchersFc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Journal Voucher"></asp:Label>
    <script type="text/javascript">
        function Count(text, long) {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }
        function ChangeFcAmount(obj) {



            if (obj.value != null && !obj.value.toString().match(/^[-]?\d*\.?\d*$/) || obj.value == "") {
                obj.value = "0.00";
                document.getElementById('<%=txtAmount.ClientID %>').value = "0.00";
                alert("Please Enter Numeric Value");

            }
            else {

                if (parseFloat(obj.value) <= 0) {
                    obj.value = "0.00";
                    alert("Invalid amount");
                }

                var FcAmount = document.getElementById('<%=txtFcAmount.ClientID %>').value;

                var FcRate = document.getElementById('<%=txtFcRate.ClientID %>').value;

                var Amount = FcAmount * FcRate;

                var result = Math.round(Amount * 100) / 100
                document.getElementById('<%=txtAmount.ClientID %>').value = result;

            }

        }

        function ChangeCreditFcAmount(obj) {
            if (obj.value != null && !obj.value.toString().match(/^[-]?\d*\.?\d*$/) || obj.value == "") {
                obj.value = "0.00";
                document.getElementById('<%=txtcreditamount.ClientID %>').value = "0.00";
                alert("Please Enter Numeric Value");
            }
            else {

                if (parseFloat(obj.value) <= 0) {
                    obj.value = "0.00";
                    alert("Invalid amount");
                }

                var FcAmount = document.getElementById('<%=txtCreditFcAmount.ClientID %>').value;

                var FcRate = document.getElementById('<%=txtCreditFcRate.ClientID %>').value;

                var Amount = FcAmount * FcRate;

                var result = Math.round(Amount * 100) / 100
                document.getElementById('<%=txtcreditamount.ClientID %>').value = result;

            }

        }
        
    </script>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);

        function BeginRequestHandler(sender, args) {

            if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Save") {

                document.getElementById('<%= ImageButton2.ClientID %>').value = "Saving...";
                args.get_postBackElement().disabled = true;
            }
            if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Update") {

                document.getElementById('<%= btnAddNew.ClientID %>').value = "Updating...";
                args.get_postBackElement().disabled = true;
            }


        }
        function SaveClick() {
            document.getElementById('<%= hdnButtonText.ClientID %>').value = "Save";

        }

        function UpdateClick() {
            document.getElementById('<%= hdnButtonText.ClientID %>').value = "Update";

        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
          <asp:HiddenField ID="hdnButtonText" runat="server" />
            <asp:HiddenField ID="hdnVoucherSimpleMainId" runat="server" />
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                width="100%">
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                        Voucher # :
                    </td>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 5px; text-align: left">
                        <table>
                            <tr>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 0px;">
                                    <asp:TextBox ID="txtVoucherNo" TabIndex="1" onChange="isNumericPositive(this)" CssClass="input_box33"
                                        Style="float: left; text-transform: uppercase;" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill  Voucher #" Text="*" ControlToValidate="txtVoucherNo"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <span id="spanPgging" runat="server" style="padding: 0 20px 0 20px; font-weight: bold;
                                        font-size: 18px">
                                        <asp:LinkButton ID="lnkFirst" Text="|<" runat="server" OnClick="lnkFirst_Click"> </asp:LinkButton>
                                        <asp:LinkButton ID="lnkPrevious" Style="margin-left: 10px;" Text="<" runat="server"
                                            OnClick="lnkPrevious_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkNext" Text=">" Style="margin-left: 10px;" runat="server" OnClick="lnkNext_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnLast" Text=">|" Style="margin-left: 10px;" runat="server" OnClick="lnLast_Click"></asp:LinkButton></span>
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    Voucher Date :
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    <asp:TextBox ID="txtVoucherDate" TabIndex="2" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtVoucherDate'),this);"
                                        runat="server" Width="100px"></asp:TextBox>
                                    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                        onclick="scwShow(scwID('ctl00_MainHome_txtVoucherDate'),this);" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill Voucher Date." Text="*" ControlToValidate="txtVoucherDate"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                        ControlToValidate="txtVoucherDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid Voucher Date !!"
                                        Display="None"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="sundryDR" runat="server" style="height: 380px; width: 880px; padding: 10px;
                            overflow: auto; border: solid 2px  #528627">
                            <table>
                                <tr>
                                    <td style="margin-right: 10px; width: 150px;">
                                        A/C Title :
                                    </td>
                                    <td style="margin-right: 10px;" colspan="3">
                                        <asp:DropDownList ID="ddlAcdesc" TabIndex="3" Style="width: 550px" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Display="Dynamic"
                                            InitialValue="0" ErrorMessage="Please Select A/C Title" Text="*" ControlToValidate="ddlAcdesc"
                                            ValidationGroup="AddVoucherDetail"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;">
                                        Debit F.C. Amount :
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtFcAmount" runat="server" TabIndex="4" Text="0.00" onChange="ChangeFcAmount(this);"
                                            Style="width: 150px;" MaxLength="10"></asp:TextBox>
                                    </td>
                                    <td style="margin-right: 10px; width: 150px;">
                                        Debit F.C. Rate :
                                    </td>
                                    <td style="margin-right: 10px; padding-right: 30px">
                                        <asp:TextBox ID="txtFcRate" runat="server" TabIndex="5" Text="0.00" MaxLength="15"
                                            onChange="ChangeFcAmount(this);" Style="width: 150px; text-transform: uppercase"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;">
                                        Credit F.C. Amount :
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtCreditFcAmount" runat="server" TabIndex="6" Text="0.00" onChange="ChangeCreditFcAmount(this);"
                                            Style="width: 150px;" MaxLength="10"></asp:TextBox>
                                    </td>
                                    <td style="margin-right: 10px; width: 80px;">
                                        Credit F.C. Rate :
                                    </td>
                                    <td style="margin-right: 10px; padding-right: 30px">
                                        <asp:TextBox ID="txtCreditFcRate" runat="server" TabIndex="7" Text="0.00" MaxLength="15"
                                            onChange="ChangeCreditFcAmount(this);" Style="width: 150px; text-transform: uppercase"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;">
                                        Debit Amount :
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtAmount" Text="0.00" TabIndex="8" runat="server" onChange="isNumericDecimal(this)"
                                            Style="width: 150px;" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill Debit Amount " Text="*" ControlToValidate="txtAmount"
                                            ValidationGroup="AddVoucherDetail"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="margin-right: 10px;">
                                        Credit Amount :
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtcreditamount" Text="0.00" TabIndex="9" runat="server" onChange="isNumericDecimal(this)"
                                            Style="width: 150px;" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill  Credit Amount " Text="*" ControlToValidate="txtcreditamount"
                                            ValidationGroup="AddVoucherDetail"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px; vertical-align: top">
                                        Description :
                                    </td>
                                    <td style="margin-right: 10px;" colspan="3">
                                        <asp:TextBox ID="txtDescriptionDetail" TabIndex="10" runat="server" onKeyUp="Count(this,250)"
                                            onChange="Count(this,250);capitalizeMe(this)" TextMode="MultiLine" Style="width: 550px;"
                                            Rows="2" MaxLength="250"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill Description Detail " Text="*" ControlToValidate="txtDescriptionDetail"
                                            ValidationGroup="AddVoucherDetail"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" style="text-align: right">
                                        <asp:Button Style="text-align: right" ID="btnAddNew" TabIndex="11" OnClick="btnAddNew_Click" OnClientClick="UpdateClick()"
                                            runat="server" Text="Update" ValidationGroup="AddVoucherDetail" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                            ShowSummary="false" ValidationGroup="AddVoucherDetail" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <asp:HiddenField ID="hdndetail" runat="server" />
                                        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                                            AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                            AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                            BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery_RowDataBound"
                                            OnItemCommand="dgGallery_ItemCommand">
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="S.No.">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataSetIndex + 1%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="A/C Code">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Eval("AccountId")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="A/C Title">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="15%" HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        <%# Eval("AccountDesc")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="F.C. Debit Amount">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("FCAmountDebit")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="F.C. Debit Rate">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Right" Width="8%" />
                                                    <ItemTemplate>
                                                        <%# Convert.ToDecimal(Eval("FCRateDebit")).ToString("0.0000")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="F.C. Credit Amount">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("FCAmountCredit")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="F.C. Credit Rate">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Right" Width="8%" />
                                                    <ItemTemplate>
                                                        <%# Convert.ToDecimal(Eval("FCRateCredit")).ToString("0.0000")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Debit Amount">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("DebitAmount")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Credit Amount">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("CreditAmount")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Description">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="20%" HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        <%# Eval("Description")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="imgDel1" runat="server" Text="Delete" CommandArgument='<%#Eval("Id")%>'
                                                            CommandName="Delete" OnClientClick="return askDeleteRow();" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Edit">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="ImageButton1" runat="server" CommandArgument='<%#Eval("Id")%>'
                                                            Text="Edit" CommandName="Edit" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: left; width: 660px;">
                        <table style="width: 800px;">
                            <tr>
                                <td>
                                    Totals :: &nbsp;&nbsp;&nbsp;&nbsp; <b>Debit Amount:</b><asp:Label ID="lblDebitAmount"
                                        Text="0.00" runat="server" Style="margin-right: 20px; padding-left: 5px"></asp:Label>
                                    &nbsp;&nbsp; <b>Credit Amount:</b><asp:Label ID="lblCreditAmount" Text="0.00" runat="server"
                                        Style="margin-right: 20px; padding-left: 5px"></asp:Label>
                                    &nbsp;&nbsp; <b>Difference Amount:</b><asp:Label ID="lblDifference" Text="0.00" runat="server"
                                        Style="margin-right: 20px; padding-left: 5px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 43px;">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Debit F.C. Amount:</b><asp:Label ID="lblDebitAmountFc"
                                        Text="0.00" runat="server" Style="margin-right: 20px; padding-left: 5px"></asp:Label>
                                    &nbsp;&nbsp; <b>Credit F.C. Amount:</b><asp:Label ID="lblCreditAmountFc" Text="0.00"
                                        runat="server" Style="margin-right: 20px; padding-left: 5px"></asp:Label>
                                           &nbsp;&nbsp; <b>Difference Amount:</b><asp:Label ID="lblFcDifference" Text="0.00" runat="server"
                                        Style="margin-right: 20px; padding-left: 5px"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td style="width: 335px;">
                                    <span>
                                        <%--  <asp:Button ID="lblbutton" OnClientClick="location.href='JournalVouchers.aspx?addnew=1'"
                                            Style="cursor: pointer" runat="server" Text="Add New" />--%>
                                        <asp:Button ID="lblbutton" Style="cursor: pointer" runat="server" Text="Add New"
                                            OnClick="lblbutton_Click" />
                                        <asp:Button Text="Delete" OnClientClick="return askDeleteVoucher();" ID="btnDeleteInvoice"
                                            runat="server" OnClick="btnDeleteInvoice_Click" />
                                        <asp:Button Text="Print" ID="btnPrint" runat="server" OnClick="btnPrint_Click" />
                                    </span>
                                </td>
                                <td style="width: 550px">
                                    <table cellpadding="0" cellspacing="0" style="padding: 5px 155px 0px 0px; width: 100%">
                                        <tr>
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 33%;">
                                            </td>
                                            <td align="left" valign="top" style="padding: 2px 0px 2px 5px; text-align: right;
                                                width: 33%;">
                                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                                                    ShowSummary="false" ValidationGroup="AddInvoice" />
                                              <%--  <asp:ImageButton ID="ImageButton2" TabIndex="12" runat="server" ImageUrl="~/Images/btn-save.gif"
                                                    OnClick="btn_saveInvoice_Click" ValidationGroup="AddInvoice" />--%>
                                                    
                                                      <asp:Button ID="ImageButton2" OnClientClick="SaveClick()" TabIndex="12" Text="Save"
                                                    runat="server" ValidationGroup="AddInvoice" OnClick="btn_saveInvoice_Click" />
                                                    
                                                    &nbsp;</div>
                                            </td>
                                            <td style="width: 33%; padding: 2px 2px 20px 3px;">
                                              <asp:Button ID="ImageButton3" Text="Cancel" runat="server" OnClick="btn_cancel_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
