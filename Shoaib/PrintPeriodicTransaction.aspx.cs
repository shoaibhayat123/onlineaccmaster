﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintPeriodicTransaction : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    clsCookie ck = new clsCookie();
    decimal TotalDebit = 0;
    decimal TotalCredit = 0;
    decimal TotalDebitRow = 0;
    decimal TotalCreditRow = 0;
    decimal TotalAmount = 0;
    Reports objReports = new Reports();
    SetCustomers objCust = new SetCustomers();
    DataTable dttbl = new DataTable();
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {
            if (Request["From"].ToString() != "" || Request["To"].ToString() != "" || Request["AcfileId"].ToString() != "")
            {
                Session["s"] = null;
                bindSupplier();
                GetSequenceTrialBalance();
                FillLogo();
                lblFromDate.Text = ClsGetDate.FillFromDate(Request["From"].ToString());
                lblToDate.Text = ClsGetDate.FillFromDate(Request["To"].ToString());
                lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
                lblUser.Text = Sessions.UseLoginName.ToString();
                bindAccount();
            }
        }
    }
    protected void bindAccount()
    {

        objReports.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objReports.Id = Convert.ToInt32(Request["AcfileId"].ToString());
        DataSet ds = new DataSet();
        ds = objReports.getAcfileGroup();
        if (ds.Tables[0].Rows.Count > 0)
        {
            lblAccount.Text = Convert.ToString(ds.Tables[0].Rows[0]["Title"]);

        }

    }
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }


    }
    #endregion
    protected void GetSequenceTrialBalance()
    {


        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objSalesInvoice.AcfileID = Convert.ToInt32(Request["AcfileId"].ToString());
        DataSet ds = new DataSet();
        ds = objSalesInvoice.GetSequencePeriodicTransaction();
        DataTable dtResult = new DataTable();
        dtResult = ds.Tables[0].Copy();
        dtResult.Merge(ds.Tables[1]);

        if (dtResult.Rows.Count > 0)
        {

            DataView dv = dtResult.DefaultView;
            dv.Sort="ParrentId,Id";
            DataTable dt1 = new DataTable();
            dt1 = dv.ToTable();

         

            grdInvoice.DataSource = dt1;
            grdInvoice.DataBind();
            grandTotalDebit.Text = String.Format("{0:C}", TotalDebit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            grandTotalCredit.Text = String.Format("{0:C}", TotalCredit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            dvNoRecord.Style.Add("display", "none");
            dvResult.Style.Add("display", " ");

        }

        else
        {
            dvNoRecord.Style.Add("display", " ");
            dvResult.Style.Add("display", "none");
        }
    }


    #region Grid RowDataBound
    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblDebit = ((Label)e.Row.FindControl("lblDebit"));
            Label lblCredit = ((Label)e.Row.FindControl("lblCredit"));
            string OpeningBal = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "OpeningBal"));
            string TransTotal = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TransTotal"));
            if (OpeningBal == "")
            {
                OpeningBal = "0.00";
            }
            if (TransTotal == "")
            {
                TransTotal = "0.00";
            }
            TotalAmount = Convert.ToDecimal(TransTotal);
            if (TotalAmount > 0)
            {
                lblDebit.Text = String.Format("{0:C}", TotalAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblCredit.Text = "0.00";
                TotalDebit = TotalDebit + TotalAmount;
                TotalDebitRow = TotalDebitRow + TotalAmount;
            }
            else
            {
                lblCredit.Text = String.Format("{0:C}", TotalAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblDebit.Text = "0.00";
                TotalCredit = TotalCredit + TotalAmount;
                TotalCreditRow = TotalCreditRow + TotalAmount;
            }


            if (TotalAmount == 0)
            {
                e.Row.Style.Add("display", "none");
            }


        }



        if (e.Row.RowType == DataControlRowType.Footer)  // For footer totals
        {
            Label lblTotalDebit = (Label)e.Row.FindControl("lblTotalDebit");
            lblTotalDebit.Text = String.Format("{0:C}", TotalDebitRow).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            Label lblTotalCredit = (Label)e.Row.FindControl("lblTotalCredit");
            lblTotalCredit.Text = String.Format("{0:C}", TotalCreditRow).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

        }
    }
    #endregion
    #region Fill session Table
    public void filltable()
    {
        if (Session["s"] == null)
        {
            DataColumn column = new DataColumn();
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Title";
            column.Caption = "Title";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "ParrentId";
            column.Caption = "ParrentId";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "depth";
            column.Caption = "depth";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);




        }
        else
        {
            dttbl = (DataTable)Session["s"];
        }
    }

    protected void AddRecord(int Id, string Title, int ParrentId, int depth)
    {
        DataRow row;
        row = dttbl.NewRow();
        row["Id"] = Id;
        row["Title"] = Title;
        row["ParrentId"] = ParrentId;
        row["depth"] = depth;


        dttbl.Rows.Add(row);
        dttbl.AcceptChanges();
        Session.Add("s", dttbl);
    }
    #endregion

    protected void grdInvoice_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnid = ((HiddenField)e.Row.FindControl("hdnid"));
            hdnid.Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Id"));
            objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);

            objSalesInvoice.DateFrom = Convert.ToDateTime(Request["From"]);
            objSalesInvoice.DateTo = Convert.ToDateTime(Request["To"]);
            objSalesInvoice.id = Convert.ToInt32(hdnid.Value);

            DataSet ds = objSalesInvoice.GetProfitAndLoss();
            GridView grdLedger = (GridView)e.Row.FindControl("grdLedger");
            if (ds.Tables[0].Rows.Count > 0)
            {
                //decimal TransTotal = (decimal)ds.Tables[0].Compute("SUM(TransTotal)", "");

                int Count = 0;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[i]["TransTotal"]) != "")
                    {
                        if (Convert.ToDecimal(ds.Tables[0].Rows[i]["TransTotal"]) != 0)
                        {
                            Count = 1; break;
                        }
                    }
                }
                if (Count == 1)
                {
                    Label lblTitle = (Label)e.Row.FindControl("lblTitle");
                    lblTitle.Style.Add("display", " ");
                    TotalDebitRow = 0;
                    TotalCreditRow = 0;
                    grdLedger.DataSource = ds.Tables[0];
                    grdLedger.DataBind();
                }

            }

            else
            {
                Label lblTitle = (Label)e.Row.FindControl("lblTitle");
                lblTitle.Style.Add("display", "none");

            }
        }

    }
}
