﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="ServiceInvoice.aspx.cs" Inherits="ServiceInvoice" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Service Invoice"></asp:Label>
    <script type="text/javascript">
        function Count(text, long) {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }
    </script>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);

        function BeginRequestHandler(sender, args) {

            if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Save") {

                document.getElementById('<%= ImageButton2.ClientID %>').value = "Saving...";
                args.get_postBackElement().disabled = true;
            }
            if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Update") {

                document.getElementById('<%= btnAddNew.ClientID %>').value = "Updating...";
                args.get_postBackElement().disabled = true;
            }


        }
        function SaveClick() {
            document.getElementById('<%= hdnButtonText.ClientID %>').value = "Save";

        }

        function UpdateClick() {
            document.getElementById('<%= hdnButtonText.ClientID %>').value = "Update";

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: fixed; width: 100%">
                <div style="position: static; width: 100%">
                    <div style="position: absolute; top: -100px; margin-left: 37%;">
                        <img src="images/loader.gif" />
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
        <asp:HiddenField ID="hdnButtonText" runat="server" />
            <asp:HiddenField ID="hdnServiceInvoiceMainId" runat="server" />
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                width="100%">
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                        Inv. No. :
                    </td>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 5px; text-align: left">
                        <table>
                            <tr>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 0px;">
                                    <asp:TextBox ID="txtInvNo" TabIndex="1" onChange="isNumericPositive(this)" CssClass="input_box33"
                                        Style="float: left; text-transform: uppercase;" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill  Inv. No." Text="*" ControlToValidate="txtInvNo" ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <span id="spanPgging" runat="server" style="padding: 0 20px 0 20px; font-weight: bold;
                                        font-size: 18px">
                                        <asp:LinkButton ID="lnkFirst" Text="|<" runat="server" OnClick="lnkFirst_Click"> </asp:LinkButton>
                                        <asp:LinkButton ID="lnkPrevious" Style="margin-left: 10px;" Text="<" runat="server"
                                            OnClick="lnkPrevious_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkNext" Text=">" Style="margin-left: 10px;" runat="server" OnClick="lnkNext_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnLast" Text=">|" Style="margin-left: 10px;" runat="server" OnClick="lnLast_Click"></asp:LinkButton></span>
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    Inv. Date :
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    <asp:TextBox ID="txtInvDate" TabIndex="2" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvDate'),this);"
                                        runat="server" Width="100px"></asp:TextBox>
                                    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                        onclick="scwShow(scwID('ctl00_MainHome_txtInvDate'),this);" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill Voucher Date." Text="*" ControlToValidate="txtInvDate"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                        ControlToValidate="txtInvDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid Voucher Date !!"
                                        Display="None"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 110px; padding: 2px 2px 2px 8px;">
                        Cost Center :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                        <asp:DropDownList ID="ddlCostCenter" TabIndex="3" runat="server" Style="width: 550px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select Cost Center" Text="*" ControlToValidate="ddlCostCenter"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                        Debit A/C :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                        <asp:DropDownList ID="ddlDebit" TabIndex="4" runat="server" Style="width: 550px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select   Debit A/C " Text="*" ControlToValidate="ddlDebit"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                        Credit A/C :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                        <asp:DropDownList ID="ddlCredit" TabIndex="5" runat="server" Style="width: 550px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select Credit A/C" Text="*" ControlToValidate="ddlCredit"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="sundryDR" runat="server" style="height: 305px; width: 660px; padding: 10px;
                            overflow: auto; border: solid 2px  #528627">
                            <table>
                                <tr>
                                    <td style="margin-right: 10px; vertical-align: top">
                                        Description :
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtDescriptionDetail" TabIndex="6" runat="server" onKeyUp="Count(this,250)"
                                            onChange="Count(this,250);capitalizeMe(this)" TextMode="MultiLine" Style="width: 550px;"
                                            Rows="2" MaxLength="250"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill Description Detail " Text="*" ControlToValidate="txtDescriptionDetail"
                                            ValidationGroup="AddVoucherDetail"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;">
                                        Amount :
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtAmount" runat="server" TabIndex="7" onChange="isNumericDecimal(this)"
                                            Style="width: 150px;" MaxLength="10"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill Amount " Text="*" ControlToValidate="txtAmount" ValidationGroup="AddVoucherDetail"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: right">
                                        <asp:Button Style="text-align: right" ID="btnAddNew" TabIndex="7" OnClick="btnAddNew_Click" OnClientClick="UpdateClick()" 
                                            runat="server" Text="Update" ValidationGroup="AddVoucherDetail" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                            ShowSummary="false" ValidationGroup="AddVoucherDetail" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:HiddenField ID="hdndetail" runat="server" />
                                        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                                            AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                            AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                            BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery_RowDataBound"
                                            OnItemCommand="dgGallery_ItemCommand">
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="S.No.">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataSetIndex + 1%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Description">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="left" Width="60%" />
                                                    <ItemTemplate>
                                                        <%# Eval("Description")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Amount">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("Amount")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%-- <asp:TemplateColumn HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgDel1" runat="server" ImageUrl="Images/bt-delete.gif" AlternateText="Delete"
                                                            CommandArgument='<%#Eval("Id")%>' CommandName="Delete" OnClientClick="return askDelete();" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>--%>
                                                <asp:TemplateColumn HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="imgDel1" runat="server" Text="Delete" CommandArgument='<%#Eval("Id")%>'
                                                            CommandName="Delete" OnClientClick="return askDeleteRow();" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Edit">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="ImageButton1" runat="server" CommandArgument='<%#Eval("Id")%>'
                                                            Text="Edit" CommandName="Edit" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: left; width: 660px;">
                        <table style="width: 800px;">
                            <tr>
                                <td>
                                    Totals :: &nbsp;&nbsp;&nbsp;&nbsp; <b>Amount:</b><asp:Label ID="lblAmount" Text="0.00"
                                        runat="server" Style="margin-right: 20px; padding-left: 5px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="float: left">
                                    <asp:TextBox ID="txtDescription" TabIndex="9" Width="490px" Style="height: 50px;"
                                        onKeyUp="Count(this,250)" onChange="Count(this,250);capitalizeMe(this)" TextMode="MultiLine"
                                        runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td style="width: 435px;">
                                    <span>
                                        <%--   <asp:Button ID="lblbutton" OnClientClick="location.href='ServiceInvoice.aspx?addnew=1'"
                                            Style="cursor: pointer" runat="server" Text="Add New" />--%>
                                        <asp:Button ID="lblbutton" Style="cursor: pointer" runat="server" Text="Add New"
                                            OnClick="lblbutton_Click" />
                                        <asp:Button Text="Delete" OnClientClick="return askDeleteInvoice();" ID="btnDeleteInvoice"
                                            runat="server" OnClick="btnDeleteInvoice_Click" />
                                        <asp:Button Text="Print" ID="btnPrint" runat="server" OnClick="btnPrint_Click" />
                                    </span>&nbsp;&nbsp;&nbsp;&nbsp; <span id="spanLastInvoice" runat="server" style="display: none">
                                        <b>Last Inv No:
                                            <asp:Literal ID="litLastInvoiceNo" runat="server"></asp:Literal>&nbsp;&nbsp;<asp:Literal
                                                ID="litLastInvDate" runat="server"></asp:Literal></b></span>
                                </td>
                                <td style="width: 400px">
                                    <table cellpadding="0" cellspacing="0" style="padding: 5px 155px 0px 0px; width: 100%">
                                        <tr>
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 33%;">
                                            </td>
                                            <td align="left" valign="top" style="padding: 2px 0px 2px 5px; text-align: right;
                                                width: 33%;">
                                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                                                    ShowSummary="false" ValidationGroup="AddInvoice" />
                                                <%--<asp:ImageButton ID="ImageButton2" TabIndex="10" runat="server" ImageUrl="~/Images/btn-save.gif"
                                                    OnClick="btn_saveInvoice_Click" ValidationGroup="AddInvoice" />--%>
                                                       <asp:Button ID="ImageButton2" OnClientClick="SaveClick()" TabIndex="10" Text="Save"
                                                    runat="server" ValidationGroup="AddInvoice" OnClick="btn_saveInvoice_Click" />
                                                    
                                                    &nbsp;</div>
                                            </td>
                                            <td style="width: 33%; padding: 2px 2px 7px 3px;">
                                                 <asp:Button ID="ImageButton3" Text="Cancel" runat="server" OnClick="btn_cancel_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
