﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class ProductSetup : System.Web.UI.Page
{

    #region DataMembers & Varriables
    SetCustomers objCust = new SetCustomers();
    #endregion
    #region Page Load Event

    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "" || Sessions.CustomerCode == null)
        {

            Response.Redirect("Default.aspx");

        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
            if (!IsPostBack)
            {

                FilItems();
                bindCostCenter();
                FillMainCategory();
            }
        }
    }

    #endregion
    #region Add/ Edit Product Category  Setup

    protected void btn_save_Click(object sender, EventArgs e)
    {
        if (rptrCostCenter.Items.Count > 0)
        {

            if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
            {
                objCust.ItemId = int.Parse(hdnItemId.Value);
            }
            else
            {
                objCust.ItemId = 0;
            }
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.ItemDesc = txtItemDesc.Text;
            objCust.BarcodeNo = txtBarCode.Text;
            objCust.BoutiqueItemCode = txtSku.Text;
            objCust.MainCategoryCode = Convert.ToInt32(ddlMainCategory.SelectedValue);
            objCust.SubCategoryCode = Convert.ToInt32(ddlSubCategory.SelectedValue);
            objCust.Unit = txtUnit.Text;
            objCust.Packing = txtPacking.Text;

            Nullable<decimal> nullPurchaseRate;
            nullPurchaseRate = null;
            if (txtPurchaseRate.Text != "")
            {
                nullPurchaseRate = Convert.ToDecimal(txtPurchaseRate.Text);
            }
            objCust.PurchaseRate = nullPurchaseRate;


            Nullable<decimal> nullSaleRate;
            nullSaleRate = null;
            if (txtSaleRate.Text != "")
            {
                nullSaleRate = Convert.ToDecimal(txtSaleRate.Text);
            }
            objCust.SaleRate = nullSaleRate;

            Nullable<decimal> nullSalesTax;
            nullSalesTax = null;
            if (txtSalesTax.Text != "")
            {
                nullSalesTax = Convert.ToDecimal(txtSalesTax.Text);
            }
            objCust.SalesTax = nullSalesTax;


            Nullable<decimal> nullDutyNumber;
            nullDutyNumber = null;
            if (txtDuty.Text != "")
            {
                nullDutyNumber = Convert.ToDecimal(txtDuty.Text);
            }
            objCust.DutyNumber = nullDutyNumber;

            if (txtMinstock.Text != "")
            {
                objCust.MinOrderLevel = Convert.ToDecimal(txtMinstock.Text);
            }


            if (chkInvetory.Checked == true)
            {
                objCust.Opbal = Convert.ToDecimal(lblTotal.Text);
            }
            else
            {
                objCust.Opbal = 0;
            }

            objCust.IsInvetory = Convert.ToBoolean(chkInvetory.Checked);
            if (Sessions.UserRole.ToString() != "Admin")
            {
                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
                if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" || Convert.ToString(dt.Rows[0]["IsAdd"]) == "True")
                {

                    if (Convert.ToString(dt.Rows[0]["IsAdd"]) == "True" && (hdnItemId.Value == null || hdnItemId.Value == string.Empty))
                    {
                        int result = objCust.SetItem();

                        /* region add Cost center data*/
                        if (result != -1)
                        {
                            /* 1. delete Old records */
                            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            objCust.ItemId = result;
                            objCust.DeleteItemSetupCostCenterByItemId();

                            /* 2. Insert new records */
                            for (int i = 0; i < rptrCostCenter.Items.Count; i++)
                            {
                                //find control
                                HiddenField hdnCostcenterCode = (HiddenField)rptrCostCenter.Items[i].FindControl("hdnCostcenterCode");
                                TextBox txtCostCenter = (TextBox)rptrCostCenter.Items[i].FindControl("txtCostCenter");
                                objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                if (chkInvetory.Checked == true)
                                {
                                    objCust.OpeningQuty = Convert.ToDecimal(txtCostCenter.Text);
                                }
                                else
                                {
                                    objCust.OpeningQuty = 0;
                                }
                                objCust.ItemId = result;
                                objCust.ItemSetupCostCenterId = 0;
                                objCust.CostCenterCode = Convert.ToInt32(hdnCostcenterCode.Value);
                                objCust.SetItemSetupCostCenter();

                            }
                        }

                        if (hdnItemId.Value == null || hdnItemId.Value == string.Empty)
                        {
                            if (result != -1)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Product Setup successful added!');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                            }
                            clear();
                            bindCostCenter();
                            FilItems();
                        }
                    }
                    else
                    {
                        if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" && hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                        {
                            int result = objCust.SetItem();

                            /* region add Cost center data*/
                            if (result != -1)
                            {
                                /* 1. delete Old records */
                                objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                objCust.ItemId = result;
                                objCust.DeleteItemSetupCostCenterByItemId();

                                /* 2. Insert new records */
                                for (int i = 0; i < rptrCostCenter.Items.Count; i++)
                                {
                                    //find control
                                    HiddenField hdnCostcenterCode = (HiddenField)rptrCostCenter.Items[i].FindControl("hdnCostcenterCode");
                                    TextBox txtCostCenter = (TextBox)rptrCostCenter.Items[i].FindControl("txtCostCenter");
                                    objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                    if (chkInvetory.Checked == true)
                                    {
                                        objCust.OpeningQuty = Convert.ToDecimal(txtCostCenter.Text);
                                    }
                                    else
                                    {
                                        objCust.OpeningQuty = 0;
                                    }
                                    objCust.ItemId = result;
                                    objCust.ItemSetupCostCenterId = 0;
                                    objCust.CostCenterCode = Convert.ToInt32(hdnCostcenterCode.Value);
                                    objCust.SetItemSetupCostCenter();

                                }
                            }

                            if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                            {
                                if (result != -1)
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Product Setup successful updated!');", true);
                                }
                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                                }
                                clear();
                                bindCostCenter();
                                FilItems();
                            }

                        }

                        else
                        {
                            if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='ProductSetup.aspx';", true);
                            }

                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='ProductSetup.aspx';", true);
                            }
                        }
                    }
                }
                else
                {
                    if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='ProductSetup.aspx';", true);
                    }

                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='ProductSetup.aspx';", true);
                    }
                }
               
            }
            else
            {

                int result = objCust.SetItem();

                /* region add Cost center data*/
                if (result != -1)
                {
                    /* 1. delete Old records */
                    objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                    objCust.ItemId = result;
                    objCust.DeleteItemSetupCostCenterByItemId();

                    /* 2. Insert new records */
                    for (int i = 0; i < rptrCostCenter.Items.Count; i++)
                    {
                        //find control
                        HiddenField hdnCostcenterCode = (HiddenField)rptrCostCenter.Items[i].FindControl("hdnCostcenterCode");
                        TextBox txtCostCenter = (TextBox)rptrCostCenter.Items[i].FindControl("txtCostCenter");
                        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                        if (chkInvetory.Checked == true)
                        {
                            objCust.OpeningQuty = Convert.ToDecimal(txtCostCenter.Text);
                        }
                        else
                        {
                            objCust.OpeningQuty = 0;
                        }
                        objCust.ItemId = result;
                        objCust.ItemSetupCostCenterId = 0;
                        objCust.CostCenterCode = Convert.ToInt32(hdnCostcenterCode.Value);
                        objCust.SetItemSetupCostCenter();

                    }
                }

                if (hdnItemId.Value != null && hdnItemId.Value != string.Empty)
                {
                    if (result != -1)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Product Setup successful updated!');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                    }
                }
                else
                {
                    if (result != -1)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Product Setup successful added!');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                    }

                }
                clear();
                bindCostCenter();
                FilItems();
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please Add Cost Center First!');", true);
        }

    }

    #endregion
    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        clear();
        bindCostCenter();
    }
    #endregion
    #region Blank Controls
    private void clear()
    {

        txtDuty.Text = "";
        txtItemDesc.Text = "";
        txtBarCode.Text = "";
        txtSku.Text = "";
        txtPacking.Text = "";
        txtPurchaseRate.Text = "";
        txtSaleRate.Text = "";
        txtSalesTax.Text = "";
        txtUnit.Text = "";
        lblTotal.Text = "0.00";
        chkInvetory.Checked = true;
        ddlSubCategory.SelectedValue = "0";
        ddlMainCategory.SelectedValue = "0";
        hdnItemId.Value = string.Empty;
        txtMinstock.Text = "";

    }
    #endregion
    #region Fill Item Setup
    protected void FillData(int ItemId)
    {
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.ItemId = ItemId;
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        if (ds.Tables[0].Rows.Count > 0)
        {
            txtDuty.Text = Convert.ToString(ds.Tables[0].Rows[0]["DutyNumber"].ToString());
            txtMinstock.Text = Convert.ToString(ds.Tables[0].Rows[0]["MinOrderLevel"].ToString());
            txtItemDesc.Text = Convert.ToString(ds.Tables[0].Rows[0]["ItemDesc"].ToString());
            txtBarCode.Text = Convert.ToString(ds.Tables[0].Rows[0]["BarcodeNo"].ToString());
            txtSku.Text = Convert.ToString(ds.Tables[0].Rows[0]["BoutiqueItemCode"].ToString());
            txtPacking.Text = Convert.ToString(ds.Tables[0].Rows[0]["Packing"].ToString());
            txtPurchaseRate.Text = Convert.ToString(ds.Tables[0].Rows[0]["PurchaseRate"].ToString());
            txtSaleRate.Text = Convert.ToString(ds.Tables[0].Rows[0]["SaleRate"].ToString());
            txtSalesTax.Text = Convert.ToString(ds.Tables[0].Rows[0]["SalesTax"].ToString());
            txtUnit.Text = Convert.ToString(ds.Tables[0].Rows[0]["Unit"].ToString());
            ddlMainCategory.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["MainCategoryCode"].ToString());
            lblTotal.Text = Convert.ToString(ds.Tables[0].Rows[0]["Opbal"].ToString());
            chkInvetory.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsInvetory"].ToString());
            FillSubCategory();
            ddlSubCategory.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["SubCategoryCode"].ToString());
            hdnItemId.Value = ItemId.ToString();

        }

        bindCostCenter();
        if (ds.Tables[0].Rows.Count > 0)
        {
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.ItemId = ItemId;
            DataSet dsCostcenter = new DataSet();
            dsCostcenter = objCust.getItemSetupCostCenterByItemId();
            for (int i = 0; i < rptrCostCenter.Items.Count; i++)
            {
                HiddenField hdnCostcenterCode = (HiddenField)rptrCostCenter.Items[i].FindControl("hdnCostcenterCode");
                TextBox txtCostCenter = (TextBox)rptrCostCenter.Items[i].FindControl("txtCostCenter");

                for (int j = 0; j < dsCostcenter.Tables[0].Rows.Count; j++)
                {
                    if (Convert.ToString(hdnCostcenterCode.Value) == Convert.ToString(dsCostcenter.Tables[0].Rows[j]["CostCenterCode"]))
                    {

                        txtCostCenter.Text = Convert.ToString(dsCostcenter.Tables[0].Rows[j]["OpeningQuty"]);
                        break;
                    }

                }

            }


        }



    }
    #endregion
    #region Fill Main Category  Setup
    protected void FilItems()
    {
       

            objCust.ItemId = 0;
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataSet ds = new DataSet();
            ds = objCust.getItems();
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 30)
                {
                    grdCustomer.AllowPaging = true;
                }
                grdCustomer.Visible = true;
                grdCustomer.DataSource = ds.Tables[0];
                grdCustomer.DataBind();
                lblError.Visible = false;
            }
            else
            {
                grdCustomer.Visible = false;
                lblError.Visible = true;
            }
        

    }
    #endregion
    #region GridView Events
    protected void grdCustomer_RowCommand(object sender, DataGridCommandEventArgs e)
    {

        if (e.CommandName == "Delete")
        {
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.ItemId = int.Parse(e.CommandArgument.ToString());
            objCust.DeleteItemSetupCostCenterByItemId();
            objCust.DeleteItems();
            FilItems();
        }

        if (e.CommandName == "Edit")
        {
            FillData(int.Parse(e.CommandArgument.ToString()));

        }
    }
    protected void grdCustomer_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {


            if (Sessions.UserRole.ToString() != "Admin")
            {


                LinkButton lbEdit = (LinkButton)e.Item.FindControl("lbEdit");
                LinkButton lbDelete = (LinkButton)e.Item.FindControl("lbDelete");

                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
                if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
                {
                    Response.Redirect(Request.UrlReferrer.ToString());
                }
                if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
                {

                    lbDelete.Visible = false;

                }
                if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
                {

                    lbEdit.Visible = false;

                }

            }
        }


    }

    protected void grdCustomer_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        grdCustomer.CurrentPageIndex = e.NewPageIndex;
        FilItems();
    }
    #endregion
    #region Fill Main Category
    protected void FillMainCategory()
    {
       
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.MainCategoryCode = 0;
            DataSet ds = new DataSet();
            ds = objCust.GetMainCategory();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlMainCategory.DataSource = ds.Tables[0];
                ddlMainCategory.DataTextField = "MainCategoryName";
                ddlMainCategory.DataValueField = "MainCategoryCode";
                ddlMainCategory.DataBind();
                ddlMainCategory.Items.Insert(0, new ListItem("Select Main Category", "0"));
            }
        

    }
    #endregion
    #region Fill Sub Category

    protected void FillSubCategory()
    {
       
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.MainCategoryCode = Convert.ToInt32(ddlMainCategory.SelectedValue);
            DataSet ds = new DataSet();
            ds = objCust.GetSubCategoryByMainCategory();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlSubCategory.DataSource = ds.Tables[0];
                ddlSubCategory.DataTextField = "SubCategoryName";
                ddlSubCategory.DataValueField = "SubCategoryCode";
                ddlSubCategory.DataBind();
                ddlSubCategory.Items.Insert(0, new ListItem("Select Sub Category", "0"));
            }
       
    }
    #endregion

    protected void ddlMainCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSubCategory();
        ddlSubCategory.Focus();
    }
    protected void bindCostCenter()
    {
        objCust.CostCenterCode = 0;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.GetCostCenter();
        if (ds.Tables[0].Rows.Count > 0)
        {
            rptrCostCenter.DataSource = ds.Tables[0];
            rptrCostCenter.DataBind();

        }
    }
    protected void txtCostCenter_Onchanged(object sender, EventArgs e)
    {
        decimal TotalQuantity = 0;
        for (int i = 0; i < rptrCostCenter.Items.Count; i++)
        {
            //find control
            TextBox txtCostCenter = (TextBox)rptrCostCenter.Items[i].FindControl("txtCostCenter");
            if (txtCostCenter.Text != "")
            {

                TotalQuantity += Convert.ToDecimal(txtCostCenter.Text);
            }
            else
            {
                txtCostCenter.Text = "0.00";
            }


        }
        lblTotal.Text = Convert.ToString(TotalQuantity);


    }


}

