﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="CashPaymentVoucherSimpleReportFc.aspx.cs" Inherits="CashPaymentVoucherSimpleReportFc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Cash Payment Voucher Report"></asp:Label>
    <script type="text/javascript">

        function Count(text, long) {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }
          
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnInvoiceId" runat="server" />
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Voucher # :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:TextBox ID="txtVoucherNo" onChange="isNumericint(this)" CssClass="input_box33"
                            Width="100px" Style="float: left; width: 264px; text-transform: uppercase;" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Cash A/C :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:DropDownList ID="ddlCashAC" runat="server" Style="width: 270px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Voucher Date From:
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:TextBox ID="txtVoucherDateFrom" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtVoucherDateFrom'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtVoucherDateFrom'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill  Voucher Date From." Text="*" ControlToValidate="txtVoucherDateFrom"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtVoucherDateFrom" ValidationGroup="AddInvoice" ErrorMessage="Invalid  Voucher Date !!"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Voucher Date To :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:TextBox ID="txtVoucherDateTo" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtVoucherDateTo'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtVoucherDateTo'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill Voucher Date To." Text="*" ControlToValidate="txtVoucherDateTo"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtVoucherDateTo" ValidationGroup="AddInvoice" ErrorMessage="Invalid Voucher Date To !!"
                            Display="None"></asp:RegularExpressionValidator>
                        <span style="float: right; padding-right: 65px">
                            <asp:Button ID="btnsearch" Text="Search" TabIndex="6" runat="server" OnClick="btnsearch_Click" /></span>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" style="float: left; padding: 0 10px 10px 10px;" border="0"
                cellpadding="0" width="90%">
                <tr>
                    <td colspan="3">
                        <asp:GridView ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                            AllowPaging="true" PageSize="500000" PagerSettings-Visible="false" PagerSettings-Mode="Numeric"
                            HeaderStyle-CssClass="gridheader" AlternatingRowStyle-CssClass="gridAlternateItem" OnRowCommand="grdCustomer_RowCommand"
                            OnRowDataBound="dgGallery_RowDataBound" GridLines="none" BorderColor="black"
                            FooterStyle-CssClass="gridFooter" BorderStyle="Dotted" BorderWidth="1" RowStyle-CssClass="gridItem"
                            ShowFooter="true">
                            <EmptyDataTemplate>
                                <div style="padding-bottom: 10px; width: 100%; text-align: center; font-family: Gisha;
                                    font-size: 20px; background-color: #E1F5FD; color: #110F0F;">
                                    <div style="padding-top: 10px; padding-bottom: 5px;">
                                        No record found
                                    </div>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Voucher No">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="8%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Eval("VoucherNo")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cash A/C">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Eval("Title")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Voucher Date">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblVoucherDate" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="F.C. Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("TotalAmountFC")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                               
                                <asp:TemplateField HeaderText="Voucher Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("TotalAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalGrossAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Edit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                       <%-- <a href='CashPaymentVoucherSimpleFc.aspx?VoucherSimpleMainId=<%#Eval("id") %>'>
                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" /></a>--%>
                                              <asp:Button ID="btnEdit" CommandName="EditCustomer" CommandArgument='<%#Eval("id") %>' runat="server" Text="Edit" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
