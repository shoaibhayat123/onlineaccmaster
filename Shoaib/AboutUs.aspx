﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaticPages.master" AutoEventWireup="true"
    CodeFile="AboutUs.aspx.cs" Inherits="AboutUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content_wraper5">
        <div id="content_midd5">
            <div class="acc-features-middle-bg">
                <div class="top-heading">
                    <h1>
                        About Us</h1>
                </div>
            </div>
            <div class="accmiddle-features-bg-2" style="min-height: 520px">
                <div class="features-wrapper">
                    <div class="features-box">
                        <div style="float: left">
                            <div class="features-left-link-box">
                                <div class="left-link-Content">
                                    <h2 style="text-decoration: underline; font-size: 18px;">
                                        AccMaster</h2>
                                    <br />
                                    <ul style="margin-left: 20px">
                                        <li><a href="index.aspx">Home</a></li>
                                        <li><a href="features.aspx">Features</a></li>
                                        <li><a href="pricing.aspx">Product & Pricing</a></li>
                                        <li><a href="PayOnline.aspx">Pay Online</a></li>
                                        <li><a href="Privacy.aspx">Privacy</a></li>
                                        <li><a href="Terms.aspx">Terms</a></li>
                                        <li><a href="ContactUs.aspx">Contact us</a></li>
                                        <li><a href="AboutUs.aspx">About Us</a></li>
                                        <li><a href="Careers.aspx">Careers</a></li>
                                    </ul>
                                </div>
                                <div class="left-link-bottom">
                                    <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                                </div>
                            </div>
                            <div class="features-left-link-box" style="clear: both; margin-top: 20px;">
                                <div class="left-link-Content" style="min-height: 100px;line-height:0.6">
                                    <asp:Literal ID="ltrlAddress" runat="server"></asp:Literal>
                                </div>
                                <div class="left-link-bottom">
                                    <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                                </div>
                            </div>
                        </div>
                        <div class="feat-right-part-box">
                            <div class="feta-right-part-content">
                                <div class="feat-right-box-dtls">
                                    <div class="ft-right-box-details2">
                                        <h2>
                                            Company Overview</h2>
                                        <b>SYSTEM DEVELOPMENT SERVICES</b> was established in 1991 as a software<br />
                                        development company committed to the creation and delivery of
                                        <br />
                                        outstanding software solutions for Small and Medium Enterprises.<br />
                                        <br />
                                        The company adheres a vision to transform the society through software<br />
                                        development by producing unique solutions that are tailored to the individual
                                        <br />
                                        business requirements by highly qualified programming and experienced technology
                                        <br />
                                        experts to create a custom base solution.<br />
                                        <br />
                                        With over 2 decade of development we specialize in automation of Business Process
                                        and have developed a range of solutions and gained ample experience in the services
                                        we offer: Custom Software Development. Our specialists have been providing software
                                        development services for multiple clients from all segments of Businesses like Manufacturing,
                                        Intellectual Properties, Automobile Trading (NEW / USED), Designer Boutique, Logistics
                                        and Warehousing, Travels and Tours, Supply Chain Management, POS (FMCG)<br />
                                        <table style="margin-top: 20px">
                                            <tr>
                                                <td style="vertical-align: top; text-align: left; width: 33%; margin-left: 15px">
                                                    <h2>
                                                        Personnel</h2>
                                                    <ul style="text-align: left; padding-left: 10px">
                                                        <li>CEO – 1</li>
                                                        <li>Project Coordinator/General managers - 1 </li>
                                                        <li>Marketing – 1 </li>
                                                        <li>Project Managers - 1 </li>
                                                        <li>IT/Business Analysts - 2 </li>
                                                        <li>Senior developers/Architects - 1 </li>
                                                        <li>Developers -8 </li>
                                                        <li>Support Technicians, QA engineers/Testers/Tech. Writers – 2 </li>
                                                    </ul>
                                                </td>
                                                <%--  <td style="vertical-align: top; text-align: left; width: 33%; margin-left: 15px">
                                                    <b style="margin: auto; font-size: 15px; text-decoration: underline">Key Industries</b>
                                                    <ul style="text-align: left">
                                                        <li>Manufacturing</li>
                                                        <li>Retail</li>
                                                        <li>Law Firms</li>
                                                        <li>Warehouse Management</li>
                                                        <li>Trading</li>
                                                        <li>Automobile and Fleet Management</li>
                                                        <li>Metal Industries</li>
                                                        <li>Petro Site Services</li>
                                                    </ul>
                                                </td>
                                                <td style="vertical-align: top; text-align: left; width: 33%; margin-left: 15px">
                                                    <b style="margin: auto; font-size: 15px; text-decoration: underline">Technologies</b><br />
                                                    <ul style="text-align: left">
                                                        <li>Microsoft .NET. </li>
                                                        <li>ASP.NET (Web Forms, Web Controls) </li>
                                                        <li>Windows Forms, Windows Controls </li>
                                                        <li>ADO.NET </li>
                                                        <li>Enterprise Services (COM+, MSMQ) </li>
                                                        <li>XML Web Services </li>
                                                        <li>VB.NET.</li>
                                                        <li>MS SQL</li>
                                                        <li>CISCO</li>
                                                        <li>Cloud Base Computing.</li>
                                                    </ul>
                                                </td>--%>
                                            </tr>
                                        </table>
                                        <h2>
                                            Key Industries</h2>
                                        <ul style="text-align: left">
                                            <li>Manufacturing</li>
                                            <li>Retail</li>
                                            <li>Law Firms</li>
                                            <li>Warehouse Management</li>
                                            <li>Trading</li>
                                            <li>Automobile and Fleet Management</li>
                                            <li>Metal Industries</li>
                                            <li>Petro Site Services</li>
                                        </ul>
                                        <h2>
                                            Technologies</h2>
                                        Our company has gained extensive expertise in various technologies we cater:
                                        <ul style="text-align: left">
                                            <li>Microsoft .NET. </li>
                                            <li>ASP.NET (Web Forms, Web Controls) </li>
                                            <li>Windows Forms, Windows Controls </li>
                                            <li>ADO.NET </li>
                                            <li>Enterprise Services (COM+, MSMQ) </li>
                                            <li>XML Web Services </li>
                                            <li>VB.NET.</li>
                                            <li>MS SQL</li>
                                            <li>CISCO</li>
                                            <li>Cloud Base Computing.</li>
                                        </ul>
                                        <h2>
                                            Methodology</h2>
                                        <b>SYSTEM DEVELOPMENT SERVICES</b> carries out the creation of tailored business
                                        solutions The Company works with customers to enable them to access unique solutions
                                        for their particular business needs, while retaining a strong focus on customer
                                        service. The advantage of using a company like System Development Services to develop
                                        specialized software
                                        <br />
                                        In developing our project execution model, we at SDS adhere to the international
                                        standards of project management, to make the project flow easy and transparent for
                                        the client.
                                        <br />
                                        Every project passes through its 5 stages:
                                        <ul>
                                            <li>Business Process Optimization / Data Analysis</li>
                                            <li>Project Preparation Requirements Management </li>
                                            <li>Solutions Development / Testing</li>
                                            <li>Product Integration </li>
                                            <li>Quality Assurance / Support</li>
                                        </ul>
                                        <h2>
                                            Business Process Optimization / Data Analysis</h2>
                                        This is when the initial requirements collection takes place – our business analysts
                                        and software architects carefully study customer requirements to prepare a comprehensive
                                        proposal. The client shares their business process knowledge and our specialists
                                        will work on defining all the details by butting up some queries and offering best
                                        possible ideas for the project. The proposal is free and has no obligations.
                                        <h2>
                                            Project Preparation Requirements Management
                                        </h2>
                                        Here we describe client’s requirements into project documentation. Our specialists
                                        will prepare a proposal describing the product's performance, technologies, design
                                        features, verification requirements, etc. as well as the estimation of workload
                                        and cost of the project.<br />
                                        The requirements management allows the activities for obtaining control requirement
                                        changes and ensuring that other relevant plans and data are kept current. Requirements
                                        management continues all through the project, culminating in the comparison of the
                                        product against the requirements.
                                        <h2>
                                            Solution Development / Testing
                                        </h2>
                                        The software solution development is the greatest stage of the project where most
                                        of the programming, coding and testing takes place.
                                        <h2>
                                            Product Integration
                                        </h2>
                                        At the Product Integration stage we define the best possible integration sequence,
                                        integrate product components and deliver the product to the client. Where we feed
                                        live data conduct training and test solution behavior on live data<br />
                                        <h2>
                                            Quality Assurance / Support</h2>
                                        Quality Assurance is an essential part of any project, which ensures the top quality
                                        of our solutions. This is a complicated process, which takes place at all other
                                        stages of the project and includes the following activities:
                                        <br />
                                        <ul>
                                            <li>Strong project management </li>
                                            <li>Transparent project flow </li>
                                            <li>Well-defined requirements and structured project documentation </li>
                                            <li>Deep testing, bug-tracking and fixing </li>
                                            <li>Process monitoring, product evaluation in regard to the specified requirements and
                                                client expectations </li>
                                            <li>Usability analysis </li>
                                            <li>Confidentiality and Intellectual Property Protection </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="feat-right-part-bottom">
                                <img src="images/feat-right-part-bottom-bg.jpg" alt="" />
                            </div>
                        </div>
</asp:Content>
