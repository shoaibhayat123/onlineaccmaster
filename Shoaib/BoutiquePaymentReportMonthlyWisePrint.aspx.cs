﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class BoutiquePaymentReportMonthlyWisePrint : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();

    AccFiles objFile = new AccFiles();
    Reports objReports = new Reports();

    decimal TotalQuantity = 0;
    decimal TotalPaybleamount = 0;
    decimal GrandTotalPaybleamount = 0;
    decimal TotalSaleTaxAmount = 0;
    decimal TotalAmountIncludeTaxes = 0;
    decimal TotalInvoiceAmount = 0;
    decimal TotalAlterCharges = 0;
    decimal GrandTotalQuantity = 0;
    decimal GrandTotalGrossAmount = 0;
    decimal GrandTotalSaleTaxAmount = 0;
    decimal GrandTotalAmountIncludeTaxes = 0;
    decimal GrandTotalInvoiceAmount = 0;
    decimal TotalNetPayble = 0;

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if ((Sessions.CustomerCode == "") || (Request["From"].ToString() == "") || (Request["To"].ToString() == "") || (Request["CostCenter"].ToString() == "") || (Request["Item"].ToString() == ""))
        {
            Response.Redirect("default.aspx");
        }
        if (!IsPostBack)
        {

            bindSupplier();
            lblFromDate.Text = ClsGetDate.FillFromDate(Request["From"].ToString());
            lblToDate.Text = ClsGetDate.FillFromDate(Request["To"].ToString());
            lblConversionRate.Text = Convert.ToString(Request["ConvRate"].ToString());
            bindBuyer();
            FillLogo();
            lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
            lblUser.Text = Sessions.UseLoginName.ToString();
            bindrptrInvoiceReport();
            GrandlblTotalQuantity.Text = String.Format("{0:C}", GrandTotalQuantity).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            GrandlblTotalGrossAmount.Text = String.Format("{0:C}", GrandTotalPaybleamount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        }
    }

    protected void bindBuyer()
    {
    }
    #region Grid RowDataBound
    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {        
    }

    #endregion

    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }

    }
    #endregion
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }






    protected void bindrptrInvoiceReport()
    {
        
        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.DataFrom = "SALESINV";
        objReports.ItemCode = Convert.ToInt32(Request["Item"]);
        objReports.storeId = Convert.ToInt32(Request["CostCenter"]);
        objReports.InvDateFrom = Convert.ToDateTime(Request["From"]);
        objReports.InvDateTo = Convert.ToDateTime(Request["To"]);
        objReports.MainCategoryCode = Convert.ToInt32(Request["MainCategoryCode"]);
        DataSet ds = new DataSet();
        ds = objReports.getBrandForBoutiquePaymentSheetByDate();
        //if (ds.Tables[0].Rows.Count > 0)
        //{
            rptrInvoiceReport.DataSource = ds.Tables[0];
            rptrInvoiceReport.DataBind();
        //}
    }


    protected void dgGallery_OnItemDataBound(object sender, DataGridItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            Label lblInvDate = (Label)e.Item.FindControl("lblinvDate");
            lblInvDate.Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "InvDate").ToString());

            Label lblPayableAmount = (Label)e.Item.FindControl("lblPayableAmount");
            decimal Paybleamount = 0;

            Label lblDiscountAmount = (Label)e.Item.FindControl("lblDiscountAmount");
            decimal DiscountAmount = 0;
            decimal DesignerPrice = 0;


            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DesignerPrice")) != "")
            {
                DesignerPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "DesignerPrice")); 
            }
            Label lblDesignerPrice = (Label)e.Item.FindControl("lblDesignerPrice");
            lblDesignerPrice.Text = DesignerPrice.ToString("0.00");


            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BoutiqueDiscount")) != "")
            {
                DiscountAmount = DesignerPrice * (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "BoutiqueDiscount")) / 100);
                lblDiscountAmount.Text = DiscountAmount.ToString("0.00");
            }

             Label lblActualSalesAmount = (Label)e.Item.FindControl("lblActualSalesAmount");
            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DataFrom")) == "SALESINV")
            {
             lblActualSalesAmount.Text = (DesignerPrice - DiscountAmount).ToString("0.00");
            }
            else
            {
                lblActualSalesAmount.Text = (0-(DesignerPrice - DiscountAmount)).ToString("0.00");
            }
           
        


            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DataFrom")) == "SALERETINV")
            {
                TotalQuantity -= Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                lblQuantity.Text = "-" + Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Quantity"));
            }
            else
            {
                TotalQuantity += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                lblQuantity.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Quantity"));
            }



            Label lblPurchaseCostPrice = (Label)e.Item.FindControl("lblPurchaseCostPrice");
            decimal PurchaseCostPrice = 0;
            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BoutiqueDiscount")) != "" && Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BoutiquePurchaseCostPrice")) != "")
            {
                PurchaseCostPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "BoutiquePurchaseCostPrice")) - (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "BoutiquePurchaseCostPrice")) * (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "BoutiqueDiscount")) / 100));
            }
            else
            {
                if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BoutiquePurchaseCostPrice")) != "")
                {
                    PurchaseCostPrice = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "BoutiquePurchaseCostPrice"));
                }
            }




            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DataFrom")) == "SALERETINV")
            {
                lblPurchaseCostPrice.Text = "-" + PurchaseCostPrice.ToString("0.00");
            }
            else
            {
                lblPurchaseCostPrice.Text = Convert.ToString(PurchaseCostPrice.ToString("0.00"));
            }


            decimal AlterCharges = 0;
            decimal ConvRate = Convert.ToDecimal((Request["ConvRate"]));
            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AlterCharges")) != "")
            {
              
                if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DataFrom")) == "SALERETINV")
                {
                    objReports.ItemCode = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "ItemCode"));
                    objReports.invno = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "invno"));
                    DataTable dt = new DataTable();
                    dt = objReports.getchargesForReport();

                    if (dt.Rows.Count > 0)
                    {
                        AlterCharges = Convert.ToDecimal(dt.Rows[0]["AlterCharges"]) * Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                    }
                }
                else
                {
                    AlterCharges = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "AlterCharges"));

                }


            }

            ((Label)e.Item.FindControl("lblAlterCharges")).Text = (AlterCharges * ConvRate).ToString("0.00");



            Label lblNetAmount = (Label)e.Item.FindControl("lblNetAmount");
            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DataFrom")) == "SALERETINV")
            {
                lblNetAmount.Text = (0-((DesignerPrice - DiscountAmount) - (AlterCharges * ConvRate))).ToString("0.00");

            }
            else
            {
                lblNetAmount.Text = ((DesignerPrice - DiscountAmount) - (AlterCharges * ConvRate)).ToString("0.00");
            }

            Label lblNetPayable = (Label)e.Item.FindControl("lblNetPayable");
            decimal NetPayable = 0;
            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BoutiquePurchaseDiscount")) != "")
            {
                NetPayable =100-Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "BoutiquePurchaseDiscount"));
            }
            lblNetPayable.Text = NetPayable.ToString("0.00");
           
            Label lblNetPayableAmount = (Label)e.Item.FindControl("lblNetPayableAmount");

            lblNetPayableAmount.Text = (Convert.ToDecimal(lblNetAmount.Text) * (NetPayable / 100)).ToString("0.00");
            TotalNetPayble += Convert.ToDecimal(lblNetPayableAmount.Text);

            TotalAlterCharges += (AlterCharges * ConvRate);

            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BoutiquePurchaseCostPrice")) != "" && Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Quantity")) != "")
            {
                if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DataFrom")) == "SALERETINV")
                {
                    Paybleamount = ((Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity")) * PurchaseCostPrice) * -1) - Convert.ToDecimal((AlterCharges * ConvRate).ToString("0.00"));


                    lblPayableAmount.Text = String.Format("{0:C}", Paybleamount).Replace('$', ' ').Replace('(', '-').Replace(')', ' '); ;
                }
                else
                {
                    Paybleamount = (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity")) * PurchaseCostPrice) - Convert.ToDecimal((AlterCharges * ConvRate).ToString("0.00"));
                    lblPayableAmount.Text = String.Format("{0:C}", Paybleamount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' '); ;
                }
            }
            TotalPaybleamount += Convert.ToDecimal(Paybleamount.ToString("0.00"));
            GrandTotalPaybleamount += Convert.ToDecimal(Paybleamount.ToString("0.00"));



            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DataFrom")) == "SALERETINV")
            {

                GrandTotalQuantity -= Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity"));
            }
            else
            {

                GrandTotalQuantity += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity"));
            }


            Label lblType = (Label)e.Item.FindControl("lblType");

            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DataFrom")) == "SALERETINV")
            {
                lblType.Text = "SALERET";
            }
            else
            {
                lblType.Text = "SALE";
            }


            /*******************************************************************************************/
            /* Added "Show Received and Balance" by "Anoop" on "20Nov12" */
            /*******************************************************************************************/
            //Single BalAmt = 0;
            //string currInvNo;
            //Single TotalReceivedAmt, SaleAmt;
            //currInvNo = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "InvNo"));
            //SaleAmt = Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "Rate")) * Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "Quantity"));
            //TotalReceivedAmt = Convert.ToSingle(DataBinder.Eval(e.Item.DataItem, "Sum_ReceivedAmt"));

            //if (e.Item.ItemIndex == 0)
            //{
            //    Session["_prevInvNo"] = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "InvNo"));
            //    BalAmt = TotalReceivedAmt;
            //    Session["_GroupReceivedAmt"] = "0";
            //    Session["_SumSaleAmt"] = "0";
            //}
            //else
            //    BalAmt = Convert.ToSingle(Session["_BalAmt"]);

            //if (Convert.ToString(Session["_prevInvNo"]) == currInvNo)
            //{
            //    Session["_saleAmt"] = Convert.ToSingle(Session["_saleAmt"]) + SaleAmt;
            //}
            //else
            //{
            //    BalAmt = TotalReceivedAmt;
            //    Session["_saleAmt"] = Convert.ToSingle(Session["_saleAmt"]);
            //}


            //if (BalAmt >= SaleAmt)
            //{
            //    ((Label)e.Item.FindControl("lblReceivedAmount")).Text = SaleAmt.ToString();
            //    Session["_GroupReceivedAmt"] = Convert.ToSingle(Session["_GroupReceivedAmt"]) + SaleAmt;
            //}
            //else if (BalAmt >= 0)
            //{
            //    ((Label)e.Item.FindControl("lblReceivedAmount")).Text = BalAmt.ToString();
            //    Session["_GroupReceivedAmt"] = Convert.ToSingle(Session["_GroupReceivedAmt"]) + BalAmt;
            //}
            //else
            //    ((Label)e.Item.FindControl("lblReceivedAmount")).Text = "0";

            //BalAmt = BalAmt - SaleAmt;
            //Session["_BalAmt"] = BalAmt;
            //((Label)e.Item.FindControl("lblBalanceAmount")).Text = BalAmt.ToString();
            //Session["_SumSaleAmt"] = Convert.ToSingle(Session["_SumSaleAmt"]) + SaleAmt;

            //Session["_prevInvNo"] = currInvNo;

            /*******************************************************************************************/
        }

        if (e.Item.ItemType == ListItemType.Footer)
        {

            Label lblTotalQuantity = ((Label)e.Item.FindControl("lblTotalQuantity"));
            Label lblTotalPaybleamount = ((Label)e.Item.FindControl("lblTotalPaybleamount"));
            Label lblTotalNetPayable = ((Label)e.Item.FindControl("lblTotalNetPayable"));
            lblTotalNetPayable.Text = TotalNetPayble.ToString("0.00");
            if (TotalQuantity > 0)
            {

                lblTotalQuantity.Text = String.Format("{0:C}", TotalQuantity).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalPaybleamount.Text = String.Format("{0:C}", TotalPaybleamount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            }
            else
            {
                lblTotalQuantity.Text = "-" + String.Format("{0:C}", TotalQuantity).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalPaybleamount.Text = "-" + String.Format("{0:C}", TotalPaybleamount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            }

          //  ((Label)e.Item.FindControl("lblSumReceivedAmount")).Text = Session["_GroupReceivedAmt"].ToString();
           // ((Label)e.Item.FindControl("lblSumBalanceAmount")).Text = Convert.ToString(Convert.ToSingle(Session["_GroupReceivedAmt"]) - Convert.ToSingle(Session["_SumSaleAmt"]));
            ((Label)e.Item.FindControl("lblTotalAlterCharges")).Text = TotalAlterCharges.ToString("0.00");
            
                
        }

    }


    protected void rptrInvoiceReport_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {


        Label lblBrandName = ((Label)e.Item.FindControl("lblBrandName"));
        lblBrandName.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "MainCategoryName"));
        DataGrid dgGallery = ((DataGrid)e.Item.FindControl("dgGallery"));
        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.DataFrom = "SALESINV";
        objReports.ItemCode = Convert.ToInt32(Request["Item"]);
        objReports.storeId = Convert.ToInt32(Request["CostCenter"]);
        objReports.MainCategoryCode = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "MainCategoryCode"));
        objReports.InvDateFrom = Convert.ToDateTime(Request["From"]);
        objReports.InvDateTo = Convert.ToDateTime(Request["To"]);

        DataSet ds = new DataSet();
        TotalQuantity = 0;
        TotalPaybleamount = 0;
        TotalAlterCharges = 0;
        TotalNetPayble = 0;
        ds = objReports.getInvoiceBoutiquePaymentSheetDate();
        if (ds.Tables[0].Rows.Count > 0)
        {
            dgGallery.DataSource = ds.Tables[0];
            dgGallery.DataBind();
        }

    }

}