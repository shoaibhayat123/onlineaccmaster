﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class StockLedgerCostCenterWise : System.Web.UI.Page
{
    #region properties
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    Reports objReports = new Reports();
    decimal TotalBalance = 0;
    decimal TotalDebit = 0;
    decimal TotalCredit = 0;
    public string attachment = "attachment; filename=Ledger_Report_" + DateTime.Now.ToString("dd_MM_yyyy") + ".xls";
    #endregion
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                if (ck.GetCookieValue("CustomerCode") != "")
                {
                    Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
        }
        if (!IsPostBack)
        {
            ddlCostCenter.Focus();
            FillItemDesc();
            FillCostCenter();      
            if (Sessions.FromDate != null)
            {
                txtstartDate.Text = Convert.ToDateTime(Sessions.FromDate).ToString("dd MMM yyyy");
                txtEndDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
            }
            else
            {
                Response.Redirect("default.aspx");
            }

        }
        SetUserRight();
    }
    #endregion
    #region Fill Item Description
    protected void FillItemDesc()
    {
        objCust.ItemId = 0;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.getInventoryItems();
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlItemDesc.DataSource = ds.Tables[0];
            ddlItemDesc.DataTextField = "ItemDesc";
            ddlItemDesc.DataValueField = "ItemId";
            ddlItemDesc.DataBind();
            ddlItemDesc.Items.Insert(0, new ListItem("Select Product", "0"));


        }

    }
    #endregion
    #region btnGenrete
    protected void btnGenerate_Click(object sender, EventArgs e)
    {

        string urllast = "PrintStockLedgerCostCenterWise.aspx?itemcode=" + ddlItemDesc.SelectedValue + "&From=" + txtstartDate.Text + "&To=" + txtEndDate.Text + "&CostCenter=" + ddlCostCenter.SelectedValue;
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);


    }
    #endregion

    #region Fill Cost Center
    protected void FillCostCenter()
    {
        
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.CostCenterCode = 0;
            DataSet ds = new DataSet();
            ds = objCust.GetCostCenter();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlCostCenter.DataSource = ds.Tables[0];
                ddlCostCenter.DataTextField = "CostCenterName";
                ddlCostCenter.DataValueField = "CostCenterCode";
                ddlCostCenter.DataBind();
                ddlCostCenter.Items.Insert(0, new ListItem("Select cost center", "0"));


            }


    }
    #endregion
    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }
            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }

            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

                btnGenerate.Enabled = false;

            }

        }
    }
}
