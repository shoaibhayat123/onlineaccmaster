﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="LocalCustoms.aspx.cs" Inherits="LocalCustoms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Local Customs"></asp:Label>
    <script type="text/javascript">

        function Count(text, long) {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }

        function replaceAll(txt, replace, with_this) 
        {
            return txt.replace(new RegExp(replace, 'g'), with_this);
        }
        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                        num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }
      

          
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnInvoiceId" runat="server" />
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                width="100%">
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 110px;">
                        Voucher date :
                    </td>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 5px; text-align: left;">
                        <table>
                            <tr>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 0px;">
                                    <asp:HiddenField ID="hdnInvoiceSummaryId" runat="server" />
                                    <asp:TextBox ID="txtVoucherDate" TabIndex="1" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtVoucherDate'),this);"
                                        runat="server" Width="100px"></asp:TextBox>
                                    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                        onclick="scwShow(scwID('ctl00_MainHome_txtVoucherDate'),this);" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill Voucher Date." Text="*" ControlToValidate="txtVoucherDate"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                        ControlToValidate="txtVoucherDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid Voucher Date !!"
                                        Display="None"></asp:RegularExpressionValidator>
                                     <span id="spanPgging" runat="server" style="padding: 0 20px 0 20px; font-weight: bold;
                                        font-size: 18px">
                                        <asp:LinkButton ID="lnkFirst" Text="|<" runat="server" OnClick="lnkFirst_Click"> </asp:LinkButton>
                                        <asp:LinkButton ID="lnkPrevious" Style="margin-left: 10px;" Text="<" runat="server"
                                            OnClick="lnkPrevious_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkNext" Text=">" Style="margin-left: 10px;" runat="server" OnClick="lnkNext_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnLast" Text=">|" Style="margin-left: 10px;" runat="server" OnClick="lnLast_Click"></asp:LinkButton></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 111px; padding: 2px 2px 2px 8px;">
                        Container # :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                        <asp:DropDownList ID="ddlContainer" AutoPostBack="true" OnSelectedIndexChanged="ddlContainer_SelectedIndexChanged" TabIndex="2" runat="server" Style="width: 550px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select Container Number" Text="*" ControlToValidate="ddlContainer"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table style="width: 59%">
                            <tr>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 108px">
                                    Bill Of Entry # :
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                    <asp:TextBox ID="txtEntryNO" TabIndex="3" MaxLength="25" runat="server" Width="100px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Enter Bill Of Entry #" Text="*" ControlToValidate="txtEntryNO"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                </td>
                                <td align="right" valign="top" style="padding: 2px 2px 2px 8px;">
                                    B/E Date:
                                </td>
                                <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 150px;">
                                    <asp:TextBox ID="txtBEDate" TabIndex="4" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtBEDate'),this);"
                                        runat="server" Width="100px"></asp:TextBox>
                                    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                        onclick="scwShow(scwID('ctl00_MainHome_txtBEDate'),this);" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill B/E Date." Text="*" ControlToValidate="txtVoucherDate"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                        ControlToValidate="txtBEDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid  B/E Date !!"
                                        Display="None"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="sundryDR" runat="server" style="height: 400px; width: 780px; padding: 10px;
                            overflow: auto; border: solid 2px  #528627">
                            <table>
                                <tr>
                                    <td style="margin-right: 10px; width: 110px;">
                                        Expenses :
                                    </td>
                                    <td colspan="5" style="margin-right: 10px;">
                                        <asp:DropDownList ID="ddlExpenses" TabIndex="5" runat="server" Style="width: 642px">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Display="Dynamic"
                                            InitialValue="0" ErrorMessage="Please select Expense" Text="*" ControlToValidate="ddlExpenses"
                                            ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;">
                                        Receipt No. :
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtReceipt" TabIndex="6" runat="server" Style="width: 150px; text-transform: uppercase;"
                                            MaxLength="25"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill  Receipt No " Text="*" ControlToValidate="txtReceipt"
                                            ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="margin-right: 10px; width: 80px;">
                                        Amount :
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtamount" TabIndex="7" runat="server" onChange="isNumericDecimal(this)"
                                            Style="width: 150px;" MaxLength="15"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill Amount " Text="*" ControlToValidate="txtamount" ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px; width: 80px;">
                                        Date From :
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtDateFrom" TabIndex="8" runat="server" Style="width: 150px;" MaxLength="15"></asp:TextBox>
                                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill  Date From " Text="*" ControlToValidate="txtDateFrom"
                                            ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>--%>
                                    </td>
                                    <td style="margin-right: 10px; width: 80px;">
                                        Date To :
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtDateTo" TabIndex="9" runat="server" Style="width: 150px;" MaxLength="15"></asp:TextBox>
                                     <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill  Date To " Text="*" ControlToValidate="txtDateTo" ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px; width: 110px;">
                                        Credit Account :
                                    </td>
                                    <td colspan="5" style="margin-right: 10px;">
                                        <asp:DropDownList ID="ddlCredit" TabIndex="10" runat="server" Style="width: 642px">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                                            InitialValue="0" ErrorMessage="Please select  Credit Account" Text="*" ControlToValidate="ddlCredit"
                                            ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px; width: 110px;">
                                        Debit Account :
                                    </td>
                                    <td colspan="5" style="margin-right: 10px;">
                                        <asp:DropDownList ID="ddlDebit" TabIndex="11" runat="server" Style="width: 642px">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" Display="Dynamic"
                                            InitialValue="0" ErrorMessage="Please select Debit Account" Text="*" ControlToValidate="ddlDebit"
                                            ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px; width: 80px;">
                                    </td>
                                    <td colspan="5" style="text-align: right">
                                        <asp:Button TabIndex="12" Style="text-align: right" ID="btnAddNew" OnClick="btnAddNew_Click"
                                            runat="server" Text="Update" ValidationGroup="AddInvoiceDetail" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                            ShowSummary="false" ValidationGroup="AddInvoiceDetail" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6">
                                        <asp:HiddenField ID="hdndetail" runat="server" />
                                        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                                            AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                            AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                            BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery_RowDataBound"
                                            OnItemCommand="dgGallery_ItemCommand">
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="S.No.">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataSetIndex + 1%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Description">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="50%" HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        <%# Eval("CustomExpense")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Date From">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="15%" HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        <%# Eval("DateFrom")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Date To">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="15%" HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        <%# Eval("DateTo")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Amount">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="15%" HorizontalAlign="right" />
                                                    <ItemTemplate>
                                                        <%# Convert.ToDecimal(Eval("Amount")).ToString("0.00")  %>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Account(Credit)">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="15%" HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        <%# Eval("AccountCRAcfile")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Account(Debit)">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="15%" HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        <%# Eval("AccountDRAcfile")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                               
                                                <asp:TemplateColumn HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="imgDel1" runat="server" Text="Delete" CommandArgument='<%#Eval("Id")%>'
                                                            CommandName="Delete" OnClientClick="return askDeleteRow();" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Edit">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="ImageButton1" runat="server" CommandArgument='<%#Eval("Id")%>'
                                                            Text="Edit" CommandName="Edit" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>

                            </table>
                        </div>
                    </td>
                </tr>
                 <tr>
                    <td colspan="2" style="text-align: left; width: 800px;">
                        <table style="width: 800px;">
                            <tr>
                                <td>
                                    Totals :: 
                                    <b>Amount:</b>
                                    <asp:Label ID="lblTotalAmount" runat="server" Text="0.00" Style="margin-right: 20px;padding-left: 5px"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                 <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td style="width: 450px;">
                                    <span>
                                        <asp:Button ID="lblbutton" Style="cursor: pointer" runat="server" Text="Add New Invoice"
                                            OnClick="lblbutton_Click" />
                                        <asp:Button Text="Delete Invoice" OnClientClick="return askDeleteInvoice();" ID="btnDeleteInvoice"
                                            runat="server" OnClick="btnDeleteInvoice_Click" />
                                       
                                </td>
                                <td style="width: 550px">
                                    <table cellpadding="0" cellspacing="0" style="padding: 5px 155px 0px 0px; width: 100%">
                                        <tr>
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 33%;">
                                            </td>
                                            <td align="left" valign="top" style="padding: 2px 0px 2px 5px; text-align: right;
                                                width: 33%;">
                                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                                                    ShowSummary="false" ValidationGroup="AddInvoice" />
                                                <asp:ImageButton ID="ImageButton2" TabIndex="13" runat="server" ImageUrl="~/Images/btn-save.gif"
                                                    OnClick="btn_saveInvoice_Click" ValidationGroup="AddInvoice" />&nbsp;
                                                    </div>
                                            </td>
                                            <td style="width: 33%; padding: 2px 2px 20px 3px;">
                                                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/bt-cancal.gif"
                                                    OnClick="btn_cancel_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
