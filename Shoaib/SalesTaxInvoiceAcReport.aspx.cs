﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class SalesTaxInvoiceAcReport : System.Web.UI.Page
{
    #region properties
    clsCookie ck = new clsCookie();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();
    Reports objReports = new Reports();   
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
         if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
         if (!IsPostBack)
         {
             txtstartDate.Text = Convert.ToDateTime(Sessions.FromDate).ToString("dd MMM yyyy");
             txtEndDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
             FillClientAC();
             FillItemDesc();
             FillCostCenter();
             FillMainCategory();
             FillSubCategory();
         }
         SetUserRight();
    }
    #region btnGenrete
    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        string subcategory = ddlSubCategory.SelectedValue;
        if(subcategory =="")
        {
            subcategory = "0";
        }
        string urllast = "SalesTaxInvoiceAcReportPrint.aspx?Client=" + ddlClientAC.SelectedValue + "&From=" + txtstartDate.Text + "&To=" + txtEndDate.Text + "&CostCenter=" + ddlCostCenter.SelectedValue + "&Item=" + ddlItemDesc.SelectedValue + "&MainCategoryCode=" + ddlMainCategory.SelectedValue + "&SubCategoryCode=" + subcategory;
            ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open('" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);
    }
    #endregion
    #region Fill  Client A/C
    protected void FillClientAC()
    {
        
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtClient = new DataTable();
            dtClient = objSalesInvoice.getAcFileBySundryDebtorOnly();
            if (dtClient.Rows.Count > 0)
            {
                ddlClientAC.DataSource = dtClient;
                ddlClientAC.DataTextField = "Title";
                ddlClientAC.DataValueField = "Id";
                ddlClientAC.DataBind();
                ddlClientAC.Items.Insert(0, new ListItem("Select Client A/c", "0"));

            }

        

    }
    #endregion
    #region Fill Item Description
    protected void FillItemDesc()
    {
        objCust.ItemId = 0;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlItemDesc.DataSource = ds.Tables[0];
            ddlItemDesc.DataTextField = "ItemDesc";
            ddlItemDesc.DataValueField = "ItemId";
            ddlItemDesc.DataBind();
            ddlItemDesc.Items.Insert(0, new ListItem("Select  Item", "0"));
        }

    }
    #endregion
    #region Fill Cost Center
    protected void FillCostCenter()
    {
        
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.CostCenterCode = 0;
            DataSet ds = new DataSet();
            ds = objCust.GetCostCenter();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlCostCenter.DataSource = ds.Tables[0];
                ddlCostCenter.DataTextField = "CostCenterName";
                ddlCostCenter.DataValueField = "CostCenterCode";
                ddlCostCenter.DataBind();
                ddlCostCenter.Items.Insert(0, new ListItem("Select cost center", "0"));

            }

       

    }
    #endregion

    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }
            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }

            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

                btnGenerate.Enabled= false;

            }

        }
    }


    #region Fill Main Category
    protected void FillMainCategory()
    {
        SetCustomers objCust = new SetCustomers();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.MainCategoryCode = 0;
        DataSet ds = new DataSet();
        ds = objCust.GetMainCategory();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlMainCategory.DataSource = ds.Tables[0];
            ddlMainCategory.DataTextField = "MainCategoryName";
            ddlMainCategory.DataValueField = "MainCategoryCode";
            ddlMainCategory.DataBind();
            ddlMainCategory.Items.Insert(0, new ListItem("Select Main Category", "0"));
        }


    }
    #endregion
    #region Fill Sub Category

    protected void FillSubCategory()
    {
        SetCustomers objCust = new SetCustomers();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.MainCategoryCode = Convert.ToInt32(ddlMainCategory.SelectedValue);
        DataSet ds = new DataSet();
        ds = objCust.GetSubCategoryByMainCategory();
        //if (ds.Tables[0].Rows.Count > 0)
        //{
            ddlSubCategory.DataSource = ds.Tables[0];
            ddlSubCategory.DataTextField = "SubCategoryName";
            ddlSubCategory.DataValueField = "SubCategoryCode";
            ddlSubCategory.DataBind();
            ddlSubCategory.Items.Insert(0, new ListItem("Select Sub Category", "0"));
        //}

    }
    #endregion
    protected void ddlMainCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSubCategory();
        ddlSubCategory.Focus();
    }
}
