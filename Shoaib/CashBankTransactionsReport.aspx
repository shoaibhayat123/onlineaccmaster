﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="CashBankTransactionsReport.aspx.cs" Inherits="CashBankTransactionsReport"
    Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Cash / Bank Book Transaction Report"></asp:Label>
    <script type="text/javascript">

        function Count(text, long) {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }
          
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnInvoiceId" runat="server" />
            <%--<table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                width="70%">
                <tr>
                   
                    <td style="text-align: left" align="left">
                        Inv.Date From:
                        <asp:TextBox ID="txtInvoiceDateFrom" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateFrom'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateFrom'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill Invoice Date." Text="*" ControlToValidate="txtInvoiceDateFrom"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtInvoiceDateFrom" ValidationGroup="AddInvoice" ErrorMessage="Invalid Invoice Date !!"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                    <td style="text-align: left" align="left">
                        To :
                        <asp:TextBox ID="txtInvoiceDateTo" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateTo'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateTo'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill Invoice Date." Text="*" ControlToValidate="txtInvoiceDateTo"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtInvoiceDateTo" ValidationGroup="AddInvoice" ErrorMessage="Invalid Invoice Date !!"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                    <td>
                            Cash/Bank A/c :
                            <asp:DropDownList ID="ddlClientAC" runat="server" Style="width: 270px">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2" style="text-align: left">
                            <asp:Button ID="btnsearch" Text="Search" runat="server" OnClick="btnsearch_Click" />
                        </td>
                </tr>
                <tr>
                    <tr>
                        
                    </tr>
            </table>--%>
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Cash/Bank A/c :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:DropDownList ID="ddlClientAC" runat="server" Style="width: 270px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Date From:
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:TextBox ID="txtInvoiceDateFrom" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateFrom'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateFrom'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill From Date." Text="*" ControlToValidate="txtInvoiceDateFrom"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtInvoiceDateFrom" ValidationGroup="AddInvoice" ErrorMessage="Invalid Invoice From Date !!"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Date To :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:TextBox ID="txtInvoiceDateTo" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateTo'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateTo'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill To Date." Text="*" ControlToValidate="txtInvoiceDateTo"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtInvoiceDateTo" ValidationGroup="AddInvoice" ErrorMessage="Invalid  To Date !!"
                            Display="None"></asp:RegularExpressionValidator>
                        <span style="float: right; padding-right: 65px">
                            <asp:Button ID="btnsearch" Text="Search" TabIndex="6" runat="server" OnClick="btnsearch_Click" /></span>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" style="float: left; padding: 0 10px 10px 10px;" border="0"
                cellpadding="0" width="90%">
                <tr>
                    <td colspan="3">
                        <asp:GridView ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                            AllowPaging="true" PageSize="500000" PagerSettings-Visible="false" PagerSettings-Mode="Numeric"
                            HeaderStyle-CssClass="gridheader" AlternatingRowStyle-CssClass="gridAlternateItem" OnRowCommand="grdCustomer_RowCommand"
                            OnRowDataBound="dgGallery_RowDataBound" GridLines="none" BorderColor="black"
                            FooterStyle-CssClass="gridFooter" BorderStyle="Dotted" BorderWidth="1" RowStyle-CssClass="gridItem"
                            ShowFooter="true">
                            <EmptyDataTemplate>
                                <div style="padding-bottom: 10px; width: 100%; text-align: center; font-family: Gisha;
                                    font-size: 20px; background-color: #E1F5FD; color: #110F0F;">
                                    <div style="padding-top: 10px; padding-bottom: 5px;">
                                        No record found
                                    </div>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cash/Bank A/C">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Eval("Title")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText=" Transaction Date">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblinvDate" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Receive">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("TotalReceive")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalTotalReceive" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Payment">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("TotalPayment")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalPayment" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Edit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                    <%--    <a href='CashBankTransactions.aspx?InvoiceSummaryId=<%#Eval("Id") %>'>
                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" /></a>--%>
                                                     <asp:Button ID="btnEdit" CommandName="EditCustomer" CommandArgument='<%#Eval("id") %>' runat="server" Text="Edit" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
