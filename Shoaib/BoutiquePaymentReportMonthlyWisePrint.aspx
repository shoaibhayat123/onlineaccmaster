﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BoutiquePaymentReportMonthlyWisePrint.aspx.cs"
    Inherits="BoutiquePaymentReportMonthlyWisePrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Tahoma; background: none; font-size: 10px;">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; font-weight: bold;">
             <tr id="trTrialBal" runat="server">
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Payment
                        Sheet Monthly Designer Wise<br />
                    </span><span style="font-size: 14px;">From :
                        <asp:Label ID="lblFromDate" runat="server" Style="padding-right: 15px;"></asp:Label>
                        To :
                        <asp:Label ID="lblToDate" runat="server"></asp:Label><br />
                        <span style="display: none">Conversion Rate :
                            <asp:Label ID="lblConversionRate" runat="server"></asp:Label><br />
                        </span></span>
                    <asp:Label ID="lblBuyerName" runat="server"></asp:Label><br />
                    <span style="font-size: 12px; font-weight: normal">
                        <asp:Label ID="lblBuyerAddress" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress2" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress3" runat="server"></asp:Label></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <hr />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <asp:Repeater ID="rptrInvoiceReport" OnItemDataBound="rptrInvoiceReport_OnItemDataBound"
            runat="server">
            <ItemTemplate>
                <table style="width: 800px; font-weight: bold; font-size: 12px;">
                    <tr>
                        <td style="font-weight: bold; width: 15%; text-align: left">
                            Brand :
                            <asp:Label ID="lblBrandName" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" FooterStyle-CssClass="gridFooter"
                    HeaderStyle-CssClass="gridheader" ShowFooter="true" AlternatingItemStyle-CssClass="gridAlternateItem"
                    GridLines="none" BorderColor="black" OnItemDataBound="dgGallery_OnItemDataBound"
                    BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem">
                    <Columns>
                        <asp:TemplateColumn HeaderText="S.No.">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <%# Container.DataSetIndex + 1%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="TYPE">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="8%" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblType" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Invoice #">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="5%" />
                            <ItemTemplate>
                                <%# Eval("InvNo")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Invoice Date">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="5%" />
                            <ItemTemplate>
                                <asp:Label ID="lblinvDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Item Description">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="25%" />
                            <ItemTemplate>
                                <%# Eval("ItemDesc") %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Label">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="20%" />
                            <ItemTemplate>
                                <%# Eval("SubcategoryName")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Quantity">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="10%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalQuantity" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Designer Price">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="10%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="lblDesignerPrice" runat="server"></asp:Label>
                                <%--     <%# Eval("DesignerPrice")%>--%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--    <asp:TemplateColumn HeaderText="Sales Price">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="10%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                            </ItemTemplate>
                        </asp:TemplateColumn>--%>
                        <asp:TemplateColumn HeaderText="Sale Discount %">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="10%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%# Eval("BoutiqueDiscount")%>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText=" Discount Amount">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="10%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="lblDiscountAmount" runat="server"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Actual Sale Price">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="10%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%--   <%# Eval("BoutiqueAmountAfterDis")%>--%>
                                <asp:Label ID="lblActualSalesAmount" runat="server"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Cost Price" Visible="false">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="10%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="lblPurchaseCostPrice" runat="server"></asp:Label>
                                <%--<%#String.Format("{0:C}", Eval("BoutiquePurchaseCostPrice")).Replace('$', ' ')%>--%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalGrossAmount" Style="display: none" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Charges">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="10%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%-- <%#String.Format("{0:C}", Eval("AlterCharges")).Replace('$', ' ')%>--%>
                                <asp:Label ID="lblAlterCharges" runat="server"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label Style="display: none" ID="lblTotalAlterCharges" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Net Amount">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="20%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="lblNetAmount" runat="server"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Net Payable %">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="20%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="lblNetPayable" runat="server"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Net Payable Amount">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="20%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="lblNetPayableAmount" runat="server"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalNetPayable" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Amount" Visible="false">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="20%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="lblPayableAmount" runat="server"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalPaybleamount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Received" Visible="false">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="20%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="lblReceivedAmount" runat="server"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumReceivedAmount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Balance" Visible="false">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="20%" HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label ID="lblBalanceAmount" runat="server"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumBalanceAmount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <br />
            </ItemTemplate>
        </asp:Repeater>
        <table style="width: 800px; display: none">
            <tr>
                <td style="width: 60%; font-weight: bold; text-align: right">
                    Grand Total :
                </td>
                <td style="width: 10%; font-weight: bold; text-align: right">
                    <asp:Label ID="GrandlblTotalQuantity" Style="border-bottom: solid 1px black; border-top: solid 1px black;"
                        runat="server"></asp:Label>
                </td>
                <td style="width: 35%; font-weight: bold; text-align: right">
                    <asp:Label ID="GrandlblTotalGrossAmount" Style="border-bottom: solid 1px black; border-top: solid 1px black;"
                        runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>
                                <script type="text/javascript">

                                    var currentTime = new Date()
                                    var hours = currentTime.getHours()
                                    var Newhours = 0;
                                    var minutes = currentTime.getMinutes()
                                    if (minutes < 10) {
                                        minutes = "0" + minutes
                                    }
                                    if (hours > 12) {
                                        Newhours = hours - 12;
                                    }
                                    else {
                                        Newhours = hours;
                                    }
                                    document.write(Newhours + ":" + minutes + " ")
                                    if (hours > 11) {
                                        document.write("PM")
                                    } else {
                                        document.write("AM")
                                    }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
