﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaticPages.master" AutoEventWireup="true"
    CodeFile="Security.aspx.cs" Inherits="Security" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content_wraper5">
        <div id="content_midd5">
            <div class="acc-features-middle-bg">
                <div class="top-heading">
                    <h1>
                        Security Measures</h1>
                </div>
            </div>
            <div class="accmiddle-features-bg-2" style="min-height: 520px">
                <div class="features-wrapper">
                    <div class="features-box">
                      <%--  <div class="features-left-link-box">
                            <div class="left-link-Content">
                                <h2>
                                    Privacy Topics:</h2>
                                <ul>
                                    <li><a href="#">Privacy at AccMaster</a></li>
                                    <li><a href="#">Data Stewardship</a></li>
                                    <li><a href="#">AccMaster Services</a>
                                        <ul>
                                            <li><a href="#">Online Services</a></li>
                                            <li><a href="#">Mobile Services</a></li>
                                            <li><a href="#">On Your Computer</a></li>
                                            <li><a href="#">For Individuals</a></li>
                                            <li><a href="#">For Businesses</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="left-link-bottom">
                                <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                            </div>
                        </div>--%>


                        <div style=" float:left">
                        <div class="features-left-link-box">
                            <div class="left-link-Content">
                                <h2 style=" text-decoration:underline; font-size:18px;">
                                  AccMaster</h2><br />
                                <ul style=" margin-left:20px">
                                    <li><a href="index.aspx">Home</a></li>
                                    <li><a href="features.aspx">Features</a></li>
                                    <li><a href="pricing.aspx">Product & Pricing</a></li>
                                    <li><a href="PayOnline.aspx">Pay Online</a></li>
                                    <li><a href="Privacy.aspx">Privacy</a></li>
                                    <li><a href="Terms.aspx">Terms</a></li>
                                    <li><a href="ContactUs.aspx">Contact us</a></li>
                                    <li><a href="AboutUs.aspx">About Us</a></li>
                                    <li><a href="Careers.aspx">Careers</a></li>
                                </ul>
                            </div>
                            <div class="left-link-bottom">
                                <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                            </div>
                        </div>
                         <div class="features-left-link-box" style=" clear:both; margin-top:20px;">
                           <div class="left-link-Content" style="min-height: 100px;line-height:0.6">
                                    <asp:Literal ID="ltrlAddress" runat="server"></asp:Literal>
                                </div>
                            <div class="left-link-bottom">
                                <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                            </div>
                        </div>
                        </div>
                        <div class="feat-right-part-box">
                            <div class="feta-right-part-content">
                                <div class="feat-right-box-dtls">
                                    <div class="ft-right-box-details" style="background: url('images/round_logo.png') no-repeat scroll right top transparent;
                                        height: 200px;">
                                        <h1>
                                            Security you can trust</h1>
                                        <h4>
                                            We use several layers of security measures to keep your<br />
                                            information out of the wrong hands. We understand how<br />
                                            important it is to keep information safe, so we spent a great<br />
                                            deal of time developing our security package. To ensure your<br />
                                            data is protected, every security measure is executed on every<br />
                                            page in our system.</h4>
                                    </div>
                                    <div class="what-type">
                                        <div class="what-type-image">
                                            <img src="images/Points.png" alt="" />
                                        </div>
                                        <div class="what-type-content">
                                            <h2>
                                                Your data is protected and private.</h2>
                                            <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                rely on advanced, industry-recognized security safeguards to keep all of your financial
                                                data private and protected. AccMaster® Online is a Rapid SSL product. Rapid SSL
                                                is the leading secure sockets layer (SSL) Certificate Authority. With password-protected
                                                login, firewall protected servers, and the same encryption technology (128 bit SSL)
                                                used by the world's top banking institutions, we have the security elements in place
                                                to give you peace of mind.</p>
                                        </div>
                                    </div>
                                    <div class="what-type">
                                        <div class="what-type-image">
                                            <img src="images/Points.png" alt="" />
                                        </div>
                                        <div class="what-type-content">
                                            <h2>
                                                You'll never forget to backup your data.</h2>
                                            <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                That's because we automatically do it for you. Your information is saved to the
                                                same offsite. You get the convenience of automatic offsite storage without the extra
                                                effort and cost of creating and managing physical backup copies on your own. And
                                                should the unexpected ever happen to your system, all of data will still be instantly
                                                accessible to you from any computer connected to the Internet.</p>
                                        </div>
                                    </div>
                                    <div class="what-type">
                                        <div class="what-type-image">
                                            <img src="images/Points.png" alt="" />
                                        </div>
                                        <div class="what-type-content">
                                            <h2>
                                                AccMaster® is always available, whenever you need it.</h2>
                                            <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                AccMaster® Online has been available greater than 99.8% of the time for the past
                                                three years. That's because we rely on redundant servers and a self-correcting error
                                                detection program. So even if one server becomes impacted or unavailable, your service
                                                will probably not be affected. This means that you can access your data online no
                                                matter where you are or what you are doing.</p>
                                        </div>
                                    </div>
                                    <div class="what-type">
                                        <div class="what-type-image">
                                            <img src="images/Points.png" alt="" />
                                        </div>
                                        <div class="what-type-content">
                                            <h2>
                                                You get to make the rules.</h2>
                                            <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                You control who accesses your financial data and what they can see and do with it.
                                                Each person you invite to use AccMaster® Online must create a unique password and
                                                no one — not even the Chairman of AccMaster® — can see it. We also offer multiple
                                                permission levels that let you limit the access privileges of each user. This means
                                                that if you want your part-time contractor to be able enter his hours, but not access
                                                your latest P&L charts, AccMaster® Online will make it so.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="feat-right-part-bottom">
                                <img src="images/feat-right-part-bottom-bg.jpg" alt="" />
                            </div>
                        </div>
</asp:Content>
