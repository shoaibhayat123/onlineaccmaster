﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Welcome : System.Web.UI.Page
{
    Reports objreport = new Reports();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();
    DataTable dtSalesReportMonth = new DataTable();
    string AcFileId = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillYear();
            ddlYear.SelectedValue = System.DateTime.Now.Year.ToString();
            lblTitle.Text = "Welcome " + Sessions.CustomerName;
            BindAgingResult();
            bindMonthlyReport();
            BindGraph();
          
        }
      
    }

    protected void BindAgingResult()
    {
        objreport.AcFileId = 10;
        objreport.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtLedger = new DataTable();
        dtLedger = objreport.getChildLedger();
        DataTable dtSalesReport = new DataTable();
        if (dtLedger.Rows.Count > 0)
        {
            for (int i = 0; i < dtLedger.Rows.Count; i++)
            {

                AcFileId += Convert.ToString(dtLedger.Rows[i]["id"]) + ",";

            }
        }    

    }

    protected void bindMonthlyReport()
    {

        for (int i = 1; i <= 12; i++)
        {
            objreport.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objreport.year = Convert.ToInt32(ddlYear.SelectedValue);           
            objreport.Month = i;
            objreport.AcFileIdText = AcFileId;

            DataTable dtRusult = new DataTable();
            dtRusult = objreport.getSalesVSsRecoveryYearly();
            if (dtRusult.Rows.Count > 0)
            {
                if (i == 1)
                {
                    dtSalesReportMonth = dtRusult.Copy();
                }
                else
                {
                    dtSalesReportMonth.Merge(dtRusult);
                }
            }
         }
        
    }

    protected void FillYear()
    {
        ArrayList YearArrayList = new ArrayList();
        for (int i = 0; i < 91; i++)
        {
            int year = 1980 + i;
            YearArrayList.Add(year.ToString());
        }
        ddlYear.DataSource = YearArrayList;
        ddlYear.DataBind();
        ddlYear.Items.Insert(0, (new ListItem("Select Year", "0")));
    }
   

    protected void BindGraph()
    {
     
    }





    private DataTable CreateDataTable()
    {
        //Create a DataTable as the data source of the Chart control
        DataTable dt = new DataTable();

        //Add three columns to the DataTable
        dt.Columns.Add("Month");
        dt.Columns.Add("Sales");
        dt.Columns.Add("Recovery");

        DataRow dr;

        //Add rows to the table which contains some random data for demonstration
        dr = dt.NewRow();

        dr["Month"] = "Jan";

        if (Convert.ToString(dtSalesReportMonth.Rows[0]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[0]["TotalSalesAmt"]);
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[0]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[0]["TotalRecoveryAmt"]) * -1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);

        dr = dt.NewRow();

        dr["Month"] = "Feb";

        if (Convert.ToString(dtSalesReportMonth.Rows[1]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[1]["TotalSalesAmt"]);
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[1]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[1]["TotalRecoveryAmt"]) * -1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);

        dr = dt.NewRow();

        dr["Month"] = "Mar";

        if (Convert.ToString(dtSalesReportMonth.Rows[2]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[2]["TotalSalesAmt"]);
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[2]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[2]["TotalRecoveryAmt"]) * -1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);

        dr = dt.NewRow();

        dr["Month"] = "Apr";

        if (Convert.ToString(dtSalesReportMonth.Rows[3]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[3]["TotalSalesAmt"]);
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[3]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[3]["TotalRecoveryAmt"]) * -1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);


        dr = dt.NewRow();

        dr["Month"] = "May";

        if (Convert.ToString(dtSalesReportMonth.Rows[4]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[4]["TotalSalesAmt"]);
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[4]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[4]["TotalRecoveryAmt"]) * -1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);

        dr = dt.NewRow();




        dr["Month"] = "Jun";

        if (Convert.ToString(dtSalesReportMonth.Rows[5]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[5]["TotalSalesAmt"]);
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[5]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[5]["TotalRecoveryAmt"]) * -1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);



        dr = dt.NewRow();
        dr["Month"] = "Jul";

        if (Convert.ToString(dtSalesReportMonth.Rows[6]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[6]["TotalSalesAmt"]);
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[6]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[6]["TotalRecoveryAmt"]) * -1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);



        dr = dt.NewRow();
        dr["Month"] = "Aug";

        if (Convert.ToString(dtSalesReportMonth.Rows[7]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[7]["TotalSalesAmt"]);
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[7]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[7]["TotalRecoveryAmt"]) * -1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);




        dr = dt.NewRow();
        dr["Month"] = "Sep";

        if (Convert.ToString(dtSalesReportMonth.Rows[8]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[8]["TotalSalesAmt"]);
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[8]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[8]["TotalRecoveryAmt"]) * -1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Month"] = "Oct";

        if (Convert.ToString(dtSalesReportMonth.Rows[9]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[9]["TotalSalesAmt"]);
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[9]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[9]["TotalRecoveryAmt"]) * -1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Month"] = "Nov";

        if (Convert.ToString(dtSalesReportMonth.Rows[10]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[10]["TotalSalesAmt"]);
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[10]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[10]["TotalRecoveryAmt"]) * -1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);


        dr = dt.NewRow();
        dr["Month"] = "Dec";

        if (Convert.ToString(dtSalesReportMonth.Rows[11]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[11]["TotalSalesAmt"]);
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[11]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[11]["TotalRecoveryAmt"]) * -1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);
        return dt;
    }



    #region btnGenrete
    protected void btnGenerate_Click(object sender, EventArgs e)
    {

        BindAgingResult();
        bindMonthlyReport();
        BindGraph();


    }
    #endregion


}
