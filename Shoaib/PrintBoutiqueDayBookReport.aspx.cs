﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintBoutiqueDayBookReport : System.Web.UI.Page
{


    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    clsCookie ck = new clsCookie();
    decimal TotalOpbal = 0;
    decimal TotalTransections = 0;
    decimal TotalOpeningQuantity = 0;
    decimal TotalPurchaseRate = 0;
    decimal TotalProduction = 0;
    decimal TotalSalesReturn = 0;
    decimal TotalPurchaseRateAdjustment = 0;
    decimal AllTotalStockReceived = 0;
    decimal TotalSales = 0;
    decimal TotalConsumption = 0;
    decimal TotalPurchaseRateReturn = 0;
    decimal TotalSalesAdjustment = 0;
    decimal AllTotalStockSale = 0;
    decimal TotalClosingStock = 0;
  

    decimal AllTotalOpbal = 0;
    decimal AllTotalTransections = 0;
    decimal AllTotalOpeningQuantity = 0;
    decimal AllTotalPurchaseRate = 0;
    decimal AllTotalProduction = 0;
    decimal AllTotalSalesReturn = 0;
    decimal AllTotalPurchaseRateAdjustment = 0;
    decimal CompleteTotalStockReceived = 0;
    decimal AllTotalSales = 0;
    decimal AllTotalConsumption = 0;
    decimal AllTotalPurchaseRateReturn = 0;
    decimal AllTotalSalesAdjustment = 0;
    decimal CompleteTotalStockSale = 0;
    decimal AllTotalClosingStock = 0;
    decimal AllTotalAmount = 0;

    decimal GroupTotalOpeningQuantity = 0;
    decimal GroupTotalPurchaseRate = 0;
    decimal GroupTotalAmount = 0;



    decimal TotalDebit = 0;
    decimal TotalCredit = 0;

    Reports objReports = new Reports();
    SetCustomers objCust = new SetCustomers();
    DataTable dttbl = new DataTable();
    Car objCar = new Car();
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {

            Session["s"] = null;
            bindSupplier();
            BindBasicSearch();
            FillLogo();
            lblFromDate.Text = ClsGetDate.FillFromDate(Request["From"].ToString());
            lblToDate.Text = ClsGetDate.FillFromDate(Request["To"].ToString());
            lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
            lblUser.Text = Sessions.UseLoginName.ToString();


        }
    }

    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();

        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }

    }
    #endregion

    protected void dgGallery_OnItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Label)e.Item.FindControl("lblDate")).Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "TransactionDate").ToString());
            
            if (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt")) > 0)
            {
                ((Label)e.Item.FindControl("lblDebit")).Text = String.Format("{0:C}", Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt"))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');  
                TotalDebit += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt"));
            }
            else
            {
                ((Label)e.Item.FindControl("lblCredit")).Text = String.Format("{0:C}", Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt"))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                TotalCredit += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt"));
            }

          //  ((Label)e.Item.FindControl("lblTotalAmountSum")).Text = TotalAmount.ToString("0.00");

       //     TotalAmount += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "totalBillamount"));

        }
        if (e.Item.ItemType == ListItemType.Footer)
        {

            ((Label)e.Item.FindControl("lblTotalDebit")).Text = String.Format("{0:C}", TotalDebit).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
             //   TotalDebit.ToString("0.00");
            ((Label)e.Item.FindControl("lblTotalCredit")).Text = String.Format("{0:C}", TotalCredit).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
           // TotalCredit.ToString("0.00").Replace("-", "");

        }

    }


    protected void BindBasicSearch()
    {

        objSalesInvoice.InvDateFrom = Convert.ToDateTime(Request["From"]);
        objSalesInvoice.invDateTo = Convert.ToDateTime(Request["To"]);
        DataTable dt = new DataTable();
      
        dt = objSalesInvoice.BoutiqueDayBookReport();

        if (dt.Rows.Count > 0)
        {
            dgGallery.DataSource = dt;
            dgGallery.DataBind();

        }
    }
   



}