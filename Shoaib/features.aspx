﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaticPages.master" AutoEventWireup="true"
    CodeFile="features.aspx.cs" Inherits="features" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content_wraper5">
        <div id="content_midd5">
            <div class="acc-features-middle-bg">
                <div class="top-heading">
                    <h1>
                        Features & Tools for Small Business Accounting</h1>
                </div>
            </div>
            <div class="accmiddle-features-bg-2" style="min-height: 520px">
                <div class="features-wrapper">
                    <div class="features-box">
                    <div style=" float:left">
                        <div class="features-left-link-box">
                            <div class="left-link-Content">
                                <h2 style=" text-decoration:underline; font-size:18px;">
                                  AccMaster</h2><br />
                                <ul style=" margin-left:20px">
                                    <li><a href="index.aspx">Home</a></li>
                                    <li><a href="features.aspx">Features</a></li>
                                    <li><a href="pricing.aspx">Product & Pricing</a></li>
                                    <li><a href="PayOnline.aspx">Pay Online</a></li>
                                    <li><a href="Privacy.aspx">Privacy</a></li>
                                    <li><a href="Terms.aspx">Terms</a></li>
                                    <li><a href="ContactUs.aspx">Contact us</a></li>
                                    <li><a href="AboutUs.aspx">About Us</a></li>
                                    <li><a href="Careers.aspx">Careers</a></li>
                                </ul>
                            </div>
                            <div class="left-link-bottom">
                                <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                            </div>
                        </div>
                         <div class="features-left-link-box" style=" clear:both; margin-top:20px;">
                           <div class="left-link-Content" style="min-height: 100px;line-height:0.6">
                                    <asp:Literal ID="ltrlAddress" runat="server"></asp:Literal>
                                </div>
                            <div class="left-link-bottom">
                                <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                            </div>
                        </div>
                        </div>
                        <div class="feat-right-part-box" style=" vertical-align:top">
                            <div class="feta-right-part-content">
                                <div class="feat-right-box-dtls">
                                    <div class="ft-right-box-details" style="background: none">
                                        <h1>
                                            Features</h1>
                                        <h4>
                                            <strong>ACCMASTER</strong> online makes Books keeping easy and secured.
                                        </h4>
                                        <table style="width: 100%">
                                            <tr>
                                                <td width="30%" style="margin: 10%; padding-right: 15px;">
                                                    “Security of data from its storage to transmission, data encryption, password protection
                                                    makes it fully secure which is foremost for any accounting software.” - <i>Mark Anthony
                                                        (Project Manager)</i>
                                                </td>
                                                <td width="60%" style="vertical-align: top; margin-left: 15px">
                                                    <%--<iframe width="100%" height="250" src="http://www.youtube.com/embed/2HSuwY-ztAY"
                                                        frameborder="0" allowfullscreen></iframe>--%>
                                                    <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="426" height="236"
                                                        id="FLVPlayer1">
                                                        <param name="movie" value="FLVPlayer_Progressive.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="wmode" value="opaque" />
                                                        <param name="scale" value="noscale" />
                                                        <param name="salign" value="lt" />
                                                        <param name="FlashVars" value="&amp;MM_ComponentVersion=1&amp;skinName=Clear_Skin_1&amp;streamName=videos/file2&amp;autoPlay=false&amp;autoRewind=false" />
                                                        <param name="swfversion" value="8,0,0,0" />
                                                        <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
                                                        <param name="expressinstall" value="Scripts/expressInstall.swf" />
                                                        <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="FLVPlayer_Progressive.swf" width="426"
                                                            height="236">
                                                            <!--<![endif]-->
                                                            <param name="quality" value="high" />
                                                            <param name="wmode" value="opaque" />
                                                            <param name="scale" value="noscale" />
                                                            <param name="salign" value="lt" />
                                                            <param name="FlashVars" value="&amp;MM_ComponentVersion=1&amp;skinName=Clear_Skin_1&amp;streamName=videos/file2&amp;autoPlay=false&amp;autoRewind=false" />
                                                            <param name="swfversion" value="8,0,0,0" />
                                                            <param name="expressinstall" value="Scripts/expressInstall.swf" />
                                                            <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                                                            <div>
                                                                <h4>
                                                                    Content on this page requires a newer version of Adobe Flash Player.</h4>
                                                                <p>
                                                                    <a href="http://www.adobe.com/go/getflashplayer">
                                                                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
                                                                            alt="Get Adobe Flash player" /></a></p>
                                                            </div>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </td>
                                            </tr>
                                        </table>
                                        <table style="margin-top: 15px">
                                            <tr>
                                                <td style="vertical-align: top; text-align: center; width: 33%; margin-left: 15px">
                                                    <b style="margin: auto; font-size: 15px; text-decoration: underline">Application Features</b>
                                                    <ul style="text-align: left">
                                                        <li>Requires no Installations.</li>
                                                        <li>Browser Independent</li>
                                                        <li>24 Hrs. Free Support</li>
                                                        <li>Password Protected</li>
                                                        <li>Multiuser Environment</li>
                                                        <li>Secured</li>
                                                        <li>Auto Backup Facility</li>
                                                        <li>Free from Database / System crashes</li>
                                                        <li>Saves a lot of organizational precious investment on disaster mgmt.</li>
                                                        <li>Location independent</li>
                                                    </ul>
                                                </td>
                                                <td style="vertical-align: top; text-align: center; width: 33%; margin-left: 15px">
                                                    <b style="text-align: center; font-size: 15px; text-decoration: underline">Organizational
                                                        Highlights</b>
                                                    <ul style="text-align: left">
                                                        <li>Accounting</li>
                                                        <li>Costing and Controlling</li>
                                                        <li>Inventory Management</li>
                                                        <li>Sales and Procurement</li>
                                                        <li>HR management</li>
                                                        <li>Legal Issues like SALES TAX, VAT etc.</li>
                                                        <li>Reports related to all the above define modules</li>
                                                    </ul>
                                                </td>
                                                <td style="vertical-align: top; text-align: center; width: 33%; margin-left: 15px">
                                                    <b style="text-align: center; font-size: 15px; text-decoration: underline">Industry
                                                        Specific Solution include</b>
                                                    <ul style="text-align: left">
                                                        <li>Manufacturing</li>
                                                        <li>IP law firms</li>
                                                        <li>Automobile Trading (NEW / USED)</li>
                                                        <li>Designer Boutique</li>
                                                        <li>Logistics and Warehousing</li>
                                                        <li>Travels and Tours.</li>
                                                        <li>Supply Chain Management POS (FMCG)</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>
                                        <h1>
                                            Time Saving</h1>
                                        <h4 style="line-height: 18px">
                                            <strong>With ACCMASTER</strong> online you can concentrate more on business activity
                                            and less on accounting. It streamlines and automates all time consuming activities
                                            like creating invoices, estimates, reports during business hours and can help you
                                            working on such activities in your free time form anywhere.
                                        </h4>
                                        <table style="width: 100%">
                                            <tr>
                                                <td width="60%" style="vertical-align: top;">
                                                    <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="100%" height="251"
                                                        id="FLVPlayer">
                                                        <param name="movie" value="FLVPlayer_Progressive.swf" />
                                                        <param name="quality" value="high" />
                                                        <param name="wmode" value="opaque" />
                                                        <param name="scale" value="noscale" />
                                                        <param name="salign" value="lt" />
                                                        <param name="FlashVars" value="&amp;MM_ComponentVersion=1&amp;skinName=Clear_Skin_1&amp;streamName=videos/file1&amp;autoPlay=false&amp;autoRewind=false" />
                                                        <param name="swfversion" value="8,0,0,0" />
                                                        <!-- This param tag prompts users with Flash Player 6.0 r65 and higher to download the latest version of Flash Player. Delete it if you don’t want users to see the prompt. -->
                                                        <param name="expressinstall" value="Scripts/expressInstall.swf" />
                                                        <!-- Next object tag is for non-IE browsers. So hide it from IE using IECC. -->
                                                        <!--[if !IE]>-->
                                                        <object type="application/x-shockwave-flash" data="FLVPlayer_Progressive.swf" width="100%"
                                                            height="251">
                                                            <!--<![endif]-->
                                                            <param name="quality" value="high" />
                                                            <param name="wmode" value="opaque" />
                                                            <param name="scale" value="noscale" />
                                                            <param name="salign" value="lt" />
                                                            <param name="FlashVars" value="&amp;MM_ComponentVersion=1&amp;skinName=Clear_Skin_1&amp;streamName=videos/file1&amp;autoPlay=false&amp;autoRewind=false" />
                                                            <param name="swfversion" value="8,0,0,0" />
                                                            <param name="expressinstall" value="Scripts/expressInstall.swf" />
                                                            <!-- The browser displays the following alternative content for users with Flash Player 6.0 and older. -->
                                                            <div>
                                                                <h4>
                                                                    Content on this page requires a newer version of Adobe Flash Player.</h4>
                                                                <p>
                                                                    <a href="http://www.adobe.com/go/getflashplayer">
                                                                        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
                                                                            alt="Get Adobe Flash player" /></a></p>
                                                            </div>
                                                            <!--[if !IE]>-->
                                                        </object>
                                                        <!--<![endif]-->
                                                    </object>
                                                </td>
                                                <td width="30%" style="margin: 10%; padding-left: 15px;">
                                                    “The main feature of this system is it is platform independent, can be accessed
                                                    from anywhere, anytime, any system any operating system, any browser and any device.”
                                                    –<i> John (Customer)</i>
                                                </td>
                                            </tr>
                                        </table>
                                        <h1>
                                            Accessibility</h1>
                                        <h4 style="line-height: 18px">
                                            <strong>One application multi location</strong>
                                        </h4>
                                        <table>
                                            <tr>
                                                <td width="50%" style="vertical-align: top;">
                                                    <br />
                                                    <b style="text-decoration: underline">Online access</b>
                                                    <br />
                                                    You can access AccMaster at anytime from anywhere. All your precious data is stored
                                                    safely and secured online. AccMaster is browser, operating system, and device independent.<br />
                                                    You can collaborate with other users on different locations in real time and work
                                                    anywhere without ever having to sync data between devices.
                                                </td>
                                                <td width="50%" style="vertical-align: top; margin-top: 15px">
                                                    <br />
                                                    <img src="images/Org1.png" width="300px" />
                                                </td>
                                            </tr>
                                            <tr style="margin-top: 15px">
                                                <td width="50%" style="vertical-align: top; text-align: center; margin-top: 15px">
                                                    <br />
                                                    <img src="images/Org2.png" />
                                                </td>
                                                <td width="50%" style="vertical-align: center;">
                                                    <br />
                                                    <b style="text-decoration: underline">Mobile Access</b>
                                                    <br />
                                                    With mobile version you can access important transactions; reports of your precious
                                                    data from any web enabled devices line IPhone, Black Berry, Android, IPad, Playbook,
                                                    Tablets irrelevant to their Operating system.<br />
                                                    With mobile version you can grant access to your teams on road to perform real time
                                                    sales activities like acquiring sales orders, update order fulfillment on a click
                                                    of button.<br />
                                                    Mobile receivable management helps the recovery agent to update the receipt of payments
                                                </td>
                                            </tr>
                                        </table>
                                        <%--                                      <tr style="margin-top: 15px">
                                                <td width="50%" style="vertical-align: top; margin-top: 15px">
                                                    <br />
                                                    <img src="images/Org3.png" width="300px" />
                                                </td>
                                                <td width="50%" style="vertical-align: center;">
                                                    <b style="text-decoration: underline">Create / Manage Inventory</b><br />
                                                    Inventories can be managed based on product category from Raw material to Finished
                                                    Goods in various warehouse locations.
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="50%" style="vertical-align: top; margin-top: 15px">
                                                    <br />
                                                    Goods issue posting from pp academy
                                                </td>
                                                <td width="50%" style="vertical-align: center;">
                                                    <br />
                                                    <b style="text-decoration: underline">Automatic Postings</b><br />
                                                    With AccMaster you can post your inbound inventory automatically through purchase
                                                    entry, through goods issues and deduct finished goods by means of sales invoices
                                                    or delivery orders.
                                                </td>
                                            </tr>
                                            <tr style="margin-top: 15px">
                                                <td width="50%" style="vertical-align: top; margin-top: 15px">
                                                    <br />
                                                    Page not available
                                                </td>
                                                <td width="50%" style="vertical-align: center;">
                                                    <br />
                                                    <b style="text-decoration: underline">Manage Human Resource</b><br />
                                                    AccMaster does not only provide books keeping facility but is a complete solution
                                                    to business by giving an edge to manage the important human resource of an organization
                                                </td>
                                            </tr>
                                            <tr style="">
                                                <td width="50%" style="vertical-align: top; margin-top: 15px">
                                                    <br />
                                                    <img src="images/Org4.png" width="300px" />
                                                </td>
                                                <td width="50%" style="vertical-align: center;">
                                                    <br />
                                                    <b style="text-decoration: underline">Reporting</b><br />
                                                    With one click reporting AccMaster provides easy to read reports which includes
                                                    balance sheets, profit and loss, cash flow statement, taxable and non taxable activity,
                                                    inventory reports, time management reports (HR) etc.
                                                </td>
                                            </tr>
                                        </table>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="feat-right-part-bottom">
                                <img src="images/feat-right-part-bottom-bg.jpg" alt="" />
                            </div>
                        </div>
</asp:Content>
