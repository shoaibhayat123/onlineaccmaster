﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintClientAgingReports.aspx.cs"
    Inherits="PrintClientAgingReports" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Tahoma; background: none; font-size: 10px;">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; font-weight: bold;">
            <tr>
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Aging Receivable Report<br />
                    </span><span style="font-size: 14px;">
                        <asp:Label ID="lblFromDate" runat="server" Style="padding-right: 15px;"></asp:Label>
                    </span>
                    <asp:Label ID="lblBuyerName" runat="server"></asp:Label><br />
                    <span style="font-size: 12px; font-weight: normal">
                        <asp:Label ID="lblBuyerAddress" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress2" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress3" runat="server"></asp:Label></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <hr />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <asp:Repeater ID="rptrInvoiceReport" runat="server">
            <ItemTemplate>
                <table style="width: 800px; font-weight: bold; font-size: 12px;">
                    <tr>
                        <td style="font-weight: bold; width: 45%; text-align: left">
                            A/c. Title :
                            <%#Eval("Title")%>
                        </td>
                        <td style="font-weight: bold; text-align: left; margin-left: 50px;">
                            Current Balance :
                            <%# Convert.ToString(Convert.ToDecimal(Eval("OpeningBal")) + Convert.ToDecimal(Eval("SumAmount")))%>
                        </td>
                        <td style="font-weight: bold; text-align: left; margin-left: 50px;">
                        </td>
                    </tr>
                </table>
            </ItemTemplate>
        </asp:Repeater>
        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
            ShowFooter="true" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
            FooterStyle-CssClass="gridFooter" HeaderStyle-CssClass="gridheader" AlternatingItemStyle-CssClass="gridAlternateItem"
            GridLines="none" BorderColor="black" BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem"
            OnItemDataBound="dgGallery_OnItemDataBound">
            <Columns>
                <asp:TemplateColumn HeaderText="S.No.">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                    <ItemTemplate>
                        <%# Container.DataSetIndex + 1%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Transaction Date">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="20%" />
                    <ItemTemplate>
                        <asp:Label ID="lblInvDate" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Invoice no">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="10%" HorizontalAlign="left" />
                    <ItemTemplate>
                        <%# Eval("InvoiceNo")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="DataFrom">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="10%" HorizontalAlign="left" />
                    <ItemTemplate>
                        <%# Eval("DataFrom")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Description">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="30%" />
                    <ItemTemplate>
                        <%# Eval("TransDescription")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Debit Amount">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="lblTransAmt" runat="server"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalTransAmt" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Due Days">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="10%" HorizontalAlign="left" />
                    <ItemTemplate>
                       <asp:Label ID="lblduedays" runat="server"></asp:Label>
                      <%--  <%# Eval("duedays")%>--%>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>
                                <script type="text/javascript">

                                    var currentTime = new Date()
                                    var hours = currentTime.getHours()
                                    var Newhours = 0;
                                    var minutes = currentTime.getMinutes()
                                    if (minutes < 10) {
                                        minutes = "0" + minutes
                                    }
                                    if (hours > 12) {
                                        Newhours = hours - 12;
                                    }
                                    else {
                                        Newhours = hours;
                                    }
                                    document.write(Newhours + ":" + minutes + " ")
                                    if (hours > 11) {
                                        document.write("PM")
                                    } else {
                                        document.write("AM")
                                    }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
