﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class SalesReportPrint : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();
    AccFiles objFile = new AccFiles();
    Reports objReports = new Reports();
    decimal Total = 0;
    decimal TotalMonth1 = 0;
    decimal TotalMonth2 = 0;
    decimal TotalMonth3 = 0;
    decimal TotalMonth4 = 0;
    decimal TotalMonth5 = 0;
    decimal TotalMonth6 = 0;
    decimal TotalMonth7 = 0;
    decimal TotalMonth8 = 0;
    decimal TotalMonth9 = 0;
    decimal TotalMonth10 = 0;
    decimal TotalMonth11 = 0;
    decimal TotalMonth12 = 0;
    //public string attachment = "attachment; filename=Dresses27_Report_" + DateTime.Now.ToString("dd_MM_yyyy") + ".xls";
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Sessions.CustomerCode == "") || (Request["year"].ToString() == "") || (Request["Client"].ToString() == "") || (Request["Type"].ToString() == ""))
        {
            Response.Redirect("default.aspx");
        }
        if (!IsPostBack)
        {


            bindSupplier();

            lblYear.Text = Request["year"].ToString();

            if (Convert.ToInt32(Request["Client"]) > 0)
            {
                bindBuyer();
            }
            else
            {
                lblBuyerName.Text = "All Client";
            }
            FillLogo();
            // bindYearlySales();
            bindMainCategory();
            lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
            lblUser.Text = Sessions.UseLoginName.ToString();
           
            lblGrandTotal.Text = String.Format("{0:C}", Total).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblGrandTotalMonth1.Text = String.Format("{0:C}", TotalMonth1).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblGrandTotalMonth2.Text = String.Format("{0:C}", TotalMonth2).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblGrandTotalMonth3.Text = String.Format("{0:C}", TotalMonth3).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblGrandTotalMonth4.Text = String.Format("{0:C}", TotalMonth4).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblGrandTotalMonth5.Text = String.Format("{0:C}", TotalMonth5).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblGrandTotalMonth6.Text = String.Format("{0:C}", TotalMonth6).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblGrandTotalMonth7.Text = String.Format("{0:C}", TotalMonth7).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblGrandTotalMonth8.Text = String.Format("{0:C}", TotalMonth8).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblGrandTotalMonth9.Text = String.Format("{0:C}", TotalMonth9).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblGrandTotalMonth10.Text = String.Format("{0:C}", TotalMonth10).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblGrandTotalMonth11.Text = String.Format("{0:C}", TotalMonth11).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblGrandTotalMonth12.Text = String.Format("{0:C}", TotalMonth12).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

        }
    }
    protected void bindMainCategory()
    {
        objReports.year = Convert.ToInt32(Convert.ToInt32(Request["year"]));
        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.ClientAccountId = Convert.ToInt32(Convert.ToInt32(Request["Client"]));
        DataSet ds = new DataSet();
        ds = objReports.getMainCategoryYearlySales();
        if (ds.Tables[0].Rows.Count > 0)
        {

            rptrMainCategory.DataSource = ds.Tables[0];
            rptrMainCategory.DataBind();
            grandTotal.Style.Add("display", "");
        }
        else
        {
            DvEmpty.Style.Add("display", "");
        }
    }
    protected void bindBuyer()
    {
        objFile.id = Convert.ToInt32(Request["Client"].ToString()); ;
        objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtBuyer = new DataTable();
        dtBuyer = objFile.getMenueDetail();


        if (dtBuyer.Rows.Count > 0)
        {
            lblBuyerName.Text = Convert.ToString(dtBuyer.Rows[0]["Title"]);
            lblBuyerAddress.Text = Convert.ToString(dtBuyer.Rows[0]["Address1"]);
            if (Convert.ToString(dtBuyer.Rows[0]["Address2"]) != "")
            {
                lblBuyerAddress2.Text = "," + Convert.ToString(dtBuyer.Rows[0]["Address2"]);
            }
            if (Convert.ToString(dtBuyer.Rows[0]["Address3"]) != "")
            {
                lblBuyerAddress3.Text = "," + Convert.ToString(dtBuyer.Rows[0]["Address3"]);
            }
        }


    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }


    }
    #endregion
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }

    protected void rptrMainCategory_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rptrSubCategory = ((Repeater)e.Item.FindControl("rptrSubCategory"));
        objReports.year = Convert.ToInt32(Convert.ToInt32(Request["year"]));
        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.ClientAccountId = Convert.ToInt32(Convert.ToInt32(Request["Client"]));
        objReports.MainCategoryCode = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "MainCategoryCode"));
        DataSet ds = new DataSet();
        ds = objReports.getSubCategoryYearlySales();
        if (ds.Tables[0].Rows.Count > 0)
        {
            rptrSubCategory.DataSource = ds.Tables[0];
            rptrSubCategory.DataBind();
        }
       
    }
    protected void rptrSubCategory_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        DataGrid dgGallery = ((DataGrid)e.Item.FindControl("dgGallery"));
        objReports.year = Convert.ToInt32(Convert.ToInt32(Request["year"]));
        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.ClientAccountId = Convert.ToInt32(Convert.ToInt32(Request["Client"]));
        objReports.SubCategoryCode = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "SubCategoryCode"));
        DataSet ds = new DataSet();
        ds = objReports.getYearlySales();
        if (ds.Tables[0].Rows.Count > 0)
        {
            dgGallery.DataSource = ds.Tables[0];
            dgGallery.DataBind();
        }
       

    }
    protected void dgGallery_OnItemDataBound(object sender, DataGridItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

        }


        if (e.Item.ItemType == ListItemType.Footer)
        {
            Label lblTotalMonth1 = ((Label)e.Item.FindControl("lblTotalMonth1"));
            Label lblTotalMonth2 = ((Label)e.Item.FindControl("lblTotalMonth2"));
            Label lblTotalMonth3 = ((Label)e.Item.FindControl("lblTotalMonth3"));
            Label lblTotalMonth4 = ((Label)e.Item.FindControl("lblTotalMonth4"));
            Label lblTotalMonth5 = ((Label)e.Item.FindControl("lblTotalMonth5"));
            Label lblTotalMonth6 = ((Label)e.Item.FindControl("lblTotalMonth6"));
            Label lblTotalMonth7 = ((Label)e.Item.FindControl("lblTotalMonth7"));
            Label lblTotalMonth8 = ((Label)e.Item.FindControl("lblTotalMonth8"));
            Label lblTotalMonth9 = ((Label)e.Item.FindControl("lblTotalMonth9"));
            Label lblTotalMonth10 = ((Label)e.Item.FindControl("lblTotalMonth10"));
            Label lblTotalMonth11 = ((Label)e.Item.FindControl("lblTotalMonth11"));
            Label lblTotalMonth12 = ((Label)e.Item.FindControl("lblTotalMonth12"));
            Label lblTotalMonth = ((Label)e.Item.FindControl("lblTotalMonth"));
            objReports.year = Convert.ToInt32(Convert.ToInt32(Request["year"]));
            objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objReports.ClientAccountId = Convert.ToInt32(Convert.ToInt32(Request["Client"]));
            DataSet ds = new DataSet();
            ds = objReports.getYearlySales();

            if (ds.Tables[0].Rows.Count > 0)
            {

                lblTotalMonth1.Text = String.Format("{0:C}", ds.Tables[0].Compute("Sum(Month1)", " ")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth2.Text = String.Format("{0:C}", ds.Tables[0].Compute("Sum(Month2)", " ")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth3.Text = String.Format("{0:C}", ds.Tables[0].Compute("Sum(Month3)", " ")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth4.Text = String.Format("{0:C}", ds.Tables[0].Compute("Sum(Month4)", " ")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth5.Text = String.Format("{0:C}", ds.Tables[0].Compute("Sum(Month5)", " ")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

                lblTotalMonth6.Text = String.Format("{0:C}", ds.Tables[0].Compute("Sum(Month6)", " ")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth7.Text = String.Format("{0:C}", ds.Tables[0].Compute("Sum(Month7)", " ")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth8.Text = String.Format("{0:C}", ds.Tables[0].Compute("Sum(Month8)", " ")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth9.Text = String.Format("{0:C}", ds.Tables[0].Compute("Sum(Month9)", " ")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth10.Text = String.Format("{0:C}", ds.Tables[0].Compute("Sum(Month10)", " ")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth11.Text = String.Format("{0:C}", ds.Tables[0].Compute("Sum(Month11)", " ")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth12.Text = String.Format("{0:C}", ds.Tables[0].Compute("Sum(Month12)", " ")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblTotalMonth.Text = String.Format("{0:C}", ds.Tables[0].Compute("Sum(total)", " ")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');




                Total += Convert.ToDecimal(ds.Tables[0].Compute("Sum(total)", " "));
                TotalMonth1 += Convert.ToDecimal(ds.Tables[0].Compute("Sum(Month1)", " "));
                TotalMonth2 += Convert.ToDecimal(ds.Tables[0].Compute("Sum(Month2)", " "));
                TotalMonth3 += Convert.ToDecimal(ds.Tables[0].Compute("Sum(Month3)", " "));
                TotalMonth4 += Convert.ToDecimal(ds.Tables[0].Compute("Sum(Month4)", " "));
                TotalMonth5 += Convert.ToDecimal(ds.Tables[0].Compute("Sum(Month5)", " "));
                TotalMonth6 += Convert.ToDecimal(ds.Tables[0].Compute("Sum(Month6)", " "));
                TotalMonth7 += Convert.ToDecimal(ds.Tables[0].Compute("Sum(Month7)", " "));
                TotalMonth8 += Convert.ToDecimal(ds.Tables[0].Compute("Sum(Month8)", " "));
                TotalMonth9 += Convert.ToDecimal(ds.Tables[0].Compute("Sum(Month9)", " "));
                TotalMonth10 += Convert.ToDecimal(ds.Tables[0].Compute("Sum(Month10)", " "));
                TotalMonth11 += Convert.ToDecimal(ds.Tables[0].Compute("Sum(Month11)", " "));
                TotalMonth12 += Convert.ToDecimal(ds.Tables[0].Compute("Sum(Month12)", " "));

            }


        }

    }
}
