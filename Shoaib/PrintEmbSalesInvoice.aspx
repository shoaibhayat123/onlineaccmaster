﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintEmbSalesInvoice.aspx.cs"
    Inherits="PrintEmbSalesInvoice" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Verdana; background: none; font-size: 10px;">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; border-bottom: solid 1px black;
            font-weight: bold;">
            <tr>
                <td>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">S A L
                        E - I N V O I C E<br />
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 100%;font-size: 12px;">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <b style="font-weight: bold; font-size: 12px;">Customer Name</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblCustomerName" Style="font-weight: bold; font-size: 12px;" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">
                                <b>Quality</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblQuality" Style="font-weight: bold; font-size: 12px;" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">
                                <b>DC</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblDc" Style="font-weight: bold; font-size: 12px;" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="float: right">
                    <table style="float: right;font-size: 12px;">
                        <tr>
                            <td style="vertical-align: top">
                                <b>Invoice No. </b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblInvoiceNo" Style="font-weight: bold; font-size: 12px;" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">
                                <b>Invoice Date</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblDate" Style="font-weight: bold; font-size: 12px;" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">
                                <b>Calculate Length</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblCalc" Style="font-weight: bold; font-size: 12px;" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Description :
                    <asp:Label ID="lblDescription" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <asp:HiddenField ID="hdndetail" runat="server" />
        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
            AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
            AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
            FooterStyle-CssClass="gridFooter" ShowFooter="true" BorderStyle="Dotted" BorderWidth="1" HeaderStyle-Font-Bold="true" FooterStyle-Font-Size="10px"
            ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery_RowDataBound">
            <Columns>
                <asp:TemplateColumn HeaderText="S.No.">
                    <HeaderStyle HorizontalAlign="Center" ForeColor="Black" />
                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                    <ItemTemplate>
                        <%# Container.DataSetIndex + 1%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Design #">
                      <HeaderStyle HorizontalAlign="Center" ForeColor="Black" />
                    <ItemStyle HorizontalAlign="left" Width="15%" />
                    <ItemTemplate>
                        <%# Eval("DesignNo")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Pcs">
                      <HeaderStyle HorizontalAlign="Center" ForeColor="Black" />
                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                    <ItemTemplate>
                        <%# Eval("pcs")%>
                    </ItemTemplate>
                       <FooterTemplate>
                        <asp:Label ID="lblTotalPcs" runat="server"></asp:Label></FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Yards">
                      <HeaderStyle HorizontalAlign="Center" ForeColor="Black" />
                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                    <ItemTemplate>
                        <%# Eval("yard")%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalYard" runat="server"></asp:Label></FooterTemplate>
                </asp:TemplateColumn>
              
                <asp:TemplateColumn HeaderText="Yard Rate">
                       <HeaderStyle HorizontalAlign="Center" ForeColor="Black" />
                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                    <ItemTemplate>
                        <%# Eval("YardRate")%>
                    </ItemTemplate>
                   
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Piece Rate">
                     <HeaderStyle HorizontalAlign="Center" ForeColor="Black" />
                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                    <ItemTemplate>
                        <%# Eval("PieceRate")%>
                    </ItemTemplate>
                 
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Amount">
                     <HeaderStyle HorizontalAlign="Center" ForeColor="Black" />
                    <ItemStyle HorizontalAlign="Right" Width="15%" />
                    <ItemTemplate>
                        <asp:Label ID="lblAmount" runat="server"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblAmountOfGrid" runat="server"></asp:Label></FooterTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
        <table border="1" style="width: 100%; float: left; border-collapse: collapse;">
            <tr>
                <td style=" width:10%">
                    <b>Add</b>
                </td>
                 <td style=" width:70%">
                    <asp:Label ID="lblAdd1Desc" runat="server"></asp:Label>
                </td>
                <td style="text-align: right;width:30%">
                    <asp:Label ID="lblAdd1Amount" runat="server" Text="0.00"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Add</b>
                </td>
                <td>
                    <asp:Label ID="lblAdd2Desc" runat="server"></asp:Label>
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblAdd2Amount" runat="server" Text="0.00"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Less</b>
                </td>
                <td>
                    <asp:Label ID="lblLess1Desc" runat="server"></asp:Label>
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblLess1" runat="server" Text="0.00"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Less</b>
                </td>
                <td>
                    <asp:Label ID="lblLess2Desc" runat="server"></asp:Label>
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblLess2" runat="server" Text="0.00"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 808px; float: left; font-size: 10px;">
            <tr>
                <td style="float: right; text-align: right; font-weight: bold">
                    Net Amount :
                    <asp:Label ID="lblNetAmount" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <b>
                        <asp:Label ID="lblCurrency" runat="server"></asp:Label>
                        :</b>
                    <asp:Label ID="lblTotalEng" runat="server"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:LinkButton Text="Print" ID="lnkPrint" OnClientClick="window.print();" runat="server"></asp:LinkButton>
                    <a href="SendEmail.aspx?page=SendEmail.aspx?page" title="AccMaster" style="margin-left: 15px"
                        rel="gb_page_center[580, 440]">Email</a>
                </td>
            </tr>
            <tr>
                <td style="text-align: center" colspan="4">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>
                                <script type="text/javascript">

                                    var currentTime = new Date()
                                    var hours = currentTime.getHours()
                                    var Newhours = 0;
                                    var minutes = currentTime.getMinutes()
                                    if (minutes < 10) {
                                        minutes = "0" + minutes
                                    }
                                    if (hours > 12) {
                                        Newhours = hours - 12;
                                    }
                                    else {
                                        Newhours = hours;
                                    }
                                    document.write(Newhours + ":" + minutes + " ")
                                    if (hours > 11) {
                                        document.write("PM")
                                    } else {
                                        document.write("AM")
                                    }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
