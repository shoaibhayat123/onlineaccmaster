﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintBoutiqueOrder.aspx.cs" Inherits="PrintBoutiqueOrder" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
</head>
<body style="font-family: Verdana; background: none; font-size: 10px;">
    <form id="form1" runat="server">
    <div style="text-align: center; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; border-bottom: solid 1px black;
            font-weight: bold;">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td style="text-align: left">
                                <%--   <b>Supplier Name</b>--%>
                                <asp:Label ID="lblSuplierName" runat="server"></asp:Label>
                            </td>
                            <td style="padding-left: 15px">
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; text-align: left">
                                <%--  <b>Address</b>--%>
                                <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                                <span id="spAddress2" runat="server">
                                    <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                                </span><span id="spAddress3" runat="server">
                                    <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                                </span>
                            </td>
                            <td style="padding-left: 15px">
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">
                                <%-- <b>Telephone No.</b>--%>
                                <asp:Label ID="lblSupplierTelephone" runat="server"></asp:Label>
                            </td>
                            <td style="padding-left: 15px">
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td>
                                <%-- <b>ST Reg. No.</b>--%>
                                <asp:Label ID="lblSellerRegNo" runat="server"></asp:Label>
                            </td>
                            <td style="padding-left: 15px">
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td>
                                <%--  <b>NTN No.</b>--%>
                                <asp:Label ID="lblSellerNTN" runat="server"></asp:Label>
                            </td>
                            <td style="padding-left: 15px">
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">BOUTIQUE ORDER<br />
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 100%">
            <tr>
                <td>
                    <table>
                        <%--  <tr>
                            <td>
                                <b style="font-weight: bold; font-size: 12px;">Customer Name</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblBuyerName" Style="font-weight: bold; font-size: 12px;" runat="server"></asp:Label>
                            </td>
                        </tr>--%>
                        <tr>
                            <td>
                                <b style="font-weight: bold; font-size: 12px;">Customer Name</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblcustomername" Style="font-weight: bold; font-size: 12px;" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Contact Number</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblcontactnumber" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Email Address</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblemailaddress" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <%-- <tr>
                            <td style="vertical-align: top">
                                <b>Address</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblBuyerAddress" runat="server"></asp:Label><br />
                                <asp:Label ID="lblBuyerAddress2" runat="server"></asp:Label><br />
                                <asp:Label ID="lblBuyerAddress3" runat="server"></asp:Label><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Telephone No.</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblBuyerTelephone" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>ST Reg. No.</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblBuyerRegNo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>NTN No.</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblBuyerNTN" runat="server"></asp:Label>
                            </td>
                        </tr>--%>
                    </table>
                </td>
                <td style="float: right">
                    <table style="float: right; width: 200px; border-collapse: collapse" border="1">
                        <tr>
                            <td style="text-align: left; height: 30px;">
                                <b>Order No. </b>
                            </td>
                            <td style="padding-left: 10px">
                                <asp:Label ID="lblInvoiceNo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; height: 30px;">
                                <b>Order Date</b>
                            </td>
                            <td style="padding-left: 10px">
                                <asp:Label ID="lblDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <%-- <tr>
                            <td style="text-align: lef; height: 30px;">
                                <b>Payment Terms</b>
                            </td>
                            <td style="padding-left: 10px">
                                <asp:Label ID="lblCrdays" runat="server"></asp:Label>
                                days
                            </td>
                        </tr>--%>
                    </table>
                </td>
            </tr>
        </table>
        <br />
        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
            ShowFooter="true" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
            BorderColor="black" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery_RowDataBound">
            <Columns>
                <asp:TemplateColumn HeaderText="S.No.">
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                    <ItemTemplate>
                        <%# Container.DataSetIndex + 1%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Item Description">
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemStyle Width="30%" />
                    <ItemTemplate>
                        <%# Eval("ItemDesc") %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Quantity">
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalQuantity" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Unit">
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                    <ItemTemplate>
                        <%# Eval("Unit")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Rate">
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemStyle Width="13%" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Discount %">
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemStyle Width="7%" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("BoutiqueDiscount")).Replace('$', ' ')%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="After Discount">
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemStyle Width="7%" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("BoutiqueAmountAfterDis")).Replace('$', ' ')%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Amount">
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Gross Amount">
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalGrossAmount" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <%--<asp:TemplateColumn HeaderText="S.T %">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%# Convert.ToDecimal(Eval("SaleTax")).ToString("0.00")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="S.T Amount">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("SaleTaxAmount")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalSaleTaxAmount" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Amount Including Taxes">
                    <HeaderStyle HorizontalAlign="Center" Width="15%" />
                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("AmountIncludeTaxes")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalAmountIncludeTax" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>--%>
            </Columns>
        </asp:DataGrid>
        <table style="width: 350px; float: left;">
            <tr>
                <td>
                    <asp:Label ID="lblCreditCarddetail" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <table style="width: 238px; float: right; border-collapse: collapse" border="1">
            <tr>
                <td style="width: 115px; height: 20px; text-align: left">
                    <b>Discount</b>
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblDiscount" runat="server" Text="0.00" Style="font-weight: bold"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 115px; height: 20px; text-align: left">
                    <b>Cartage Charges</b>
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblCartege" runat="server" Text="0.00" Style="font-weight: bold"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="height: 20px; text-align: left">
                    <b>Bill Amt</b>
                </td>
                <td style="text-align: right">
                    <asp:Label ID="txtBillAmount" runat="server" Text="0.00" Style="font-weight: bold"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 115px; height: 20px; text-align: left">
                    <b>Cash Received </b>
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblCashReceived" runat="server" Text="0.00" Style="font-weight: bold"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 115px; height: 20px; text-align: left">
                    <b>Cash Returned</b>
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblCashReturned" runat="server" Text="0.00" Style="font-weight: bold"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 808px; float: left; font-size: 10px;">
            <tr>
                <td style="float: right; text-align: right">
                </td>
            </tr>
            <tr>
                <td>
                    <b>
                        <asp:Label ID="lblCurrency" runat="server"></asp:Label>
                        :</b>
                    <asp:Label ID="lblTotalEng" runat="server"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%">
                        <tr>
                            <td style="margin-top: 20px; width: 50%;">
                                <div style="float: left">
                                    <b>Signature :</b></div>
                                <div style="float: left; margin-left: 63px; padding-top: 10px; height: 20px; width: 200px;">
                                    <hr />
                                </div>
                            </td>
                            <td style="margin-top: 20px; text-align: right">
                                <div style="float: right; margin-left: 20px; padding-top: 10px; height: 20px; line-height: 3px;
                                    text-align: center; width: 200px;">
                                    <hr />
                                    <b>Receiver's Signature</b>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:LinkButton Text="Print" ID="lnkPrint" OnClientClick="window.print();" runat="server"></asp:LinkButton>
                    <a href="SendEmail.aspx?page=SendEmail.aspx?page" title="AccMaster" style="margin-left: 15px"
                        rel="gb_page_center[580, 440]">Email</a>
                </td>
            </tr>
            <tr>
                <td style="text-align: center" colspan="4">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>
                                <script type="text/javascript">

                                    var currentTime = new Date()
                                    var hours = currentTime.getHours()
                                    var Newhours = 0;
                                    var minutes = currentTime.getMinutes()
                                    if (minutes < 10) {
                                        minutes = "0" + minutes
                                    }
                                    if (hours > 12) {
                                        Newhours = hours - 12;
                                    }
                                    else {
                                        Newhours = hours;
                                    }
                                    document.write(Newhours + ":" + minutes + " ")
                                    if (hours > 11) {
                                        document.write("PM")
                                    } else {
                                        document.write("AM")
                                    }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
