﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class LedgerFC : System.Web.UI.Page
{
    #region properties
    clsCookie ck = new clsCookie();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    Reports objReports = new Reports();
    decimal TotalBalance = 0;
    decimal TotalDebit = 0;
    decimal TotalCredit = 0;
    public string attachment = "attachment; filename=Ledger_Report_" + DateTime.Now.ToString("dd_MM_yyyy") + ".xls";
    #endregion
    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();

        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                if (ck.GetCookieValue("CustomerCode") != "")
                {
                    Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
        }
        if (!IsPostBack)
        {
            ddlClientAC.Focus();
            FillClientAC();
            if (Sessions.FromDate != null)
            {
                txtstartDate.Text = Convert.ToDateTime(Sessions.FromDate).ToString("dd MMM yyyy");
                txtEndDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
            }
            else
            {
                Response.Redirect("default.aspx");
            }

        }
        SetUserRight();
    }
    #endregion
    #region Fill  Client A/C
    protected void FillClientAC()
    {
       
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtClient = new DataTable();
            dtClient = objSalesInvoice.getAcFileAllLedger();
            if (dtClient.Rows.Count > 0)
            {
                ddlClientAC.DataSource = dtClient;
                ddlClientAC.DataTextField = "Title";
                ddlClientAC.DataValueField = "Id";
                ddlClientAC.DataBind();
                ddlClientAC.Items.Insert(0, new ListItem("Select A/c. Title", "0"));

            }

        

    }
    #endregion
    #region btnGenrete
    protected void btnGenerate_Click(object sender, EventArgs e)
    {
      
        if (RadioButton4.Checked)
        {
            string urllast = "PrintLedger.aspx?Client=" + ddlClientAC.SelectedValue + "&From=" + txtstartDate.Text + "&To=" + txtEndDate.Text;
            ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);
        }
        else
        {
            if (RadioButton3.Checked)
            {
                string urllast = "PrintLedgerWithQuantity.aspx?Client=" + ddlClientAC.SelectedValue + "&From=" + txtstartDate.Text + "&To=" + txtEndDate.Text;
                ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);
            }
            else 
                if (rdbOnlyFC.Checked)
            {
                string urllast = "PrintLedgerWithOnlyFC.aspx?Client=" + ddlClientAC.SelectedValue + "&From=" + txtstartDate.Text + "&To=" + txtEndDate.Text;
                ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);
            }
            else
            {
                string urllast = "PrintLedgerWithFC.aspx?Client=" + ddlClientAC.SelectedValue + "&From=" + txtstartDate.Text + "&To=" + txtEndDate.Text;
                ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);
            }
        }
     
    }
    #endregion
    #region Bind ledger grid
    protected void bindLedger()
    {
        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.DateFrom = Convert.ToDateTime(txtstartDate.Text);
        objReports.DateTo = Convert.ToDateTime(txtEndDate.Text);
        objReports.AcFileId = Convert.ToInt32(ddlClientAC.SelectedValue);
        DataSet ds = new DataSet();
        ds = objReports.getLedger();

        /********** Add new row to result for Opening Balance**********/
        DataRow dr = ds.Tables[2].NewRow(); //table 2 for detail trans
        dr["Description"] = "Opening Balance";
        decimal OpeningBal = 0;
        decimal TotalTrans = 0;
        if (Convert.ToString(ds.Tables[0].Rows[0]["OpeningBal"]) != "")
        {
            OpeningBal = Convert.ToDecimal(ds.Tables[0].Rows[0]["OpeningBal"]);
        }
        if (Convert.ToString(ds.Tables[1].Rows[0]["TotalTrans"]) != "")
        {
            TotalTrans = Convert.ToDecimal(ds.Tables[1].Rows[0]["TotalTrans"]);
        }

        decimal OpeningBalAmount = OpeningBal + TotalTrans;
        dr["TransAmt"] = OpeningBalAmount;

        ds.Tables[2].Rows.InsertAt(dr, 0);

        grdLedger.DataSource = ds.Tables[2];


        grdLedger.DataBind();


    }

    #endregion
    #region Grid RowDataBound
    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbltransDate = ((Label)e.Row.FindControl("lbltransDate"));
            Label lblChqDate = ((Label)e.Row.FindControl("lblChqDate"));
            Label lblBalance = ((Label)e.Row.FindControl("lblBalance"));
            Label lblDebit = ((Label)e.Row.FindControl("lblDebit"));
            Label lblCredit = ((Label)e.Row.FindControl("lblCredit"));


            string transDate = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TransactionDate"));
            string ChqDate = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ChqDate"));
            if (DataBinder.Eval(e.Row.DataItem, "TransactionDate").ToString() != "")
            {
                lbltransDate.Text = ClsGetDate.FillFromDate(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TransactionDate")));
            }
            if (DataBinder.Eval(e.Row.DataItem, "ChqDate").ToString() != "")
            {
                lblChqDate.Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Row.DataItem, "ChqDate").ToString());
            }
            if (DataBinder.Eval(e.Row.DataItem, "TransAmt").ToString() != "")
            {
                decimal TransAmt = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TransAmt"));
                if (e.Row.RowIndex.ToString() != "0")
                {
                    if (TransAmt > 0)
                    {
                        lblDebit.Text = String.Format("{0:C}", TransAmt).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                        lblCredit.Text = "0.00";
                        TotalDebit = TotalDebit + TransAmt;
                    }
                    else
                    {
                        lblCredit.Text = String.Format("{0:C}", TransAmt).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                        lblDebit.Text = "0.00";
                        TotalCredit = TotalCredit + TransAmt;
                    }


                    TotalBalance = TotalBalance + TransAmt;

                }
                else
                {
                    TotalBalance = TransAmt;


                }
                if (TotalBalance > 0) // For balance amounts
                {
                    lblBalance.Text = String.Format("{0:C}", TotalBalance).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') + " Dr.";
                }
                else
                {
                    lblBalance.Text = String.Format("{0:C}", TotalBalance).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') + " Cr.";
                }


            }


        }

        if (e.Row.RowType == DataControlRowType.Footer)  // For footer totals
        {

            Label lblTotalDebit = (Label)e.Row.FindControl("lblTotalDebit");
            lblTotalDebit.Text = String.Format("{0:C}", TotalDebit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblTotalCredit = (Label)e.Row.FindControl("lblTotalCredit");
            lblTotalCredit.Text = String.Format("{0:C}", TotalCredit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblTotalTransection = (Label)e.Row.FindControl("lblTotalTransection");
            lblTotalTransection.Text = "=" + ((grdLedger.Rows.Count) - 1).ToString() + "=";
        }

    }
    #endregion


    #region ExportExcel
    public override void VerifyRenderingInServerForm(Control control) { }



    private void ExportGridView()
    {
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        grdLedger.AllowPaging = false;
        grdLedger.AllowSorting = false;
        grdLedger.AutoGenerateDeleteButton = false;
        grdLedger.AutoGenerateEditButton = false;
        grdLedger.DataBind();
        grdLedger.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }

    private void PrepareGridViewForExport(Control gv)
    {
        LinkButton lb = new LinkButton();
        Literal l = new Literal();
        string name = String.Empty;
        for (int i = 0; i < gv.Controls.Count; i++)
        {
            if (gv.Controls[i].GetType() == typeof(LinkButton))
            {
                l.Text = (gv.Controls[i] as LinkButton).Text;
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }
            else if (gv.Controls[i].GetType() == typeof(DropDownList))
            {
                l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }
            else if (gv.Controls[i].GetType() == typeof(CheckBox))
            {
                l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }
            if (gv.Controls[i].HasControls())
            {
                PrepareGridViewForExport(gv.Controls[i]);
            }
        }

    }
    #endregion
    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }
            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }

            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

                btnGenerate.Enabled = false;

            }

        }
    }
}