﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class TestBarCode2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    [
    System.Web.Services.WebMethod()
    ]
    public static string GetBarcode(string valueToEncode, string symbology)
    {
        //Use Codeless Barcode Generation in v5.0
        string addChecksum = "&AC=T";
        string code39Ext = "&EXT=T";

        //If barcode symbology is Code 39
        if (symbology == "Code39")
        {
            //Disable checksum    
            addChecksum = "";
        }

        //HTTP Handler for Codeless Barcode Generation
        //IMPORTANT: Look for the following setting in the web.config file to learn how to configure it
        //<add verb="*" path="BarcodeGen.axd" type="Neodynamic.WebControls.BarcodeProfessional.BarcodeProfessional, Neodynamic.WebControls.BarcodeProfessional"/>

        //Return url for barcode generation
        return "BarcodeGen.axd?S=" + symbology + "&C=" + valueToEncode + "&DC=T" + addChecksum + code39Ext;
    }
}