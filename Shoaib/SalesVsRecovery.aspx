﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="SalesVsRecovery.aspx.cs" Inherits="SalesVsRecovery" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Sales Vs Recovery Report"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
        >
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                Year :
            </td>
            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                <asp:DropDownList ID="ddlYear" runat="server" Style="width: 200px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                    InitialValue="0" ErrorMessage="Please select Year " Text="*" ControlToValidate="ddlYear"
                    ValidationGroup="Ledger"></asp:RequiredFieldValidator> 
                     <asp:Button ID="btnGenerate" Text="Generate" ValidationGroup="Ledger" 
                    runat="server" OnClick="btnGenerate_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="Ledger" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
              
            </td>
        </tr>
    </table>
</asp:Content>
