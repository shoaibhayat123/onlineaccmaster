﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class ContactUs : System.Web.UI.Page
{
    Car objCar = new Car();
    protected void Page_Load(object sender, EventArgs e)
    {
        FillAddress();
    }
    protected void FillAddress()
    {
        objCar.id = 1;
        DataTable dt = objCar.getAccAddress();
        if (dt.Rows.Count > 0)
        {
            ltrlAddress.Text = dt.Rows[0]["Address"].ToString();
        }
    }
    protected void btnResponse_Click(object sender, EventArgs e)
    {
          ccJoin.ValidateCaptcha(txtCap.Text);
          if (!ccJoin.UserValidated)
          {
              Page.RegisterStartupScript("Msg1", "<script>alert('Enter the letters as they are shown in the image below !!!');</script>");
              
            

          }
          else
          {

              sendMail();
          }
    }

    private void sendMail()
    {
        string phoneno = "";
        if (txtPhone.Text == "")
        {
            phoneno = "N/A";
        }
        else
        {
            phoneno = txtPhone.Text.Trim();
        }

        string[] strArray = new string[5];
        strArray[0] = txtName.Text.Trim();
        strArray[1] = phoneno;
        strArray[2] = txtEmail.Text.Trim();
        strArray[3] = "";
        strArray[4] = txtComment.Text.Trim();

        EmailSender objMail = new EmailSender();
        objMail.ValueArray = strArray;
        objMail.MailTo = "sales@accmaster.com";
        objMail.Subject = "Contact Us AccMaster";
        objMail.MailBcc = "support@accmaster.com";
        objMail.TemplateDoc = "Contact.htm";
        objMail.Send();

            //string phoneno = "";
            //if (txtPhone.Text == "")
            //{
            //    phoneno = "N/A";
            //}
            //else
            //{
            //    phoneno = txtPhone.Text.Trim();
            //}
            //string[] valueArray = new string[6];
            //valueArray[0] = txtName.Text.Trim();
            //valueArray[1] = phoneno;
            //valueArray[2] = txtEmail.Text.Trim();
            //valueArray[3] = "";
            //valueArray[4] = txtComment.Text.Trim();

            //EmailSender objSendMail = new EmailSender();
            //objSendMail.MailTo = "sales@accmaster.com";
            //objSendMail.MailBcc = "reliablekhandelwal@gmail.com";


            //objSendMail.MailFrom = txtEmail.Text;
            //objSendMail.MailUser = txtName.Text;


            //objSendMail.Subject = "Contact Us AccMaster";

            //objSendMail.TemplateDoc = "Contact.htm";

            //objSendMail.Subject = "Inquiry from Choco";

            //objSendMail.ValueArray = valueArray;



            //objSendMail.SendEmail();
        Page.RegisterStartupScript("a", "<script>alert('Your request has been sent successfully'); location.href='ContactUs.aspx'</script>");

      
    }
}