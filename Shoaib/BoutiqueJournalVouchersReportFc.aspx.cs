﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class BoutiqueJournalVouchersReportFc : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        if (!IsPostBack)
        {
            txtVoucherNo.Focus();
            txtVoucherDateFrom.Text = Convert.ToDateTime(Sessions.FromDate).ToString("dd MMM yyyy");
            txtVoucherDateTo.Text = System.DateTime.Now.ToString("dd MMM yyyy");

        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        objSalesInvoice.VoucherDateTo = Convert.ToDateTime(txtVoucherDateTo.Text);
        objSalesInvoice.VoucherDateFrom = Convert.ToDateTime(txtVoucherDateFrom.Text);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.DataFrom = "JVFC";
        if (txtVoucherNo.Text != "")
        {
            objSalesInvoice.VoucherNo = Convert.ToDecimal(txtVoucherNo.Text);
        }
        else
        {
            objSalesInvoice.VoucherNo = 0;
        }

        DataTable dt = new DataTable();
        dt = objSalesInvoice.SearchJournalVoucher();
        dgGallery.DataSource = dt;
        dgGallery.DataBind();




    }



    protected void dgGallery_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            if (DataBinder.Eval(e.Row.DataItem, "VoucherDate").ToString() != "")
            {
                Label lblVoucherDate = (Label)e.Row.FindControl("lblVoucherDate");
                lblVoucherDate.Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Row.DataItem, "VoucherDate").ToString());
            }
            if (Sessions.UserRole.ToString() != "Admin")
            {


                Button btnEdit = (Button)e.Row.FindControl("btnEdit");

                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
                if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
                {
                    Response.Redirect(Request.UrlReferrer.ToString());
                }

                if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
                {

                    // btnEdit.Visible = false;

                }

            }

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            objSalesInvoice.VoucherDateTo = Convert.ToDateTime(txtVoucherDateTo.Text);
            objSalesInvoice.VoucherDateFrom = Convert.ToDateTime(txtVoucherDateFrom.Text);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.DataFrom = "JVFC";
            if (txtVoucherNo.Text != "")
            {
                objSalesInvoice.VoucherNo = Convert.ToDecimal(txtVoucherNo.Text);
            }
            else
            {
                objSalesInvoice.VoucherNo = 0;
            }

            DataTable dt = new DataTable();
            dt = objSalesInvoice.SearchJournalVoucher();

            Label lblTotalGrossAmount = (Label)e.Row.FindControl("lblTotalGrossAmount");
            decimal TotalGrossAmount = (decimal)dt.Compute("SUM(TotalAmount)", "");
            lblTotalGrossAmount.Text = String.Format("{0:C}", TotalGrossAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            Label lblTotalGrossAmountFc = (Label)e.Row.FindControl("lblTotalGrossAmountFc");
            decimal FCAmountDebit = (decimal)dt.Compute("SUM(FCAmountDebit)", "");
            lblTotalGrossAmountFc.Text = String.Format("{0:C}", FCAmountDebit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            

        }

    }

    protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditCustomer")
        {
              Response.Redirect("BoutiquejournalvoucherFC.aspx?VoucherSimpleMainId=" + e.CommandArgument.ToString());
        }

    } 
}