﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintProfitAndLossReport.aspx.cs"
    Inherits="PrintProfitAndLossReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .Level1
        {
            margin: 5px 0 0 30px;
            font-weight: bold;
            color: Green;
        }
        .Level2
        {
            margin: 5px 0 0 60px;
            font-weight: bold;
            color: Green;
        }
        .Level3
        {
            margin: 5px 0 0 90px;
            font-weight: bold;
            color: Green;
        }
        .Level4
        {
            margin: 5px 0 0 120px;
            font-weight: bold;
            color: Green;
        }
        .Level5
        {
            margin: 5px 0 0 150px;
            font-weight: bold;
            color: Green;
        }
        .Level6
        {
            margin: 5px 0 0 180px;
            font-weight: bold;
            color: Green;
        }
        .Ledger
        {
            margin: 5px 0 0 215px;
        }
    </style>
</head>
<body style="font-family: Tahoma; background: none;">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; font-weight: bold;">
             <tr id="trTrialBal" runat="server">
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Profit
                        & Loss Statement<br />
                    </span><span style="font-size: 14px;"><span style="font-size: 14px;">From :
                        <asp:Label ID="lblFromDate" runat="server" Style="padding-right: 15px;"></asp:Label>
                        To :
                        <asp:Label ID="lblToDate" runat="server"></asp:Label><br />
                    </span>
                        <asp:Label ID="lblBuyerName" runat="server"></asp:Label><br />
                        <span style="font-size: 12px; font-weight: normal">
                            <asp:Label ID="lblBuyerAddress" runat="server"></asp:Label>
                            <asp:Label ID="lblBuyerAddress2" runat="server"></asp:Label>
                            <asp:Label ID="lblBuyerAddress3" runat="server"></asp:Label></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <hr />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 100%; text-align: center; border: solid 1px black; font-size: 18px;
            font-weight: bold; background: none repeat scroll 0 0 #85A68C; color: #FFFFFF;">
            <tr>
                <td style="width: 68%;">
                    Account Title
                </td>
                <td style="width: 12%;">
                    Debits
                </td>
                <td style="width: 20%;">
                    Credits
                </td>
            </tr>
        </table>
        <asp:GridView ID="grdInvoice" AutoGenerateColumns="false" GridLines="None" BorderWidth="0"
            CellPadding="0" CellSpacing="0" runat="server" OnRowDataBound="grdInvoice_RowDataBound">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <div class="Level<%#Eval("depth")%>">
                            <asp:Label ID="lblTitle" Text='<%#Eval("Title")%>' Style="text-decoration: underline;
                                font-size: 16px" runat="server"></asp:Label>
                            <asp:HiddenField ID="hdnid" runat="server" />
                        </div>
                        <div class="Ledger">
                            <asp:GridView ID="grdLedger" Width="585px" runat="server" ShowFooter="True" AutoGenerateColumns="False"
                                ShowHeader="false" AllowPaging="true" PageSize="500000" PagerSettings-Visible="false"
                                PagerSettings-Mode="Numeric" FooterStyle-CssClass="gridFooter" AlternatingRowStyle-CssClass="gridAlternateItem"
                                GridLines="none" BorderColor="black" BorderStyle="Dotted" OnRowDataBound="grdLedger_RowDataBound"
                                BorderWidth="1" RowStyle-CssClass="gridItem">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemStyle Width="50%" HorizontalAlign="left" CssClass=" gridpadding" />
                                        <FooterStyle HorizontalAlign="Right" Font-Size="14px" />
                                        <ItemTemplate>
                                            <%# Eval("Title") %>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotal" Text="Total" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemStyle Width="25%" HorizontalAlign="right" CssClass=" gridpadding" />
                                        <FooterStyle HorizontalAlign="Right" Font-Size="14px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblDebit" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalDebit" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemStyle Width="25%" HorizontalAlign="right" CssClass=" gridpadding" />
                                        <FooterStyle HorizontalAlign="Right" Font-Size="14px" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblCredit" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblTotalCredit" runat="server"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <table style="width: 100%; text-align: right; border: solid 1px black; font-size: 14px;
            font-weight: bold; background: none repeat scroll 0 0 #85A68C; margin-top: 15px;
            margin-bottom: 15px; color: black;">
            <tr>
                <td style="width: 64%;">
                </td>
                <td style="width: 18%;">
                    <asp:Label ID="grandTotalDebit" Style="border-bottom: solid 1px black; border-top: solid 1px black;"
                        runat="server"></asp:Label>
                </td>
                <td style="">
                    <asp:Label ID="grandTotalCredit" Style="border-bottom: solid 1px black; border-top: solid 1px black;"
                        runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 64%;">
                <asp:Label ID="lblProfitLoss" runat="server"></asp:Label>
                </td>
                <td style="width: 18%;">
                    <asp:Label ID="LblDebitBal" Style="border-bottom: solid 1px black; border-top: solid 1px black;"
                        runat="server"></asp:Label>
                </td>
                <td style="">
                    <asp:Label ID="lblCreditBal" Style="border-bottom: solid 1px black; border-top: solid 1px black;"
                        runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 64%;">
                    Grand Total
                </td>
                <td style="width: 18%;">
                    <asp:Label ID="lblAfterBalDebit" Style="border-bottom: solid 1px black; border-top: solid 1px black;"
                        runat="server"></asp:Label>
                </td>
                <td style="">
                    <asp:Label ID="lblAfterBalCredit" Style="border-bottom: solid 1px black; border-top: solid 1px black;"
                        runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
              <tr>
                <td style="text-align: right">
                    <asp:LinkButton Text="Print" ID="lnkPrint" OnClientClick="window.print();" runat="server"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>

                                <script type="text/javascript">

                                            var currentTime = new Date()
                                            var hours = currentTime.getHours()
                                             var Newhours=0;
                                            var minutes = currentTime.getMinutes()
                                            if (minutes < 10)
                                            {
                                            minutes = "0" + minutes
                                            }                                           
                                            if(hours >12)
                                            {
                                            Newhours= hours-12;
                                            }
                                            else
                                            {
                                              Newhours= hours;
                                            }
                                            document.write(Newhours + ":" + minutes + " ")
                                             if(hours > 11)
                                            {                                           
                                            document.write("PM")
                                            } else 
                                            {
                                            document.write("AM")
                                            }

                                </script>

                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
