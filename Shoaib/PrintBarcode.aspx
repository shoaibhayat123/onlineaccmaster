﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintBarcode.aspx.cs" Inherits="PrintBarcode" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ASP.NET AJAX Barcode Sample</title>
</head>
<body onload="getBarcode(<%=Request.QueryString["barcode"]%>, 'Code39');">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" />
    <div style="text-align: left; width: 300px; float: left">
        <div style="text-align: left; width: 250px; margin-left:70px; float: left">
            <table>
                <tr>
                    <td style="text-align: left; font-size: 12px; vertical-align: middle;">
                        <asp:Image ID="imgBarcodePreview"  runat="server" />
                        Dh :
                        <asp:Label ID="lblPrice" Style="font-size: 12px;" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblSuplierName" Style="font-size: 7px; display: none" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 7px">
                    </td>
                </tr>
                <%--<tr>
                    <td colspan="2" style="text-align: center">
                        <br />
                        <br />
                        <br />
                        <hr />
                        System Development Services © 2012-13
                        <br />
                        sales@accmaster.com
                    </td>
                </tr>--%>
            </table>
        </div>
        <div style="text-align: left; display: none; width: 300px; float: left">
            <table style="width: 300px; float: left; font-size: 14px; font-weight: bold;">
                <tr>
                    <td>
                        <%--   <asp:Label ID="lblSuplierName" runat="server"></asp:Label>--%><br />
                        <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                        <span id="spAddress2" runat="server">
                            <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                        </span><span id="spAddress3" runat="server">
                            <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                        </span>
                        <asp:Label ID="lblCity" runat="server"></asp:Label>,
                        <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                        <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                    </td>
                    <td style="text-align: right; vertical-align: top">
                        <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
    <script type="text/javascript">

        function getBarcode(valueToEncode, symbology) {

            PageMethods.GetBarcode(valueToEncode, symbology, OnSucceeded, OnFailed);
        }

        function OnSucceeded(result, userContext, methodName) {
            if (methodName == "GetBarcode") {
                //Set URL to Barcode Preview Image control
                $get('imgBarcodePreview').src = result;
            }
        }

        // Callback function invoked on failure 
        // of the page method.
        function OnFailed(error, userContext, methodName) {
            if (error != null) {

            }
        }

    </script>
</body>
</html>
