﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class CurrentStockPosition : System.Web.UI.Page
{
    clsCookie ck = new clsCookie();
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        if (!IsPostBack)
        {

            if (Sessions.FromDate != null)
            {
                txtstartDate.Text =  System.DateTime.Now.ToString("dd MMM yyyy");
               
            }
            else
            {
                Response.Redirect("default.aspx");
            }

        }
        SetUserRight();
    }
    #region btnGenrete
    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        string urllast;

        if (chkMinStockLevel.Checked == false)
        {
            if (rdbWithValue.Checked == true)
            {
                if (rdWith.Checked == true)
                {
                    urllast = "PrintCurrentStockPosition.aspx?AsOn=" + txtstartDate.Text + "&Type=1";
                }
                else
                {
                    urllast = "PrintCurrentStockPosition.aspx?AsOn=" + txtstartDate.Text + "&Type=2";
                }
            }
            else
            {
                if (rdWith.Checked == true)
                {
                    urllast = "PrintCurrentStockPositionWithOutValue.aspx?AsOn=" + txtstartDate.Text + "&Type=1";
                }
                else
                {
                    urllast = "PrintCurrentStockPositionWithOutValue.aspx?AsOn=" + txtstartDate.Text + "&Type=2";
                }
            }
        }
        else
        {
            if (rdbWithValue.Checked == true)
            {
                if (rdWith.Checked == true)
                {
                    urllast = "PrintCurrentStockPositionMinstock.aspx?AsOn=" + txtstartDate.Text + "&Type=1";
                }
                else
                {
                    urllast = "PrintCurrentStockPositionMinstock.aspx?AsOn=" + txtstartDate.Text + "&Type=2";
                }
            }
            else
            {
                if (rdWith.Checked == true)
                {
                    urllast = "PrintCurrentStockPositionWithOutValueMinstock.aspx?AsOn=" + txtstartDate.Text + "&Type=1";
                }
                else
                {
                    urllast = "PrintCurrentStockPositionWithOutValueMinstock.aspx?AsOn=" + txtstartDate.Text + "&Type=2";
                }
            }
        }
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);

    }
    #endregion
    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }
            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }

            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

                btnGenerate.Enabled = false;

            }

        }
    }
}
