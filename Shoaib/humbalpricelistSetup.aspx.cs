﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class humbalpricelistSetup : System.Web.UI.Page
{

    #region DataMembers & Varriables
    SetCustomers objCust = new SetCustomers();
    #endregion


    #region Page Load Event

    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (!IsPostBack)
        {
            FillItemDesc();
            FillSubCategory();
            FillMainCategory();
        }
    }

    #endregion

    #region Add/ Edit Price  Setup

    protected void btn_save_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
        
        if (hdnid.Value != null && hdnid.Value != string.Empty)
        {
            objCust.id = int.Parse(hdnid.Value);
        }
        else
        {
            objCust.id = 0;
        }

        objCust.price = Convert.ToDecimal(txtPrice.Text);
        objCust.CategoryId = Convert.ToInt32(ddlMainCategory.SelectedValue);
        objCust.ItemId = Convert.ToInt32(ddlItemDesc.SelectedValue);
        
        if (Sessions.UserRole.ToString() != "Admin")
        {
            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }
            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" || Convert.ToString(dt.Rows[0]["IsAdd"]) == "True")
            {
                if (Convert.ToString(dt.Rows[0]["IsAdd"]) == "True" && (hdnid.Value == null || hdnid.Value == string.Empty))
                {
                    int result = objCust.SetHambalPriceList();
                    if (result != -1)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Price Setup successful added!');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                    }
                    clear();
                    FillSubCategory();
                }
                else
                {
                    if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" && hdnid.Value != null && hdnid.Value != string.Empty)
                    {
                        int result = objCust.SetHambalPriceList();
                        if (result != -1)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Price Setup successful updated!');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                        }
                        clear();
                        FillSubCategory();

                    }

                    else
                    {
                        if (objCust.id != 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='humbalpricelistSetup.aspx';", true);
                        }

                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='humbalpricelistSetup.aspx';", true);
                        }

                    }

                }
            }
            else
            {
                if (objCust.id != 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='humbalpricelistSetup.aspx';", true);
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='humbalpricelistSetup.aspx';", true);
                }

            }

        }

        else
        {
            int result = objCust.SetHambalPriceList();

            if (objCust.id != 0)
            {
                if (result != -1)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Price Setup successful updated!');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                }
            }
            else
            {
                if (result != -1)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Price Setup successful added!');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                }
            }
            clear();
            FillSubCategory();
        }
    }

    #endregion

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        clear();
    }
    #endregion

    #region Blank Controls
    private void clear()
    {

        txtPrice.Text = string.Empty;
       
        ddlItemDesc.SelectedValue = "0";
        hdnid.Value = string.Empty;
        
    }
    #endregion


    #region Fill Price  Setup
    protected void FillData(int id)
    {
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.id = id;
        objCust.CategoryId = 0;
        DataSet ds = new DataSet();
        ds = objCust.GethumbalPricelist();
        if (ds.Tables[0].Rows.Count > 0)
        {

            txtPrice.Text = Convert.ToString(ds.Tables[0].Rows[0]["Price"].ToString());
            ddlMainCategory.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["CategoryId"].ToString());
            ddlItemDesc.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["ItemId"].ToString());
            hdnid.Value = id.ToString();
        }

    }
    #endregion

    #region Fill Main Category  Setup
    protected void FillSubCategory()
    {

        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.id = 0;
        objCust.CategoryId = 0;
        DataSet ds = new DataSet();
        ds = objCust.GethumbalPricelist();
        if (ds.Tables[0].Rows.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 30)
            {
                grdCustomer.AllowPaging = true;
            }
            grdCustomer.Visible = true;
            grdCustomer.DataSource = ds.Tables[0];
            grdCustomer.DataBind();
            lblError.Visible = false;
        }
        else
        {
            grdCustomer.Visible = false;
            lblError.Visible = true;
        }


    }
    #endregion

    #region GridView Events
    protected void grdCustomer_RowCommand(object sender, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            objCust.id = int.Parse(e.CommandArgument.ToString());
            objCust.DeletehumbalpricelistSetup();
            FillSubCategory();
        }

        if (e.CommandName == "Edit")
        {
            FillData(int.Parse(e.CommandArgument.ToString()));

        }
    }
    protected void grdCustomer_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (Sessions.UserRole.ToString() != "Admin")
            {
                LinkButton lbEdit = (LinkButton)e.Item.FindControl("lbEdit");
                LinkButton lbDelete = (LinkButton)e.Item.FindControl("lbDelete");

                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
                if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
                {
                    Response.Redirect(Request.UrlReferrer.ToString());
                }
                if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
                {

                    lbDelete.Visible = false;

                }
                if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
                {

                    lbEdit.Visible = false;

                }

            }
        }

    }

    protected void grdCustomer_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        grdCustomer.CurrentPageIndex = e.NewPageIndex;
        FillSubCategory();
    }
    #endregion


    #region Fill Main Category
    protected void FillMainCategory()
    {
        objCust.CategoryId = 0;
        DataSet ds = new DataSet();
        ds = objCust.GethumbalPricelistcategory();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlMainCategory.DataSource = ds.Tables[0];
            ddlMainCategory.DataTextField = "Categoryname";
            ddlMainCategory.DataValueField = "CategoryId";
            ddlMainCategory.DataBind();
            ddlMainCategory.Items.Insert(0, new ListItem("Select  Category", "0"));
        }


    }
    #endregion

    #region Fill Item Description
    protected void FillItemDesc()
    {
        objCust.ItemId = 0;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlItemDesc.DataSource = ds.Tables[0];
            ddlItemDesc.DataTextField = "ItemDesc";
            ddlItemDesc.DataValueField = "ItemId";
            ddlItemDesc.DataBind();
            ddlItemDesc.Items.Insert(0, new ListItem("Select Item", "0"));


        }

    }
    #endregion
}