﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class BoutiqueSalesInvoiceAcReport : System.Web.UI.Page
{
    #region properties
    clsCookie ck = new clsCookie();
    SalesInvoices objSalesInvoice = new SalesInvoices();
SetCustomers objCust = new SetCustomers();
    Reports objReports = new Reports();
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        if (!IsPostBack)
        {
            txtstartDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
            txtEndDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");

            BindBrand();
            FillLabel();

        }

    }
    #region btnGenrete
    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        string urllast = "BoutiqueSalesInvoiceAcReportPrint.aspx?From=" + txtstartDate.Text + "&To=" + txtEndDate.Text + "&Item=" + ddlItem.SelectedValue + "&MainCategoryCode=" + ddlBrand.SelectedValue + "&SubCategoryCode=" + ddlLabel.SelectedValue;
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open('" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);
    }
    #endregion



    #region BindBrand
    protected void BindBrand()
    {

        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.MainCategoryCode = 0;
        DataSet ds = new DataSet();
        ds = objCust.GetMainCategory();
        ddlBrand.DataSource = ds.Tables[0];
        ddlBrand.DataTextField = "MainCategoryName";
        ddlBrand.DataValueField = "MainCategoryCode";
        ddlBrand.DataBind();
        ddlBrand.Items.Insert(0, new ListItem("Select Brand", "0"));
        ddlBrand.SelectedValue = Convert.ToString(Sessions.Brand);

    }
    #endregion
    #region BindLabel
    protected void FillLabel()
    {
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.MainCategoryCode = Convert.ToInt32(ddlBrand.SelectedValue);
        DataSet ds = new DataSet();
        ds = objCust.GetSubCategoryByMainCategory();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlLabel.DataSource = ds.Tables[0];
            ddlLabel.DataTextField = "SubCategoryName";
            ddlLabel.DataValueField = "SubCategoryCode";
            ddlLabel.DataBind();
            ddlLabel.Items.Insert(0, new ListItem("Select Label", "0"));
        }

    }
    #endregion

    protected void ddlLabel_SelectedIndexChanged(object sender, EventArgs e)
    {
        getItemByBrand();
    }
    #region BindLabel
    protected void getItemByBrand()
    {
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.BrandId = Convert.ToInt32(ddlBrand.SelectedValue);
        objCust.SubCategoryCode = Convert.ToInt32(ddlLabel.SelectedValue);
        DataSet ds = new DataSet();
        ds = objCust.getItemByBrand();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlItem.DataSource = ds.Tables[0];
            ddlItem.DataTextField = "ItemDesc";
            ddlItem.DataValueField = "ItemId";
            ddlItem.DataBind();
            ddlItem.Items.Insert(0, new ListItem("Select Item", "0"));
        }

    }
    #endregion
}