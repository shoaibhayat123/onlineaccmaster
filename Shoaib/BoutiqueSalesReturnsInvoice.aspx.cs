﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class BoutiqueSalesReturnsInvoice : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();
    int Last = 0;
    int Corrent = 0;
    int FirstRecord = 0;
    int LastRecord = 0;
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {

        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        if (!IsPostBack)
        {
            ViewState["s"] = null;
            txtInvNo.Focus();
            FillCostCenter();
            FillClientAC();
            FillSalesAc();
            FillItemDesc();
            FillAllLedgerAc();
            DataTable dt = new DataTable();
            dgGallery.DataSource = dt;
            dgGallery.DataBind();
            Session["Corrent"] = null;
            Session["FirstRecord"] = null;
            Session["LastRecord"] = null;
            Session["InvoiceSummary"] = null;
            if (Convert.ToString(Request["addnew"]) != "1")
            {

                bindpagging();
                if (Request["InvoiceSummaryId"] == null)
                {
                    BindInvoice();
                }
                else
                {
                    DataTable dtInvoiceSummary = new DataTable();
                    dtInvoiceSummary = (DataTable)Session["InvoiceSummary"];
                    DataView dv = new DataView(dtInvoiceSummary);
                    dv.RowFilter = "InvoiceSummaryId=" + Request["InvoiceSummaryId"];
                    dtInvoiceSummary = dv.ToTable();
                    if (dtInvoiceSummary.Rows.Count > 0)
                    {
                        Session["Corrent"] = Convert.ToInt32(dtInvoiceSummary.Rows[0]["row"]);
                        BindInvoice();
                    }
                    else
                    {
                        Response.Redirect("default.aspx");
                    }
                }


            }
            else
            {
                objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objSalesInvoice.InvoiceSummaryId = 0;
                objSalesInvoice.DataFrom = "SALERETINV";
                DataTable dtInvoiceNewDetails = new DataTable();
                dtInvoiceNewDetails = objSalesInvoice.getBoutiqueInvoiceInvNo();
                if (dtInvoiceNewDetails.Rows.Count > 0)
                {
                    int Last = Convert.ToInt32(dtInvoiceNewDetails.Rows.Count) - 1;
                    ddlSaleAC.SelectedValue = Convert.ToString(dtInvoiceNewDetails.Rows[Last]["SalesAccountId"]);
                    // ddlSaleTaxAC.SelectedValue = Convert.ToString(dtInvoiceNewDetails.Rows[Last]["SaleTaxAccountId"]);
                    spanLastInvoice.Style.Add("display", "");
                    litLastInvoiceNo.Text = Convert.ToString(dtInvoiceNewDetails.Rows[Last]["InvNo"]);
                    litLastInvDate.Text = ClsGetDate.FillFromDate(Convert.ToString(dtInvoiceNewDetails.Rows[Last]["InvDate"]));
                    txtInvoiceDate.Text = Convert.ToDateTime(dtInvoiceNewDetails.Rows[Last]["InvDate"]).ToString("dd MMM yyyy");
                    string Str = Convert.ToString(dtInvoiceNewDetails.Rows[Last]["InvNo"]);
                    double Num;
                    bool isNum = double.TryParse(Str, out Num);
                    if (isNum)
                    {
                        txtInvNo.Text = (Convert.ToInt32(dtInvoiceNewDetails.Rows[Last]["InvNo"]) + 1).ToString();
                    }
                }
                else
                {
                    txtInvoiceDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
                    txtInvNo.Text = "1";
                }
                spanPgging.Style.Add("display", "none");
                btnDeleteInvoice.Style.Add("display", "none");
                //btnPrint.Style.Add("display", "none");
                btnPrintBill.Style.Add("display", "none");
            }
        }

        SetUserRight();

    }
    #endregion

    #region Fill Item Description
    protected void FillItemDesc()
    {
        objCust.ItemId = 0;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlItemDesc.DataSource = ds.Tables[0];
            ddlItemDesc.DataTextField = "ItemDesc";
            ddlItemDesc.DataValueField = "ItemId";
            ddlItemDesc.DataBind();
            ddlItemDesc.Items.Insert(0, new ListItem("Select Item", "0"));


        }

    }
    #endregion

    #region Fill Cost Center
    protected void FillCostCenter()
    {

        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.CostCenterCode = 0;
        DataSet ds = new DataSet();
        ds = objCust.GetCostCenter();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCostCenter.DataSource = ds.Tables[0];
            ddlCostCenter.DataTextField = "CostCenterName";
            ddlCostCenter.DataValueField = "CostCenterCode";
            ddlCostCenter.DataBind();
            ddlCostCenter.Items.Insert(0, new ListItem("Select cost center", "0"));

        }



    }
    #endregion

    #region Fill  Client A/C
    protected void FillClientAC()
    {

        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtClient = new DataTable();
        dtClient = objSalesInvoice.getAcFileAllLedger();
        if (dtClient.Rows.Count > 0)
        {
            ddlClientAC.DataSource = dtClient;
            ddlClientAC.DataTextField = "Title";
            ddlClientAC.DataValueField = "Id";
            ddlClientAC.DataBind();
            ddlClientAC.Items.Insert(0, new ListItem("Select Client A/c", "0"));

        }



    }
    #endregion

    #region Fill  FillSales A/c
    protected void FillSalesAc()
    {

        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtLedger = new DataTable();
        dtLedger = objSalesInvoice.getAcFileBySales();
        if (dtLedger.Rows.Count > 0)
        {
            ddlSaleAC.DataSource = dtLedger;
            ddlSaleAC.DataTextField = "Title";
            ddlSaleAC.DataValueField = "Id";
            ddlSaleAC.DataBind();
            ddlSaleAC.Items.Insert(0, new ListItem("Select Sales Return A/c", "0"));

            //ddlSaleTaxAC.DataSource = dtLedger;
            //ddlSaleTaxAC.DataTextField = "Title";
            //ddlSaleTaxAC.DataValueField = "Id";
            //ddlSaleTaxAC.DataBind();
            //ddlSaleTaxAC.Items.Insert(0, new ListItem("Select Sales A/C", "0"));

        }


    }
    #endregion

    #region Add Details
    protected void btn_saveInvoice_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
        if (Sessions.FromDate != null)
        {
            if (ViewState["s"] != null)
            {
                if ((Convert.ToDateTime(txtInvoiceDate.Text) >= Convert.ToDateTime(Sessions.FromDate)) && (Convert.ToDateTime(txtInvoiceDate.Text) <= Convert.ToDateTime(Sessions.ToDate)))
                {
                    if (Convert.ToDecimal(lblBillAmount.Text) >= 0)
                    {
                        if (hdnInvoiceSummaryId.Value == "")
                        {
                            hdnInvoiceSummaryId.Value = "0";
                        }
                        /************ Add Invoice Summary **************/
                        if (Convert.ToInt32(hdnInvoiceSummaryId.Value) > 0)
                        {
                            objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(hdnInvoiceSummaryId.Value);
                        }
                        else
                        {
                            objSalesInvoice.InvoiceSummaryId = 0;
                        }
                        objSalesInvoice.InvNo = txtInvNo.Text.ToUpper();
                        objSalesInvoice.InvDate = Convert.ToDateTime(txtInvoiceDate.Text);
                        if (txtCrDays.Text != "")
                        {
                            objSalesInvoice.CrDays = Convert.ToInt32(txtCrDays.Text);
                        }
                        else
                        {
                            objSalesInvoice.CrDays = 0;
                        }
                        objSalesInvoice.storeId = Convert.ToInt32(ddlCostCenter.SelectedValue);
                        objSalesInvoice.ClientAccountId = Convert.ToInt32(ddlClientAC.SelectedValue);
                        objSalesInvoice.SalesAccountId = Convert.ToInt32(ddlSaleAC.SelectedValue);
                        //objSalesInvoice.SaleTaxAccountId = Convert.ToInt32(ddlSaleTaxAC.SelectedValue);
                        objSalesInvoice.TotalQuantity = Convert.ToDecimal(txtTotalQuantity.Text);
                        objSalesInvoice.TotalGrossAmount = Convert.ToDecimal(txtTotalGrossAmount.Text);
                        //objSalesInvoice.TotalSaleTaxAmount = Convert.ToDecimal(txtSTtotalAmount.Text);
                        // objSalesInvoice.TotalAmountIncludeTax = Convert.ToDecimal(txtTotalAmountIncludingTaxe.Text);
                        objSalesInvoice.CartageAmount = Convert.ToDecimal(txtCartagecharges.Text);
                        objSalesInvoice.BillAmount = Convert.ToDecimal(lblBillAmount.Text);
                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                        objSalesInvoice.Description = txtDescription.Text;
                        objSalesInvoice.InvoiceDiscount = Convert.ToDecimal(txtDiscount.Text);
                        if (txtconversionRate.Text != "")
                        {
                            objSalesInvoice.ConversionRate = Convert.ToDecimal(txtconversionRate.Text);
                        }
                        objSalesInvoice.SalesInfoRefNo = Convert.ToString(txtSalesInvNo.Text);

                        objSalesInvoice.DataFrom = "SALERETINV";
                        DataTable dtresult = new DataTable();
                        dtresult = objSalesInvoice.AddEditBoutiqueSalesReturnInvoiceSummary();

                        /************ Add Trans ***************/

                        objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                        objSalesInvoice.DataFrom = "SALERETINV";
                        objSalesInvoice.DeleteTrans();

                        for (int i = 0; i < 2; i++)
                        {
                            objSalesInvoice.TransId = 0;

                            objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvoiceDate.Text);

                            objSalesInvoice.DataFrom = "SALERETINV";
                            objSalesInvoice.InvoiceNo = txtInvNo.Text.ToUpper();
                            //objSalesInvoice.ChqNo = "";

                            //objSalesInvoice.ChqDate = Convert.ToDateTime(System.DateTime.Now.ToString());
                            if (txtCrDays.Text != "")
                            {
                                objSalesInvoice.Crdays = Convert.ToInt32(txtCrDays.Text);
                            }
                            else
                            {
                                objSalesInvoice.Crdays = 0;
                            }

                            /**** Add CR Days *******/
                            System.DateTime today = Convert.ToDateTime(txtInvoiceDate.Text);
                            if (txtCrDays.Text != "")
                            {
                                System.DateTime answer = today.AddDays(Convert.ToInt32(txtCrDays.Text));
                                objSalesInvoice.DueDate = answer;
                            }
                            else
                            {
                                System.DateTime answer = Convert.ToDateTime(txtInvoiceDate.Text);
                                objSalesInvoice.DueDate = answer;
                            }

                            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                            if (i == 0)  /* For Client account */
                            {
                                objSalesInvoice.AcFileId = Convert.ToInt32(ddlClientAC.SelectedValue);
                                objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(lblBillAmount.Text);
                                if (txtconversionRate.Text != "")
                                {
                                    objSalesInvoice.FCAmount = 0 - Convert.ToDecimal(lblBillAmount.Text) * Convert.ToDecimal(txtconversionRate.Text);
                                }
                                objSalesInvoice.Description = txtDescription.Text;
                            }

                            if (i == 1) /* For Sales  account */
                            {
                                objSalesInvoice.AcFileId = Convert.ToInt32(ddlSaleAC.SelectedValue);

                                objSalesInvoice.TransAmt = (Convert.ToDecimal(txtTotalGrossAmount.Text) + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text));

                                if (txtconversionRate.Text != "")
                                {
                                    objSalesInvoice.FCAmount = (Convert.ToDecimal(txtTotalGrossAmount.Text) + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text)) * Convert.ToDecimal(txtconversionRate.Text);
                                }


                                objSalesInvoice.Description = txtDescription.Text;
                            }


                            objSalesInvoice.AddEditTrans();

                        }






                        /************ Add Invoice**************/
                        if (hdnInvoiceId.Value != "")
                        {
                            objFile.id = Convert.ToInt32(hdnInvoiceId.Value);
                            objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
                            objFile.DataFrom = "SALERETINV";
                            objFile.deleteInvoiceFromInvoiceTax();
                        }

                        objFile.InvoiceDate = Convert.ToDateTime(txtInvoiceDate.Text);
                        objFile.InvoiceNo = Convert.ToString(txtInvNo.Text).ToUpper();
                        if (txtCrDays.Text != "")
                        {
                            objFile.CreditDays = Convert.ToInt32(txtCrDays.Text);
                        }
                        else
                        {
                            objFile.CreditDays = 0;
                        }
                        objFile.Amount = Convert.ToDecimal(lblBillAmount.Text);
                        objFile.AcFileId = Convert.ToInt32(ddlClientAC.SelectedValue);
                        objFile.DataFrom = "SALERETINV";
                        objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
                        objFile.DebitCredit = "Credit";

                        if (hdnInvoiceId.Value != "")
                        {
                            objFile.InvoiceID = Convert.ToInt32(hdnInvoiceId.Value);
                        }
                        else
                        {
                            objFile.InvoiceID = 0;
                        }
                        objFile.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString());

                        objFile.AddeditInvoice();

                        /************ Add Invoice Detail **************/
                        if (dtresult.Rows.Count > 0)
                        {
                            if (ViewState["s"] != null)
                            {
                                DataTable dt = new DataTable();
                                dt = (DataTable)ViewState["s"];
                                if (dt.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        objSalesInvoice.ItemId = 0;
                                        objSalesInvoice.ItemCode = Convert.ToInt32(dt.Rows[i]["ItemCode"].ToString());
                                        // objSalesInvoice.ItemDesc = Convert.ToString(dt.Rows[i]["ItemDesc"].ToString());
                                        objSalesInvoice.Quantity = Convert.ToDecimal(dt.Rows[i]["Quantity"].ToString());
                                        objSalesInvoice.Rate = Convert.ToDecimal(dt.Rows[i]["Rate"].ToString());
                                        objSalesInvoice.GrossAmount = Convert.ToDecimal(dt.Rows[i]["GrossAmount"].ToString());
                                        objSalesInvoice.SaleTax = Convert.ToDecimal(dt.Rows[i]["SaleTax"].ToString());
                                        objSalesInvoice.SaleTaxAmount = Convert.ToDecimal(dt.Rows[i]["SaleTaxAmount"].ToString());
                                        objSalesInvoice.AmountIncludeTaxes = Convert.ToDecimal(dt.Rows[i]["AmountIncludeTaxes"].ToString());
                                        objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                        objSalesInvoice.DataFrom = "SALERETINV";
                                        objSalesInvoice.storeId = Convert.ToInt32(ddlCostCenter.SelectedValue);
                                        if (dt.Rows[i]["DRID"].ToString() != "")
                                        {
                                            objSalesInvoice.DRID = Convert.ToInt32(dt.Rows[i]["DRID"].ToString());
                                        }
                                        else
                                        {
                                            objSalesInvoice.DRID = 0;
                                        }

                                        if (dt.Rows[i]["CRID"].ToString() != "")
                                        {
                                            objSalesInvoice.CRID = Convert.ToInt32(dt.Rows[i]["CRID"].ToString());
                                        }
                                        else
                                        {
                                            objSalesInvoice.CRID = 0;
                                        }

                                        if (dt.Rows[i]["BoutiqueCostPrice"].ToString() != "")
                                        {
                                            objSalesInvoice.BoutiqueCostPrice = Convert.ToDecimal(dt.Rows[i]["BoutiqueCostPrice"].ToString());
                                        }

                                        DataTable dtInvoiceSummary = new DataTable();
                                        dtInvoiceSummary = objSalesInvoice.AddEditBoutiqueInvoiceDetail();


                                    }
                                }
                            }



                            DataTable dtTransData = (DataTable)ViewState["s"];

                            if (dtTransData.Rows.Count > 0)
                            {
                                for (int x = 0; x < dtTransData.Rows.Count; x++)
                                {
                                    if (Convert.ToString(dtTransData.Rows[x]["SaleTaxAmount"]) != "0.00" && Convert.ToString(dtTransData.Rows[x]["SaleTaxAmount"]) != "0")
                                    {

                                        for (int j = 0; j < 2; j++)
                                        {
                                            objSalesInvoice.TransId = 0;

                                            objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvoiceDate.Text);

                                            objSalesInvoice.DataFrom = "SALERETINV";
                                            objSalesInvoice.InvoiceNo = txtInvNo.Text.ToUpper();

                                            if (txtCrDays.Text != "")
                                            {
                                                objSalesInvoice.Crdays = Convert.ToInt32(txtCrDays.Text);
                                            }
                                            else
                                            {
                                                objSalesInvoice.Crdays = 0;
                                            }

                                            /**** Add CR Days *******/
                                            System.DateTime today = Convert.ToDateTime(txtInvoiceDate.Text);
                                            if (txtCrDays.Text != "")
                                            {
                                                System.DateTime answer = today.AddDays(Convert.ToInt32(txtCrDays.Text));
                                                objSalesInvoice.DueDate = answer;
                                            }
                                            else
                                            {
                                                System.DateTime answer = Convert.ToDateTime(txtInvoiceDate.Text);
                                                objSalesInvoice.DueDate = answer;
                                            }

                                            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                            objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString());
                                            if (j == 0)  /* For Dr. account */
                                            {
                                                objSalesInvoice.AcFileId = Convert.ToInt32(dtTransData.Rows[x]["DRID"].ToString());
                                                objSalesInvoice.TransAmt = Convert.ToDecimal(dtTransData.Rows[x]["SaleTaxAmount"].ToString());
                                                if (txtconversionRate.Text != "")
                                                {
                                                    objSalesInvoice.FCAmount = Convert.ToDecimal(dtTransData.Rows[x]["SaleTaxAmount"].ToString()) *
                                                        Convert.ToDecimal(txtconversionRate.Text);
                                                }
                                                objSalesInvoice.Description = "Charges on Sales Return Inv. " + txtInvNo.Text.ToUpper() + " Item : " + Convert.ToString(dtTransData.Rows[x]["ItemDesc"]);
                                            }

                                            if (j == 1) /* For Cr.  account */
                                            {
                                                objSalesInvoice.AcFileId = Convert.ToInt32(dtTransData.Rows[x]["CRID"].ToString());
                                                objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(dtTransData.Rows[x]["SaleTaxAmount"].ToString());
                                                if (txtconversionRate.Text != "")
                                                {
                                                    objSalesInvoice.FCAmount = 0 - (Convert.ToDecimal(dtTransData.Rows[x]["SaleTaxAmount"].ToString()) *
                                                        Convert.ToDecimal(txtconversionRate.Text));
                                                }

                                                objSalesInvoice.Description = "Charges on Sales Return Inv." + txtInvNo.Text.ToUpper() + " Item : " + Convert.ToString(dtTransData.Rows[x]["ItemDesc"]);
                                            }
                                            objSalesInvoice.AddEditTrans();
                                        }

                                    }
                                }
                            }






                            if (dtTransData.Rows.Count > 0)
                            {
                                for (int x = 0; x < dtTransData.Rows.Count; x++)
                                {
                                   

                                        for (int j = 0; j < 2; j++)
                                        {
                                            objSalesInvoice.TransId = 0;

                                            objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvoiceDate.Text);

                                            objSalesInvoice.DataFrom = "SALERETINV";
                                          

                                            if (txtCrDays.Text != "")
                                            {
                                                objSalesInvoice.Crdays = Convert.ToInt32(txtCrDays.Text);
                                            }
                                            else
                                            {
                                                objSalesInvoice.Crdays = 0;
                                            }

                                            /**** Add CR Days *******/
                                            System.DateTime today = Convert.ToDateTime(txtInvoiceDate.Text);
                                            if (txtCrDays.Text != "")
                                            {
                                                System.DateTime answer = today.AddDays(Convert.ToInt32(txtCrDays.Text));
                                                objSalesInvoice.DueDate = answer;
                                            }
                                            else
                                            {
                                                System.DateTime answer = Convert.ToDateTime(txtInvoiceDate.Text);
                                                objSalesInvoice.DueDate = answer;
                                            }

                                            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                            objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString());


                                            objSalesInvoice.ItemId = Convert.ToInt32(dtTransData.Rows[x]["ItemCode"].ToString());
                                            objSalesInvoice.InvoiceNo = Convert.ToString(txtSalesInvNo.Text);
                                            DataTable dtcharge = objSalesInvoice.getCharges();
                                            if (dtcharge.Rows.Count > 0)
                                            {
                                                objSalesInvoice.ItemId = 0;
                                                objSalesInvoice.InvoiceNo = txtInvNo.Text.ToUpper();
                                                if (j == 0)  /* For Dr. account */
                                                {
                                                    objSalesInvoice.AcFileId = Convert.ToInt32(dtcharge.Rows[0]["CRID"].ToString());

                                                    objSalesInvoice.TransAmt = Convert.ToDecimal(dtTransData.Rows[x]["Quantity"].ToString()) * Convert.ToDecimal(dtcharge.Rows[0]["AlterCharges"]);
                                                    if (txtconversionRate.Text != "")
                                                    {
                                                        objSalesInvoice.FCAmount = Convert.ToDecimal(dtTransData.Rows[x]["Quantity"].ToString()) * Convert.ToDecimal(dtcharge.Rows[0]["AlterCharges"]) * Convert.ToDecimal(txtconversionRate.Text);
                                                    }
                                                    objSalesInvoice.Description = "Reversal on Sales Charges Inv. " + txtInvNo.Text.ToUpper() + " Item : " + Convert.ToString(dtTransData.Rows[x]["ItemDesc"]);
                                                }

                                                if (j == 1) /* For Cr.  account */
                                                {


                                                    objSalesInvoice.AcFileId = Convert.ToInt32(dtcharge.Rows[0]["DRID"].ToString());
                                                    objSalesInvoice.TransAmt = 0 - (Convert.ToDecimal(dtTransData.Rows[x]["Quantity"].ToString()) * Convert.ToDecimal(dtcharge.Rows[0]["AlterCharges"]));

                                                    if (txtconversionRate.Text != "")
                                                    {
                                                        objSalesInvoice.FCAmount = 0 - (
     Convert.ToDecimal(dtTransData.Rows[x]["Quantity"].ToString()) * Convert.ToDecimal(dtcharge.Rows[0]["AlterCharges"]) * Convert.ToDecimal(txtconversionRate.Text));
                                                    }

                                                    objSalesInvoice.Description = "Reversal on Sales Charges Inv." + txtInvNo.Text.ToUpper() + " Item : " + Convert.ToString(dtTransData.Rows[x]["ItemDesc"]);
                                                }
                                                objSalesInvoice.AddEditTrans();
                                            }

                                        }
                                    
                                }

                            }











                        }



                        if (Convert.ToInt32(hdnInvoiceSummaryId.Value) > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Updated.');location.href='BoutiqueSalesReturnsInvoice.aspx';", true);

                        }

                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Saved.');location.href='BoutiqueSalesReturnsInvoice.aspx';", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Invalid Invoice Amount.');", true);

                    }
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Date out of accouting year.');", true);

                }
            }

            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please Fill Items Details.');", true);
            }
        }
    }
    #endregion

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("BoutiqueSalesReturnsInvoice.aspx");

    }
    #endregion

    #region Fill session Table
    public void filltable()
    {
        if (ViewState["s"] == null)
        {

            DataColumn column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "ItemCode";
            column.Caption = "ItemCode";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ItemDesc";
            column.Caption = "ItemDesc";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Quantity";
            column.Caption = "Quantity";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Rate";
            column.Caption = "Rate";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "GrossAmount";
            column.Caption = "GrossAmount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "SaleTax";
            column.Caption = "SaleTax";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "SaleTaxAmount";
            column.Caption = "SaleTaxAmount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "AmountIncludeTaxes";
            column.Caption = "AmountIncludeTaxes";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "DRID";
            column.Caption = "DRID";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "CRID";
            column.Caption = "CRID";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "BoutiqueCostPrice";
            column.Caption = "BoutiqueCostPrice";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "DRTitle";
            column.Caption = "DRTitle";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "CRTitle";
            column.Caption = "CRTitle";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

        }
        else
        {
            dttbl = (DataTable)ViewState["s"];
        }
    }
    #endregion

    #region Add New Bill details
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        if (Convert.ToString(txtDesignerPrice.Text) == "")
        {
            filAddNew();

        }
        else
        {
            if (Convert.ToDecimal(txtDesignerPrice.Text) > 0)
            {
                if ((ddlDR.SelectedValue.ToString() != "0") && (ddlCR.SelectedValue.ToString() != "0"))
                {
                    filAddNew();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please select valid Dr. Or CR. account');", true);
                }

            }
            else
            {
                filAddNew();
            }
        }    
      
    }

    protected void filAddNew()
    {
        hdnButtonText.Value = "";
        filltable();
        if (hdndetail.Value == "")
        {
            DataRow row;
            if (dttbl.Rows.Count > 0)
            {

                int maxid = 0;
                for (int i = 0; i < dttbl.Rows.Count; i++)
                {
                    if (dttbl.Rows[i]["Id"].ToString() != "")
                    {
                        if (maxid < Convert.ToInt32(dttbl.Rows[i]["Id"].ToString()))
                        {
                            maxid = Convert.ToInt32(dttbl.Rows[i]["Id"].ToString());
                        }
                    }
                }
                decimal Rate = 0;
                decimal SaleTex = 0;
                row = dttbl.NewRow();
                row["Id"] = maxid + 1;
                row["ItemCode"] = Convert.ToInt32(ddlItemDesc.SelectedValue.ToString());
                row["ItemDesc"] = Convert.ToString(ddlItemDesc.SelectedItem.Text);
                row["Quantity"] = Convert.ToDecimal(txtQuantity.Text);
                if (txtRate.Text != "")
                {
                    Rate = Convert.ToDecimal(txtRate.Text);
                }
                //if (txtSt.Text != "")
                //{
                SaleTex = 0;
                //}


                row["Rate"] = Rate;
                row["GrossAmount"] = Rate * Convert.ToDecimal(txtQuantity.Text);
                row["SaleTax"] = SaleTex;

                row["AmountIncludeTaxes"] = (Rate * Convert.ToDecimal(txtQuantity.Text)) + ((SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100);

                if (txtDesignerPrice.Text != "")
                {
                    row["SaleTaxAmount"] = Convert.ToDecimal(txtQuantity.Text) * Convert.ToDecimal(txtDesignerPrice.Text);
                }
                else
                {
                    row["SaleTaxAmount"] = 0;
                }
               

                if (txtDesignerPrice.Text != "")
                {

                    if (Convert.ToDecimal(txtDesignerPrice.Text) > 0)
                    {
                        row["BoutiqueCostPrice"] = Convert.ToDecimal(txtDesignerPrice.Text);

                        if (ddlDR.SelectedValue.ToString() != "0")
                        {
                            row["DRID"] = Convert.ToInt32(ddlDR.SelectedValue.ToString());
                            row["DRTitle"] = ddlDR.SelectedItem.Text;
                        }
                        else
                        {
                            row["DRID"] = 0;
                            row["DRTitle"] = "";
                        }

                        if (ddlCR.SelectedValue.ToString() != "0")
                        {
                            row["CRID"] = Convert.ToInt32(ddlCR.SelectedValue.ToString());
                            row["CRTitle"] = ddlCR.SelectedItem.Text;
                        }
                        else
                        {
                            row["CRID"] = 0;
                            row["CRTitle"] = "";
                        }
                    }
                    else
                    {
                        row["BoutiqueCostPrice"] = 0;
                        row["DRID"] = 0;
                        row["DRTitle"] = "";
                        row["CRID"] = 0;
                        row["CRTitle"] = "";
                    }

                }

                else
                {
                    row["BoutiqueCostPrice"] = 0;
                    row["DRID"] = 0;
                    row["DRTitle"] = "";
                    row["CRID"] = 0;
                    row["CRTitle"] = "";
                }

            }
            else
            {
                decimal Rate = 0;
                decimal SaleTex = 0;
                row = dttbl.NewRow();
                row["Id"] = 1;
                row["ItemCode"] = Convert.ToInt32(ddlItemDesc.SelectedValue.ToString());
                row["ItemDesc"] = Convert.ToString(ddlItemDesc.SelectedItem.Text);
                row["Quantity"] = Convert.ToDecimal(txtQuantity.Text);
                if (txtRate.Text != "")
                {
                    Rate = Convert.ToDecimal(txtRate.Text);
                }
                //if (txtSt.Text != "")
                //{
                SaleTex = 0;
                //}
                row["Rate"] = Rate;
                row["GrossAmount"] = Rate * Convert.ToDecimal(txtQuantity.Text);
                row["SaleTax"] = SaleTex;
                //row["SaleTaxAmount"] = (SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100;
                row["AmountIncludeTaxes"] = (Rate * Convert.ToDecimal(txtQuantity.Text)) + ((SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100);



                if (txtDesignerPrice.Text != "")
                {
                    row["SaleTaxAmount"] = Convert.ToDecimal(txtQuantity.Text) * Convert.ToDecimal(txtDesignerPrice.Text);
                }
                else
                {
                    row["SaleTaxAmount"] = 0;

                }
              

                if (txtDesignerPrice.Text != "")
                {
                    if (Convert.ToDecimal(txtDesignerPrice.Text) > 0)
                    {
                        row["BoutiqueCostPrice"] = Convert.ToDecimal(txtDesignerPrice.Text);
                        if (ddlDR.SelectedValue.ToString() != "0")
                        {
                            row["DRID"] = Convert.ToInt32(ddlDR.SelectedValue.ToString());
                            row["DRTitle"] = ddlDR.SelectedItem.Text;
                        }
                        else
                        {
                            row["DRID"] = 0;
                            row["DRTitle"] = "";
                        }

                        if (ddlCR.SelectedValue.ToString() != "0")
                        {
                            row["CRID"] = Convert.ToInt32(ddlCR.SelectedValue.ToString());
                            row["CRTitle"] = ddlCR.SelectedItem.Text;
                        }
                        else
                        {
                            row["CRID"] = 0;
                            row["CRTitle"] = "";
                        }
                    }
                    else
                    {
                        row["BoutiqueCostPrice"] = 0;
                        row["DRID"] = 0;
                        row["DRTitle"] = "";
                        row["CRID"] = 0;
                        row["CRTitle"] = "";
                    }
                }

                else
                {
                    row["BoutiqueCostPrice"] = 0;
                    row["DRID"] = 0;
                    row["DRTitle"] = "";
                    row["CRID"] = 0;
                    row["CRTitle"] = "";


                }



            }
            dttbl.Rows.Add(row);
            dttbl.AcceptChanges();
        }
        else
        {
            decimal Rate = 0;
            decimal SaleTex = 0;
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == hdndetail.Value)
                {
                    dttbl.Rows[i]["ItemCode"] = Convert.ToInt32(ddlItemDesc.SelectedValue.ToString());
                    dttbl.Rows[i]["ItemDesc"] = Convert.ToString(ddlItemDesc.SelectedItem.Text);
                    dttbl.Rows[i]["Quantity"] = Convert.ToDecimal(txtQuantity.Text);
                    if (txtRate.Text != "")
                    {
                        Rate = Convert.ToDecimal(txtRate.Text);
                    }
                    //if (txtSt.Text != "")
                    //{
                    SaleTex = 0;
                    //}
                    dttbl.Rows[i]["Rate"] = Rate;
                    dttbl.Rows[i]["GrossAmount"] = Rate * Convert.ToDecimal(txtQuantity.Text);
                    dttbl.Rows[i]["SaleTax"] = SaleTex;

                    dttbl.Rows[i]["AmountIncludeTaxes"] = (Rate * Convert.ToDecimal(txtQuantity.Text)) + ((SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100);

                    if (txtDesignerPrice.Text != "")
                    {
                        dttbl.Rows[i]["SaleTaxAmount"] = Convert.ToDecimal(txtQuantity.Text) * Convert.ToDecimal(txtDesignerPrice.Text);
                        dttbl.Rows[i]["BoutiqueCostPrice"] = Convert.ToDecimal(txtDesignerPrice.Text);
                    }
                    else
                    {
                        dttbl.Rows[i]["SaleTaxAmount"] = 0;
                        dttbl.Rows[i]["BoutiqueCostPrice"] = 0;

                    }
                    if (ddlDR.SelectedValue.ToString() != "0")
                    {
                        dttbl.Rows[i]["DRID"] = Convert.ToInt32(ddlDR.SelectedValue.ToString());
                        dttbl.Rows[i]["DRTitle"] = ddlDR.SelectedItem.Text;
                    }
                    else
                    {
                        dttbl.Rows[i]["DRID"] = 0;
                        dttbl.Rows[i]["DRTitle"] = "";

                    }

                    if (ddlCR.SelectedValue.ToString() != "0")
                    {
                        dttbl.Rows[i]["CRID"] = Convert.ToInt32(ddlCR.SelectedValue.ToString());
                        dttbl.Rows[i]["CRTitle"] = ddlCR.SelectedItem.Text;
                    }
                    else
                    {
                        dttbl.Rows[i]["CRID"] = 0;
                        dttbl.Rows[i]["CRTitle"] = "";

                    }
                }
            }
        }
        ViewState.Add("s", dttbl);
        dgGallery.DataSource = dttbl;
        dgGallery.DataBind();
        ShowTotal();
        if (dttbl.Rows.Count > 0)
        {
            dgGallery.Visible = true;
        }

        ClearInvoiceDetail();
        ddlItemDesc.Focus();
    }
    #endregion

    #region Grid Event
    protected void dgGallery_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{
        //    ((Label)e.Item.FindControl("lblInvoiceDate")).Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "InvoiceDate").ToString());

        //}



    }
    protected void dgGallery_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        dttbl = (DataTable)ViewState["s"];
        if (e.CommandName == "Delete")
        {
            int j = 0;
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    dttbl.Rows.RemoveAt(i);
                }
            }
            dttbl.AcceptChanges();
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            ViewState.Add("s", dttbl);
            ShowTotal();

        }


        if (e.CommandName == "Edit")
        {
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {


                    ddlItemDesc.SelectedValue = dttbl.Rows[i]["ItemCode"].ToString();
                    txtQuantity.Text = dttbl.Rows[i]["Quantity"].ToString();
                    txtRate.Text = dttbl.Rows[i]["Rate"].ToString();
                    hdndetail.Value = dttbl.Rows[i]["Id"].ToString();

                    if (dttbl.Rows[i]["DRID"].ToString() != "")
                    {
                        ddlDR.SelectedValue = dttbl.Rows[i]["DRID"].ToString();
                    }
                    if (dttbl.Rows[i]["CRID"].ToString() != "")
                    {
                        ddlCR.SelectedValue = dttbl.Rows[i]["CRID"].ToString();
                    }
                    txtDesignerPrice.Text = dttbl.Rows[i]["BoutiqueCostPrice"].ToString();
                }
            }
        }

    }
    #endregion

    #region ClearInvoiceDetail
    protected void ClearInvoiceDetail()
    {
        ddlItemDesc.SelectedValue = "0";
        txtQuantity.Text = "";
        txtRate.Text = "";
        // txtSt.Text = "";
        hdndetail.Value = "";
        txtDesignerPrice.Text = "";
        ddlCR.SelectedValue = "0";
        ddlDR.SelectedValue = "0";
    }
    #endregion

    #region Show Total
    protected void ShowTotal()
    {
        Decimal Quantity = 0;
        Decimal TotalGrossAmount = 0;
        Decimal TotalStAmount = 0;
        Decimal amountIncludingTax = 0;

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["s"];
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Quantity += Convert.ToDecimal(dt.Rows[i]["Quantity"]);
                TotalGrossAmount += Convert.ToDecimal(dt.Rows[i]["GrossAmount"]);
                // TotalStAmount += Convert.ToDecimal(dt.Rows[i]["SaleTaxAmount"]);
                amountIncludingTax += (0 + Convert.ToDecimal(dt.Rows[i]["GrossAmount"]));

            }
        }

        txtTotalQuantity.Text = String.Format("{0:C}", Quantity).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        txtTotalGrossAmount.Text = String.Format("{0:C}", TotalGrossAmount).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        lblBillAmount.Text = String.Format("{0:C}", (amountIncludingTax + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


    }
    #endregion



    #region Pagging
    private void bindpagging()
    {
        Last = 0;
        Corrent = 0;
        FirstRecord = 0;
        LastRecord = 0;
        //  int row = Convert.ToInt32(Request["row"]);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.InvoiceSummaryId = 0;
        DataTable dt = new DataTable();
        objSalesInvoice.DataFrom = "SALERETINV";
        dt = objSalesInvoice.getInvoiceSummary();
        if (dt.Rows.Count > 0)
        {
            Session["InvoiceSummary"] = dt;
            Last = dt.Rows.Count;
            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            //Corrent = Convert.ToInt32(dt.Rows[0]["Row"]);
            Session["FirstRecord"] = FirstRecord;
            Session["LastRecord"] = LastRecord;
            Session["Corrent"] = LastRecord;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "location.href='BoutiqueSalesReturnsInvoice.aspx?addnew=1';", true);
        }


    }
    protected void lnkFirst_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["FirstRecord"];
        BindInvoice();

    }
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["FirstRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) - 1;
            BindInvoice();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["LastRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) + 1;
            BindInvoice();
        }
    }
    protected void lnLast_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["LastRecord"];
        BindInvoice();
    }
    #endregion

    #region Fill invoice summary
    protected void BindInvoice()
    {
        if (Session["InvoiceSummary"] != null)
        {
            DataTable dtInvoiceSummary = (DataTable)Session["InvoiceSummary"];
            if (dtInvoiceSummary.Rows.Count > 0)
            {

                DataView DvInvoiceSummary = dtInvoiceSummary.DefaultView;
                DvInvoiceSummary.RowFilter = "Row = '" + Session["Corrent"].ToString() + "'";
                dtInvoiceSummary = DvInvoiceSummary.ToTable();
                if (dtInvoiceSummary.Rows.Count > 0)
                {
                    txtconversionRate.Text = "";
                    txtSalesInvNo.Text = "";





                    txtInvNo.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["InvNo"]);
                    txtInvoiceDate.Text = Convert.ToDateTime(dtInvoiceSummary.Rows[0]["InvDate"].ToString()).ToString("dd MMM yyyy");
                    txtconversionRate.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["ConversionRate"]);
                    txtSalesInvNo.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["SalesInfoRefNo"]);

                    if (Convert.ToString(dtInvoiceSummary.Rows[0]["CrDays"]) != "0")
                    {
                        txtCrDays.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CrDays"]);
                    }
                    else
                    {
                        txtCrDays.Text = "";
                    }
                    ddlCostCenter.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["storeId"]);
                    ddlClientAC.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["ClientAccountId"]);
                    ddlSaleAC.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["SalesAccountId"]);
                    txtTotalQuantity.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["TotalQuantity"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    txtTotalGrossAmount.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["TotalGrossAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

                    txtCartagecharges.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["CartageAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

                    lblBillAmount.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["BillAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

                    hdnInvoiceSummaryId.Value = Convert.ToString(dtInvoiceSummary.Rows[0]["InvoiceSummaryId"]);

                    txtDiscount.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["InvoiceDiscount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

                    txtDescription.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["Description"]);
                    fillInvoiceDetails(Convert.ToInt32(dtInvoiceSummary.Rows[0]["InvoiceSummaryId"]));

                    /* For invoice Update */
                    hdnInvoiceId.Value = Convert.ToString(dtInvoiceSummary.Rows[0]["InvoiceID"]);



                }
            }

        }
    }
    #endregion

    #region fillInvoiceDetails
    protected void fillInvoiceDetails(int pid)
    {
        ViewState["s"] = null;

        DataTable dtInvoice = new DataTable();
        objSalesInvoice.InvoiceSummaryId = pid;
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objSalesInvoice.DataFrom = "SALERETINV";
        dtInvoice = objSalesInvoice.getInvoiceDetails();

        if (dtInvoice.Rows.Count > 0)
        {
            for (int i = 0; i < dtInvoice.Rows.Count; i++)
            {
                filltable();
                DataRow row;
                row = dttbl.NewRow();
                row["Id"] = i + 1;

                row["ItemCode"] = Convert.ToInt32(dtInvoice.Rows[i]["ItemCode"]);
                row["ItemDesc"] = Convert.ToString(dtInvoice.Rows[i]["ItemDesc"]);
                row["Quantity"] = Convert.ToDecimal(dtInvoice.Rows[i]["Quantity"]);
                row["Rate"] = Convert.ToDecimal(dtInvoice.Rows[i]["Rate"]);
                row["GrossAmount"] = Convert.ToDecimal(dtInvoice.Rows[i]["GrossAmount"]);
                row["SaleTax"] = Convert.ToDecimal(dtInvoice.Rows[i]["SaleTax"]);
                row["SaleTaxAmount"] = Convert.ToDecimal(dtInvoice.Rows[i]["SaleTaxAmount"]);
                row["AmountIncludeTaxes"] = Convert.ToDecimal(dtInvoice.Rows[i]["AmountIncludeTaxes"]);
                if (dtInvoice.Rows[i]["DRID"].ToString() != "")
                {
                    row["DRID"] = Convert.ToInt32(dtInvoice.Rows[i]["DRID"]);
                    row["DRTitle"] = Convert.ToString(dtInvoice.Rows[i]["DRTitle"]);
                }
                if (dtInvoice.Rows[i]["CRID"].ToString() != "")
                {
                    row["CRID"] = Convert.ToInt32(dtInvoice.Rows[i]["CRID"]);
                    row["CRTitle"] = Convert.ToString(dtInvoice.Rows[i]["CRTitle"]);
                }
                row["BoutiqueCostPrice"] = Convert.ToDecimal(dtInvoice.Rows[i]["BoutiqueCostPrice"]);
                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
                ViewState.Add("s", dttbl);
            }
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            dgGallery.Visible = true;

        }
        else
        {
            dgGallery.Visible = false;
        }

    }
    #endregion

    #region Fill item rate
    protected void ddlItemDesc_SelectedIndexChanged(object sender, EventArgs e)
    {
        objCust.ItemId = Convert.ToInt32(ddlItemDesc.SelectedValue);
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        if (ds.Tables[0].Rows.Count == 1)
        {
            txtRate.Text = Convert.ToString(ds.Tables[0].Rows[0]["SaleRate"]);
            //txtSt.Text = Convert.ToString(ds.Tables[0].Rows[0]["SalesTax"]);
        }
        else
        {
            txtRate.Text = "";
            //txtSt.Text = "";
        }

        txtQuantity.Focus();
    }
    #endregion

    #region Cartage

    //protected void txtDiscount_TextChanged(object sender, EventArgs e)
    //{
    //    if (txtDiscount.Text == "")
    //    {
    //        txtDiscount.Text = "0.00";
    //    }
    //    lblBillAmount.Text = Convert.ToString(String.Format("{0:C}", (Convert.ToDecimal(txtTotalGrossAmount.Text) + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

    //}

    //protected void txtCartagecharges_TextChanged(object sender, EventArgs e)
    //{
    //    if (txtCartagecharges.Text == "")
    //    {
    //        txtCartagecharges.Text = "0.00";
    //    }
    //    lblBillAmount.Text = Convert.ToString(String.Format("{0:C}", (Convert.ToDecimal(txtTotalGrossAmount.Text) + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

    //}
    #endregion

    #region Delete Invoice
    protected void btnDeleteInvoice_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(hdnInvoiceSummaryId.Value) > 0)
        {
            int result = 0;
            objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(hdnInvoiceSummaryId.Value);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.DataFrom = "SALERETINV";
            result = objSalesInvoice.deleteAllInvoiceData();
            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record deleted successfully.');location.href='BoutiqueSalesReturnsInvoice.aspx';", true);
            }
        }

    }
    #endregion

    protected void btnPrintBill_Click(object sender, EventArgs e)
    {
        string urllast = "PrintSalesReturnInvoiceBill.aspx?InvoiceSummaryId=" + Convert.ToString(hdnInvoiceSummaryId.Value);
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);

    }

    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }

            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
            {
                btnDeleteInvoice.Visible = false;
            }
            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

                btnPrintBill.Visible = false;
            }

            if (Convert.ToString(dt.Rows[0]["IsAdd"]) != "True")
            {

                lblbutton.Visible = false;
                if (Convert.ToString(Request["addnew"]) == "1")
                {

                    ImageButton2.Visible = false;
                }
            }



            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {
                if (Convert.ToString(Request["addnew"]) != "1")
                {

                    ImageButton2.Visible = false;
                }
            }

        }
    }
    protected void lblbutton_Click(object sender, EventArgs e)
    {
        Response.Redirect("BoutiqueSalesReturnsInvoice.aspx?addnew=1");
    }

    #region Fill Accounts

    protected void FillAllLedgerAc()
    {

        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtLedger = new DataTable();
        dtLedger = objSalesInvoice.getAcFileAllLedger();
        if (dtLedger.Rows.Count > 0)
        {

            ddlDR.DataSource = dtLedger;
            ddlDR.DataTextField = "Title";
            ddlDR.DataValueField = "Id";
            ddlDR.DataBind();
            ddlDR.Items.Insert(0, new ListItem("Select", "0"));

            ddlCR.DataSource = dtLedger;
            ddlCR.DataTextField = "Title";
            ddlCR.DataValueField = "Id";
            ddlCR.DataBind();
            ddlCR.Items.Insert(0, new ListItem("Select", "0"));



        }



    }
    #endregion
}