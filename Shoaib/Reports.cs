﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Reports
/// </summary>
public class Reports
{
    #region Data Members
    public string DataFrom, AcFileIdText;
    public int AcFileId, Id, CostCenterCode, MainCategoryCode, SubCategoryCode, itemcode, InvoiceSummaryId, ClientAccountId, ItemCode, storeId, year, Month;
  //  public Nullable<int> ;
    public decimal CustomerCode;
    public DateTime DateFrom,EndDate,AsOn, DateTo, Date, InvDateFrom, invDateTo;
  //  public Nullable<decimal> ;
    int retVal = 0;
    #endregion

	public Reports()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet getLedger()
    {
        DataSet ds1 = new DataSet();
       
       
            string sqlCommand = "proc_getLedger";
           
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@AcFileId", AcFileId);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@DateFrom", DateFrom);
            param[3] = new SqlParameter("@DateTo", DateTo);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);     
            

       
        return ds1;
    }

    public DataSet getLedgerFc()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getLedgerFc";

            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@AcFileId", AcFileId);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@DateFrom", DateFrom);
            param[3] = new SqlParameter("@DateTo", DateTo);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        
        return ds1;
    }

    public DataSet getStockReport()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getStockReport";

            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@itemcode", itemcode);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@DateFrom", DateFrom);
            param[3] = new SqlParameter("@DateTo", DateTo);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


       
        
        return ds1;
    }

    public DataSet getStockReportByCostCenter()
    {
        DataSet ds1 = new DataSet();

         string sqlCommand = "proc_getStockReportByCostCenter";

            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@itemcode", itemcode);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@DateFrom", DateFrom);
            param[3] = new SqlParameter("@DateTo", DateTo);
            param[4] = new SqlParameter("@CostCenterCode", CostCenterCode);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        
        return ds1;
    }

    public DataSet getPeriodicStock()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getPeriodicStock";

            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@DateFrom", DateFrom);
            param[3] = new SqlParameter("@DateTo", DateTo);
            param[4] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        return ds1;
    }


    public DataSet getCurrentStockPosition()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getCurrentStockPosition";

            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@AsOn", AsOn);
            param[3] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        
        return ds1;
    }

    public DataSet getCurrentStockPositionByCostCenter()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getCurrentStockPositionByCostCenter";

            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@AsOn", AsOn);
            param[3] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
            param[4] = new SqlParameter("@CostCenterCode", CostCenterCode);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


       
        return ds1;
    }

    public DataSet GetMainCategoryPeriodicStock()
    {
        DataSet ds1 = new DataSet();

           string sqlCommand = "GetMainCategoryPeriodicStock";

            SqlParameter[] param = new SqlParameter[3];          
            param[0] = new SqlParameter("@CustomerCode", CustomerCode);
            param[1] = new SqlParameter("@DateFrom", DateFrom);
            param[2] = new SqlParameter("@DateTo", DateTo);
           
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        
        return ds1;
    }

    public DataSet GetBoutiqueMainCategoryPeriodicStock()
    {
        DataSet ds1 = new DataSet();

        
       
            string sqlCommand = "GetBoutiqueMainCategoryPeriodicStock";

            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@CustomerCode", CustomerCode);
            param[1] = new SqlParameter("@DateFrom", DateFrom);
            param[2] = new SqlParameter("@DateTo", DateTo);
            param[3] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
            
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        
        return ds1;
    }
    public DataSet GetSubCategoryPeriodicStock()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "GetSubCategoryPeriodicStock";
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@CustomerCode", CustomerCode);
            param[1] = new SqlParameter("@DateFrom", DateFrom);
            param[2] = new SqlParameter("@DateTo", DateTo);
            param[3] = new SqlParameter("@MainCategoryCode", MainCategoryCode);

            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        
        return ds1;
    }

    public DataSet Getcashbankbookreport()
    {
        DataSet ds1 = new DataSet();
       
       
            string sqlCommand = "proc_Getcashbankbookreport";
           
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@AcFileId", AcFileId);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@DateFrom", DateFrom);
            param[3] = new SqlParameter("@DateTo", DateTo);
            param[4] = new SqlParameter("@DataFrom", DataFrom);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);     
            

        
        return ds1;
    }


    public DataSet getAccountsDate()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getAccountsDate";

            SqlParameter[] param = new SqlParameter[4];          
            param[0] = new SqlParameter("@CustomerCode", CustomerCode);
            param[1] = new SqlParameter("@DateFrom", DateFrom);
            param[2] = new SqlParameter("@DateTo", DateTo);
            param[3] = new SqlParameter("@DataFrom", DataFrom);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);


        
        return ds1;
    }

    public DataSet getTransInvoiceSummaryId()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getTransInvoiceSummaryId";

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@CustomerCode", CustomerCode);
            param[1] = new SqlParameter("@Date", Date);          
            param[2] = new SqlParameter("@DataFrom", DataFrom);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        
        return ds1;
    }

    public DataSet getVoucherMainDetail()
    {
        DataSet ds1 = new DataSet();

          string sqlCommand = "proc_getVoucherMainDetail";

            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@CustomerCode", CustomerCode);
            param[1] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);          
            param[2] = new SqlParameter("@DataFrom", DataFrom);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

       
        return ds1;
    }

    public DataSet SearchInvoiceReport()
    {
        DataSet ds1 = new DataSet();

        
           string sqlCommand = "proc_SearchInvoiceReport";           
           SqlParameter[] param = new SqlParameter[7];    
            param[0] = new SqlParameter("@CustomerCode", CustomerCode);
            param[1] = new SqlParameter("@InvDateFrom", InvDateFrom);
            param[2] = new SqlParameter("@invDateTo", invDateTo);
            param[3] = new SqlParameter("@DataFrom", DataFrom);
            param[4] = new SqlParameter("@ClientAccountId", ClientAccountId);
            param[5] = new SqlParameter("@ItemCode", ItemCode);
            param[6] = new SqlParameter("@storeId", storeId);
           ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds1;
    }
    public DataSet SearchBoutiqueInvoiceReport()
    {
        DataSet ds1 = new DataSet();


        string sqlCommand = "proc_SearchBoutiqueInvoiceReport";
        SqlParameter[] param = new SqlParameter[8];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@InvDateFrom", InvDateFrom);
        param[2] = new SqlParameter("@invDateTo", invDateTo);
        param[3] = new SqlParameter("@DataFrom", DataFrom);
        param[4] = new SqlParameter("@ClientAccountId", ClientAccountId);
        param[5] = new SqlParameter("@ItemCode", ItemCode);
        param[6] = new SqlParameter("@storeId", storeId);
        param[7] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds1;
    }



    public DataSet SearchPOSInvoiceReport()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_SearchPOSInvoiceReport";           
           SqlParameter[] param = new SqlParameter[8];    
            param[0] = new SqlParameter("@CustomerCode", CustomerCode);
            param[1] = new SqlParameter("@InvDateFrom", InvDateFrom);
            param[2] = new SqlParameter("@invDateTo", invDateTo);
            param[3] = new SqlParameter("@DataFrom", DataFrom);           
            param[4] = new SqlParameter("@ItemCode", ItemCode);

            param[6] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
            param[7] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
           ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds1;
    }


    


    public DataSet getInvoiceMainDetail()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getInvoiceMainDetail";
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@CustomerCode", CustomerCode);          
            param[1] = new SqlParameter("@DataFrom", DataFrom);
            param[2] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
            param[3] = new SqlParameter("@ItemCode", ItemCode);
            param[4] = new SqlParameter("@storeId", storeId);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
       
        return ds1;
    }
    public DataSet getInvoiceMainDetailBoutique()
    {
        DataSet ds1 = new DataSet();


        string sqlCommand = "proc_getInvoiceMainDetailBoutique";
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@DataFrom", DataFrom);
        param[2] = new SqlParameter("@InvoiceSummaryId", InvoiceSummaryId);
        param[3] = new SqlParameter("@ItemCode", ItemCode);
        param[4] = new SqlParameter("@storeId", storeId);
        param[5] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds1;
    }

    public DataSet getInvoiceBoutiquePaymentSheet()
    {
        DataSet ds1 = new DataSet();


        string sqlCommand = "proc_getInvoiceBoutiquePaymentSheet";
        SqlParameter[] param = new SqlParameter[6];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@DataFrom", DataFrom);       
        param[2] = new SqlParameter("@ItemCode", ItemCode);      
        param[3] = new SqlParameter("@MainCategoryCode", MainCategoryCode);
        ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds1;
    }
    


    #region Yearly Sales
    public DataSet getYearlySales()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getYearlySales";
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@year", year);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@ClientAccountId", ClientAccountId);
            param[3] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds1;
    }

    public DataSet getYearlySalesAmountWise()
    {
        DataSet ds1 = new DataSet();

      
            string sqlCommand = "proc_getYearlySalesAmountWise";
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@year", year);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@ClientAccountId", ClientAccountId);
            param[3] = new SqlParameter("@SubCategoryCode", SubCategoryCode);

            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds1;
    }


    public DataSet getMainCategoryYearlySales()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getMainCategoryYearlySales";
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@year", year);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@ClientAccountId", ClientAccountId);

            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
       
        return ds1;
    }


    public DataSet getSubCategoryYearlySales()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getSubCategoryYearlySales";
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@year", year);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@ClientAccountId", ClientAccountId);
            param[3] = new SqlParameter("@MainCategoryCode", MainCategoryCode);

            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);

        return ds1;
    }
    public DataSet getYearlySalesByClient()
    {
        DataSet ds1 = new DataSet();

          string sqlCommand = "proc_getYearlySalesByClient";
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@year", year);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@ItemCode", ItemCode);

            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds1;
    }
    public DataSet getYearlyQtySalesByClient()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getYearlyQtySalesByClient";
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@year", year);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@ItemCode", ItemCode);

            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds1;
    }
    #endregion
    #region Yearly Purchase
    public DataSet getYearlyPurchase()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getYearlyPurchase";
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@year", year);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@ClientAccountId", ClientAccountId);
            param[3] = new SqlParameter("@SubCategoryCode", SubCategoryCode);
            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds1;
    }

    public DataSet getYearlyPurchaseAmountWise()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getYearlyPurchaseAmountWise";
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@year", year);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@ClientAccountId", ClientAccountId);
            param[3] = new SqlParameter("@SubCategoryCode", SubCategoryCode);

            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds1;
    }


    public DataSet getMainCategoryYearlyPurchase()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getMainCategoryYearlyPurchase";
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@year", year);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@ClientAccountId", ClientAccountId);

            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds1;
    }


    public DataSet getSubCategoryYearlyPurchase()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getSubCategoryYearlyPurchase";
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@year", year);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@ClientAccountId", ClientAccountId);
            param[3] = new SqlParameter("@MainCategoryCode", MainCategoryCode);

            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds1;
    }
    public DataSet getYearlyPurchaseByClient()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getYearlyPurchaseByClient";
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@year", year);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@ItemCode", ItemCode);

            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds1;
    }


    


    public DataSet getYearlyQtyPurchaseByClient()
    {
        DataSet ds1 = new DataSet();

        
            string sqlCommand = "proc_getYearlyQtyPurchaseByClient";
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@year", year);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@ItemCode", ItemCode);

            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds1;
    }

    public DataSet getYearlyExpenses()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getYearlyExpenses";
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@year", year);
            param[1] = new SqlParameter("@CustomerCode", CustomerCode);
            param[2] = new SqlParameter("@AcFileId", AcFileId);

            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds1;
    }
    #endregion

    public DataSet getAcfileGroup()
    {
        DataSet ds1 = new DataSet();

       
            string sqlCommand = "proc_getAcfileGroup";
            SqlParameter[] param = new SqlParameter[2];

            param[0] = new SqlParameter("@CustomerCode", CustomerCode);
            param[1] = new SqlParameter("@Id", Id);
         
         

            ds1 = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        
        return ds1;
    }



    public DataTable getAcFileGroupBySundryDebtor()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getAcFileGroupBySundryDebtor";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }

    public DataTable getAgingReport()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getAgingReport";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@EndDate", EndDate);
        param[2] = new SqlParameter("@AcFileId", AcFileId);
       
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
    public DataTable getSalesVSsRecovery()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getSalesVSsRecovery";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@year", year);
        param[2] = new SqlParameter("@AcFileId", AcFileId);
       
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }


    public DataTable getPurchaseVSPayment()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getPurchaseVSPayment";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@year", year);
        param[2] = new SqlParameter("@AcFileId", AcFileId);

        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }



    public DataTable getChildLedger()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "getChildLedger";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@AcFileId", AcFileId);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }


    public DataTable getSalesVSsRecoveryYearly()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getSalesVSsRecoveryYearly";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@AcFileIdText", @AcFileIdText);
        param[3] = new SqlParameter("@year", year);
        param[4] = new SqlParameter("@Month", Month);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }


    public DataTable getPurchaseVSPaymentYearly()
    {
        DataSet ds = new DataSet();
        string sqlCommand = "proc_getPurchaseVSPaymentYearly";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@CustomerCode", CustomerCode);
        param[1] = new SqlParameter("@AcFileIdText", @AcFileIdText);
        param[3] = new SqlParameter("@year", year);
        param[4] = new SqlParameter("@Month", Month);
        ds = DataTier.SqlHelper.ExecuteDataset(CommandType.StoredProcedure, sqlCommand, param);
        return ds.Tables[0];
    }
}
