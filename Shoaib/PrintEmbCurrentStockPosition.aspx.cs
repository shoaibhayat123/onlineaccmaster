﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintEmbCurrentStockPosition : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    clsCookie ck = new clsCookie();
    decimal TotalOpbal = 0;
    decimal TotalTransections = 0;
    decimal TotalOpeningQuantity = 0;
    decimal TotalPurchaseRate = 0;
    decimal TotalProduction = 0;
    decimal TotalSalesReturn = 0;
    decimal TotalPurchaseRateAdjustment = 0;
    decimal AllTotalStockReceived = 0;
    decimal TotalSales = 0;
    decimal TotalConsumption = 0;
    decimal TotalPurchaseRateReturn = 0;
    decimal TotalSalesAdjustment = 0;
    decimal AllTotalStockSale = 0;
    decimal TotalClosingStock = 0;
    decimal TotalAmount = 0;

    decimal AllTotalOpbal = 0;
    decimal AllTotalTransections = 0;
    decimal AllTotalOpeningQuantity = 0;
    decimal AllTotalPurchaseRate = 0;
    decimal AllTotalProduction = 0;
    decimal AllTotalSalesReturn = 0;
    decimal AllTotalPurchaseRateAdjustment = 0;
    decimal CompleteTotalStockReceived = 0;
    decimal AllTotalSales = 0;
    decimal AllTotalConsumption = 0;
    decimal AllTotalPurchaseRateReturn = 0;
    decimal AllTotalSalesAdjustment = 0;
    decimal CompleteTotalStockSale = 0;
    decimal AllTotalClosingStock = 0;
    decimal AllTotalAmount = 0;
    Decimal TotalWeightedQuantity, RemainingQuantity, TotalWeightedAmount = 0;
    decimal GroupTotalOpeningQuantity = 0;
    decimal GroupTotalPurchaseRate = 0;
    decimal GroupTotalAmount = 0;
    Reports objReports = new Reports();
    SetCustomers objCust = new SetCustomers();
    DataTable dttbl = new DataTable();
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {
            if (Request["AsOn"].ToString() != "")
            {
                Session["s"] = null;
                bindSupplier();
                FillMainCategory();
                FillLogo();
                lblFromDate.Text = ClsGetDate.FillFromDate(Request["AsOn"].ToString());

                lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
                lblUser.Text = Sessions.UseLoginName.ToString();

                if (AllTotalAmount >= 0)
                {
                    grandTotalAmount.Text = String.Format("{0:C}", AllTotalAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                }
                else
                {
                    grandTotalAmount.Text = String.Format("{0:C}", AllTotalAmount).Replace('$', ' ');
                }

            }
        }
    }

    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }

    }
    #endregion

    #region Grid RowDataBound
    protected void grdMainCategory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnMainCategoryCode = ((HiddenField)e.Row.FindControl("hdnMainCategoryCode"));



            objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objReports.DateFrom = Convert.ToDateTime(Request["AsOn"]);
            objReports.DateTo = Convert.ToDateTime(Request["AsOn"]);
            objReports.MainCategoryCode = Convert.ToInt32(hdnMainCategoryCode.Value);
            DataSet ds = new DataSet();
            ds = objReports.GetSubCategoryPeriodicStock();

            GridView grdSubCategory = (GridView)e.Row.FindControl("grdSubCategory");
            if (ds.Tables[0].Rows.Count > 0)
            {
                GroupTotalOpeningQuantity = 0;
                GroupTotalPurchaseRate = 0;
                GroupTotalAmount = 0;

                grdSubCategory.DataSource = ds.Tables[0];
                grdSubCategory.DataBind();


                if (GroupTotalOpeningQuantity == 0 && Convert.ToString(Request["Type"]) == "2")
                {
                    GridViewRow Parent = (GridViewRow)grdSubCategory.Parent.Parent;
                    Parent.Style.Add("display", "none");
                }
            }

        }


    }

    protected void grdSubCategory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnMainCategoryCode2 = ((HiddenField)e.Row.FindControl("hdnMainCategoryCode2"));
            HiddenField hdnSubCategoryCode = ((HiddenField)e.Row.FindControl("hdnSubCategoryCode"));

            objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objReports.AsOn = Convert.ToDateTime(Request["AsOn"]);
            objReports.MainCategoryCode = Convert.ToInt32(hdnMainCategoryCode2.Value);
            objReports.SubCategoryCode = Convert.ToInt32(hdnSubCategoryCode.Value);

            DataSet ds = new DataSet();
            ds = objReports.getCurrentStockPosition();

            GridView grdLedger = (GridView)e.Row.FindControl("grdLedger");
            if (ds.Tables[0].Rows.Count > 0)
            {
                TotalOpbal = 0;
                TotalTransections = 0;
                TotalOpeningQuantity = 0;
                TotalPurchaseRate = 0;
                TotalProduction = 0;
                TotalSalesReturn = 0;
                TotalPurchaseRateAdjustment = 0;
                AllTotalStockReceived = 0;
                TotalSales = 0;
                TotalConsumption = 0;
                TotalPurchaseRateReturn = 0;
                TotalSalesAdjustment = 0;
                AllTotalStockSale = 0;
                TotalClosingStock = 0;
                TotalAmount = 0;
                grdLedger.DataSource = ds.Tables[0];
                grdLedger.DataBind();

                if (TotalOpeningQuantity == 0 && Convert.ToString(Request["Type"]) == "2")
                {
                    GridViewRow Parent = (GridViewRow)grdLedger.Parent.Parent;
                    Parent.Style.Add("display", "none");
                }
            }

        }

        if (e.Row.RowType == DataControlRowType.Footer)  // For footer totals
        {


        }
    }

    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblOpeningQuantity = ((Label)e.Row.FindControl("lblOpeningQuantity"));
            Label lblPurchaseRate = ((Label)e.Row.FindControl("lblPurchaseRate"));
            Label lblAmount = ((Label)e.Row.FindControl("lblAmount"));



            decimal Opbal = 0;
            decimal Transections = 0;
            decimal OpeningQuantity = 0;
            decimal PurchaseRate = 0;
            decimal Purchase = 0;
            decimal Production = 0;
            decimal SalesReturn = 0;
            decimal PurchaseAdjustment = 0;
            decimal Sales = 0;
            decimal Consumption = 0;
            decimal PurchaseReturn = 0;
            decimal SalesAdjustment = 0;
            decimal Amount = 0;
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Opbal")) != "")
            {
                Opbal = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Opbal"));

            }
            //if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Transections")) != "")
            //{
            //    Transections = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Transections"));

            //}

            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Purchase")) != "")
            {
                Purchase = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Purchase"));

            }
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Production")) != "")
            {
                Production = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Production"));

            }
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SalesReturn")) != "")
            {
                SalesReturn = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "SalesReturn"));

            }
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PurchaseAdjustment")) != "")
            {
                PurchaseAdjustment = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PurchaseAdjustment"));

            }



            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Sales")) != "")
            {
                Sales = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Sales"));

            }
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Consumption")) != "")
            {
                Consumption = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Consumption"));

            }
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PurchaseReturn")) != "")
            {
                PurchaseReturn = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PurchaseReturn"));

            }

            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SalesAdjustment")) != "")
            {
                SalesAdjustment = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "SalesAdjustment"));

            }
            //  OpeningQuantity = Opbal + Purchase + Production + SalesReturn + PurchaseAdjustment + Sales + Consumption + PurchaseReturn + SalesAdjustment;
            OpeningQuantity = Opbal + Purchase + Production + SalesReturn + PurchaseAdjustment + Sales + Consumption + PurchaseReturn + SalesAdjustment;
            TotalOpeningQuantity += OpeningQuantity;
            GroupTotalOpeningQuantity += OpeningQuantity;
            if (OpeningQuantity >= 0)
            {
                lblOpeningQuantity.Text = String.Format("{0:C}", OpeningQuantity).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            }
            else
            {
                lblOpeningQuantity.Text = String.Format("{0:C}", OpeningQuantity).Replace('$', ' ');
            }

            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PurchaseRate")) != "")
            {
                PurchaseRate = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "PurchaseRate"));
                TotalPurchaseRate += PurchaseRate;
                AllTotalPurchaseRate += PurchaseRate;
                GroupTotalPurchaseRate += PurchaseRate;
            }

            /* Calculetion Of weighted average*/
            objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objReports.AsOn = Convert.ToDateTime(Request["AsOn"]);
            objReports.ItemCode = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "ItemId"));            
            DataSet ds = new DataSet();
            ds = objReports.getWeightedAverage();

             TotalWeightedQuantity=0;
             RemainingQuantity = 0;
             TotalWeightedAmount = 0;

             RemainingQuantity = OpeningQuantity;
             if (ds.Tables[0].Rows.Count > 0)
             {
                

                 for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                 {

                     if (RemainingQuantity >= Convert.ToDecimal(ds.Tables[0].Rows[i]["Quantity"]))
                     {
                         TotalWeightedQuantity += Convert.ToDecimal(ds.Tables[0].Rows[i]["Quantity"]);
                         TotalWeightedAmount += (Convert.ToDecimal(ds.Tables[0].Rows[i]["Quantity"]) * Convert.ToDecimal(ds.Tables[0].Rows[i]["Rate"]));
                         RemainingQuantity = OpeningQuantity - Convert.ToDecimal(ds.Tables[0].Rows[i]["Quantity"]);
                     }
                     else
                     {
                         if (RemainingQuantity > 0)
                         {
                             TotalWeightedQuantity += (Convert.ToDecimal(ds.Tables[0].Rows[i]["Quantity"]) - RemainingQuantity);
                             TotalWeightedAmount += (Convert.ToDecimal(ds.Tables[0].Rows[i]["Quantity"]) - RemainingQuantity) * Convert.ToDecimal(ds.Tables[0].Rows[i]["Rate"]);

                         }
                     }
                 }
             }
             else
             {



                 TotalWeightedQuantity = Opbal;
                 TotalWeightedAmount += (Opbal * PurchaseRate);
               


             }
             Decimal AverageRate = 0;
             if (TotalWeightedQuantity > 0 && TotalWeightedAmount > 0)
             {
                  AverageRate = TotalWeightedAmount / TotalWeightedQuantity;
                 
             }
             lblPurchaseRate.Text = String.Format("{0:C}", AverageRate).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            /* End Weighted Average */



            


            //lblPurchaseRate.Text = String.Format("{0:C}", PurchaseRate).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
          //  Amount = PurchaseRate * OpeningQuantity;
            Amount = AverageRate * OpeningQuantity;
            TotalAmount +=   Convert.ToInt32(Amount);
            AllTotalAmount += Convert.ToInt32(Amount);
            GroupTotalAmount += Convert.ToInt32(Amount);

            
            //if (Amount >= 0)
            //{
            //   lblAmount.Text = String.Format("{0:C}", Amount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            //}
            //else
            //{
            //   lblAmount.Text = String.Format("{0:C}", Amount).Replace('$', ' ');
            //}
            if (Amount >= 0)
            {
                lblAmount.Text = Convert.ToInt32(Amount).ToString() + ".00";

            }
            else
            {
                lblAmount.Text = Convert.ToInt32(Amount).ToString() + ".00";
            }

            if (OpeningQuantity == 0 && Convert.ToString(Request["Type"]) == "2")
            {
                e.Row.Visible = false;
            }

        }

        if (e.Row.RowType == DataControlRowType.Footer)  // For footer totals
        {
            Label TotallblOpeningQuantity = ((Label)e.Row.FindControl("TotallblOpeningQuantity"));
            Label TotallblPurchaseRate = ((Label)e.Row.FindControl("TotallblPurchaseRate"));
            Label TotallblAmount = ((Label)e.Row.FindControl("TotallblAmount"));


            if (TotalOpeningQuantity >= 0)
            {
                TotallblOpeningQuantity.Text = Convert.ToDecimal(TotalOpeningQuantity).ToString("0.000");
                    //String.Format("{0:C}", TotalOpeningQuantity).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            }
            else
            {
                TotallblOpeningQuantity.Text =  Convert.ToDecimal(TotalOpeningQuantity).ToString("0.000");
                    //String.Format("{0:C}", TotalOpeningQuantity).Replace('$', ' ');
            }

            TotallblPurchaseRate.Text = String.Format("{0:C}", TotalPurchaseRate).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            if (TotalAmount >= 0)
            {

                TotallblAmount.Text = Convert.ToInt32(TotalAmount).ToString() +".00";
                //String.Format("{0:C}", TotalAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            }
            else
            {

                TotallblAmount.Text = Convert.ToInt32(TotalAmount).ToString() + ".00";
                     //String.Format("{0:C}", TotalAmount).Replace('$', ' ');

            }


        }

    }
    #endregion
    #region Fill Main Category  Setup
    protected void FillMainCategory()
    {

        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.DateFrom = Convert.ToDateTime(Request["AsOn"]);
        objReports.DateTo = Convert.ToDateTime(Request["AsOn"]);
        DataSet ds = new DataSet();
        ds = objReports.GetMainCategoryPeriodicStock();

        if (ds.Tables[0].Rows.Count > 0)
        {


            grdMainCategory.DataSource = ds.Tables[0];
            grdMainCategory.DataBind();




        }


    }
    #endregion
}