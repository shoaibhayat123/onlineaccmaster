﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintLedgerWithOnlyFC : System.Web.UI.Page
{

    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();
    AccFiles objFile = new AccFiles();
    Reports objReports = new Reports();
    decimal TotalBalance = 0;
    decimal TotalDebit = 0;
    decimal TotalCredit = 0;

    decimal TotalBalanceFc = 0;
    decimal TotalDebitFc = 0;
    decimal TotalCreditFc = 0;
    //public string attachment = "attachment; filename=Dresses27_Report_" + DateTime.Now.ToString("dd_MM_yyyy") + ".xls";
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Request["CustomerCode"] != null)
        {
            Sessions.CustomerCode = Request["CustomerCode"].ToString();
        }
        if ((Sessions.CustomerCode == "") || (Request["From"].ToString() == "") || (Request["To"].ToString() == "") || (Request["Client"].ToString() == ""))
        {
            Response.Redirect("default.aspx");
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {


            bindSupplier();
            bindLedger();
            lblFromDate.Text = ClsGetDate.FillFromDate(Request["From"].ToString());
            lblToDate.Text = ClsGetDate.FillFromDate(Request["To"].ToString());
            bindBuyer();
            FillLogo();

            lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
            lblUser.Text = Sessions.UseLoginName.ToString();
            //PrepareGridViewForExport(grdLedger);
            //ExportGridView();


        }
    }

    protected void bindBuyer()
    {
        objFile.id = Convert.ToInt32(Request["Client"].ToString()); ;
        objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtBuyer = new DataTable();
        dtBuyer = objFile.getMenueDetail();


        if (dtBuyer.Rows.Count > 0)
        {
            lblBuyerName.Text = Convert.ToString(dtBuyer.Rows[0]["Title"]);
            lblBuyerAddress.Text = Convert.ToString(dtBuyer.Rows[0]["Address1"]);
            if (Convert.ToString(dtBuyer.Rows[0]["Address2"]) != "")
            {
                lblBuyerAddress2.Text = "," + Convert.ToString(dtBuyer.Rows[0]["Address2"]);
            }
            if (Convert.ToString(dtBuyer.Rows[0]["Address3"]) != "")
            {
                lblBuyerAddress3.Text = "," + Convert.ToString(dtBuyer.Rows[0]["Address3"]);
            }
        }


    }


    #region Bind ledger grid
    protected void bindLedger()
    {
        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.DateFrom = Convert.ToDateTime(Request["From"]);
        objReports.DateTo = Convert.ToDateTime(Request["To"]);
        objReports.AcFileId = Convert.ToInt32(Request["Client"]);
        DataSet ds = new DataSet();
        ds = objReports.getLedgerFc();

        /********** Add new row to result for Opening Balance**********/
        DataRow dr = ds.Tables[3].NewRow(); //table 2 for detail trans
        dr["Description"] = "Opening Balance";
        decimal OpeningBal = 0;
        decimal TotalTrans = 0;
        if (Convert.ToString(ds.Tables[0].Rows[0]["OpeningBal"]) != "")
        {
            OpeningBal = Convert.ToDecimal(ds.Tables[0].Rows[0]["OpeningBal"]);
        }
        if (Convert.ToString(ds.Tables[1].Rows[0]["TotalTrans"]) != "")
        {
            TotalTrans = Convert.ToDecimal(ds.Tables[1].Rows[0]["TotalTrans"]);
        }

        decimal OpeningBalAmount = OpeningBal + TotalTrans;
        dr["TransAmt"] = OpeningBalAmount;

        if (Convert.ToString(ds.Tables[2].Rows[0]["TotalTransFc"]) != "")
        {
            dr["FCAmount"] = Convert.ToDecimal(ds.Tables[2].Rows[0]["TotalTransFc"]);
        }
        //else
        //{
        //    dr["TransAmtFc"] = 0.00;
        //}
        ds.Tables[3].Rows.InsertAt(dr, 0);

        grdLedger.DataSource = ds.Tables[3];
        grdLedger.DataBind();


    }

    #endregion
    #region Grid RowDataBound
    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbltransDate = ((Label)e.Row.FindControl("lbltransDate"));
            Label lblChqDate = ((Label)e.Row.FindControl("lblChqDate"));
            Label lblBalance = ((Label)e.Row.FindControl("lblBalance"));
            Label lblDebit = ((Label)e.Row.FindControl("lblDebit"));
            Label lblCredit = ((Label)e.Row.FindControl("lblCredit"));


            Label lblBalanceFc = ((Label)e.Row.FindControl("lblBalanceFc"));
            Label lblDebitFc = ((Label)e.Row.FindControl("lblDebitFc"));
            Label lblCreditFc = ((Label)e.Row.FindControl("lblCreditFc"));


            string transDate = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TransactionDate"));
            string ChqDate = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ChqDate"));
            if (DataBinder.Eval(e.Row.DataItem, "TransactionDate").ToString() != "")
            {
                lbltransDate.Text = ClsGetDate.FillFromDate(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TransactionDate")));
            }
            if (DataBinder.Eval(e.Row.DataItem, "ChqDate").ToString() != "")
            {
                lblChqDate.Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Row.DataItem, "ChqDate").ToString());
            }
            if (DataBinder.Eval(e.Row.DataItem, "TransAmt").ToString() != "")
            {
                decimal TransAmt = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TransAmt"));
                if (e.Row.RowIndex.ToString() != "0")
                {
                    if (TransAmt > 0)
                    {
                        lblDebit.Text = String.Format("{0:C}", TransAmt).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                        lblCredit.Text = "0.00";
                        TotalDebit = TotalDebit + TransAmt;
                    }
                    else
                    {
                        lblCredit.Text = String.Format("{0:C}", TransAmt).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                        lblDebit.Text = "0.00";
                        TotalCredit = TotalCredit + TransAmt;
                    }


                    TotalBalance = TotalBalance + TransAmt;

                }
                else
                {
                    TotalBalance = TransAmt;


                }
                if (TotalBalance > 0) // For balance amounts
                {
                    lblBalance.Text = String.Format("{0:C}", TotalBalance).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') + " Dr.";
                }
                else
                {
                    lblBalance.Text = String.Format("{0:C}", TotalBalance).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') + " Cr.";
                }


            }




            if (DataBinder.Eval(e.Row.DataItem, "FCAmount").ToString() != "")
            {
                decimal TransAmtFc = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "FCAmount"));
                if (e.Row.RowIndex.ToString() != "0")
                {
                    if (TransAmtFc > 0)
                    {
                        lblDebitFc.Text = String.Format("{0:C}", TransAmtFc).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                        lblCreditFc.Text = "0.00";
                        TotalDebitFc = TotalDebitFc + TransAmtFc;
                    }
                    else
                    {
                        lblCreditFc.Text = String.Format("{0:C}", TransAmtFc).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                        lblDebitFc.Text = "0.00";
                        TotalCreditFc = TotalCreditFc + TransAmtFc;
                    }


                    TotalBalanceFc = TotalBalanceFc + TransAmtFc;

                }
                else
                {
                    TotalBalanceFc = TransAmtFc;


                }
                if (TotalBalanceFc > 0) // For balance amounts
                {
                    lblBalanceFc.Text = String.Format("{0:C}", TotalBalanceFc).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') + " Dr.";
                }
                else
                {
                    lblBalanceFc.Text = String.Format("{0:C}", TotalBalanceFc).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') + " Cr.";
                }


            }
















        }

        if (e.Row.RowType == DataControlRowType.Footer)  // For footer totals
        {

            Label lblTotalDebit = (Label)e.Row.FindControl("lblTotalDebit");
            lblTotalDebit.Text = String.Format("{0:C}", TotalDebit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblTotalCredit = (Label)e.Row.FindControl("lblTotalCredit");
            lblTotalCredit.Text = String.Format("{0:C}", TotalCredit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblTotalDebitFc = (Label)e.Row.FindControl("lblTotalDebitFc");
            lblTotalDebitFc.Text = String.Format("{0:C}", TotalDebitFc).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblTotalCreditFc = (Label)e.Row.FindControl("lblTotalCreditFc");
            lblTotalCreditFc.Text = String.Format("{0:C}", TotalCreditFc).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblTotalTransection = (Label)e.Row.FindControl("lblTotalTransection");
            lblTotalTransection.Text = "=" + ((grdLedger.Rows.Count) - 1).ToString() + "=";
        }

    }
    #endregion

    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "http://online.accmaster.com/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }

        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }
    }
    #endregion
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
}