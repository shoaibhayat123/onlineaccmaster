﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintSalesInvoice.aspx.cs"
    Inherits="PrintSalesInvoice" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
</head>
<body>
    <div>
        <table id="print_Grid" width="800px" style="padding: 10px; font-size: 15px; border: solid 1px black">
            <tr>
                <td style="text-align: center; width: 100%; text-decoration: underline">
                    <b style="font-size: 30px;">Sales Invoice</b>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <b>Invoice No :</b>
                            </td>
                            <td style="text-align: left; padding-left: 20px">
                                <asp:Label ID="lblInvoiceNo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Date :</b>
                            </td>
                            <td style="text-align: left; padding-left: 20px">
                                <asp:Label ID="lblDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td style="width: 600px; text-align: right">
                                <b style="padding-right: 10px;">Payment Term :</b>
                                <asp:Label ID="lblCrdays" runat="server"></asp:Label>
                                Days
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="1" style="width: 100%; border-collapse: collapse">
                        <tr>
                            <td style="width: 50%">
                                <b>Supplier Name&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblSuplierName" runat="server"></asp:Label></b>
                            </td>
                            <td style="width: 50%">
                                <b>Buyer Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblBuyerName" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table>
                                    <tr>
                                        <td style="width: 105px; vertical-align: top">
                                            <b>Address</b>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                                            <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                                            <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Telephone No.</b>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSupplierTelephone" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>ST Reg. No.</b>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSellerRegNo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>NTN No.</b>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblSellerNTN" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td style="width: 105px; vertical-align: top">
                                            <b>Address</b>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBuyerAddress" runat="server"></asp:Label><br />
                                            <asp:Label ID="lblBuyerAddress2" runat="server"></asp:Label><br />
                                            <asp:Label ID="lblBuyerAddress3" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Telephone No.</b>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBuyerTelephone" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>ST Reg. No.</b>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBuyerRegNo" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>NTN No.</b>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblBuyerNTN" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="1" style="width: 100%; border-collapse: collapse;">
                        <tr>
                            <td style="width: 10%; text-align: center; border-bottom: 3px solid black">
                                Quantity
                            </td>
                            <td style="width: 30%; text-align: center; border-bottom: 3px solid black">
                                Description of goods
                            </td>
                            <td style="width: 5%; text-align: center; border-bottom: 3px solid black">
                                Rate
                            </td>
                            <td style="width: 12%; text-align: center; border-bottom: 3px solid black">
                                Value
                            </td>
                            <%--  <td style="width: 12%; text-align: center; border-bottom: 3px solid black">
                                Rate of Sales Tax
                            </td>
                            <td style="width: 12%; text-align: center; border-bottom: 3px solid black">
                                Amount of Sales Tax as per Column 4
                            </td>
                            <td style="width: 12%; border-bottom: 3px solid black">
                                Value Including Sales Tax
                            </td>--%>
                        </tr>
                        <asp:Repeater ID="rptrItemDesc" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td style="width: 10%; text-align: right; border-bottom: 1px dotted black">
                                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>&nbsp;<%# Eval("Unit")%>
                                    </td>
                                    <td style="width: 30%; border-bottom: 1px dotted black">
                                        <%# Eval("ItemDesc")%>
                                    </td>
                                    <td style="width: 5%; text-align: right; border-bottom: 1px dotted black">
                                        <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                                    </td>
                                    <td style="width: 12%; text-align: right; border-bottom: 1px dotted black">
                                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                                    </td>
                                    <%-- <td style="width: 12%; text-align: center; border-bottom: 1px dotted black">
                                        <%#String.Format("{0:C}", Eval("SaleTax")).Replace('$', ' ')%>&nbsp;%
                                    </td>
                                    <td style="width: 12%; text-align: right; border-bottom: 1px dotted black">
                                        <%#String.Format("{0:C}", Eval("SaleTaxAmount")).Replace('$', ' ')%>
                                    </td>
                                    <td style="width: 12%; text-align: right; border-bottom: 1px dotted black">
                                        <%#String.Format("{0:C}", Eval("AmountIncludeTaxes")).Replace('$', ' ')%>
                                    </td>--%>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                Totals :: &nbsp;&nbsp;&nbsp;&nbsp; <b>Amount:</b>
                                <asp:Label ID="txtTotalGrossAmount" runat="server" Text="0.00" Style="margin-right: 20px;
                                    padding-left: 5px"></asp:Label>
                                <%--  <b>Tax Amt:</b>
                                <asp:Label ID="txtSTtotalAmount" runat="server" Text="0.00" Style="margin-right: 20px;
                                    padding-left: 5px"></asp:Label>--%>
                                <%--  <b>Net Amt:</b>
                                <asp:Label ID="txtTotalAmountIncludingTaxe" runat="server" Text="0.00" Style="margin-right: 20px;
                                    padding-left: 5px"></asp:Label>--%>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblCurrency" runat="server"></asp:Label>
                    :
                    <asp:Label ID="lblAmount" runat="server"></asp:Label>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td style="margin-top: 20px; width: 500px;">
                    <div style="float: left">
                        <b>Received By :</b></div>
                    <div style="float: left; margin-left: 20px; height: 20px; padding-top: 10px; width: 200px;">
                        <hr />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%">
                        <tr>
                            <td style="margin-top: 20px; width: 50%;">
                                <div style="float: left">
                                    <b>Dated :</b></div>
                                <div style="float: left; margin-left: 63px; padding-top: 10px; height: 20px; width: 200px;">
                                    <hr />
                                </div>
                            </td>
                            <td style="margin-top: 20px; text-align: right">
                                <div style="float: right; margin-left: 20px; padding-top: 10px; height: 20px; line-height: 3px;
                                    text-align: center; width: 200px;">
                                    <hr />
                                    <b>Signature</b>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <hr />
                </td>
            </tr>
            <tr>
                <td>
                    i- Only Return claims would be entertained within 7 days of the receipt of the delivery.<br />
                    ii- Losses arising out of thefts, accident to conveyance damages/leakages due to
                    improper handling shall not be entertained.<br />
                    iii- Deduction of amount of this invoice is not allowed. In case of any difference
                    regarding price, the buyer must notify in writing &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;within
                    5 days of receipt of invoice.
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
