		var ARRcookies=document.cookie.split(";");
		var triggerSelect = "essp"
		for (i=0;i<ARRcookies.length;i++){
		  x = ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
		  y = ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
		  x = x.replace(/^\s+|\s+$/g,"");			
		  if(x == "navigation_param") {
		  	var param_value=y;
		  	if(param_value == "qboSelect_ss") triggerSelect = "ss";
		  	else if(param_value == "qboSelect_es") triggerSelect = "es";
		  	else if(param_value == "qboSelect_op") triggerSelect = "op";
		  }
		}	
		
		$(document).ready(function(){
			//$.scrollTo('0px');
			var $panels = $('#sign-up-free-5sku-main-content-top-cap #slider .scroll .scrollContainer .panel .panelItem');
			
			var $container = $('#sign-up-free-5sku-main-content-top-cap #slider .scroll .scrollContainer');
			
			//alert($container[0].id);
			
			// if false, we'll float all the panels left and fix the width 
			// of the container
			var horizontal = true;
			
			// float the panels left if we're going horizontal
			if (horizontal) {
			  $panels.css({
			    'float' : 'left',
			    'position' : 'relative' // IE fix to ensure overflow is hidden
			  });
			   
			  // calculate a new width for the container (so it holds all panels)
			  //$container.css('width', $panels[0].offsetWidth * $panels.length); 
			  //alert($panels[0].offsetWidth * $panels.length);
			}
			
			// collect the scroll object, at the same time apply the hidden overflow
			// to remove the default scrollbars that will appear
			var $scroll = $('#sign-up-free-5sku-main-content-top-cap #slider .scroll').css('overflow', 'hidden');
		
			//alert($scroll[0].id);
			
			// handle nav selection
			function selectNav() {
				if(this.id =='sign-up-free-5sku-navigation-opp'){}
					
					
			}
			
			//$('.slideTrigger').hover(selectNav);
			
			// go find the navigation link that has this target and select the nav
			function trigger(data) { 
			  var el = $('#sign-up-free-5sku-main-content-middle .navigation').find('a[href$="' + data.id + '"]').get(0);
			  selectNav.call(el);
			}
			
			if (window.location.hash) { 
			  trigger({ id : window.location.hash.substr(1) });
			  //alert(id);
			} else { 
			  $("#sign-up-free-5sku-navigation-essp").trigger('mouseover');
			}
			
			// offset is used to move to *exactly* the right place, since I'm using
			// padding on my example, I need to subtract the amount of padding to
			// the offset.  Try removing this to get a good idea of the effect
			var offset = parseInt((horizontal ? $container.css('paddingTop') :  $container.css('paddingLeft')) || 0) * -1;
			
			
			var scrollOptions = {
			  target: $scroll, // the element that has the overflow
			  
			  // can be a selector which will be relative to the target
			  items: $panels,
			  
			  navigation: '.navagation a',
			  
			  // allow the scroll effect to run both directions
			  axis: 'xy',
			  
			  onAfter: trigger, // our final callback
			  
			  offset: offset,
			  
			  // duration of the sliding effect
			  duration: 500,
			  
			  event: 'mouseover',
			  
			  // easing - can be used with the easing plugin: 
			  // http://gsgd.co.uk/sandbox/jquery/easing/
			  easing: 'swing'
			};
			
			
			// apply serialScroll to the slider - we chose this plugin because it 
			// supports// the indexed next and previous scroll along with hooking 
			// in to our navigation.
			$('#slider').serialScroll(scrollOptions);
			
			// now apply localScroll to hook any other arbitrary links to trigger 
			// the effect
			$.localScroll(scrollOptions);
			
			// finally, if the URL has a hash, move the slider in to position, 
			// setting the duration to 1 because I don't want it to scroll in the
			// very first page load.  We don't always need this, but it ensures
			// the positioning is absolutely spot on when the pages loads.
			//scrollOptions.duration = 1;
			//$.localScroll.hash(scrollOptions);
			
			$("#offer_details").show();
			
			function turnOffSS(){
				$("#sign-up-free-5sku-navigation-ss").removeClass('sign-up-free-5sku-navigation-ss-hover');
					$("#sign-up-free-5sku-navigation-ss-title").show();
					$("#sign-up-free-5sku-navigation-ss-box").removeClass('sign-up-free-5sku-navigation-ss-box-hover');
					$("#sign-up-free-5sku-navigation-ss-trial").removeClass('sign-up-free-5sku-navigation-ss-trial-hover');
					$("#sign-up-free-5sku-navigation-ss-box-title").removeClass('sign-up-free-5sku-navigation-ss-box-title-hover');
					$("#sign-up-free-5sku-try-it-button-sep-ss").addClass('sign-up-free-5sku-try-it-button-ss');
					$("#sign-up-free-5sku-try-it-button-sep-ss").removeClass('sign-up-free-5sku-try-it-button-ss-hover');
			}
			
			function turnOffEss(){
				$("#sign-up-free-5sku-navigation-ess-title").show();
				$("#sign-up-free-5sku-navigation-ess").removeClass('sign-up-free-5sku-navigation-ess-hover');
				$("#sign-up-free-5sku-navigation-ess-box").removeClass('sign-up-free-5sku-navigation-ess-box-hover');
				$("#sign-up-free-5sku-navigation-ess-trial").removeClass('sign-up-free-5sku-navigation-ess-trial-hover');
				$("#sign-up-free-5sku-navigation-ess-trial").addClass('sign-up-free-5sku-navigation-ess-trial');
				$("#sign-up-free-5sku-navigation-ess-box-title").removeClass('sign-up-free-5sku-navigation-ess-box-title-hover');
				$("#sign-up-free-5sku-try-it-button-sep-ess").removeClass('sign-up-free-5sku-try-it-button-ess-hover');
				$("#sign-up-free-5sku-try-it-button-sep-ess").addClass('sign-up-free-5sku-try-it-button-sep-ess');
				$("#sign-up-free-5sku-navigation-ess-paragraph").removeClass('sign-up-free-5sku-navigation-ess-paragraph-hover');
				$("#sign-up-free-5sku-navigation-ess-paragraph").addClass('sign-up-free-5sku-navigation-ess-paragraph');
			}
			
			function turnOffOp(){
				$("#sign-up-free-5sku-navigation-op-title").show();
				$("#sign-up-free-5sku-navigation-op").removeClass('sign-up-free-5sku-navigation-op-hover');
				$("#sign-up-free-5sku-navigation-op-box").removeClass('sign-up-free-5sku-navigation-op-box-hover');
				$("#sign-up-free-5sku-navigation-op-box-title").removeClass('sign-up-free-5sku-navigation-op-box-title-hover');
				$("#sign-up-free-5sku-try-it-button-op").removeClass('sign-up-free-5sku-try-it-button-op-hover');
				$("#sign-up-free-5sku-navigation-op-paragraph").removeClass('sign-up-free-5sku-navigation-op-paragraph-hover');
			}
			
			 function turnOffEssp(){
				$("#sign-up-free-5sku-navigation-essp-title").show();
				$("#sign-up-free-5sku-navigation-essp").removeClass('sign-up-free-5sku-navigation-essp-hover');
				$("#best-value-label").removeClass('best-value-label-hover');
				$("#sign-up-free-5sku-navigation-essp-box").removeClass('sign-up-free-5sku-navigation-essp-box-hover');
				$("#sign-up-free-5sku-navigation-essp-box-title").removeClass('sign-up-free-5sku-navigation-essp-box-title-hover');
				$("#sign-up-free-5sku-navigation-essp-trial").removeClass('sign-up-free-5sku-navigation-essp-trial-hover');
				$("#sign-up-free-5sku-try-it-button-essp").removeClass('sign-up-free-5sku-try-it-button-essp-hover');
				$("#sign-up-free-5sku-navigation-essp-paragraph").removeClass('sign-up-free-5sku-navigation-essp-paragraph-hover');
			}
			
			function turnOffOpp(){
				$("#sign-up-free-5sku-navigation-opp-title").show();
				$("#sign-up-free-5sku-navigation-opp").removeClass('sign-up-free-5sku-navigation-opp-hover');
				$("#sign-up-free-5sku-navigation-opp-box").removeClass('sign-up-free-5sku-navigation-opp-box-hover');
				$("#sign-up-free-5sku-navigation-opp-box-title").removeClass('sign-up-free-5sku-navigation-opp-box-title-hover');
				$("#sign-up-free-5sku-navigation-opp-trial").removeClass('sign-up-free-5sku-navigation-opp-trial-hover');
				$("#sign-up-free-5sku-try-it-button-opp").removeClass('sign-up-free-5sku-try-it-button-opp-hover');
				$("#sign-up-free-5sku-navigation-opp-paragraph").removeClass('sign-up-free-5sku-navigation-opp-paragraph-hover');
			}
			
			
			//Set a trigger for switching based on url param
			
			//var triggerSelect = sbweb.util.url.getQueryParam("qboSelect");
			
			if (triggerSelect == "ss") {
				$("#sign-up-free-5sku-navigation-ss").hover(function(){
					$("#sign-up-free-5sku-main-content-interior-box-image-OSS-anchor").trigger('mouseover');
					turnOffEss();
					turnOffOp();
					turnOffEssp();
					turnOffOpp();
					$("#sign-up-free-5sku-navigation-ess").trigger('mouseout');
					$("#sign-up-free-5sku-navigation-ss").addClass('sign-up-free-5sku-navigation-ss-hover');
					$("#sign-up-free-5sku-navigation-ss-title").hide();
					$("#sign-up-free-5sku-navigation-ss-box").addClass('sign-up-free-5sku-navigation-ss-box-hover');
					$("#sign-up-free-5sku-navigation-ss-box-title").addClass('sign-up-free-5sku-navigation-ss-box-title-hover');
					$("#sign-up-free-5sku-navigation-ss-trial").addClass('sign-up-free-5sku-navigation-ss-trial-hover');
					$("#sign-up-free-5sku-try-it-button-sep-ss").removeClass('sign-up-free-5sku-try-it-button-ss');
					$("#sign-up-free-5sku-try-it-button-sep-ss").addClass('sign-up-free-5sku-try-it-button-ss-hover');
				}, function(){
				});
				
				$("#sign-up-free-5sku-navigation-ss").trigger('mouseover');
				$('#sign-up-free-5sku-navigation-ss').unbind();
				
			}else if(triggerSelect == "op"){
				$("#sign-up-free-5sku-navigation-op").hover(function(){
					$("#sign-up-free-5sku-main-content-interior-box-image-OP-anchor").trigger('mouseover');
					turnOffSS();
					turnOffEss();
					turnOffEssp();
					turnOffOpp();
					$("#sign-up-free-5sku-navigation-ess").trigger('mouseout');
					$("#sign-up-free-5sku-navigation-op-title").hide();
					$("#sign-up-free-5sku-navigation-op").addClass('sign-up-free-5sku-navigation-op-hover');
					$("#sign-up-free-5sku-navigation-op-box").addClass('sign-up-free-5sku-navigation-op-box-hover');
					$("#sign-up-free-5sku-navigation-op-box-title").addClass('sign-up-free-5sku-navigation-op-box-title-hover');
					$("#sign-up-free-5sku-try-it-button-op").addClass('sign-up-free-5sku-try-it-button-op-hover');
					$("#sign-up-free-5sku-navigation-op-paragraph").addClass('sign-up-free-5sku-navigation-op-paragraph-hover');
				}, function(){
				});
				
				$("#sign-up-free-5sku-navigation-op").trigger('mouseover');
				$('#sign-up-free-5sku-navigation-op').unbind();
			}else if(triggerSelect == "es"){
				$("#sign-up-free-5sku-navigation-ess").hover(function(){
					$("#sign-up-free-5sku-main-content-interior-box-image-OE-anchor").trigger('mouseover');
					turnOffSS();
					turnOffEss();
					turnOffOp();
					turnOffOpp();
					$("#sign-up-free-5sku-navigation-ess").trigger('mouseout');
					$("#sign-up-free-5sku-navigation-ess-title").hide();
					$("#sign-up-free-5sku-navigation-ess").addClass('sign-up-free-5sku-navigation-ess-hover');
					$("#sign-up-free-5sku-navigation-ess-box").addClass('sign-up-free-5sku-navigation-ess-box-hover');
					$("#sign-up-free-5sku-navigation-ess-box-title").addClass('sign-up-free-5sku-navigation-ess-box-title-hover');
					$("#sign-up-free-5sku-navigation-ess-trial").addClass('sign-up-free-5sku-navigation-ess-trial-hover');
					$("#sign-up-free-5sku-try-it-button-ess").addClass('sign-up-free-5sku-try-it-button-ess-hover');
					$("#sign-up-free-5sku-navigation-ess-paragraph").addClass('sign-up-free-5sku-navigation-ess-paragraph-hover');
				}, function(){
				});
				
				$("#sign-up-free-5sku-navigation-ess").trigger('mouseover');
				$('#sign-up-free-5sku-navigation-ess').unbind();
				
			}else{
				$("#sign-up-free-5sku-navigation-essp").hover(function(){
					$("#sign-up-free-5sku-main-content-interior-box-image-OEP-anchor").trigger('mouseover');
					turnOffSS();
					turnOffEss();
					turnOffOp();
					turnOffOpp();
					$("#sign-up-free-5sku-navigation-ess").trigger('mouseout');
					$("#sign-up-free-5sku-navigation-essp-title").hide();
					$("#sign-up-free-5sku-navigation-essp").addClass('sign-up-free-5sku-navigation-essp-hover');
					$("#sign-up-free-5sku-navigation-essp-box").addClass('sign-up-free-5sku-navigation-essp-box-hover');
					$("#sign-up-free-5sku-navigation-essp-box-title").addClass('sign-up-free-5sku-navigation-essp-box-title-hover');
					$("#sign-up-free-5sku-navigation-essp-trial").addClass('sign-up-free-5sku-navigation-essp-trial-hover');
					$("#sign-up-free-5sku-try-it-button-essp").addClass('sign-up-free-5sku-try-it-button-essp-hover');
					$("#sign-up-free-5sku-navigation-essp-paragraph").addClass('sign-up-free-5sku-navigation-essp-paragraph-hover');
				}, function(){
				});
				
				$("#sign-up-free-5sku-navigation-essp").trigger('mouseover');
				$('#sign-up-free-5sku-navigation-essp').unbind();
			}
			
				$("#sign-up-free-5sku-navigation-ss").hoverIntent(function(){
					$("#sign-up-free-5sku-main-content-interior-box-image-OSS-anchor").trigger('mouseover');
					turnOffEss();
					turnOffOp();
					turnOffEssp();
					turnOffOpp();
					$("#sign-up-free-5sku-navigation-ess").trigger('mouseout');
					$("#sign-up-free-5sku-navigation-ss").addClass('sign-up-free-5sku-navigation-ss-hover');
					$("#sign-up-free-5sku-navigation-ss-title").hide();
					$("#sign-up-free-5sku-navigation-ss-box").addClass('sign-up-free-5sku-navigation-ss-box-hover');
					$("#sign-up-free-5sku-navigation-ss-box-title").addClass('sign-up-free-5sku-navigation-ss-box-title-hover');
					$("#sign-up-free-5sku-navigation-ss-trial").addClass('sign-up-free-5sku-navigation-ss-trial-hover');
					$("#sign-up-free-5sku-try-it-button-sep-ss").removeClass('sign-up-free-5sku-try-it-button-ss');
					$("#sign-up-free-5sku-try-it-button-sep-ss").addClass('sign-up-free-5sku-try-it-button-ss-hover');
				}, function(){
				});
				
				
				$("#sign-up-free-5sku-navigation-ess").hoverIntent(function(){
					$("#sign-up-free-5sku-main-content-interior-box-image-OE-anchor").trigger('mouseover');
					turnOffSS();
					turnOffOp();
					turnOffEssp();
					turnOffOpp();
					
					$("#sign-up-free-5sku-navigation-ess-title").hide();
					$("#sign-up-free-5sku-navigation-ess").addClass('sign-up-free-5sku-navigation-ess-hover');
					$("#sign-up-free-5sku-navigation-ess-box").addClass('sign-up-free-5sku-navigation-ess-box-hover');
					$("#sign-up-free-5sku-navigation-ess-box-title").addClass('sign-up-free-5sku-navigation-ess-box-title-hover');
					$("#sign-up-free-5sku-navigation-ess-trial").removeClass('sign-up-free-5sku-navigation-ess-trial');
					$("#sign-up-free-5sku-navigation-ess-trial").addClass('sign-up-free-5sku-navigation-ess-trial-hover');
					$("#sign-up-free-5sku-try-it-button-sep-ess").removeClass('sign-up-free-5sku-try-it-button-sep-ess');
					$("#sign-up-free-5sku-try-it-button-sep-ess").addClass('sign-up-free-5sku-try-it-button-ess-hover');
					$("#sign-up-free-5sku-navigation-ess-paragraph").removeClass('sign-up-free-5sku-navigation-ess-paragraph');
					$("#sign-up-free-5sku-navigation-ess-paragraph").addClass('sign-up-free-5sku-navigation-ess-paragraph-hover');
				}, function(){
				});
				
				
				$("#sign-up-free-5sku-navigation-op").hoverIntent(function(){
					$("#sign-up-free-5sku-main-content-interior-box-image-OP-anchor").trigger('mouseover');
					turnOffSS();
					turnOffEss();
					turnOffEssp();
					turnOffOpp();
					$("#sign-up-free-5sku-navigation-ess").trigger('mouseout');
					$("#sign-up-free-5sku-navigation-op-title").hide();
					$("#sign-up-free-5sku-navigation-op").addClass('sign-up-free-5sku-navigation-op-hover');
					$("#sign-up-free-5sku-navigation-op-box").addClass('sign-up-free-5sku-navigation-op-box-hover');
					$("#sign-up-free-5sku-navigation-op-box-title").addClass('sign-up-free-5sku-navigation-op-box-title-hover');
					$("#sign-up-free-5sku-try-it-button-op").addClass('sign-up-free-5sku-try-it-button-op-hover');
					$("#sign-up-free-5sku-navigation-op-paragraph").addClass('sign-up-free-5sku-navigation-op-paragraph-hover');
				}, function(){
				});
				
				$("#sign-up-free-5sku-navigation-opp").hoverIntent(function(){
					$("#sign-up-free-5sku-main-content-interior-box-image-OPP-anchor").trigger('mouseover');
					turnOffSS();
					turnOffEss();
					turnOffOp();
					turnOffEssp();
					$("#sign-up-free-5sku-navigation-ess").trigger('mouseout');
					$("#sign-up-free-5sku-navigation-opp-title").hide();
					$("#sign-up-free-5sku-navigation-opp").addClass('sign-up-free-5sku-navigation-opp-hover');
					$("#sign-up-free-5sku-navigation-opp-box").addClass('sign-up-free-5sku-navigation-opp-box-hover');
					$("#sign-up-free-5sku-navigation-opp-box-title").addClass('sign-up-free-5sku-navigation-opp-box-title-hover');
					$("#sign-up-free-5sku-navigation-opp-trial").addClass('sign-up-free-5sku-navigation-opp-trial-hover');
					$("#sign-up-free-5sku-try-it-button-opp").addClass('sign-up-free-5sku-try-it-button-opp-hover');
					$("#sign-up-free-5sku-navigation-opp-paragraph").addClass('sign-up-free-5sku-navigation-opp-paragraph-hover');
				}, function(){
				});
				
				$("#sign-up-free-5sku-navigation-essp").hoverIntent(function(){
					$("#sign-up-free-5sku-main-content-interior-box-image-OEP-anchor").trigger('mouseover');
					turnOffSS();
					turnOffEss();
					turnOffOp();
					turnOffOpp();
					$("#sign-up-free-5sku-navigation-ess").trigger('mouseout');
					$("#sign-up-free-5sku-navigation-essp-title").hide();
					$("#sign-up-free-5sku-navigation-essp").addClass('sign-up-free-5sku-navigation-essp-hover');
					$("#best-value-label").addClass('best-value-label-hover');
					$("#sign-up-free-5sku-navigation-essp-box").addClass('sign-up-free-5sku-navigation-essp-box-hover');
					$("#sign-up-free-5sku-navigation-essp-box-title").addClass('sign-up-free-5sku-navigation-essp-box-title-hover');
					$("#sign-up-free-5sku-navigation-essp-trial").addClass('sign-up-free-5sku-navigation-essp-trial-hover');
					$("#sign-up-free-5sku-try-it-button-essp").addClass('sign-up-free-5sku-try-it-button-essp-hover');
					$("#sign-up-free-5sku-navigation-essp-paragraph").addClass('sign-up-free-5sku-navigation-essp-paragraph-hover');
				}, function(){
				});
			
	});