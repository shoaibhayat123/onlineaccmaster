﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainMaster.master" AutoEventWireup="true" CodeFile="BoutiqueLogin.aspx.cs" Inherits="BoutiqueLogin" %>

<asp:Content ContentPlaceHolderID="MainHome" ID="Content1" runat="server">
    <div id="content_wraper">
        <div id="content_midd">
            <div class="content_midd_in">
                <div class="cont_midd_row" style="padding-bottom: 11px;">
                    <span>Boutique Login</span></div>
                <div class="cont_midd_row2" style="padding-bottom: 2px; padding-top: 3px;">
                    <div class="cont_midd_text">
                        Customer Code</div>
                    <div class="cont_midd_input">
                        <asp:TextBox ID="txtCustomerCode" runat="server" CssClass="input_box1" />
                        <asp:RequiredFieldValidator runat="server" ID="req1" ControlToValidate="txtCustomerCode"
                            ValidationGroup="Login" ErrorMessage="Please enter customer code" Display="None">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="txtCustomerCode"
                            MinimumValue="0" MaximumValue="99999999999999" ValidationGroup="Login" ErrorMessage="Please enter valid customer code"
                            Display="None">*</asp:RangeValidator>
                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="txtCustomerCode"
                            ValidationGroup="Forget" ErrorMessage="Please enter customer code" Display="None">*</asp:RequiredFieldValidator>
                        <asp:RangeValidator runat="server" ID="RangeValidator1" ControlToValidate="txtCustomerCode"
                            MinimumValue="0" MaximumValue="99999999999999" ValidationGroup="Forget" ErrorMessage="Please enter valid customer code"
                            Display="None">*</asp:RangeValidator>
                    </div>
                </div>
                <div class="cont_midd_row2" style="padding-bottom: 2px; padding-top: 3px;">
                    <div class="cont_midd_text">
                        User name</div>
                    <div class="cont_midd_input">
                        <asp:TextBox ID="txtUsername" autocomplete="off" runat="server" CssClass="input_box1" />
                        <asp:RequiredFieldValidator runat="server" ID="req2" ControlToValidate="txtUsername"
                            ValidationGroup="Login" ErrorMessage="Please enter username" Display="None">*</asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="cont_midd_row2" style="padding-bottom: 2px; padding-top: 3px;">
                    <div class="cont_midd_text">
                        Password</div>
                    <div class="cont_midd_input">
                        <asp:TextBox ID="txtPassword" autocomplete="off" runat="server" TextMode="Password"
                            CssClass="input_box1" />
                        <asp:RequiredFieldValidator runat="server" ID="req3" ControlToValidate="txtPassword"
                            ValidationGroup="Login" ErrorMessage="Please enter password" Display="None">*</asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="cont_midd_row3" style="display: none">
                    <span class="forgot">Forgot your Password</span></div>
                <div class="cont_midd_row4">
                    <div class="float_L" style="display: none">
                        <a href="#">
                            <asp:Image runat="server" ID="btnsubmit" ImageUrl="images/right_midd_rep_y.png" Width="75"
                                Height="29" alt="" /></a></div>
                    <div class="float_L padd10">
                        <asp:ImageButton runat="server" ValidationGroup="Login" ID="btnLogin" ImageUrl="images/login_button.png"
                            Width="75" Height="29" alt="" OnClick="btnLogin_Click" />
                    </div>
                    <asp:ValidationSummary runat="server" ID="val1" ValidationGroup="Login" ShowMessageBox="true"
                        ShowSummary="false" />
                </div>
                <div class="cont_midd_row3">
                    <asp:ValidationSummary runat="server" ID="ValidationSummary1" ValidationGroup="Forget"
                        ShowMessageBox="true" ShowSummary="false" />
                  <%--  <span class="forgot">
                      
                        <asp:LinkButton ID="btnForget" runat="server" Text="Forgot your Password" ValidationGroup="Forget"
                            OnClick="btnForget_Click"></asp:LinkButton></span>--%>
                  </div>
                <div class="cont_midd_row3" runat="server" id="lblError" visible="false">
                    <span class="forgot" style="color: red">Invalid Login Details !!</span></div>
            </div>
        </div>
    </div>
    
</asp:Content>

