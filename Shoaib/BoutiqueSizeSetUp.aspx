﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true" CodeFile="BoutiqueSizeSetUp.aspx.cs" Inherits="BoutiqueSizeSetUp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text=" Boutique Size Setup"></asp:Label>
      <script type="text/javascript">
          function askDeleteCostCenter() {
              var agree;
              agree = confirm("Are You sure to delete this Size ?");
              if (agree) {
                  return true;
              }
              return false;

          }
        </script>
            <script type="text/javascript">
                Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);

                function BeginRequestHandler(sender, args) {

                    if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Save") {

                        document.getElementById('<%= btn_save.ClientID %>').value = "Saving...";
                        args.get_postBackElement().disabled = true;
                    }


                }
                function SaveClick() {
                    document.getElementById('<%= hdnButtonText.ClientID %>').value = "Save";
                }

        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
        <asp:HiddenField ID="hdnButtonText" runat="server" />
            <table cellspacing="0" style="float:left" border="0" cellpadding="0" width="100%">
                <tr>
                    <td width="40%">
                        <table border="0" cellpadding="5" cellspacing="5">
                            <tr>
                                <td width="100%" valign="top" style="left: 5; padding-top: 10px;">
                                    <table style="margin-left: 10px;">
                                        <tr>
                                            <td align="left">
                                                Size<span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtSize" onChange="capitalizeMe(this)" TextMode="SingleLine"
                                                    runat="server" MaxLength="100" CssClass="input_box2"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSize"
                                                    Display="None" ErrorMessage="Please enter Size" SetFocusOnError="True"
                                                    ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <%-- <asp:ImageButton ID="btn_save" runat="server" ImageUrl="~/admin/images/bt-update.gif"
                                    OnClick="btn_save_Click" ValidationGroup="v" />--%>
                                                <asp:HiddenField runat="server" ID="hdnSizeCode" />
                                                <asp:Button ID="btn_save" OnClientClick="SaveClick()"  runat="server" Text="Save" OnClick="btn_save_Click" ValidationGroup="v" />
                                                <asp:Button ID="btn_cancel" runat="server" Text="Cancel" OnClick="btn_cancel_Click"
                                                    ValidationGroup="v" CausesValidation="false" />
                                                <br />
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                                    ShowSummary="False" ValidationGroup="v" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="text-align: left; float: left" width="700px" align="left">
                      <div style="text-align: center; width: 600px; height: 450px; overflow: auto; margin-bottom: 15px;
                            margin-left: 20px; padding-top: 20px">
                            <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="No record found." Visible="false"></asp:Label>
                            <asp:DataGrid ID="grdCustomer" Width="100%" runat="server" AutoGenerateColumns="False"
                                AllowPaging="false" PagerStyle-Visible="false" PageSize="50000" PagerSettings-Mode="Numeric" OnPageIndexChanged="grdCustomer_PageIndexChanged"
                                OnItemCommand="grdCustomer_RowCommand" HeaderStyle-CssClass="gridheader" AlternatingItemStyle-CssClass="gridAlternateItem"
                                GridLines="none" BorderColor="black" BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem"
                                OnItemDataBound="grdCustomer_RowDataBound">
                                <Columns>
                                    <asp:TemplateColumn HeaderText="S.No.">
                                        <ItemStyle Width="25%" />
                                        <ItemTemplate>
                                            <%# Container.DataSetIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Size">
                                        <ItemStyle Width="45%" HorizontalAlign="Left"  CssClass="gridpadding"  />
                                        <ItemTemplate>
                                            <%# Eval("Size")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Edit">
                                        <ItemStyle Width="15%" />
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lbEdit" CommandArgument='<%#Eval("SizeId") %>'
                                                CommandName="Edit" Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Delete">
                                        <ItemStyle Width="15%" />
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lbDelete" OnClientClick="return askDeleteCostCenter();"
                                                CommandArgument='<%#Eval("SizeId") %>' CommandName="Delete" Text="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

