﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintBankReceiptVoucherSimple : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();
    AccFiles objFile = new AccFiles();
    Reports objReports = new Reports();
    string TotalAmount = "0.00";

   
   
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if ((Sessions.CustomerCode == "") || (Request["VoucherSimpleMainId"].ToString() == "") )
        {
            Response.Redirect("default.aspx");
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {


             bindSupplier();                    
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.id = Convert.ToInt32(Request["VoucherSimpleMainId"]);
            DataTable dt = new DataTable();
            objSalesInvoice.DataFrom = "BRV";
            dt = objSalesInvoice.getVoucherSimpleMain();
            if (dt.Rows.Count > 0)
            {
                lblVoucherNo.Text = Convert.ToString(dt.Rows[0]["VoucherNo"]);
                lblVoucherDate.Text = ClsGetDate.FillFromDate(Convert.ToString(dt.Rows[0]["VoucherDate"]));
                lblBankAc.Text = Convert.ToString(dt.Rows[0]["BankAcTitle"]); 
                // ddlBankAC.SelectedValue = Convert.ToString(dtBankReceiptSummary.Rows[0]["BankAcId"]);
                lblChqNo.Text = Convert.ToString(dt.Rows[0]["ChqNo"]);
                lblChqDate.Text = ClsGetDate.FillFromDate(Convert.ToString(dt.Rows[0]["ChqDate"]));
                lblDesForMain.Text = Convert.ToString(dt.Rows[0]["Description"]);
               
                TotalAmount = Convert.ToString(String.Format("{0:C}", dt.Rows[0]["TotalAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                string[] num = Convert.ToDecimal(TotalAmount).ToString().Split('.');
                lblTotalEng.Text = NumberToText(Convert.ToInt32(num[0])) + " Only.";
                if (Convert.ToInt32(num[1]) > 0)
                {
                    lblTotalEng.Text = NumberToText(Convert.ToInt32(num[0])) + " AND " + Convert.ToString(num[1]) + "/100 Only.";

                }
                DataSet ds = new DataSet();
                objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                ds = objCust.GetCurrency();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblCurrency.Text = ds.Tables[0].Rows[0]["CurrencyDesc"].ToString();
                }              
            }
            bindBrvSimple();  
            FillLogo();
            lblUser.Text = Sessions.UseLoginName.ToString();


        }
    }

   


    #region Bind ledger grid
    protected void bindBrvSimple()
    {
        DataTable dtBRVSimpleDetail = new DataTable();
        objSalesInvoice.id = 0;
        objSalesInvoice.VoucherSimpleMainId = Convert.ToInt32(Request["VoucherSimpleMainId"]);
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objSalesInvoice.DataFrom = "BRV";
        dtBRVSimpleDetail = objSalesInvoice.getVoucherSimpleDetail();
        if (dtBRVSimpleDetail.Rows.Count > 0)
        {
            grdLedger.DataSource = dtBRVSimpleDetail;
            grdLedger.DataBind();
            grdLedger.Visible = true;
        }
        else
        {
            grdLedger.Visible = false;
        }

    }

    #endregion
    #region Grid RowDataBound
    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        

        if (e.Row.RowType == DataControlRowType.Footer)  // For footer totals
        {

            Label lblTotalAmount = (Label)e.Row.FindControl("lblTotalAmount");
            lblTotalAmount.Text = TotalAmount;
            //Label lblTotalEng = (Label)e.Row.FindControl("lblTotalEng");
            //Label lblCurrency = (Label)e.Row.FindControl("lblCurrency");
            
          
           
        }

    }
    #endregion

    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }

    }
    #endregion
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }

    public string NumberToText(int number)
    {
        if (number == 0) return "Zero";

        if (number == -2147483648) return "Minus Two Hundred and Fourteen Crore Seventy Four Lakh Eighty Three Thousand Six Hundred and Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        if (number < 0)
        {
            sb.Append("Minus ");
            number = -number;
        }

        string[] words0 = {"" ,"One ", "Two ", "Three ", "Four ", 
                "Five " ,"Six ", "Seven ", "Eight ", "Nine "};

        string[] words1 = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", 
                "Fifteen ","Sixteen ","Seventeen ","Eighteen ", "Nineteen "};

        string[] words2 = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", 
                "Seventy ","Eighty ", "Ninety "};

        string[] words3 = { "Thousand ", "Lakh ", "Crore " };

        num[0] = number % 1000; // units 
        num[1] = number / 1000;
        num[2] = number / 100000;
        num[1] = num[1] - 100 * num[2]; // thousands 
        num[3] = number / 10000000; // crores 
        num[2] = num[2] - 100 * num[3]; // lakhs 

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }


        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;

            u = num[i] % 10; // ones 
            t = num[i] / 10;
            h = num[i] / 100; // hundreds 
            t = t - 10 * h; // tens 

            if (h > 0) sb.Append(words0[h] + "Hundred ");

            if (u > 0 || t > 0)
            {
                if (h > 0 || i == 0) sb.Append("and ");

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);
            }

            if (i != 0) sb.Append(words3[i - 1]);

        }
        return sb.ToString().TrimEnd();
    } 
}
