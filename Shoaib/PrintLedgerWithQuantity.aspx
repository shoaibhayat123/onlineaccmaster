﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintLedgerWithQuantity.aspx.cs"
    Inherits="PrintLedgerWithQuantity" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Tahoma; background: none; font-size: 10px;">
    <form id="form1" runat="server">
        <div style="text-align: left; width: 1000px; float: left">
        <table style="width: 1000px; float: left; font-size: 14px; font-weight: bold;">
           <tr id="trTrialBal" runat="server">
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Account
                        Ledger<br />
                    </span><span style="font-size: 14px;">From :
                        <asp:Label ID="lblFromDate" runat="server" Style="padding-right: 15px;"></asp:Label>
                        To :
                        <asp:Label ID="lblToDate" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblBuyerName" runat="server"></asp:Label><br />
                    <span style="font-size: 12px; font-weight: normal">
                        <asp:Label ID="lblBuyerAddress" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress2" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress3" runat="server"></asp:Label></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <hr />
                    <br />
                </td>
            </tr>
        </table>
    </div>
        <div style="text-align: left; width: 1000px; float: left">
        <asp:GridView ID="grdLedger" Width="1000px" runat="server" ShowFooter="True" AutoGenerateColumns="False"
            AllowPaging="true" PageSize="500000" PagerSettings-Visible="false" PagerSettings-Mode="Numeric"
            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
            BorderColor="black" OnRowDataBound="grdLedger_RowDataBound">
            <EmptyDataTemplate>
                <div style="padding-bottom: 10px; width: 100%; text-align: center; font-family: Gisha;
                    font-size: 20px; background-color: #E1F5FD; color: #110F0F;">
                    <div style="padding-top: 10px; padding-bottom: 5px;">
                        No record found
                    </div>
                </div>
            </EmptyDataTemplate>
            <Columns>
                <asp:TemplateField HeaderText="Date">
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemStyle Width="8%" HorizontalAlign="Center" CssClass=" gridpadding" />
                    <ItemTemplate>
                        <asp:Label ID="lbltransDate" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 
                <asp:TemplateField HeaderText="Description">
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemStyle Width="15%"  HorizontalAlign="Left" CssClass=" gridpadding" />
                    <FooterStyle HorizontalAlign="Left" />
                    <ItemTemplate>
                        <%#Eval("Description")%>
                    </ItemTemplate>
                    <FooterTemplate>
                        Total Transactions :
                        <asp:Label ID="lblTotalTransection" runat="server"></asp:Label></FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Data From">
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemStyle Width="4%" HorizontalAlign="Left" CssClass=" gridpadding" />
                    <ItemTemplate>
                        <%#Eval("DataFrom")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="V.NO.">
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemStyle Width="6%" HorizontalAlign="Center" CssClass=" gridpadding" />
                    <ItemTemplate>
                        <%#Eval("InvoiceNo")%>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="R #">
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemStyle Width="6%" HorizontalAlign="Center" CssClass=" gridpadding" />
                    <ItemTemplate>
                        <%#Eval("ReceiptNo")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Instrument No">
                    <ItemStyle Width="8%" HorizontalAlign="Center" CssClass=" gridpadding" />
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemTemplate>
                        <%#Eval("ChqNo").ToString() == "" ? Eval("InvoiceNo") : Eval("ChqNo")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Chq Date">
                    <ItemStyle Width="6%" HorizontalAlign="Center" CssClass=" gridpadding" />
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemTemplate>
                        <asp:Label ID="lblChqDate" runat="server"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        Totals :</FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Debit" FooterText="Total Debit">
                    <ItemStyle Width="7%" HorizontalAlign="Right" CssClass=" gridpadding" />
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemTemplate>
                        <asp:Label ID="lblDebit" runat="server"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalDebit" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Credit">
                    <ItemStyle Width="7%" HorizontalAlign="Right" CssClass=" gridpadding" />
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemTemplate>
                        <asp:Label ID="lblCredit" runat="server"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalCredit" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Balance">
                    <ItemStyle Width="12%" HorizontalAlign="Right" CssClass=" gridpadding" />
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemTemplate>
                        <asp:Label ID="lblBalance" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Detail">
                    <ItemStyle Width="30%" HorizontalAlign="Right" CssClass=" gridpadding" />
                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                    <ItemTemplate>
                        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowFooter="false" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                            BorderColor="black" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Item Description">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemStyle Width="35%" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemTemplate>
                                        <%# Eval("ItemDesc") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Quantity">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemStyle Width="8%" HorizontalAlign="Right" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalQuantity" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Unit">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemStyle Width="8%" HorizontalAlign="left" />
                                    <ItemTemplate>
                                        <%# Eval("Unit")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rate">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Gross Amount">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalGrossAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="S.T %">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemStyle Width="7%" HorizontalAlign="Right" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemTemplate>
                                        <%# Convert.ToDecimal(Eval("SaleTax")).ToString("0.00")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="S.T Amount">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemStyle Width="8%" HorizontalAlign="Right" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("SaleTaxAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalSaleTaxAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount Including Taxes">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" Width="15%" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("AmountIncludeTaxes")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalAmountIncludeTax" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dgSales" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowFooter="false" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                            BorderColor="black" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Item Description">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemStyle Width="50%" />
                                    <ItemTemplate>
                                        <%# Eval("ItemDesc") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Quantity">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalQuantity" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Unit">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemStyle Width="10%" HorizontalAlign="left" />
                                    <ItemTemplate>
                                        <%# Eval("Unit")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rate">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemStyle Width="13%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Gross Amount">
                                    <HeaderStyle HorizontalAlign="Center" BorderColor="#000000" BorderWidth="1px" />
                                    <ItemStyle Width="18%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalGrossAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dgPurchaseTax" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowFooter="false" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                            BorderColor="black" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Item Description">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="35%" />
                                    <ItemTemplate>
                                        <%# Eval("ItemDesc") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Quantity">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="8%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalQuantity" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Unit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="8%" HorizontalAlign="left" />
                                    <ItemTemplate>
                                        <%# Eval("Unit")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Gross Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalGrossAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="S.T %">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="7%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Convert.ToDecimal(Eval("SaleTax")).ToString("0.00")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="S.T Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="8%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("SaleTaxAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalSaleTaxAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount Including Taxes">
                                    <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("AmountIncludeTaxes")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalAmountIncludeTax" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dgPurchase" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowFooter="false" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                            BorderColor="black" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Item Description">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="50%" />
                                    <ItemTemplate>
                                        <%# Eval("ItemDesc") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Quantity">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalQuantity" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Unit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="left" />
                                    <ItemTemplate>
                                        <%# Eval("Unit")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="13%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Gross Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="18%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalGrossAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dgPurTaxRet" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowFooter="false" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                            BorderColor="black" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Item Description">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="35%" />
                                    <ItemTemplate>
                                        <%# Eval("ItemDesc") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Quantity">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="8%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalQuantity" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Unit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="8%" HorizontalAlign="left" />
                                    <ItemTemplate>
                                        <%# Eval("Unit")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Gross Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalGrossAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="S.T %">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="7%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Convert.ToDecimal(Eval("SaleTax")).ToString("0.00")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="S.T Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="8%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("SaleTaxAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalSaleTaxAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount Including Taxes">
                                    <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("AmountIncludeTaxes")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalAmountIncludeTax" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dgPurRet" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowFooter="false" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                            BorderColor="black" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Item Description">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="50%" />
                                    <ItemTemplate>
                                        <%# Eval("ItemDesc") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Quantity">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalQuantity" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Unit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="left" />
                                    <ItemTemplate>
                                        <%# Eval("Unit")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="13%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Gross Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="18%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalGrossAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dgSalesTaxreturn" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowFooter="false" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                            BorderColor="black" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Item Description">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="35%" />
                                    <ItemTemplate>
                                        <%# Eval("ItemDesc") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Quantity">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="8%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalQuantity" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Unit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="8%" HorizontalAlign="left" />
                                    <ItemTemplate>
                                        <%# Eval("Unit")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Gross Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalGrossAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="S.T %">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="7%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Convert.ToDecimal(Eval("SaleTax")).ToString("0.00")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="S.T Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="8%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("SaleTaxAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalSaleTaxAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount Including Taxes">
                                    <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("AmountIncludeTaxes")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalAmountIncludeTax" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dgSalesRet" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowFooter="false" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                            BorderColor="black" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Item Description">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="50%" />
                                    <ItemTemplate>
                                        <%# Eval("ItemDesc") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Quantity">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalQuantity" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Unit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="left" />
                                    <ItemTemplate>
                                        <%# Eval("Unit")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="13%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Gross Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="18%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalGrossAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dgService" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowFooter="false" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                            BorderColor="black" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Description">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="left" Width="65%" />
                                    <ItemTemplate>
                                        <%# Eval("Description")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Right" Width="20%" />
                                    <FooterStyle HorizontalAlign="Right" Font-Size="12px" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Amount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dgPOS" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowFooter="false" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                            BorderColor="black" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Bar Code No">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="12%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Eval("BarcodeNo")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Item Description">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="50%" />
                                    <ItemTemplate>
                                        <%# Eval("ItemDesc") %>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Quantity">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Rate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dgYarnSale" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowFooter="false" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                            BorderColor="black" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="item">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="50%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Eval("item")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="D.O.NO.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="12%" />
                                    <ItemTemplate>
                                        <%# Eval("DoNumber")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Kgs">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Eval("Kgs")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Lbs">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Eval("Lbs")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Sale Rate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Eval("SaleRate")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Sale Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Eval("SaleAmount")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dgYarnPurchase" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowFooter="false" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                            BorderColor="black" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="item">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="50%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Eval("item")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="D.O.NO.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="12%" />
                                    <ItemTemplate>
                                        <%# Eval("DoNumber")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Kgs">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Eval("Kgs")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Lbs">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Eval("Lbs")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Purchase Rate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Eval("PurcheseRate")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Purchase Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Eval("PurchaseAmount")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dgJvSale" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowFooter="false" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                            BorderColor="black" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="item">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="50%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Eval("item")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="D.O.NO.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="12%" />
                                    <ItemTemplate>
                                        <%# Eval("DoNumber")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Quantity">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Eval("Lbs")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Sale Rate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Eval("SaleRate")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Sale Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Eval("SaleAmount")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:DataGrid ID="dgJvPurchase" Width="100%" runat="server" AutoGenerateColumns="False"
                            ShowFooter="false" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                            HeaderStyle-BackColor="#85A68C" HeaderStyle-ForeColor="#ffffff" FooterStyle-BackColor="#85A68C"
                            FooterStyle-Font-Size="9px" FooterStyle-Font-Bold="true" FooterStyle-ForeColor="Black"
                            FooterStyle-HorizontalAlign="Right" ItemStyle-BackColor="#ffffff" AlternatingItemStyle-BackColor="#DDEEDC"
                            BorderColor="black" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="item">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="50%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Eval("item")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="D.O.NO.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="12%" />
                                    <ItemTemplate>
                                        <%# Eval("DoNumber")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            
                                <asp:TemplateColumn HeaderText="Quantity">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Eval("Lbs")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Purchase Rate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Eval("PurcheseRate")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Purchase Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Eval("PurchaseAmount")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        </div>
        <div style="text-align: left; width: 1000px; float: left">
        <table style="width: 1000px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 1000px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>
                                <script type="text/javascript">

                                    var currentTime = new Date()
                                    var hours = currentTime.getHours()
                                    var Newhours = 0;
                                    var minutes = currentTime.getMinutes()
                                    if (minutes < 10) {
                                        minutes = "0" + minutes
                                    }
                                    if (hours > 12) {
                                        Newhours = hours - 12;
                                    }
                                    else {
                                        Newhours = hours;
                                    }
                                    document.write(Newhours + ":" + minutes + " ")
                                    if (hours > 11) {
                                        document.write("PM")
                                    } else {
                                        document.write("AM")
                                    }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: right;">
                                <asp:LinkButton Text="Print" ID="lnkPrint" OnClientClick="window.print();" runat="server"></asp:LinkButton>
                                <a href="SendEmail.aspx?page=SendEmail.aspx?page" style="margin-left: 15px" title="AccMaster"
                                    rel="gb_page_center[580, 440]">Email</a>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
