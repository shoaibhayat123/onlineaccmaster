﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="SingleTMSearch.aspx.cs" Inherits="SingleTMSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="TM Single Query"></asp:Label>
    <script type="text/javascript">

        function Count(text, long) {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }
        function func(obj) {

            var t2 = document.getElementById(obj);
            t2.focus();

        }
          
    </script>
    <script type="text/javascript" src="highslide/highslide-with-gallery.js"></script>
    <link rel="stylesheet" type="text/css" href="highslide/highslide.css" />
    <script type="text/javascript">
        hs.graphicsDir = 'highslide/graphics/';
        hs.align = 'center';
        hs.transitions = ['expand', 'crossfade'];
        hs.outlineType = 'rounded-white';
        hs.fadeInOut = true;
        hs.numberPosition = 'caption';
        hs.dimmingOpacity = 0.75;

        // Add the controlbar
        if (hs.addSlideshow) hs.addSlideshow({
            //slideshowGroup: 'group1',
            interval: 500,
            repeat: false,
            useControls: true,
            fixedControls: 'fit',
            overlayOptions: {
                opacity: .75,
                position: 'bottom center',
                hideOnMouseOut: true
            }
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <div>
              
                <div style="float: left; width: 60%">
                    <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                        width="100%">
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 13%">
                                Application # :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <table>
                                    <tr>
                                        <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                            <asp:TextBox Enabled="false" ID="txtApplication" AutoPostBack="true" OnChange="isNumericint(this)"
                                                TabIndex="1" MaxLength="10" runat="server" Width="100px"></asp:TextBox>
                                            <asp:HiddenField ID="hdnBrokerageId" runat="server" />
                                        </td>
                                        <td align="left" valign="top" style="padding: 2px 7px 2px 12px; text-align: right;
                                            width: 79px;">
                                            File # :
                                        </td>
                                        <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                            <asp:TextBox Enabled="false" ID="txtMemo" TabIndex="2" MaxLength="10" runat="server"
                                                Width="100px" AutoPostBack="true"></asp:TextBox>
                                        </td>
                                        <td align="left" valign="top" style="padding: 2px 2px 2px 8px; display: none">
                                            <span id="spanPgging" runat="server" style="padding: 0 20px 0 20px; font-weight: bold;
                                                font-size: 18px">
                                                <asp:LinkButton ID="lnkFirst" Text="|<" runat="server" OnClick="lnkFirst_Click"> </asp:LinkButton>
                                                <asp:LinkButton ID="lnkPrevious" Style="margin-left: 10px;" Text="<" runat="server"
                                                    OnClick="lnkPrevious_Click"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkNext" Text=">" Style="margin-left: 10px;" runat="server" OnClick="lnkNext_Click"></asp:LinkButton>
                                                <asp:LinkButton ID="lnLast" Text=">|" Style="margin-left: 10px;" runat="server" OnClick="lnLast_Click"></asp:LinkButton></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Date Of Filling :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                                <table>
                                    <tr>
                                        <td align="left" valign="top">
                                            <asp:TextBox Enabled="false" ID="txtDateOfFilling" TabIndex="3" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtDateOfFilling'),this);"
                                                runat="server" Width="100px"></asp:TextBox>
                                            <%--  <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                                onclick="scwShow(scwID('ctl00_MainHome_txtDateOfFilling'),this);" />--%>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" Display="Dynamic"
                                                ErrorMessage="Please Fill  Date Of Filling." Text="*" ControlToValidate="txtDateOfFilling"
                                                ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                                ControlToValidate="txtDateOfFilling" ValidationGroup="AddInvoice" ErrorMessage="Invalid  Date Of Filling !!"
                                                Display="None"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" valign="top" style="padding: 2px 2px 2px 0px; width: 100px">
                                            Take Over Date :
                                        </td>
                                        <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                            <asp:TextBox Enabled="false" ID="txtTakeOverDate" TabIndex="3" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtTakeOverDate'),this);"
                                                runat="server" Width="100px"></asp:TextBox>
                                            <%--    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                                onclick="scwShow(scwID('ctl00_MainHome_txtTakeOverDate'),this);" />--%>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" Display="Dynamic"
                                                ErrorMessage="Please Fill  Take Over Date." Text="*" ControlToValidate="txtTakeOverDate"
                                                ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                                ControlToValidate="txtTakeOverDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid  Take Over Date !!"
                                                Display="None"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Period of use :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <asp:TextBox Enabled="false" ID="txtPeriod" onChange="capitalizeMe(this)" TextMode="SingleLine"
                                    runat="server" TabIndex="3" MaxLength="25" CssClass="input_box2"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtPeriod"
                                    Display="None" ErrorMessage="Please enter   Period of use " SetFocusOnError="True"
                                    ValidationGroup="AddInvoice">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Word/Label :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <asp:RadioButton ID="rdbWord" Enabled="false" Text="Word" GroupName="Word" TabIndex="3"
                                    Checked="true" runat="server" />
                                <asp:RadioButton ID="rdbLabel" Enabled="false" Text="Label" GroupName="Word" TabIndex="3"
                                    runat="server" />
                                <asp:FileUpload ID="fuTradeMark" TabIndex="23" Style="display: none" runat="server" />
                                <div id="imagediv" runat="server" style="display: none">
                                    <a id="thumb1" runat="server" class="highslide" onclick="return hs.expand(this)">
                                        <img id="imgTradeMark" runat="server" width="100" height="100" alt="." border="0" /></a></div>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" colspan="2" style="padding: 2px 2px 2px 8px;">
                                <b style="text-decoration: underline; font-size: 18px">Classes :</b>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" colspan="2" style="padding: 2px 2px 2px 8px;">
                                <asp:CheckBoxList Enabled="false" RepeatDirection="Horizontal" TabIndex="3" Width="750px"
                                    RepeatColumns="6" ID="chkClass" runat="server">
                                </asp:CheckBoxList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Trademark :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <asp:TextBox Enabled="false" ID="txttradeMark" TabIndex="4" onChange="capitalizeMe(this)"
                                    TextMode="SingleLine" runat="server" MaxLength="100" CssClass="input_box2"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txttradeMark"
                                    Display="None" ErrorMessage="Please enter  Trademark " SetFocusOnError="True"
                                    ValidationGroup="AddInvoice">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Goods :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <asp:TextBox Enabled="false" ID="txtGoods" onChange="capitalizeMe(this)" Width="415px"
                                    TabIndex="4" TextMode="MultiLine" Rows="4" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Client Name:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <asp:DropDownList Enabled="false" ID="ddlClient" TabIndex="5" runat="server" Style="width: 415px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                                    InitialValue="0" ErrorMessage="Please select  Client Name" Text="*" ControlToValidate="ddlClient"
                                    ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Show cause received :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <asp:DropDownList Enabled="false" ID="ddlShowCase" TabIndex="5" runat="server" AutoPostBack="true">
                                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                    <asp:ListItem Text="No" Selected="True" Value="No"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td colspan="2" style="width: 600px">
                                            <asp:HiddenField ID="HiddenField1" runat="server" />
                                            <asp:DataGrid ID="dgGallery2" Width="100%" runat="server" AutoGenerateColumns="False"
                                                AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                                AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                                BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery2_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="S.No.">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <%# Container.DataSetIndex + 1%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Show Cause Rec. Date">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" Width="30%" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblShowCauseRecDate" runat="server"></asp:Label>
                                                            <%--  <%# Eval("ShowCauseRecDate")%>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Show Cause Reply Date">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" Width="30%" />
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblShowCauseReplyDate" runat="server"></asp:Label>
                                                            <%--    <%# Eval("ShowCauseReplyDate")%>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                App. Evidence Sub. Date :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <asp:TextBox Enabled="false" ID="txtFilmSubDate" TabIndex="7" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtFilmSubDate'),this);"
                                    runat="server" Width="100px"></asp:TextBox>
                                <%--   <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                    onclick="scwShow(scwID('ctl00_MainHome_txtFilmSubDate'),this);" />--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                    ControlToValidate="txtFilmSubDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid App. Evidence Sub. Date!!"
                                    Display="None"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Acc Rec date :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                                <table>
                                    <tr>
                                        <td align="left" valign="top">
                                            <asp:TextBox Enabled="false" ID="txtAcRecDate" TabIndex="8" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtAcRecDate'),this);"
                                                runat="server" Width="100px"></asp:TextBox>
                                            <%--  <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                                onclick="scwShow(scwID('ctl00_MainHome_txtAcRecDate'),this);" />--%>
                                            <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill  Acc Rec  Date." Text="*" ControlToValidate="txtAcRecDate"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>--%>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                                ControlToValidate="txtAcRecDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid  Acc Rec date !!"
                                                Display="None"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" valign="top" style="padding: 2px 2px 2px 0px; width: 168px">
                                            Acceptance Send To client :
                                        </td>
                                        <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                            <asp:DropDownList Enabled="false" ID="ddlAcceptenceToClient" TabIndex="9" runat="server">
                                                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Selected="True" Value="No"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Opposition :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <asp:DropDownList Enabled="false" ID="ddlOpposition" TabIndex="11" runat="server"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlOpposition_SelectedIndexChanged">
                                    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                    <asp:ListItem Text="No" Selected="True" Value="No"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Demand Note Rec Date :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                                <table>
                                    <tr>
                                        <td align="left" valign="top">
                                            <asp:TextBox Enabled="false" ID="txtNoteRecDate" TabIndex="12" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtNoteRecDate'),this);"
                                                runat="server" Width="100px"></asp:TextBox>
                                            <%--    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                                onclick="scwShow(scwID('ctl00_MainHome_txtNoteRecDate'),this);" />--%>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                                ControlToValidate="txtNoteRecDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid  D/Note Rec Date !!"
                                                Display="None"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" valign="top" style="padding: 2px 2px 2px 0px; width: 168px">
                                            Demand Note Send To Client :
                                        </td>
                                        <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                            <asp:DropDownList Enabled="false" ID="ddlNoteSendToClient" TabIndex="13" runat="server">
                                                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Selected="True" Value="No"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Demand Note Deposit Date :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <asp:TextBox Enabled="false" ID="txtDemandNoteDeposit" TabIndex="13" MaxLength="25"
                                    onclick="scwShow(scwID('ctl00_MainHome_txtDemandNoteDeposit'),this);" runat="server"
                                    Width="100px"></asp:TextBox>
                                <%--      <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                    onclick="scwShow(scwID('ctl00_MainHome_txtDemandNoteDeposit'),this);" />--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                    ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                    ControlToValidate="txtDemandNoteDeposit" ValidationGroup="AddInvoice" ErrorMessage="Invalid  Demand Note Deposit Date!!"
                                    Display="None"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Certificate Issued :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <table>
                                    <tr>
                                        <td align="left" valign="top">
                                            <asp:DropDownList Enabled="false" ID="ddlCertificateIssued" TabIndex="13" runat="server">
                                                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                                <asp:ListItem Text="No" Selected="True" Value="No"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" valign="top" style="padding: 2px 2px 2px 0px; width: 168px">
                                            Certificate Received Date:
                                        </td>
                                        <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                            <asp:TextBox Enabled="false" ID="txtCertificateRecDate" TabIndex="13" MaxLength="25"
                                                onclick="scwShow(scwID('ctl00_MainHome_txtCertificateRecDate'),this);" runat="server"
                                                Width="100px"></asp:TextBox>
                                            <%--     <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                                onclick="scwShow(scwID('ctl00_MainHome_txtCertificateRecDate'),this);" />--%>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                                ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                                ControlToValidate="txtCertificateRecDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid  Demand Note Deposit Date!!"
                                                Display="None"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Remarks :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <asp:TextBox Enabled="false" ID="txtRemark" TabIndex="14" onChange="capitalizeMe(this)"
                                    TextMode="SingleLine" runat="server" MaxLength="100" CssClass="input_box2"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Status:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <asp:DropDownList Enabled="false" ID="ddlStatus" TabIndex="15" runat="server" Style="width: 415px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="Dynamic"
                                    InitialValue="0" ErrorMessage="Please select Status" Text="*" ControlToValidate="ddlStatus"
                                    ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Reminder For :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <asp:TextBox Enabled="false" ID="txtReminder" onChange="capitalizeMe(this)" TabIndex="16"
                                    TextMode="SingleLine" runat="server" MaxLength="100" CssClass="input_box2"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Reminder Date:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                <asp:TextBox Enabled="false" ID="txtReminderDate" TabIndex="17" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtReminderDate'),this);"
                                    runat="server" Width="100px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                    ControlToValidate="txtReminderDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid Reminder Date !!"
                                    Display="None"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                TM 11 Filled On :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                                <table>
                                    <tr>
                                        <td align="left" valign="top">
                                            <asp:TextBox Enabled="false" ID="txtTm11Filled" TabIndex="24" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtTm11Filled'),this);"
                                                runat="server" Width="100px"></asp:TextBox>
                                            <%--    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                                onclick="scwShow(scwID('ctl00_MainHome_txtTm11Filled'),this);" />--%>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                                ControlToValidate="txtTm11Filled" ValidationGroup="AddInvoice" ErrorMessage="Invalid TM 11 Filled On Date !!"
                                                Display="None"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" valign="top" style="padding: 2px 2px 2px 0px; width: 168px">
                                            TM 12 Filled On :
                                        </td>
                                        <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                            <asp:TextBox Enabled="false" ID="txtTm12Filled" TabIndex="25" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtTm12Filled'),this);"
                                                runat="server" Width="100px"></asp:TextBox>
                                            <%--    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                                onclick="scwShow(scwID('ctl00_MainHome_txtTm12Filled'),this);" />--%>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                                ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                                ControlToValidate="txtTm12Filled" ValidationGroup="AddInvoice" ErrorMessage="Invalid TM 12 Filled On Date !!"
                                                Display="None"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td colspan="2" style="width: 200px">
                                            <asp:HiddenField ID="hdndetail" runat="server" />
                                            <asp:DataGrid ID="dgGallery" Width="80%" runat="server" AutoGenerateColumns="False"
                                                AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                                AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                                BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="S.No.">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <%# Container.DataSetIndex + 1%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Conflicting Trademark">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="left" Width="25%" />
                                                        <ItemTemplate>
                                                            <%# Eval("ConflictingTrademark")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="TM. No.">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="left" Width="15%" />
                                                        <ItemTemplate>
                                                            <%# Eval("TmNo")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Status">
                                                        <HeaderStyle HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="left" Width="35%" />
                                                        <ItemTemplate>
                                                            <%# Eval("Status")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="Form1" runat="server">
                            <td colspan="2">
                                <b>TM Form Details</b>
                            </td>
                        </tr>
                        <tr id="Form2" runat="server">
                            <td colspan="2" style="width: 200px">
                                <asp:DataGrid ID="dgTmForm" Width="80%" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                    AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                    BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgTmForm_RowDataBound">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Form #">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="20%" />
                                            <ItemTemplate>
                                                <%# Eval("FormName")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Filling Date">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblFillingDate" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Remark">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="60%" />
                                            <ItemTemplate>
                                                <%# Eval("Comment")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr id="Hearing1" runat="server">
                            <td colspan="2">
                                <b>TM Hearing</b>
                            </td>
                        </tr>
                        <tr id="Hearing2" runat="server">
                            <td colspan="2" style="width: 200px">
                                <asp:DataGrid ID="dgHearing" Width="80%" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                    AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                    BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="ddgHearing_RowDataBound">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Hearing Date">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblHearingDate" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Before">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="20%" />
                                            <ItemTemplate>
                                                <%# Eval("Before")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Comments/Arguments">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="25%" />
                                            <ItemTemplate>
                                                <%# Eval("Comment")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Adv/Agent Appeared">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="20%" />
                                            <ItemTemplate>
                                                <%# Eval("AdvOrAgentAppeared")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr id="Jurnal1" runat="server">
                            <td colspan="2">
                                <b>TM Journal</b>
                            </td>
                        </tr>
                        <tr id="Jurnal2" runat="server">
                            <td colspan="2" style="width: 200px">
                                <asp:DataGrid ID="dgJournal" Width="80%" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                    AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                    BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgJournal_RowDataBound">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="J/No">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="20%" />
                                            <ItemTemplate>
                                                <%# Eval("JournalNo")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Page No">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="20%" />
                                            <ItemTemplate>
                                                <%# Eval("PageNo")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Journal Date">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblJournalDate" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Published Date">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblPurchaseDate" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Remark">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="25%" />
                                            <ItemTemplate>
                                                <%# Eval("Comment")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr id="Renewal1" runat="server">
                            <td colspan="2">
                                <b>TM Renewal</b>
                            </td>
                        </tr>
                        <tr id="Renewal2" runat="server">
                            <td colspan="2" style="width: 200px">
                                <asp:DataGrid ID="dgRenewal" Width="80%" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                    AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                    BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgRenewal_RowDataBound">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Renewed Upto">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="15%" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblRenewalDate" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Remark">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="45%" />
                                            <ItemTemplate>
                                                <%# Eval("Remark")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr id="Document1" runat="server">
                            <td colspan="2">
                                <b>TM Document</b>
                            </td>
                        </tr>
                        <tr id="Document2" runat="Server">
                            <td colspan="2" style="width: 200px">
                                <asp:DataGrid ID="dgdocument" Width="80%" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                    AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                    BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgdocument_RowDataBound">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="S.No.">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <%# Container.DataSetIndex + 1%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Image/Document">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="20%" VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <div class="slide" id="d1" runat="server">
                                                    <div class="highslide-gallery" id="Naveen" runat="server" style="position: relative;
                                                        z-index: 50;">
                                                        <a id="thumb1" href="<%# "images/TMImages/"  + Eval("Image") %>" class="highslide"
                                                            onclick="return hs.expand(this)">
                                                            <img id="i1" src='<%# "images/TMImages/"+ Eval("Image") %>' width="100" height="100"
                                                                alt="." border="0" /></a>
                                                    </div>
                                                </div>
                                                <div id="imageDocument" style="display: none" runat="server">
                                                    <a href="<%# "images/TMImages/"+ Eval("Image") %>" target="_blank">Download </a>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Show To Client">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="15%" VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <%# Eval("IsShowtoClient")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Remarks">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="50%" VerticalAlign="Top" />
                                            <ItemTemplate>
                                                <%# Eval("Remark")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr id="trOpposition1" runat="server">
                            <td colspan="2">
                                <b>TM Opposition</b>
                            </td>
                        </tr>
                        <tr id="trOpposition2" runat="server">
                            <td colspan="2" style="width: 200px">
                                <asp:DataGrid ID="dgOpposition" Width="80%" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                    AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                    BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgOpposition_RowDataBound">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="S.No.">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Container.DataSetIndex + 1%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Opp.#/Rect.#">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle />
                                            <ItemTemplate>
                                                <%# Eval("OppositionNo")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Opposition Type">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle />
                                            <ItemTemplate>
                                                <%# Eval("OppositionType")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Status">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Eval("OppositionStatus")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Jounal No">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Convert.ToString(Eval("TmJounalNo"))!="0"?Eval("TmJounalNo"):""%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Date Of Journal">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lblJournalDate" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="client Name" ItemStyle-Width="20%">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Eval("TmCustomerName")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </div>
                
                <div style="float: left; width: 435px;">
                    <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                        width="100%">
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                Search By :
                                <asp:DropDownList ID="ddlSearchBy" runat="server">
                                    <asp:ListItem Text="Application #" Value="Application#"></asp:ListItem>
                                    <asp:ListItem Text="Client Name" Value="CustomerName"></asp:ListItem>
                                    <asp:ListItem Text="TradeMark" Value="TradeMark"></asp:ListItem>
                                    <asp:ListItem Text="File #" Value="Memo#"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                    ErrorMessage="Please Fill Search Text." Text="*" ControlToValidate="txtSearch"
                                    ValidationGroup="Serch"></asp:RequiredFieldValidator>
                                <asp:ValidationSummary ID="ValidationSummary3" runat="server" ShowMessageBox="true"
                                    ShowSummary="false" ValidationGroup="Serch" />
                                <asp:Button Text="Search" ID="btnSearch" ValidationGroup="Serch" OnClick="btnSearch_Click"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                <asp:DataGrid ID="dgagentSearch" Width="100%" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                    AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                    OnItemCommand="grdCustomer_RowCommand" BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText=" App #">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="15%" HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <%# Eval("ApplicationNum")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText=" File #">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="15%" HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <%# Eval("MemoNumber")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Customer Name">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="35%" HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <%# Eval("CustomerName")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="TradeMark">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="25%" HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <%# Eval("TradeMark")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Action">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="15%" HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <asp:Button ID="Button1" Text="View" CommandName="View" CommandArgument='<%#Eval("Id") %>'
                                                    runat="server" />
                                                      <asp:Button ID="Button2" Text="Print" CommandName="Print" CommandArgument='<%#Eval("Id") %>'
                                                    runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
