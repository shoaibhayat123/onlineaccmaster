﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class BoutiqueSalesInvoiceAcReportPrint : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();

    AccFiles objFile = new AccFiles();
    Reports objReports = new Reports();

    
    decimal TotalPurchaseAmount = 0;
    decimal TotalQuantity = 0;


    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if ((Sessions.CustomerCode == "") || (Request["From"].ToString() == "") || (Request["To"].ToString() == "") || (Request["MainCategoryCode"].ToString() == ""))
        {
            Response.Redirect("default.aspx");
        }
        if (!IsPostBack)
        {

            bindSupplier();

            lblFromDate.Text = ClsGetDate.FillFromDate(Request["From"].ToString());
            lblToDate.Text = ClsGetDate.FillFromDate(Request["To"].ToString());
            bindBuyer();
            FillLogo();
            lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
            lblUser.Text = Sessions.UseLoginName.ToString();
            bindrptrInvoiceReport();
            FillBrand();
            FillLabel();
            FillItem();

          //  GrandlblTotalQuantity.Text = String.Format("{0:C}", GrandTotalQuantity).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
           // GrandlblTotalGrossAmount.Text = String.Format("{0:C}", GrandTotalGrossAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            //GrandlblTotalSaleTaxAmount.Text = String.Format("{0:C}", GrandTotalSaleTaxAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            //GrandlblTotalAmountIncludeTaxes.Text = String.Format("{0:C}", GrandTotalAmountIncludeTaxes).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            
        }
    }

    protected void bindBuyer()
    {

    }
    #region Grid RowDataBound
    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    #endregion

    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }

    }
    #endregion
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }


   



    protected void bindrptrInvoiceReport()
    {
        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.InvDateFrom = Convert.ToDateTime(Request["from"]);
        objReports.invDateTo = Convert.ToDateTime(Request["to"]);
        objReports.DataFrom = "SALESINV";
        if (Convert.ToString(Request["Item"]) != "")
        {
            objReports.ItemCode = Convert.ToInt32(Request["Item"]);
        }
        objReports.MainCategoryCode = Convert.ToInt32(Request["MainCategoryCode"]);
        if (Convert.ToString(Request["SubCategoryCode"]) != "")
        {
            objReports.SubCategoryCode = Convert.ToInt32(Request["SubCategoryCode"]);
        }
        DataSet ds = new DataSet();
        ds = objReports.SearchPOSInvoiceReport();
        if (ds.Tables[0].Rows.Count > 0)
        {
            dgGallery.DataSource = ds.Tables[0];
            dgGallery.DataBind();
        }
    }


    protected void dgGallery_OnItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblInvDate = ((Label)e.Item.FindControl("lblInvDate"));
            lblInvDate.Text = ClsGetDate.FillFromDate(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "InvDate")));

            Label lblPurchaseRate = ((Label)e.Item.FindControl("lblPurchaseRate"));
            Label lblPurchaseAmount = ((Label)e.Item.FindControl("lblPurchaseAmount"));
            Label lblQuantity = ((Label)e.Item.FindControl("lblQuantity"));
            Label lblPurchaseOldAmount = ((Label)e.Item.FindControl("lblPurchaseOldAmount"));
            Label lblAlterCharges = ((Label)e.Item.FindControl("lblAlterCharges"));
            Label lblBoutiqueDiscount = ((Label)e.Item.FindControl("lblBoutiqueDiscount"));
            Label lblType = (Label)e.Item.FindControl("lblType");
            decimal AlterCharges = 0;
            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BoutiqueConvRateNew")) != "" && Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AlterCharges")) != "")
            {
                lblAlterCharges.Text = (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "BoutiqueConvRateNew")) * Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "AlterCharges"))).ToString("0.00");
                AlterCharges = (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "BoutiqueConvRateNew")) * Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "AlterCharges")));
            
            }
            else
            {
                lblAlterCharges.Text = "0.00";
            }
            lblBoutiqueDiscount.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "BoutiqueDiscount")) + "%";
                decimal PurchaseRate=0;
            decimal PurchaseAmount=0;
            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "PurchaseNewRate")) != "")
            {
                if (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "PurchaseNewRate")) != 0)
                {
                    lblPurchaseRate.Text = String.Format("{0:C}", Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "PurchaseNewRate"))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                 

                    PurchaseRate = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "PurchaseNewRate"));
                }
                else
                {
                    lblPurchaseRate.Text = String.Format("{0:C}", Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "PurchaseRate"))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
               
                    PurchaseRate = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "PurchaseRate"));
                }
            }

            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DataFrom")) != "SALERETINV")
            {
                PurchaseAmount = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity")) * PurchaseRate;

                lblPurchaseOldAmount.Text = String.Format("{0:C}", PurchaseAmount).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


                decimal Discount = 0;
                if (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "BoutiqueDiscount")) > 0)
                {
                    PurchaseAmount = PurchaseAmount - (PurchaseAmount * (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "BoutiqueDiscount")) / 100));
                }

               
                if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "AlterCharges")) != "")
                {
                    if (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "AlterCharges")) > 0)
                    {
                        PurchaseAmount = PurchaseAmount - AlterCharges;
                    }
                }
                lblPurchaseAmount.Text = String.Format("{0:C}", PurchaseAmount).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                lblQuantity.Text = Convert.ToString(Convert.ToInt32(Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity"))));
                TotalQuantity += Convert.ToInt32(Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity")));
                lblType.Text = "SALE";
            }

            else
            {
                PurchaseAmount = (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity")) * PurchaseRate) * -1;
                           
                lblPurchaseAmount.Text = String.Format("{0:C}", PurchaseAmount).Replace('-', ' ').Replace('$', ' ').Replace('(', '-').Replace(')', ' ');
                lblQuantity.Text ="-" + Convert.ToString(Convert.ToInt32(Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity"))));

                TotalQuantity -= Convert.ToInt32(Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity")));
                lblType.Text = "SALERET";
            }

          
               
            TotalPurchaseAmount += PurchaseAmount;
        }

        if (e.Item.ItemType == ListItemType.Footer)
        {
            Label lblTotalPurchaseAmount = ((Label)e.Item.FindControl("lblTotalPurchaseAmount"));

            lblTotalPurchaseAmount.Text = String.Format("{0:C}", TotalPurchaseAmount).Replace('-', ' ').Replace('$', ' ').Replace('(', '-').Replace(')', ' ');
            
            Label lblTotalQuantity = ((Label)e.Item.FindControl("lblTotalQuantity"));
            lblTotalQuantity.Text = Convert.ToString(TotalQuantity);
        }

    }



    #region BindBrand
    protected void FillBrand()
    {
        if (Convert.ToString(Request["MainCategoryCode"]) != "")
        {
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.MainCategoryCode = Convert.ToInt32(Request["MainCategoryCode"]);
            DataSet ds = new DataSet();
            ds = objCust.GetMainCategory();
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblBrand.Text = Convert.ToString(ds.Tables[0].Rows[0]["MainCategoryName"]);
            }
        }
        else
        {
            lblBrand.Text = "-";
        }


    }
    #endregion
    #region BindLabel
    protected void FillLabel()
    {
        if (Convert.ToDecimal(Request["SubCategoryCode"]) > 0)
        {
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.SubCategoryCode = Convert.ToInt32(Request["SubCategoryCode"]);
            DataSet ds = new DataSet();
            ds = objCust.GetSubCategory();
            if (ds.Tables[0].Rows.Count > 0)
            {
               lblLabel.Text = Convert.ToString(ds.Tables[0].Rows[0]["SubCategoryName"]);
            }
        }
        else
        {
            lblLabel.Text = "-";
        }
        
    }


    #endregion

    #region BindLabel
    protected void FillItem()
    {
        if (Convert.ToString(Request["Item"]) != "")
        {
            if (Convert.ToDecimal(Request["Item"]) > 0)
            {
                objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objCust.ItemId = Convert.ToInt32(Request["Item"]);
                DataSet ds = new DataSet();
                ds = objCust.getItems();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblItem.Text = Convert.ToString(ds.Tables[0].Rows[0]["ItemDesc"]);
                }
            }
            else
            {
                lblItem.Text = "-";
            }
        }
        else
        {
            lblItem.Text = "-";
        }

    }


    #endregion


}