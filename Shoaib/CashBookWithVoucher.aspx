﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="CashBookWithVoucher.aspx.cs" Inherits="CashBookWithVoucher" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Cash/Bank Book Report"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                       Cash / Bank A/c :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:DropDownList ID="ddlClientAC" runat="server" Style="width: 415px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select A/c. Title " Text="*" ControlToValidate="ddlClientAC"
                            ValidationGroup="Ledger"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Starting Date :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        <asp:TextBox ID="txtstartDate" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtstartDate'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtstartDate'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill Starting  Date." Text="*" ControlToValidate="txtstartDate"
                            ValidationGroup="Ledger"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtstartDate" ValidationGroup="Ledger" ErrorMessage="Invalid Invoice Date !!"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        End Date :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        <asp:TextBox ID="txtEndDate" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtEndDate'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtEndDate'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill  End  Date ." Text="*" ControlToValidate="txtEndDate"
                            ValidationGroup="Ledger"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtEndDate" ValidationGroup="Ledger" ErrorMessage="Invalid Invoice Date !!"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <%-- <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                Report Type :
            </td>
            <td>
                <asp:RadioButton ID="rdpprint" GroupName="report" Checked="true" runat="server" Text="Print" />
                <asp:RadioButton ID="rdbexcel" GroupName="report" runat="server" Text="Export in Excel" />
            </td>
        </tr>--%>
                <tr>
                    <td>
                        <asp:Button ID="btnGenerate" Text="Generate" ValidationGroup="Ledger" runat="server"
                            OnClick="btnGenerate_Click" />
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                            ShowSummary="false" ValidationGroup="Ledger" />
                    </td>
                </tr>
                <tr style="display: none">
                    <td colspan="2">
                        <asp:RadioButton ID="RadioButton3" GroupName="reporttype" runat="server" Text="Detail Ledger" />
                        <asp:RadioButton ID="RadioButton4" GroupName="reporttype" Checked="true" runat="server"
                            Text="Subsidiary Ledger" />
                        <asp:RadioButton ID="RadioButton5" GroupName="reporttype" runat="server" Text="FC Ledger" />
                    </td>
                </tr>
            </table>
            <div style="text-align: left; width: 1000px; float: left">
                <asp:GridView ID="grdLedger" Width="1000px" runat="server" ShowFooter="True" AutoGenerateColumns="False"
                    AllowPaging="true" PageSize="500000" PagerSettings-Visible="false" PagerSettings-Mode="Numeric"
                    HeaderStyle-CssClass="gridheader" FooterStyle-CssClass="gridFooter" AlternatingRowStyle-CssClass="gridAlternateItem"
                    GridLines="none" BorderColor="black" BorderStyle="Dotted" BorderWidth="1" RowStyle-CssClass="gridItem"
                    OnRowDataBound="grdLedger_RowDataBound">
                    <EmptyDataTemplate>
                        <div style="padding-bottom: 10px; width: 100%; text-align: center; font-family: Gisha;
                            font-size: 20px; background-color: #E1F5FD; color: #110F0F;">
                            <div style="padding-top: 10px; padding-bottom: 5px;">
                                No record found
                            </div>
                        </div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Date">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="8%" HorizontalAlign="Center" CssClass=" gridpadding" />
                            <ItemTemplate>
                                <asp:Label ID="lbltransDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" CssClass=" gridpadding" />
                            <FooterStyle HorizontalAlign="Left" />
                            <ItemTemplate>
                                <%#Eval("Description")%>
                            </ItemTemplate>
                            <FooterTemplate>
                                Total Transactions :
                                <asp:Label ID="lblTotalTransection" runat="server"></asp:Label></FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data From">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="10%" HorizontalAlign="Left" CssClass=" gridpadding" />
                            <ItemTemplate>
                                <%#Eval("DataFrom")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Instrument No">
                            <ItemStyle Width="8%" HorizontalAlign="Center" CssClass=" gridpadding" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <%#Eval("ChqNo").ToString() == "" ? Eval("InvoiceNo") : Eval("ChqNo")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Chq Date">
                            <ItemStyle Width="8%" HorizontalAlign="Center" CssClass=" gridpadding" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblChqDate" runat="server"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                Totals :</FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Debit" FooterText="Total Debit">
                            <ItemStyle Width="10%" HorizontalAlign="Right" CssClass=" gridpadding" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblDebit" runat="server"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalDebit" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Credit">
                            <ItemStyle Width="10%" HorizontalAlign="Right" CssClass=" gridpadding" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblCredit" runat="server"></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalCredit" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Balance">
                            <ItemStyle Width="12%" HorizontalAlign="Right" CssClass=" gridpadding" />
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblBalance" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
