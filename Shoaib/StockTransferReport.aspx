﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="StockTransferReport.aspx.cs" Inherits="StockTransferReport" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Stock Transfer Report"></asp:Label>
    <script type="text/javascript">

        function Count(text, long) {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }
          
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <%--<asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>--%>
            <asp:HiddenField ID="hdnInvoiceId" runat="server" />
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                width="60%">
                <tr>
                    <td style="text-align: left;">
                        <span style="float: left; padding-right: 47px">Voucher No :</span>
                        <asp:TextBox ID="txtVoucherNo" CssClass="input_box33" Width="100px" Style="float: left;
                            text-transform: uppercase;" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: left" align="left">
                                    Voucher Date From:
                                    <asp:TextBox ID="txtVoucherDateFrom" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtVoucherDateFrom'),this);"
                                        runat="server" Width="100px"></asp:TextBox>
                                    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                        onclick="scwShow(scwID('ctl00_MainHome_txtVoucherDateFrom'),this);" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill Voucher Date From." Text="*" ControlToValidate="txtVoucherDateFrom"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                        ControlToValidate="txtVoucherDateFrom" ValidationGroup="AddInvoice" ErrorMessage="Invalid Voucher Date From !!"
                                        Display="None"></asp:RegularExpressionValidator>
                                </td>
                                <td style="text-align: left" align="left">
                                    To :
                                    <asp:TextBox ID="txtVoucherDateTo" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtVoucherDateTo'),this);"
                                        runat="server" Width="100px"></asp:TextBox>
                                    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                        onclick="scwShow(scwID('ctl00_MainHome_txtVoucherDateTo'),this);" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill Voucher Date To." Text="*" ControlToValidate="txtVoucherDateTo"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                        ControlToValidate="txtVoucherDateTo" ValidationGroup="AddInvoice" ErrorMessage="Invalid Voucher Date To !!"
                                        Display="None">
                                    </asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        Cost Center From :
                        <asp:DropDownList ID="ddlCostCenter" runat="server" Style="width: 270px; margin-left: 12px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: left">
                                    Cost Center To :
                                    <asp:DropDownList ID="ddlCostCenterTo" runat="server" Style="width: 270px; margin-left: 25px">
                                    </asp:DropDownList>
                                </td>
                                <td style="text-align: left">
                                    <td>
                                        <asp:Button ID="btnsearch" Text="Search" runat="server" OnClick="btnsearch_Click" />
                                    </td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" style="float: left; padding: 0 10px 10px 10px;" border="0"
                cellpadding="0" width="70%">
                <tr>
                    <td colspan="3">
                        <asp:GridView ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                            AllowPaging="true" PageSize="500000" PagerSettings-Visible="false" PagerSettings-Mode="Numeric"
                            HeaderStyle-CssClass="gridheader" AlternatingRowStyle-CssClass="gridAlternateItem" OnRowCommand="grdCustomer_RowCommand"
                            OnRowDataBound="dgGallery_RowDataBound" GridLines="none" BorderColor="black"
                            FooterStyle-CssClass="gridFooter" BorderStyle="Dotted" BorderWidth="1" RowStyle-CssClass="gridItem"
                            ShowFooter="true">
                            <EmptyDataTemplate>
                                <div style="padding-bottom: 10px; width: 100%; text-align: center; font-family: Gisha;
                                    font-size: 20px; background-color: #E1F5FD; color: #110F0F;">
                                    <div style="padding-top: 10px; padding-bottom: 5px;">
                                        No record found
                                    </div>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="voucher No">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Eval("VoucherNo")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Voucher Date">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblVoucherDate" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cost Center From ">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Eval("CostCenterFromName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cost Center To">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Eval("CostCenterToName")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Quantity">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Totalquantity")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalquantity" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Eval("Description")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Edit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                      <%--  <a href='StockTransfer.aspx?VoucherSimpleMainId=<%#Eval("TransferSummaryId") %>'>
                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" /></a>--%>
                                             <asp:Button ID="btnEdit" CommandName="EditCustomer" CommandArgument='<%#Eval("TransferSummaryId") %>' runat="server" Text="Edit" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
    <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
