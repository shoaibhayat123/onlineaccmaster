﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PaymentApproved : System.Web.UI.Page
{
    Car objcar = new Car();
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Request["order_number"].ToString()) != "")
        {
            lbltransectionNo.Text = Request["invoice_id"].ToString();
            lblamount.Text = Request["total"].ToString();
            lblDate.Text = System.DateTime.Now.ToString();

            objcar.OrderId = Convert.ToString(Request["order_number"].ToString());

            objcar.getPaymentDetails();

        }
           
    }
}