﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintSalesInvoice : System.Web.UI.Page
{
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    User objUser = new User();
    SetCustomers objCust = new SetCustomers();
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();

        if (!IsPostBack)
        {
            if (Request["InvoiceSummaryId"] != null)
            {
                if (Sessions.CustomerCode != null && Convert.ToString(Sessions.CustomerCode) != "")
                {
                    Sessions.CustomerCode = Sessions.CustomerCode;
                    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                    objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(Request["InvoiceSummaryId"]);
                    objSalesInvoice.DataFrom = "SALESINV";
                    DataTable dtInvoiceNewDetails = new DataTable();
                    dtInvoiceNewDetails = objSalesInvoice.getInvoiceSummary();
                    if (dtInvoiceNewDetails.Rows.Count > 0)
                    {
                        lblInvoiceNo.Text = Convert.ToString(dtInvoiceNewDetails.Rows[0]["InvNo"]);
                        lblCrdays.Text = Convert.ToString(dtInvoiceNewDetails.Rows[0]["CrDays"]);
                        lblDate.Text = ClsGetDate.FillFromDate(Convert.ToString(dtInvoiceNewDetails.Rows[0]["InvDate"]));
                        BindItenDesc();
                        bindSupplier();
                        string[] num = Convert.ToDecimal(txtTotalGrossAmount.Text).ToString().Split('.');
                        lblAmount.Text = NumberToText(Convert.ToInt32(num[0])) + " Only.";
                        if (Convert.ToInt32(num[1]) > 0)
                        {
                            lblAmount.Text = NumberToText(Convert.ToInt32(num[0])) + " AND " + Convert.ToString(num[1]) + "/100 Only.";
                        }


                        DataSet ds = new DataSet();
                        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                        ds = objCust.GetCurrency();
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            lblCurrency.Text = ds.Tables[0].Rows[0]["CurrencyDesc"].ToString();
                        }
                    }

                }
                else
                {
                    Response.Redirect("default.aspx");
                }

            }


        }


    }
    protected void BindItenDesc()
    {
        DataTable dtInvoice = new DataTable();
        objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(Request["InvoiceSummaryId"]);
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objSalesInvoice.DataFrom = "SALESINV";
        dtInvoice = objSalesInvoice.getInvoiceDetails();

        if (dtInvoice.Rows.Count > 0)
        {
            rptrItemDesc.DataSource = dtInvoice;
            rptrItemDesc.DataBind();

            txtTotalGrossAmount.Text = Convert.ToString(String.Format("{0:C}", dtInvoice.Rows[0]["TotalGrossAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
           // txtSTtotalAmount.Text = Convert.ToString(String.Format("{0:C}", dtInvoice.Rows[0]["TotalSaleTaxAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
           // txtTotalAmountIncludingTaxe.Text = Convert.ToString(String.Format("{0:C}", dtInvoice.Rows[0]["TotalAmountIncludeTax"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

            bindBuyer(Convert.ToInt32(dtInvoice.Rows[0]["ClientAccountId"]));



        }
    }
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            lblSupplierTelephone.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);
            lblSellerRegNo.Text = Convert.ToString(dtSupplier.Rows[0]["SalesTaxNo"]);
            lblSellerNTN.Text = Convert.ToString(dtSupplier.Rows[0]["PanNo"]);

        }
    }
    protected void bindBuyer(int AcfileId)
    {
        objFile.id = AcfileId;
        objFile.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtBuyer = new DataTable();
        dtBuyer = objFile.getMenueDetail();


        if (dtBuyer.Rows.Count > 0)
        {
            lblBuyerName.Text = Convert.ToString(dtBuyer.Rows[0]["Title"]);
            lblBuyerAddress.Text = Convert.ToString(dtBuyer.Rows[0]["Address1"]);
            lblBuyerAddress2.Text = Convert.ToString(dtBuyer.Rows[0]["Address2"]);
            lblBuyerAddress3.Text = Convert.ToString(dtBuyer.Rows[0]["Address3"]);
            lblBuyerTelephone.Text = Convert.ToString(dtBuyer.Rows[0]["PhoneNo"]);
            lblBuyerRegNo.Text = Convert.ToString(dtBuyer.Rows[0]["SalesTaxNo"]);
            lblBuyerNTN.Text = Convert.ToString(dtBuyer.Rows[0]["PanNo"]);
        }


    }
    public string NumberToText(int number)
    {
        if (number == 0) return "Zero";

        if (number == -2147483648) return "Minus Two Hundred and Fourteen Crore Seventy Four Lakh Eighty Three Thousand Six Hundred and Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        if (number < 0)
        {
            sb.Append("Minus ");
            number = -number;
        }

        string[] words0 = {"" ,"One ", "Two ", "Three ", "Four ", 
                "Five " ,"Six ", "Seven ", "Eight ", "Nine "};

        string[] words1 = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", 
                "Fifteen ","Sixteen ","Seventeen ","Eighteen ", "Nineteen "};

        string[] words2 = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", 
                "Seventy ","Eighty ", "Ninety "};

        string[] words3 = { "Thousand ", "Lakh ", "Crore " };

        num[0] = number % 1000; // units 
        num[1] = number / 1000;
        num[2] = number / 100000;
        num[1] = num[1] - 100 * num[2]; // thousands 
        num[3] = number / 10000000; // crores 
        num[2] = num[2] - 100 * num[3]; // lakhs 

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }


        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;

            u = num[i] % 10; // ones 
            t = num[i] / 10;
            h = num[i] / 100; // hundreds 
            t = t - 10 * h; // tens 

            if (h > 0) sb.Append(words0[h] + "Hundred ");

            if (u > 0 || t > 0)
            {
                if (h > 0 || i == 0) sb.Append("and ");

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);
            }

            if (i != 0) sb.Append(words3[i - 1]);

        }
        return sb.ToString().TrimEnd();
    } 
}
