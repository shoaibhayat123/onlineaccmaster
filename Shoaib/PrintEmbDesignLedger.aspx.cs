﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintEmbDesignLedger : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();
    AccFiles objFile = new AccFiles();
    Reports objReports = new Reports();
    decimal TotalBalance = 0;
    decimal Totalqtyin, TotalNetWeight = 0;
    decimal TotalqtyOut = 0;  
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if ((Sessions.CustomerCode == "") || (Request["From"].ToString() == "") || (Request["To"].ToString() == "") || (Request["DesignNo"].ToString() == ""))
        {
            Response.Redirect("default.aspx");
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {


            bindSupplier();
            bindStock();
            lblFromDate.Text = ClsGetDate.FillFromDate(Request["From"].ToString());
            lblToDate.Text = ClsGetDate.FillFromDate(Request["To"].ToString());
            lblBuyerName.Text = Convert.ToString(Request["DesignNo"]);
            FillLogo();
            lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
            lblUser.Text = Sessions.UseLoginName.ToString();

        }
    }  


    #region Bind ledger grid
    protected void bindStock()
    {


        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.DateFrom = Convert.ToDateTime(Request["From"]);
        objReports.DateTo = Convert.ToDateTime(Request["To"]);
        objReports.DesignNo = Convert.ToString(Request["DesignNo"]);
        DataSet ds = new DataSet();
        ds = objReports.getDesignLedger();

        

        ///********** Add new row to result for Opening Balance**********/
        DataRow dr = ds.Tables[0].NewRow(); //table 2 for detail trans
        dr["Client"] = "Opening Balance";
        int BfInPcs = 0;
        int BfOutPcs = 0;
        if (Convert.ToString(ds.Tables[1].Rows[0]["BfInPcs"]) != "")
        {
            BfInPcs = Convert.ToInt32(ds.Tables[1].Rows[0]["BfInPcs"]);
        }
        if (Convert.ToString(ds.Tables[2].Rows[0]["BfOutPcs"]) != "")
        {
            BfOutPcs = Convert.ToInt32(ds.Tables[2].Rows[0]["BfOutPcs"]);
        }

        int OpeningBalAmount = BfInPcs - BfOutPcs;
        if (OpeningBalAmount > 0)
        {
            dr["Entry"] = "PROD";
            dr["Pcs"] = OpeningBalAmount;

        }
        else
        {
            dr["Entry"] = "DC";
            dr["Pcs"] = OpeningBalAmount;
          
        }
        ds.Tables[0].Rows.InsertAt(dr, 0);


        grdLedger.DataSource = ds.Tables[0];
        grdLedger.DataBind();

    }

    #endregion
    #region Grid RowDataBound
    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            Label lbltransDate = ((Label)e.Row.FindControl("lbltransDate"));
            Label lblChqDate = ((Label)e.Row.FindControl("lblChqDate"));
            Label lblBalance = ((Label)e.Row.FindControl("lblBalance"));
            Label lblqtyin = ((Label)e.Row.FindControl("lblqtyin"));
            Label lblqtyout = ((Label)e.Row.FindControl("lblqtyout"));
            Label lblClient = ((Label)e.Row.FindControl("lblClient"));
            Label lblEntry = ((Label)e.Row.FindControl("lblEntry"));
            Label lblProduction = ((Label)e.Row.FindControl("lblProduction"));
            Label lblDelivered = ((Label)e.Row.FindControl("lblDelivered"));
            
            if (e.Row.DataItemIndex != 0)
            {
                lblClient.Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Client"));
                lblEntry.Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Entry"));
                if (DataBinder.Eval(e.Row.DataItem, "Entry").ToString() == "PROD")
                {
                    lblProduction.Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Pcs"));
                }
                if (DataBinder.Eval(e.Row.DataItem, "Entry").ToString() == "DC")
                {
                    lblDelivered.Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Pcs"));
                }
            }
            else
            {
                lblEntry.Text = "B/F Bal.";
            }

                
           
            if (DataBinder.Eval(e.Row.DataItem, "ReportDate").ToString() != "")
            {
                lbltransDate.Text = ClsGetDate.FillFromDate(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "ReportDate")));
            }
            lblStitches.Text = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Stitches"));

            decimal qtyin = 0;
            decimal qtyout = 0;
            
            if (DataBinder.Eval(e.Row.DataItem, "Entry").ToString() == "PROD")
            {
                qtyin = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Pcs"));
                if (e.Row.DataItemIndex != 0)
                {
                    Totalqtyin += qtyin;
                }               
                TotalBalance = TotalBalance + qtyin;                 
            }
            if (DataBinder.Eval(e.Row.DataItem, "Entry").ToString() == "DC")
            {
                qtyout = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Pcs"));
                if (e.Row.DataItemIndex != 0)
                {
                    TotalqtyOut += qtyout;
                }               
                TotalBalance = TotalBalance - qtyout;
            }
                      
            lblBalance.Text = Convert.ToString(TotalBalance);
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "NetWeight")) != "")
            {
                TotalNetWeight += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "NetWeight"));
            }
        }
        

        if (e.Row.RowType == DataControlRowType.Footer)  // For footer totals
        {

            Label lblTotalNetWeight = (Label)e.Row.FindControl("lblTotalNetWeight");
            lblTotalNetWeight.Text = String.Format("{0:C}", TotalNetWeight).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblTotalProduction = (Label)e.Row.FindControl("lblTotalProduction");
            lblTotalProduction.Text = Convert.ToString(Totalqtyin);

            Label lblTotalDelivered = (Label)e.Row.FindControl("lblTotalDelivered");
            lblTotalDelivered.Text = Convert.ToString(TotalqtyOut);
        }

    }
    #endregion

    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }


    }
    #endregion
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }

            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
}