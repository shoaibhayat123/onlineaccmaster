﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="BoutiqueUserSetup.aspx.cs" Inherits="BoutiqueUserSetup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="User Setup"></asp:Label>
     <script type="text/javascript">
         Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);

         function BeginRequestHandler(sender, args) {

             if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Save") {

                 document.getElementById('<%= btn_save.ClientID %>').value = "Saving...";
                 args.get_postBackElement().disabled = true;
             }


         }
         function SaveClick() {
             document.getElementById('<%= hdnButtonText.ClientID %>').value = "Save";
         }

        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
         <asp:HiddenField ID="hdnButtonText" runat="server" />
            <table cellspacing="0" style="float: left" border="0" cellpadding="0" width="100%">
                <tr>
                    <td width="39%" valign="top" align="left" style="text-align: left; float: left">
                        <table border="0" cellpadding="5" cellspacing="5">
                            <tr>
                                <td width="100%" valign="top" style="left: 5; padding-top: 10px;">
                                    <table style="margin-left: 10px;">
                                        <tr>
                                            <td align="left">
                                                Designer Name <span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtDesigner" onChange="capitalizeMe(this)" CssClass="input_box2"
                                                    MaxLength="100" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDesigner"
                                                    Display="None" ErrorMessage="Please fill Designer Name" SetFocusOnError="True"
                                                    ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Contact No. <span style="color: Red;">*</span>:
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtContactNo" onChange="capitalizeMe(this)" CssClass="input_box2"
                                                    MaxLength="20" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtContactNo"
                                                    Display="None" ErrorMessage="Please fill Contact No." SetFocusOnError="True"
                                                    ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Mobile No. <span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtMobileNo" onChange="capitalizeMe(this)" CssClass="input_box2"
                                                    MaxLength="20" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMobileNo"
                                                    Display="None" ErrorMessage="Please fill Mobile No." SetFocusOnError="True" ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Email Address <span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtEmail" CssClass="input_box2" MaxLength="100" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtEmail"
                                                    Display="None" ErrorMessage="Please fill  Email Address " SetFocusOnError="True"
                                                    ValidationGroup="v">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail"
                                                    Display="None" ErrorMessage="Please Enter Valid Email Address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ValidationGroup="AddLadger"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Country <span style="color: Red;">*</span>:
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtCountry" onChange="capitalizeMe(this)" CssClass="input_box2"
                                                    MaxLength="20" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtCountry"
                                                    Display="None" ErrorMessage="Please fill Country" SetFocusOnError="True" ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                City <span style="color: Red;">*</span>:
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtCity" onChange="capitalizeMe(this)" CssClass="input_box2" MaxLength="50"
                                                    runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtCity"
                                                    Display="None" ErrorMessage="Please fill City" SetFocusOnError="True" ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Address :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtAddress" CssClass="input_box2" MaxLength="250" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Brand<span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlBrand" Style="width: 282px;" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlBrand"
                                                    InitialValue="0" Display="None" ErrorMessage="Please select Brand name" SetFocusOnError="True"
                                                    ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                User Name <span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtUser" CssClass="input_box2" MaxLength="50" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtUser"
                                                    Display="None" ErrorMessage="Please fill User Name" SetFocusOnError="True" ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Password <span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtPassword" CssClass="input_box2" MaxLength="50" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPassword"
                                                    Display="None" ErrorMessage="Please fill Password." SetFocusOnError="True" ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                <asp:HiddenField runat="server" ID="hdnItemId" />
                                                <asp:Button ID="btn_save" OnClientClick="SaveClick()" runat="server" Text="Save" OnClick="btn_save_Click" ValidationGroup="v" />
                                                <asp:Button ID="btn_cancel" runat="server" Text="Cancel" OnClick="btn_cancel_Click"
                                                    ValidationGroup="v" CausesValidation="false" />
                                                <br />
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                                    ShowSummary="False" ValidationGroup="v" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                            <br /><br />
                                                Note: Privde this URL to your customer
                                                <br />
                                                <a href="https://www.online.accmaster.com/BoutiqueLogin.aspx" target="_blank"  > https://www.online.accmaster.com/BoutiqueLogin.aspx </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" style="text-align: left; float: left" width="40%" align="left">
                        <div style="text-align: center; width: 600px; height: 510px; overflow: auto; margin: 0;
                            padding-top: 20px">
                            <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="No record found." Visible="false"></asp:Label>
                            <asp:DataGrid ID="grdCustomer" Width="100%" runat="server" AutoGenerateColumns="False"
                                AllowPaging="false" PageSize="15" PagerSettings-Mode="Numeric" OnPageIndexChanged="grdCustomer_PageIndexChanged"
                                OnItemCommand="grdCustomer_RowCommand" HeaderStyle-CssClass="gridheader" AlternatingItemStyle-CssClass="gridAlternateItem"
                                GridLines="none" BorderColor="black" PagerStyle-Mode="NumericPages" BorderStyle="Dotted"
                                BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="grdCustomer_RowDataBound">
                                <Columns>
                                    <asp:TemplateColumn HeaderText="S.No.">
                                        <ItemStyle Width="5%" />
                                        <ItemTemplate>
                                            <%# Container.DataSetIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="DesignerName">
                                        <ItemStyle Width="20%" HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <%# Eval("DesignerName")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="City">
                                        <ItemStyle Width="15%" HorizontalAlign="left" />
                                        <ItemTemplate>
                                            <%# Eval("City")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="User Name">
                                        <ItemStyle Width="20%" HorizontalAlign="Left" CssClass="gridpadding" />
                                        <ItemTemplate>
                                            <%# Eval("UserName")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Edit">
                                        <ItemStyle Width="10%" />
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lbEdit" CommandArgument='<%#Eval("BoutiqueUserId") %>'
                                                CommandName="Edit" Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Delete">
                                        <ItemStyle Width="10%" />
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lbDelete" OnClientClick="return askDelete();"
                                                CommandArgument='<%#Eval("BoutiqueUserId") %>' CommandName="Delete" Text="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
