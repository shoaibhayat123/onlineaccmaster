﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class Manage_AddMenuSubCat : System.Web.UI.Page
{
    SetCustomers objCust = new SetCustomers();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillMainCategory();
            FillSubCategory();
          
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {


        if (hdnSubCatid.Value != null && hdnSubCatid.Value != string.Empty)
        {
            objCust.SubCatid = int.Parse(hdnSubCatid.Value);
        }
        else
        {
            objCust.SubCatid = 0;
        }
        objCust.SubCatTitle = txtTitle.Text;

        objCust.Position = Convert.ToInt32(txtPosition.Text);
        objCust.PageName = Convert.ToString(txtPageName.Text);
        objCust.CatId = Convert.ToInt32(ddlMainCategory.SelectedValue);
        objCust.AddEditMenuSubCategory();

        if (objCust.SubCatid != 0)
        {
            Page.RegisterStartupScript("Msg1", "<script>alert('Sub Category has been updated.');  </script>");
        }
        else
        {
            Page.RegisterStartupScript("Msg1", "<script>alert('Sub Category has been added.');  </script>");
        }
        clear();
        FillSubCategory();

    }

    private void clear()
    {

        ddlMainCategory.SelectedValue = "0";
        txtPosition.Text = string.Empty;
        txtTitle.Text = string.Empty;
        txtPageName.Text = string.Empty;
        hdnSubCatid.Value = "";

    }

    public void FillSubCategoryDetail(int SubCatid)
    {

        objCust.CatId = 0;
        objCust.SubCatid = SubCatid;
        DataTable dt = new DataTable();
        dt = objCust.GetMenuSubCategory();
        if (dt.Rows.Count > 0)
        {
            ddlMainCategory.SelectedValue = dt.Rows[0]["CatId"].ToString();
            txtTitle.Text = dt.Rows[0]["SubCatTitle"].ToString();
            txtPosition.Text = dt.Rows[0]["Position"].ToString();
            txtPageName.Text = dt.Rows[0]["PageName"].ToString();
            hdnSubCatid.Value = SubCatid.ToString();
        }

    }

    public void FillSubCategory()
    {
        objCust.CatId =Convert.ToInt32(ddlMainCategory.SelectedValue);
        objCust.SubCatid = 0;
        DataTable dt = new DataTable();
        dt = objCust.GetMenuSubCategory();
        if (dt.Rows.Count > 0)
        {
            grdIndustrial.DataSource = dt;
            grdIndustrial.DataBind();

        }

    }
    public void FillMainCategory()
    {
        objCust.CatId = 0;
        DataTable dt = new DataTable();
        dt = objCust.GetMenuMainCategory();
        if (dt.Rows.Count > 0)
        {
            DataView dv = new DataView(dt);
            dv.Sort = "Title";
            ddlMainCategory.DataSource = dt;
            ddlMainCategory.DataTextField = "Title";
            ddlMainCategory.DataValueField = "CatId";
            ddlMainCategory.DataBind();
            ddlMainCategory.Items.Insert(0, new ListItem("Select Main Category", "0"));

        }

    }


    #region GridView Events
    protected void grdIndustrial_RowCommand(object sender, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            FillSubCategoryDetail(int.Parse(e.CommandArgument.ToString()));

        }
    }
    protected void grdIndustrial_RowDataBound(object sender, DataGridItemEventArgs e)
    {


    }

    protected void grdIndustrial_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        grdIndustrial.CurrentPageIndex = e.NewPageIndex;
        FillSubCategory();

    }
    #endregion
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        clear();
    }
}