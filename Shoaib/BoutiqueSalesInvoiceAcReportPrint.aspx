﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BoutiqueSalesInvoiceAcReportPrint.aspx.cs"
    Inherits="BoutiqueSalesInvoiceAcReportPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Tahoma; background: none; font-size: 10px;">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; font-weight: bold;">
            <tr>
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Sales
                        Invoice Report<br />
                    </span><span style="font-size: 14px;">From :
                        <asp:Label ID="lblFromDate" runat="server" Style="padding-right: 15px;"></asp:Label>
                        To :
                        <asp:Label ID="lblToDate" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblBuyerName" Style="display: none" runat="server"></asp:Label>
                    <span style="font-size: 12px; display: none; font-weight: normal">
                        <asp:Label ID="lblBuyerAddress" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress2" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress3" runat="server"></asp:Label></span> <span>Brand :
                            <asp:Label ID="lblBrand" runat="server"></asp:Label><br />
                            Label :
                            <asp:Label ID="lblLabel" runat="server"></asp:Label><br />
                            Item :
                            <asp:Label ID="lblItem" runat="server"></asp:Label>
                        </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <hr />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
            AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" FooterStyle-CssClass="gridFooter"
            HeaderStyle-CssClass="gridheader" ShowFooter="true" AlternatingItemStyle-CssClass="gridAlternateItem"
            GridLines="none" BorderColor="black" OnItemDataBound="dgGallery_OnItemDataBound"
            BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem">
            <Columns>
                <asp:TemplateColumn HeaderText="S.No.">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                    <ItemTemplate>
                        <%# Container.DataSetIndex + 1%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                     <asp:TemplateColumn HeaderText="TYPE">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="8%" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblType" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Inv. No.">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                    <ItemTemplate>
                        <%# Eval("InvNo")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Inv. Date">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="10%" />
                    <ItemTemplate>
                        <asp:Label ID="lblInvDate" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Item Code">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="10%" />
                    <ItemTemplate>
                        <%# Eval("BoutiqueItemCode")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Item Description">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle />
                    <ItemTemplate>
                        <%# Eval("ItemDesc") %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Terms">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="10%" />
                    <ItemTemplate>
                        <%# Eval("POSSaleType")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Quantity">
                    <HeaderStyle HorizontalAlign="Center" />
                    <FooterStyle HorizontalAlign="Center" />
                    <ItemStyle Width="6%" HorizontalAlign="Center" />
                    <ItemTemplate>
                        <asp:Label ID="lblQuantity" runat="server"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalQuantity" runat="server"></asp:Label></FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn Visible="false" HeaderText="Purchase Rate">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="lblPurchaseRate" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn Visible="false" HeaderText="Purchase Amount">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="lblPurchaseOldAmount" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn Visible="false" HeaderText="Charges" HeaderStyle-Wrap="true">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="lblAlterCharges" runat="server"></asp:Label>
                        <%--   <%#String.Format("{0:C}", Eval("BoutiqueConvRate")).Replace('$', ' ')%>--%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn Visible="false" HeaderText="Discount %">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="7%" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="lblBoutiqueDiscount" runat="server"></asp:Label>
                        <%--   <%#String.Format("{0:C}", Eval("BoutiqueDiscount")).Replace('$', ' ')%>--%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn Visible="false" HeaderText="Purchase Amount">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                    <ItemTemplate>
                        <asp:Label ID="lblPurchaseAmount" runat="server"></asp:Label>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalPurchaseAmount" runat="server"></asp:Label></FooterTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </div>
    <div style="text-align: center; font-size: 14px; font-weight: bold; width: 800px;
        float: left">
        **Please contact our shop / office to reconsile accounts.</div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>
                                <script type="text/javascript">

                                    var currentTime = new Date()
                                    var hours = currentTime.getHours()
                                    var Newhours = 0;
                                    var minutes = currentTime.getMinutes()
                                    if (minutes < 10) {
                                        minutes = "0" + minutes
                                    }
                                    if (hours > 12) {
                                        Newhours = hours - 12;
                                    }
                                    else {
                                        Newhours = hours;
                                    }
                                    document.write(Newhours + ":" + minutes + " ")
                                    if (hours > 11) {
                                        document.write("PM")
                                    } else {
                                        document.write("AM")
                                    }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
