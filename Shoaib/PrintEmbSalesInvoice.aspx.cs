﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintEmbSalesInvoice : System.Web.UI.Page
{
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    User objUser = new User();
    SetCustomers objCust = new SetCustomers();
    Car objCar = new Car();
    int Last = 0;
    int Corrent = 0;
    int FirstRecord = 0;
    int LastRecord = 0;
    decimal TotalYard, TotalPcs, TotalAmount;

    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (!IsPostBack)
        {
            if (Request["CustomerCode"] != null)
            {
                Sessions.CustomerCode = Request["CustomerCode"].ToString();
            }
            if (Request["InvoiceSummaryId"] != null)
            {
                if (Sessions.CustomerCode != null && Convert.ToString(Sessions.CustomerCode) != "")
                {

                                          
                        BindItenDesc();                     
                        FillLogo();
                   
                    lblUser.Text = Sessions.UseLoginName.ToString();

                }
                else
                {
                    Response.Redirect("default.aspx");
                }

            }


        }


    }
    protected void BindItenDesc()
    {
        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCar.Id = Convert.ToInt32(Request["InvoiceSummaryId"]);
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        ds = objCar.GetEmbSales();
        dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            lblCustomerName.Text = Convert.ToString(dt.Rows[0]["Client"]);
            lblQuality.Text = Convert.ToString(dt.Rows[0]["Quality"]);
            lblDc.Text = Convert.ToString(dt.Rows[0]["InvoiceNo"]);
            lblInvoiceNo.Text = Convert.ToString(dt.Rows[0]["InvoiceNo"]);
            lblDate.Text = ClsGetDate.FillFromDate(Convert.ToString(dt.Rows[0]["SalesDate"])); 
            lblCalc.Text = "";
            lblDescription.Text = Convert.ToString(dt.Rows[0]["Description"]);

            lblAdd1Desc.Text = Convert.ToString(dt.Rows[0]["AddRemark1"]);
            lblAdd1Amount.Text = Convert.ToString(dt.Rows[0]["AddAmount1"]);

            lblAdd2Desc.Text = Convert.ToString(dt.Rows[0]["AddRemark2"]);
            lblAdd2Amount.Text = Convert.ToString(dt.Rows[0]["AddAmount2"]);

            lblLess1.Text = Convert.ToString(dt.Rows[0]["LessAmount1"]);
            lblLess1Desc.Text = Convert.ToString(dt.Rows[0]["LessRemark1"]);

            lblLess2.Text = Convert.ToString(dt.Rows[0]["LessAmount2"]);
            lblLess2Desc.Text = Convert.ToString(dt.Rows[0]["LessRemark2"]);
            lblNetAmount.Text = Convert.ToString(dt.Rows[0]["InvoiceAmount"]);


            string[] num = Convert.ToDecimal(dt.Rows[0]["InvoiceAmount"]).ToString().Split('.');
            lblTotalEng.Text = NumberToText(Convert.ToInt32(num[0])) + " Only.";
            if (Convert.ToInt32(num[1]) > 0)
            {
                lblTotalEng.Text = NumberToText(Convert.ToInt32(num[0])) + " AND " + Convert.ToString(num[1]) + "/100 Only.";
            }
            DataSet ds3 = new DataSet();
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            ds3 = objCust.GetCurrency();
            if (ds3.Tables[0].Rows.Count > 0)
            {
                lblCurrency.Text = ds3.Tables[0].Rows[0]["CurrencyDesc"].ToString();
            }

                   }

        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCar.EmbSalesMainId = Convert.ToInt32(Request["InvoiceSummaryId"]); 
        DataSet ds1 = objCar.GetEmbSalesDetail();
        DataTable dtInvoice = new DataTable();
        dtInvoice = ds1.Tables[0];
        if (dtInvoice.Rows.Count > 0)
        {
            dgGallery.DataSource = dtInvoice;
            dgGallery.DataBind();
            dgGallery.Visible = true;

        }      
               
    }
       
    public string NumberToText(int number)
    {
        if (number == 0) return "Zero";

        if (number == -2147483648) return "Minus Two Hundred and Fourteen Crore Seventy Four Lakh Eighty Three Thousand Six Hundred and Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        if (number < 0)
        {
            sb.Append("Minus ");
            number = -number;
        }

        string[] words0 = {"" ,"One ", "Two ", "Three ", "Four ", 
                "Five " ,"Six ", "Seven ", "Eight ", "Nine "};

        string[] words1 = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", 
                "Fifteen ","Sixteen ","Seventeen ","Eighteen ", "Nineteen "};

        string[] words2 = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", 
                "Seventy ","Eighty ", "Ninety "};

        string[] words3 = { "Thousand ", "Lakh ", "Crore " };

        num[0] = number % 1000; // units 
        num[1] = number / 1000;
        num[2] = number / 100000;
        num[1] = num[1] - 100 * num[2]; // thousands 
        num[3] = number / 10000000; // crores 
        num[2] = num[2] - 100 * num[3]; // lakhs 

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }


        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;

            u = num[i] % 10; // ones 
            t = num[i] / 10;
            h = num[i] / 100; // hundreds 
            t = t - 10 * h; // tens 

            if (h > 0) sb.Append(words0[h] + "Hundred ");

            if (u > 0 || t > 0)
            {
                if (h > 0 || i == 0) sb.Append("and ");

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);
            }

            if (i != 0) sb.Append(words3[i - 1]);

        }
        return sb.ToString().TrimEnd();
    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "http://online.accmaster.com/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }


    }
    #endregion
    #region Grid Event

    protected void dgGallery_RowDataBound(object sender, DataGridItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            decimal YardRate = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "YardRate"));
            decimal yard = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "yard"));
            decimal Amount = YardRate * yard;
            ((Label)e.Item.FindControl("lblAmount")).Text = Amount.ToString("0.00");
            TotalYard += yard;
            TotalAmount += Convert.ToDecimal(Amount.ToString("0.00"));
            TotalPcs += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Pcs"));

        }
        if (e.Item.ItemType == ListItemType.Footer)
        {

            lblCalc.Text = (TotalYard / TotalPcs).ToString("0.00");
         ((Label)e.Item.FindControl("lblTotalPcs")).Text    = Convert.ToString(TotalPcs);
           ((Label)e.Item.FindControl("lblTotalYard")).Text     = Convert.ToString(TotalYard);
             ((Label)e.Item.FindControl("lblAmountOfGrid")).Text   = Convert.ToString(TotalAmount);
        }
    }

    #endregion
}
