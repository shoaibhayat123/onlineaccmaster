﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;



public partial class PrintHambalProductPricelist : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();
    AccFiles objFile = new AccFiles();
    Reports objReports = new Reports();
    decimal TotalBalance = 0;
    decimal TotalDebit = 0;
    decimal TotalCredit = 0;
    //public string attachment = "attachment; filename=Dresses27_Report_" + DateTime.Now.ToString("dd_MM_yyyy") + ".xls";
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Request["CustomerCode"] != null)
        {
            Sessions.CustomerCode = Request["CustomerCode"].ToString();
        }
        if ((Sessions.CustomerCode == "") || (Request["CategoryId"].ToString() == ""))
        {
            Response.Redirect("default.aspx");
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {


            bindSupplier();
            FillMainCategory();       
            FillLogo();
            lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
            lblUser.Text = Sessions.UseLoginName.ToString();
            lblDate.Text = System.DateTime.Now.ToString("dd/MMM/yyyy");

        }
    }

   


    #region Bind ledger grid
    

    #region Fill Main Category  Setup
    protected void FillMainCategory()
    {

        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.CategoryId = Convert.ToInt32(Request["CategoryId"]);
        DataSet ds = new DataSet();
        ds = objCust.GetMainCategory();       
        if (ds.Tables[0].Rows.Count > 0)
        {
            grdMainCategory.DataSource = ds.Tables[0];
            grdMainCategory.DataBind();
        }


    }
    #endregion

    #endregion
    #region Grid RowDataBound
    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        

    }

    protected void grdMainCategory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnMainCategoryCode = ((HiddenField)e.Row.FindControl("hdnMainCategoryCode"));

            objCust.CategoryId = Convert.ToInt32(Request["CategoryId"]);
            objCust.MainCategoryCode = Convert.ToInt32(hdnMainCategoryCode.Value);
            DataSet ds = new DataSet();
            ds = objCust.GetHambalSubCategory();
            GridView grdSubCategory = (GridView)e.Row.FindControl("grdSubCategory");
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdSubCategory.DataSource = ds.Tables[0];
                grdSubCategory.DataBind();
            }

        }


    }

    protected void grdSubCategory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            GridView grdLedger = (GridView)e.Row.FindControl("grdLedger");
            HiddenField hdnMainCategoryCode2 = ((HiddenField)e.Row.FindControl("hdnMainCategoryCode2"));
            HiddenField hdnSubCategoryCode = ((HiddenField)e.Row.FindControl("hdnSubCategoryCode"));
            objReports.MainCategoryCode = Convert.ToInt32(hdnMainCategoryCode2.Value);
            objReports.SubCategoryCode = Convert.ToInt32(hdnSubCategoryCode.Value);
            objReports.CategoryId = Convert.ToInt32(Request["CategoryId"]);
            DataSet ds = new DataSet();
            ds = objReports.getHambalItemDetail();
            grdLedger.DataSource = ds.Tables[0];
            grdLedger.DataBind();
        }
    }
    #endregion

    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "http://online.accmaster.com/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }

    }
    #endregion
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
}