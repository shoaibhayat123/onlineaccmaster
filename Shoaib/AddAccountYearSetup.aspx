﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="AddAccountYearSetup.aspx.cs" Inherits="AddAccountYearSetup" %>

<%@ Register Assembly="RJS.Web.WebControl.PopCalendar.Net.2008" Namespace="RJS.Web.WebControl"
    TagPrefix="rjs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Accounting Year Setup"></asp:Label>

      <script type="text/javascript">
          Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);

          function BeginRequestHandler(sender, args) 
          {

              if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Save") 
              {

                  document.getElementById('<%= btn_save.ClientID %>').value = "Saving...";
                  args.get_postBackElement().disabled = true;
              }
          }
          function SaveClick() {
              document.getElementById('<%= hdnButtonText.ClientID %>').value = "Save";

          }

         
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
         <asp:HiddenField ID="hdnButtonText" runat="server" />
            <table cellspacing="0" border="0" cellpadding="0" style="float: left" width="100%">
                <tr>
                    <td width="40%">
                        <table border="0" cellpadding="5" cellspacing="5">
                            <tr>
                                <td width="100%" valign="top" style="left: 5; padding-top: 10px;">
                                    <table style="margin-left: 10px;">
                                        <tr>
                                            <td align="left">
                                                Date From<span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtFromDate" MaxLength="25" CssClass="input_box2" onclick="scwShow(scwID('ctl00_MainHome_txtFromDate'),this);"
                                                    runat="server" Width="250px"></asp:TextBox>
                                                <img src="images/Calendar.gif" style="vertical-align: top" title='Click Here' alt='Click Here'
                                                    onclick="scwShow(scwID('ctl00_MainHome_txtFromDate'),this);" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFromDate"
                                                    Display="None" ErrorMessage="Please enter From date" SetFocusOnError="True" ValidationGroup="v">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                                    ControlToValidate="txtFromDate" ValidationGroup="v" ErrorMessage="Invalid  Date From !!"
                                                    Display="None"></asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="height: 8px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Date To <span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtToDate" MaxLength="25" CssClass="input_box2" onclick="scwShow(scwID('ctl00_MainHome_txtToDate'),this);"
                                                    runat="server" Width="250px"></asp:TextBox>
                                                <img src="images/Calendar.gif" style="vertical-align: top" title='Click Here' alt='Click Here'
                                                    onclick="scwShow(scwID('ctl00_MainHome_txtToDate'),this);" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                                    ControlToValidate="txtToDate" ValidationGroup="v" ErrorMessage="Invalid Date To !!"
                                                    Display="None"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtToDate"
                                                    Display="None" ErrorMessage="Please enter To date" SetFocusOnError="True" ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <tr>
                                                <td align="left">
                                                    Active :
                                                </td>
                                                <td align="left">
                                                    <asp:CheckBox ID="chkActive" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:HiddenField runat="server" ID="hdnAccId" />
                                                    <asp:Button ID="btn_save" OnClientClick="SaveClick()" runat="server" Text="Save" OnClick="btn_save_Click" ValidationGroup="v" />
                                                    <asp:Button ID="btn_cancel" runat="server" Text="Cancel" OnClick="btn_cancel_Click"
                                                        ValidationGroup="v" CausesValidation="false" />
                                                    <br />
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                                        ShowSummary="False" ValidationGroup="v" />
                                                </td>
                                            </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="text-align: left; float: left" width="700px" align="left">
                        <div style="text-align: center; width: 600px; margin: 0; padding-top: 20px">
                            <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="No record found." Visible="false"></asp:Label>
                            <asp:DataGrid ID="grdCustomer" Width="100%" runat="server" AutoGenerateColumns="False"
                                AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" OnPageIndexChanged="grdCustomer_PageIndexChanged"
                                OnItemCommand="grdCustomer_RowCommand" HeaderStyle-CssClass="gridheader" AlternatingItemStyle-CssClass="gridAlternateItem"
                                GridLines="none" BorderColor="black" BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem"
                                OnItemDataBound="grdCustomer_RowDataBound">
                                <Columns>
                                    <asp:TemplateColumn HeaderText="S.No.">
                                        <ItemTemplate>
                                            <%# Container.DataSetIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Date From">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblFromDate"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Date To">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblToDate"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Status">
                                        <ItemTemplate>
                                         <img src='<%# Convert.ToBoolean(Eval("IsActive"))==true?"images/tick.gif" : "images/cross.gif" %>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lbEdit" CommandArgument='<%#Eval("AccId") %>'
                                                CommandName="Edit" Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lbDelete" CommandArgument='<%#Eval("AccId") %>'
                                                CommandName="Delete" Text="Delete" OnClientClick="return askDelete();"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
