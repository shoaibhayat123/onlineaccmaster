﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VoucherReportPrint.aspx.cs"
    Inherits="VoucherReportPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Tahoma; background: none; font-size: 10px;">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; font-weight: bold;">
           <tr id="trTrialBal" runat="server">
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Voucher
                        Report<br />
                    </span><span style="font-size: 14px;">From :
                        <asp:Label ID="lblFromDate" runat="server" Style="padding-right: 15px;"></asp:Label>
                        To :
                        <asp:Label ID="lblToDate" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblBuyerName" runat="server"></asp:Label><br />
                    <span style="font-size: 12px; font-weight: normal">
                        <asp:Label ID="lblBuyerAddress" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress2" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress3" runat="server"></asp:Label></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <hr />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <asp:Repeater ID="rptrvoucherReport" OnItemDataBound="rptrvoucherReport_OnItemDataBound"
            runat="server">
            <ItemTemplate>
                <asp:Label Style="font-weight: bold; font-size: 12px;" ID="lblVucherDate" runat="server"></asp:Label><br />
                <%--<asp:HiddenField ID="hdnVoucherId" runat="server" />--%>
                <asp:Repeater ID="rptrVoucherMain" OnItemDataBound="rptrVoucherMain_OnItemDataBound"
                    runat="server">
                    <ItemTemplate>
                        <%--  <asp:GridView ID="grdVoucherDetail" runat="server">
                        </asp:GridView>--%>
                        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                            AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" FooterStyle-CssClass="gridFooter"
                            HeaderStyle-CssClass="gridheader" ShowFooter="true" AlternatingItemStyle-CssClass="gridAlternateItem"
                            GridLines="none" BorderColor="black" OnItemDataBound="dgGallery_OnItemDataBound"
                            BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Voucher No">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemTemplate>
                                        <%# Eval("InvoiceNo")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="A/C Title">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="left" Width="20%" />
                                    <ItemTemplate>
                                        <%# Eval("AccountDesc")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Description">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="35%" HorizontalAlign="left" />
                                    <ItemTemplate>
                                        <%# Eval("Description")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Receipt #">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                                    <ItemTemplate>
                                        <%# Eval("ReceiptNo")%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Debit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Right" Width="15%" />
                                    <FooterStyle HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%# Convert.ToDecimal(Eval("TransAmt")) > 0 ? String.Format("{0:C}", Eval("TransAmt")).Replace('$', ' ') : "0.00"%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalDebit" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Credit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle HorizontalAlign="Right" />
                                    <ItemStyle HorizontalAlign="Right" Width="15%" />
                                    <ItemTemplate>
                                        <%# Convert.ToDecimal(Eval("TransAmt")) < 0 ? String.Format("{0:C}", Eval("TransAmt")).Replace('$', ' ').Replace('(',' ').Replace(')',' ') : "0.00"%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalCredit" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <br />
                    </ItemTemplate>
                </asp:Repeater>
                <table style="width: 800px">
                    <tr>
                        <td style="width: 70%; font-weight: bold; text-align: right">
                            Date Total :
                        </td>
                        <td style="width: 15%; font-weight: bold; text-align: right">
                            <asp:Label ID="lblDateDebit" runat="server"></asp:Label>
                        </td>
                        <td style="width: 15%; font-weight: bold; text-align: right">
                            <asp:Label ID="lblDateCredit" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
            </ItemTemplate>
        </asp:Repeater>
        <table style="width: 800px">
            <tr>
                <td style="width: 70%; font-weight: bold; text-align: right">
                    Grand Total :
                </td>
                <td style="width: 15%; font-weight: bold; text-align: right">
                    <asp:Label ID="lblvoucherDebit" runat="server"></asp:Label>
                </td>
                <td style="width: 15%; font-weight: bold; text-align: right">
                    <asp:Label ID="lblvoucherCredit" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
              <tr>
                <td style="text-align: right">
                    <asp:LinkButton Text="Print" ID="lnkPrint" OnClientClick="window.print();" runat="server"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>

                                  <script type="text/javascript">

                                            var currentTime = new Date()
                                            var hours = currentTime.getHours()
                                             var Newhours=0;
                                            var minutes = currentTime.getMinutes()
                                            if (minutes < 10)
                                            {
                                            minutes = "0" + minutes
                                            }                                           
                                            if(hours >12)
                                            {
                                            Newhours= hours-12;
                                            }
                                            else
                                            {
                                              Newhours= hours;
                                            }
                                            document.write(Newhours + ":" + minutes + " ")
                                             if(hours > 11)
                                            {                                           
                                            document.write("PM")
                                            } else 
                                            {
                                            document.write("AM")
                                            }

                                </script>

                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
