﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class AddCarImage : System.Web.UI.Page
{

    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();

    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    int Last = 0;
    int Corrent = 0;
    int FirstRecord = 0;
    int LastRecord = 0;
    Car objCar = new Car();
    #endregion
    # region PageLode
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        if (!IsPostBack)
        {
            ddlVIN.Focus();
            BindVIN();


            Session["s"] = null;
            Session["Corrent"] = null;
            Session["FirstRecord"] = null;
            Session["LastRecord"] = null;
            Session["Yarnbrokerage"] = null;

            DataTable dt = new DataTable();
            dgGallery.DataSource = dt;
            dgGallery.DataBind();

            if (Convert.ToString(Request["addnew"]) != "1")
            {

                bindpagging();
                if (Request["InvoiceSummaryId"] == null)
                {
                    BindInvoice();
                }


            }
            else
            {
                objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objCar.CarImageMainid = 0;
                DataSet ds = new DataSet();

                ds = objCar.getCarImages();
                dt = ds.Tables[0];


                if (dt.Rows.Count > 0)
                {

                    int Last = Convert.ToInt32(dt.Rows.Count) - 1;

                }
                else
                {
                }
                spanPgging.Style.Add("display", "none");
                btnDeleteInvoice.Style.Add("display", "none");

            }

        }
        SetUserRight();
    }

    # endregion
    # region VIN Index change
    protected void ddlVIN_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["s"] = null;
        BindInventoryData();
        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCar.CarImageMainid = 0;
        DataSet ds = new DataSet();

        ds = objCar.getCarImages();
        DataTable dt = new DataTable();
        dt = ds.Tables[0];
        Session["Yarnbrokerage"] = dt;
        Last = dt.Rows.Count;
        if (dt.Rows.Count > 0)
        {

            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            DataView dv = new DataView(dt);
            dv.RowFilter = "VinId=" + ddlVIN.SelectedValue;
            dt = dv.ToTable();

            if (dt.Rows.Count > 0)
            {
                hdnBrokerageId.Value = Convert.ToString(dt.Rows[0]["CarImageMainid"]);
                Session["Corrent"] = Convert.ToString(dt.Rows[0]["row"]);
                BindInvoice();

                btnDeleteInvoice.Style.Add("display", "");

            }
            else
            {

                DataTable dtBlank = new DataTable();
                dgGallery.DataSource = dtBlank;
                dgGallery.DataBind();

            }

        }

        rdImage.Focus();
    }
    #endregion
    # region BindInventory data on change of VIN
    protected void BindInventoryData()
    {
        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCar.PurchaseId = Convert.ToInt32(ddlVIN.SelectedValue);
        DataTable dt = new DataTable();
        dt = objCar.getInventoryDetails();
        if (dt.Rows.Count > 0)
        {
            tdInventory.Style.Add("display", "");
            lblLotNo.Text = Convert.ToString(dt.Rows[0]["CarLot"]);
            lblMake.Text = Convert.ToString(dt.Rows[0]["CarMakeName"]);
            lblModel.Text = Convert.ToString(dt.Rows[0]["CarModel"]);
        }
        else
        {
            tdInventory.Style.Add("display", "none");
        }
    }
    #endregion
    #region BindVIN
    protected void BindVIN()
    {
        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCar.PurchaseId = 0;
        DataTable dt = new DataTable();
        dt = objCar.GetCarPurchase();
        if (dt.Rows.Count > 0)
        {
            ddlVIN.DataSource = dt;
            ddlVIN.DataValueField = "PurchaseId";
            ddlVIN.DataTextField = "VIN";
            ddlVIN.DataBind();
            ddlVIN.Items.Insert(0, new ListItem("Select VIN", "0"));
        }
    }

    #endregion
    #region User Right
    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }

            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
            {
                btnDeleteInvoice.Visible = false;
            }
            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

                //btnPrintSales.Enabled = false;
                //btnPrintPurchase.Enabled = false;
            }

            if (Convert.ToString(dt.Rows[0]["IsAdd"]) != "True")
            {
                lblbutton.Enabled = false;
                if (Convert.ToString(Request["addnew"]) == "1")
                {

                    ImageButton2.Visible = false;
                }
            }



            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {
                if (Convert.ToString(Request["addnew"]) != "1")
                {

                    ImageButton2.Visible = false;
                }
            }

        }
    }
    #endregion
    #region Pagging
    private void bindpagging()
    {
        Last = 0;
        Corrent = 0;
        FirstRecord = 0;
        LastRecord = 0;

        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCar.CarImageMainid = 0;
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        ds = objCar.getCarImages();
        dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            Session["Yarnbrokerage"] = dt;
            Last = dt.Rows.Count;
            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            //Corrent = Convert.ToInt32(dt.Rows[0]["Row"]);
            Session["FirstRecord"] = FirstRecord;
            Session["LastRecord"] = LastRecord;
            Session["Corrent"] = LastRecord;


        }
        else
        {
            Response.Redirect("AddCarImage.aspx?addnew=1");

        }


    }
    protected void lnkFirst_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["FirstRecord"];
        BindInvoice();

    }
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["FirstRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) - 1;
            BindInvoice();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["LastRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) + 1;
            BindInvoice();
        }
    }
    protected void lnLast_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["LastRecord"];
        BindInvoice();
    }
    #endregion
    #region Cancel
    protected void btn_cancel_Click(object sender, ImageClickEventArgs e)
    {

        Response.Redirect("AddCarImage.aspx");



    }
    #endregion


    #region Fill invoice summary
    protected void BindInvoice()
    {
        if (Session["Yarnbrokerage"] != null)
        {
            DataTable dtYarnbrokerage = (DataTable)Session["Yarnbrokerage"];
            if (dtYarnbrokerage.Rows.Count > 0)
            {

                DataView DvYarnbrokerage = dtYarnbrokerage.DefaultView;
                DvYarnbrokerage.RowFilter = "Row = '" + Session["Corrent"].ToString() + "'";
                dtYarnbrokerage = DvYarnbrokerage.ToTable();
                if (dtYarnbrokerage.Rows.Count > 0)
                {
                    ddlVIN.SelectedValue = Convert.ToString(dtYarnbrokerage.Rows[0]["VinId"]);
                    hdnBrokerageId.Value = Convert.ToString(dtYarnbrokerage.Rows[0]["CarImageMainid"]);
                    BindInventoryData();

                    fillInvoiceDetails(Convert.ToInt32(dtYarnbrokerage.Rows[0]["CarImageMainid"]));

                }
            }

        }



    }

    #endregion
    #region Save
    protected void btn_saveInvoice_Click(object sender, ImageClickEventArgs e)
    {



        if (hdnBrokerageId.Value == "")
        {
            hdnBrokerageId.Value = "0";
        }
        /************ Add bROKERAGE **************/
        if (Convert.ToInt32(hdnBrokerageId.Value) > 0)
        {
            objCar.CarImageMainid = Convert.ToInt32(hdnBrokerageId.Value);
        }
        else
        {
            objCar.CarImageMainid = 0;
        }


        objCar.VinId = Convert.ToInt32(ddlVIN.SelectedValue);
        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);

        int result = objCar.AddEditCarImageMain();


        // add Details 

        if (result > 0)
        {
            if (Session["s"] != null)
            {
                DataTable dt = new DataTable();
                dt = (DataTable)Session["s"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objCar.CarImageId = 0;
                        objCar.VinId = Convert.ToInt32(ddlVIN.SelectedValue);
                        objCar.CarImage = Convert.ToString(dt.Rows[i]["CarImage"].ToString());
                        objCar.IsImage = Convert.ToBoolean(dt.Rows[i]["IsImage"].ToString());
                        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                        objCar.CarImageMainid = result;
                        int resultDetails = objCar.AddEditCarImageDetail();

                    }
                }
            }

        }

        if (Convert.ToInt32(hdnBrokerageId.Value) > 0)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Updated.');location.href='AddCarImage.aspx';", true);

        }

        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Saved.');location.href='AddCarImage.aspx';", true);
        }
    }
    #endregion
    #region ADD New
    protected void lblbutton_Click(object sender, EventArgs e)
    {


        Response.Redirect("AddCarImage.aspx?addnew=1");


    }
    #endregion
    #region delete
    protected void btnDeleteInvoice_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(hdnBrokerageId.Value) > 0)
        {
            int result = 0;
            objCar.CarImageMainid = Convert.ToInt32(hdnBrokerageId.Value);
            objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);

            result = objCar.DeleteCarImage();
            Session["Yarnbrokerage"] = null;
            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record deleted successfully.');location.href='AddCarImage.aspx';", true);
            }


        }
    }

    #endregion


    #region Add Image details
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        if (FuImage.HasFile == true)
        {
            if (FuImage.PostedFile.FileName != "")
            {
                string ImageName = UploadImage(FuImage);
                if (ImageName != "")
                {

                    filltable();


                    DataRow row;
                    if (dttbl.Rows.Count > 0)
                    {

                        int maxid = 0;
                        for (int i = 0; i < dttbl.Rows.Count; i++)
                        {
                            if (dttbl.Rows[i]["Id"].ToString() != "")
                            {
                                if (maxid < Convert.ToInt32(dttbl.Rows[i]["Id"].ToString()))
                                {
                                    maxid = Convert.ToInt32(dttbl.Rows[i]["Id"].ToString());
                                }
                            }
                        }

                        row = dttbl.NewRow();
                        row["Id"] = maxid + 1;
                        row["CarImage"] = ImageName;
                        row["IsImage"] = Convert.ToBoolean(rdImage.Checked);


                    }
                    else
                    {

                        row = dttbl.NewRow();
                        row["Id"] = 1;
                        row["CarImage"] = ImageName;
                        row["IsImage"] = Convert.ToBoolean(rdImage.Checked);


                    }
                    dttbl.Rows.Add(row);
                    dttbl.AcceptChanges();
                    Session.Add("s", dttbl);
                    dgGallery.DataSource = dttbl;
                    dgGallery.DataBind();

                    if (dttbl.Rows.Count > 0)
                    {
                        dgGallery.Visible = true;
                    }

                }
            }
        }

    }
    #endregion

    #region Fill session Table
    public void filltable()
    {
        if (Session["s"] == null)
        {
            if (dttbl.Rows.Count == 0)
            {

                DataColumn column = new DataColumn();


                column = new DataColumn();
                column.DataType = System.Type.GetType("System.Int32");
                column.ColumnName = "Id";
                column.Caption = "Id";
                column.ReadOnly = false;
                dttbl.Columns.Add(column);

                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "CarImage";
                column.Caption = "CarImage";
                column.ReadOnly = false;
                dttbl.Columns.Add(column);


                column = new DataColumn();
                column.DataType = System.Type.GetType("System.Boolean");
                column.ColumnName = "IsImage";
                column.Caption = "IsImage";
                column.ReadOnly = false;
                dttbl.Columns.Add(column);

            }


        }
        else
        {
            dttbl = (DataTable)Session["s"];
        }
    }
    #endregion

    #region Item Command Dggallery
    protected void dgGallery_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        dttbl = (DataTable)Session["s"];
        if (e.CommandName == "Delete")
        {
            int j = 0;
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    dttbl.Rows.RemoveAt(i);
                }
            }
            dttbl.AcceptChanges();
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            Session.Add("s", dttbl);


        }



    }
    #endregion
    #region Image Uploder
    private string UploadImage(FileUpload FileUpload1)
    {

        if (FileUpload1.HasFile == true)
        {
            if (FileUpload1.PostedFile.FileName != "")
            {
                string strImage = null;
                string FileName = string.Empty;
                string strExtension = "";

                string TempPath = "~/images/CarIamge/";
                strExtension = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                string Extension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                if (Extension == ".jpg" || Extension == ".jpeg" || Extension == ".png" || Extension == ".gif")
                {
                    string strImageName = System.Guid.NewGuid().ToString().Replace("-", "") + strExtension;
                    string StrImagePath = Server.MapPath("~/images/CarIamge/");
                    FileUpload1.PostedFile.SaveAs(Server.MapPath(TempPath + strImageName));
                    strImage = strImageName;
                    FileInfo fileInfo = new FileInfo(Server.MapPath("~/images/CarIamge/" + strImage));
                    double fileSizeKB = fileInfo.Length / 1024;
                    if (rdImage.Checked == true)
                    {
                        if (fileSizeKB <= 150)
                        {
                            return strImage;
                        }
                        else
                        {
                            Page.RegisterStartupScript("Msg1", "<script>alert('Please select file with less then Or Equal 150KB');</script>");
                          
                        }
                    }
                    else
                    {
                        if (fileSizeKB <= 500)
                        {
                            return strImage;
                        }
                        else
                        {
                            Page.RegisterStartupScript("Msg1", "<script>alert('Please select file with less then Or Equal 500KB');</script>");
      
                
                        }
                    }

                  
                  

                }
                else
                {
                    Page.RegisterStartupScript("Msg1", "<script>alert('Please select file with .jpg/.jpeg/.png/.gif extension');</script>");
                    return "";

                }
            }
        }
        else
        {
            Page.RegisterStartupScript("Msg1", "<script>alert('Please Upload Image');</script>");
        }
        return "";

    }
    #endregion
    #region fillImage Details
    protected void fillInvoiceDetails(int pid)
    {
        Session["s"] = null;

        DataTable dtInvoice = new DataTable();
        objCar.CarImageMainid = pid;
        objCar.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCar.getCarImagesDetail();
        dtInvoice = ds.Tables[0];

        if (dtInvoice.Rows.Count > 0)
        {
            for (int i = 0; i < dtInvoice.Rows.Count; i++)
            {

                filltable();
                DataRow row;
                row = dttbl.NewRow();
                row["Id"] = i + 1;
                row["CarImage"] = Convert.ToString(dtInvoice.Rows[i]["CarImage"]);
                row["IsImage"] = Convert.ToBoolean(dtInvoice.Rows[i]["IsImage"]);
                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
                Session.Add("s", dttbl);
            }
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            dgGallery.Visible = true;

        }
        else
        {
            dgGallery.Visible = false;
        }

    }
    #endregion
}