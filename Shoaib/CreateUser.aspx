﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="CreateUser.aspx.cs" Inherits="CreateUser" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphead" runat="Server">
    Add New User

    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);

        function BeginRequestHandler(sender, args) {

            if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Save") {

                document.getElementById('<%= btnAdd.ClientID %>').value = "Saving...";
                args.get_postBackElement().disabled = true;
            }
           

        }
        function SaveClick() 
        {
            document.getElementById('<%= hdnButtonText.ClientID %>').value = "Save";
        }

        
    </script>

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
        <asp:HiddenField ID="hdnButtonText" runat="server" />
            <table cellspacing="0" style="float: left" border="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <table cellpadding="5" style="float: left" cellspacing="5">
                            <tr id="tr1" runat="server" visible="false">
                                <td>
                                    Customer Code<span style="color: Red;">*</span> :
                                </td>
                                <td>
                                    <asp:TextBox CssClass="input_box2" ID="txtCustomerCode" Enabled="false" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr id="trUserId" runat="server">
                                <td>
                                    UserId<span style="color: Red;">*</span> :
                                </td>
                                <td>
                                    <asp:HiddenField ID="hdnUserId" runat="server" />
                                    <asp:TextBox CssClass="input_box2" ID="txtNewUserId" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator Text="*" Display="None" ID="RequiredFieldValidator4"
                                        runat="server" ControlToValidate="txtNewUserId" ErrorMessage="Please Enter Username"
                                        SetFocusOnError="True" ValidationGroup="Reg"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr id="trPassword" runat="server">
                                <td>
                                    Password<span style="color: Red;">*</span> :
                                </td>
                                <td>
                                    <asp:TextBox CssClass="input_box2" ID="txtNewPassword" TextMode="Password" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator Text="*" Display="None" ID="RequiredFieldValidator3"
                                        runat="server" ControlToValidate="txtNewPassword" ErrorMessage="Please Enter Password"
                                        SetFocusOnError="True" ValidationGroup="Reg"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;" colspan="2">
                                    User Rights :
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;" colspan="2">
                                    <asp:DataGrid ID="dgGallery" Width="600px" runat="server" AutoGenerateColumns="False"
                                        AllowPaging="false" PagerStyle-Visible="false" PageSize="50000" PagerSettings-Mode="Numeric"
                                        HeaderStyle-CssClass="gridheader" AlternatingItemStyle-CssClass="gridAlternateItem"
                                        GridLines="none" BorderColor="black" BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="S.No.">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Width="5%" HorizontalAlign="Center" BorderColor="Black" />
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Access">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkAccess" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Main Category">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Width="20%" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <%# Eval("Title")%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Menu">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Width="30%" HorizontalAlign="Left" />
                                                <ItemTemplate>
                                                    <%# Eval("SubCatTitle") %>
                                                    <asp:HiddenField ID="hdnSubCatid" Value='<%# Eval("SubCatid") %>' runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Add">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkAdd" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Edit">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkEdit" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Delete">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:CheckBox Checked="false" ID="chkDelete" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Print">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:CheckBox Checked="false" ID="chkPrint" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                    <%--   <asp:CheckBoxList RepeatDirection="Horizontal" RepeatColumns="1" ID="chkMenu" runat="server">
                                    </asp:CheckBoxList>--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td style=" text-align:right">
                                    <asp:Button ID="btnAdd" runat="server" OnClientClick="SaveClick()" Text="Save" ValidationGroup="Reg" OnClick="btnAdd_Click" />&nbsp;
                               
                                    <asp:ValidationSummary ID="vs" runat="server" ShowMessageBox="true" ShowSummary="false"
                                        ValidationGroup="Reg" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" style="text-align: left; float: left" width="700px" align="left">
                        <div style="text-align: center; width: 400px; height: 450px; overflow: auto; margin-bottom: 15px;
                            margin-left: 20px; padding-top: 10px">
                            <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="No record found." Visible="false"></asp:Label>
                            <asp:DataGrid ID="grdCustomer" Width="100%" runat="server" AutoGenerateColumns="False"
                                AllowPaging="false" PagerStyle-Visible="false" PageSize="50000" PagerSettings-Mode="Numeric"
                                OnPageIndexChanged="grdCustomer_PageIndexChanged" OnItemCommand="grdCustomer_RowCommand"
                                HeaderStyle-CssClass="gridheader" AlternatingItemStyle-CssClass="gridAlternateItem"
                                GridLines="none" BorderColor="black" BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem"
                                OnItemDataBound="grdCustomer_RowDataBound">
                                <Columns>
                                    <asp:TemplateColumn HeaderText="S.No.">
                                        <ItemStyle Width="25%" />
                                        <ItemTemplate>
                                            <%# Container.DataSetIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="User Name">
                                        <ItemStyle Width="35%" HorizontalAlign="Left" CssClass="gridpadding" />
                                        <ItemTemplate>
                                            <%# Eval("UserID")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Role">
                                        <ItemStyle Width="10%" HorizontalAlign="Left" CssClass="gridpadding" />
                                        <ItemTemplate>
                                            <%# Eval("UserRole")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Edit">
                                        <ItemStyle Width="15%" />
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" Style='<%# Convert.ToString(Eval("UserRole"))== "Admin" ? "display:none": "display:" %>'
                                                ID="lbEdit" CommandArgument='<%#Eval("ID") %>' CommandName="Edit" Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Delete">
                                        <ItemStyle Width="15%" />
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lbDelete" OnClientClick="return askDelete();"
                                                CommandArgument='<%#Eval("ID") %>' CommandName="Delete" Text="Delete" Style='<%# Convert.ToString(Eval("UserRole"))== "Admin" ? "display:none": "display:" %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <asp:Label ID="lblUser" runat="server"></asp:Label>
                        </div>
                    </td>
                </tr>

            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
