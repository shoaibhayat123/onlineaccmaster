﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintPurchaseVsPayment : System.Web.UI.Page
{

    Reports objreport = new Reports();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();
    DataTable dtSalesReportMonth = new DataTable();
    decimal TotalSales = 0;
    decimal TotalRecovery = 0;
    decimal TotalSalesPr = 0;
    decimal TotalRecoveryPr = 0;

    decimal TotalSalesMonth = 0;
    decimal TotalRecoveryMonth = 0;
    decimal TotalSalesPrMonth = 0;
    decimal TotalRecoveryPrMonth = 0;


    string AcFileId = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if ((Sessions.CustomerCode == "") || (Request["year"].ToString() == ""))
        {
            Response.Redirect("default.aspx");
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {
            bindSupplier();
            lblFromDate.Text = Request["year"].ToString();
            FillLogo();
            BindAgingResult();
            bindMonthlyReport();
            lblUser.Text = Sessions.UseLoginName.ToString();
            BindGraph();
        }
    }

    protected void BindAgingResult()
    {
        objreport.AcFileId = 14;
        objreport.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtLedger = new DataTable();
        dtLedger = objreport.getChildLedger();
        DataTable dtSalesReport = new DataTable();
        if (dtLedger.Rows.Count > 0)
        {
            for (int i = 0; i < dtLedger.Rows.Count; i++)
            {
                objreport.AcFileId = Convert.ToInt32(dtLedger.Rows[i]["id"]);

                AcFileId += Convert.ToString(dtLedger.Rows[i]["id"]) + ",";

                objreport.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
                objreport.year = Convert.ToInt32(Request["year"]);
                DataTable dtRusult = new DataTable();
                dtRusult = objreport.getPurchaseVSPayment();
                if (i == 0)
                {
                    dtSalesReport = dtRusult.Copy();
                }
                else
                {
                    dtSalesReport.Merge(dtRusult);
                }
            }
        }
        if (dtSalesReport.Rows.Count > 0)
        {
            if (Convert.ToString(dtSalesReport.Compute("Sum(TotalSalesAmt)", " ")) != "")
            {
                TotalSales = Convert.ToDecimal(dtSalesReport.Compute("Sum(TotalSalesAmt)", " "))* 1;
            }
            if (Convert.ToString(dtSalesReport.Compute("Sum(TotalRecoveryAmt)", " ")) != "")
            {
                TotalRecovery = Convert.ToDecimal(dtSalesReport.Compute("Sum(TotalRecoveryAmt)", " ")) * 1;
            }
            grdLedger.DataSource = dtSalesReport;
            grdLedger.DataBind();


        }
        else
        {
            grdLedger.DataBind();
        }

    }
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }

    }



    #endregion
    protected void dgGallery_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblAgeOfSale = ((Label)e.Row.FindControl("lblAgeOfSale"));
            Label lblAgeOfRecovery = ((Label)e.Row.FindControl("lblAgeOfRecovery"));

            Label lblSalesAmt = ((Label)e.Row.FindControl("lblSalesAmt"));
            Label lblRecoveryAmt = ((Label)e.Row.FindControl("lblRecoveryAmt"));


            decimal AgeOfSale = 0;
            decimal AgeOfRecovery = 0;
            decimal TotalSalesAmt = 0;
            decimal TotalRecoveryAmt = 0;
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TotalSalesAmt")) != "")
            {
                TotalSalesAmt = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TotalSalesAmt"));
            }
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TotalRecoveryAmt")) != "")
            {
                TotalRecoveryAmt = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TotalRecoveryAmt")) * 1;
            }
            lblSalesAmt.Text = String.Format("{0:C}", TotalSalesAmt).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblRecoveryAmt.Text = String.Format("{0:C}", TotalRecoveryAmt).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            if (TotalSales != 0)
            {
                AgeOfSale = (TotalSalesAmt / TotalSales) * 100;
            }
            if (TotalRecovery != 0)
            {
                AgeOfRecovery = (TotalRecoveryAmt / TotalRecovery) * 100;
            }
            TotalSalesPr += AgeOfSale;
            TotalRecoveryPr += AgeOfRecovery;
            lblAgeOfSale.Text = AgeOfSale.ToString("0.00") + "%";
            lblAgeOfRecovery.Text = AgeOfRecovery.ToString("0.00") + "%";
        }

        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblSumSales = ((Label)e.Row.FindControl("lblSumSales"));
            Label lblSumRecovery = ((Label)e.Row.FindControl("lblSumRecovery"));

            Label lblSumAgeOfSale = ((Label)e.Row.FindControl("lblSumAgeOfSale"));
            Label lblSumAgeOfRecovery = ((Label)e.Row.FindControl("lblSumAgeOfRecovery"));
            lblSumSales.Text = String.Format("{0:C}", TotalSales).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblSumRecovery.Text = String.Format("{0:C}", TotalRecovery).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            lblSumAgeOfSale.Text = TotalSalesPr.ToString("0.00") + "%";
            lblSumAgeOfRecovery.Text = TotalRecoveryPr.ToString("0.00") + "%";

        }
    }

    protected void bindMonthlyReport()
    {

        for (int i = 1; i <= 12; i++)
        {
            objreport.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objreport.year = Convert.ToInt32(Request["year"]);
            objreport.Month = i;
            objreport.AcFileIdText = AcFileId;

            DataTable dtRusult = new DataTable();
            dtRusult = objreport.getPurchaseVSPaymentYearly();
            if (dtRusult.Rows.Count > 0)
            {
                if (i == 1)
                {
                    dtSalesReportMonth = dtRusult.Copy();
                }
                else
                {
                    dtSalesReportMonth.Merge(dtRusult);
                }
            }
        }

        if (dtSalesReportMonth.Rows.Count > 0)
        {
            if (TotalSales != 0)
            {
                TotalSalesMonth = Convert.ToDecimal(dtSalesReportMonth.Compute("Sum(TotalSalesAmt)", " "));
            }
            if (TotalRecovery != 0)
            {
                TotalRecoveryMonth = Convert.ToDecimal(dtSalesReportMonth.Compute("Sum(TotalRecoveryAmt)", " ")) * 1;
            }

            if (TotalSalesMonth != 0 || TotalRecoveryMonth != 0)
            {
                grdMonth.DataSource = dtSalesReportMonth;
                grdMonth.DataBind();
            }
            else
            {
                grdMonth.DataBind();
            }

        }
    }


    protected void grdMonth_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblAgeOfSale = ((Label)e.Row.FindControl("lblAgeOfSale"));
            Label lblAgeOfRecovery = ((Label)e.Row.FindControl("lblAgeOfRecovery"));
            Label lblSalesAmt = ((Label)e.Row.FindControl("lblSalesAmt"));
            Label lblRecoveryAmt = ((Label)e.Row.FindControl("lblRecoveryAmt"));
            Label lblMonth = ((Label)e.Row.FindControl("lblMonth"));


            decimal AgeOfSale = 0;
            decimal AgeOfRecovery = 0;
            decimal TotalSalesAmt = 0;
            decimal TotalRecoveryAmt = 0;
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TotalSalesAmt")) != "")
            {
                TotalSalesAmt = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TotalSalesAmt"));
            }
            if (Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TotalRecoveryAmt")) != "")
            {
                TotalRecoveryAmt = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TotalRecoveryAmt")) * 1;
            }
            lblSalesAmt.Text = String.Format("{0:C}", TotalSalesAmt).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblRecoveryAmt.Text = String.Format("{0:C}", TotalRecoveryAmt).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            if (TotalSalesMonth != 0)
            {
                AgeOfSale = (TotalSalesAmt / TotalSalesMonth) * 100;
            }
            if (TotalRecoveryMonth != 0)
            {
                AgeOfRecovery = (TotalRecoveryAmt / TotalRecoveryMonth) * 100;
            }
            TotalSalesPrMonth += AgeOfSale;
            TotalRecoveryPrMonth += AgeOfRecovery;
            lblAgeOfSale.Text = AgeOfSale.ToString("0.00") + "%";
            lblAgeOfRecovery.Text = AgeOfRecovery.ToString("0.00") + "%";

            if (e.Row.DataItemIndex.ToString() == "0")
            {
                lblMonth.Text = "Jan";
            }
            if (e.Row.DataItemIndex.ToString() == "1")
            {
                lblMonth.Text = "Feb";
            }
            if (e.Row.DataItemIndex.ToString() == "2")
            {
                lblMonth.Text = "Mar";
            }
            if (e.Row.DataItemIndex.ToString() == "3")
            {
                lblMonth.Text = "Apr";
            }
            if (e.Row.DataItemIndex.ToString() == "4")
            {
                lblMonth.Text = "May";
            }
            if (e.Row.DataItemIndex.ToString() == "5")
            {
                lblMonth.Text = "Jun";
            }
            if (e.Row.DataItemIndex.ToString() == "6")
            {
                lblMonth.Text = "Jul";
            }
            if (e.Row.DataItemIndex.ToString() == "7")
            {
                lblMonth.Text = "Aug";
            }
            if (e.Row.DataItemIndex.ToString() == "8")
            {
                lblMonth.Text = "Sep";
            }
            if (e.Row.DataItemIndex.ToString() == "9")
            {
                lblMonth.Text = "Oct";
            }
            if (e.Row.DataItemIndex.ToString() == "10")
            {
                lblMonth.Text = "Nov";
            }
            if (e.Row.DataItemIndex.ToString() == "11")
            {
                lblMonth.Text = "Dec";
            }
        }

        if (e.Row.RowType == DataControlRowType.Footer)
        {
            Label lblSumSales = ((Label)e.Row.FindControl("lblSumSales"));
            Label lblSumRecovery = ((Label)e.Row.FindControl("lblSumRecovery"));
            Label lblSumAgeOfSale = ((Label)e.Row.FindControl("lblSumAgeOfSale"));
            Label lblSumAgeOfRecovery = ((Label)e.Row.FindControl("lblSumAgeOfRecovery"));
            lblSumSales.Text = String.Format("{0:C}", TotalSalesMonth).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblSumRecovery.Text = String.Format("{0:C}", TotalRecoveryMonth).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            lblSumAgeOfSale.Text = TotalSalesPrMonth.ToString("0.00") + "%";
            lblSumAgeOfRecovery.Text = TotalRecoveryPrMonth.ToString("0.00") + "%";

        }
    }

    protected void BindGraph()
    {
        DataTable dt = default(DataTable);
        dt = CreateDataTable();

        //Set the DataSource property of the Chart control to the DataTabel
        Chart1.DataSource = dt;

        //Give two columns of data to Y-axle
        Chart1.Series[0].LegendText = "Purchase";
        Chart1.Series[0].YValueMembers = "Sales";
        Chart1.Series[1].LegendText = "Payment";
        Chart1.Series[1].YValueMembers = "Recovery";

        //Set the X-axle as date value
        Chart1.Series[0].XValueMember = "Month";


        //Bind the Chart control with the setting above

        Chart1.DataBind();
    }





    private DataTable CreateDataTable()
    {
        //Create a DataTable as the data source of the Chart control
        DataTable dt = new DataTable();

        //Add three columns to the DataTable
        dt.Columns.Add("Month");
        dt.Columns.Add("Sales");
        dt.Columns.Add("Recovery");

        DataRow dr;

        //Add rows to the table which contains some random data for demonstration
        dr = dt.NewRow();

        dr["Month"] = "Jan";

        if (Convert.ToString(dtSalesReportMonth.Rows[0]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[0]["TotalSalesAmt"]) * -1;
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[0]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[0]["TotalRecoveryAmt"]) * 1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);

        dr = dt.NewRow();

        dr["Month"] = "Feb";

        if (Convert.ToString(dtSalesReportMonth.Rows[1]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[1]["TotalSalesAmt"]) * -1;
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[1]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[1]["TotalRecoveryAmt"]) * 1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);

        dr = dt.NewRow();

        dr["Month"] = "Mar";

        if (Convert.ToString(dtSalesReportMonth.Rows[2]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[2]["TotalSalesAmt"]) *- 1;
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[2]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[2]["TotalRecoveryAmt"]) * 1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);

        dr = dt.NewRow();

        dr["Month"] = "Apr";

        if (Convert.ToString(dtSalesReportMonth.Rows[3]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[3]["TotalSalesAmt"]) * -1;
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[3]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[3]["TotalRecoveryAmt"]) * 1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);


        dr = dt.NewRow();

        dr["Month"] = "May";

        if (Convert.ToString(dtSalesReportMonth.Rows[4]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[4]["TotalSalesAmt"]) * -1;
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[4]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[4]["TotalRecoveryAmt"]) * 1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);

        dr = dt.NewRow();




        dr["Month"] = "Jun";

        if (Convert.ToString(dtSalesReportMonth.Rows[5]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[5]["TotalSalesAmt"]) * -1;
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[5]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[5]["TotalRecoveryAmt"]) * 1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);



        dr = dt.NewRow();
        dr["Month"] = "Jul";

        if (Convert.ToString(dtSalesReportMonth.Rows[6]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[6]["TotalSalesAmt"]) * -1;
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[6]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[6]["TotalRecoveryAmt"]) * 1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);



        dr = dt.NewRow();
        dr["Month"] = "Aug";

        if (Convert.ToString(dtSalesReportMonth.Rows[7]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[7]["TotalSalesAmt"]) * -1;
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[7]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[7]["TotalRecoveryAmt"]) * 1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);




        dr = dt.NewRow();
        dr["Month"] = "Sep";

        if (Convert.ToString(dtSalesReportMonth.Rows[8]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[8]["TotalSalesAmt"]) * -1;
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[8]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[8]["TotalRecoveryAmt"]) * 1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Month"] = "Oct";

        if (Convert.ToString(dtSalesReportMonth.Rows[9]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[9]["TotalSalesAmt"]) * -1;
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[9]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[9]["TotalRecoveryAmt"]) * 1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);

        dr = dt.NewRow();
        dr["Month"] = "Nov";

        if (Convert.ToString(dtSalesReportMonth.Rows[10]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[10]["TotalSalesAmt"]) * -1;
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[10]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[10]["TotalRecoveryAmt"]) * 1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);


        dr = dt.NewRow();
        dr["Month"] = "Dec";

        if (Convert.ToString(dtSalesReportMonth.Rows[11]["TotalSalesAmt"]) != "")
        {
            dr["Sales"] = Convert.ToDecimal(dtSalesReportMonth.Rows[11]["TotalSalesAmt"]) * -1;
        }
        else
        {
            dr["Sales"] = 0;
        }

        if (Convert.ToString(dtSalesReportMonth.Rows[11]["TotalRecoveryAmt"]) != "")
        {
            dr["Recovery"] = Convert.ToDecimal(dtSalesReportMonth.Rows[11]["TotalRecoveryAmt"]) * 1;
        }
        else
        {
            dr["Recovery"] = 0;
        }

        dt.Rows.Add(dr);
        return dt;
    }
}