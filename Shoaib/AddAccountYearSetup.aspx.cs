﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class AddAccountYearSetup : System.Web.UI.Page
{
    #region DataMembers & Varriables
    SetCustomers objCust = new SetCustomers();
    string FromDate;
    #endregion 


    #region Page Load Event 

    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (!IsPostBack)
        {
            FillAccounting();
          
        }
       // SetUserRight();
    }

    #endregion

    #region Add/ Edit Accounting Year Setup

    protected void btn_save_Click(object sender, EventArgs e)
    {
        System.Threading.Thread.Sleep(500);
        hdnButtonText.Value = "";
        if (hdnAccId.Value != null && hdnAccId.Value != string.Empty)
        {
            objCust.AccountId = int.Parse(hdnAccId.Value);
        }
        else
        {
            objCust.AccountId = 0;
        }


        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.AccountFromDate = Convert.ToDateTime(txtFromDate.Text);
        objCust.AccountToDate = Convert.ToDateTime(txtToDate.Text);
        objCust.IsActiveted = Convert.ToBoolean(chkActive.Checked);


        if (Sessions.UserRole.ToString() != "Admin")
        {
            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }
            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" || Convert.ToString(dt.Rows[0]["IsAdd"]) == "True")
            {
               

                if (Convert.ToString(dt.Rows[0]["IsAdd"]) == "True" && (hdnAccId.Value == null || hdnAccId.Value == string.Empty))
                {
                  int Result = objCust.SetAccountingYearSetup();
                  if (Result != -1)
                  {
                      objCust.GetAccountyearsetupActive();
                      ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Accounting Year  Successful Added Please Login Again.');location.href='Logout.aspx';", true);
                      clear();
                      FillAccounting();
                  }
                  else
                  {
                      ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Accounting Year  Already Exist');", true);
                  }
                
                }
                else
                {
                    if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" && hdnAccId.Value != null && hdnAccId.Value != string.Empty)
                    {
                        int Result = objCust.SetAccountingYearSetup();
                        if (Result != -1)
                        {
                            objCust.GetAccountyearsetupActive();
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Accounting Year Successfully Updated Please Login Again.');location.href='Logout.aspx';", true);
                            clear();
                            FillAccounting();
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Accounting Year  Already Exist');", true);
                        }

                    }

                    else
                    {
                        if (hdnAccId.Value != null && hdnAccId.Value != string.Empty)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='AddAccountYearSetup.aspx';", true);
                        }

                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='AddAccountYearSetup.aspx';", true);
                        }
                    }

                }
            }
            else
            {
                if (hdnAccId.Value != null && hdnAccId.Value != string.Empty)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='AddAccountYearSetup.aspx';", true);
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='AddAccountYearSetup.aspx';", true);
                }
            }
            

        }

        else
        {
           int Result = objCust.SetAccountingYearSetup();
           if (Result != -1)
           {
               objCust.GetAccountyearsetupActive();

               if (hdnAccId.Value != null && hdnAccId.Value != string.Empty)
               {
                   clear();
                   FillAccounting();
                   ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Accounting Year Successfully Updated Please Login Again');location.href='Logout.aspx';", true);
               }
               else
               {
                   Response.Redirect("Logout.aspx");
                   //clear();
                   //FillAccounting();
                   //ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Accounting Year setup successful added Please login again');location.href='Logout.aspx';", true);
                   //ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Accounting Year  setup successful added Please login again');location.href='Logout.aspx';", true);
               }
              
           }
           else
           {
               ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Accounting Year  Already Exist');", true);
           }
        }
    }

    #endregion 

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        clear();
    }
    #endregion

    #region Blank Controls
    private void clear()
    {

        txtFromDate.Text = string.Empty;
        txtToDate.Text = string.Empty;
        hdnAccId.Value = string.Empty;
        chkActive.Checked = false;

    }
    #endregion


    #region Fill Accounting Year Setup
    protected void FillData(int AccountId)
    {
        objCust.AccountId = AccountId;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.GetAccountingYearSetup();
        if (ds.Tables[0].Rows.Count > 0)
        {
            txtToDate.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["ToDate"].ToString()).ToString("dd MMM yyyy");
            txtFromDate.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["FromDate"].ToString()).ToString("dd MMM yyyy");
            hdnAccId.Value = AccountId.ToString();
            chkActive.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsActive"]);
        }

    }
    #endregion

    #region Fill Accounting Year Setup
    protected void FillAccounting()
    {
       

            objCust.AccountId = 0;
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataSet ds = new DataSet();
            ds = objCust.GetAccountingYearSetup();
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 30)
                {
                    grdCustomer.AllowPaging = true;
                }
                grdCustomer.Visible = true;
                grdCustomer.DataSource = ds.Tables[0];
                grdCustomer.DataBind();
                lblError.Visible = false;
            }
            else
            {
                grdCustomer.Visible = false;
                lblError.Visible = true;
            }
       

    }
    #endregion

    #region GridView Events
    protected void grdCustomer_RowCommand(object sender, DataGridCommandEventArgs e)
    {


        if (e.CommandName == "Delete")
        {
            objCust.AccountId = int.Parse(e.CommandArgument.ToString());
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            int result = objCust.DeleteAccountingYearSetup();
            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record deleted successfully.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record can not be Deleted.');", true);
            }
            FillAccounting();
        }

        if (e.CommandName == "Edit")
        {
            FillData(int.Parse(e.CommandArgument.ToString()));
           
        }
    }
    protected void grdCustomer_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Label)e.Item.FindControl("lblFromDate")).Text = FillFromDate(DataBinder.Eval(e.Item.DataItem, "FromDate").ToString());
            ((Label)e.Item.FindControl("lblToDate")).Text = FillFromDate(DataBinder.Eval(e.Item.DataItem, "ToDate").ToString());


            if (Sessions.UserRole.ToString() != "Admin")
            {


                LinkButton lbEdit = (LinkButton)e.Item.FindControl("lbEdit");
                LinkButton lbDelete = (LinkButton)e.Item.FindControl("lbDelete");

                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
                if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
                {
                    Response.Redirect(Request.UrlReferrer.ToString());
                }
                if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
                {

                    lbDelete.Visible = false;

                }
                if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
                {

                    lbEdit.Visible = false;

                }

            }
        }


    }

    protected void grdCustomer_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        grdCustomer.CurrentPageIndex = e.NewPageIndex;
        FillAccounting();

    }
    #endregion

    #region Fill Date Format
    private string FillFromDate(string date)
    {
         FromDate="";
        if (Sessions.CustomerCode != null)
        {
            
            DataSet ds = new DataSet();
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode.ToString());
            ds = objCust.GetDateFormat();
            if (ds.Tables[0].Rows.Count > 0)
            {
                FromDate = Convert.ToDateTime(date).ToString(ds.Tables[0].Rows[0]["Dateformat"].ToString());
            }

           
        }
        return FromDate;
    }
    #endregion

    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }

            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
           
            if (Convert.ToString(dt.Rows[0]["IsAdd"]) != "True")
            {
                if (Convert.ToString(Request["addnew"]) == "1")
                {
                    btn_save.Enabled = false;
                }
            }
        }
    }
}
