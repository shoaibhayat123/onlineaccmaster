﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class CashBankTransactions : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();
    int Last = 0;
    int Corrent = 0;
    int FirstRecord = 0;
    int LastRecord = 0;
    decimal ToatlReceive = 0;
    decimal TotalPayment = 0;
    decimal balance = 0;
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();

        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        if (!IsPostBack)
        {
            ViewState["s"] = null;
            txtCashBookDate.Focus();
            FillAllLedgerAc();
            DataTable dt = new DataTable();
            dgGallery.DataSource = dt;
            dgGallery.DataBind();
            Session["Corrent"] = null;
            Session["FirstRecord"] = null;
            Session["LastRecord"] = null;
            Session["CashBookSummary"] = null;
            Session["Balance"] = null;
            if (Convert.ToString(Request["addnew"]) != "1")
            {
                bindpagging();
                if (Request["InvoiceSummaryId"] == null)
                {
                    BindCashBookMain();
                }
                else
                {
                    DataTable dtInvoiceSummary = new DataTable();
                    dtInvoiceSummary = (DataTable)Session["CashBookSummary"];
                    DataView dv = new DataView(dtInvoiceSummary);
                    dv.RowFilter = "Id=" + Request["InvoiceSummaryId"];
                    dtInvoiceSummary = dv.ToTable();
                    if (dtInvoiceSummary.Rows.Count > 0)
                    {
                        Session["Corrent"] = Convert.ToInt32(dtInvoiceSummary.Rows[0]["row"]);
                        BindCashBookMain();
                    }
                    else
                    {
                        Response.Redirect("default.aspx");
                    }
                }
              
                tdbf.Style.Add("display", "");
                lblBf.Style.Add("display", "");
                ddlAC.Enabled = false;
                txtCashBookDate.Enabled = false;

            }
            else
            {

                objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objSalesInvoice.id = 0;
                DataTable dt1 = new DataTable();
                objSalesInvoice.DataFrom = "CashBook";
                dt1 = objSalesInvoice.getCashBook();
                if (dt1.Rows.Count > 0)
                {
                    
                    int Last = Convert.ToInt32(dt1.Rows.Count) - 1;
                    txtCashBookDate.Text = Convert.ToDateTime(dt1.Rows[Last]["CashBookDate"].ToString()).AddDays(1).ToString("dd MMM yyyy");
                    //ddlAC.SelectedValue = dt1.Rows[Last]["AcFileId"].ToString();
                }
                else
                {
                    txtCashBookDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
                }


                spanPgging.Style.Add("display", "none");
                btnDeleteInvoice.Style.Add("display", "none");





                DataTable dtnew = new DataTable();
                dgGallery.DataSource = dtnew;
                dgGallery.DataBind();
                dgGallery.ShowFooter = false;




            }
        }
        SetUserRight();
    }


    #endregion
    #region Fill Accounts

    protected void FillAllLedgerAc()
    {
       
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtLedger = new DataTable();
            dtLedger = objSalesInvoice.getAcFileAllLedger();
            if (dtLedger.Rows.Count > 0)
            {

                ddlAcdesc.DataSource = dtLedger;
                ddlAcdesc.DataTextField = "Title";
                ddlAcdesc.DataValueField = "Id";
                ddlAcdesc.DataBind();
                ddlAcdesc.Items.Insert(0, new ListItem("Select A/C Title", "0"));


              

            }


            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtLedger1 = new DataTable();
            dtLedger1 = objSalesInvoice.getAcFileByCashBankAccount();
            if (dtLedger1.Rows.Count > 0)
            {
                ddlAC.DataSource = dtLedger1;
                ddlAC.DataTextField = "Title";
                ddlAC.DataValueField = "Id";
                ddlAC.DataBind();
                ddlAC.Items.Insert(0, new ListItem("Select Cash/Bank Account", "0"));
            }
            

        

    }
    #endregion

    #region Add Details
    protected void btn_saveInvoice_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
        if (Sessions.FromDate != null)
        {
            if (ViewState["s"] != null)
            {
                if ((Convert.ToDateTime(txtCashBookDate.Text) >= Convert.ToDateTime(Sessions.FromDate)) && (Convert.ToDateTime(txtCashBookDate.Text) <= Convert.ToDateTime(Sessions.ToDate)))
                {
                    if (hdnCashBookMainId.Value == "")
                    {
                        hdnCashBookMainId.Value = "0";
                    }
                    /************ Add Invoice Summary **************/
                    if (Convert.ToInt32(hdnCashBookMainId.Value) > 0)
                    {
                        objSalesInvoice.id = Convert.ToInt32(hdnCashBookMainId.Value);
                    }
                    else
                    {
                        objSalesInvoice.id = 0;
                    }
                    objSalesInvoice.AcFileId = Convert.ToInt32(ddlAC.SelectedValue);
                    objSalesInvoice.CashBookDate = Convert.ToDateTime(txtCashBookDate.Text);
                    objSalesInvoice.DataFrom = "CashBook";
                    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                    DataTable dtresult = new DataTable();
                    dtresult = objSalesInvoice.AddEditCashbookMain();

                    /************ Add Trans ***************/

                    objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["id"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                    objSalesInvoice.DataFrom = "CashBook";
                    objSalesInvoice.DeleteTrans();

                    /************ Add Invoice Detail **************/
                    if (dtresult.Rows.Count > 0)
                    {
                        if (ViewState["s"] != null)
                        {
                            DataTable dt = new DataTable();
                            dt = (DataTable)ViewState["s"];
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {

                                    objSalesInvoice.id = 0;
                                    objSalesInvoice.Description = Convert.ToString(dt.Rows[i]["Description"].ToString());
                                    objSalesInvoice.CashBookAcfileID = Convert.ToInt32(dt.Rows[i]["CashBookAcfileID"].ToString());
                                    objSalesInvoice.Receive = Convert.ToDecimal(dt.Rows[i]["Receive"].ToString());
                                    objSalesInvoice.Payment = Convert.ToDecimal(dt.Rows[i]["Payment"].ToString());
                                    objSalesInvoice.DataFrom = "CashBook";
                                    objSalesInvoice.CashBookDate = Convert.ToDateTime(txtCashBookDate.Text);
                                    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                    objSalesInvoice.CashBookMainId = Convert.ToInt32(dtresult.Rows[0]["id"].ToString()); /***** dtresult.Rows[0] is for CashBookMainId which is new****/


                                    DataTable tblCashBookDetail = new DataTable();
                                    tblCashBookDetail = objSalesInvoice.AddeditCashBookDetail();

                                    /********* Add in trans **************/
                                    objSalesInvoice.TransId = 0;
                                    objSalesInvoice.TransactionDate = Convert.ToDateTime(txtCashBookDate.Text);
                                    objSalesInvoice.AcFileId = Convert.ToInt32(dt.Rows[i]["CashBookAcfileID"].ToString());
                                    if (Convert.ToDecimal(dt.Rows[i]["Receive"].ToString()) > 0)
                                    {
                                        objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(dt.Rows[i]["Receive"].ToString());
                                    }
                                    else
                                    {
                                        objSalesInvoice.TransAmt = Convert.ToDecimal(dt.Rows[i]["Payment"].ToString());
                                    }
                                    objSalesInvoice.Description = Convert.ToString(dt.Rows[i]["Description"].ToString());
                                    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                    objSalesInvoice.DataFrom = "CashBook";

                                    objSalesInvoice.AddEditTrans();


                                    /***********Main A/c detail**************/
                                    objSalesInvoice.TransId = 0;
                                    objSalesInvoice.TransactionDate = Convert.ToDateTime(txtCashBookDate.Text);
                                    objSalesInvoice.AcFileId = Convert.ToInt32(ddlAC.SelectedValue);
                                    if (Convert.ToDecimal(dt.Rows[i]["Receive"].ToString()) > 0)
                                    {
                                        objSalesInvoice.TransAmt = Convert.ToDecimal(dt.Rows[i]["Receive"].ToString());
                                    }
                                    else
                                    {
                                        objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(dt.Rows[i]["Payment"].ToString());
                                    }
                                    objSalesInvoice.Description = Convert.ToString(dt.Rows[i]["Description"].ToString());
                                    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                    objSalesInvoice.DataFrom = "CashBook";

                                    objSalesInvoice.AddEditTrans();

                                }
                            }
                        }

                    }


                    if (Convert.ToInt32(hdnCashBookMainId.Value) > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Updated.');location.href='CashBankTransactions.aspx';", true);

                    }

                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Saved.');location.href='CashBankTransactions.aspx';", true);
                    }
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Date out of accouting year.');", true);

                }

            }


            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please Fill  Details.');", true);
            }
        }
    }

    #endregion

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {

        Response.Redirect("CashBankTransactions.aspx");


    }
    #endregion
    #region Add New Bill details
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";

        if ((Convert.ToDecimal(txtReceive.Text) > 0) || (Convert.ToDecimal(txtPayment.Text) > 0))
        {
            if ((Convert.ToDecimal(txtReceive.Text) == 0) || (Convert.ToDecimal(txtPayment.Text) == 0))
            {
                filltable();
                if (hdndetail.Value == "")
                {
                    DataRow row;
                    if (dttbl.Rows.Count > 0)
                    {

                        int maxid = 0;
                        for (int i = 0; i < dttbl.Rows.Count; i++)
                        {
                            if (dttbl.Rows[i]["Id"].ToString() != "")
                            {
                                if (maxid < Convert.ToInt32(dttbl.Rows[i]["Id"].ToString()))
                                {
                                    maxid = Convert.ToInt32(dttbl.Rows[i]["Id"].ToString());
                                }
                            }
                        }

                        row = dttbl.NewRow();
                        row["Id"] = maxid + 1;
                        row["CashBookAcfileID"] = Convert.ToInt32(ddlAcdesc.SelectedValue.ToString());
                        row["AcFileDesc"] = Convert.ToString(ddlAcdesc.SelectedItem.Text);
                        row["Receive"] = Convert.ToDecimal(txtReceive.Text);
                        row["Payment"] = Convert.ToDecimal(txtPayment.Text);
                        row["Description"] = Convert.ToString(txtDescriptionDetail.Text);
                    }
                    else
                    {

                        row = dttbl.NewRow();
                        row["Id"] = 1;
                        row["CashBookAcfileID"] = Convert.ToInt32(ddlAcdesc.SelectedValue.ToString());
                        row["AcFileDesc"] = Convert.ToString(ddlAcdesc.SelectedItem.Text);
                        row["Receive"] = Convert.ToDecimal(txtReceive.Text);
                        row["Payment"] = Convert.ToDecimal(txtPayment.Text);
                        row["Description"] = Convert.ToString(txtDescriptionDetail.Text);

                    }
                    dttbl.Rows.Add(row);
                    dttbl.AcceptChanges();
                }
                else
                {
                    for (int i = 0; i < dttbl.Rows.Count; i++)
                    {
                        if (dttbl.Rows[i]["Id"].ToString() == hdndetail.Value)
                        {

                            dttbl.Rows[i]["CashBookAcfileID"] = Convert.ToInt32(ddlAcdesc.SelectedValue.ToString());
                            dttbl.Rows[i]["AcFileDesc"] = Convert.ToString(ddlAcdesc.SelectedItem.Text);
                            dttbl.Rows[i]["Description"] = Convert.ToString(txtDescriptionDetail.Text);
                            dttbl.Rows[i]["Receive"] = Convert.ToDecimal(txtReceive.Text);
                            dttbl.Rows[i]["Payment"] = Convert.ToDecimal(txtPayment.Text);

                        }
                    }


                }

                ViewState.Add("s", dttbl);
                dgGallery.DataSource = dttbl;
                dgGallery.DataBind();
                dgGallery.ShowFooter = true;
                ShowTotal();
                if (dttbl.Rows.Count > 0)
                {
                    dgGallery.Visible = true;
                }

                ClearInvoiceDetail();

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Enter Valid Amount.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Enter Valid Amount.');", true);
        }

    }
    #endregion

    #region Grid Event
    protected void dgGallery_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Label lblBalance = ((Label)e.Item.FindControl("lblBalance"));
            if (e.Item.ItemIndex.ToString() == "0")
            {
                balance += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Receive")) - Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Payment")) + Convert.ToDecimal(lblBf.Text);
            }
            else
            {
                balance += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Receive")) - Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Payment"));

            }
            lblBalance.Text = String.Format("{0:C}", balance).Replace('-', ' ').Replace('$', ' ').Replace('(', '-').Replace(')', ' ');
            ToatlReceive += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Receive"));
            TotalPayment += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Payment"));

        }

        if (e.Item.ItemType == ListItemType.Footer)
        {
            Label lblTotalReceive = ((Label)e.Item.FindControl("lblTotalReceive"));
            Label lblTotalPayment = ((Label)e.Item.FindControl("lblTotalPayment"));

            lblTotalReceive.Text = String.Format("{0:C}", ToatlReceive).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            lblTotalPayment.Text = String.Format("{0:C}", TotalPayment).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        }

    }
    protected void dgGallery_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        dttbl = (DataTable)ViewState["s"];
        if (e.CommandName == "Delete")
        {
            int j = 0;
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    dttbl.Rows.RemoveAt(i);
                }
            }
            dttbl.AcceptChanges();
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            ViewState.Add("s", dttbl);
            // ShowTotal();

        }
        if (e.CommandName == "Edit")
        {

            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    hdndetail.Value = dttbl.Rows[i]["Id"].ToString();
                    ddlAcdesc.SelectedValue = dttbl.Rows[i]["CashBookAcfileID"].ToString();
                    txtDescriptionDetail.Text = dttbl.Rows[i]["Description"].ToString();
                    txtReceive.Text = dttbl.Rows[i]["Receive"].ToString();
                    txtPayment.Text = dttbl.Rows[i]["Payment"].ToString();

                }
            }
        }


    }
    #endregion

    #region ClearInvoiceDetail
    protected void ClearInvoiceDetail()
    {
        ddlAcdesc.SelectedValue = "0";
        txtDescriptionDetail.Text = "";
        txtReceive.Text = "0.00";
        txtPayment.Text = "0.00";
        hdndetail.Value = "";
    }
    #endregion

    #region Show Total
    protected void ShowTotal()
    {
        //Decimal Amount = 0;

        //DataTable dt = new DataTable();
        //dt = (DataTable)Session["s"];
        //if (dt.Rows.Count > 0)
        //{
        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {
        //        Amount += Convert.ToDecimal(dt.Rows[i]["Amount"]);

        //    }
        //}
        ////lblAmount.Text = String.Format("{0:C}", Amount).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');



    }
    #endregion
    #region Pagging
    private void bindpagging()
    {
        Last = 0;
        Corrent = 0;
        FirstRecord = 0;
        LastRecord = 0;
        //  int row = Convert.ToInt32(Request["row"]);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.id = 0;
        DataTable dt = new DataTable();
        objSalesInvoice.DataFrom = "CashBook";
        dt = objSalesInvoice.getCashBook();

        if (dt.Rows.Count > 0)
        {
            Session["CashBookSummary"] = dt;
            Last = dt.Rows.Count;
            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            Session["FirstRecord"] = FirstRecord;
            Session["LastRecord"] = LastRecord;
            Session["Corrent"] = LastRecord;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "location.href='CashBankTransactions.aspx?addnew=1';", true);
        }


    }
    protected void lnkFirst_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["FirstRecord"];
        BindCashBookMain();

    }
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["FirstRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) - 1;
            BindCashBookMain();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["LastRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) + 1;
            BindCashBookMain();
        }
    }
    protected void lnLast_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["LastRecord"];
        BindCashBookMain();
    }
    #endregion

    #region Fill Cash Book summary
    protected void BindCashBookMain()
    {
        if (Session["CashBookSummary"] != null)
        {
            DataTable dtCashBookSummary = (DataTable)Session["CashBookSummary"];
            if (dtCashBookSummary.Rows.Count > 0)
            {

                DataView DvCashBookSummary = dtCashBookSummary.DefaultView;
                DvCashBookSummary.RowFilter = "Row = '" + Session["Corrent"].ToString() + "'";
                dtCashBookSummary = DvCashBookSummary.ToTable();
                if (dtCashBookSummary.Rows.Count > 0)
                {
                    txtCashBookDate.Text = Convert.ToDateTime(dtCashBookSummary.Rows[0]["CashBookDate"].ToString()).ToString("dd MMM yyyy");
                    lblBf.Text = Convert.ToString(dtCashBookSummary.Rows[0]["OpeningBal"]);
                    ddlAC.SelectedValue = Convert.ToString(dtCashBookSummary.Rows[0]["AcFileId"]);
                    hdnCashBookMainId.Value = Convert.ToString(dtCashBookSummary.Rows[0]["id"]);
                    getBf();
                    fillBankReceiptDetails(Convert.ToInt32(dtCashBookSummary.Rows[0]["id"]));

                }
            }

        }

    }
    #endregion
    protected void fillBankReceiptDetails(int pid)
    {
        ViewState["s"] = null;

        DataTable dtCashBookSimpleDetail = new DataTable();
        objSalesInvoice.CashBookMainId = pid;
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objSalesInvoice.DataFrom = "CashBook";
        dtCashBookSimpleDetail = objSalesInvoice.getCashBookDetail();

        if (dtCashBookSimpleDetail.Rows.Count > 0)
        {
            for (int i = 0; i < dtCashBookSimpleDetail.Rows.Count; i++)
            {
                filltable();
                DataRow row;
                row = dttbl.NewRow();
                row["Id"] = i + 1;
                row["CashBookAcfileID"] = Convert.ToInt32(dtCashBookSimpleDetail.Rows[i]["CashBookAcfileID"]);
                row["AcFileDesc"] = Convert.ToString(dtCashBookSimpleDetail.Rows[i]["AcFileDesc"]);
                row["Payment"] = Convert.ToDecimal(dtCashBookSimpleDetail.Rows[i]["Payment"]);
                row["Receive"] = Convert.ToDecimal(dtCashBookSimpleDetail.Rows[i]["Receive"]);
                row["Description"] = Convert.ToString(dtCashBookSimpleDetail.Rows[i]["Description"]);
                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
                ViewState.Add("s", dttbl);
            }
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            dgGallery.Visible = true;
            if (dttbl.Rows.Count > 0)
            {
                dgGallery.ShowFooter = true;
            }

        }
        else
        {
            dgGallery.Visible = false;
        }

    }


    #region Fill session Table
    public void filltable()
    {
        if (ViewState["s"] == null)
        {
            DataColumn column = new DataColumn();
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "CashBookAcfileID";
            column.Caption = "CashBookAcfileID";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "AcFileDesc";
            column.Caption = "AcFileDesc";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Description";
            column.Caption = "Description";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Payment";
            column.Caption = "Payment";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Receive";
            column.Caption = "Receive";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);
        }
        else
        {
            dttbl = (DataTable)ViewState["s"];
        }
    }
    #endregion


    #region Delete BankReceiptVoucher
    protected void btnDeleteInvoice_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(hdnCashBookMainId.Value) > 0)
        {
            int result = 0;
            objSalesInvoice.CashBookMainId = Convert.ToInt32(hdnCashBookMainId.Value);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.DataFrom = "CashBook";
            result = objSalesInvoice.deleteAllCashBook();
            Session["CashBookSummary"] = null;
            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record deleted successfully.');location.href='CashBankTransactions.aspx';", true);
            }
        }
    }
    #endregion
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        string urllast = "PrintCashBankTransactions.aspx?VoucherSimpleMainId=" + hdnCashBookMainId.Value;
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,status=yes' );", true);
    }
    protected void ddlAC_SelectedIndexChanged(object sender, EventArgs e)
    {
        getBf();

        DataTable dtCashBookSimpleDetail = new DataTable();
        objSalesInvoice.DataFrom = "CashBook";
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.CashBookDate = Convert.ToDateTime(txtCashBookDate.Text);
        objSalesInvoice.AcfileID = Convert.ToInt32(ddlAC.SelectedValue);

        DataSet ds = new DataSet();
        ds = objSalesInvoice.CashbookDetails();

        if (ds.Tables[0].Rows.Count > 0)
        {
            hdnCashBookMainId.Value = Convert.ToString(ds.Tables[0].Rows[0]["Id"]);
            fillBankReceiptDetails(Convert.ToInt32(hdnCashBookMainId.Value));
            btnDeleteInvoice.Style.Add("display", "");
        }
    }

    protected void getBf()
    {
        if (txtCashBookDate.Text == "")
        {
            txtCashBookDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
        }
        DataTable dtCashBookSimpleDetail = new DataTable();
        objSalesInvoice.AcFileId = Convert.ToInt32(ddlAC.SelectedValue);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.CashBookDate = Convert.ToDateTime(txtCashBookDate.Text);
        DataSet ds = new DataSet();
        ds = objSalesInvoice.getBrodForwordBal();
        decimal tottrans = 0;
        decimal OpeningBal = 0;
        if (Convert.ToString(ds.Tables[0].Rows[0]["OpeningBal"]) != "")
        {
            OpeningBal = Convert.ToDecimal(ds.Tables[0].Rows[0]["OpeningBal"]);
        }
        if (Convert.ToString(ds.Tables[0].Rows[0]["tottrans"]) != "")
        {
            tottrans = Convert.ToDecimal(ds.Tables[0].Rows[0]["tottrans"]);
        }
        decimal bfamount = OpeningBal + tottrans;
        lblBf.Text = Convert.ToString(bfamount);
        tdbf.Style.Add("display", "");
        lblBf.Style.Add("display", "");
        ddlAC.Enabled = false;
        txtCashBookDate.Enabled = false;
        calImage.Style.Add("display", "none");

    }

    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }

            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
            {
                btnDeleteInvoice.Visible = false;
            }
            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

               

            }

            if (Convert.ToString(dt.Rows[0]["IsAdd"]) != "True")
            {

                lblbutton.Visible = false;
                if (Convert.ToString(Request["addnew"]) == "1")
                {

                    ImageButton2.Visible = false;
                }
            }



            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {
                if (Convert.ToString(Request["addnew"]) != "1")
                {

                    ImageButton2.Visible = false;
                }
            }

        }
    }
    protected void lblbutton_Click(object sender, EventArgs e)
    {
        Response.Redirect("CashBankTransactions.aspx?addnew=1");

    } 
}
