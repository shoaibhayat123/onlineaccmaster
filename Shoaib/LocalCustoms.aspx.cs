﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class LocalCustoms : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();
    Car objCar = new Car();
    int Last = 0;
    int Corrent = 0;
    int FirstRecord = 0;
    int LastRecord = 0;
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        if (!IsPostBack)
        {

            BindExpenses();
            BindContainer();
            BindAcfile();
            Session["s"] = null;
            DataTable dt = new DataTable();
            dgGallery.DataSource = dt;
            dgGallery.DataBind();
            Session["Corrent"] = null;
            Session["FirstRecord"] = null;
            Session["LastRecord"] = null;
            Session["InvoiceSummary"] = null;

            if (Convert.ToString(Request["addnew"]) != "1")
            {

                bindpagging();
                if (Request["InvoiceSummaryId"] == null)
                {
                    BindInvoice();
                }
                else
                {
                    DataTable dtInvoiceSummary = new DataTable();
                    dtInvoiceSummary = (DataTable)Session["InvoiceSummary"];
                    DataView dv = new DataView(dtInvoiceSummary);
                    dv.RowFilter = "LocalCustomMainId=" + Request["InvoiceSummaryId"];
                    dtInvoiceSummary = dv.ToTable();
                    if (dtInvoiceSummary.Rows.Count > 0)
                    {
                        Session["Corrent"] = Convert.ToInt32(dtInvoiceSummary.Rows[0]["row"]);
                        BindInvoice();
                    }
                    else
                    {
                        Response.Redirect("default.aspx");
                    }
                }


            }
            else
            {
                objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objCar.LocalCustomMainId = 0;
                DataSet ds = new DataSet();             
                ds = objCar.GetLocalCustomMain();
                dt = ds.Tables[0];


                if (dt.Rows.Count > 0)
                {

                    int Last = Convert.ToInt32(dt.Rows.Count) - 1;

                    txtVoucherDate.Text = Convert.ToDateTime(dt.Rows[Last]["VoucherDate"]).ToString("dd MMM yyyy");

                }
                else
                {
                    txtVoucherDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
                }
                spanPgging.Style.Add("display", "none");
                btnDeleteInvoice.Style.Add("display", "none");
            }

        }
    }

    #region Bind Dropdown
    protected void BindExpenses()
    {
        objCar.CustomExpenseId = 0; ;
        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCar.GetCustomExpense();

        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlExpenses.DataSource = ds.Tables[0];
            ddlExpenses.DataTextField = "CustomExpense";
            ddlExpenses.DataValueField = "CustomExpenseId";
            ddlExpenses.DataBind();
            ddlExpenses.Items.Insert(0, new ListItem("Select Custom Expense", "0"));
        }
    }
    protected void BindContainer()
    {
        objCar.CarLodingId = 0; ;
        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCar.getContainer();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlContainer.DataSource = ds.Tables[0];
            ddlContainer.DataTextField = "ContainerNo";
            ddlContainer.DataValueField = "CarLodingId";
            ddlContainer.DataBind();
            ddlContainer.Items.Insert(0, new ListItem("Select Container", "0"));
        }

    }
    protected void BindAcfile()
    {
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtLedger = new DataTable();
        dtLedger = objSalesInvoice.getAcFileAllLedger();
        if (dtLedger.Rows.Count > 0)
        {


            ddlDebit.DataSource = dtLedger;
            ddlDebit.DataTextField = "Title";
            ddlDebit.DataValueField = "Id";
            ddlDebit.DataBind();
            ddlDebit.Items.Insert(0, new ListItem("Select Debit A/C", "0"));

            ddlCredit.DataSource = dtLedger;
            ddlCredit.DataTextField = "Title";
            ddlCredit.DataValueField = "Id";
            ddlCredit.DataBind();
            ddlCredit.Items.Insert(0, new ListItem("Select Credit A/C", "0"));

        }

      
    }
    #endregion
    #region Add New Bill details
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        filltable();
        if (hdndetail.Value == "")
        {
            DataRow row;
            if (dttbl.Rows.Count > 0)
            {

                int maxid = 0;
                for (int i = 0; i < dttbl.Rows.Count; i++)
                {
                    if (dttbl.Rows[i]["Id"].ToString() != "")
                    {
                        if (maxid < Convert.ToInt32(dttbl.Rows[i]["Id"].ToString()))
                        {
                            maxid = Convert.ToInt32(dttbl.Rows[i]["Id"].ToString());
                        }
                    }
                }

                row = dttbl.NewRow();
                row["Id"] = maxid + 1;
               
                row["CustomExpenseId"] = Convert.ToInt32(ddlExpenses.SelectedValue);
                row["CustomExpense"] = Convert.ToString(ddlExpenses.SelectedItem.Text);

                row["ReceiptNo"] = Convert.ToString(txtReceipt.Text);
                row["DateFrom"] = Convert.ToString(txtDateFrom.Text);             

                row["DateTo"] = Convert.ToString(txtDateTo.Text);


                row["Amount"] = Convert.ToDecimal(txtamount.Text);


                row["AccountCRAcfile"] = Convert.ToString(ddlCredit.SelectedItem.Text);
                row["AccountCRAcfileId"] = Convert.ToInt32(ddlCredit.SelectedValue);

                row["AccountDrAcfile"] = Convert.ToString(ddlDebit.SelectedItem.Text);
                row["AccountDrAcfileId"] = Convert.ToString(ddlDebit.SelectedValue);




            }
            else
            {


                row = dttbl.NewRow();
                row["Id"] = 1;
                row["CustomExpenseId"] = Convert.ToInt32(ddlExpenses.SelectedValue);
                row["CustomExpense"] = Convert.ToString(ddlExpenses.SelectedItem.Text);

                row["ReceiptNo"] = Convert.ToString(txtReceipt.Text);
                row["DateFrom"] = Convert.ToString(txtDateFrom.Text);

                row["DateTo"] = Convert.ToString(txtDateTo.Text);


                row["Amount"] = Convert.ToDecimal(txtamount.Text);

                row["AccountCRAcfile"] = Convert.ToString(ddlCredit.SelectedItem.Text);
                row["AccountCRAcfileId"] = Convert.ToInt32(ddlCredit.SelectedValue);

                row["AccountDrAcfile"] = Convert.ToString(ddlDebit.SelectedItem.Text);
                row["AccountDrAcfileId"] = Convert.ToString(ddlDebit.SelectedValue);
            }
            dttbl.Rows.Add(row);
            dttbl.AcceptChanges();
        }

        else
        {

            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == hdndetail.Value)
                {

                     dttbl.Rows[i]["CustomExpenseId"] = Convert.ToInt32(ddlExpenses.SelectedValue);
                     dttbl.Rows[i]["CustomExpense"] = Convert.ToString(ddlExpenses.SelectedItem.Text);
                     dttbl.Rows[i]["ReceiptNo"] = Convert.ToString(txtReceipt.Text);
                     dttbl.Rows[i]["DateFrom"] = Convert.ToString(txtDateFrom.Text);
                     dttbl.Rows[i]["DateTo"] = Convert.ToString(txtDateTo.Text);
                     dttbl.Rows[i]["Amount"] = Convert.ToDecimal(txtamount.Text);
                     dttbl.Rows[i]["AccountCRAcfile"] = Convert.ToString(ddlCredit.SelectedItem.Text);
                     dttbl.Rows[i]["AccountCRAcfileId"] = Convert.ToInt32(ddlCredit.SelectedValue);
                     dttbl.Rows[i]["AccountDrAcfile"] = Convert.ToString(ddlDebit.SelectedItem.Text);
                     dttbl.Rows[i]["AccountDrAcfileId"] = Convert.ToInt32(ddlDebit.SelectedValue);
                     dttbl.Rows[i]["CustomExpenseId"] = Convert.ToInt32(ddlExpenses.SelectedValue);
                     dttbl.Rows[i]["CustomExpense"] = Convert.ToString(ddlExpenses.SelectedItem.Text);

                }
            }
        }
        Session.Add("s", dttbl);
        dgGallery.DataSource = dttbl;
        dgGallery.DataBind();
        lblTotalAmount.Text = Convert.ToDecimal(dttbl.Compute("Sum(Amount)", "")).ToString("0.00");
        if (dttbl.Rows.Count > 0)
        {
            dgGallery.Visible = true;
        }

      ddlExpenses.SelectedValue = "0";
      ddlCredit.SelectedValue = "0";
      ddlDebit.SelectedValue = "0";
      txtReceipt.Text = "";
      txtamount.Text = "";
      txtDateFrom.Text = "";
      txtDateTo.Text = "";
      ddlExpenses.Focus();

    }
    #endregion


    #region Fill session Table
    public void filltable()
    {
        if (Session["s"] == null)
        {

            DataColumn column = new DataColumn();

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "CustomExpenseId";
            column.Caption = "CustomExpenseId";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "CustomExpense";
            column.Caption = "CustomExpense";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ReceiptNo";
            column.Caption = "ReceiptNo";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "DateFrom";
            column.Caption = "DateFrom";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "DateTo";
            column.Caption = "DateTo";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Amount";
            column.Caption = "Amount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "AccountCRAcfileId";
            column.Caption = "AccountCRAcfileId";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "AccountCRAcfile";
            column.Caption = "AccountCRAcfile";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "AccountDrAcfileId";
            column.Caption = "AccountDrAcfileId";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "AccountDrAcfile";
            column.Caption = "AccountDrAcfile";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            

        }
        else
        {
            dttbl = (DataTable)Session["s"];
        }
    }
    #endregion

    #region Grid Event
    protected void dgGallery_RowDataBound(object sender, DataGridItemEventArgs e)
    {
     



    }
    protected void dgGallery_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        dttbl = (DataTable)Session["s"];
        if (e.CommandName == "Delete")
        {
            int j = 0;
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    dttbl.Rows.RemoveAt(i);
                }
            }
            dttbl.AcceptChanges();
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            lblTotalAmount.Text = Convert.ToDecimal(dttbl.Compute("Sum(Amount)", "")).ToString("0.00");
            Session.Add("s", dttbl);


        }

        if (e.CommandName == "Edit")
        {
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                  
                    ddlExpenses.SelectedValue = Convert.ToString(dttbl.Rows[i]["CustomExpenseId"]);
                    txtReceipt.Text = Convert.ToString(dttbl.Rows[i]["ReceiptNo"]);
                    txtDateFrom.Text = Convert.ToString(dttbl.Rows[i]["DateFrom"]);
                    txtDateTo.Text = Convert.ToString(dttbl.Rows[i]["DateTo"]);
                    txtamount.Text = Convert.ToString(dttbl.Rows[i]["Amount"]);
                    ddlCredit.SelectedValue = Convert.ToString(dttbl.Rows[i]["AccountCRAcfileId"]);
                    ddlDebit.SelectedValue = Convert.ToString(dttbl.Rows[i]["AccountDrAcfileId"]);
                    ddlExpenses.SelectedValue = Convert.ToString(dttbl.Rows[i]["CustomExpenseId"]);

                    hdndetail.Value = dttbl.Rows[i]["Id"].ToString();
                }
            }
        }
    }
    #endregion

    #region Pagging
    private void bindpagging()
    {
        Last = 0;
        Corrent = 0;
        FirstRecord = 0;
        LastRecord = 0;
        //  int row = Convert.ToInt32(Request["row"]);
        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCar.LocalCustomMainId = 0;
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        ds = objCar.GetLocalCustomMain();
        dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            Session["InvoiceSummary"] = dt;
            Last = dt.Rows.Count;
            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            //Corrent = Convert.ToInt32(dt.Rows[0]["Row"]);
            Session["FirstRecord"] = FirstRecord;
            Session["LastRecord"] = LastRecord;
            Session["Corrent"] = LastRecord;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "location.href='LocalCustoms.aspx?addnew=1';", true);
        }


    }
    protected void lnkFirst_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["FirstRecord"];
        BindInvoice();

    }
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["FirstRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) - 1;
            BindInvoice();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["LastRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) + 1;
            BindInvoice();
        }
    }
    protected void lnLast_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["LastRecord"];
        BindInvoice();
    }
    #endregion

      #region Fill invoice summary
    protected void BindInvoice()
    {
        if (Session["InvoiceSummary"] != null)
        {
            DataTable dtInvoiceSummary = (DataTable)Session["InvoiceSummary"];
            if (dtInvoiceSummary.Rows.Count > 0)
            {

                DataView DvInvoiceSummary = dtInvoiceSummary.DefaultView;
                DvInvoiceSummary.RowFilter = "Row = '" + Session["Corrent"].ToString() + "'";
                dtInvoiceSummary = DvInvoiceSummary.ToTable();
                if (dtInvoiceSummary.Rows.Count > 0)
                {
                  
                    txtVoucherDate.Text = Convert.ToDateTime(dtInvoiceSummary.Rows[0]["VoucherDate"].ToString()).ToString("dd MMM yyyy");
                    txtBEDate.Text = Convert.ToDateTime(dtInvoiceSummary.Rows[0]["BeDate"].ToString()).ToString("dd MMM yyyy");
                    txtEntryNO.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["BillOfEntryNo"]);
                    ddlContainer.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["ContainerNoId"]);


                    hdnInvoiceSummaryId.Value = Convert.ToString(dtInvoiceSummary.Rows[0]["LocalCustomMainId"]);


                    fillInvoiceDetails(Convert.ToInt32(dtInvoiceSummary.Rows[0]["LocalCustomMainId"]));

            



                }
            }

        }
    }

    #endregion

    #region fillInvoiceDetails
    protected void fillInvoiceDetails(int pid)
    {
        Session["s"] = null;

        DataTable dtInvoice = new DataTable();
        objCar.LocalCustomMainId = pid;
        objCar.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataSet ds = new DataSet();        
        ds = objCar.GetLocalCustomDetail();
        dtInvoice = ds.Tables[0];
        if (dtInvoice.Rows.Count > 0)
        {
            for (int i = 0; i < dtInvoice.Rows.Count; i++)
            {
                filltable();
                DataRow row;
                row = dttbl.NewRow();
                row["Id"] = i + 1;

                row["CustomExpenseId"] = Convert.ToInt32(dtInvoice.Rows[i]["CustomExpenseId"]);
                row["CustomExpense"] = Convert.ToString(dtInvoice.Rows[i]["CustomExpense"]);
                row["ReceiptNo"] = Convert.ToString(dtInvoice.Rows[i]["ReceiptNo"]);
                row["DateFrom"] = Convert.ToString(dtInvoice.Rows[i]["DateFrom"]);
                row["DateTo"] = Convert.ToString(dtInvoice.Rows[i]["DateTo"]);
                row["Amount"] = Convert.ToDecimal(dtInvoice.Rows[i]["Amount"]);
                row["AccountCRAcfile"] = Convert.ToString(dtInvoice.Rows[i]["AccountCRAcfile"]);
                row["AccountCRAcfileId"] = Convert.ToInt32(dtInvoice.Rows[i]["AccountCRAcfileId"]);
                row["AccountDrAcfile"] = Convert.ToString(dtInvoice.Rows[i]["AccountDrAcfile"]);
                row["AccountDrAcfileId"] = Convert.ToInt32(dtInvoice.Rows[i]["AccountDrAcfileId"]);


                //ddlExpenses.SelectedValue = Convert.ToString(dttbl.Rows[i]["CustomExpenseId"]);
                //txtReceipt.Text = Convert.ToString(dttbl.Rows[i]["ReceiptNo"]);
                //txtDateFrom.Text = Convert.ToString(dttbl.Rows[i]["DateFrom"]);
                //txtDateTo.Text = Convert.ToString(dttbl.Rows[i]["DateTo"]);
                //txtamount.Text = Convert.ToString(dttbl.Rows[i]["Amount"]);
                //ddlCredit.SelectedValue = Convert.ToString(dttbl.Rows[i]["AccountCRAcfileId"]);
                //ddlDebit.SelectedValue = Convert.ToString(dttbl.Rows[i]["AccountDrAcfileId"]);
                //ddlExpenses.SelectedValue = Convert.ToString(dttbl.Rows[i]["CustomExpenseId"]);


                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
                Session.Add("s", dttbl);
            }
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            dgGallery.Visible = true;

            lblTotalAmount.Text = Convert.ToDecimal(dttbl.Compute("Sum(Amount)", "")).ToString("0.00");
        }
        else
        {
            dgGallery.Visible = false;
        }

    }
    #endregion


    #region Delete Invoice
    protected void btnDeleteInvoice_Click(object sender, EventArgs e)
    {

        if (Convert.ToInt32(hdnInvoiceSummaryId.Value) > 0)
        {
            int result = 0;
            objCar.LocalCustomMainId = Convert.ToInt32(hdnInvoiceSummaryId.Value);
            objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);

            result = objCar.DeleteLocalCustomsMain();
            Session["InvoiceSummary"] = null;
            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record deleted successfully.');location.href='LocalCustoms.aspx';", true);
            }


        }


    }
    #endregion

    protected void lblbutton_Click(object sender, EventArgs e)
    {
        Response.Redirect("LocalCustoms.aspx?addnew=1");
    }



    #region Add Details
    protected void btn_saveInvoice_Click(object sender, ImageClickEventArgs e)
    {
        if (Sessions.FromDate != null)
        {
            if (Session["s"] != null)
            {
                if ((Convert.ToDateTime(txtVoucherDate.Text) >= Convert.ToDateTime(Sessions.FromDate)) && (Convert.ToDateTime(txtVoucherDate.Text) <= Convert.ToDateTime(Sessions.ToDate)))
                {
                    DataTable dtSession = (DataTable)Session["s"];
                 


                        if (hdnInvoiceSummaryId.Value == "")
                        {
                            hdnInvoiceSummaryId.Value = "0";
                        }
                        /************ Add Invoice Summary **************/
                        if (Convert.ToInt32(hdnInvoiceSummaryId.Value) > 0)
                        {
                            objCar.LocalCustomMainId = Convert.ToInt32(hdnInvoiceSummaryId.Value);
                        }
                        else
                        {
                            objCar.LocalCustomMainId = 0;
                        }
                        //int Exists = 0;
                        //for (int i = 0; i < dtSession.Rows.Count; i++)
                        //{
                        //    objCar.VinId = Convert.ToInt32(dtSession.Rows[i]["VinId"]);
                        //    objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);

                        //    Exists += objCar.getExists();
                        //    if (Exists == 1)
                        //    {
                        //        string msg = "VIN NO. " + Convert.ToString(dtSession.Rows[i]["VIN"]) + " already Exist.";
                        //        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('" + msg + "');", true);
                        //        break;
                        //    }
                        //}



                        //if (Exists == 0)
                        //{



                           
                            objCar.VoucherDate = Convert.ToDateTime(txtVoucherDate.Text);
                            objCar.ContainerNoId = Convert.ToInt32(ddlContainer.SelectedValue);
                            objCar.BillOfEntryNo = Convert.ToString(txtEntryNO.Text).ToUpper();
                            objCar.BeDate = Convert.ToDateTime(txtBEDate.Text);
                           
                            objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            int result = 0;
                            result = objCar.AddEditLocalCustomMain();


                            // Add Details in  Trans

                            //objSalesInvoice.InvoiceSummaryId = result; /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                            //objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            //objSalesInvoice.DataFrom = "CARLOADING";
                            //objSalesInvoice.DeleteTrans();

                            //if (Convert.ToDecimal(txtTotalCost1.Text) > 0)
                            //{
                            //    for (int i = 0; i < 2; i++)
                            //    {
                            //        objSalesInvoice.TransId = 0;
                            //        objSalesInvoice.TransactionDate = Convert.ToDateTime(txtVoucherDate.Text);
                            //        objSalesInvoice.DataFrom = "CARLOADING";
                            //        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            //        objSalesInvoice.VinId = Convert.ToInt32(ddlVIN.SelectedValue);
                            //        if (i == 0)  /* For Sale account */
                            //        {
                            //            objSalesInvoice.AcFileId = Convert.ToInt32(ddlLoader.SelectedValue);
                            //            objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(txtTotalCost1.Text);
                            //            objSalesInvoice.Description = txtRemark.Text;
                            //            objSalesInvoice.AddEditTrans();

                            //        }

                            //        if (i == 1) /* For Purchase  account */
                            //        {
                            //            objSalesInvoice.AcFileId = Convert.ToInt32(ddlLoadingExp.SelectedValue);
                            //            objSalesInvoice.TransAmt = Convert.ToDecimal(txtTotalCost1.Text);
                            //            objSalesInvoice.Description = txtRemark.Text;
                            //            objSalesInvoice.AddEditTrans();
                            //        }

                            //    }
                            //}

                            //if (Convert.ToDecimal(txtTotalCost2.Text) > 0)
                            //{
                            //    for (int i = 0; i < 2; i++)
                            //    {
                            //        objSalesInvoice.TransId = 0;
                            //        objSalesInvoice.TransactionDate = Convert.ToDateTime(txtVoucherDate.Text);
                            //        objSalesInvoice.DataFrom = "CARLOADING";
                            //        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            //        objSalesInvoice.VinId = Convert.ToInt32(ddlVIN.SelectedValue);
                            //        if (i == 0)  /* For Sale account */
                            //        {
                            //            objSalesInvoice.AcFileId = Convert.ToInt32(ddlLoader.SelectedValue);
                            //            objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(txtTotalCost2.Text);
                            //            objSalesInvoice.Description = txtRemark.Text;
                            //            objSalesInvoice.AddEditTrans();

                            //        }

                            //        if (i == 1) /* For Purchase  account */
                            //        {
                            //            objSalesInvoice.AcFileId = Convert.ToInt32(ddlLoadingExp.SelectedValue);
                            //            objSalesInvoice.TransAmt = Convert.ToDecimal(txtTotalCost2.Text);
                            //            objSalesInvoice.Description = txtRemark.Text;
                            //            objSalesInvoice.AddEditTrans();
                            //        }

                            //    }
                            //}

                            /************ Add VIn  Detail **************/


                            objCar.ContainerNoId = Convert.ToInt32(ddlContainer.SelectedValue);
                            objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            DataTable dtVIN = new DataTable();
                            dtVIN = objCar.getAllVinByContainer();
                            //if (dtVIN.Rows.Count > 0)
                            //{
                            //    DataTable dt = new DataTable();
                            //    dt = (DataTable)Session["s"];
                            //    if (dt.Rows.Count > 0)
                            //    {
                            //        decimal VINAmount = Convert.ToDecimal(dt.Compute("Sum(Amount)", ""));

                            //        for (int i = 0; i < dtVIN.Rows.Count; i++)
                            //        {
                            //            objCar.id=  0;
                            //            objCar.VinId = Convert.ToInt32(dtVIN.Rows[i]["VinId"]);
                            //            objCar.Amount = VINAmount;
                            //            objCar.LocalCustomMainId = result;
                            //            objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            //             int VInResult=objCar.AddEditLocalCustomByVinId();
                            //        }
                            //    }
                            //}
                           

                            /************ Add Invoice Detail **************/
                            if (result > 0)
                            {
                                if (Session["s"] != null)
                                {
                                    DataTable dt = new DataTable();
                                    dt = (DataTable)Session["s"];
                                    if (dt.Rows.Count > 0)
                                    {
                                        objSalesInvoice.InvoiceSummaryId = result; /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                        objSalesInvoice.DataFrom = "CARLCUSTOM";
                                        objSalesInvoice.DeleteTrans();



                                        for (int i = 0; i < dt.Rows.Count; i++)
                                        {
                                            objCar.LocalCustomDetailId = 0;
                                            objCar.CustomExpenseId = Convert.ToInt32(dt.Rows[i]["CustomExpenseId"]);
                                            objCar.ReceiptNo = Convert.ToString(dt.Rows[i]["ReceiptNo"]);
                                            objCar.DateFrom = Convert.ToString(dt.Rows[i]["DateFrom"]);
                                            objCar.DateTo = Convert.ToString(dt.Rows[i]["DateTo"]);
                                            objCar.Amount = Convert.ToDecimal(dt.Rows[i]["Amount"]);
                                            objCar.AccountDrAcfileId = Convert.ToInt32(dt.Rows[i]["AccountDrAcfileId"]);
                                            objCar.AccountCRAcfileId = Convert.ToInt32(dt.Rows[i]["AccountCRAcfileId"]);
                                            objCar.LocalCustomMainId = result;

                                            objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                            int resultDetail = 0;
                                            resultDetail = objCar.AddEditLocalCustomDetail();

                                            // Trans Entry

                                          
                                          
                                                for (int j = 0; j < 2; j++)
                                                {
                                                    objSalesInvoice.TransId = 0;
                                                    objSalesInvoice.TransactionDate = Convert.ToDateTime(txtVoucherDate.Text);
                                                    objSalesInvoice.DataFrom = "CARLCUSTOM";
                                                    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                                   
                                                    if (j == 0)  /* For Sale account */
                                                    {
                                                        objSalesInvoice.AcFileId = Convert.ToInt32(dt.Rows[i]["AccountDrAcfileId"]);
                                                        objSalesInvoice.TransAmt = Convert.ToDecimal(dt.Rows[i]["Amount"]);
                                                        objSalesInvoice.Description = Convert.ToString(dt.Rows[i]["CustomExpense"]) + ",  B/E. # : " + Convert.ToString(txtEntryNO.Text).ToUpper() + ", Container# : " + Convert.ToString(ddlContainer.SelectedItem.Text);
                                                        objSalesInvoice.AddEditTrans();

                                                    }

                                                    if (j == 1) /* For Purchase  account */
                                                    {
                                                        objSalesInvoice.AcFileId = Convert.ToInt32(dt.Rows[i]["AccountCRAcfileId"]);
                                                        objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(dt.Rows[i]["Amount"]);
                                                        objSalesInvoice.Description = Convert.ToString(dt.Rows[i]["CustomExpense"]) + ",  B/E. # : " + Convert.ToString(txtEntryNO.Text).ToUpper() + ", Container# : " + Convert.ToString(ddlContainer.SelectedItem.Text);
                                                        objSalesInvoice.AddEditTrans(); 
                                                    }

                                                }
                                            






                                            
                                        }
                                    }
                                }

                            }


                            if (Convert.ToInt32(hdnInvoiceSummaryId.Value) > 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Updated.');location.href='LocalCustoms.aspx';", true);

                            }

                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Saved.');location.href='LocalCustoms.aspx';", true);
                            }
                       // }
                    
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Date out of accouting year.');", true);

                }
            }

            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please Fill Items Details.');", true);
            }
        }
    }
    #endregion
    #region Cancel
    protected void btn_cancel_Click(object sender, ImageClickEventArgs e)
    {


        Response.Redirect("LocalCustoms.aspx");

    }
    #endregion
    #region UserRight
    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }

            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
            {
                btnDeleteInvoice.Visible = false;
            }


            if (Convert.ToString(dt.Rows[0]["IsAdd"]) != "True")
            {
                lblbutton.Visible = false;
                if (Convert.ToString(Request["addnew"]) == "1")
                {

                    ImageButton2.Visible = false;
                }
            }



            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {
                if (Convert.ToString(Request["addnew"]) != "1")
                {

                    ImageButton2.Visible = false;
                }
            }

        }
    }
    #endregion

    protected void ddlContainer_SelectedIndexChanged(object sender, EventArgs e)
    {

        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCar.LocalCustomMainId = 0;
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        ds = objCar.GetLocalCustomMain();
        dt = ds.Tables[0];
        Session["InvoiceSummary"] = dt;
        Last = dt.Rows.Count;
        if (dt.Rows.Count > 0)
        {
            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            DataView dv = new DataView(dt);
            dv.RowFilter = "ContainerNoId=" +ddlContainer.SelectedValue;
            dt = dv.ToTable();

            if (dt.Rows.Count > 0)
            {
                hdnInvoiceSummaryId.Value = Convert.ToString(dt.Rows[0]["LocalCustomMainId"]);
                Session["Corrent"] = Convert.ToString(dt.Rows[0]["row"]);
                BindInvoice();
                btnDeleteInvoice.Style.Add("display", "");

            }
            else
            {
                hdnInvoiceSummaryId.Value = "";
                Session["s"] = null;
                Session["Corrent"] = null;
                Session["FirstRecord"] = null;
                Session["LastRecord"] = null;
                Session["InvoiceSummary"] = null;
                btnDeleteInvoice.Style.Add("display", "none");
                DataTable dtBlank = new DataTable();
                dgGallery.DataSource = dtBlank;
                dgGallery.DataBind();
            }

        }

        txtEntryNO.Focus();

     }
    

}