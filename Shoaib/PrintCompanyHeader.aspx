﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true" CodeFile="PrintCompanyHeader.aspx.cs" Inherits="PrintCompanyHeader" %>

<asp:Content ID="Content2" ContentPlaceHolderID="cphead" runat="Server">
    Header Setup
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <table cellspacing="0" style="float: left" border="0" cellpadding="5" cellspacing="5">
                <tr>
                    <td width="100%" valign="top" style="left: 5; padding-top: 10px;">
                        <table style="margin-left: 10px;">
                            <tr>
                                <td align="left">
                                   Active Header :
                                </td>
                                <td align="left">
                                   <asp:CheckBox ID="chkHeader" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="height: 8px;">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:Button ID="btn_save" runat="server" Text="Update" OnClick="btn_save_Click" ValidationGroup="v" />
                                    <br />
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                        ShowSummary="False" ValidationGroup="v" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

