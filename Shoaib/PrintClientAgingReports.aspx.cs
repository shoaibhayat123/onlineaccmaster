﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintClientAgingReports : System.Web.UI.Page
{
    Reports objreport = new Reports();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();
    decimal TotalBalance = 0;
    decimal TotalTrans = 0;
    decimal SumTotalTrans = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Sessions.CustomerCode == "") || (Request["AsOn"].ToString() == "") || (Request["Acfile"].ToString() == ""))
        {
            Response.Redirect("default.aspx");
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {
            bindSupplier();
            lblFromDate.Text = ClsGetDate.FillFromDate(Request["AsOn"].ToString());
            FillLogo();
            FillCurrentBalance();
            getAgingDetails();
            lblUser.Text = Sessions.UseLoginName.ToString();
            //BindAgingResult();
        }
    }

    protected void FillCurrentBalance()
    {
        objreport.AcFileId = Convert.ToInt32(Request["Acfile"]);
        objreport.AsOn = Convert.ToDateTime(Request["AsOn"]);
        DataTable dtCorrentBalance = new DataTable();
        dtCorrentBalance = objreport.getCurrentBalance();
        rptrInvoiceReport.DataSource = dtCorrentBalance;
        rptrInvoiceReport.DataBind();
        TotalBalance = Convert.ToDecimal(dtCorrentBalance.Rows[0]["OpeningBal"]) + Convert.ToDecimal(dtCorrentBalance.Rows[0]["SumAmount"]);

    }
    protected void getAgingDetails()
    {
        objreport.AcFileId = Convert.ToInt32(Request["Acfile"]);
        objreport.AsOn = Convert.ToDateTime(Request["AsOn"]);
        DataTable dtCorrentDetails = new DataTable();
        dtCorrentDetails = objreport.getAgingDetails();
        dgGallery.DataSource = dtCorrentDetails;
        dgGallery.DataBind();

    }
    protected void dgGallery_OnItemDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (Convert.ToString(DataBinder.Eval(e.Item.DataItem, "DataFrom")) != "opbal")
            {

                Label lblInvDate = ((Label)e.Item.FindControl("lblInvDate"));
                lblInvDate.Text = ClsGetDate.FillFromDate(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "TransactionDate")));

                Label lblduedays = ((Label)e.Item.FindControl("lblduedays"));
                lblduedays.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "duedays"));

            }
            Label lblTransAmt = (Label)e.Item.FindControl("lblTransAmt");
            TotalTrans += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt"));
            if (TotalTrans > TotalBalance)
            {
                //if (((TotalBalance - SumTotalTrans) <= Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt"))) && (TotalBalance - SumTotalTrans) !=0)
                //{

                //    lblTransAmt.Text = Convert.ToString((TotalBalance - SumTotalTrans).ToString("0.00"));
                //    SumTotalTrans += (TotalBalance - SumTotalTrans);
                //}
                //else
                //{
                //    e.Item.Visible = false;
                //}


                if (((TotalBalance - SumTotalTrans) <= Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt"))))
                {
                    if ((TotalBalance - SumTotalTrans) != 0)
                    {
                        lblTransAmt.Text = Convert.ToString((TotalBalance - SumTotalTrans).ToString("0.00"));
                        SumTotalTrans += (TotalBalance - SumTotalTrans);
                    }
                    else
                    {
                        e.Item.Visible = false;
                    }
                }
                else
                {
                    e.Item.Visible = false;
                }
            }
            else
            {
                if (Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt")) != 0)
                {
                    lblTransAmt.Text = Convert.ToString((Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt")).ToString("0.00")));
                    SumTotalTrans += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "TransAmt"));
                }
                else
                {
                    e.Item.Visible = false;
                }
            }
        }
        if (e.Item.ItemType == ListItemType.Footer)
        {
            Label lblTotalTransAmt = (Label)e.Item.FindControl("lblTotalTransAmt");
            lblTotalTransAmt.Text = Convert.ToString(SumTotalTrans.ToString("0.00"));
        }

    }

    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }


    }
    #endregion
}