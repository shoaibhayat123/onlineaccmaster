﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class ItemWiseProfitReportPrint : System.Web.UI.Page
{
    
        #region properties
        SalesInvoices objSalesInvoice = new SalesInvoices();
        SetCustomers objCust = new SetCustomers();

        AccFiles objFile = new AccFiles();
        Reports objReports = new Reports();

        decimal TotalQuantity = 0;
        decimal TotalGrossAmount = 0;
        decimal TotalSaleTaxAmount = 0;
        decimal TotalAmountIncludeTaxes = 0;
        decimal TotalInvoiceAmount = 0;

        decimal GrandTotalQuantity = 0;
        decimal GrandTotalGrossAmount = 0;
        decimal GrandTotalSaleTaxAmount = 0;
        decimal GrandTotalAmountIncludeTaxes = 0;
        decimal GrandTotalInvoiceAmount = 0;


        decimal TotalGrossPurchaseAmount = 0;
     decimal TotalProfitLoss = 0;
     decimal GrandTotalGrossPurchaseAmount = 0;
     decimal GrandTotalProfitLoss = 0;


     decimal TotalSumGrossAmount = 0;
     decimal GrandSumGrossAmount = 0;
     decimal TotalSumDiscountAmount = 0;
     decimal GrandSumDiscountAmount = 0;

    

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            UpdateSession objUpdateSession = new UpdateSession();
            objUpdateSession.UpdateSessions();
            if ((Sessions.CustomerCode == "") || (Request["From"].ToString() == "") || (Request["To"].ToString() == "") || (Request["Client"].ToString() == "") || (Request["CostCenter"].ToString() == "") || (Request["Item"].ToString() == ""))
            {
                Response.Redirect("default.aspx");
            }
            if (!IsPostBack)
            {


                bindSupplier();

                lblFromDate.Text = ClsGetDate.FillFromDate(Request["From"].ToString());
                lblToDate.Text = ClsGetDate.FillFromDate(Request["To"].ToString());
                bindBuyer();
                FillLogo();
                lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
                lblUser.Text = Sessions.UseLoginName.ToString();
                bindrptrInvoiceReport();
                GrandlblTotalQuantity.Text = String.Format("{0:C}", GrandTotalQuantity).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                GrandlblTotalGrossAmount.Text = String.Format("{0:C}", GrandSumGrossAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                GrandlblGrossPurchaseAmount.Text = String.Format("{0:C}", GrandTotalGrossPurchaseAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                GrandlblProfitLoss.Text = String.Format("{0:C}", GrandTotalProfitLoss).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                GrandlbltotalNetSale.Text = String.Format("{0:C}", GrandTotalGrossAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                //GrandlblTotalSaleTaxAmount.Text = String.Format("{0:C}", GrandTotalSaleTaxAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
               // GrandlblTotalAmountIncludeTaxes.Text = String.Format("{0:C}", GrandTotalAmountIncludeTaxes).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');




            }
        }

        protected void bindBuyer()
        {



        }



        #region Grid RowDataBound
        protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }


        #endregion

        #region Fill Logo
        private void FillLogo()
        {
            DataSet ds = new DataSet();
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            ds = objCust.GetDateFormat();
            if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
            {
                imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

            }
            else
            {
                imgLogo.Style.Add("display", "none");
            }
            if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
            {
                trTrialBal.Style.Add("display", "none");
            }

        }
        #endregion
        protected void bindSupplier()
        {
            objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            DataTable dtSupplier = new DataTable();
            dtSupplier = objSalesInvoice.GetCustomerDetails();


            if (dtSupplier.Rows.Count > 0)
            {
                lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
                lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
                if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
                {
                    lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
                }
                else
                {
                    spAddress2.Style.Add("display", "none");
                }
                if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
                {
                    lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
                }
                else
                {
                    spAddress3.Style.Add("display", "none");
                }
                lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
                lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
                lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

            }
        }


        protected void rptrInvoiceReport_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Label lblInvDate = ((Label)e.Item.FindControl("lblInvDate"));
            lblInvDate.Text = ClsGetDate.FillFromDate(Convert.ToString(DataBinder.Eval(e.Item.DataItem, "InvDate")));

            Label lblInvNo = ((Label)e.Item.FindControl("lblInvNo"));
            lblInvNo.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "InvNo"));

            Label lblParty = ((Label)e.Item.FindControl("lblParty"));
            lblParty.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Title"));

            Label lblcostCenter = ((Label)e.Item.FindControl("lblcostCenter"));
            lblcostCenter.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "CostCenterName"));



            DataGrid dgGallery = ((DataGrid)e.Item.FindControl("dgGallery"));         

            objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objReports.InvoiceSummaryId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "InvoiceSummaryId"));
            objReports.DataFrom = "GSTINVSALE";
            objReports.ItemCode = Convert.ToInt32(Request["Item"]);
            objReports.storeId = Convert.ToInt32(Request["CostCenter"]);
            objReports.MainCategoryCode = Convert.ToInt32(Request["MainCategoryCode"]);
            objReports.SubCategoryCode = Convert.ToInt32(Request["SubCategoryCode"]);

            DataSet ds = new DataSet();
            TotalQuantity = 0;
            TotalGrossAmount = 0;
            TotalSaleTaxAmount = 0;
            TotalAmountIncludeTaxes = 0;
            TotalInvoiceAmount = 0;
            TotalGrossPurchaseAmount = 0;
            TotalProfitLoss=0;
            TotalSumGrossAmount = 0;
            TotalSumDiscountAmount = 0;
            ds = objReports.getInvoiceMainDetail();
            if (ds.Tables[0].Rows.Count > 0)
            {
                dgGallery.DataSource = ds.Tables[0];
                dgGallery.DataBind();

                TotalInvoiceAmount = TotalAmountIncludeTaxes + Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "CartageAmount"));
                Label lblCartage = ((Label)e.Item.FindControl("lblCartage"));
                lblCartage.Text = String.Format("{0:C}", Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "CartageAmount"))).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

                Label lblInvoiceAmount = ((Label)e.Item.FindControl("lblInvoiceAmount"));
                lblInvoiceAmount.Text = String.Format("{0:C}", Convert.ToDecimal(TotalInvoiceAmount)).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            }




        }



        protected void bindrptrInvoiceReport()
        {
           
            objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objReports.InvDateFrom = Convert.ToDateTime(Request["from"]);
            objReports.invDateTo = Convert.ToDateTime(Request["to"]);
            objReports.DataFrom = "GSTINVSALE";
            objReports.ClientAccountId = Convert.ToInt32(Request["Client"]);
            objReports.ItemCode = Convert.ToInt32(Request["Item"]);
            objReports.storeId = Convert.ToInt32(Request["CostCenter"]);
            objReports.MainCategoryCode = Convert.ToInt32(Request["MainCategoryCode"]);
            objReports.SubCategoryCode = Convert.ToInt32(Request["SubCategoryCode"]);

            DataSet ds = new DataSet();
            ds = objReports.SearchInvoiceReport();
            if (ds.Tables[0].Rows.Count > 0)
            {
                rptrInvoiceReport.DataSource = ds.Tables[0];
                rptrInvoiceReport.DataBind();
            }

        }


        protected void dgGallery_OnItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                Label lblPurchaseRate = ((Label)e.Item.FindControl("lblPurchaseRate"));
                Label lblGrossPurchaseAmount = ((Label)e.Item.FindControl("lblGrossPurchaseAmount"));
                Label lblProfitLoss = ((Label)e.Item.FindControl("lblProfitLoss"));
                Label lblProfitLossPercentage = ((Label)e.Item.FindControl("lblProfitLossPercentage"));
                Label lbldiscountamount = ((Label)e.Item.FindControl("lbldiscountamount"));
                Label lblNetAmount = ((Label)e.Item.FindControl("lblNetAmount"));
                decimal NetSaleAmount = 0;
                decimal Salediscount = 0;
                decimal PurchaseAmount = 0;
                decimal ProfitLoss = 0;
                objReports.InvDateFrom = Convert.ToDateTime(Request["from"]);
                objReports.invDateTo = Convert.ToDateTime(Request["to"]);
                objReports.ItemCode = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "ItemCode"));
                DataSet ds = new DataSet();

                ds = objReports.getPurchaseRate();
                if (ds.Tables[0].Rows.Count > 0)
                {

                   
                    Salediscount = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "GrossAmount")) * ((Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "CardChargesRate"))) / 100);
                    lbldiscountamount.Text = Convert.ToString(Salediscount.ToString("0.00"));                   
                    NetSaleAmount = Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "GrossAmount")) - Salediscount;
                    lblNetAmount.Text = Convert.ToString(NetSaleAmount.ToString("0.00"));





                    lblPurchaseRate.Text = Convert.ToString(ds.Tables[0].Rows[0]["Rate"]);
                  
                    PurchaseAmount += Convert.ToDecimal(ds.Tables[0].Rows[0]["Rate"])*Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                    TotalGrossPurchaseAmount += PurchaseAmount;
                    GrandTotalGrossPurchaseAmount += PurchaseAmount;                   
                    lblGrossPurchaseAmount.Text = String.Format("{0:C}", PurchaseAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

                  
                    ProfitLoss = NetSaleAmount - PurchaseAmount;
                    TotalProfitLoss+=ProfitLoss;
                    GrandTotalProfitLoss += ProfitLoss;
                    lblProfitLoss.Text = String.Format("{0:C}", ProfitLoss).Replace('$', ' ').Replace('(', '-').Replace(')', ' ');
                    if (PurchaseAmount > 0)
                    {
                        lblProfitLossPercentage.Text = Convert.ToDecimal((ProfitLoss / PurchaseAmount) * 100).ToString("0.00");

                    }

                }

                TotalQuantity += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                TotalGrossAmount += NetSaleAmount;
                TotalSaleTaxAmount += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "SaleTaxAmount"));
                TotalAmountIncludeTaxes += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "AmountIncludeTaxes"));
                TotalSumGrossAmount += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "GrossAmount"));
                TotalSumDiscountAmount += Salediscount;

                GrandTotalQuantity += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Quantity"));
                GrandTotalGrossAmount += NetSaleAmount;
                GrandTotalSaleTaxAmount += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "SaleTaxAmount"));
                GrandTotalAmountIncludeTaxes += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "AmountIncludeTaxes"));

                GrandSumGrossAmount += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "GrossAmount"));
                GrandSumDiscountAmount += Salediscount;


            }

            if (e.Item.ItemType == ListItemType.Footer)
            {

                Label lblTotalQuantity = ((Label)e.Item.FindControl("lblTotalQuantity"));
                lblTotalQuantity.Text = String.Format("{0:C}", TotalQuantity).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                Label lblTotalGrossAmount = ((Label)e.Item.FindControl("lblTotalGrossAmount"));
                lblTotalGrossAmount.Text = String.Format("{0:C}", TotalGrossAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                 Label lblTotalGrossPurchaseAmount = ((Label)e.Item.FindControl("lblTotalGrossPurchaseAmount"));
                 lblTotalGrossPurchaseAmount.Text = String.Format("{0:C}", TotalGrossPurchaseAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                  Label lbltotalProfitLoss = ((Label)e.Item.FindControl("lbltotalProfitLoss"));
                 lbltotalProfitLoss.Text = String.Format("{0:C}", TotalProfitLoss).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

                 Label lblSumGrossAmount = ((Label)e.Item.FindControl("lblSumGrossAmount"));
                 lblSumGrossAmount.Text = String.Format("{0:C}", TotalSumGrossAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                 Label lblSumDiscountAmount = ((Label)e.Item.FindControl("lblSumDiscountAmount"));
                 lblSumDiscountAmount.Text = String.Format("{0:C}", TotalSumDiscountAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


                 if (TotalGrossPurchaseAmount > 0)
                 {
                     Label lblTotalPercentage = ((Label)e.Item.FindControl("lblTotalPercentage"));
                     lblTotalPercentage.Text = Convert.ToDecimal((TotalProfitLoss / TotalGrossPurchaseAmount) * 100).ToString("0.00") + "%";
                 }
                
               // Label lblTotalSaleTaxAmount = ((Label)e.Item.FindControl("lblTotalSaleTaxAmount"));
               // lblTotalSaleTaxAmount.Text = String.Format("{0:C}", TotalSaleTaxAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
               // Label lblTotalAmountIncludeTaxes = ((Label)e.Item.FindControl("lblTotalAmountIncludeTaxes"));
               // lblTotalAmountIncludeTaxes.Text = String.Format("{0:C}", TotalAmountIncludeTaxes).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            }

        }
    
}