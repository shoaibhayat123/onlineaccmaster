﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaticPages.master" AutoEventWireup="true"
    CodeFile="Terms.aspx.cs" Inherits="Terms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content_wraper5">
        <div id="content_midd5">
            <div class="acc-features-middle-bg">
                <div class="top-heading">
                    <h1>
                        Terms</h1>
                </div>
            </div>
            <div class="accmiddle-features-bg-2" style="min-height: 520px">
                <div class="features-wrapper">
                    <div class="features-box">
                        <div style="float: left">
                            <div class="features-left-link-box">
                                <div class="left-link-Content">
                                    <h2 style="text-decoration: underline; font-size: 18px;">
                                        AccMaster</h2>
                                    <br />
                                    <ul style="margin-left: 20px">
                                        <li><a href="index.aspx">Home</a></li>
                                        <li><a href="features.aspx">Features</a></li>
                                        <li><a href="pricing.aspx">Product & Pricing</a></li>
                                        <li><a href="PayOnline.aspx">Pay Online</a></li>
                                        <li><a href="Privacy.aspx">Privacy</a></li>
                                        <li><a href="Terms.aspx">Terms</a></li>
                                        <li><a href="ContactUs.aspx">Contact us</a></li>
                                        <li><a href="AboutUs.aspx">About Us</a></li>
                                        <li><a href="Careers.aspx">Careers</a></li>
                                    </ul>
                                </div>
                                <div class="left-link-bottom">
                                    <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                                </div>
                            </div>
                            <div class="features-left-link-box" style="clear: both; margin-top: 20px;">
                                <div class="left-link-Content" style=" min-height:100px; line-height:0.6">
                                   
                                    <asp:Literal ID="ltrlAddress" runat="server"></asp:Literal>
                                </div>
                                <div class="left-link-bottom">
                                    <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                                </div>
                            </div>
                        </div>
                        <div class="feat-right-part-boxNew">
                            <div class="feat-middle-right-part-box">
                                <div class="feta-right-part-content">
                                    <div class="feat-right-box-dtls">
                                        <div class="ft-right-box-details" style="background: url('images/round_logo_ok.png') no-repeat scroll right top transparent;
                                            height: 200px;">
                                            <h1>
                                                Acceptance of Terms</h1>
                                            <h4>
                                                You must agree following terms and conditions,before you buy<br />
                                                any of System Development Services Products. Please review<br />
                                                the following terms and conditions, before downloading any
                                                <br />
                                                material from the System Development Services website or<br />
                                                buying of our software products.</h4>
                                        </div>
                                        <div class="what-type">
                                            <div class="what-type-image">
                                                <img src="images/Points.png" alt="" />
                                            </div>
                                            <div class="what-type-content">
                                                <h2>
                                                    Notice Specific to Software Available</h2>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    Any software that is made available to download is the copyrighted work of “System
                                                    Development Services”. Use of software is governed by the terms of the end user
                                                    license agreement that accompanies or is included with the Software (“License Agreement”).</p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    Any reproduction or redistribution of the “Software Development Services” not in
                                                    accordance with the License Agreement is expressly prohibited by law, and may result
                                                    in severe civil and criminal penalties. Violators will be prosecuted the maximum
                                                    extent possible.</p>
                                            </div>
                                        </div>
                                        <div class="what-type">
                                            <div class="what-type-image">
                                                <img src="images/Points.png" alt="" />
                                            </div>
                                            <div class="what-type-content">
                                                <h2>
                                                    NON-Refundable</h2>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    Products purchased both online and downloadable are NOT refundable.</p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    You have the chance to ‘try before you buy’. If you have not tested our free evaluation
                                                    versions from our download or online site(s), PLEASE do so before you place your
                                                    order to make sure that the product you are ordering may be helpful for you or not.</p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    Customers are encouraged to fully evaluate the software prior to purchasing a software
                                                    license. During the evaluation period, technical support is available by sending
                                                    e-mail to mail addresses given on support pages on our website.</p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    It is not possible for us to keep track installation, un-installation, removing
                                                    or deleting downloadable software on customers PC and therefore we don’t issue refunds
                                                    for downloadable products to avoid continued use after reimbursement.
                                                </p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    THERE ARE ABSOLUTELY NO CANCELLATION OF ONLINE ORDERS THAT HAVE BEEN PROGRAMMED
                                                    FOR DOWNLOAD ACCESS. NOR WILL WE ACCEPT CLAIMS OF MISSING THE IMMEDIATE ONLINE LINKS
                                                    THAT WERE PROVIDED TO YOU TO DOWNLOAD YOUR ORDER. THERE ARE NO EXCEPTIONS TO THIS
                                                    CANCELLATION POLICY.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="what-type">
                                            <div class="what-type-image">
                                                <img src="images/Points.png" alt="" />
                                            </div>
                                            <div class="what-type-content">
                                                <h2>
                                                    Fraudulent online Transaction Claims, Fraudulent Credit Card Charges or Fraudulent
                                                    Claims.</h2>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    All requests to negate charges are investigated by our technical department and
                                                    legal department. If you make a fraudulent claim of unauthorized card usage, we
                                                    will report this to credit card services. This report may result in cancellation
                                                    of your card services, put negative information on your credit report, and create
                                                    possible criminal charges that may be filed against you.</p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    We record all IP Addresses (even if you’re using IP mask) for all transactions,
                                                    investigate and prosecute all credit card frauds to the fullest extent of the International
                                                    law.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="what-type">
                                            <div class="what-type-image">
                                                <img src="images/Points.png" alt="" />
                                            </div>
                                            <div class="what-type-content">
                                                <h2>
                                                    Delivery of Product (Desktop Applications)</h2>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    After placing formal Order, You’ll receive an e-mail (Generally, within 15 minutes)
                                                    from us, then you have to send an e-mail to support department with our invoice
                                                    number and product ID number which appears on your screen after completion of demo
                                                    time, our representative will send you Serial Key number within 6-12 hours after
                                                    confirming your order. Serial Key number is a unique number for every hard disk,
                                                    we provide serial key number only once, but if you change your hard disk then product
                                                    ID will be changed and you have to enter new serial key number. New serial Key number
                                                    will be provided after receiving sufficient evidence from customer, like purchase
                                                    invoice, credit card transaction etc. Charges for new key are USD: 20/= for existing
                                                    users.
                                                </p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    The customer is responsible for providing a correct/working e-mail address. If you
                                                    have filled invalid e-mail address in order from due to typing error, please contact
                                                    by sending e-mail to email addresses given on support pages on our website.
                                                </p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    If you’ve not received Software Activation key even after 6 Hours of purchase, please
                                                    contact support team by sending e-mail to email addresses given on support pages,
                                                    on our website for resending the mail.
                                                </p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    Please make sure that our e-mails are not BLOCKED by your SPAM GUARD or our mails
                                                    are not forwarded to your Bulk/Junk/Spam mails Folder.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="what-type">
                                            <div class="what-type-image">
                                                <img src="images/Points.png" alt="" />
                                            </div>
                                            <div class="what-type-content">
                                                <h2>
                                                    Delivery of Product (Web based Applications)</h2>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    After registration of online web based accounting software, You’ll receive and email
                                                    with your customer code, user id & password and login to our web based accounting
                                                    software. You will get 30 days free trial, after 30 days you will be charged as
                                                    per number of users and package you selected. There is no contract period, you can
                                                    cancel contract with us any time.</p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    Please make sure that our e-mails are not BLOCKED by your SPAM GUARD or our mails
                                                    are not forwarded to your Bulk/Junk/Spam mails Folder.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="what-type">
                                            <div class="what-type-image">
                                                <img src="images/Points.png" alt="" />
                                            </div>
                                            <div class="what-type-content">
                                                <h2>
                                                    USE WITH YOUR MOBILE DEVICE</h2>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    Use of these Services may be available through a compatible mobile device, may require
                                                    Internet access and/or additional software. You agree that you are solely responsible
                                                    for these requirements, including any applicable changes, updates and fees as well
                                                    as the terms of your agreement with your mobile device and telecommunications provider.
                                                </p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    WE MAKE NO WARRANTIES OR REPRESENTATIONS OF ANY KIND, EXPRESS, STATUTORY OR IMPLIED
                                                    AS TO:
                                                    <ul>
                                                        <li>THE AVAILABILITY OF TELECOMMUNICATION SERVICES FROM YOUR PROVIDER AND ACCESS TO
                                                            THE SERVICES AT ANY TIME OR FROM ANY LOCATION;</li>
                                                        <li>ANY LOSS, DAMAGE, OR OTHER SECURITY INTRUSION OF THE TELECOMMUNICATION SERVICES;
                                                        </li>
                                                        <li>ANY DISCLOSURE OF INFORMATION TO THIRD PARTIES OR FAILURE TO TRANSMIT ANY DATA,
                                                            COMMUNICATIONS OR SETTINGS CONNECTED WITH THE SERVICES. </li>
                                                    </ul>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="what-type">
                                            <div class="what-type-image">
                                                <img src="images/Points.png" alt="" />
                                            </div>
                                            <div class="what-type-content">
                                                <h2>
                                                    Use of Website Materials</h2>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    <b>Software<b><br />COPYING OR REPRODUCTION OF SOFTWARE OR REDISTRIBUTION IS EXPRESSLY
                                                    PROHIBITED EXCEPT TO ANY EXTENT ALLOWED BY A RELEVANT LICENSE AGREEMENT.
                                                </p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    <b>Documents</b><br />
                                                    Permission to use Website, website content, Documents or any other System Development
                                                    Software related information is strictly prohibited unless expressly permitted by
                                                    System Development Services. Violators will be prosecuted to maximum extent possible.
                                                </p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    <b>Design/Layout</b><br />
                                                    The design or layout of accmaster.com website or any other System Development Services
                                                    owned, operated, licensed to controlled site is the property of “System Development
                                                    Services”. Elements of “System Development Services” websites are protected by copyright,
                                                    trademarks and other laws and may not be copied in whole or in part. No logo, graphic,
                                                    sound or image from any “System Development Services” website may be copied retransmitted
                                                    unless expressly permitted by “System Development Services”.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="what-type">
                                            <div class="what-type-image">
                                                <img src="images/Points.png" alt="" />
                                            </div>
                                            <div class="what-type-content">
                                                <h2>
                                                    CHANGES</h2>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    We reserve the right to change this Agreement at any time, and the changes will
                                                    be effective when posted through the Services, on our website for the Services or
                                                    when we notify you by other means. We may also change or discontinue the Services,
                                                    in whole or in part. Your continued use of the Services indicates your agreement
                                                    to the changes.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="what-type">
                                            <div class="what-type-image">
                                                <img src="images/Points.png" alt="" />
                                            </div>
                                            <div class="what-type-content">
                                                <h2>
                                                    Legal Disclaimers</h2>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    The information on the website content is provided “as is” with all faults and without
                                                    warranty of any kind, expressed or implied, including those of merchantability and
                                                    fitness for a particular purpose, or arising from a course of dealing, usage or
                                                    trade practice. System Development Services makes no warranties or representations
                                                    regarding the accuracy or completeness of the information. Any information about
                                                    competitive products is not to be used or distribution where prohibited by law.
                                                </p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    In no event shall “System Development Services” be liable for any incidental or
                                                    consequential damages, lost profits, or lost data, or any indirect damages even
                                                    if “System Development Services” has been informed of the possibility thereof. THE
                                                    SOFTWARE IS WARRANTED, IF AT ALL, ONLY ACCORDING TO THE TERMS OF THE LICENSE AGREEMENT.
                                                    EXCEPT AS WARRANTED IN THE LICENSE AGREEMENT, ACCMASTER HEREBY DISMAILMS ALL WARRANTIES
                                                    AND CONDITIONS WITH REGARD TO THE SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES AND
                                                    CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NO-INFRINGEMENT.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="what-type">
                                            <div class="what-type-image">
                                                <img src="images/Points.png" alt="" />
                                            </div>
                                            <div class="what-type-content">
                                                <h2>
                                                    Contacting Company</h2>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    You may contact the company for customer support of service by sending e-mail to
                                                    email addresses given on support pages on our website.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="what-type">
                                            <div class="what-type-image">
                                                <img src="images/Points.png" alt="" />
                                            </div>
                                            <div class="what-type-content">
                                                <h2>
                                                    All Rights Reserved</h2>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    “System Development Services” reserves the rights to modify these Terms and Conditions
                                                    and other policies at its discretion or against any customer it believes is abusing
                                                    Terms and Conditions and policies.
                                                </p>
                                                <p style="color: #3C481C; font-size: 13px; line-height: normal;">
                                                    Any such revision or change will be binding and effective immediately after posting
                                                    of the revised terms and conditions and policies on Web sites. You agree to review
                                                    our websites including the current version of our terms and Conditions
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="feat-right-part-bottom">
                                <img src="images/feat-right-part-bottom-bg.jpg" alt="" />
                            </div>
                        </div>
</asp:Content>
