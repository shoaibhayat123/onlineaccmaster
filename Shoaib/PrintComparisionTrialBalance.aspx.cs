﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintComparisionTrialBalance : System.Web.UI.Page
{
    clsCookie ck = new clsCookie();
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    decimal TotalOpBalDebit = 0;
    decimal TotalOpBalCredit = 0;
    decimal TotalOpBalDebitRow = 0;
    decimal TotalOpBalCreditRow = 0;

    decimal TotalTransDebit = 0;
    decimal TotallTransCredit = 0;
    decimal TotallTransDebitRow = 0;
    decimal TotallTransCreditRow = 0;

    decimal TotalClosingDebit = 0;
    decimal TotalClosingCredit = 0;
    decimal TotalClosingDebitRow = 0;
    decimal TotalClosingCreditRow = 0;



    decimal TotalTransOpeningBal = 0;
    decimal TotalTransAmount = 0;
    decimal TotalTransClosingAmount = 0;


    decimal DiffTransOpeningBal = 0;
    decimal DiffTransAmount = 0;
    decimal DiffTransClosingAmount = 0;


    decimal TotalAmount = 0;
    SetCustomers objCust = new SetCustomers();
    DataTable dttbl = new DataTable();
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();

        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {

            if (Request["To"].ToString() != "" || (Request["From"].ToString() == "") | (Request["Type"].ToString() == ""))
            {
                Session["s"] = null;
                bindSupplier();
                GetSequenceTrialBalance();
                FillLogo();
                lblFromDate.Text = ClsGetDate.FillFromDate(Request["From"].ToString());
                lblToDate.Text = ClsGetDate.FillFromDate(Request["To"].ToString());
                lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
                lblUser.Text = Sessions.UseLoginName.ToString();
            }
        }
    }
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }


    }
    #endregion
    protected void GetSequenceTrialBalance()
    {

        //DataSet ds = new DataSet();
        //string sqlCommand = "proc_GetSequenceTrialBalance";
        //Database db = DatabaseFactory.CreateDatabase();
        //DBCommandWrapper dbCw = db.GetStoredProcCommandWrapper(sqlCommand);
        //dbCw.AddInParameter("@CustomerCode", DbType.Decimal, Convert.ToDecimal(Sessions.CustomerCode));
        //ds = db.ExecuteDataSet(dbCw);
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objSalesInvoice.GetSequenceTrialBalance();

        DataView dv = ds.Tables[0].DefaultView;
        dv.RowFilter = "depth = '1'";
        DataTable dt1 = new DataTable();
        dt1 = dv.ToTable();

        DataView dv2 = ds.Tables[0].DefaultView;
        dv2.RowFilter = "depth = '2'";
        DataTable dt2 = new DataTable();
        dt2 = dv2.ToTable();


        DataView dv3 = ds.Tables[0].DefaultView;
        dv.RowFilter = "depth = '3'";
        DataTable dt3 = new DataTable();
        dt3 = dv3.ToTable();


        DataView dv4 = ds.Tables[0].DefaultView;
        dv4.RowFilter = "depth = '4'";
        DataTable dt4 = new DataTable();
        dt4 = dv4.ToTable();

        DataView dv5 = ds.Tables[0].DefaultView;
        dv5.RowFilter = "depth = '5'";
        DataTable dt5 = new DataTable();
        dt5 = dv5.ToTable();


        DataView dv6 = ds.Tables[0].DefaultView;
        dv6.RowFilter = "depth = '6'";
        DataTable dt6 = new DataTable();
        dt6 = dv6.ToTable();

        filltable();
        string test = "";
        for (int i = 0; i < dt1.Rows.Count; i++)
        {
            // test += + ",";
            AddRecord(Convert.ToInt32(dt1.Rows[i]["Id"]), Convert.ToString(dt1.Rows[i]["Title"]), Convert.ToInt32(dt1.Rows[i]["ParrentId"]), Convert.ToInt32(dt1.Rows[i]["depth"]));
            for (int j = 0; j < dt2.Rows.Count; j++)
            {
                if (dt2.Rows[j]["ParrentId"].ToString() == dt1.Rows[i]["Id"].ToString())
                {
                    // test += dt2.Rows[j]["Title"].ToString() + ",";
                    AddRecord(Convert.ToInt32(dt2.Rows[j]["Id"]), Convert.ToString(dt2.Rows[j]["Title"]), Convert.ToInt32(dt2.Rows[j]["ParrentId"]), Convert.ToInt32(dt2.Rows[j]["depth"]));
                    for (int k = 0; k < dt3.Rows.Count; k++)
                    {
                        if (dt3.Rows[k]["ParrentId"].ToString() == dt2.Rows[j]["Id"].ToString())
                        {
                            // test += dt3.Rows[k]["Title"].ToString() + ",";
                            AddRecord(Convert.ToInt32(dt3.Rows[k]["Id"]), Convert.ToString(dt3.Rows[k]["Title"]), Convert.ToInt32(dt3.Rows[k]["ParrentId"]), Convert.ToInt32(dt3.Rows[k]["depth"]));
                            for (int l = 0; l < dt4.Rows.Count; l++)
                            {

                                if (dt4.Rows[l]["ParrentId"].ToString() == dt3.Rows[k]["Id"].ToString())
                                {

                                    // test += dt4.Rows[l]["Title"].ToString() + ",";
                                    AddRecord(Convert.ToInt32(dt4.Rows[l]["Id"]), Convert.ToString(dt4.Rows[l]["Title"]), Convert.ToInt32(dt4.Rows[l]["ParrentId"]), Convert.ToInt32(dt4.Rows[l]["depth"]));
                                    for (int m = 0; m < dt5.Rows.Count; m++)
                                    {
                                        if (dt5.Rows[m]["ParrentId"].ToString() == dt4.Rows[l]["Id"].ToString())
                                        {

                                            // test += dt5.Rows[m]["Title"].ToString() + ",";
                                            AddRecord(Convert.ToInt32(dt5.Rows[m]["Id"]), Convert.ToString(dt5.Rows[m]["Title"]), Convert.ToInt32(dt5.Rows[m]["ParrentId"]), Convert.ToInt32(dt5.Rows[m]["depth"]));
                                            for (int n = 0; n < dt6.Rows.Count; n++)
                                            {
                                                if (dt6.Rows[n]["ParrentId"].ToString() == dt5.Rows[m]["Id"].ToString())
                                                {
                                                    AddRecord(Convert.ToInt32(dt6.Rows[n]["Id"]), Convert.ToString(dt6.Rows[n]["Title"]), Convert.ToInt32(dt6.Rows[n]["ParrentId"]), Convert.ToInt32(dt6.Rows[n]["depth"]));
                                                    // test += dt6.Rows[n]["Title"].ToString() + ",";
                                                }
                                            }

                                        }

                                    }

                                }

                            }
                        }


                    }
                }
            }

        }

        grdInvoice.DataSource = dttbl;
        grdInvoice.DataBind();
        grandTotalOpBalDebit.Text = String.Format("{0:C}", TotalOpBalDebit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        grandTotalOpBalCredit.Text = String.Format("{0:C}", TotalOpBalCredit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

        grandTotalTransDebit.Text = String.Format("{0:C}", TotalTransDebit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        grandTotalTransCredit.Text = String.Format("{0:C}", TotallTransCredit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

        grandTotalClosingDebit.Text = String.Format("{0:C}", TotalClosingDebit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        grandTotalClosingCredit.Text = String.Format("{0:C}", TotalClosingCredit).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');



        DiffTransOpeningBal = TotalOpBalDebit + TotalOpBalCredit;
        DiffTransAmount = TotalTransDebit + TotallTransCredit;
        DiffTransClosingAmount = TotalClosingDebit + TotalClosingCredit;

        lblDiffOpBal.Text = String.Format("{0:C}", DiffTransOpeningBal).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        lblDiffTrans.Text = String.Format("{0:C}", DiffTransAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        lblDiffClosing.Text = String.Format("{0:C}", DiffTransClosingAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

    }




    #region Grid RowDataBound
    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {


        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblOpBalDebit = ((Label)e.Row.FindControl("lblOpBalDebit"));
            Label lblOpBalCredit = ((Label)e.Row.FindControl("lblOpBalCredit"));

            Label lblTransDebit = ((Label)e.Row.FindControl("lblTransDebit"));
            Label lblTransCredit = ((Label)e.Row.FindControl("lblTransCredit"));


            Label lblClosingDebit = ((Label)e.Row.FindControl("lblClosingDebit"));
            Label lblClosingCredit = ((Label)e.Row.FindControl("lblClosingCredit"));


            string OpeningBal = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "OpeningBal"));
            string TransOpeningBal = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TransOpeningBal"));
            string TransAmount = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TransAmount"));
            string TransClosingAmount = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "TransClosingAmount"));

            if (OpeningBal == "")
            {
                OpeningBal = "0.00";
            }
            if (TransOpeningBal == "")
            {
                TransOpeningBal = "0.00";
            }
            if (TransAmount == "")
            {
                TransAmount = "0.00";
            }
            if (TransClosingAmount == "")
            {
                TransClosingAmount = "0.00";
            }


            TotalTransOpeningBal = Convert.ToDecimal(OpeningBal) + Convert.ToDecimal(TransOpeningBal);

            if (TotalTransOpeningBal > 0)
            {
                lblOpBalDebit.Text = String.Format("{0:C}", TotalTransOpeningBal).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ').Replace('-', ' ');
                lblOpBalCredit.Text = "0.00";
                TotalOpBalDebit = TotalOpBalDebit + TotalTransOpeningBal;
                TotalOpBalDebitRow = TotalOpBalDebitRow + TotalTransOpeningBal;

            }
            else
            {
                lblOpBalCredit.Text = String.Format("{0:C}", TotalTransOpeningBal).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ').Replace('-', ' ');
                lblOpBalDebit.Text = "0.00";
                TotalOpBalCredit = TotalOpBalCredit + TotalTransOpeningBal;
                TotalOpBalCreditRow = TotalOpBalCreditRow + TotalTransOpeningBal;
            }

            TotalTransAmount = Convert.ToDecimal(TransAmount);
            if (TotalTransAmount > 0)
            {
                lblTransDebit.Text = String.Format("{0:C}", TotalTransAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ').Replace('-', ' ');
                lblTransCredit.Text = "0.00";



                TotalTransDebit = TotalTransDebit + TotalTransAmount;
                TotallTransDebitRow = TotallTransDebitRow + TotalTransAmount;

            }
            else
            {
                lblTransCredit.Text = String.Format("{0:C}", TotalTransAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ').Replace('-', ' ');
                lblTransDebit.Text = "0.00";
                TotallTransCredit = TotallTransCredit + TotalTransAmount;
                TotallTransCreditRow = TotallTransCreditRow + TotalTransAmount;
            }


            TotalTransClosingAmount = Convert.ToDecimal(OpeningBal) + Convert.ToDecimal(TransClosingAmount);
            if (TotalTransClosingAmount > 0)
            {
                lblClosingDebit.Text = String.Format("{0:C}", TotalTransClosingAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ').Replace('-', ' ');
                lblClosingCredit.Text = "0.00";
                TotalClosingDebit = TotalClosingDebit + TotalTransClosingAmount;
                TotalClosingDebitRow = TotalClosingDebitRow + TotalTransClosingAmount;
            }
            else
            {

                lblClosingCredit.Text = String.Format("{0:C}", TotalTransClosingAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ').Replace('-', ' ');
                lblClosingDebit.Text = "0.00";

                TotalClosingCredit = TotalClosingCredit + TotalTransClosingAmount;
                TotalClosingCreditRow = TotalClosingCreditRow + TotalTransClosingAmount;
            }


            decimal CheckZero = (1* TotalTransOpeningBal) + (1*TotalTransAmount) + (1*TotalTransClosingAmount);
            if (CheckZero == 0 && Request["Type"].ToString() == "2")
            {
                e.Row.Style.Add("display", "none");
            }

        }



        if (e.Row.RowType == DataControlRowType.Footer)  // For footer totals
        {
            Label lblTotalOpBalDebit = (Label)e.Row.FindControl("lblTotalOpBalDebit");
            lblTotalOpBalDebit.Text = String.Format("{0:C}", TotalOpBalDebitRow).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            Label lblTotalOpBalCredit = (Label)e.Row.FindControl("lblTotalOpBalCredit");
            lblTotalOpBalCredit.Text = String.Format("{0:C}", TotalOpBalCreditRow).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            Label lblTotalTransDebit = (Label)e.Row.FindControl("lblTotalTransDebit");
            lblTotalTransDebit.Text = String.Format("{0:C}", TotallTransDebitRow).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            Label lblTotalTransCredit = (Label)e.Row.FindControl("lblTotalTransCredit");
            lblTotalTransCredit.Text = String.Format("{0:C}", TotallTransCreditRow).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            Label lblTotalClosingDebit = (Label)e.Row.FindControl("lblTotalClosingDebit");
            lblTotalClosingDebit.Text = String.Format("{0:C}", TotalClosingDebitRow).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            Label lblTotalClosingCredit = (Label)e.Row.FindControl("lblTotalClosingCredit");
            lblTotalClosingCredit.Text = String.Format("{0:C}", TotalClosingCreditRow).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


        }


    }
    #endregion
    #region Fill session Table
    public void filltable()
    {
        if (Session["s"] == null)
        {
            DataColumn column = new DataColumn();
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Title";
            column.Caption = "Title";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "ParrentId";
            column.Caption = "ParrentId";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "depth";
            column.Caption = "depth";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);




        }
        else
        {
            dttbl = (DataTable)Session["s"];
        }
    }

    protected void AddRecord(int Id, string Title, int ParrentId, int depth)
    {
        DataRow row;
        row = dttbl.NewRow();
        row["Id"] = Id;
        row["Title"] = Title;
        row["ParrentId"] = ParrentId;
        row["depth"] = depth;


        dttbl.Rows.Add(row);
        dttbl.AcceptChanges();
        Session.Add("s", dttbl);
    }
    #endregion



    protected void grdInvoice_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnid = ((HiddenField)e.Row.FindControl("hdnid"));
            hdnid.Value = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Id"));


            objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objSalesInvoice.AcFileId = Convert.ToInt32(hdnid.Value);
            objSalesInvoice.DateFrom = Convert.ToDateTime(Request["From"]);
            objSalesInvoice.DateTo = Convert.ToDateTime(Request["To"]);



            DataSet ds = objSalesInvoice.GetComparisionTrialBalance();
            GridView grdLedger = (GridView)e.Row.FindControl("grdLedger");
            if (ds.Tables[0].Rows.Count > 0)
            {

                TotalOpBalDebitRow = 0;
                TotalOpBalCreditRow = 0;


                TotallTransDebitRow = 0;
                TotallTransCreditRow = 0;


                TotalClosingDebitRow = 0;
                TotalClosingCreditRow = 0;

                if (Request["Type"].ToString() == "1")
                {
                    grdLedger.DataSource = ds.Tables[0];
                    grdLedger.DataBind();
                }
                else
                {
                    decimal ReportTotalTransOpeningBal = 0; ;
                    decimal ReportTotalTransAmount = 0; ;
                    decimal ReportTotalTransClosingAmount = 0;
                    decimal ReportCheckZero = 0;
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        TotalOpBalDebitRow = 0;
                        TotalOpBalCreditRow = 0;


                        TotallTransDebitRow = 0;
                        TotallTransCreditRow = 0;


                        TotalClosingDebitRow = 0;
                        TotalClosingCreditRow = 0;

                        string OpeningBal = Convert.ToString(ds.Tables[0].Rows[i]["OpeningBal"]);
                        string TransOpeningBal = Convert.ToString(ds.Tables[0].Rows[i]["TransOpeningBal"]);
                        string TransAmount = Convert.ToString(ds.Tables[0].Rows[i]["TransAmount"]);
                        string TransClosingAmount = Convert.ToString(ds.Tables[0].Rows[i]["TransClosingAmount"]); 

                        if (OpeningBal == "")
                        {
                            OpeningBal = "0.00";
                        }
                        if (TransOpeningBal == "")
                        {
                            TransOpeningBal = "0.00";
                        }
                        if (TransAmount == "")
                        {
                            TransAmount = "0.00";
                        }
                        if (TransClosingAmount == "")
                        {
                            TransClosingAmount = "0.00";
                        }
                         ReportTotalTransOpeningBal += Convert.ToDecimal(OpeningBal) + Convert.ToDecimal(TransOpeningBal);
                         ReportTotalTransAmount += Convert.ToDecimal(TransAmount);
                         ReportTotalTransClosingAmount += Convert.ToDecimal(OpeningBal) + Convert.ToDecimal(TransClosingAmount);
                        
                    }

                   if( ReportTotalTransOpeningBal ==0 &&  ReportTotalTransAmount == 0 && ReportTotalTransClosingAmount==0)
                    {
                        Label lblTitle = (Label)e.Row.FindControl("lblTitle");
                        lblTitle.Style.Add("display", "none");
                      
                    }
                    else
                    {
                        grdLedger.DataSource = ds.Tables[0];
                        grdLedger.DataBind();
                    }

                }

            }
        }

    }
}
