﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class SalesTaxReturnsInvoiceReport : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        if (!IsPostBack)
        {
            txtInvNo.Focus();
            FillClientAC();
            FillCostCenter();
            txtInvoiceDateFrom.Text = Convert.ToDateTime(Sessions.FromDate).ToString("dd MMM yyyy");
            txtInvoiceDateTo.Text = System.DateTime.Now.ToString("dd MMM yyyy");

        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        objSalesInvoice.invDateTo = Convert.ToDateTime(txtInvoiceDateTo.Text);
        objSalesInvoice.InvDateFrom = Convert.ToDateTime(txtInvoiceDateFrom.Text);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.ClientAccountId = Convert.ToInt32(ddlClientAC.SelectedValue);
        objSalesInvoice.DataFrom = "GSTINVSALERET";
        objSalesInvoice.InvNo = txtInvNo.Text;
        objSalesInvoice.CostCenterCode = Convert.ToInt32(ddlCostCenter.SelectedValue);
        DataTable dt = new DataTable();
        dt = objSalesInvoice.SearchInvoice();
        dgGallery.DataSource = dt;
        dgGallery.DataBind();




    }

    #region Fill  Client A/C
    protected void FillClientAC()
    {
       
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtClient = new DataTable();
            dtClient = objSalesInvoice.getAcFileBySundryDebtorOnly();
            if (dtClient.Rows.Count > 0)
            {
                ddlClientAC.DataSource = dtClient;
                ddlClientAC.DataTextField = "Title";
                ddlClientAC.DataValueField = "Id";
                ddlClientAC.DataBind();
                ddlClientAC.Items.Insert(0, new ListItem("Select Client A/c", "0"));

            }
      

    }
    #endregion

    protected void dgGallery_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            if (DataBinder.Eval(e.Row.DataItem, "InvDate").ToString() != "")
            {
                Label lblInvDate = (Label)e.Row.FindControl("lblinvDate");
                lblInvDate.Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Row.DataItem, "InvDate").ToString());
            }

            if (Sessions.UserRole.ToString() != "Admin")
            {


                Button btnEdit = (Button)e.Row.FindControl("btnEdit");

                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
                if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
                {
                    Response.Redirect(Request.UrlReferrer.ToString());
                }

                if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
                {

                   // btnEdit.Visible = false;

                }

            }

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            objSalesInvoice.invDateTo = Convert.ToDateTime(txtInvoiceDateTo.Text);
            objSalesInvoice.InvDateFrom = Convert.ToDateTime(txtInvoiceDateFrom.Text);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.ClientAccountId = Convert.ToInt32(ddlClientAC.SelectedValue);
            objSalesInvoice.DataFrom = "GSTINVSALERET";
            objSalesInvoice.InvNo = txtInvNo.Text;
            objSalesInvoice.CostCenterCode = Convert.ToInt32(ddlCostCenter.SelectedValue);

            DataTable dt = new DataTable();
            dt = objSalesInvoice.SearchInvoice();

            Label lblTotalGrossAmount = (Label)e.Row.FindControl("lblTotalGrossAmount");
            decimal TotalGrossAmount = (decimal)dt.Compute("SUM(TotalGrossAmount)", "");
            lblTotalGrossAmount.Text = String.Format("{0:C}", TotalGrossAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblTotalSaleTaxAmount = (Label)e.Row.FindControl("lblTotalSaleTaxAmount");
            decimal TotalSaleTaxAmount = (decimal)dt.Compute("SUM(TotalSaleTaxAmount)", "");
            lblTotalSaleTaxAmount.Text = String.Format("{0:C}", TotalSaleTaxAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            Label lblTotalAmountIncludeTax = (Label)e.Row.FindControl("lblTotalAmountIncludeTax");
            decimal TotalAmountIncludeTax = (decimal)dt.Compute("SUM(TotalAmountIncludeTax)", "");
            lblTotalAmountIncludeTax.Text = String.Format("{0:C}", TotalAmountIncludeTax).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblBillAmount = (Label)e.Row.FindControl("lblBillAmount");
            decimal BillAmount = (decimal)dt.Compute("SUM(BillAmount)", "");
            lblBillAmount.Text = String.Format("{0:C}", BillAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            Label lblCartageAmount = (Label)e.Row.FindControl("lblCartageAmount");
            decimal CartageAmount = (decimal)dt.Compute("SUM(CartageAmount)", "");
            lblCartageAmount.Text = String.Format("{0:C}", CartageAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


        }

    }
    #region Fill Cost Center
    protected void FillCostCenter()
    {
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.CostCenterCode = 0;
            DataSet ds = new DataSet();
            ds = objCust.GetCostCenter();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlCostCenter.DataSource = ds.Tables[0];
                ddlCostCenter.DataTextField = "CostCenterName";
                ddlCostCenter.DataValueField = "CostCenterCode";
                ddlCostCenter.DataBind();
                ddlCostCenter.Items.Insert(0, new ListItem("Select cost center", "0"));

            }

       

    }
    #endregion
    protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditCustomer")
        {
            Response.Redirect("SalesTaxReturnsInvoice.aspx?InvoiceSummaryId=" + e.CommandArgument.ToString());
        }

    }
}
