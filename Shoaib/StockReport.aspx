﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="StockReport.aspx.cs" Inherits="StockReport" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Stock Ledger"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
     >
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                Product :
            </td>
            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                <asp:DropDownList ID="ddlItemDesc" runat="server" Style="width: 415px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                    InitialValue="0" ErrorMessage="Please select Product " Text="*" ControlToValidate="ddlItemDesc"
                    ValidationGroup="Ledger"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                Starting Date :
            </td>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                <asp:TextBox ID="txtstartDate" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtstartDate'),this);"
                    runat="server" Width="100px"></asp:TextBox>
                <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                    onclick="scwShow(scwID('ctl00_MainHome_txtstartDate'),this);" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                    ErrorMessage="Please Fill Starting  Date." Text="*" ControlToValidate="txtstartDate"
                    ValidationGroup="Ledger"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                    ControlToValidate="txtstartDate" ValidationGroup="Ledger" ErrorMessage="Invalid Invoice Date !!"
                    Display="None"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                End Date :
            </td>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                <asp:TextBox ID="txtEndDate" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtEndDate'),this);"
                    runat="server" Width="100px"></asp:TextBox>
                <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                    onclick="scwShow(scwID('ctl00_MainHome_txtEndDate'),this);" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                    ErrorMessage="Please Fill  End  Date ." Text="*" ControlToValidate="txtEndDate"
                    ValidationGroup="Ledger"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                    ControlToValidate="txtEndDate" ValidationGroup="Ledger" ErrorMessage="Invalid Invoice Date !!"
                    Display="None"></asp:RegularExpressionValidator>
                      <asp:Button ID="btnGenerate" Text="Generate" ValidationGroup="Ledger"
                    runat="server" OnClick="btnGenerate_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="Ledger" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
              
            </td>
        </tr>
    </table>
</asp:Content>
