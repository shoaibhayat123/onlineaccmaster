﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintJournalVouchersFc.aspx.cs"
    Inherits="PrintJournalVouchersFc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Tahoma; background: none; font-size: 10px;">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; font-weight: bold;">
            <tr id="trTrialBal" runat="server">
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Journal
                        Voucher<br />
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <hr />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table>
            <tr>
                <td style="text-align: right">
                    <b>Voucher # :</b>
                </td>
                <td>
                    <asp:Label ID="lblVoucherNo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <b>Date :</b>
                </td>
                <td>
                    <asp:Label ID="lblVoucherDate" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
            AllowPaging="false" PageSize="30" ShowFooter="true" FooterStyle-CssClass="gridFooter"
            PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader" AlternatingItemStyle-CssClass="gridAlternateItem"
            GridLines="none" BorderColor="black" BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem"
            OnItemDataBound="dgGallery_RowDataBound">
            <Columns>
                <asp:TemplateColumn HeaderText="S.No.">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                    <ItemTemplate>
                        <%# Container.DataSetIndex + 1%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="A/C Code">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Center" />
                    <ItemTemplate>
                        <%# Eval("AccountId")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="A/C Title">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <%# Eval("AccountDesc")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Description">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="30%" HorizontalAlign="left" />
                    <ItemTemplate>
                        <%# Eval("Description")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="F.C. Debit Amount">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("FCAmountDebit")).Replace('$', ' ')%>
                    </ItemTemplate>
                        <FooterTemplate>
                        <asp:Label ID="lblTotalDebitFc" runat="server"></asp:Label>
                    </FooterTemplate>

                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="F.C. Debit Rate">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Right" Width="8%" />
                    <ItemTemplate>
                        <%# Convert.ToDecimal(Eval("FCRateDebit")).ToString("0.0000")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="F.C. Credit Amount">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("FCAmountCredit")).Replace('$', ' ')%>
                    </ItemTemplate>
                      <FooterTemplate>
                        <asp:Label ID="lblTotalCreditFc" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="F.C. Credit Rate">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Right" Width="8%" />
                    <ItemTemplate>
                        <%# Convert.ToDecimal(Eval("FCRateCredit")).ToString("0.0000")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Debit Amount">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Right" />
                   <%-- <FooterStyle HorizontalAlign="Right" Font-Size="14px" />--%>
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("DebitAmount")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalDebit" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Credit Amount">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Right" />
                   <%-- <FooterStyle HorizontalAlign="Right" Font-Size="14px" />--%>
                    <ItemTemplate>
                        <%#String.Format("{0:C}", Eval("CreditAmount")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalCredit" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <b>
                        <asp:Label ID="lblCurrency" runat="server"></asp:Label>
                        :</b>
                    <asp:Label ID="lblTotalEng" runat="server"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="800px">
                        <tr>
                            <td style="padding: 10px; text-align: center">
                                <hr />
                                <br />
                                Prepared By
                            </td>
                            <td style="padding: 10px; text-align: center">
                                <hr />
                                <br />
                                Authorized By
                            </td>
                            <td style="padding: 10px; text-align: center">
                                <hr />
                                <br />
                                Approved By
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    <asp:LinkButton Text="Print" ID="lnkPrint" OnClientClick="window.print();" runat="server"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td style="text-align: center" colspan="4">
                    <hr />
                    <table style="width: 780px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>
                                <script type="text/javascript">

                                    var currentTime = new Date()
                                    var hours = currentTime.getHours()
                                    var Newhours = 0;
                                    var minutes = currentTime.getMinutes()
                                    if (minutes < 10) {
                                        minutes = "0" + minutes
                                    }
                                    if (hours > 12) {
                                        Newhours = hours - 12;
                                    }
                                    else {
                                        Newhours = hours;
                                    }
                                    document.write(Newhours + ":" + minutes + " ")
                                    if (hours > 11) {
                                        document.write("PM")
                                    } else {
                                        document.write("AM")
                                    }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
