﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class MainCategory : System.Web.UI.Page
{
    #region DataMembers & Varriables
    SetCustomers objCust = new SetCustomers();
    #endregion


    #region Page Load Event

    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (!IsPostBack)
        {
            FillMainCategory();
        }
    }

    #endregion

    #region Add/ Edit Main Category  Setup

    protected void btn_save_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
        if (hdnMainCategoryCode.Value != null && hdnMainCategoryCode.Value != string.Empty)
        {
            objCust.MainCategoryCode = int.Parse(hdnMainCategoryCode.Value);
        }
        else
        {
            objCust.MainCategoryCode = 0;
        }
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.MainCategoryName = Convert.ToString(txtMainCategoryName.Text);
       // objCust.ACFileID = 1;
        if (Sessions.UserRole.ToString() != "Admin")
        {
            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }
            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" || Convert.ToString(dt.Rows[0]["IsAdd"]) == "True")
            {
               


                if (Convert.ToString(dt.Rows[0]["IsAdd"]) == "True" && (hdnMainCategoryCode.Value == null || hdnMainCategoryCode.Value == string.Empty))
                {
                    int result = objCust.SetMainCategory();
                    if (result != -1)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Main Category Setup successful added!');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                    }
                    clear();
                    FillMainCategory();
                }
                else
                {
                    if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" && hdnMainCategoryCode.Value != null && hdnMainCategoryCode.Value != string.Empty)
                    {
                        int result = objCust.SetMainCategory();
                        if (result != -1)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Main Category Setup successful updated!');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                        }
                        clear();
                        FillMainCategory();

                    }

                    else
                    {
                        if (hdnMainCategoryCode.Value != null && hdnMainCategoryCode.Value != string.Empty)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='MainCategory.aspx';", true);
                        }

                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='MainCategory.aspx';", true);
                        }
                    }
                }

            }
            else
            {
                if (hdnMainCategoryCode.Value != null && hdnMainCategoryCode.Value != string.Empty)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='MainCategory.aspx';", true);
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='MainCategory.aspx';", true);
                }
            }

        }

        else
        {
            int result = objCust.SetMainCategory();
            if (objCust.MainCategoryCode != 0)
            {
                if (result != -1)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Main Category Setup successful updated!');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                }
            }
            else
            {
                if (result != -1)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Main Category Setup successful added!');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                }
            }
            clear();
            FillMainCategory();
        }






    }

    #endregion

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        clear();
    }
    #endregion

    #region Blank Controls
    private void clear()
    {

        txtMainCategoryName.Text = string.Empty;
        hdnMainCategoryCode.Value = string.Empty;

    }
    #endregion


    #region Fill Main Category  Setup
    protected void FillData(int MainCategoryCode)
    {
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.MainCategoryCode = MainCategoryCode;
        DataSet ds = new DataSet();
        ds = objCust.GetMainCategory();
        if (ds.Tables[0].Rows.Count > 0)
        {
            txtMainCategoryName.Text = Convert.ToString(ds.Tables[0].Rows[0]["MainCategoryName"].ToString());
            hdnMainCategoryCode.Value = MainCategoryCode.ToString();
        }

    }
    #endregion

    #region Fill Main Category  Setup
    protected void FillMainCategory()
    {
       
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.MainCategoryCode = 0;
            DataSet ds = new DataSet();
            ds = objCust.GetMainCategory();
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 30)
                {
                    grdCustomer.AllowPaging = true;
                }
                grdCustomer.Visible = true;
                grdCustomer.DataSource = ds.Tables[0];
                grdCustomer.DataBind();
                lblError.Visible = false;
            }
            else
            {
                grdCustomer.Visible = false;
                lblError.Visible = true;
            }
       

    }
    #endregion

    #region GridView Events
    protected void grdCustomer_RowCommand(object sender, DataGridCommandEventArgs e)
    {


        if (e.CommandName == "Delete")
        {
            objCust.MainCategoryCode = int.Parse(e.CommandArgument.ToString());
            int result = objCust.DeleteMainCategory();
           
            FillMainCategory();
            if (result == 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('First of all delete sub category!');", true);
              
            }
        }

        if (e.CommandName == "Edit")
        {
            FillData(int.Parse(e.CommandArgument.ToString()));

        }
    }
    protected void grdCustomer_RowDataBound(object sender, DataGridItemEventArgs e)
    {
       
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
              

                if (Sessions.UserRole.ToString() != "Admin")
                {


                    LinkButton lbEdit = (LinkButton)e.Item.FindControl("lbEdit");
                    LinkButton lbDelete = (LinkButton)e.Item.FindControl("lbDelete");

                    string PageUrl = Request.Url.ToString();
                    string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                    for (int i = 0; i < splitPageUrl.Length; i++)
                    {
                        if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                        {
                            PageUrl = Convert.ToString(splitPageUrl[i]);
                        }
                    }

                    User ObjUser = new User();
                    ObjUser.PageName = PageUrl;
                    ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                    DataTable dt = ObjUser.getPageUserRights();
                    if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
                    {
                        Response.Redirect(Request.UrlReferrer.ToString());
                    }
                    if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
                    {

                        lbDelete.Visible = false;

                    }
                    if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
                    {

                        lbEdit.Visible = false;

                    }

                }
            }


       

    }

    protected void grdCustomer_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        grdCustomer.CurrentPageIndex = e.NewPageIndex;
        FillMainCategory();

    }
    #endregion
}
