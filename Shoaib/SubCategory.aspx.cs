﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class SubCategory : System.Web.UI.Page
{

    #region DataMembers & Varriables
    SetCustomers objCust = new SetCustomers();
    #endregion


    #region Page Load Event

    protected void Page_Load(object sender, EventArgs e)

    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (!IsPostBack)
        {
            FillSubCategory();
            FillMainCategory();
        }
    }

    #endregion

    #region Add/ Edit Sub Category  Setup

    protected void btn_save_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";

        if (hdnSubCategoryCode.Value != null && hdnSubCategoryCode.Value != string.Empty)
        {
            objCust.SubCategoryCode = int.Parse(hdnSubCategoryCode.Value);
        }
        else
        {
            objCust.SubCategoryCode = 0;
        }
        objCust.CustomerCode     = decimal.Parse(Sessions.CustomerCode);
        objCust.SubcategoryName  = Convert.ToString(txtSubCategoryName.Text);
        objCust.MainCategoryCode = Convert.ToInt32(ddlMainCategory.SelectedValue);

        if (Sessions.UserRole.ToString() != "Admin")
        {
            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }
            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" || Convert.ToString(dt.Rows[0]["IsAdd"]) == "True")
            {
                    if (Convert.ToString(dt.Rows[0]["IsAdd"]) == "True" && (hdnSubCategoryCode.Value == null || hdnSubCategoryCode.Value == string.Empty))
                    {
                        int result = objCust.SetSubCategory();
                        if (result != -1)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Sub Category Setup successful added!');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                        }
                         clear();
                        FillSubCategory();
                    }
                    else
                    {
                        if (Convert.ToString(dt.Rows[0]["IsEdit"]) == "True" && hdnSubCategoryCode.Value != null && hdnSubCategoryCode.Value != string.Empty)
                        {
                            int result = objCust.SetSubCategory();
                            if (result != -1)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Sub Category Setup successful updated!');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                            }
                             clear();
                            FillSubCategory();

                        }

                        else
                        {
                            if (objCust.SubCategoryCode != 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='SubCategory.aspx';", true);
                            }

                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='SubCategory.aspx';", true);
                            }

                        }

                    }
             }
            else
            {
                if (objCust.SubCategoryCode != 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to Edit!');location.href='SubCategory.aspx';", true);
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Not allowed to add!');location.href='SubCategory.aspx';", true);
                }

            }
            
        }

        else
        {
            int result = objCust.SetSubCategory();

            if (objCust.SubCategoryCode != 0)
            {
                if (result != -1)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Sub Category Setup successful updated!');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                }
            }
            else
            {
                if (result != -1)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Sub Category Setup successful added!');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record already exists!');", true);
                }
            }
            clear();
            FillSubCategory();
        }
    }

    #endregion

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        clear();
    }
    #endregion

    #region Blank Controls
    private void clear()
    {

        txtSubCategoryName.Text = string.Empty;
        ddlMainCategory.SelectedValue = "0";
        hdnSubCategoryCode.Value = string.Empty;

    }
    #endregion


    #region Fill Sub Category  Setup
    protected void FillData(int SubCategoryCode)
    {
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.SubCategoryCode = SubCategoryCode;
        DataSet ds = new DataSet();
        ds = objCust.GetSubCategory();
        if (ds.Tables[0].Rows.Count > 0)
        {

            txtSubCategoryName.Text = Convert.ToString(ds.Tables[0].Rows[0]["SubcategoryName"].ToString());
            ddlMainCategory.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["MainCategoryCode"].ToString());

            hdnSubCategoryCode.Value = SubCategoryCode.ToString();
        }

    }
    #endregion

    #region Fill Main Category  Setup
    protected void FillSubCategory()
    {
       
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.SubCategoryCode = 0;
            DataSet ds = new DataSet();
            ds = objCust.GetSubCategory();
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 30)
                {
                    grdCustomer.AllowPaging = true;
                }
                grdCustomer.Visible = true;
                grdCustomer.DataSource = ds.Tables[0];
                grdCustomer.DataBind();
                lblError.Visible = false;
            }
            else
            {
                grdCustomer.Visible = false;
                lblError.Visible = true;
            }
       

    }
    #endregion

    #region GridView Events
    protected void grdCustomer_RowCommand(object sender, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            objCust.SubCategoryCode = int.Parse(e.CommandArgument.ToString());
            objCust.DeleteSubcategoryCode();
            FillSubCategory();
        }

        if (e.CommandName == "Edit")
        {
            FillData(int.Parse(e.CommandArgument.ToString()));

        }
    }
    protected void grdCustomer_RowDataBound(object sender, DataGridItemEventArgs e)
    {
         if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Sessions.UserRole.ToString() != "Admin")
                {
                    LinkButton lbEdit = (LinkButton)e.Item.FindControl("lbEdit");
                    LinkButton lbDelete = (LinkButton)e.Item.FindControl("lbDelete");

                    string PageUrl = Request.Url.ToString();
                    string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                    for (int i = 0; i < splitPageUrl.Length; i++)
                    {
                        if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                        {
                            PageUrl = Convert.ToString(splitPageUrl[i]);
                        }
                    }

                    User ObjUser = new User();
                    ObjUser.PageName = PageUrl;
                    ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                    DataTable dt = ObjUser.getPageUserRights();
                    if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
                    {
                        Response.Redirect(Request.UrlReferrer.ToString());
                    }
                    if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
                    {

                        lbDelete.Visible = false;

                    }
                    if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
                    {

                        lbEdit.Visible = false;

                    }

                }
            }

    }

    protected void grdCustomer_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        grdCustomer.CurrentPageIndex = e.NewPageIndex;
        FillSubCategory();
    }
    #endregion


    #region Fill Main Category
    protected void FillMainCategory()
    {
       

            objCust.MainCategoryCode = 0;
            DataSet ds = new DataSet();
            ds = objCust.GetMainCategory();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlMainCategory.DataSource = ds.Tables[0];
                ddlMainCategory.DataTextField = "MainCategoryName";
                ddlMainCategory.DataValueField = "MainCategoryCode";
                ddlMainCategory.DataBind();
                ddlMainCategory.Items.Insert(0,new ListItem("Select Main Category","0"));
            }
       

    }
    #endregion
}
