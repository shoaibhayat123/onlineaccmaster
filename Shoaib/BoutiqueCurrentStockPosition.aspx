﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BoutiqueInnermaster.master" AutoEventWireup="true" CodeFile="BoutiqueCurrentStockPosition.aspx.cs" Inherits="BoutiqueCurrentStockPosition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Current Stock Position"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
        >
         <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
               Brand :
            </td>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                <asp:DropDownList ID="ddlBrand" Enabled="false" runat="server" Style="width: 415px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width:85px;">
                As On :
            </td>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                <asp:TextBox ID="txtstartDate" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtstartDate'),this);"
                    runat="server" Width="100px"></asp:TextBox>
                <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                    onclick="scwShow(scwID('ctl00_MainHome_txtstartDate'),this);" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                    ErrorMessage="Please Fill As On." Text="*" ControlToValidate="txtstartDate" ValidationGroup="Ledger"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                    ControlToValidate="txtstartDate" ValidationGroup="Ledger" ErrorMessage="Invalid As On !!"
                    Display="None"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                Report Type :
            </td>
            <td>
                <asp:RadioButton ID="rdWith" GroupName="report" Checked="true" runat="server" Text="With 0 balance" />
                <asp:RadioButton ID="rdbWithOut" GroupName="report" runat="server" Text="without  0 balance" />
                  <asp:Button ID="btnGenerate" Text="Generate" ValidationGroup="Ledger" 
                    runat="server" OnClick="btnGenerate_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="Ledger" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
              
            </td>
        </tr>
    </table>
</asp:Content>

