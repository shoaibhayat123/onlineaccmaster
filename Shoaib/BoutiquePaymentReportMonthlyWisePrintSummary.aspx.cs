﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;


public partial class BoutiquePaymentReportMonthlyWisePrintSummary : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();

    AccFiles objFile = new AccFiles();
    Reports objReports = new Reports();

    decimal TotalQuantity = 0;
    decimal TotalPaybleamount = 0;
    decimal GrandTotalPaybleamount = 0;
    decimal TotalSaleTaxAmount = 0;
    decimal TotalAmountIncludeTaxes = 0;
    decimal TotalInvoiceAmount = 0;
    decimal TotalAlterCharges = 0;
    decimal GrandTotalQuantity = 0;
    decimal GrandTotalGrossAmount = 0;
    decimal GrandTotalSaleTaxAmount = 0;
    decimal GrandTotalAmountIncludeTaxes = 0;
    decimal GrandTotalInvoiceAmount = 0;
    decimal TotalNetPayble = 0;

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if ((Sessions.CustomerCode == "") || (Request["From"].ToString() == "") || (Request["To"].ToString() == "") || (Request["CostCenter"].ToString() == "") || (Request["Item"].ToString() == ""))
        {
            Response.Redirect("default.aspx");
        }
        if (!IsPostBack)
        {

            bindSupplier();
            lblFromDate.Text = ClsGetDate.FillFromDate(Request["From"].ToString());
            lblToDate.Text = ClsGetDate.FillFromDate(Request["To"].ToString());
            lblConversionRate.Text = Convert.ToString(Request["ConvRate"].ToString());
            bindBuyer();
            FillLogo();
            lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
            lblUser.Text = Sessions.UseLoginName.ToString();
            bindrptrInvoiceReport();
            GrandlblTotalQuantity.Text = String.Format("{0:C}", GrandTotalQuantity).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            GrandlblTotalGrossAmount.Text = String.Format("{0:C}", GrandTotalPaybleamount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        }
    }

    protected void bindBuyer()
    {
    }
    #region Grid RowDataBound
    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {





    }


    #endregion

    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }


    }
    #endregion
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }






    protected void bindrptrInvoiceReport()
    {
        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.DataFrom = "SALESINV";
        objReports.ItemCode = Convert.ToInt32(Request["Item"]);
        objReports.storeId = Convert.ToInt32(Request["CostCenter"]);
        objReports.InvDateFrom = Convert.ToDateTime(Request["From"]);
        objReports.InvDateTo = Convert.ToDateTime(Request["To"]);
        objReports.MainCategoryCode = Convert.ToInt32(Request["MainCategoryCode"]);
        DataSet ds = new DataSet();
        ds = objReports.getBrandForBoutiquePaymentSheetByDate();
        if (ds.Tables[0].Rows.Count > 0)
        {
            dgGallery.DataSource = ds.Tables[0];
            dgGallery.DataBind();

        }
    }


    protected void dgGallery_OnItemDataBound(object sender, DataGridItemEventArgs e)
    {


        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objReports.DataFrom = "SALESINV";
            objReports.ItemCode = Convert.ToInt32(Request["Item"]);
            objReports.storeId = Convert.ToInt32(Request["CostCenter"]);
            objReports.MainCategoryCode = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "MainCategoryCode"));
            objReports.InvDateFrom = Convert.ToDateTime(Request["From"]);
            objReports.InvDateTo = Convert.ToDateTime(Request["To"]);
            DataSet ds = new DataSet();
            TotalQuantity = 0;
            TotalPaybleamount = 0;
            TotalAlterCharges = 0;
            TotalNetPayble = 0;
            decimal NetPayableAmount = 0;
            ds = objReports.getInvoiceBoutiquePaymentSheetDate();
            if (ds.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[i]["DataFrom"]) == "SALERETINV")
                    {
                        TotalQuantity -= Convert.ToDecimal(ds.Tables[0].Rows[i]["Quantity"]);

                    }
                    else
                    {
                        TotalQuantity += Convert.ToDecimal(ds.Tables[0].Rows[i]["Quantity"]);

                    }


                    decimal AlterCharges = 0;
                    decimal ConvRate = Convert.ToDecimal((Request["ConvRate"]));
                    if (Convert.ToString(ds.Tables[0].Rows[i]["AlterCharges"]) != "")
                    {
                        AlterCharges = Convert.ToDecimal(ds.Tables[0].Rows[i]["AlterCharges"]);
                    }

                    decimal DiscountAmount = 0;
                    decimal DesignerPrice = 0;
                    if (Convert.ToString(ds.Tables[0].Rows[i]["DesignerPrice"]) != "")
                    {
                        DesignerPrice = Convert.ToDecimal(ds.Tables[0].Rows[i]["DesignerPrice"]);
                    }
                    if (Convert.ToString(ds.Tables[0].Rows[i]["BoutiqueDiscount"]) != "")
                    {
                        DiscountAmount = DesignerPrice * (Convert.ToDecimal(ds.Tables[0].Rows[i]["BoutiqueDiscount"]) / 100);


                    }
                    decimal netamount = 0;
                    netamount = Convert.ToDecimal(((DesignerPrice - DiscountAmount) - (AlterCharges * ConvRate)).ToString("0.00")); ;

                    decimal NetPayable = 0;
                    if (Convert.ToString(ds.Tables[0].Rows[i]["BoutiquePurchaseDiscount"]) != "")
                    {
                        NetPayable = Convert.ToDecimal((100 - Convert.ToDecimal(ds.Tables[0].Rows[i]["BoutiquePurchaseDiscount"])).ToString("0.00"));
                    }


                    if (Convert.ToString(ds.Tables[0].Rows[i]["DataFrom"]) == "SALERETINV")
                    {
                        NetPayableAmount += 0-(netamount * (NetPayable / 100));
                    }
                    else
                    {
                        NetPayableAmount += (netamount * (NetPayable / 100));
                    }
                }

            }

            Label lblQuantity = (Label)e.Item.FindControl("lblQuantity");
            lblQuantity.Text = TotalQuantity.ToString("0");

            Label lblNetPayableAmount = (Label)e.Item.FindControl("lblNetPayableAmount");
            lblNetPayableAmount.Text = NetPayableAmount.ToString("0.00");
          


            

        }


    }


    protected void rptrInvoiceReport_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {


        Label lblBrandName = ((Label)e.Item.FindControl("lblBrandName"));
        lblBrandName.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "MainCategoryName"));
        DataGrid dgGallery = ((DataGrid)e.Item.FindControl("dgGallery"));
        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.DataFrom = "SALESINV";
        objReports.ItemCode = Convert.ToInt32(Request["Item"]);
        objReports.storeId = Convert.ToInt32(Request["CostCenter"]);
        objReports.MainCategoryCode = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "MainCategoryCode"));
        objReports.InvDateFrom = Convert.ToDateTime(Request["From"]);
        objReports.InvDateTo = Convert.ToDateTime(Request["To"]);

        DataSet ds = new DataSet();
        TotalQuantity = 0;
        TotalPaybleamount = 0;
        TotalAlterCharges = 0;
        TotalNetPayble = 0;
        ds = objReports.getInvoiceBoutiquePaymentSheetDate();
        if (ds.Tables[0].Rows.Count > 0)
        {
            dgGallery.DataSource = ds.Tables[0];
            dgGallery.DataBind();
        }

    }

}