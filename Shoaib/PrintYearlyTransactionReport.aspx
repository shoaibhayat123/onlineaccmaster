﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintYearlyTransactionReport.aspx.cs"
    Inherits="PrintYearlyTransactionReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .Level1
        {
            margin: 5px 0 0 30px;
            font-weight: bold;
            color: Green;
        }
        .Level2
        {
            margin: 5px 0 0 60px;
            font-weight: bold;
            color: Green;
        }
        .Level3
        {
            margin: 5px 0 0 90px;
            font-weight: bold;
            color: Green;
        }
        .Level4
        {
            margin: 5px 0 0 120px;
            font-weight: bold;
            color: Green;
        }
        .Level5
        {
            margin: 5px 0 0 150px;
            font-weight: bold;
            color: Green;
        }
        .Level6
        {
            margin: 5px 0 0 180px;
            font-weight: bold;
            color: Green;
        }
        .Ledger
        {
            margin: 5px 0 0 115px;
        }
    </style>
</head>
<body style="font-family: Tahoma; background: none; font-size: 12px;">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 1200px; float: left">
        <table style="width: 1200px; float: left; font-size: 14px; font-weight: bold;">
           <tr id="trTrialBal" runat="server">
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Yearly
                        Transaction Sheet<br />
                    </span><span style="font-size: 14px;"><span style="font-size: 14px;">Year
                        <asp:Label ID="lblYear" runat="server" Style="padding-right: 15px;"></asp:Label>
                        <br />
                    </span>
                        <asp:Label ID="lblAccount" runat="server"></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <hr />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div id="dvResult" runat="server" style="text-align: left; width: 1200px; float: left">
        <asp:GridView ID="grdInvoice" AutoGenerateColumns="false" GridLines="None" BorderWidth="0"
            CellPadding="0" CellSpacing="0" runat="server" OnRowDataBound="grdInvoice_RowDataBound">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <div class="Level<%#Eval("depth")%>">
                            <asp:Label ID="lblTitle" Text='<%#Eval("Title")%>' Style="text-decoration: underline;
                                font-size: 16px" runat="server"></asp:Label>
                            <asp:HiddenField ID="hdnid" runat="server" />
                        </div>
                        <div class="Ledger">
                            <div style="text-align: left; width: 1080px; float: left">
                                <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" FooterStyle-CssClass="gridFooter"
                                    HeaderStyle-CssClass="gridheader" ShowFooter="true" AlternatingItemStyle-CssClass="gridAlternateItem"
                                    GridLines="none" BorderColor="black" BorderStyle="Dotted" BorderWidth="1" OnItemDataBound="dgGallery_OnItemDataBound"
                                    ItemStyle-CssClass="gridItem">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="S.No.">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <%# Container.DataSetIndex + 1%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Title of Account">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="left" Width="30%" />
                                            <ItemTemplate>
                                                <%# Eval("Title")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Jan">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="right" Width="5%" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <ItemTemplate>
                                                <%#  Convert.ToDecimal(Eval("Month1")) >= 0 ? String.Format("{0:C}", Eval("Month1")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') : String.Format("{0:C}", Eval("Month1")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ')%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalMonth1" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Feb">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="right" Width="5%" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <ItemTemplate>
                                                <%#  Convert.ToDecimal(Eval("Month2")) >= 0 ? String.Format("{0:C}", Eval("Month2")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') : String.Format("{0:C}", Eval("Month2")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ')%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalMonth2" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Mar">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="right" Width="5%" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <ItemTemplate>
                                                <%#  Convert.ToDecimal(Eval("Month3")) >= 0 ? String.Format("{0:C}", Eval("Month3")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') : String.Format("{0:C}", Eval("Month3")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ')%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalMonth3" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Apr">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="right" Width="5%" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <ItemTemplate>
                                                <%#  Convert.ToDecimal(Eval("Month4")) >= 0 ? String.Format("{0:C}", Eval("Month4")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') : String.Format("{0:C}", Eval("Month4")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ')%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalMonth4" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="May">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="right" Width="5%" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <ItemTemplate>
                                                <%#  Convert.ToDecimal(Eval("Month5")) >= 0 ? String.Format("{0:C}", Eval("Month5")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') : String.Format("{0:C}", Eval("Month5")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ')%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalMonth5" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Jun">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="right" Width="5%" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <ItemTemplate>
                                                <%#  Convert.ToDecimal(Eval("Month6")) >= 0 ? String.Format("{0:C}", Eval("Month6")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') : String.Format("{0:C}", Eval("Month6")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ')%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalMonth6" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Jul">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="right" Width="5%" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <ItemTemplate>
                                                <%#  Convert.ToDecimal(Eval("Month7")) >= 0 ? String.Format("{0:C}", Eval("Month7")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') : String.Format("{0:C}", Eval("Month7")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ')%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalMonth7" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Aug">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="right" Width="5%" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <ItemTemplate>
                                                <%#  Convert.ToDecimal(Eval("Month8")) >= 0 ? String.Format("{0:C}", Eval("Month8")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') : String.Format("{0:C}", Eval("Month8")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ')%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalMonth8" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Sep">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="right" Width="5%" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <ItemTemplate>
                                                <%#  Convert.ToDecimal(Eval("Month9")) >= 0 ? String.Format("{0:C}", Eval("Month9")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') : String.Format("{0:C}", Eval("Month9")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ')%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalMonth9" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Oct">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="right" Width="5%" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <ItemTemplate>
                                                <%#  Convert.ToDecimal(Eval("Month10")) >= 0 ? String.Format("{0:C}", Eval("Month10")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') : String.Format("{0:C}", Eval("Month10")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ')%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalMonth10" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Nov">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="right" Width="5%" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <ItemTemplate>
                                                <%#  Convert.ToDecimal(Eval("Month11")) >= 0 ? String.Format("{0:C}", Eval("Month11")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') : String.Format("{0:C}", Eval("Month11")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ')%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalMonth11" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Dec">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="right" Width="5%" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <ItemTemplate>
                                                <%#  Convert.ToDecimal(Eval("Month12")) >= 0 ? String.Format("{0:C}", Eval("Month12")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') : String.Format("{0:C}", Eval("Month12")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ')%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalMonth12" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Total">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="right" Width="5%" />
                                            <FooterStyle HorizontalAlign="Right" />
                                            <ItemTemplate>
                                                <%#  Convert.ToDecimal(Eval("total")) >= 0 ? String.Format("{0:C}", Eval("total")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') : String.Format("{0:C}", Eval("total")).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ')%>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotalMonth" runat="server"></asp:Label>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <div id="grandTotal" runat="server" style="text-align: center; width: 800px; display: none;
            margin-top: 30px; margin-left: 115px; float: left">
            <table width="20%" style="border-collapse: collapse; border-color: Black; border-style: dotted;
                border-width: 1px;">
                <tr class="gridheader">
                    <td colspan="2" style="text-align: center; font-weight: bold; text-decoration: undreline;
                        margin-bottom: 15px; font-size: 14px">
                        Grand Total Summary
                    </td>
                </tr>
                <tr class="gridItem">
                    <td style="width: 40%; text-align: center">
                        Jan
                    </td>
                    <td style="text-align: right">
                        <asp:Label ID="lblGrandTotalMonth1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr class="gridAlternateItem">
                    <td style="text-align: center">
                        Feb
                    </td>
                    <td style="text-align: right">
                        <asp:Label ID="lblGrandTotalMonth2" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr class="gridItem">
                    <td style="text-align: center">
                        Mar
                    </td>
                    <td style="text-align: right">
                        <asp:Label ID="lblGrandTotalMonth3" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr class="gridAlternateItem">
                    <td style="text-align: center">
                        Apr
                    </td>
                    <td style="text-align: right">
                        <asp:Label ID="lblGrandTotalMonth4" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr class="gridItem">
                    <td style="text-align: center">
                        May
                    </td>
                    <td style="text-align: right">
                        <asp:Label ID="lblGrandTotalMonth5" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr class="gridAlternateItem">
                    <td style="text-align: center">
                        Jun
                    </td>
                    <td style="text-align: right">
                        <asp:Label ID="lblGrandTotalMonth6" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr class="gridItem">
                    <td style="text-align: center">
                        Jul
                    </td>
                    <td style="text-align: right">
                        <asp:Label ID="lblGrandTotalMonth7" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr class="gridAlternateItem">
                    <td style="text-align: center">
                        Aug
                    </td>
                    <td style="text-align: right">
                        <asp:Label ID="lblGrandTotalMonth8" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr class="gridItem">
                    <td style="text-align: center">
                        Sep
                    </td>
                    <td style="text-align: right">
                        <asp:Label ID="lblGrandTotalMonth9" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr class="gridAlternateItem">
                    <td style="text-align: center">
                        Oct
                    </td>
                    <td style="text-align: right">
                        <asp:Label ID="lblGrandTotalMonth10" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr class="gridItem">
                    <td style="text-align: center">
                        Nov
                    </td>
                    <td style="text-align: right">
                        <asp:Label ID="lblGrandTotalMonth11" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr class="gridAlternateItem">
                    <td style="text-align: center">
                        Dec
                    </td>
                    <td style="text-align: right">
                        <asp:Label ID="lblGrandTotalMonth12" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr class="gridFooter">
                    <td style="border: 1px dotted #000000; text-align: center">
                        Total
                    </td>
                    <td style="text-align: right">
                        <asp:Label ID="lblGrandTotal" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="dvNoRecord" runat="server" style="padding-bottom: 10px; width: 1200px; text-align: center;
        font-family: Gisha; font-size: 20px; background-color: #E1F5FD; display: none;
        float: left; color: #110F0F;">
        <div style="padding-top: 10px; padding-bottom: 5px;">
            No record found
        </div>
    </div>
    <div style="clear: both">
        <asp:Chart ID="Chart1" runat="server" Height="400px" Width="800px">
            <Legends>
                <asp:Legend BackColor="GreenYellow">
                </asp:Legend>
            </Legends>
            <Titles>
                <asp:Title Name="Naveen" Text="Yearly Transaction Sheet" TextStyle="Default">
                </asp:Title>
            </Titles>
            <Series>
                <asp:Series Name="Series1" ChartType="Column" ChartArea="ChartArea1">
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea Name="ChartArea1" BackColor="#DDEEDC">
                    <AxisX Title="Months" IsLabelAutoFit="False" Interval="1">
                        <LabelStyle Format="MMM" />
                    </AxisX>
                    <AxisY Title="Amount">
                    </AxisY>
                </asp:ChartArea>
            </ChartAreas>
        </asp:Chart>
    </div>
    <div style="text-align: left; width: 1200px; float: left">
        <table style="width: 1200px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 1200px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>
                                <script type="text/javascript">

                                    var currentTime = new Date()
                                    var hours = currentTime.getHours()
                                    var Newhours = 0;
                                    var minutes = currentTime.getMinutes()
                                    if (minutes < 10) {
                                        minutes = "0" + minutes
                                    }
                                    if (hours > 12) {
                                        Newhours = hours - 12;
                                    }
                                    else {
                                        Newhours = hours;
                                    }
                                    document.write(Newhours + ":" + minutes + " ")
                                    if (hours > 11) {
                                        document.write("PM")
                                    } else {
                                        document.write("AM")
                                    }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
