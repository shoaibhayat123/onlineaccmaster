﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="POSSaleInvoice.aspx.cs" Inherits="POSSaleInvoice" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Boutique POS Sales Invoice"></asp:Label>
    <script type="text/javascript">


        function DetailClear() 
        {
            document.getElementById('<%=txtCustomerName.ClientID%>').value = "";
            document.getElementById('<%=txtContactNumber.ClientID%>').value = "";
            document.getElementById('<%=txtEmailAddress.ClientID%>').value = "";
        }

       
        function Count(text, long) 
        {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }
        function ChangeCartage(obj) 
        {
      
                if (obj.value != null && !obj.value.toString().match(/^[-]?\d*\.?\d*$/) || obj.value == "")
            
                 {
                    obj.value = "0.00";
                    var BillAmount = parseFloat(replaceAll(document.getElementById('<%=txtTotalGrossAmount.ClientID%>').innerHTML, ',', '')) + parseFloat(replaceAll(document.getElementById('<%=txtCartagecharges.ClientID%>').value, ',', '')) - parseFloat(replaceAll(document.getElementById('<%=txtDiscount.ClientID%>').value, ',', ''));
                    document.getElementById('<%=lblBillAmount.ClientID%>').value = formatCurrency(BillAmount);
                    alert("Please Enter Numeric Value");
                }
            else 
            {
                 if (obj.value < 0)
                 {
                    obj.value = "0.00";
                    alert("Please Enter Valid Value");
                 }
                 else 
                 {

                 var BillAmount =   parseFloat(replaceAll(document.getElementById('<%=txtTotalGrossAmount.ClientID%>').innerHTML, ',', '')) 
                                 + parseFloat(replaceAll(document.getElementById('<%=txtCartagecharges.ClientID%>').value, ',', ''))
                                 - parseFloat(replaceAll(document.getElementById('<%=txtDiscount.ClientID%>').value, ',', ''));
                                
                document.getElementById('<%=lblBillAmount.ClientID%>').value = formatCurrency(BillAmount);

                var CardCharge = replaceAll(document.getElementById('<%=txtCardCharge.ClientID %>').value, ',', '');
                var CashReturned = (parseFloat(BillAmount) * (CardCharge / 100));

                document.getElementById('<%=lblCardamount.ClientID%>').innerHTML = formatCurrency(CashReturned);
                document.getElementById('<%=hdnCardamount.ClientID %>').value = CashReturned;
                 }

              
                
            }
        }

        function ChangeCardRate(obj) {

            if (obj.value != null && !obj.value.toString().match(/^[-]?\d*\.?\d*$/) || obj.value == "") {
                obj.value = "0.00";
                document.getElementById('<%=lblCardamount.ClientID%>').innerHTML = "0.00";
                alert("Please Enter Numeric Value");
            }
            else {

                var BillAmount = replaceAll(document.getElementById('<%=lblBillAmount.ClientID %>').value, ',', '');
                var CashReturned = (parseFloat(BillAmount) * (obj.value / 100));
                document.getElementById('<%=lblCardamount.ClientID%>').innerHTML = formatCurrency(CashReturned);
                document.getElementById('<%=hdnCardamount.ClientID %>').value = CashReturned;

            }
        }

        function replaceAll(txt, replace, with_this) {
            return txt.replace(new RegExp(replace, 'g'), with_this);
        }
        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                        num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }

        function ChangeCaseReceived() {

            var BillAmount = replaceAll(document.getElementById('<%=lblBillAmount.ClientID %>').value, ',', '');

            var CashReceived = replaceAll(document.getElementById('<%=txtCashReceived.ClientID %>').value, ',', '');

            var CashReturned = (parseFloat(CashReceived) - parseFloat(BillAmount));

            document.getElementById('<%=txtCashReturned.ClientID %>').value = parseFloat(Math.round(CashReturned * 100) / 100).toFixed(2);
        }


        function func(obj) {

            var t2 = document.getElementById(obj);
            t2.focus();

        }


        function isNumericQuantity(obj) 
        {

            if (obj.value != null && !obj.value.toString().match(/^[-]?\d*\.?\d*$/) || obj.value == "") 
            {
                obj.value = "1";
              

            }
            else {

              
                }
            }
        
    </script>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);

        function BeginRequestHandler(sender, args) {

            if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Save") {

                document.getElementById('<%= ImageButton2.ClientID %>').value = "Saving...";
                args.get_postBackElement().disabled = true;
            }
            if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Update") {

                document.getElementById('<%= btnAddNew.ClientID %>').value = "Updating...";
                args.get_postBackElement().disabled = true;
            }


        }
        function SaveClick() {
            document.getElementById('<%= hdnButtonText.ClientID %>').value = "Save";

        }

        function UpdateClick() {
            document.getElementById('<%= hdnButtonText.ClientID %>').value = "Update";

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
         <asp:HiddenField ID="hdnButtonText" runat="server" />
            <asp:HiddenField ID="hdnInvoiceId" runat="server" />
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                width="100%">
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 110px;">
                        Inv No :
                    </td>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 5px; text-align: left;">
                        <table>
                            <tr>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 0px;">
                                    <asp:TextBox ID="txtInvNo" Enabled="false" AutoPostBack="true" OnTextChanged="txtInvNo_TextChanged" TabIndex="1" CssClass="input_box33" Style="float: left;
                                        text-transform: uppercase;" runat="server"></asp:TextBox>
                                    <asp:HiddenField ID="hdnInvoiceSummaryId" runat="server" OnValueChanged="hdnInvoiceSummaryId_ValueChanged" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill Inv No." Text="*" ControlToValidate="txtInvNo" ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <span id="spanPgging" runat="server" style="padding: 0 20px 0 20px; font-weight: bold;
                                        font-size: 18px">
                                        <asp:LinkButton ID="lnkFirst" Text="|<" runat="server" OnClick="lnkFirst_Click"> </asp:LinkButton>
                                        <asp:LinkButton ID="lnkPrevious" Style="margin-left: 10px;" Text="<" runat="server"
                                            OnClick="lnkPrevious_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkNext" Text=">" Style="margin-left: 10px;" runat="server" OnClick="lnkNext_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnLast" Text=">|" Style="margin-left: 10px;" runat="server" OnClick="lnLast_Click"></asp:LinkButton></span>
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    Date :
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                    <asp:TextBox ID="txtInvoiceDate" TabIndex="2" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDate'),this);"
                                        runat="server" Width="100px" OnTextChanged="txtInvoiceDate_TextChanged"></asp:TextBox>
                                    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                        onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDate'),this);" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill Invoice Date." Text="*" ControlToValidate="txtInvoiceDate"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                        ControlToValidate="txtInvoiceDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid Invoice Date !!"
                                        Display="None"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    Cr Days :
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    <asp:TextBox ID="txtCrDays" onChange="isNumericPositive(this)" TabIndex="3" MaxLength="25"
                                        runat="server" Width="40px"></asp:TextBox>
                                    <asp:Label ID="Lblcrdate" Style="display: none" runat="server" Width="80px" MaxLength="25"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                 <tr>
                    <td align="left" valign="top" style="width: 111px; padding: 2px 2px 2px 8px;">
                       Conversion Rate :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;  text-align: left">
                       <asp:TextBox ID="txtconversionRate" runat="server" MaxLength="10" Style="width: 550px" TabIndex="4" onChange="isNumericDecimal(this)"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please Enter Conversion Rate" Text="*" ControlToValidate="txtconversionRate"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                
                <tr>
                    <td align="left" valign="top" style="width: 111px; padding: 2px 2px 2px 8px;">
                        Cost Center :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                        <asp:DropDownList ID="ddlCostCenter" TabIndex="4" runat="server" Style="width: 550px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select Cost Center" Text="*" ControlToValidate="ddlCostCenter"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;" class="auto-style1">
                        Debit A/C :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;" class="auto-style1">
                        <asp:DropDownList ID="ddlClientAC" TabIndex="5" runat="server" Style="width: 550px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select Client A/C " Text="*" ControlToValidate="ddlClientAC"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Sales A/C :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        <asp:DropDownList ID="ddlSaleAC" TabIndex="6" runat="server" Style="width: 227px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please select Sales A/C " Text="*" ControlToValidate="ddlSaleAC"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                            
                        &nbsp;&nbsp;
                        &nbsp;&nbsp;
                        Sales Person : <asp:TextBox ID="txtSalesPerson" TabIndex="6" MaxLength="15" style="text-transform:uppercase"  runat="server"></asp:TextBox>
                        &nbsp;&nbsp;
                        <asp:Button ID="btnpopupBasicinfo" runat="server" Text="Details" /><asp:Label ID="lblPurchaseRate" runat="server"></asp:Label>
                        <style>
                            .popupBackColor
                            {
                                background-color: Gray;
                                opacity: .50;
                                -moz-opacity: .50;
                                -webkit-opacity: .50;
                            }
                        </style>
                        <%--TargetControlID="btnpopupBasicinfo"--%>
                        <cc1:ModalPopupExtender ID="mpeAddDetails" runat="server" BackgroundCssClass="popupBackColor"
                            CancelControlID="btnAddClose" PopupControlID="divAddDetails" PopupDragHandleControlID="divAddDetails"
                            TargetControlID="btnpopupBasicinfo" />
                        <div id="divAddDetails" runat="server" style="display: none;">
                            <table width="460" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="border: solid 1px #333333;">
                                <tr>
                                    <td colspan="3" style="border-bottom: solid 1px #333333; background-color: #85A68C;
                                        color: Black; padding: 8px 0 8px 10px; font-size: 15px; color: #ffffff;">
                                        <strong>Customer Details</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px; text-align: left; height: 33px;">
                                        Contact Number
                                    </td>
                                    <td class="base" style="height: 33px; text-align: left; padding-left: 10px;">                                           
                                        :00971-&nbsp;                                 
                                        <asp:TextBox ID="txtContactNumber" TabIndex="501" OnTextChanged="txtContactNumber_OnChanged"
                                            AutoPostBack="true" MaxLength="50" runat="server" Style="width: 100px;"></asp:TextBox>
                                    </td>
                                    <td class="base">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px; text-align: left; height: 33px; width: 140px;">
                                        Customer Name
                                    </td>
                                    <td class="base" style="height: 33px; text-align: left; width: 180px;">
                                        :&nbsp;<asp:TextBox ID="txtCustomerName" TabIndex="502" onChange="capitalizeMe(this)"
                                            MaxLength="100" runat="server" Style="width: 150px;"></asp:TextBox>
                                    </td>
                                    <td class="base">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 10px; text-align: left; height: 33px;">
                                        ARY Card No.
                                    </td>
                                    <td class="base" style="height: 33px; text-align: left;">
                                        :&nbsp;<asp:TextBox ID="txtEmailAddress" TabIndex="503" MaxLength="100" runat="server"
                                            Style="width: 150px;"></asp:TextBox>
                                    </td>
                                    <td class="base">
                                        <%-- <asp:RegularExpressionValidator ID="rgvdetailsemail" Font-Size="Small" ControlToValidate="txtEmailAddress"
                                            Display="Dynamic" runat="server" ErrorMessage="Invalid e-mail address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center" height="10">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center" style="padding: 0px 0 0 20px;">
<asp:Button ID="btnAdd" TabIndex="504" runat="server" Text="Ok" OnClientClick="$find('ctl00_MainHome_divAddDetails').hide();return false;" ValidationGroup="AddDetail" />
                                        <asp:Button ID="btnAddClose" runat="server" Text="Cancel" OnClientClick="DetailClear()" />
                                        <%--     OnClientClick="$find('ctl00_MainHome_divAddDetails').hide();return false;" --%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center" height="20">
                                       
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="sundryDR" runat="server" style="height: 237px; width: 780px; margin-top: 8px;
                            margin-bottom: 4px; padding: 15px; overflow: auto; border: solid 2px  #528627">
                            <table>
                                <tr>
                                    <td style="margin-right: 10px;">
                                        Barcode No
                                    </td>
                                    <td>
                                        :
                                    </td>


                                    <td colspan="5" style="margin-right: 10px;">
                                        <asp:TextBox ID="txtBarCodeNo" TabIndex="7" AutoPostBack="true" OnTextChanged="txtBarCodeNo_TextChanged"
                                            Style="text-transform: uppercase" runat="server"></asp:TextBox>&nbsp;Item Description &nbsp;:&nbsp;
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Display="None"
                                            InitialValue="0" ErrorMessage="Please select Item Description " Text="*" ControlToValidate="ddlItemDesc"
                                            ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="ddlItemDesc" TabIndex="7" runat="server"
                                           OnSelectedIndexChanged="ddlItemDesc_SelectedIndexChanged" AutoPostBack="True" Height="20px" Width="308px" style="margin-left: 10px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;" colspan="6">
                                        <table>
                                            <tr>
                                                <td align="left" style="width: 67px;">
                                                    Quantity
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td style="margin-right: 10px;">
                                                    <asp:TextBox ID="txtQuantity" TabIndex="8" runat="server" onblur="isNumericQuantity(this)"
                                                        onChange="isNumericQuantity(this)" Style="width: 50px;" MaxLength="10"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="None"
                                                        ErrorMessage="Please fill Quantity " Text="*" ControlToValidate="txtQuantity"
                                                        ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                                </td>
                                                <td align="left" style="width: 40px; padding-left: 20px;">
                                                    Rate :
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRate" TabIndex="9" AutoPostBack="true" OnTextChanged="txtRate_changed" Enabled="false"
                                                        runat="server" onChange="isNumericDecimal(this)" Style="width: 75px;" MaxLength="10"></asp:TextBox>
                                                </td>
                                                <td align="left" style="width: 70px; padding-left: 20px;">
                                                    Discount % :
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtBoutiqueDiscount" Text="0.00" OnTextChanged="txtBoutiqueDiscount_changed"
                                                        AutoPostBack="true" TabIndex="9" runat="server" onChange="isNumericDecimal(this)"
                                                        Style="width: 50px;" MaxLength="10"></asp:TextBox>
                                                </td>
                                                <td align="left" style="padding-left: 20px;">
                                                    After Discount :
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="lblRateAfterDiscount" TabIndex="9" Text="0.00" Style="width: 80px;"
                                                        runat="server"></asp:TextBox>
                                                    <%--  <asp:Label ID="lblRateAfterDiscount" Text="0.00" runat="server"></asp:Label>--%>
                                                </td>
                                                <td>
                                                 
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;">
                                        Charges
                                    </td>
                                    <td>
                                        :
                                    </td>
                                    <td>
                                        <%-- OnTextChanged="txtAlterCharges_TextChanged" AutoPostBack="true"--%>
                                        <asp:TextBox ID="txtAlterCharges" TabIndex="10" runat="server" onChange="isNumericDecimal(this)"
                                            Style="width: 70px;" MaxLength="10"></asp:TextBox>
                                    </td>
                                    
                                    <td style="margin-right: 10px;">
                                        <table>
                                            <tr>
                                                <td style="width: 40px;">
                                                    Dr.
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlDR" Width="150px" TabIndex="11" runat="server" OnSelectedIndexChanged="ddlDR_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Enabled="false"
                                                        Display="Dynamic" InitialValue="0" ErrorMessage="Please select Dr." Text="*"
                                                        ControlToValidate="ddlDR" ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                    Cr.
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlCR" Width="150px" TabIndex="12" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" Enabled="false"
                                                        Display="Dynamic" InitialValue="0" ErrorMessage="Please select Cr." Text="*"
                                                        ControlToValidate="ddlCR" ValidationGroup="AddInvoiceDetail"></asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                    VAT %
                                                </td>
                                                <td>
                                                    :
                                                </td>
                                                <td>
                                                    <%-- OnTextChanged="txtAlterCharges_TextChanged" AutoPostBack="true"--%>
                                                    <asp:TextBox ID="txtSt" TabIndex="10" runat="server" Text="5.00"  onChange="isNumericDecimal(this)" 
                                                         Style="width: 70px;" MaxLength="10"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td colspan="3">
                                        <asp:Button TabIndex="13" Style="text-align: right; height: 26px;" ID="btnAddNew"  OnClick="btnAddNew_Click" OnClientClick="UpdateClick()" 
                                            runat="server" Text="Update" ValidationGroup="AddInvoiceDetail" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                            ShowSummary="false" ValidationGroup="AddInvoiceDetail" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7" style="">
                                        <asp:HiddenField ID="hdndetail" runat="server" />
                                        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                                            AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                            AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                            BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery_RowDataBound"
                                            OnItemCommand="dgGallery_ItemCommand">
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="S.No.">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataSetIndex + 1%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%-- <asp:TemplateColumn HeaderText="Item Code">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="8%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Eval("ItemCode")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>--%>
                                                <asp:TemplateColumn HeaderText="Item Description">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="56%" />
                                                    <ItemTemplate>
                                                        <%# Eval("ItemDesc") %>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Quantity">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="7%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("Quantity")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Rate">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="16%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("Rate")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Discount %">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="7%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("BoutiqueDiscount")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="After Discount">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="7%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("BoutiqueAmountAfterDis")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>                                                
                                               <%-- <asp:TemplateColumn HeaderText="Amount">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="16%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("GrossAmount")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>--%>
                                                <asp:TemplateColumn HeaderText="VAT %">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="7%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("SaleTax")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="VAT Amount">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="7%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("SaleTaxAmount")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Amount">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="16%" HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("AmountIncludeTaxes")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Charges" HeaderStyle-Wrap="true">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("AlterCharges")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Dr. A/C." HeaderStyle-Wrap="true">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <%#Eval("DRTitle")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Cr. A/C." HeaderStyle-Wrap="true">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <ItemTemplate>
                                                        <%#Eval("CRTitle")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--       <asp:TemplateColumn HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgDel1" runat="server" ImageUrl="Images/bt-delete.gif" AlternateText="Delete"
                                                            CommandArgument='<%#Eval("Id")%>' CommandName="Delete" OnClientClick="return askDeleteRow();" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>--%>
                                                <asp:TemplateColumn HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="imgDel1" runat="server" Text="Delete" CommandArgument='<%#Eval("Id")%>'
                                                            CommandName="Delete" OnClientClick="return askDeleteRow();" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Edit">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="ImageButton1" runat="server" CommandArgument='<%#Eval("Id")%>'
                                                            Text="Edit" CommandName="Edit" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: left; width: 800px;">
                        <table style="width: 800px;">
                            <tr>
                                <td>
                                    Totals :: &nbsp;&nbsp;&nbsp;&nbsp; <b>Qty:</b><asp:Label ID="txtTotalQuantity" Text="0.00"
                                        runat="server" Style="margin-right: 20px; padding-left: 5px"></asp:Label>
                                    <b>Gross Amount:</b>
                                    <asp:Label ID="txtTotalGrossAmount" runat="server" Text="0.00" Style="margin-right: 20px;
                                        padding-left: 5px"></asp:Label>
                                    <%--<b>VAT Tax:</b>--%>
                                   <%-- <asp:Label ID="txtSaleTax" runat="server" Text="0.00" Style="margin-right: 20px;
                                        padding-left: 5px"></asp:Label>--%>
                                    <%--<b>Amount:</b>--%>
                                    <asp:Label ID="txtTotalAmountIncludingTax" runat="server" Text="0.00" Style="margin-right: 20px; display:none;
                                        padding-left: 5px"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td style="float: left">
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                <asp:RadioButton ID="rdbCash" TabIndex="14" OnCheckedChanged="OnCheckChangedCash"
                                                    AutoPostBack="true" Checked="true" GroupName="PaymentType" Text="Cash" runat="server" />
                                                <asp:RadioButton ID="rdbcashCredit" TabIndex="15" OnCheckedChanged="OnCheckChangedCreditCard"
                                                    AutoPostBack="true" GroupName="PaymentType" Text="Credit Card" runat="server" />
                                                <asp:RadioButton ID="rdbCredit" TabIndex="16" OnCheckedChanged="OnCheckChangedCredit"
                                                    AutoPostBack="true" GroupName="PaymentType" Text="CR / CR Note / Order" runat="server" />
                                                <asp:RadioButton ID="rdbARYSahulatCard" TabIndex="16" OnCheckedChanged="rdbARYSahulatCard_CheckedChanged"
                                                    AutoPostBack="true" GroupName="PaymentType" Text="ARY Sahulat Card" runat="server"  />
                                            </td>
                                        </tr>
                                        <tr id="trCreditCard" runat="server">
                                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                                <asp:Label ID="lblDes1" runat="server" Text="Credit Card #"></asp:Label>
                                                &nbsp;</td>
                                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                                <asp:TextBox ID="txtCreditCarNo" Enabled="false" MaxLength="16" TabIndex="17" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trApproval" runat="server">
                                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                                <asp:Label ID="lblDes2" runat="server" Text="Approval #"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                                <asp:TextBox ID="txtApprovelNO" Enabled="false" MaxLength="10" TabIndex="18" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="trCashReceipt" runat="server">
                                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                                <asp:Label ID="lblCashReceipt" Text="Cash Receipt A/C" runat="server"></asp:Label>
                                            </td>
                                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                                <asp:DropDownList ID="ddlCashReceipt" OnSelectedIndexChanged="ddlCashReceipt_SelectedIndexChanged"
                                                    AutoPostBack="true" TabIndex="19" runat="server" Style="width: 360px">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                                    InitialValue="0" ErrorMessage="Please select  Cash Receipt A/C " Text="*" ControlToValidate="ddlCashReceipt"
                                                    ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>                                        
                                        <tr id="trCard1" runat="server">
                                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                                Card Charges %
                                            </td>
                                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                                <asp:TextBox ID="txtCardCharge" MaxLength="16" onChange="ChangeCardRate(this)" TabIndex="19"
                                                    runat="server"></asp:TextBox>
                                                &nbsp; &nbsp; &nbsp;
                                                <asp:Label ID="lblCardamount" runat="server"></asp:Label>
                                                <asp:HiddenField ID="hdnCardamount" runat="server"></asp:HiddenField>
                                            </td>
                                        </tr>
                                        <tr id="trCard2" runat="server">
                                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                                Card Charges A/c.
                                            </td>
                                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                                <asp:DropDownList ID="ddlCardChargesaccount" TabIndex="19" runat="server" Style="width: 360px">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic"
                                                    InitialValue="0" ErrorMessage="Please Select  Card Charges A/c " Text="*" ControlToValidate="ddlCardChargesaccount"
                                                    ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:TextBox ID="txtDescription" TabIndex="20" Width="482px" Style="height: 50px;"
                                                    onKeyUp="Count(this,250)" onChange="Count(this,250);capitalizeMe(this)" TextMode="MultiLine"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="left" style="">
                                    <table cellpadding="0" cellspacing="0" style="width: 300px; border: solid 0px red;
                                        padding: 5px 0px 0px 0px">
                                        <tr>
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 115px;">
                                                Discount:
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtDiscount" Text="0.00" TabIndex="21" AutoPostBack="true" onChange="isNumericDecimal(this)"  Style="text-align: right"
                                                    OnTextChanged="txtDiscount_TextChanged" MaxLength="10" Enabled="false" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 115px;">
                                                Add Charges:
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtCartagecharges" Text="0.00" TabIndex="22" AutoPostBack="true" onChange="isNumericDecimal(this)"
                                                    OnTextChanged="txtCartagecharges_TextChanged" MaxLength="10" Style="text-align: right" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                VAT Amount:
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtSaleTax" TabIndex="23" Enabled="false" Style="text-align: right"
                                                    Text="0.00" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Invoice Amount:
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="lblBillAmount" TabIndex="23" Enabled="false" Style="text-align: right"
                                                    Text="0.00" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Cash Received :
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtCashReceived" onChange="ChangeCaseReceived()" TabIndex="24" Style="text-align: right"
                                                    Text="0.00" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                Cash Returned :
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="txtCashReturned" Enabled="false" TabIndex="25" Style="text-align: right"
                                                    Text="0.00" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td style="width: 450px;">
                                    <span>
                                        <%--   <asp:Button ID="lblbutton" Style="cursor: pointer" runat="server" Text="Add New Invoice"
                                            OnClientClick="location.href='SalesInvoice.aspx?addnew=1'" />--%>
                                        <asp:Button ID="lblbutton" Style="cursor: pointer" runat="server" Text="Add New Invoice"
                                            OnClick="lblbutton_Click" />
                                        <asp:Button Text="Delete Invoice" OnClientClick="return askDeleteInvoice();" ID="btnDeleteInvoice"
                                            runat="server" OnClick="btnDeleteInvoice_Click" />
                                        <%-- <a id="PrintBill"
                                                runat="server" target="_blank">
                                                <asp:Button Text="Print Invoice" ID="btnPrintBill" runat="server" /></a>--%>
                                        <asp:Button Text="Print Invoice" ID="btnPrintBill" runat="server" OnClick="btnPrintBill_Click" />
                                    </span>&nbsp;&nbsp;&nbsp;&nbsp; <span id="spanLastInvoice" runat="server" style="display: none">
                                        <b>Last Inv No:
                                            <asp:Literal ID="litLastInvoiceNo" runat="server"></asp:Literal>&nbsp;&nbsp;<asp:Literal
                                                ID="litLastInvDate" runat="server"></asp:Literal></b></span>
                                </td>
                                <td style="width: 550px">
                                    <table cellpadding="0" cellspacing="0" style="padding: 5px 155px 0px 0px; width: 100%">
                                        <tr>
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 33%;">
                                            </td>
                                            <td align="left" valign="top" style="padding: 2px 0px 2px 5px; text-align: right;
                                                width: 33%;">
                                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                                                    ShowSummary="false" ValidationGroup="AddInvoice" />
                                                <%--<asp:ImageButton ID="ImageButton2" TabIndex="26" runat="server" ImageUrl="~/Images/btn-save.gif"
                                                    OnClick="btn_saveInvoice_Click" ValidationGroup="AddInvoice" />--%>
                                                    
                                                          <asp:Button ID="ImageButton2" OnClientClick="SaveClick()" TabIndex="26" Text="Save" runat="server"
                                                    ValidationGroup="AddInvoice" OnClick="btn_saveInvoice_Click" />
                                                    
                                                    &nbsp;</div>
                                            </td>
                                            <td style="width: 33%; padding: 2px 2px 20px 3px;">
                                              <%--  <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/bt-cancal.gif"
                                                    OnClick="btn_cancel_Click" />--%>    <asp:Button ID="ImageButton3" Text="Cancel" runat="server" OnClick="btn_cancel_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
