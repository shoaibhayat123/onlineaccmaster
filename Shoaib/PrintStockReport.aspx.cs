﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintStockReport : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();
    AccFiles objFile = new AccFiles();
    Reports objReports = new Reports();
    decimal TotalBalance = 0;
    decimal Totalqtyin = 0;
    decimal TotalqtyOut = 0;
    //public string attachment = "attachment; filename=Dresses27_Report_" + DateTime.Now.ToString("dd_MM_yyyy") + ".xls";
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if ((Sessions.CustomerCode == "") || (Request["From"].ToString() == "") || (Request["To"].ToString() == "") || (Request["itemcode"].ToString() == ""))
        {
            Response.Redirect("default.aspx");
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {


            bindSupplier();
            bindStock();
            lblFromDate.Text = ClsGetDate.FillFromDate(Request["From"].ToString());
            lblToDate.Text = ClsGetDate.FillFromDate(Request["To"].ToString());
            bindBuyer();
            FillLogo();

            lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
            lblUser.Text = Sessions.UseLoginName.ToString();
          


        }
    }

    protected void bindBuyer()
    {

         objCust.ItemId = Convert.ToInt32(Request["itemcode"].ToString());
         objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
         DataSet ds = new DataSet();
         ds = objCust.getItems();
         DataTable dtBuyer = new DataTable();
         dtBuyer = ds.Tables[0];
        if (dtBuyer.Rows.Count > 0)
        {
            lblBuyerName.Text = Convert.ToString(dtBuyer.Rows[0]["ItemDesc"]);
            //lblBuyerAddress.Text = Convert.ToString(dtBuyer.Rows[0]["Address1"]);
            //if (Convert.ToString(dtBuyer.Rows[0]["Address2"]) != "")
            //{
            //    lblBuyerAddress2.Text = "," + Convert.ToString(dtBuyer.Rows[0]["Address2"]);
            //}
            //if (Convert.ToString(dtBuyer.Rows[0]["Address3"]) != "")
            //{
            //    lblBuyerAddress3.Text = "," + Convert.ToString(dtBuyer.Rows[0]["Address3"]);
            //}
        }


    }


    #region Bind ledger grid
    protected void bindStock()
    {
        objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objReports.DateFrom = Convert.ToDateTime(Request["From"]);
        objReports.DateTo = Convert.ToDateTime(Request["To"]);
        objReports.itemcode = Convert.ToInt32(Request["itemcode"]);
        DataSet ds = new DataSet();
        ds = objReports.getStockReport();

        /********** Add new row to result for Opening Balance**********/
        DataRow dr = ds.Tables[2].NewRow(); //table 2 for detail trans
        dr["Description"] = "Opening Balance";
        decimal OpeningBal = 0;
        decimal TotalTrans = 0;
        if (Convert.ToString(ds.Tables[0].Rows[0]["Opbal"]) != "")
        {
            OpeningBal = Convert.ToDecimal(ds.Tables[0].Rows[0]["Opbal"]);
        }
        if (Convert.ToString(ds.Tables[1].Rows[0]["TotalTransQty"]) != "")
        {
            TotalTrans = Convert.ToDecimal(ds.Tables[1].Rows[0]["TotalTransQty"]);
        }

        decimal OpeningBalAmount = OpeningBal + TotalTrans;
        if (OpeningBalAmount > 0)
        {
            dr["qtyin"] = OpeningBalAmount;

        }
        else
        {
            dr["qtyout"] = OpeningBalAmount;
        }
       

        ds.Tables[2].Rows.InsertAt(dr, 0);


        /*************** getting All Record by mearging them ******************/

        DataTable dtAllRecord = new DataTable();
        dtAllRecord = ds.Tables[2].Copy();

        if (ds.Tables[3].Rows.Count > 0)
        {
            dtAllRecord.Merge(ds.Tables[3]);
        }
        if (ds.Tables[4].Rows.Count > 0)
        {
            dtAllRecord.Merge(ds.Tables[4]);
        }
        //if (ds.Tables[5].Rows.Count > 0)
        //{
        //    dtAllRecord.Merge(ds.Tables[5]);
        //}

        DataTable dt = new DataTable();
        DataView dv = new DataView(dtAllRecord);
        dv.Sort = "InvDate,InvNo";   
        dt = dv.ToTable();
        
        grdLedger.DataSource = dt;
        grdLedger.DataBind();


    }

    #endregion
    #region Grid RowDataBound
    protected void grdLedger_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lbltransDate = ((Label)e.Row.FindControl("lbltransDate"));
            Label lblChqDate = ((Label)e.Row.FindControl("lblChqDate"));
            Label lblBalance = ((Label)e.Row.FindControl("lblBalance"));
            Label lblqtyin = ((Label)e.Row.FindControl("lblqtyin"));
            Label lblqtyout = ((Label)e.Row.FindControl("lblqtyout"));
          


            string transDate = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "InvDate"));
          
            if (DataBinder.Eval(e.Row.DataItem, "InvDate").ToString() != "")
            {
                lbltransDate.Text = ClsGetDate.FillFromDate(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "InvDate")));
            }
            decimal qtyin =0;
            decimal qtyout =0;
            if (DataBinder.Eval(e.Row.DataItem, "qtyin").ToString() != "")
            {
                 qtyin = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "qtyin"));
            }
            if (DataBinder.Eval(e.Row.DataItem, "qtyout").ToString() != "")
            {
                 qtyout = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "qtyout"));
            }
               

                if (e.Row.RowIndex.ToString() != "0")
                {
                    if (qtyin > 0)
                    {
                        lblqtyin.Text = String.Format("{0:C}", qtyin).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                        lblqtyout.Text = "0.00";
                        Totalqtyin = Totalqtyin + qtyin;
                        TotalBalance = TotalBalance + qtyin;
                    }
                    else
                    {
                        lblqtyout.Text = String.Format("{0:C}", qtyout).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                        lblqtyin.Text = "0.00";
                        TotalqtyOut = TotalqtyOut + qtyout;
                        TotalBalance = TotalBalance + qtyout;
                    }

                }
                else
                {
                    if (qtyin > 0)
                    {
                        TotalBalance = qtyin;
                    }
                    else
                    {
                        TotalBalance = qtyout;
                    }
                }
                if (TotalBalance > 0) // For balance amounts
                {
                    lblBalance.Text = String.Format("{0:C}", TotalBalance).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ') ;
                }
                else
                {
                    lblBalance.Text = String.Format("{0:C}", TotalBalance).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
                    lblBalance.Text = "-" + lblBalance.Text;
                    lblBalance.Style.Add("Color", "red");
                }


            


        }

        if (e.Row.RowType == DataControlRowType.Footer)  // For footer totals
        {

            Label lblTotalqtyin = (Label)e.Row.FindControl("lblTotalqtyin");
            lblTotalqtyin.Text = String.Format("{0:C}", Totalqtyin).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblTotalqtyOut = (Label)e.Row.FindControl("lblTotalqtyOut");
          lblTotalqtyOut.Text = String.Format("{0:C}", TotalqtyOut).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblTotalTransection = (Label)e.Row.FindControl("lblTotalTransection");
            lblTotalTransection.Text = "=" + ((grdLedger.Rows.Count) - 1).ToString() + "=";
        }

    }
    #endregion

    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }


    }
    #endregion
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }

            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
}
