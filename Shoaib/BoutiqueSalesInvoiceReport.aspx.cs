﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
public partial class BoutiqueSalesInvoiceReport : System.Web.UI.Page
{
 #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();

    //decimal totalInvDispercent = 0;
    decimal totalAmountBeforDis = 0;
    Reports objReports = new Reports();

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        //totalInvDispercent = 0;
        totalAmountBeforDis = 0;
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        if (!IsPostBack)
        {
            txtInvNo.Focus();
            FillClientAC();
            FillCostCenter();
            txtInvoiceDateFrom.Text = Convert.ToDateTime(Sessions.FromDate).ToString("dd MMM yyyy");
            txtInvoiceDateTo.Text = System.DateTime.Now.ToString("dd MMM yyyy");

        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {        
        objSalesInvoice.invDateTo = Convert.ToDateTime(txtInvoiceDateTo.Text);
        objSalesInvoice.InvDateFrom = Convert.ToDateTime(txtInvoiceDateFrom.Text);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.ClientAccountId = Convert.ToInt32(ddlClientAC.SelectedValue);
        objSalesInvoice.DataFrom = "SALESINV";
        objSalesInvoice.InvNo = txtInvNo.Text;
        objSalesInvoice.CostCenterCode = Convert.ToInt32(ddlCostCenter.SelectedValue);

        DataTable dt = new DataTable();
        dt = objSalesInvoice.SearchInvoice();

        // Shoaib Worked 20-jan-2018
        DataColumn boutiqueDiscount = new DataColumn("TotalBoutiqueDiscount", typeof(decimal));
        dt.Columns.Add(boutiqueDiscount);

        DataColumn boutDisNotPercent = new DataColumn("TotalBoutDisNotPercent", typeof(decimal));
        dt.Columns.Add(boutDisNotPercent);

        DataColumn amountNotDiscountInc = new DataColumn("AmountNotDiscountInc", typeof(decimal));
        dt.Columns.Add(amountNotDiscountInc);

        

        objReports.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objReports.DataFrom = "SALESINV";
        objReports.ClientAccountId = Convert.ToInt32(ddlClientAC.SelectedValue);
        objReports.CostCenterCode = Convert.ToInt32(ddlCostCenter.SelectedValue);
        if (dt.Rows.Count > 0)
        {

            for (var row= 0; row < dt.Rows.Count; row++)
            {
                decimal disc = 0;
                decimal discNotInPer = 0;
                decimal amountNotIncDis = 0;
                objReports.InvoiceSummaryId = Convert.ToInt32(dt.Rows[row][0]);
                DataSet ds = new DataSet();
                ds = objReports.getInvoiceMainDetailBoutique();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (var data = 0; data < ds.Tables[0].Rows.Count; data++)
                    {
                        //disc += Convert.ToDecimal(ds.Tables[0].Rows[data][42]);
                        discNotInPer += ((Convert.ToDecimal(ds.Tables[0].Rows[data][20]) * Convert.ToInt32(ds.Tables[0].Rows[data][27]))
                                            - Convert.ToDecimal(ds.Tables[0].Rows[data][21]));
                        amountNotIncDis += (Convert.ToDecimal(ds.Tables[0].Rows[data][20]) * Convert.ToInt32(ds.Tables[0].Rows[data][27]));
                    }
                    disc = (discNotInPer / amountNotIncDis) * 100;
                }
                totalAmountBeforDis += amountNotIncDis;
                dt.Rows[row][37] = disc;
                dt.Rows[row][38] = discNotInPer;
                dt.Rows[row][39] = amountNotIncDis;

            }
        }
        

        dgGallery.DataSource = dt;
        dgGallery.DataBind();
    }
    #region Fill  Client A/C
    protected void FillClientAC()
    {
        
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtClient = new DataTable();
            dtClient = objSalesInvoice.getAcFileBySundryDebtorOnly();
            if (dtClient.Rows.Count > 0)
            {
                ddlClientAC.DataSource = dtClient;
                ddlClientAC.DataTextField = "Title";
                ddlClientAC.DataValueField = "Id";
                ddlClientAC.DataBind();
                ddlClientAC.Items.Insert(0, new ListItem("Select Client A/c", "0"));

            }
     

    }
    #endregion

    protected void dgGallery_RowDataBound(object sender, GridViewRowEventArgs e)
    {        
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            if (DataBinder.Eval(e.Row.DataItem, "InvDate").ToString() != "")
            {
                Label lblInvDate = (Label)e.Row.FindControl("lblinvDate");
                lblInvDate.Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Row.DataItem, "InvDate").ToString());
            }

            // Shoaib Work Start
            if (DataBinder.Eval(e.Row.DataItem, "InvoiceDiscount").ToString() != "" &&
                Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "InvoiceDiscount")).ToString() != "0")
            {
                if(DataBinder.Eval(e.Row.DataItem, "BillAmount").ToString() != "")
                {
                    Decimal discountPercent = 0;
                    Label lblinvDisPercent = (Label)e.Row.FindControl("lblinvDisPercent");
                    discountPercent = ((Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "InvoiceDiscount").ToString()) * 100)
                                            / (Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "BillAmount")) + Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "InvoiceDiscount"))));
                    //totalInvDispercent += discountPercent;
                    lblinvDisPercent.Text = String.Format("{0:0.##}", discountPercent);
                }
            }
            else
            {
                Label lblinvDisPercent = (Label)e.Row.FindControl("lblinvDisPercent");
                lblinvDisPercent.Text = String.Format("{0:0.##}", 0.00);
            }

            //if (DataBinder.Eval(e.Row.DataItem, "BillAmount").ToString() != "" &&
            //    Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "InvoiceDiscount")).ToString() != "")
            //{
            //    decimal amountBdisount = 0;
            //    Label lblamountNotDiscountInc = (Label)e.Row.FindControl("lblamountNotDiscountInc");
            //    amountBdisount = (Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "BillAmount")) + Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "InvoiceDiscount"))
            //                        + Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TotalBoutDisNotPercent")));
            //    totalAmountBeforDis += amountBdisount;
            //    lblamountNotDiscountInc.Text = amountBdisount.ToString();
            //}
            //else
            //{
            //    Label lblamountNotDiscountInc = (Label)e.Row.FindControl("lblamountNotDiscountInc");
            //    lblamountNotDiscountInc.Text = String.Format("{0:0.##}", 0.00);
            //}
            //Shoaib Work End

            if (Sessions.UserRole.ToString() != "Admin")
            {


                Button btnEdit = (Button)e.Row.FindControl("btnEdit");

                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
                if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
                {
                    Response.Redirect(Request.UrlReferrer.ToString());
                }

                if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
                {

                   // btnEdit.Visible = false;

                }

            }

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            objSalesInvoice.invDateTo = Convert.ToDateTime(txtInvoiceDateTo.Text);
            objSalesInvoice.InvDateFrom = Convert.ToDateTime(txtInvoiceDateFrom.Text);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.ClientAccountId = Convert.ToInt32(ddlClientAC.SelectedValue);
            objSalesInvoice.DataFrom = "SALESINV";
            objSalesInvoice.InvNo = txtInvNo.Text;
            objSalesInvoice.CostCenterCode = Convert.ToInt32(ddlCostCenter.SelectedValue);

            DataTable dt = new DataTable();
            dt = objSalesInvoice.SearchInvoice();

            //Label lblTotalGrossAmount = (Label)e.Row.FindControl("lblTotalGrossAmount");
            //decimal TotalGrossAmount = (decimal)dt.Compute("SUM(TotalGrossAmount)", "");
            //lblTotalGrossAmount.Text = String.Format("{0:C}", TotalGrossAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            
            //Label lblinvSubDisPercent = (Label)e.Row.FindControl("lblinvSubDisPercent");
            //decimal TotalSaleTaxAmount = (decimal)dt.Compute("SUM(TotalSaleTaxAmount)", "");
            //lblinvSubDisPercent.Text = String.Format("{0:C}", TotalSaleTaxAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            Label lblTotalSaleTaxAmount = (Label)e.Row.FindControl("lblTotalSaleTaxAmount");
            decimal TotalSaleTaxAmount = (decimal)dt.Compute("SUM(TotalSaleTaxAmount)", "");
            lblTotalSaleTaxAmount.Text = String.Format("{0:C}", TotalSaleTaxAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            //Label lblTotalAmountIncludeTax = (Label)e.Row.FindControl("lblTotalAmountIncludeTax");
            //decimal TotalAmountIncludeTax = (decimal)dt.Compute("SUM(TotalAmountIncludeTax)", "");
            //lblTotalAmountIncludeTax.Text = String.Format("{0:C}", TotalAmountIncludeTax).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblBillAmount = (Label)e.Row.FindControl("lblBillAmount");
            decimal BillAmount = (decimal)dt.Compute("SUM(BillAmount)", "");
            lblBillAmount.Text = String.Format("{0:C}", BillAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


            Label lblCartageAmount = (Label)e.Row.FindControl("lblCartageAmount");
            decimal CartageAmount = (decimal)dt.Compute("SUM(CartageAmount)", "");
            lblCartageAmount.Text = String.Format("{0:C}", CartageAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lblInvoiceDiscount = (Label)e.Row.FindControl("lblInvoiceDiscount");
            decimal InvoiceDiscount = (decimal)dt.Compute("SUM(InvoiceDiscount)", "");
            lblInvoiceDiscount.Text = String.Format("{0:C}", InvoiceDiscount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            //Label lbltotalInvDisPercent = (Label)e.Row.FindControl("lbltotalInvDisPercent");
            //lbltotalInvDisPercent.Text = String.Format("{0:C}", totalInvDispercent).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

            Label lbltotalAmountNotDiscountInc = (Label)e.Row.FindControl("lbltotalAmountNotDiscountInc");
            lbltotalAmountNotDiscountInc.Text = String.Format("{0:C}", totalAmountBeforDis).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            
        }

    }
    #region Fill Cost Center
    protected void FillCostCenter()
    {
        
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.CostCenterCode = 0;
            DataSet ds = new DataSet();
            ds = objCust.GetCostCenter();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlCostCenter.DataSource = ds.Tables[0];
                ddlCostCenter.DataTextField = "CostCenterName";
                ddlCostCenter.DataValueField = "CostCenterCode";
                ddlCostCenter.DataBind();
                ddlCostCenter.Items.Insert(0, new ListItem("Select cost center", "0"));

            }

       

    }
    #endregion
    protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditCustomer")
        {
            Response.Redirect("POSSaleInvoice.aspx?InvoiceSummaryId=" + e.CommandArgument.ToString());
        }

    }
}
