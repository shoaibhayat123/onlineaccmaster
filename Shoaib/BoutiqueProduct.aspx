﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="BoutiqueProduct.aspx.cs" Inherits="BoutiqueProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Boutique Product Setup"></asp:Label>
    <script type="text/javascript">
        function isNumericRepeter(obj) {
            if (obj.value != null && !obj.value.toString().match(/^[-]?\d*\.?\d*$/)) {

                obj.value = "0.00";

                var aa = document.getElementById('<%=lblTotal.ClientID %>').value;
                var SUM = 0;
                var f = document.getElementById("tblCostCenter");
                for (var i = 0; i < f.getElementsByTagName("input").length; i++) {
                    if (f.getElementsByTagName("input").item(i).type == "text") {
                        SUM += parseFloat(f.getElementsByTagName("input").item(i).value);
                        document.getElementById('<%=lblTotal.ClientID %>').value = parseFloat(SUM);
                    }
                }
                alert("Please Enter Numeric Value");

            }
            else {

                if (obj.value < 0) {
                    obj.value = "0.00";
                    var aa = document.getElementById('<%=lblTotal.ClientID %>').value;
                    var SUM = 0;
                    var f = document.getElementById("tblCostCenter");
                    for (var i = 0; i < f.getElementsByTagName("input").length; i++) {
                        if (f.getElementsByTagName("input").item(i).type == "text") {
                            SUM += parseFloat(f.getElementsByTagName("input").item(i).value);
                            document.getElementById('<%=lblTotal.ClientID %>').value = parseFloat(SUM);
                        }
                    }
                    alert("Please Enter Valid Value");
                }
                else {
                    var aa = document.getElementById('<%=lblTotal.ClientID %>').value;
                    var SUM = 0;
                    var f = document.getElementById("tblCostCenter");
                    for (var i = 0; i < f.getElementsByTagName("input").length; i++) {
                        if (f.getElementsByTagName("input").item(i).type == "text") {
                            SUM += parseFloat(f.getElementsByTagName("input").item(i).value);
                            document.getElementById('<%=lblTotal.ClientID %>').value = parseFloat(SUM);
                        }
                    }
                }
            }
        }


        function askDeleteProduct() {
            var agree;
            agree = confirm("Are You sure to delete this Product ?");
            if (agree) {
                return true;
            }
            return false;

        }
       
    </script>
     <script type="text/javascript">
         Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);

         function BeginRequestHandler(sender, args) {

             if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Save") {

                 document.getElementById('<%= btn_save.ClientID %>').value = "Saving...";
                 args.get_postBackElement().disabled = true;
             }


         }
         function SaveClick() {
             document.getElementById('<%= hdnButtonText.ClientID %>').value = "Save";
         }

        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>.
          <asp:HiddenField ID="hdnButtonText" runat="server" />
            <table cellspacing="0" style="float: left" border="0" cellpadding="0" width="100%">
                <tr>
                    <td width="39%">
                        <table border="0" cellpadding="5" cellspacing="5">
                            <tr>
                                <td width="100%" valign="top" style="left: 5; padding-top: 10px;">
                                    <table style="margin-left: 10px;">
                                        <tr>
                                            <td align="left">
                                                Item Code <span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtItemCode" Style="text-transform: uppercase;" CssClass="input_box2"
                                                    MaxLength="250" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtItemCode"
                                                    InitialValue="0" Display="None" ErrorMessage="Please fill Item Code" SetFocusOnError="True"
                                                    ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Item Description <span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtItemDesc" onChange="capitalizeMe(this)" CssClass="input_box2"
                                                    MaxLength="250" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlMainCategory"
                                                    InitialValue="0" Display="None" ErrorMessage="Please fill  item Description"
                                                    SetFocusOnError="True" ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Article Type <span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlarticleType" runat="server" Style="width: 282px;">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlarticleType"
                                                    InitialValue="0" Display="None" ErrorMessage="Please select Article Type" SetFocusOnError="True"
                                                    ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Article Name <span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlarticleName" runat="server" Style="width: 282px;">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlarticleName"
                                                    InitialValue="0" Display="None" ErrorMessage="Please select Article Name" SetFocusOnError="True"
                                                    ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Color <span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlColor" runat="server" Style="width: 282px;">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvColor" runat="server" ControlToValidate="ddlColor"
                                                    InitialValue="0" Display="None" ErrorMessage="Please select Color" SetFocusOnError="True"
                                                    ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Size <span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlSize" runat="server" Style="width: 282px;">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="rfvSize" runat="server" ControlToValidate="ddlSize"
                                                    InitialValue="0" Display="None" ErrorMessage="Please select Size " SetFocusOnError="True"
                                                    ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="left">
                                                Brand<span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlMainCategory" AutoPostBack="true" runat="server" Style="width: 282px;"
                                                    OnSelectedIndexChanged="ddlMainCategory_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlMainCategory"
                                                    InitialValue="0" Display="None" ErrorMessage="Please select Brand" SetFocusOnError="True"
                                                    ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Label<span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlSubCategory" Style="width: 282px;" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlSubCategory"
                                                    InitialValue="0" Display="None" ErrorMessage="Please select Label" SetFocusOnError="True"
                                                    ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Unit <span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtUnit" Text="PCS" onChange="capitalizeMe(this)" CssClass="input_box2"
                                                    MaxLength="10" runat="server"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtUnit"
                                                    Display="None" ErrorMessage="Please fill Unit" SetFocusOnError="True" ValidationGroup="v">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td align="left">
                                                Packing <span style="color: Red;">*</span> :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtPacking" onChange="capitalizeMe(this)" CssClass="input_box2"
                                                    MaxLength="10" runat="server"></asp:TextBox>
                                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtPacking"
                                                    Display="None" ErrorMessage="Please fill Packing" SetFocusOnError="True" ValidationGroup="v">*</asp:RequiredFieldValidator>--%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Purchase Rate :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtPurchaseRate" ReadOnly="true" onChange="isNumericDecimal(this)" CssClass="input_box2"
                                                    MaxLength="10" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Selling Price :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtSaleRate" MaxLength="10" ReadOnly="true" onChange="isNumericDecimal(this)" CssClass="input_box2"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Sales Tax (%) :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtSalesTax" MaxLength="10" onChange="isNumericDecimal(this)" CssClass="input_box2"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Duty (%) :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtDuty" MaxLength="10" onChange="isNumericDecimal(this)" CssClass="input_box2"
                                                    runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td align="left">
                                               Purchase Type :
                                            </td>
                                            <td align="left">
                                               <asp:Label ID="lblPurchaseType" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="display:none" >
                                            <td align="left">
                                                Barcode ID :
                                            </td>
                                            <td align="left">
                                               
                                                    <asp:Label ID="txtBarCode" runat="server"></asp:Label> 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Inventory Item :
                                            </td>
                                            <td align="left">
                                                <asp:CheckBox ID="chkInvetory" Checked="true" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div style="height: 160px; overflow: auto">
                                                    <table width="100%" border="1" id="tblCostCenter" style="border-collapse: collapse;">
                                                        <tr>
                                                            <td style="width: 50%; background-color: #85A68C; color: White; text-align: center">
                                                                Cost Center
                                                            </td>
                                                            <td style="background-color: #85A68C; color: White; text-align: center">
                                                                Opening Quantity
                                                            </td>
                                                        </tr>
                                                        <asp:Repeater ID="rptrCostCenter" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="text-align: left; padding-left: 10px">
                                                                        <asp:HiddenField ID="hdnCostcenterCode" Value='<%# Eval("CostCenterCode")%>' runat="server" />
                                                                        <asp:Label ID="lblCostcenter" Text='<%# Eval("CostCenterName")%>' runat="server"></asp:Label>
                                                                    </td>
                                                                    <td style="text-align: center">
                                                                        <asp:TextBox ID="txtCostCenter" Style="text-align: right" onChange="isNumericRepeter(this)"
                                                                            Text="0.00" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 50%; text-align: right">
                                            </td>
                                            <td style="font-weight: bold; text-align: right; padding-right: 44px;">
                                                Total&nbsp;&nbsp;:&nbsp;&nbsp&nbsp;<%--<asp:Label ID="lblTotal" Text="0.00" runat="server"></asp:Label>--%><asp:TextBox
                                                    ID="lblTotal" Enabled="false" Style="text-align: right" Text="0.00" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                               
                                            </td>
                                            <td>
                                                <asp:HiddenField runat="server" ID="hdnItemId" />
                                                <asp:Button ID="btnBarCodePrint"  OnClick="btnBarCodePrint_Click" Text="Print Barcode" runat="server" />
                                                <asp:Button ID="btn_save" runat="server" OnClientClick="SaveClick()" Text="Save" OnClick="btn_save_Click" ValidationGroup="v" />
                                                <asp:Button ID="btn_cancel" runat="server" Text="Cancel" OnClick="btn_cancel_Click"
                                                    ValidationGroup="v" CausesValidation="false" />
                                                <br />
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                                    ShowSummary="False" ValidationGroup="v" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" style="text-align: left; float: left" width="40%" align="left">
                        <div style="text-align: center; width: 600px; height: 510px; overflow: auto; margin: 0;
                            padding-top: 20px">
                            Search :<asp:TextBox ID="txtSerch" Style="text-transform: uppercase;" runat="server"></asp:TextBox>
                            <asp:Button ID="btnSearch" Text="Search" runat="server" OnClick="btnSearch_Click" />
                            <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="No record found." Visible="false"></asp:Label>
                            <asp:DataGrid ID="grdCustomer" Width="100%" runat="server" AutoGenerateColumns="False"
                                AllowPaging="false" PageSize="15" PagerSettings-Mode="Numeric" OnPageIndexChanged="grdCustomer_PageIndexChanged"
                                OnItemCommand="grdCustomer_RowCommand" HeaderStyle-CssClass="gridheader" AlternatingItemStyle-CssClass="gridAlternateItem"
                                GridLines="none" BorderColor="black" PagerStyle-Mode="NumericPages" BorderStyle="Dotted"
                                BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="grdCustomer_RowDataBound">
                                <Columns>
                                    <asp:TemplateColumn HeaderText="S.No.">
                                        <ItemStyle Width="5%" Font-Size="12px" />
                                        <ItemTemplate>
                                            <%# Container.DataSetIndex + 1%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Item">
                                        <ItemStyle Width="40%" HorizontalAlign="Left" Font-Size="12px" CssClass="gridpadding" />
                                        <ItemTemplate>
                                            <%# Eval("ItemDesc")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Item Description">
                                        <ItemStyle Width="40%" HorizontalAlign="Left" Font-Size="12px" CssClass="gridpadding" />
                                        <ItemTemplate>
                                            <%# Eval("BoutiqueItemDescription")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--<asp:TemplateColumn HeaderText="Unit">
                                        <ItemStyle Width="10%" />
                                        <ItemTemplate>
                                            <%# Eval("Unit")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Total Quantity">
                                        <ItemStyle Width="15%" HorizontalAlign="Right" />
                                        <ItemTemplate>
                                            <%# Eval("Opbal")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>--%>
                                    <asp:TemplateColumn HeaderText="Edit">
                                        <ItemStyle Width="10%" />
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lbEdit" CommandArgument='<%#Eval("ItemId") %>'
                                                CommandName="Edit" Text="Edit"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Delete">
                                        <ItemStyle Width="10%" />
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="lbDelete" OnClientClick="return askDeleteProduct();"
                                                CommandArgument='<%#Eval("ItemId") %>' CommandName="Delete" Text="Delete"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
