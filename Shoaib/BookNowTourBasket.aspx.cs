﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using TourBLL;

public partial class BookNowTourBasket : System.Web.UI.Page
{
    StoreProc _objStoreProc = new StoreProc();
    DataTable dTab = new DataTable();
    public static string _CurrencySymbol;
    public static string strlblTotalCost;
    public static string strlblshippingCharges;
    public static string strlblDiscountedAmount;
    public static string strlblAmountAfterPromotion;
    public static string strfinalTotal;
    int isdefault = 0;

    #region Page_Load()
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Show Cart
            BindCart();

            int BookHotel = HttpContext.Current.Session["BookHotel"] != null ? Convert.ToInt32(Session["BookHotel"].ToString()) : 0;

            if (BookHotel != 1)
            {
                divAvailable.Visible = false;
            }

            if (ClsBasket.GetCart().Rows.Count == 0)
            {
                ClsSession.HotelName = null;
                ClsSession.RoomType = null;
                ClsSession.RoomPrice = null;
                ClsSession.Rooms = null;
                ClsSession.HotelAmount = null;
                ClsSession.HotelID = null;
            }

            if (ClsSession.Rooms != null && ClsBasket.GetCart() != null)
            {
                divAvailable.Visible = true;
                lblHotelName1.Text = Convert.ToString(ClsSession.HotelName);
                lblRoomType1.Text = Convert.ToString(ClsSession.RoomType);
                lblPrice1.Text = Convert.ToString(ClsSession.RoomPrice);
                lblRooms1.Text = Convert.ToString(ClsSession.Rooms);
                lblAmount1.Text = Convert.ToString(ClsSession.HotelAmount);

            }

            //set ref. for 'Continue Shopping'      
            if ((Request.UrlReferrer != null))
            {
                ViewState["Cont"] = Request.UrlReferrer.ToString();
            }
        }
    }
    #endregion

    #region btnFinalPayment_Click()
    protected void btnFinalPayment_Click(object sender, ImageClickEventArgs e)
    {
        int OrderID;
        string strVisaType = "0";
        string strVisaAmount = "0";
        string PassportPic = "";
        string PassportCopy = "";
        OrderID = _objStoreProc.Insert_NewOrder(System.DateTime.Now, txtBillingFname.Text.Trim(), txtBillingLName.Text.Trim(), txtBillingEmail.Text.Trim(), txtBillindAdd1.Text.Trim(), Convert.ToString(ddlBillingCountry.SelectedItem), txtBillingZip.Text.Trim(), txtBillingTelephone.Text.Trim());

        ClsSession.ItemName = "Cart";
        ClsSession.OrderId = OrderID;
        ClsSession.FirsName = txtBillingFname.Text.Trim();
        ClsSession.LastName = txtBillingLName.Text.Trim();
        ClsSession.Address1 = txtBillindAdd1.Text.Trim();
        ClsSession.Address2 = txtBillingAdd2.Text.Trim();
        ClsSession.city = txtBillingCity.Text.Trim();
        ClsSession.Email = txtBillingEmail.Text.Trim();
        ClsSession.state = txtBillingState.Text.Trim();
        ClsSession.zip = txtBillingZip.Text.Trim();
        ClsSession.Country = Convert.ToString(ddlBillingCountry.SelectedValue);
        //ClsSession.FinalPayableAmount = lblAmount.Text.Trim();
        DataSet ds1 = null;
        DataTable dtBookCart = null;

        if (chkVisa.Checked == true)
        {
            if (ddlVisaType.SelectedValue != "0")
            {
                strVisaType = Convert.ToString(ddlVisaType.SelectedItem);
                strVisaAmount = Convert.ToString(ddlVisaType.SelectedValue);
                if(FuPassportPic.HasFile== true)
                {
                    PassportPic = UploadImage(FuPassportPic);
                }
                if (FuPassprotCopy.HasFile == true)
                {
                    PassportCopy = UploadImage(FuPassprotCopy);
                }


            }
            else
            {
                strVisaType = "0";
                strVisaAmount = "0";
            }
        }
        else
        {
            strVisaType = "0";
            strVisaAmount = "0";
        }

        if (ClsSession.Rooms != null)
        {
            double FinalAmountCart = Convert.ToDouble(Session["FinalAmountCart"]);
            ClsSession.FinalPayableAmount = Convert.ToString(Convert.ToDouble(ClsSession.HotelAmount) + FinalAmountCart);
            ClsSession.FinalPayableAmount = Convert.ToString(Convert.ToDouble(ClsSession.FinalPayableAmount) + Convert.ToDouble(strVisaAmount));

            ds1 = new DataSet();
            dtBookCart = new DataTable();

            string strXml = "";

            ds1.Tables.Add(ClsBasket.GetCart());
            strXml = ds1.GetXml();

            ds1.Dispose();

            //save data to DB              
            _objStoreProc.sp_InsertBookNowCart(Convert.ToInt32(OrderID), strXml);

            _objStoreProc.Update_BookNowCart(Convert.ToInt32(OrderID), strVisaType, strVisaAmount, PassportPic, PassportCopy);

            _objStoreProc.Insert_BookHotelRooms(Convert.ToInt32(OrderID), Convert.ToInt32(ClsSession.RoomID), Convert.ToInt32(ClsSession.Rooms), Convert.ToString(ClsSession.RoomPrice), Convert.ToString(ClsSession.HotelAmount), System.DateTime.Now, System.DateTime.Now);
        }
        else
        {
            ds1 = new DataSet();
            dtBookCart = new DataTable();

            string strXml = "";

            ds1.Tables.Add(ClsBasket.GetCart());
            //strXml = ds1.GetXml();

            //ds1.Dispose();

            ////save data to DB              
            //_objStoreProc.sp_InsertBookNowCart(Convert.ToInt32(OrderID), strXml);

            if (ds1.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
                {
                    _objStoreProc.addBookNowCart(Convert.ToInt32(ds1.Tables[0].Rows[i]["ProductId"]), Convert.ToString(ds1.Tables[0].Rows[i]["ProductName"]), Convert.ToInt32(ds1.Tables[0].Rows[i]["intQty"]), Convert.ToString(ds1.Tables[0].Rows[i]["Price"]), Convert.ToInt32(OrderID), Convert.ToDateTime(ds1.Tables[0].Rows[i]["BookingDate"]));
                }
                //_objStoreProc.addBookNowCart(ds1
            }


           _objStoreProc.Update_BookNowCart(Convert.ToInt32(OrderID), strVisaType, strVisaAmount, PassportPic, PassportCopy);

            ClsSession.FinalPayableAmount = Convert.ToString(Session["FinalAmountCart"]);
            ClsSession.FinalPayableAmount = Convert.ToString(Convert.ToDouble(ClsSession.FinalPayableAmount) + Convert.ToDouble(strVisaAmount));
        }

        if (OrderID > 0)
        {
            Response.Redirect("PayPal.aspx");
        }
    }
    #endregion

    #region BindCart()
    protected void BindCart()
    {
        //Bind Grid
        DataTable dt = new DataTable();
        dt = (DataTable)ClsBasket.GetCart();
        if (dt != null)
        {
            if (dt.Rows.Count > 0)
            {
                gvBasket.DataSource = dt;
                gvBasket.DataBind();
            }
            else
            {
                gvBasket.DataSource = "";
                gvBasket.DataBind();
                //btnUpdateQty.Visible = false;
                //btnCheckout.Visible = false;
                // btnContinue.Visible = false;
                DIVtotal.Style.Add("display", "none");
            }
        }
        else
        {
            gvBasket.DataSource = "";
            gvBasket.DataBind();
            //btnUpdateQty.Visible = false;
            //btnCheckout.Visible = false;
            //btnContinue.Visible = false;
            DIVtotal.Style.Add("display", "none");
        }

        //show total cost with currency symbol   counting
        strlblTotalCost = String.Format("{0:0.00}", ClsBasket.CartValue());
        lblTotalCost.Text = _CurrencySymbol + "" + strlblTotalCost;
        string strfinalTotalForPaypal = strlblTotalCost;
        ClsSession.FinalPayableAmount = strfinalTotalForPaypal;

        Session["FinalAmountCart"] = strlblTotalCost;

        //show counting   
        lblTotalItem.Text = Convert.ToString(ClsBasket.CartItemCount());
        hid_pcount.Value = Convert.ToString(ClsBasket.CartItemCount());
        Session["TotalItem"] = Convert.ToString(ClsBasket.CartItemCount());
    }
    #endregion

    #region gvBasket_RowCommand()
    protected void gvBasket_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "deleteitem")
        {
            ClsBasket.RemoveItem(Convert.ToInt32(e.CommandArgument.ToString()));
            BindCart();
        }
    }
    #endregion

    #region imgbtn_continueshopping1_Click()
    protected void imgbtn_continueshopping1_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect(Convert.ToString(ViewState["Cont"]));
    }
    #endregion

    #region chkVisa_CheckedChanged()
    protected void chkVisa_CheckedChanged(object sender, EventArgs e)
    {
        if (chkVisa.Checked == true)
            divVisa.Visible = true;
        else
            divVisa.Visible = false;
    }
    #endregion


    private string UploadImage(FileUpload FileUpload1)
    {

        if (FileUpload1.HasFile == true)
        {
            if (FileUpload1.PostedFile.FileName != "")
            {
                string strImage = null;
                string FileName = string.Empty;
                string strExtension = "";

                string TempPath = "~/Images/";
                strExtension = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                string Extension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                
                    string strImageName = System.Guid.NewGuid().ToString().Replace("-", "") + strExtension;
                    FileUpload1.PostedFile.SaveAs(Server.MapPath(TempPath + "Passport/" + strImageName));                  
                    strImage = strImageName;                   
                    return strImage;

                
            }
        }
        else
        {
            Page.RegisterStartupScript("Msg1", "<script>alert('Please Upload Image');</script>");
        }
        return "";

    }
}
