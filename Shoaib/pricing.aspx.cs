﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class pricing : System.Web.UI.Page
{
    Car objCar = new Car();
    protected void Page_Load(object sender, EventArgs e)
    {
        FillMainCategory();
    }
    public void FillMainCategory()
    {
        objCar.ProductId = 0;
        DataTable dt = new DataTable();
        dt = objCar.GetProdutPrice();
        if (dt.Rows.Count > 0)
        {
            lblSimple.Text = dt.Rows[0]["Price"].ToString();
            lblBasic.Text = dt.Rows[1]["Price"].ToString();
            lblExtra.Text = dt.Rows[2]["Price"].ToString();
            lblPlus.Text = dt.Rows[3]["Price"].ToString();

            lblSimpleUser.Text = dt.Rows[0]["NoOfUser"].ToString();
            lblBasicUser.Text = dt.Rows[1]["NoOfUser"].ToString();
            lblExtraUser.Text = dt.Rows[2]["NoOfUser"].ToString();
            lblPlusUser.Text = dt.Rows[3]["NoOfUser"].ToString();
        }

    }
}