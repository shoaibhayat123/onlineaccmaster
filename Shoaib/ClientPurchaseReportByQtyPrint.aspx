﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClientPurchaseReportByQtyPrint.aspx.cs" Inherits="ClientPurchaseReportByQtyPrint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Tahoma; background: none; font-size: 10px;">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; font-weight: bold;">
             <tr id="trTrialBal" runat="server">
                <td>
                    <asp:Label ID="lblSuplierName" runat="server"></asp:Label><br />
                    <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                    <span id="spAddress2" runat="server">
                        <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                    </span><span id="spAddress3" runat="server">
                        <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                    </span>
                    <asp:Label ID="lblCity" runat="server"></asp:Label>,
                    <asp:Label ID="lblCountry" runat="server"></asp:Label><br />
                    <asp:Label ID="lblPhoneNumber" runat="server"></asp:Label>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">Yearly
                        Purchase Report Client Wise  </span>
                    <br />
                    <span style="font-size: 14px;">(Purchase by Quantity)<br />Year :
                        <asp:Label ID="lblYear" runat="server" ></asp:Label>
                        <br />
                        <asp:Label ID="lblType" runat="server" ></asp:Label>
                    </span>
                    <asp:Label ID="lblBuyerName" runat="server"></asp:Label><br />
                    <span style="font-size: 12px; font-weight: normal">
                        <asp:Label ID="lblBuyerAddress" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress2" runat="server"></asp:Label>
                        <asp:Label ID="lblBuyerAddress3" runat="server"></asp:Label>
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <hr />
                    <br />
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <asp:DataGrid ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
            AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" FooterStyle-CssClass="gridFooter"
            HeaderStyle-CssClass="gridheader" ShowFooter="true" AlternatingItemStyle-CssClass="gridAlternateItem"
            GridLines="none" BorderColor="black" BorderStyle="Dotted" BorderWidth="1" OnItemDataBound="dgGallery_OnItemDataBound"
            ItemStyle-CssClass="gridItem">
            <Columns>
                <asp:TemplateColumn HeaderText="S.No.">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                    <ItemTemplate>
                        <%# Container.DataSetIndex + 1%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Party Name">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="left" Width="30%" />
                    <ItemTemplate>
                        <%# Eval("Title")%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Jan">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" Width="5%" />
                    <FooterStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%# String.Format("{0:C}", Eval("Month1")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalMonth1" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Feb">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" Width="5%" />
                    <FooterStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%# String.Format("{0:C}", Eval("Month2")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalMonth2" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Mar">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" Width="5%" />
                    <FooterStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%# String.Format("{0:C}", Eval("Month3")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalMonth3" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Apr">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" Width="5%" />
                    <FooterStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%# String.Format("{0:C}", Eval("Month4")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalMonth4" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="May">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" Width="5%" />
                    <FooterStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%# String.Format("{0:C}", Eval("Month5")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalMonth5" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Jun">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" Width="5%" />
                    <FooterStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%# String.Format("{0:C}", Eval("Month6")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalMonth6" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Jul">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" Width="5%" />
                    <FooterStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%# String.Format("{0:C}", Eval("Month7")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalMonth7" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Aug">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" Width="5%" />
                    <FooterStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%# String.Format("{0:C}", Eval("Month8")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalMonth8" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Sep">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" Width="5%" />
                    <FooterStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%# String.Format("{0:C}", Eval("Month9")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalMonth9" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Oct">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" Width="5%" />
                    <FooterStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%# String.Format("{0:C}", Eval("Month10")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalMonth10" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Nov">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" Width="5%" />
                    <FooterStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%# String.Format("{0:C}", Eval("Month11")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalMonth11" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Dec">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" Width="5%" />
                    <FooterStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%# String.Format("{0:C}", Eval("Month12")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalMonth12" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Total">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="right" Width="5%" />
                    <FooterStyle HorizontalAlign="Right" />
                    <ItemTemplate>
                        <%# String.Format("{0:C}",Eval("total")).Replace('$', ' ')%>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lblTotalMonth" runat="server"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateColumn>
            </Columns>
        </asp:DataGrid>
    </div>
    <div id="DvEmpty" runat="server" style="padding-bottom: 10px; width: 800px; display: none;
        text-align: center; font-family: Gisha; font-size: 20px; background-color: #E1F5FD;
        color: #110F0F; float: left">
        <div style="padding-top: 10px; padding-bottom: 5px;">
            No record found
        </div>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                </td>
            </tr>
              <tr>
                <td style="text-align: right">
                    <asp:LinkButton Text="Print" ID="lnkPrint" OnClientClick="window.print();" runat="server"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>

                                <script type="text/javascript">

                                            var currentTime = new Date()
                                            var hours = currentTime.getHours()
                                             var Newhours=0;
                                            var minutes = currentTime.getMinutes()
                                            if (minutes < 10)
                                            {
                                            minutes = "0" + minutes
                                            }                                           
                                            if(hours >12)
                                            {
                                            Newhours= hours-12;
                                            }
                                            else
                                            {
                                              Newhours= hours;
                                            }
                                            document.write(Newhours + ":" + minutes + " ")
                                             if(hours > 11)
                                            {                                           
                                            document.write("PM")
                                            } else 
                                            {
                                            document.write("AM")
                                            }

                                </script>

                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
