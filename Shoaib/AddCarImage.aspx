﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="AddCarImage.aspx.cs" Inherits="AddCarImage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Car Image"></asp:Label>
    <script type="text/javascript">

        function Count(text, long) {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }



        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }
          
    </script>
    <script type="text/javascript" src="highslide/highslide-with-gallery.js"></script>
    <link rel="stylesheet" type="text/css" href="highslide/highslide.css" />
    <script type="text/javascript">
        hs.graphicsDir = 'highslide/graphics/';
        hs.align = 'center';
        hs.transitions = ['expand', 'crossfade'];
        hs.outlineType = 'rounded-white';
        hs.fadeInOut = true;
        hs.numberPosition = 'caption';
        hs.dimmingOpacity = 0.75;

        // Add the controlbar
        if (hs.addSlideshow) hs.addSlideshow({
            //slideshowGroup: 'group1',
            interval: 500,
            repeat: false,
            useControls: true,
            fixedControls: 'fit',
            overlayOptions: {
                opacity: .75,
                position: 'bottom center',
                hideOnMouseOut: true
            }
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnADD" />
        </Triggers>
        <ContentTemplate>
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                width="100%">
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 13%">
                        VIN :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                        <table>
                            <tr>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                    <asp:HiddenField ID="hdnBrokerageId" runat="server" />
                                    <asp:DropDownList ID="ddlVIN" runat="server" AutoPostBack="true" Style="width: 200px"
                                        TabIndex="1" OnSelectedIndexChanged="ddlVIN_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Select  VIN." Text="*" InitialValue="0" ControlToValidate="ddlVIN"
                                        ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                                    <span id="spanPgging" runat="server" style="padding: 0 20px 0 20px; font-weight: bold;
                                        font-size: 18px">
                                        <asp:LinkButton ID="lnkFirst" Text="|<" runat="server" OnClick="lnkFirst_Click"> </asp:LinkButton>
                                        <asp:LinkButton ID="lnkPrevious" Style="margin-left: 10px;" Text="<" runat="server"
                                            OnClick="lnkPrevious_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkNext" Text=">" Style="margin-left: 10px;" runat="server" OnClick="lnkNext_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnLast" Text=">|" Style="margin-left: 10px;" runat="server" OnClick="lnLast_Click"></asp:LinkButton></span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" id="tdInventory" runat="server" style="display: none; padding-bottom: 25px;
                        padding-top: 25px">
                        <table>
                            <tr>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    Lot # :
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                    <asp:Label ID="lblLotNo" runat="server"></asp:Label>
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 100px;">
                                    Make:
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                    <asp:Label ID="lblMake" runat="server"></asp:Label>
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 100px;">
                                    Model :
                                </td>
                                <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                                    <asp:Label ID="lblModel" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                        <asp:RadioButton ID="rdImage" GroupName="Image" TabIndex="2" Checked="true" Text="Car image (150KB)" runat="server" />
                        <asp:RadioButton ID="rdText" GroupName="Image" TabIndex="3" Text="Scanned Car Document (500KB)" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:FileUpload ID="FuImage" Style="float: left" runat="server" TabIndex="4" /><asp:Button ID="btnADD"
                            Style="float: left" runat="server" OnClick="btnAddNew_Click" Text="Add New Image" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:DataGrid ID="dgGallery" Width="29.5%" runat="server" AutoGenerateColumns="False"
                            AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                            AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                            BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemCommand="dgGallery_ItemCommand">
                            <Columns>
                                <asp:TemplateColumn HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataSetIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Image">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="70%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                       
                                        <div class="slide" id="d1">
                                            <div class="highslide-gallery" style="position: relative; z-index: 50;">
                                                <a id="thumb1" href="<%# "images/CarIamge/"  + Eval("CarImage") %>" class="highslide"
                                                    onclick="return hs.expand(this)">
                                                    <img id="i1" src='<%# "images/CarIamge/"+ Eval("CarImage") %>' width="100" height="100"
                                                        alt="." border="0" /></a>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Delete">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="imgDel1" runat="server" Text="Delete" CommandArgument='<%#Eval("Id")%>'
                                            CommandName="Delete" OnClientClick="return askDeleteRow();" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: left">
                        <table cellpadding="0" cellspacing="0" style="padding: 5px 155px 0px 0px">
                            <tr>
                                <td align="right" valign="top" style="padding: 2px 2px 2px 8px; text-align: left;
                                    width: 270px;">
                                    <a>
                                        <asp:Button ID="lblbutton" Style="cursor: pointer" runat="server" Text="Add New"
                                            OnClick="lblbutton_Click" />
                                        <asp:Button Text="Delete" OnClientClick="return askDeleteEntry();" ID="btnDeleteInvoice"
                                            runat="server" OnClick="btnDeleteInvoice_Click" />
                                </td>
                                <td style="width: 470px">
                                    <table cellpadding="0" cellspacing="0" style="padding: 5px 155px 0px 0px; width: 100%">
                                        <tr>
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 33%;">
                                            </td>
                                            <td align="left" valign="top" style="padding: 2px 0px 2px 5px; text-align: right;
                                                width: 33%;">
                                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                                                    ShowSummary="false" ValidationGroup="AddInvoice" />
                                                <asp:ImageButton ID="ImageButton2" TabIndex="5" runat="server" ImageUrl="~/Images/btn-save.gif"
                                                    OnClick="btn_saveInvoice_Click" ValidationGroup="AddInvoice" />&nbsp;</div>
                                            </td>
                                            <td style="width: 33%; padding: 2px 2px 20px 3px;">
                                                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/bt-cancal.gif"
                                                    OnClick="btn_cancel_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
