﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="CashBankTransactions.aspx.cs" Inherits="CashBankTransactions" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text=" Cash / Bank Book Transaction"></asp:Label>
    <script type="text/javascript">
        function Count(text, long) {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }
        function ChckAmount(obj) {
            if (obj.value != null && !obj.value.toString().match(/^[-]?\d*\.?\d*$/) || obj.value == "") {
                obj.value = "0.00";
                alert("Please Enter Numeric Value");

            }
            else {
                if (parseFloat(obj.value) < 0) {
                    obj.value = "0.00";
                    alert("Invalid Quantity");
                }
            }

        }
    </script>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);

        function BeginRequestHandler(sender, args) {

            if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Save") {

                document.getElementById('<%= ImageButton2.ClientID %>').value = "Saving...";
                args.get_postBackElement().disabled = true;
            }
            if (document.getElementById('<%= hdnButtonText.ClientID %>').value == "Update") {

                document.getElementById('<%= btnAddNew.ClientID %>').value = "Updating...";
                args.get_postBackElement().disabled = true;
            }


        }
        function SaveClick() {
            document.getElementById('<%= hdnButtonText.ClientID %>').value = "Save";

        }

        function UpdateClick() {
            document.getElementById('<%= hdnButtonText.ClientID %>').value = "Update";

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
          <asp:HiddenField ID="hdnButtonText" runat="server" />
            <asp:HiddenField ID="hdnCashBookMainId" runat="server" />
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                width="100%">
                <tr>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                        Date :
                    </td>
                    <td align="left" valign="middle" style="padding: 2px 2px 2px 5px; text-align: left">
                        <table>
                            <tr>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 0px;">
                                    <asp:TextBox ID="txtCashBookDate" TabIndex="1" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtCashBookDate'),this);"
                                        runat="server" Width="100px"></asp:TextBox>
                                    <span id="calImage" runat="server">
                                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                            onclick="scwShow(scwID('ctl00_MainHome_txtCashBookDate'),this);" /></span>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                                        ErrorMessage="Please Fill  Date." Text="*" ControlToValidate="txtCashBookDate"
                                        ValidationGroup="AddCashBook"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                        ControlToValidate="txtCashBookDate" ValidationGroup="AddCashBook" ErrorMessage="Invalid  Date !!"
                                        Display="None"></asp:RegularExpressionValidator>
                                    <span id="spanPgging" runat="server" style="padding: 0 20px 0 20px; font-weight: bold;
                                        font-size: 18px">
                                        <asp:LinkButton ID="lnkFirst" Text="|<" runat="server" OnClick="lnkFirst_Click"> </asp:LinkButton>
                                        <asp:LinkButton ID="lnkPrevious" Style="margin-left: 10px;" Text="<" runat="server"
                                            OnClick="lnkPrevious_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkNext" Text=">" Style="margin-left: 10px;" runat="server" OnClick="lnkNext_Click"></asp:LinkButton>
                                        <asp:LinkButton ID="lnLast" Text=">|" Style="margin-left: 10px;" runat="server" OnClick="lnLast_Click"></asp:LinkButton></span>
                                </td>
                                <td id="tdbf" runat="server" align="left" valign="middle" style="padding: 2px 2px 2px 8px;
                                    display: none">
                                    B/F :
                                </td>
                                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                    <asp:Label ID="lblBf" Style="display: none" runat="server" Text="0.00"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 90px; padding: 2px 2px 2px 8px;">
                        Cash/Bank A/c :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                        <asp:DropDownList ID="ddlAC" TabIndex="2" runat="server" Style="width: 550px" OnSelectedIndexChanged="ddlAC_SelectedIndexChanged"
                            AutoPostBack="true">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please Select Cash/Bank A/c" Text="*" ControlToValidate="ddlAC"
                            ValidationGroup="AddCashBook"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                            InitialValue="0" ErrorMessage="Please Select Cash/Bank A/c" Text="*" ControlToValidate="ddlAC"
                            ValidationGroup="AddCashBookDetail"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="sundryDR" runat="server" style="width: 1000px; padding: 10px; overflow: auto;
                            border: solid 2px  #528627">
                            <table>
                                <tr>
                                    <td colspan="6">
                                        <asp:HiddenField ID="hdndetail" runat="server" />
                                        <asp:DataGrid ID="dgGallery" Width="1000px" runat="server" AutoGenerateColumns="False"
                                            ShowFooter="true" AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric"
                                            HeaderStyle-CssClass="gridheader" AlternatingItemStyle-CssClass="gridAlternateItem"
                                            GridLines="none" FooterStyle-CssClass="gridFooter" BorderColor="black" BorderStyle="Dotted"
                                            BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery_ItemDataBound"
                                            OnItemCommand="dgGallery_ItemCommand">
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="S.No.">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <%# Container.DataSetIndex + 1%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="A/C Title">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="left" Width="30%" />
                                                    <ItemTemplate>
                                                        <%# Eval("AcFileDesc")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Description">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="25%" HorizontalAlign="left" />
                                                    <ItemTemplate>
                                                        <%# Eval("Description")%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Receive">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                    <FooterStyle HorizontalAlign="Right" Font-Size="16px" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("Receive")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblTotalReceive" runat="server"></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Payment">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                    <FooterStyle HorizontalAlign="Right" Font-Size="16px" />
                                                    <ItemTemplate>
                                                        <%#String.Format("{0:C}", Eval("Payment")).Replace('$', ' ')%>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblTotalPayment" runat="server"></asp:Label>
                                                    </FooterTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Balance">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Right" Width="10%" />
                                                    <FooterStyle HorizontalAlign="Right" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBalance" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Delete">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="imgDel1" runat="server" Text="Delete" CommandArgument='<%#Eval("Id")%>'
                                                            CommandName="Delete" OnClientClick="return askDeleteRow();" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Edit">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="ImageButton1" runat="server" CommandArgument='<%#Eval("Id")%>'
                                                            Text="Edit" CommandName="Edit" />
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px; width: 90px;">
                                        A/C Title :
                                    </td>
                                    <td style="margin-right: 10px;" colspan="3">
                                        <asp:DropDownList ID="ddlAcdesc" TabIndex="3" Style="width: 550px" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" Display="Dynamic"
                                            InitialValue="0" ErrorMessage="Please Select A/C Title" Text="*" ControlToValidate="ddlAcdesc"
                                            ValidationGroup="AddCashBookDetail"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <%-- <td colspan="4">
                                        <table>
                                            <tr>--%>
                                    <td style="margin-right: 15px; width: 80px;">
                                        Receive :
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <asp:TextBox ID="txtReceive" runat="server" TabIndex="4" Text="0.00" MaxLength="15"
                                            onChange="ChckAmount(this)" Style="width: 150px;"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill Receive " Text="*" ControlToValidate="txtReceive" ValidationGroup="AddCashBookDetail"></asp:RequiredFieldValidator>
                                        <span style="margin-left: 80px">Payment :</span>
                                        <asp:TextBox ID="txtPayment" TabIndex="5" runat="server" Text="0.00" onChange="ChckAmount(this)"
                                            Style="width: 150px; margin-left: 30px" MaxLength="15"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill Amount " Text="*" ControlToValidate="txtPayment" ValidationGroup="AddCashBookDetail"></asp:RequiredFieldValidator>
                                    </td>
                                    <td style="margin-right: 10px; border: solid 0px red;">
                                    </td>
                                    <td style="margin-right: 10px;">
                                    </td>
                                    <%--  </tr>
                                        </table>
                                    </td>--%>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px; vertical-align: top">
                                        Description :
                                    </td>
                                    <td style="margin-right: 10px;" colspan="3">
                                        <asp:TextBox ID="txtDescriptionDetail" TabIndex="6" runat="server" onKeyUp="Count(this,250);"
                                            onChange="Count(this,250); capitalizeMe(this);" TextMode="MultiLine" Style="width: 550px;"
                                            Rows="2" MaxLength="250"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic"
                                            ErrorMessage="Please fill Description Detail " Text="*" ControlToValidate="txtDescriptionDetail"
                                            ValidationGroup="AddCashBookDetail"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" style="text-align: right">
                                        <asp:Button Style="text-align: right" ID="btnAddNew" TabIndex="7" OnClick="btnAddNew_Click" OnClientClick="UpdateClick()"
                                            runat="server" Text="Update" ValidationGroup="AddCashBookDetail" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                                            ShowSummary="false" ValidationGroup="AddCashBookDetail" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table>
                            <tr>
                                <td style="width: 440px;">
                                    <span>
                                        <%--   <asp:Button ID="lblbutton" Style="cursor: pointer" OnClientClick="location.href='CashBankTransactions.aspx?addnew=1'" runat="server" Text="Add New" />--%>
                                        <asp:Button ID="lblbutton" Style="cursor: pointer" runat="server" Text="Add New"
                                            OnClick="lblbutton_Click" />
                                        <asp:Button Text="Delete All Entries" OnClientClick="return askDeleteCashBook();"
                                            ID="btnDeleteInvoice" runat="server" OnClick="btnDeleteInvoice_Click" />
                                    </span>
                                </td>
                                <td style="width: 450px">
                                    <table cellpadding="0" cellspacing="0" style="padding: 5px 10px 0px 0px; width: 100%">
                                        <tr>
                                            <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 33%;">
                                            </td>
                                            <td align="left" valign="top" style="padding: 2px 0px 2px 5px; text-align: right;
                                                width: 33%;">
                                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                                                    ShowSummary="false" ValidationGroup="AddInvoice" />
                                                <%--        <asp:ImageButton ID="ImageButton2" TabIndex="10" runat="server" ImageUrl="~/Images/btn-save.gif"
                                                    OnClick="btn_saveInvoice_Click" ValidationGroup="AddInvoice" />--%>
                                                <asp:Button ID="ImageButton2" OnClientClick="SaveClick()" TabIndex="10" Text="Save"
                                                    runat="server" ValidationGroup="AddInvoice" OnClick="btn_saveInvoice_Click" />
                                                &nbsp;</div>
                                            </td>
                                            <td style="width: 33%; padding: 2px 2px 20px 3px;">
                                                <%--   <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/bt-cancal.gif"
                                                    OnClick="btn_cancel_Click" />--%>
                                                <asp:Button ID="ImageButton3" Text="Cancel" runat="server" OnClick="btn_cancel_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
