﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="BoutiquePriceList.aspx.cs" Inherits="BoutiquePriceList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Boutique Price List"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Brand<span style="color: Red;">*</span> :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:DropDownList ID="ddlMainCategory" AutoPostBack="true" runat="server" Style="width: 282px;"
                            OnSelectedIndexChanged="ddlMainCategory_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Label<span style="color: Red;">*</span> :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:DropDownList ID="ddlSubCategory" Style="width: 282px;" runat="server">
                        </asp:DropDownList>
                        <span style="float: right; padding-right: 65px">
                            <asp:Button ID="btnsearch" Text="Search" TabIndex="6" runat="server" OnClick="btnsearch_Click" />
                        </span>
                    </td>
                </tr>
            </table>
           <%-- <table cellspacing="0" style="float: left; padding: 0 10px 10px 10px;" border="0"
                cellpadding="0" width="90%">
                <tr>
                    <td colspan="3">
                        <asp:GridView ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                            AllowPaging="true" PageSize="500000" PagerSettings-Visible="false" PagerSettings-Mode="Numeric"
                            HeaderStyle-CssClass="gridheader" AlternatingRowStyle-CssClass="gridAlternateItem"
                            GridLines="none" BorderColor="black" FooterStyle-CssClass="gridFooter" BorderStyle="Dotted"
                            BorderWidth="1" RowStyle-CssClass="gridItem" ShowFooter="true">
                            <EmptyDataTemplate>
                                <div style="padding-bottom: 10px; width: 100%; text-align: center; font-family: Gisha;
                                    font-size: 20px; background-color: #E1F5FD; color: #110F0F;">
                                    <div style="padding-top: 10px; padding-bottom: 5px;">
                                        No record found
                                    </div>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item Code">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="20%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Eval("BoutiqueItemCode")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item Description">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="30%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Eval("BoutiqueItemDescription")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Unit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Eval("Unit")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SaleRate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Eval("SaleRate")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
