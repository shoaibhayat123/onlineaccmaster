﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintEmbDailyProductionValueReport : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    SetCustomers objCust = new SetCustomers();
    AccFiles objFile = new AccFiles();
    Reports objReports = new Reports();
    TmCountry objTmCountry = new TmCountry();
    Car objCar = new Car();

 
    decimal TotalPcs,TotalStitches,TotalAmount, TotalYard = 0;
    decimal ClientTotalPc, ClientTotalStitches, ClientTotalAmount, ClientTotalYard = 0;
    decimal GrandTotalPc, GrandTotalStitches, GrandTotalAmount, GrandTotalYard = 0;
    decimal Stitches1, Stitches2, Stitches3, Stitches4, Stitches5, Stitches6, Stitches7, Stitches8, Stitches9, Stitches10 = 0;
    decimal Amount1, Amount2, Amount3, Amount4, Amount5, Amount6, Amount7, Amount8, Amount9, Amount10 = 0;


    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();

        if (!IsPostBack)
        {
            bindSupplier();


            bindBuyer();
            FillLogo();
            BindBasicSearch();
            lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
            lblUser.Text = Sessions.UseLoginName.ToString();

            lblFromDate.Text = ClsGetDate.FillFromDate(Request["DateFrom"].ToString());
            lblToDate.Text = ClsGetDate.FillFromDate(Request["DateTo"].ToString());
            lblReportType.Text = Convert.ToString(Request["Type"].ToString());

        }
    }

    protected void bindBuyer()
    {


    }


    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }
        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }

    }
    #endregion
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();

        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }
    


    protected void BindBasicSearch()
    {

        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCar.ProductionDateFrom = Convert.ToDateTime(Request["DateFrom"]);
        objCar.ProductionDateTo = Convert.ToDateTime(Request["DateTo"]);
        objCar.ClientAcfileId = Convert.ToInt32(Request["ClientId"]);
        objCar.MachineNo = Convert.ToInt32(Request["MachineNo"]);
        objCar.QualityId = Convert.ToInt32(Request["QualityId"]);
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        ds = objCar.getEmbClientForProduction();
        if (ds.Tables[0].Rows.Count > 0)
        {
            rptMending.DataSource = ds.Tables[0];
            rptMending.DataBind();
        }

        lblClientTotalStitches.Text = Convert.ToString(GrandTotalStitches);
        lblClientTotalPcs.Text = Convert.ToString(GrandTotalPc);
        lblClientTotalYards.Text = Convert.ToString(GrandTotalYard);
        lblClientTotalAmount.Text = Convert.ToString(GrandTotalAmount);

        lblAmount1.Text = Amount1.ToString("0.00");
        lblTSt1.Text = Stitches1.ToString("0.00");

        lblAmount2.Text = Amount2.ToString("0.00");
        lblTSt2.Text = Stitches2.ToString("0.00");


        lblAmount3.Text = Amount3.ToString("0.00");
        lblTSt3.Text = Stitches3.ToString("0.00");


        lblAmount4.Text = Amount4.ToString("0.00");
        lblTSt4.Text = Stitches4.ToString("0.00");

        lblAmount5.Text = Amount5.ToString("0.00");
        lblTSt5.Text = Stitches5.ToString("0.00");

        lblAmount6.Text = Amount6.ToString("0.00");
        lblTSt6.Text = Stitches6.ToString("0.00");

        lblAmount7.Text = Amount7.ToString("0.00");
        lblTSt7.Text = Stitches7.ToString("0.00");

        lblAmount8.Text = Amount8.ToString("0.00");
        lblTSt8.Text = Stitches8.ToString("0.00");

        lblAmount9.Text = Amount9.ToString("0.00");
        lblTSt9.Text = Stitches9.ToString("0.00");

        lblAmount10.Text = Amount10.ToString("0.00");
        lblTSt10.Text = Stitches10.ToString("0.00");
        if (Amount1 == 0)
        {
            tr1.Style.Add("display", "none");
        }
        if (Amount2 == 0)
        {
            tr2.Style.Add("display", "none");
        }
        if (Amount3 == 0)
        {
            tr3.Style.Add("display", "none");
        }
        if (Amount4 == 0)
        {
            tr4.Style.Add("display", "none");
        }
        if (Amount5 == 0)
        {
            tr5.Style.Add("display", "none");
        }


        if (Amount6 == 0)
        {
            tr6.Style.Add("display", "none");
        }
        if (Amount7 == 0)
        {
            tr7.Style.Add("display", "none");
        }
        if (Amount8 == 0)
        {
            tr8.Style.Add("display", "none");
        }
        if (Amount9 == 0)
        {
            tr9.Style.Add("display", "none");
        }
        if (Amount10 == 0)
        {
            tr10.Style.Add("display", "none");
        }

        
    }


    protected void RptReport_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        ClientTotalAmount = 0;
        ClientTotalPc = 0;
        ClientTotalStitches = 0;
        ClientTotalYard = 0;

        Label lblClient = (Label)e.Item.FindControl("lblClient");
        lblClient.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Title"));

        Repeater rptQuality = (Repeater)e.Item.FindControl("rptQuality");
        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCar.ProductionDateFrom = Convert.ToDateTime(Request["DateFrom"]);
        objCar.ProductionDateTo = Convert.ToDateTime(Request["DateTo"]);
        objCar.ClientAcfileId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "ClientAcfileId"));
        objCar.MachineNo = Convert.ToInt32(Request["MachineNo"]);
        objCar.QualityId = Convert.ToInt32(Request["QualityId"]);
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        ds = objCar.getEmbQualityForProduction();
        if (ds.Tables[0].Rows.Count > 0)
        {
            rptQuality.DataSource = ds.Tables[0];
            rptQuality.DataBind();
        }

        ((Label)e.Item.FindControl("lblClientTotalStitches")).Text = Convert.ToString(ClientTotalStitches);
        ((Label)e.Item.FindControl("lblClientTotalPcs")).Text = Convert.ToString(ClientTotalPc);
        ((Label)e.Item.FindControl("lblClientTotalYards")).Text = Convert.ToString(ClientTotalYard);
        ((Label)e.Item.FindControl("lblClientTotalAmount")).Text = Convert.ToString(ClientTotalAmount);


    }

    protected void rptQuality_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Label lblQuality = (Label)e.Item.FindControl("lblQuality");
        lblQuality.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Quality"));

        Repeater rptMachine = (Repeater)e.Item.FindControl("rptMachine");
        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCar.ProductionDateFrom = Convert.ToDateTime(Request["DateFrom"]);
        objCar.ProductionDateTo = Convert.ToDateTime(Request["DateTo"]);
        objCar.ClientAcfileId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "ClientAcfileId"));
        objCar.MachineNo = Convert.ToInt32(Request["MachineNo"]);
        objCar.QualityId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "QualityId"));
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        ds = objCar.getEmbMachineForProduction();
        if (ds.Tables[0].Rows.Count > 0)
        {
            rptMachine.DataSource = ds.Tables[0];
            rptMachine.DataBind();
        }
    }
    protected void rptMachine_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Label lblMachine = (Label)e.Item.FindControl("lblMachine");
        lblMachine.Text = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "MachineNo"));

        DataGrid dgGallery = (DataGrid)e.Item.FindControl("dgGallery");
        objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCar.ProductionDateFrom = Convert.ToDateTime(Request["DateFrom"]);
        objCar.ProductionDateTo = Convert.ToDateTime(Request["DateTo"]);
        objCar.ClientAcfileId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "ClientAcfileId"));
        objCar.MachineNo = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "MachineNo"));
       
        objCar.QualityId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "QualityId"));
        DataTable dt = new DataTable();
        DataSet ds = new DataSet();
        if (Convert.ToString(Request["Type"]) == "Both")
        {
            ds = objCar.getEmbProductionValueReport();
        }
        if (Convert.ToString(Request["Type"]) == "Delivered")
        {
            ds = objCar.getEmbProductionValueReportDelivered();
        }
        if (Convert.ToString(Request["Type"]) == "Un Delivered")
        {
            ds = objCar.getEmbProductionValueUnDelivered();
        }

       
    
        if (ds.Tables[0].Rows.Count > 0)
        {
            TotalPcs = 0;
            TotalAmount = 0;
            TotalStitches = 0;
            dgGallery.DataSource = ds.Tables[0];
            dgGallery.DataBind();
            if (lblMachine.Text == "1")
            {
                Amount1+= TotalAmount;
                Stitches1 += TotalStitches;
            }
            if (lblMachine.Text == "2")
            {
                Amount2 += TotalAmount;
                Stitches2 += TotalStitches;
            }
            if (lblMachine.Text == "3")
            {
                Amount3 += TotalAmount;
                Stitches3 += TotalStitches;
            }
            if (lblMachine.Text == "4")
            {
                Amount4 += TotalAmount;
                Stitches4 += TotalStitches;
            }
            if (lblMachine.Text == "5")
            {
                Amount5 += TotalAmount;
                Stitches5 += TotalStitches;
            }
            if (lblMachine.Text == "6")
            {
                Amount6 += TotalAmount;
                Stitches6 += TotalStitches;
            }
            if (lblMachine.Text == "7")
            {
                Amount7 += TotalAmount;
                Stitches7 += TotalStitches;
            }
            if (lblMachine.Text == "8")
            {
                Amount8 += TotalAmount;
                Stitches8 += TotalStitches;
            }
            if (lblMachine.Text == "9")
            {
                Amount9 += TotalAmount;
                Stitches9 += TotalStitches;
            }
            if (lblMachine.Text == "10")
            {
                Amount10 += TotalAmount;
                Stitches10 += TotalStitches;
            }

        }


    }
    protected void dgGallery_RowDataBound(object sender, DataGridItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            objCar.DesignId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "DesignId"));
            objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataSet ds = new DataSet();
            ds = objCar.GetDesign();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ((Label)e.Item.FindControl("lblDesign")).Text = Convert.ToString(ds.Tables[0].Rows[0]["DesignNo"]);
               // ((Label)e.Item.FindControl("lblQuality")).Text = Convert.ToString(ds.Tables[0].Rows[0]["SubcategoryName"]);
                ((Label)e.Item.FindControl("lblStitches")).Text = Convert.ToString(ds.Tables[0].Rows[0]["Stitches"]);
                ((Label)e.Item.FindControl("lblTotalStitches")).Text = Convert.ToString(Convert.ToInt32(ds.Tables[0].Rows[0]["Stitches"]) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Pcs")));
                DataSet ds2 = new DataSet();
                objCar.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objCar.ProductionDateFrom = Convert.ToDateTime(Request["DateFrom"]);
                objCar.ProductionDateTo = Convert.ToDateTime(Request["DateTo"]);
                objCar.ClientAcfileId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "ClientAcfileId"));
                objCar.DesignId = Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "DesignId"));
                ds2 = objCar.GetEmbRateByDesignId();
                if (ds2.Tables[0].Rows.Count > 0)
                {

                    ((Label)e.Item.FindControl("lblYardRate")).Text = Convert.ToString(ds2.Tables[0].Rows[0]["YardRate"]);
                    ((Label)e.Item.FindControl("lblPcsRate")).Text = Convert.ToString(ds2.Tables[0].Rows[0]["PcRate"]);
                    ((Label)e.Item.FindControl("lblStitchesRate")).Text = Convert.ToString(ds2.Tables[0].Rows[0]["StitchRate"]);
                    
                    ((Label)e.Item.FindControl("lblAmount")).Text = (Convert.ToDecimal(ds2.Tables[0].Rows[0]["YardRate"]) * Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "yard"))).ToString("0.00");

                   
                    TotalAmount += Convert.ToDecimal((Convert.ToDecimal(ds2.Tables[0].Rows[0]["YardRate"]) * Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "yard"))).ToString("0.00"));
                 


                  
                    ClientTotalAmount += Convert.ToDecimal((Convert.ToDecimal(ds2.Tables[0].Rows[0]["YardRate"]) * Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "yard"))).ToString("0.00"));
                 



                    GrandTotalAmount += Convert.ToDecimal((Convert.ToDecimal(ds2.Tables[0].Rows[0]["YardRate"]) * Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "yard"))).ToString("0.00"));
                   
               
                
                }
                TotalStitches += Convert.ToDecimal((Convert.ToInt32(ds.Tables[0].Rows[0]["Stitches"]) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Pcs"))));
                TotalPcs += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Pcs"));
                TotalYard += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "yard"));
                ClientTotalPc += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Pcs"));
                ClientTotalYard += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "yard"));
                ClientTotalStitches += Convert.ToDecimal((Convert.ToInt32(ds.Tables[0].Rows[0]["Stitches"]) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Pcs"))));

                GrandTotalPc += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "Pcs"));
                GrandTotalYard += Convert.ToDecimal(DataBinder.Eval(e.Item.DataItem, "yard"));
                GrandTotalStitches += Convert.ToDecimal((Convert.ToInt32(ds.Tables[0].Rows[0]["Stitches"]) * Convert.ToInt32(DataBinder.Eval(e.Item.DataItem, "Pcs"))));

            }

        }

        if (e.Item.ItemType == ListItemType.Footer)
        {
            ((Label)e.Item.FindControl("lblMachineTotalStitches")).Text = Convert.ToString(TotalStitches);
            ((Label)e.Item.FindControl("lblMachineTotalPcs")).Text = Convert.ToString(TotalPcs);
            ((Label)e.Item.FindControl("lblMachineTotalyard")).Text = Convert.ToString(TotalYard);
            ((Label)e.Item.FindControl("lblMachineTotalAmount")).Text = Convert.ToString(TotalAmount);
        }
    }
}