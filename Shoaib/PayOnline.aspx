﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaticPages.master" AutoEventWireup="true"
    CodeFile="PayOnline.aspx.cs" Inherits="PayOnline" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        function isNumericPositive(obj) {
            if (obj.value != null && !obj.value.toString().match(/^[-]?\d*\.?\d*$/) || obj.value == "") {
                obj.value = "0.00";
                alert("Please Enter Numeric Value");

            }
            else {
                if (obj.value < 0) {
                    obj.value = "0.00";
                    alert("Please Enter Valid Value");
                }
                else {
                    Getamount();
                }
            }
        }
        function Getamount() {
            var CustomizeAmt = document.getElementById('<%=txtCustomizeAmt.ClientID %>').value;
            var Otheramount = document.getElementById('<%=txtOtheramount.ClientID %>').value;
            var BasicPackagePrice = document.getElementById('<%=txtBasicPackagePrice.ClientID %>').value;
            var e = document.getElementById('<%=ddlMonth.ClientID %>');
            var Month = e.options[e.selectedIndex].value;
            var TotalAmount = (parseFloat(BasicPackagePrice) * parseFloat(Month)) + (parseFloat(CustomizeAmt) + parseFloat(Otheramount));
            document.getElementById('<%=txtTotal.ClientID %>').value = TotalAmount;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="scriptmanager" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <div id="content_wraper5">
                <div id="content_midd5">
                    <div class="acc-features-middle-bg">
                        <div class="top-heading">
                            <h1>
                                Payment Online</h1>
                        </div>
                    </div>
                    <div class="accmiddle-features-bg-2" style="min-height: 520px">
                        <div class="features-wrapper">
                            <div class="features-box">
                                <div style="float: left">
                                    <div class="features-left-link-box">
                                        <div class="left-link-Content">
                                            <h2 style="text-decoration: underline; font-size: 18px;">
                                                AccMaster</h2>
                                            <br />
                                            <ul style="margin-left: 20px">
                                                <li><a href="index.aspx">Home</a></li>
                                                <li><a href="features.aspx">Features</a></li>
                                                <li><a href="pricing.aspx">Product & Pricing</a></li>
                                                <li><a href="PayOnline.aspx">Pay Online</a></li>
                                                <li><a href="Privacy.aspx">Privacy</a></li>
                                                <li><a href="Terms.aspx">Terms</a></li>
                                                <li><a href="ContactUs.aspx">Contact us</a></li>
                                                <li><a href="AboutUs.aspx">About Us</a></li>
                                                <li><a href="Careers.aspx">Careers</a></li>
                                            </ul>
                                        </div>
                                        <div class="left-link-bottom">
                                            <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                                        </div>
                                    </div>
                                    <div class="features-left-link-box" style="clear: both; margin-top: 20px;">
                                        <div class="left-link-Content" style="min-height: 100px; line-height: 0.6">
                                            <asp:Literal ID="ltrlAddress" runat="server"></asp:Literal>
                                        </div>
                                        <div class="left-link-bottom">
                                            <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="feat-right-part-box">
                                    <div class="feta-right-part-content">
                                        <div class="feat-right-box-dtls">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td align="left" valign="top">
                                                        <table width="400" border="0" cellspacing="0" cellpadding="0" style="padding-left: 10px;">
                                                            <tr>
                                                                <td align="left" valign="top">
                                                                    <table style="padding: 0 0 0 10px;" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td style="padding-right: 10px">
                                                                                <strong>Customer Code</strong>:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtCustomerCode" runat="server" Style="background: none; border: solid 1px #6b5645;
                                                                                    height: 20px; color: Black; width: 200px;"></asp:TextBox>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <asp:RequiredFieldValidator ID="rfvname" runat="server" ValidationGroup="Respond"
                                                                                    ErrorMessage="Please Enter Customer Code " ControlToValidate="txtCustomerCode"
                                                                                    Display="None"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-right: 10px">
                                                                                <strong>Name</strong>:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtName" runat="server" Style="background: none; border: solid 1px #6b5645;
                                                                                    height: 20px; color: Black; width: 200px;"></asp:TextBox>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ValidationGroup="Respond"
                                                                                    ErrorMessage="Please Enter Name " ControlToValidate="txtName" Display="None"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-right: 10px">
                                                                                <strong>Address</strong>:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtAddress" runat="server" Style="background: none; border: solid 1px #6b5645;
                                                                                    height: 20px; color: Black; width: 200px;"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-right: 10px">
                                                                                <strong>City</strong>:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtCity" runat="server" Style="background: none; border: solid 1px #6b5645;
                                                                                    height: 20px; color: Black; width: 200px;"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-right: 10px">
                                                                                <strong>Country</strong>:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtCountry" runat="server" Style="background: none; border: solid 1px #6b5645;
                                                                                    height: 20px; color: Black; width: 200px;"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-right: 10px">
                                                                                <strong>Email</strong>:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtEmail" runat="server" Style="background: none; border: solid 1px #6b5645;
                                                                                    height: 20px; color: Black; width: 200px;"></asp:TextBox>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="Respond"
                                                                                    ErrorMessage="Please Enter Email " ControlToValidate="txtEmail" Display="None"></asp:RequiredFieldValidator>
                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                                                                                    Display="None" ErrorMessage="Please Enter Valid  Email." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                                    ValidationGroup="Respond"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-right: 10px">
                                                                                <strong>Contact Number</strong>:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtContactNumber" runat="server" Style="background: none; border: solid 1px #6b5645;
                                                                                    height: 20px; color: Black; width: 200px;"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-right: 10px">
                                                                                <strong>Transaction Desc</strong>:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtTransaction" runat="server" Style="background: none; border: solid 1px #6b5645;
                                                                                    height: 20px; color: Black; width: 200px;"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-right: 10px">
                                                                                <strong>Product</strong>:
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlPorduct" runat="server" OnSelectedIndexChanged="ddlPorduct_SelectedIndexChanged"
                                                                                    AutoPostBack="true">
                                                                                    <asp:ListItem Text="Other" Value="0"></asp:ListItem>
                                                                                    <asp:ListItem Text="Online AccMaster Simple" Value="1"></asp:ListItem>
                                                                                    <asp:ListItem Text="Online AccMaster Basic" Value="2"></asp:ListItem>
                                                                                    <asp:ListItem Text="Online AccMaster Extra" Value="3"></asp:ListItem>
                                                                                    <asp:ListItem Text="Online AccMaster Plus Plus" Value="4"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-right: 10px">
                                                                                <strong>Customize Amount</strong>:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtCustomizeAmt" Text="0.00" runat="server" onChange="isNumericPositive(this); "
                                                                                    Style="background: none; border: solid 1px #6b5645; height: 20px; color: Black;
                                                                                    width: 200px;"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-right: 10px">
                                                                                <strong>Other Amount </strong>:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtOtheramount" Text="0.00" runat="server" onChange="isNumericPositive(this);"
                                                                                    Style="background: none; border: solid 1px #6b5645; height: 20px; color: Black;
                                                                                    width: 200px;"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-right: 10px">
                                                                                <strong>Basic Package Price</strong>:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtBasicPackagePrice" Text="0.00" runat="server" onChange="isNumericPositive(this)"
                                                                                    Style="background: none; border: solid 1px #6b5645; height: 20px; color: Black;
                                                                                    width: 200px;"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-right: 10px">
                                                                                <strong>No. of Months</strong>:
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlMonth" onChange="Getamount()" runat="server">
                                                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="padding-right: 10px">
                                                                                <strong>Total Amount</strong>:
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtTotal" runat="server" Text="0.00" onChange="isNumericPositive(this)"
                                                                                    Style="background: none; border: solid 1px #6b5645; height: 20px; color: Black;
                                                                                    width: 200px;"></asp:TextBox>
                                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="Respond"
                                                                                    ErrorMessage="Please Enter Total Amount" ControlToValidate="txtTotal" Display="None"></asp:RequiredFieldValidator>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="height: 15px">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td rowspan="3" align="left" style="padding-left: 10px;">
                                                                    &nbsp;
                                                                </td>
                                                                <tr>
                                                                    <td>
                                                                        <table border="0" cellpadding="0" cellspacing="0" style="padding: 0 0 0 0px;" width="400px">
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="button" style="padding-top: 10px;">
                                                                                        <ul>
                                                                                            <li>
                                                                                                <asp:LinkButton ID="btnResponse" ValidationGroup="Respond" Text="Submit" runat="server"
                                                                                                    OnClick="btnResponse_Click"></asp:LinkButton></li>
                                                                                        </ul>
                                                                                        <br />
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    2CheckOut.com Inc. (Ohio, USA) is an authorized retailer for goods and services
                                                                                    provided by System Development Services.
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <asp:ValidationSummary ID="vs1" ShowSummary="false" runat="server" ValidationGroup="Respond"
                                                                    ShowMessageBox="true" />
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td style="vertical-align: top; width: 250px; text-align: right; padding-right: 0px;
                                                        border: solid 0px red; margin-right: 0px">
                                                        <img src="images/payonline.png" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="feat-right-part-bottom">
                                        <img src="images/feat-right-part-bottom-bg.jpg" alt="" />
                                    </div>
                                </div>
                            </div>
                        
                    
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
