﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class PrintBoutiquePriceList : System.Web.UI.Page
{
    #region properties
    SalesInvoices objSalesInvoice = new SalesInvoices();
    clsCookie ck = new clsCookie();
    decimal TotalOpbal = 0;
    decimal TotalTransections = 0;
    decimal TotalOpeningQuantity = 0;
    decimal TotalPurchaseRate = 0;
    decimal TotalProduction = 0;
    decimal TotalSalesReturn = 0;
    decimal TotalPurchaseRateAdjustment = 0;
    decimal AllTotalStockReceived = 0;
    decimal TotalSales = 0;
    decimal TotalConsumption = 0;
    decimal TotalPurchaseRateReturn = 0;
    decimal TotalSalesAdjustment = 0;
    decimal AllTotalStockSale = 0;
    decimal TotalClosingStock = 0;
    decimal TotalAmount = 0;
    decimal AllTotalQuantity = 0;
    decimal AllTotalOpbal = 0;
    decimal AllTotalTransections = 0;
    decimal AllTotalOpeningQuantity = 0;
    decimal AllTotalPurchaseRate = 0;
    decimal AllTotalProduction = 0;
    decimal AllTotalSalesReturn = 0;
    decimal AllTotalPurchaseRateAdjustment = 0;
    decimal CompleteTotalStockReceived = 0;
    decimal AllTotalSales = 0;
    decimal AllTotalConsumption = 0;
    decimal AllTotalPurchaseRateReturn = 0;
    decimal AllTotalSalesAdjustment = 0;
    decimal CompleteTotalStockSale = 0;
    decimal AllTotalClosingStock = 0;
    decimal AllTotalAmount = 0;

    decimal GroupTotalOpeningQuantity = 0;
    decimal GroupTotalPurchaseRate = 0;
    decimal GroupTotalAmount = 0;
    Reports objReports = new Reports();
    SetCustomers objCust = new SetCustomers();
    DataTable dttbl = new DataTable();
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

         UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }
        if (!IsPostBack)
        {
            Session["s"] = null;
            bindSupplier();
            BindMaincategory();
            FillLogo();
            lblprintdate.Text = ClsGetDate.FillFromDate(System.DateTime.Now.ToString());
            lblUser.Text = Sessions.UseLoginName.ToString();
            //objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            //objCust.MainCategoryCode = Convert.ToInt32(Request["MainCategoryCode"]);
            //if (Convert.ToInt32(Convert.ToInt32(Request["MainCategoryCode"])) > 0)
            //{
            //    objCust.SubCategoryCode = Convert.ToInt32(Request["SubCategoryCode"]);
            //}
            //DataTable dt = new DataTable();
            //dt = objCust.getItemsSearch();
            //dgGallery.DataSource = dt;
            //dgGallery.DataBind();
        }

    }
    #region Fill Logo
    private void FillLogo()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }

        if (Convert.ToBoolean(ds.Tables[0].Rows[0]["IsHeader"]) == false)
        {
            trTrialBal.Style.Add("display", "none");
        }
    }
    #endregion
    protected void bindSupplier()
    {
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        DataTable dtSupplier = new DataTable();
        dtSupplier = objSalesInvoice.GetCustomerDetails();


        if (dtSupplier.Rows.Count > 0)
        {
            lblSuplierName.Text = Convert.ToString(dtSupplier.Rows[0]["CustomerName"]);
            lblSupplierAddress.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine1"]);
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]) != "")
            {
                lblSupplierAddress2.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine2"]);
            }
            else
            {
                spAddress2.Style.Add("display", "none");
            }
            if (Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]) != "")
            {
                lblSupplierAddress3.Text = Convert.ToString(dtSupplier.Rows[0]["AddressLine3"]);
            }
            else
            {
                spAddress3.Style.Add("display", "none");
            }
            lblCity.Text = Convert.ToString(dtSupplier.Rows[0]["City"]);
            lblCountry.Text = Convert.ToString(dtSupplier.Rows[0]["Country"]);
            lblPhoneNumber.Text = Convert.ToString(dtSupplier.Rows[0]["PhoneNumber"]);

        }
    }

    protected void BindMaincategory()
    {
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.MainCategoryCode = Convert.ToInt32(Request["MainCategoryCode"]);
        DataSet ds = new DataSet();
        ds = objCust.GetMainCategory();
        if (ds.Tables[0].Rows.Count > 0)
        {

            grdMainCategory.DataSource = ds.Tables[0];
            grdMainCategory.DataBind();
        }
    }


    #region Grid RowDataBound
    protected void grdMainCategory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnMainCategoryCode = ((HiddenField)e.Row.FindControl("hdnMainCategoryCode"));



            objCust.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objCust.MainCategoryCode = Convert.ToInt32(hdnMainCategoryCode.Value);
            objCust.SubCategoryCode = Convert.ToInt32(Request["SubCategoryCode"]);           
            DataSet ds = new DataSet();
            DataTable dt = objCust.getSearchSubcategory();

            GridView grdSubCategory = (GridView)e.Row.FindControl("grdSubCategory");
            if (dt.Rows.Count > 0)
            {
                //GroupTotalOpeningQuantity = 0;
                //GroupTotalPurchaseRate = 0;
                //GroupTotalAmount = 0;

                grdSubCategory.DataSource = dt;
                grdSubCategory.DataBind();


                //if (GroupTotalOpeningQuantity == 0 && Convert.ToString(Request["Type"]) == "2")
                //{
                //    GridViewRow Parent = (GridViewRow)grdSubCategory.Parent.Parent;
                //    Parent.Style.Add("display", "none");
                //}
            }

        }


    }

    protected void grdSubCategory_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnMainCategoryCode2 = ((HiddenField)e.Row.FindControl("hdnMainCategoryCode2"));
            HiddenField hdnSubCategoryCode = ((HiddenField)e.Row.FindControl("hdnSubCategoryCode"));

            //objReports.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            //objReports.AsOn = Convert.ToDateTime(Request["AsOn"]);
            //objReports.MainCategoryCode = Convert.ToInt32(hdnMainCategoryCode2.Value);
            //objReports.SubCategoryCode = Convert.ToInt32(hdnSubCategoryCode.Value);

            objCust.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objCust.MainCategoryCode = Convert.ToInt32(hdnMainCategoryCode2.Value);
            objCust.SubCategoryCode = Convert.ToInt32(hdnSubCategoryCode.Value);
            DataSet ds = new DataSet();
            DataTable dt = objCust.getItemsSearch();


            GridView dgGallery = (GridView)e.Row.FindControl("dgGallery");
            if (dt.Rows.Count > 0)
            {
                

                //DataTable DtPurchase = new DataTable();
                //DtPurchase = ds.Tables[0];

                //DataView dv = new DataView(DtPurchase);



                //if (Convert.ToString(Request["PurchaseType"]) == "ContractPurchase")
                //{
                //    dv.RowFilter = "PurchaseType='Contract Purchase'";


                //}
                //if (Convert.ToString(Request["PurchaseType"]) == "DirectPurchase")
                //{
                //    dv.RowFilter = "PurchaseType='Direct Purchase'";

                //}
                //DtPurchase = dv.ToTable();
                //if (DtPurchase.Rows.Count > 0)
                //{
                dgGallery.DataSource = dt;
                dgGallery.DataBind();
                //}

                //if (TotalOpeningQuantity == 0 && Convert.ToString(Request["Type"]) == "2")
                //{
                //    GridViewRow Parent = (GridViewRow)grdLedger.Parent.Parent;
                //    Parent.Style.Add("display", "none");
                //}
            }

        }

        if (e.Row.RowType == DataControlRowType.Footer)  // For footer totals
        {


        }
    }
    #endregion
}