﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrintYarnSalesBill.aspx.cs"
    Inherits="PrintYarnSalesBill" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>AccMaster</title>
    <link href="css/stylesheet.css" rel="stylesheet" type="text/css" />
</head>
<body style="font-family: Verdana; background: none; font-size: 10px;">
    <form id="form1" runat="server">
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 14px; border-bottom: solid 1px black;
            font-weight: bold;">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <b>Supplier Name</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblSuplierName" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top">
                                <b>Address</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblSupplierAddress" runat="server"></asp:Label><br />
                                <span id="spAddress2" runat="server">
                                    <asp:Label ID="lblSupplierAddress2" runat="server"></asp:Label><br />
                                </span><span id="spAddress3" runat="server">
                                    <asp:Label ID="lblSupplierAddress3" runat="server"></asp:Label><br />
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Telephone No.</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblSupplierTelephone" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>ST Reg. No.</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblSellerRegNo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>NTN No.</b>
                            </td>
                            <td style="padding-left: 15px">
                                <asp:Label ID="lblSellerNTN" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="text-align: right; vertical-align: top">
                    <asp:Image ID="imgLogo" Style="vertical-align: top;" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2" style="text-align: center">
                    <span style="font-size: 24px; font-weight: normal; text-decoration: underline">
                        Yarn Brokerage Sale Bill<br />
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
            width="100%">
            <tr>
                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 18%">
                    <b>Transaction Date :</b>
                </td>
                <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                    <table>
                        <tr>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                <asp:Label ID="lblSaleDate" runat="server"></asp:Label>
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 18%">
                    <b>Sale Cr Days :</b>
                </td>
                <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                    <table>
                        <tr>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                <asp:Label ID="lblSaleCrDays" runat="server"></asp:Label>
                            </td>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 18%">
                    <b>Sold To :</b>
                </td>
                <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                    <asp:Label ID="lblSoldTo" runat="server"></asp:Label>
                </td>
            </tr>
            <%--   <tr>
                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 18%">
                   <b>  Purchased from :</b>
                </td>
                <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                    <asp:Label ID="lblPurchasedFrom" runat="server"></asp:Label>
                </td>
            </tr>--%>
            <%--   <tr>
                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 18%">
                   <b>  Profit &amp; Loss A/c :</b>
                </td>
                <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                    <asp:Label ID="lblPL" runat="server"></asp:Label>
                </td>
            </tr>--%>
            <tr>
                <td align="left" valign="middle" style="padding: 5px 2px 2px 8px; width: 18%">
                    <b>Item :</b>
                </td>
                <td align="left" valign="top" style="padding: 5px 2px 2px 5px; text-align: left;">
                    <asp:Label ID="lblItem" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 18%">
                    <b>D.O.NO. :</b>
                </td>
                <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                    <table style="width: 100%; border: solid 0px red;">
                        <tr>
                            <td align="left" valign="top" style="width: 30%">
                                <asp:Label ID="lblDoNo" runat="server"></asp:Label>
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 30px; width: 115px">
                                <b>Delivered To:</b>
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                <asp:Label ID="lblDeliveredTo" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 18%">
                    <b>Cartons :</b>
                </td>
                <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                    <table style="width: 100%; border: solid 0px red;">
                        <tr>
                            <td align="left" valign="top" style="width: 30%">
                                <asp:Label ID="lblCartons" runat="server"></asp:Label>
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 30px; width: 115px">
                                <b>Cops Amount :</b>
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                <asp:Label ID="lblCopsamount" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 18%">
                    <b>Kgs:</b>
                </td>
                <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                    <table style="width: 100%; border: solid 0px red;">
                        <tr>
                            <td align="left" valign="top" style="width: 30%">
                                <asp:Label ID="lblKgs" runat="server"></asp:Label>
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 30px; width: 115px">
                                <b>Lbs:</b>
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                                <asp:Label ID="lblLbs" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 18%">
                    <b>Sale Rate :</b>
                </td>
                <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                    <table style="width: 100%; border: solid 0px red;">
                        <tr>
                            <td align="left" valign="top" style="width: 30%">
                                <asp:Label ID="lblSaleRate" runat="server"></asp:Label>
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 30px; width: 115px">
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 18%">
                    <b>%age on Sales :</b>
                </td>
                <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                    <table style="width: 100%; border: solid 0px red;">
                        <tr>
                            <td align="left" valign="top" style="width: 30%">
                                <asp:Label ID="lblAgeSale" runat="server"></asp:Label>
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 30px; width: 115px">
                               
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 0px;">
                               
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 18%">
                   <b>Sale Amount :</b>
                </td>
                <td align="left" valign="top" style="padding: 2px 2px 2px 2px; text-align: left;">
                    <table style="width: 100%; border: solid 0px red;">
                        <tr>
                            <td align="left" valign="top" style="width: 30%">
                                 <asp:Label ID="lblSaleAmount" runat="server"></asp:Label>
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 30px; width: 110px">
                             
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                              
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        <%--    <tr>
                <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 18%">
                    <b>Profit/Loss Amount :</b>
                </td>
                <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                    <asp:Label ID="lblPlAmount" runat="server"></asp:Label>
                </td>
            </tr>--%>
            <tr>
                <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 18%">
                    <b>Description :</b>
                </td>
                <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left;">
                    <asp:Label ID="lblDescription" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    </div>
    <div style="text-align: left; width: 800px; float: left">
        <table style="width: 800px; float: left; font-size: 10px;">
            <tr>
                <td>
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td style="margin-top: 20px; width: 500px;">
                    <div style="float: left">
                        <b>Received By :</b></div>
                    <div style="float: left; margin-left: 20px; height: 20px; padding-top: 10px; width: 200px;">
                        <hr />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="width: 100%">
                        <tr>
                            <td style="margin-top: 20px; width: 50%;">
                                <div style="float: left">
                                    <b>Dated :</b></div>
                                <div style="float: left; margin-left: 50px; padding-top: 10px; height: 20px; width: 200px;">
                                    <hr />
                                </div>
                            </td>
                            <td style="margin-top: 20px; text-align: right">
                                <div style="float: right; margin-left: 20px; padding-top: 10px; height: 20px; line-height: 3px;
                                    text-align: center; width: 200px;">
                                    <hr />
                                    <b>Signature</b>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
           <tr>
                <td style="text-align: right">
                    <asp:LinkButton Text="Print" ID="lnkPrint" OnClientClick="window.print();" runat="server"></asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td style="text-align: center">
                    <hr />
                    <table style="width: 800px">
                        <tr>
                            <td style="text-align: left">
                                Print Date :
                                <asp:Label ID="lblprintdate" runat="server"></asp:Label>
                                <script type="text/javascript">

                                    var currentTime = new Date()
                                    var hours = currentTime.getHours()
                                    var Newhours = 0;
                                    var minutes = currentTime.getMinutes()
                                    if (minutes < 10) {
                                        minutes = "0" + minutes
                                    }
                                    if (hours > 12) {
                                        Newhours = hours - 12;
                                    }
                                    else {
                                        Newhours = hours;
                                    }
                                    document.write(Newhours + ":" + minutes + " ")
                                    if (hours > 11) {
                                        document.write("PM")
                                    } else {
                                        document.write("AM")
                                    }

                                </script>
                            </td>
                            <%-- <td style="text-align: center">
                                Print Time:<asp:Label ID="lblPrintTime" runat="server"></asp:Label>
                            </td>--%>
                            <td style="text-align: right">
                                User :
                                <asp:Label ID="lblUser" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    Software Developed & Designed by “System Development Services”, <a href="mailto:sales@accmaster.com">
                        sales@accmaster.com</a>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
