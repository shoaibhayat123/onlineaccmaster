﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaticPages.master" AutoEventWireup="true"
    CodeFile="Privacy.aspx.cs" Inherits="Privacy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content_wraper5">
        <div id="content_midd5">
            <div class="acc-features-middle-bg">
                <div class="top-heading">
                    <h1>
                        Privacy</h1>
                </div>
            </div>
            <div class="accmiddle-features-bg-2" style="min-height: 520px">
                <div class="features-wrapper">
                    <div class="features-box">
                         <div style=" float:left">
                        <div class="features-left-link-box">
                            <div class="left-link-Content">
                                <h2 style=" text-decoration:underline; font-size:18px;">
                                  AccMaster</h2><br />
                                <ul style=" margin-left:20px">
                                    <li><a href="index.aspx">Home</a></li>
                                    <li><a href="features.aspx">Features</a></li>
                                    <li><a href="pricing.aspx">Product & Pricing</a></li>
                                    <li><a href="PayOnline.aspx">Pay Online</a></li>
                                    <li><a href="Privacy.aspx">Privacy</a></li>
                                    <li><a href="Terms.aspx">Terms</a></li>
                                    <li><a href="ContactUs.aspx">Contact us</a></li>
                                    <li><a href="AboutUs.aspx">About Us</a></li>
                                    <li><a href="Careers.aspx">Careers</a></li>
                                </ul>
                            </div>
                            <div class="left-link-bottom">
                                <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                            </div>
                        </div>
                         <div class="features-left-link-box" style=" clear:both; margin-top:20px;">
                            <div class="left-link-Content" style=" min-height:100px;line-height:0.6">
                                   
                                    <asp:Literal ID="ltrlAddress" runat="server"></asp:Literal>
                                </div>
                            <div class="left-link-bottom">
                                <img src="images/features-left-link-bottom-bg.jpg" width="238" height="9" alt="" />
                            </div>
                        </div>
                        </div>
                        <div class="feat-right-part-box">
                            <div class="feta-right-part-content">
                                <div class="feat-right-box-dtls">
                                    <div class="ft-right-box-details">
                                        <h1>
                                            AccMaster Privacy Highlights</h1>
                                        <h4>
                                            <strong>We will not,</strong>without your permission, sell,
                                            <br />
                                            publish or share information you entrust to us
                                            <br />
                                            that identifies you or any person.
                                        </h4>
                                    </div>
                                    <div class="what-type">
                                        <div class="what-type-image">
                                            <img src="images/What-Type-image.jpg" alt="" />
                                        </div>
                                        <div class="what-type-content">
                                            <h2>
                                                What Type of Information We Collect</h2>
                                            <ul>
                                                <li>Personal information such as your name, shipping/billing address,<br />
                                                    e-mail, phone, and credit card number when you interact with us<br />
                                                    for sales, service, support, registration, and payment. </li>
                                                <li>Your username and password to allow access to our products and services.</li>
                                                <li>User feedback, community discussions, chats and other interactions at our sites.</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="what-type">
                                        <div class="what-type-image">
                                            <img src="images/What-We-Do-image.jpg" alt="" />
                                        </div>
                                        <div class="what-type-content">
                                            <h2>
                                                What We Do With Your Information</h2>
                                            <ul>
                                                <li>Tell you about the products and services that are available to you. </li>
                                                <li>Give you choices about our use of information that identifies you.</li>
                                                <li>Give open and clear explanations about how we use information. </li>
                                                <li>Publish or share combined information from many users, but only in a way that would
                                                    not<br />
                                                    allow you or any person to be identified. </li>
                                                <li>Train our employees about how to keep information safe and secure, and educate you<br />
                                                    about how to keep your information safe and secure. </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="what-type">
                                        <div class="what-type-image">
                                            <img src="images/Security-of-image.jpg" alt="" />
                                        </div>
                                        <div class="what-type-content">
                                            <h2>
                                                Security of your Information</h2>
                                            <ul>
                                                <li>We use accepted best methods to protect your information. We review our security<br />
                                                    procedures carefully. </li>
                                                <li>We comply with applicable laws and security standards.</li>
                                                <li>Your sensitive information, such as credit card information, is sent securely.</li>
                                                <li>Our staff is trained and required to safeguard your information. </li>
                                                <li>We transmit, store, protect and access all cardholder information in compliance
                                                    with the
                                                    <br />
                                                    Payment Card Industry's Data Security Standards. </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="what-type">
                                        <div class="what-type-content-2">
                                            <h2>
                                                This Web site is privacy certified by Rapid SSL Privacy Seal</h2>
                                            <p>
                                                The SSL Checker makes it easy to verify your SSL certificates by connecting to your
                                                server and displaying the results of the SSL connection including what SSL certificate
                                                is installed and whether it gives out the correct intermediate certificates. The
                                                SSL Checker even lets you set up a reminder of a certificate's expiration so you
                                                don't forget to renew your certificate on time and avoid embarrassing error messages.
                                                SSL Checker entries may be cached up to a day after repeated checking to conserve
                                                server resources. To check our SSL certificate visit<br /><br />
                                                <a href="http://www.sslshopper.com/ssl-checker.html#hostname=www.online.accmaster.com" target="_blank">http://www.sslshopper.com/ssl-checker.html#hostname=www.online.accmaster.com</a>
                                            </p>
                                        </div>
                                        <div class="what-type-trust-image">
                                            <img src="images/certified-by-Rapid-SSL-image-2.jpg" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="feat-right-part-bottom">
                                <img src="images/feat-right-part-bottom-bg.jpg" alt="" />
                            </div>
                        </div>
</asp:Content>
