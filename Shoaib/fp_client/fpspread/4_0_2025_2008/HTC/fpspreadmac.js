//
//
//	Copyright?2005. FarPoint Technologies.	All rights reserved.
//
var the_fpSpread = new Fpoint_FPSpread();
function FpSpread_EventHandlers(){
var f2=the_fpSpread;
this.TranslateKey=function (event){
f2.TranslateKey(event);
}
this.SetActiveSpread=function (event){
f2.SetActiveSpread(event);
}
this.MouseDown=function (event){
f2.MouseDown(event);
}
this.MouseUp=function (event){
f2.MouseUp(event);
}
this.MouseMove=function (event){
f2.MouseMove(event);
}
this.DblClick=function (event){
f2.DblClick(event);
}
this.HandleFirstKey=function (event){
f2.HandleFirstKey(event);
}
this.DoPropertyChange=function (event){
f2.DoPropertyChange(event);
}
this.CmdbarMouseOver=function (event){
f2.CmdbarMouseOver(event);
}
this.CmdbarMouseOut=function (event){
f2.CmdbarMouseOut(event);
}
this.ScrollViewport=function (event){
f2.ScrollViewport(event);
}
this.Focus=function (event){
var f3=event.target;
f2.Focus(f3);
}
this.docLoad=function (event){
if (f2.spreads==null||f2.spreads.length==0)return ;
var f4=f2.spreads.length;
for (var f5=0;f5<f4;f5++){
if (f2.spreads[f5]==f3)return ;
var f3=f2.spreads[f5];
var f6=f2.GetRowCount(f3);
if (f6>0){
for (var f7=0;f7<f6;f7++){
var f8=f2.GetRowHeightInternal(f3,f7);
f2.SetRowHeight2(f3,f7,f8);
}
}
}
}
this.pageLoad=function (){
var f4=f2.spreads.length;
for (var f5=0;f5<f4;f5++){
var f3=f2.spreads[f5];
if (f3.parentNode==null){
var f9=document.getElementById(f3.id);
if (f9!=null){
f2.spreads[f5]=f9;
f2.Init(f9,"pageLoad");
}
}
}
}
f2.AttachEvent(document,"keydown",this.TranslateKey,true);
f2.AttachEvent(document,"mousedown",this.SetActiveSpread,false);
f2.AttachEvent(document,"mouseup",this.MouseUp,false);
f2.AttachEvent(document,"keyup",this.HandleFirstKey,true);
f2.AttachEvent(window,"resize",f2.DoResize,false);
f2.AttachEvent(window,"load",this.docLoad,false);
if (typeof(Sys)!="undefined")
Sys.Application.add_load(this.pageLoad);
this.AttachEvents=function (f3){
f2.AttachEvent(f3,"mousedown",this.MouseDown,false);
f2.AttachEvent(f3,"mouseup",this.MouseUp,false);
f2.AttachEvent(f3,"mousemove",this.MouseMove,false);
f2.AttachEvent(f3,"dblclick",this.DblClick,false);
f2.AttachEvent(f3,"focus",this.Focus,false);
var g0=f2.GetViewport(f3);
if (g0!=null){
f2.AttachEvent(f2.GetViewport(f3).parentNode,"DOMAttrModified",this.DoPropertyChange,true);
f2.AttachEvent(f2.GetViewport(f3).parentNode,"scroll",this.ScrollViewport);
}
var g1=f2.GetCommandBar(f3);
if (g1!=null){
f2.AttachEvent(g1,"mouseover",this.CmdbarMouseOver,false);
f2.AttachEvent(g1,"mouseout",this.CmdbarMouseOut,false);
}
}
this.DetachEvents=function (f3){
f2.DetachEvent(f3,"mousedown",this.MouseDown,false);
f2.DetachEvent(f3,"mouseup",this.MouseUp,false);
f2.DetachEvent(f3,"mousemove",this.MouseMove,false);
f2.DetachEvent(f3,"dblclick",this.DblClick,false);
f2.DetachEvent(f3,"focus",this.Focus,false);
var g0=f2.GetViewport(f3);
if (g0!=null){
f2.DetachEvent(f2.GetViewport(f3).parentNode,"DOMAttrModified",this.DoPropertyChange,true);
f2.DetachEvent(f2.GetViewport(f3).parentNode,"scroll",this.ScrollViewport);
}
var g1=f2.GetCommandBar(f3);
if (g1!=null){
f2.DetachEvent(g1,"mouseover",this.CmdbarMouseOver,false);
f2.DetachEvent(g1,"mouseout",this.CmdbarMouseOut,false);
}
}
}
function Fpoint_FPSpread(){
this.working=false;
this.editing=false;
this.b7=null;
this.b8=null;
this.b9=null;
this.renderAsEditor=-1;
this.validationMsg=null;
this.c2=null;
this.c3=null;
this.c4=null;
this.c5=-1;
this.c6=-1;
this.activeElement=null;
this.c8=null;
this.c9=new Array();
this.error=false;
this.left=37;
this.right=39;
this.up=38;
this.down=40;
this.tab=9;
this.enter=13;
this.shift=16;
this.space=32;
this.altkey=18;
this.home=36;
this.end=35;
this.pup=33;
this.pdn=34;
this.backspace=8;
this.InitFields=function (f3){
if (this.c2==null)
this.c2=new this.Margin();
f3.d7=null;
f3.groupBar=null;
f3.d8=null;
f3.d9=null;
f3.e0=null;
f3.e1=null;
f3.e2=null;
f3.e3=null;
f3.e4=null;
f3.e5=null;
f3.e6="";
f3.oldScrollLeft=0;
f3.oldScrollTop=0;
f3.e7=null;
f3.f1=false;
f3.slideLeft=0;
f3.slideRight=0;
f3.setAttribute("rowCount",0);
f3.setAttribute("colCount",0);
f3.e8=new Array();
f3.e9=new Array();
f3.f0=new Array();
f3.footerSpanCells=new Array();
this.activePager=null;
this.dragSlideBar=false;
f3.allowColMove=(f3.getAttribute("colMove")=="true");
f3.allowGroup=(f3.getAttribute("allowGroup")=="true");
f3.selectedCols=[];
f3.msgList=new Array();
f3.mouseY=null;
f3.bSuspendLayout=null;
}
this.RegisterSpread=function (f3){
var g2=this.GetTopSpread(f3);
if (g2!=f3)return ;
if (this.spreads==null){
this.spreads=new Array();
}
var f4=this.spreads.length;
for (var f5=0;f5<f4;f5++){
if (this.spreads[f5]==f3)return ;
}
this.spreads.length=f4+1;
this.spreads[f4]=f3;
}
this.Init=function (f3,cmd){
if (f3==null)alert("spread is not defined!");
f3.initialized=false;
this.validationMsg=null;
this.c9=new Array();
this.RegisterSpread(f3);
this.InitFields(f3);
this.InitMethods(f3);
f3.d0=document.getElementById(f3.id+"_XMLDATA");
if (f3.d0==null){
f3.d0=document.createElement("XML");
f3.d0.id=f3.id+"_XMLDATA";
f3.d0.style.display="none";
document.body.insertBefore(f3.d0,null);
}
var g3=document.getElementById(f3.id+"_data");
if (g3!=null&&g3.getAttribute("data")!=null){
f3.d0.innerHTML=g3.getAttribute("data");
g3.value="";
}
this.SaveData(f3);
f3.d1=document.getElementById(f3.id+"_viewport");
if (f3.d1!=null){
f3.d2=f3.d1.parentNode;
}
f3.d3=document.getElementById(f3.id+"_corner");
if (f3.d3!=null&&f3.d3.childNodes.length>0){
f3.d3=f3.d3.getElementsByTagName("TABLE")[0];
}
f3.d4=document.getElementById(f3.id+"_rowHeader");
if (f3.d4!=null)f3.d4=f3.d4.getElementsByTagName("TABLE")[0];
f3.d5=document.getElementById(f3.id+"_colHeader");
if (f3.d5!=null)f3.d5=f3.d5.getElementsByTagName("TABLE")[0];
f3.colFooter=document.getElementById(f3.id+"_colFooter");
if (f3.colFooter!=null)f3.colFooter=f3.colFooter.getElementsByTagName("TABLE")[0];
f3.footerCorner=document.getElementById(f3.id+"_footerCorner");
if (f3.footerCorner!=null&&f3.footerCorner.childNodes.length>0){
f3.footerCorner=f3.footerCorner.getElementsByTagName("TABLE")[0];
}
var d6=f3.d6=document.getElementById(f3.id+"_commandBar");
var g4=this.GetViewport(f3);
if (g4!=null){
f3.setAttribute("rowCount",g4.rows.length);
if (g4.rows.length==1)f3.setAttribute("rowCount",0);
f3.setAttribute("colCount",g4.getAttribute("cols"));
}
var e8=f3.e8;
var f0=f3.f0;
var e9=f3.e9;
var g5=f3.footerSpanCells;
this.InitSpan(f3,this.GetViewport(f3),e8);
this.InitSpan(f3,this.GetColHeader(f3),f0);
this.InitSpan(f3,this.GetRowHeader(f3),e9);
f3.style.overflow="hidden";
if (this.GetParentSpread(f3)==null){
this.LoadScrollbarState(f3);
var g6=this.GetData(f3);
if (g6!=null){
var g7=g6.getElementsByTagName("root")[0];
var g8=g7.getElementsByTagName("activespread")[0];
if (g8!=null&&g8.innerHTML!=""){
this.SetPageActiveSpread(document.getElementById(this.Trim(g8.innerHTML)));
}
}
}
this.InitLayout(f3);
f3.f1=true;
if (this.GetPageActiveSpread()==f3&&(f3.getAttribute("AllowInsert")=="false"||f3.getAttribute("IsNewRow")=="true")){
var g9=this.GetCmdBtn(f3,"Insert");
this.UpdateCmdBtnState(g9,true);
g9=this.GetCmdBtn(f3,"Add");
this.UpdateCmdBtnState(g9,true);
}
this.CreateTextbox(f3);
this.CreateFocusBorder(f3);
this.InitSelection(f3);
f3.initialized=true;
if (this.GetPageActiveSpread()==f3)
{
try {
if (document.activeElement==null||document.activeElement==f3||this.IsChild(document.activeElement,f3)){
if (cmd!="LoadOnDemand")this.Focus(f3);
}
}catch (e){}
}
this.SaveData(f3);
if (this.handlers==null)
this.handlers=new FpSpread_EventHandlers();
this.handlers.DetachEvents(f3);
this.handlers.AttachEvents(f3);
if (d6!=null&&f3.style.position==""){
d6.parentNode.style.backgroundColor=d6.style.backgroundColor;
d6.parentNode.style.borderTop=d6.style.borderTop;
}
this.SyncColSelection(f3);
}
this.Dispose=function (f3){
if (this.handlers==null)
this.handlers=new FpSpread_EventHandlers();
this.handlers.DetachEvents(f3);
}
this.CmdbarMouseOver=function (event){
var f9=this.GetTarget(event);
if (f9!=null&&f9.tagName=="IMG"&&f9.getAttribute("disabled")!="true"){
f9.style.backgroundColor="cyan";
}
}
this.CmdbarMouseOut=function (event){
var f9=this.GetTarget(event);
if (f9!=null&&f9.tagName=="IMG"){
f9.style.backgroundColor="";
}
}
this.DoPropertyChange=function (event){
if (event.attrName=="curpos"){
this.ScrollViewport(event);
}else if (this.c3==null&&this.c4==null&&event.attrName=="pageincrement"&&event.ctrlKey){
var f3=this.GetSpread(this.GetTarget(event));
if (f3!=null)
this.SizeAll(this.GetTopSpread(f3));
}
}
this.HandleFirstKey=function (){
var f3=this.GetPageActiveSpread();
if (f3==null)return ;
var g2=this.GetTopSpread(f3);
var h0=document.getElementById(g2.id+"_textBox");
if (h0!=null&&h0.value!=""){
h0.value="";
}
}
this.IsXHTML=function (f3){
var g2=this.GetTopSpread(f3);
if (g2==null)return false;
var h1=g2.getAttribute("strictMode");
return (h1!=null&&h1=="true");
}
this.GetData=function (f3){
return f3.d0;
}
this.AttachEvent=function (target,event,handler,useCapture){
if (target.addEventListener!=null){
target.addEventListener(event,handler,useCapture);
}else if (target.attachEvent!=null){
target.attachEvent("on"+event,handler);
}
}
this.DetachEvent=function (target,event,handler,useCapture){
if (target.removeEventListener!=null){
target.removeEventListener(event,handler,useCapture);
}else if (target.detachEvent!=null){
target.detachEvent("on"+event,handler);
}
}
this.CancelDefault=function (e){
if (e.preventDefault!=null){
e.preventDefault();
e.stopPropagation();
}else {
e.cancelBubble=false;
e.returnValue=false;
}
return false;
}
this.CreateEvent=function (name){
var h2=document.createEvent("Events")
h2.initEvent(name,true,true);
return h2;
}
this.Refresh=function (f3){
var f9=f3.style.display;
f3.style.display="none";
f3.style.display=f9;
}
this.InitMethods=function (f3){
var f2=this;
f3.Edit=function (){f2.Edit(this);}
f3.Update=function (){f2.Update(this);}
f3.Cancel=function (){f2.Cancel(this);}
f3.Clear=function (){f2.Clear(this);}
f3.Copy=function (){f2.Copy(this);}
f3.Paste=function (){f2.Paste(this);}
f3.Prev=function (){f2.Prev(this);}
f3.Next=function (){f2.Next(this);}
f3.Add=function (){f2.Add(this);}
f3.Insert=function (){f2.Insert(this);}
f3.Delete=function (){f2.Delete(this);}
f3.Print=function (){f2.Print(this);}
f3.StartEdit=function (cell){f2.StartEdit(this,cell);}
f3.EndEdit=function (){f2.EndEdit(this);}
f3.ClearSelection=function (){f2.ClearSelection(this);}
f3.GetSelectedRange=function (){return f2.GetSelectedRange(this);}
f3.SetSelectedRange=function (f7,c,f6,cc){f2.SetSelectedRange(this,f7,c,f6,cc);}
f3.GetSelectedRanges=function (){return f2.GetSelectedRanges(this);}
f3.AddSelection=function (f7,c,f6,cc){f2.AddSelection(this,f7,c,f6,cc);}
f3.AddSpan=function (f7,c,f6,cc,spans){f2.AddSpan(this,f7,c,f6,cc,spans);}
f3.RemoveSpan=function (f7,c,spans){f2.RemoveSpan(this,f7,c,spans);}
f3.GetActiveRow=function (){var f9=f2.GetRowFromCell(this,this.e0);if (f9<0)return f9;return f2.GetSheetIndex(this,f9);}
f3.GetActiveCol=function (){return f2.GetColFromCell(this,this.e0);}
f3.SetActiveCell=function (f7,c){f2.SetActiveCell(this,f7,c);}
f3.GetCellByRowCol=function (f7,c){return f2.GetCellByRowCol(this,f7,c);}
f3.GetValue=function (f7,c){return f2.GetValue(this,f7,c);}
f3.SetValue=function (f7,c,v,noEvent,recalc){f2.SetValue(this,f7,c,v,noEvent,recalc);}
f3.GetFormula=function (f7,c){return f2.GetFormula(this,f7,c);}
f3.SetFormula=function (f7,c,f,recalc,clientOnly){f2.SetFormula(this,f7,c,f,recalc,clientOnly);}
f3.GetHiddenValue=function (f7,colName){return f2.GetHiddenValue(this,f7,colName);}
f3.GetSheetRowIndex=function (f7){return f2.GetSheetRowIndex(this,f7);}
f3.GetSheetColIndex=function (c){return f2.GetSheetColIndex(this,c);}
f3.GetRowCount=function (){return f2.GetRowCount(this);}
f3.GetColCount=function (){return f2.GetColCount(this);}
f3.GetRowByKey=function (key){return f2.GetRowByKey(this,key,-1);}
f3.GetColByKey=function (key){return f2.GetColByKey(this,key,-1);}
f3.GetRowKeyFromRow=function (f7){var h3=f2.GetRowKeyFromRow(this,f7);return h3==null?-1:h3;}
f3.GetColKeyFromCol=function (c){var h3=f2.GetColKeyFromCol(this,c);return h3==null?-1:h3;}
f3.GetTotalRowCount=function (){return f2.GetTotalRowCount(this);}
f3.GetPageCount=function (){return f2.GetPageCount(this);}
f3.GetParentSpread=function (){return f2.GetParentSpread(this);}
f3.GetChildSpread=function (f7,ri){return f2.GetChildSpread(this,f7,ri);}
f3.GetChildSpreads=function (){return f2.GetChildSpreads(this);}
f3.GetParentRowIndex=function (){return f2.GetParentRowIndex(this);}
f3.GetActiveChildSheetView=function (){return f2.GetActiveChildSheetView(this);}
f3.GetSpread=function (f9){return f2.GetSpread(f9);}
f3.UpdatePostbackData=function (){f2.UpdatePostbackData(this);}
f3.SizeToFit=function (c){f2.SizeToFit(this,c);}
f3.SetColWidth=function (c,w){f2.SetColWidth(this,c,w);}
f3.GetPreferredRowHeight=function (f7){return f2.GetPreferredRowHeight(this,f7);}
f3.SetRowHeight2=function (f7,f8){f2.SetRowHeight2(this,f7,f8);}
f3.CallBack=function (cmd,asyncCallBack){f2.SyncData(this.getAttribute("name"),cmd,this,asyncCallBack);}
f3.AddKeyMap=function (keyCode,ctrl,shift,alt,action){f2.AddKeyMap(this,keyCode,ctrl,shift,alt,action);}
f3.RemoveKeyMap=function (keyCode,ctrl,shift,alt){f2.RemoveKeyMap(this,keyCode,ctrl,shift,alt);}
f3.MoveToPrevCell=function (){f2.MoveToPrevCell(this);}
f3.MoveToNextCell=function (){f2.MoveToNextCell(this);}
f3.MoveToNextRow=function (){f2.MoveToNextRow(this);}
f3.MoveToPrevRow=function (){f2.MoveToPrevRow(this);}
f3.MoveToFirstColumn=function (){f2.MoveToFirstColumn(this);}
f3.MoveToLastColumn=function (){f2.MoveToLastColumn(this);}
f3.ScrollTo=function (f7,c){f2.ScrollTo(this,f7,c);}
f3.focus=function (){f2.Focus(this);}
f3.ShowMessage=function (msg,f7,c,time){return f2.ShowMessage(this,msg,f7,c,time);}
f3.HideMessage=function (f7,c){return f2.HideMessage(this,f7,c);}
f3.ProcessKeyMap=function (event){
if (this.keyMap!=null){
var f4=this.keyMap.length;
for (var f5=0;f5<f4;f5++){
var h4=this.keyMap[f5];
if (event.keyCode==h4.key&&event.ctrlKey==h4.ctrl&&event.shiftKey==h4.shift&&event.altKey==h4.alt){
var h5=false;
if (typeof(h4.action)=="function")
h5=h4.action();
else 
h5=eval(h4.action);
return h5;
}
}
}
return true;
}
f3.Cells=function (f7,c){return f2.Cells(this,f7,c);}
f3.Rows=function (f7,c){return f2.Rows(this,f7,c);}
f3.Columns=function (f7,c){return f2.Columns(this,f7,c);}
f3.GetTitleInfo=function (f7,c){return f2.GetTitleInfo(this,f7,c);}
f3.SizeSpread=function (f3){return f2.SizeSpread(f3);}
f3.SortColumn=function (column){return f2.SortColumn(this,column);}
f3.SuspendLayout=function (){f2.SuspendLayout(this);}
f3.ResumeLayout=function (performLayout){f2.ResumeLayout(this,performLayout);}
}
this.CreateTextbox=function (f3){
var g2=this.GetTopSpread(f3);
if (g2==null)return ;
var h0=document.getElementById(g2.id+"_textBox");
if (h0==null)
{
h0=document.createElement('INPUT');
h0.type="text";
h0.setAttribute("autocomplete","off");
h0.style.position="absolute";
h0.style.borderWidth=0;
h0.style.top="-10px";
h0.style.left="-100px";
h0.style.width="1px";
h0.style.height="1px";
if (f3.tabIndex!=null)
h0.tabIndex=f3.tabIndex;
h0.id=f3.id+"_textBox";
f3.insertBefore(h0,f3.firstChild);
}
}
this.CreateLineBorder=function (f3,id){
var h6=document.getElementById(id);
if (h6==null)
{
h6=document.createElement('div');
h6.style.position="absolute";
h6.style.left="-1000px";
h6.style.top="0px";
h6.style.overflow="hidden";
h6.style.border="1px solid black";
if (f3.getAttribute("FocusBorderColor")!=null)
h6.style.borderColor=f3.getAttribute("FocusBorderColor");
if (f3.getAttribute("FocusBorderStyle")!=null)
h6.style.borderStyle=f3.getAttribute("FocusBorderStyle");
h6.id=id;
var h7=this.GetViewport(f3).parentNode;
h7.insertBefore(h6,null);
}
return h6;
}
this.CreateFocusBorder=function (f3){
if (this.GetTopSpread(f3).getAttribute("hierView")=="true")return ;
if (this.GetTopSpread(f3).getAttribute("showFocusRect")=="false")return ;
if (this.GetViewport(f3)==null)return ;
var h6=this.CreateLineBorder(f3,f3.id+"_focusRectT");
h6.style.height=0;
h6=this.CreateLineBorder(f3,f3.id+"_focusRectB");
h6.style.height=0;
h6=this.CreateLineBorder(f3,f3.id+"_focusRectL");
h6.style.width=0;
h6=this.CreateLineBorder(f3,f3.id+"_focusRectR");
h6.style.width=0;
}
this.GetPosIndicator=function (f3){
var h8=f3.posIndicator;
if (h8==null)
h8=this.CreatePosIndicator(f3);
else if (h8.parentNode!=f3)
f3.insertBefore(h8,null);
return h8;
}
this.CreatePosIndicator=function (f3){
var h8=document.createElement("img");
h8.style.position="absolute";
h8.style.top="0px";
h8.style.left="-400px";
h8.style.width="10px";
h8.style.height="10px";
h8.style.zIndex=1000;
h8.id=f3.id+"_posIndicator";
if (f3.getAttribute("clienturl")!=null)
h8.src=f3.getAttribute("clienturl")+"down.gif";
else 
h8.src=f3.getAttribute("clienturlres");
f3.insertBefore(h8,null);
f3.posIndicator=h8;
return h8;
}
this.InitSpan=function (f3,g0,spans){
if (g0!=null){
var h9=0;
if (g0==this.GetViewport(f3))
h9=g0.rows.length;
var i0=g0.rows;
var i1=this.GetColCount(f3);
for (var f7=0;f7<i0.length;f7++){
if (this.IsChildSpreadRow(f3,g0,f7)){
if (g0==this.GetViewport(f3))h9--;
}else {
var i2=i0[f7].cells;
for (var i3=0;i3<i2.length;i3++){
var i4=i2[i3];
if (i4!=null&&((i4.rowSpan!=null&&i4.rowSpan>1)||(i4.colSpan!=null&&i4.colSpan>1))){
var i5=this.GetRowFromCell(f3,i4);
var i6=parseInt(i4.getAttribute("scol"));
if (i6<i1){
this.AddSpan(f3,i5,i6,i4.rowSpan,i4.colSpan,spans);
}
}
}
}
}
if (g0==this.GetViewport(f3))f3.setAttribute("rowCount",h9);
}
}
this.GetColWithSpan=function (f3,f7,spans,i3){
var i7=0;
var i8=0;
if (i3==0){
while (this.IsCovered(f3,f7,i8,spans))
{
i8++;
}
}
for (var f5=0;f5<spans.length;f5++){
if (spans[f5].rowCount>1&&(spans[f5].col<=i3||i3==0&&spans[f5].col<i8)&&f7>=spans[f5].row&&f7<spans[f5].row+spans[f5].rowCount)
i7+=spans[f5].colCount;
}
return i7;
}
this.AddSpan=function (f3,f7,i3,f6,i1,spans){
if (spans==null)spans=f3.e8;
var i9=new this.Range();
this.SetRange(i9,"Cell",f7,i3,f6,i1);
spans.push(i9);
this.PaintFocusRect(f3);
}
this.RemoveSpan=function (f3,f7,i3,spans){
if (spans==null)spans=f3.e8;
for (var f5=0;f5<spans.length;f5++){
var i9=spans[f5];
if (i9.row==f7&&i9.col==i3){
var j0=spans.length-1;
for (var j1=f5;j1<j0;j1++){
spans[j1]=spans[j1+1];
}
spans.length=spans.length-1;
break ;
}
}
this.PaintFocusRect(f3);
}
this.Focus=function (f3){
if (this.editing)return ;
this.SetPageActiveSpread(f3);
var j2=this.GetOperationMode(f3);
if (f3.e0==null&&j2!="MultiSelect"&&j2!="ExtendedSelect"&&f3.GetRowCount()>0&&f3.GetColCount()>0){
var j3=this.FireActiveCellChangingEvent(f3,0,0,0);
if (!j3){
f3.SetActiveCell(0,0);
var h2=this.CreateEvent("ActiveCellChanged");
h2.cmdID=f3.id;
h2.row=h2.Row=0;
h2.col=h2.Col=0;
if (f3.getAttribute("LayoutMode"))
h2.InnerRow=h2.innerRow=0;
this.FireEvent(f3,h2);
}
}
var g2=this.GetTopSpread(f3);
var h0=document.getElementById(g2.id+"_textBox");
if (f3.e0!=null){
var j4=this.GetEditor(f3.e0);
if (j4==null){
if (h0!=null){
if (this.activeElement!=h0){
try {h0.focus();}catch (h2){}
}
}
}else {
if (j4.tagName!="SELECT")j4.focus();
this.SetEditorFocus(j4);
}
}else {
if (h0!=null){
try {h0.focus();}catch (h2){}
}
}
this.EnableButtons(f3);
}
this.GetTotalRowCount=function (f3){
var f9=parseInt(f3.getAttribute("totalRowCount"));
if (isNaN(f9))f9=0;
return f9;
}
this.GetPageCount=function (f3){
var f9=parseInt(f3.getAttribute("pageCount"));
if (isNaN(f9))f9=0;
return f9;
}
this.GetColCount=function (f3){
var f9=parseInt(f3.getAttribute("colCount"));
if (isNaN(f9))f9=0;
return f9;
}
this.GetRowCount=function (f3){
var f9=parseInt(f3.getAttribute("rowCount"));
if (isNaN(f9))f9=0;
return f9;
}
this.GetRowCountInternal=function (f3){
var f9=parseInt(this.GetViewport(f3).rows.length);
if (isNaN(f9))f9=0;
return f9;
}
this.IsChildSpreadRow=function (f3,view,f7){
if (f3==null||view==null)return false;
if (f7>=1&&f7<view.rows.length){
if (view.rows[f7].cells.length>0&&view.rows[f7].cells[0]!=null&&view.rows[f7].cells[0].firstChild!=null){
var f9=view.rows[f7].cells[0].firstChild;
if (f9.nodeName!="#text"&&f9.getAttribute("FpSpread")=="Spread")return true;
}
}
return false;
}
this.GetChildSpread=function (f3,row,rindex){
var j5=this.GetViewport(f3);
if (j5!=null){
var f7=this.GetDisplayIndex(f3,row)+1;
if (typeof(rindex)=="number")f7+=rindex;
if (f7>=1&&f7<j5.rows.length){
if (j5.rows[f7].cells.length>0&&j5.rows[f7].cells[0]!=null&&j5.rows[f7].cells[0].firstChild!=null){
var f9=j5.rows[f7].cells[0].firstChild;
if (f9.nodeName!="#text"&&f9.getAttribute("FpSpread")=="Spread"){
return f9;
}
}
}
}
return null;
}
this.GetChildSpreads=function (f3){
var f5=0;
var h5=new Array();
var j5=this.GetViewport(f3);
if (j5!=null){
for (var f7=1;f7<j5.rows.length;f7++){
if (j5.rows[f7].cells.length>0&&j5.rows[f7].cells[0]!=null&&j5.rows[f7].cells[0].firstChild!=null){
var f9=j5.rows[f7].cells[0].firstChild;
if (f9.nodeName!="#text"&&f9.getAttribute("FpSpread")=="Spread"){
h5.length=f5+1;
h5[f5]=f9;
f5++;
}
}
}
}
return h5;
}
this.GetDisplayIndex=function (f3,row){
if (row<0)return -1;
var f5=0;
var f7=0;
var j5=this.GetViewport(f3);
if (j5!=null){
for (f5=0;f5<j5.rows.length;f5++){
if (this.IsChildSpreadRow(f3,j5,f5))continue ;
if (f7==row)break ;
f7++;
}
}
return f5;
}
this.GetSheetIndex=function (f3,row,d1){
var f5=0
var f7=0;
var j5=d1;
if (j5==null)j5=this.GetViewport(f3);
if (j5!=null){
if (row<0||row>=j5.rows.length)return -1;
for (f5=0;f5<row;f5++){
if (this.IsChildSpreadRow(f3,j5,f5))continue ;
f7++;
}
}
return f7;
}
this.GetParentRowIndex=function (f3){
var j6=this.GetParentSpread(f3);
if (j6==null)return -1;
var j5=this.GetViewport(j6);
if (j5==null)return -1;
var j7=f3.parentNode.parentNode;
var f5=j7.rowIndex-1;
for (;f5>0;f5--){
if (this.IsChildSpreadRow(j6,j5,f5))continue ;
else 
break ;
}
return this.GetSheetIndex(j6,f5,j5);
}
this.CreateTestBox=function (f3){
var j8=document.getElementById(f3.id+"_testBox");
if (j8==null)
{
j8=document.createElement("span");
j8.style.position="absolute";
j8.style.borderWidth=0;
j8.style.top="-500px";
j8.style.left="-100px";
j8.id=f3.id+"_testBox";
f3.insertBefore(j8,f3.firstChild);
}
return j8;
}
this.SizeToFit=function (f3,i3){
if (i3==null||i3<0)i3=0;
var g0=this.GetViewport(f3);
if (g0!=null){
var j8=this.CreateTestBox(f3);
var i0=g0.rows;
var j9=0;
for (var f7=0;f7<i0.length;f7++){
if (!this.IsChildSpreadRow(f3,g0,f7)){
var k0=this.GetCellFromRowCol(f3,f7,i3);
if (k0.colSpan>1)continue ;
var k1=this.GetPreferredCellWidth(f3,k0,j8);
if (k1>j9)j9=k1;
}
}
this.SetColWidth(f3,i3,j9);
}
}
this.GetPreferredCellWidth=function (f3,k0,j8){
if (j8==null)j8=this.CreateTestBox(f3);
var k2=this.GetRender(f3,k0);
var k3=this.GetCellType(k0);
var k4=this.GetEditor(k0);
if (k2!=null){
j8.style.fontFamily=k2.style.fontFamily;
j8.style.fontSize=k2.style.fontSize;
j8.style.fontWeight=k2.style.fontWeight;
j8.style.fontStyle=k2.style.fontStyle;
}
if (k2!=null&&k3=="MultiColumnComboBoxCellType"){
var k5=k0.getElementsByTagName("Table")[0];
if (k5!=null){
j8.innerHTML=this.GetEditorValue(k4)+"123";
}
}
else {
j8.innerHTML=k0.innerHTML;
}
var k1=j8.offsetWidth+8;
if (k0.style.paddingLeft!=null&&k0.style.paddingLeft.length>0)
k1+=parseInt(k0.style.paddingLeft);
if (k0.style.paddingRight!=null&&k0.style.paddingRight.length>0)
k1+=parseInt(k0.style.paddingRight);
return k1;
}
this.GetHierBar=function (f3){
if (f3.d7==null)f3.d7=document.getElementById(f3.id+"_hierBar");
return f3.d7;
}
this.GetGroupBar=function (f3){
if (f3.groupBar==null)f3.groupBar=document.getElementById(f3.id+"_groupBar");
return f3.groupBar;
}
this.GetPager1=function (f3){
if (f3.d8==null)f3.d8=document.getElementById(f3.id+"_pager1");
return f3.d8;
}
this.GetPager2=function (f3){
if (f3.d9==null)f3.d9=document.getElementById(f3.id+"_pager2");
return f3.d9;
}
this.SynRowHeight=function (f3,d4,g0,f7,updateParent,header,d3){
if (d4==null||g0==null)return ;
if (typeof(d4.rows[f7])!="undefined"&&
typeof(d4.rows[f7].cells[0])!="undefined")
{
if (d4.rows[f7].cells[0].style.posHeight==null||d4.rows[f7].cells[0].style.posHeight=="")
d4.rows[f7].cells[0].style.posHeight=d4.rows[f7].offsetHeight-1;
}
var k6=d4.rows[f7].offsetHeight;
var h7=g0.rows[f7].offsetHeight;
if (k6==h7&&(f7>0||d3))return ;
var k7=0;
if (g0.cellSpacing=="0"&&f7==0){
if (document.defaultView!=null&&document.defaultView.getComputedStyle!=null){
var k8=0;
for (var f5=0;f5<g0.rows[f7].cells.length;f5++){
k8=parseInt(document.defaultView.getComputedStyle(g0.rows[f7].cells[f5],'').getPropertyValue("border-top-width"));
if (k8>k7)k7=k8;
}
}
}
g0.rows[f7].style.height="";
var f8=Math.max(k6,h7);
k7=parseInt(k7/2);
var k9=this.IsXHTML(f3);
if (this.IsChildSpreadRow(f3,g0,f7)){
if (k9)f8-=1;
d4.rows[f7].cells[0].style.posHeight=f8-1;
return ;
}
if (f8==k6){
if (g0.rows[f7].cells[0]!=null){
if (g0.cellSpacing=="0"&&f7==0){
g0.rows[f7].cells[0].style.posHeight+=(f8-h7-k7);
}else {
g0.rows[f7].cells[0].style.posHeight+=(f8-h7);
}
}
}else {
if (d4.rows[f7].cells[0]!=null){
if (d4.cellSpacing=="0"&&f7==0){
d4.rows[f7].cells[0].style.posHeight+=(f8-k6+k7);
}else {
d4.rows[f7].cells[0].style.posHeight+=(f8-k6);
}
}
}
if (updateParent){
var j6=this.GetParentSpread(f3);
if (j6!=null)this.UpdateRowHeight(j6,f3);
}
}
this.SizeAll=function (f3){
var l0=this.GetChildSpreads(f3);
if (l0!=null&&l0.length>0){
for (var f5=0;f5<l0.length;f5++){
this.SizeAll(l0[f5]);
}
}
this.SizeSpread(f3);
if (this.GetParentSpread(f3)!=null)
this.Refresh(f3);
}
this.SuspendLayout=function (f3){
f3.bSuspendLayout=true;
}
this.ResumeLayout=function (f3,performLayout){
f3.bSuspendLayout=false;
if (performLayout)this.SizeSpread(f3);
}
this.SizeSpread=function (f3){
if (f3.bSuspendLayout)return ;
if (f3.offsetHeight==0)
{
var f9=f3;
while (f9!=null&&f9!=document.body){
f9.displaySetting=f9.style.display;
f9.style.display="";
f9=f9.parentNode;
}
}
if (f3.clientHeight==0||f3.clientWidth==0)return ;
var k9=this.IsXHTML(f3);
var d1=this.GetViewport(f3);
if (d1==null)return ;
this.SyncMsgs(f3);
var d4=this.GetRowHeader(f3);
if (d4!=null){
for (var f5=0;f5<d1.rows.length&&f5<d4.rows.length;f5++){
this.SynRowHeight(f3,d4,d1,f5,false,true);
this.SynRowHeight(f3,d4,d1,f5,false,true);
if (f5==0&&d4.rows[0].cells[0]&&d1.rows[0].cells[0]&&d1.rows[0].cells[0].getAttribute("CellType2")=="SlideShowCellType")
d4.rows[0].cells[0].style.posHeight=d1.rows[0].cells[0].offsetHeight-1;
}
}
var l1=this.GetColFooter(f3);
var d5=this.GetColHeader(f3);
var d3=this.GetCorner(f3);
if (d3!=null&&d5!=null&&d3.getAttribute("allowTableCorner")){
for (var f5=0;f5<d3.rows.length&&f5<d5.rows.length;f5++){
if (d5.rows[f5].cells.length){
if (d5.rows[0].cells.length>1)
this.SynRowHeight(f3,d5,d3,f5,true,true,false);
this.SynRowHeight(f3,d3,d5,f5,true,false,true);
}
}
}
var l2=this.GetColGroup(d1);
var l3=this.GetColGroup(d5);
if (l2!=null&&l2.childNodes.length>0&&l3!=null&&l3.childNodes.length>0){
var l4=-1;
if (this.c3!=null)l4=parseInt(this.c3.getAttribute("index"));
if (this.c3==null||l4==0)
{
var l5=parseInt(l2.childNodes[0].width);var l6=parseInt(l2.childNodes[0].offsetLeft);
l3.childNodes[0].width=""+(l5-l6)+"px";
l2.childNodes[0].width=""+l5+"px";
this.SetWidthFix(d5,0,(l5-l6));
this.SetWidthFix(d1,0,l5);
}
}
var j6=this.GetParentSpread(f3);
if (j6!=null)this.UpdateRowHeight(j6,f3);
var f8=f3.clientHeight;
var l7=this.GetCommandBar(f3);
if (l7!=null)
{
l7.style.width=""+f3.clientWidth+"px";
if (f3.style.position!="absolute"&&f3.style.position!="relative"){
l7.parentNode.style.borderTop="1px solid white";
l7.parentNode.style.backgroundColor=l7.style.backgroundColor;
}
var l8=this.GetElementById(l7,f3.id+"_cmdTable");
if (l8!=null){
if (f3.style.position!="absolute"&&f3.style.position!="relative"&&(l8.style.height==""||parseInt(l8.style.height)<27)){
l8.style.height=""+(l8.offsetHeight+3)+"px";
}
if (!k9&&parseInt(d1.cellSpacing)>0)
l8.parentNode.style.height=""+(l8.offsetHeight+3)+"px";
f8-=Math.max(l8.parentNode.offsetHeight,l8.offsetHeight);
}
if (f3.style.position!="absolute"&&f3.style.position!="relative")
l7.style.position="";
}
var d5=this.GetColHeader(f3);
if (d5!=null)
{
f8-=d5.offsetHeight;
d5.parentNode.style.height=""+(d5.offsetHeight-parseInt(d5.cellSpacing))+"px";
if (k9)
f8+=parseInt(d5.cellSpacing);
}
var l1=this.GetColFooter(f3);
if (l1!=null)
{
f8-=l1.offsetHeight;
l1.parentNode.style.height=""+(l1.offsetHeight)+"px";
}
var d7=this.GetHierBar(f3);
if (d7!=null)
{
f8-=d7.offsetHeight;
}
var l9=document.getElementById(f3.id+"_titleBar");
if (l9)f8-=l9.parentNode.parentNode.offsetHeight;
var m0=this.GetGroupBar(f3);
if (m0!=null){
f8-=m0.offsetHeight;
}
var d8=this.GetPager1(f3);
if (d8!=null)
{
f8-=d8.offsetHeight;
this.InitSlideBar(f3,d8);
}
var m1=(f3.getAttribute("cmdTop")=="true");
var d9=this.GetPager2(f3);
if (d9!=null)
{
d9.style.width=""+(f3.clientWidth-10)+"px";
f8-=Math.max(d9.offsetHeight,28);
this.InitSlideBar(f3,d9);
}
var m2=null;
if (d4!=null)m2=d4.parentNode;
var m3=null;
if (d5!=null)m3=d5.parentNode;
var m4=null;
if (l1!=null)m4=l1.parentNode;
var m5=this.GetFooterCorner(f3);
if (m4!=null)
{
m4.style.height=""+l1.offsetHeight-parseInt(d1.cellSpacing)+"px";
if (m5!=null){
m5.parentNode.style.height=m4.style.height;
}
}
if (m5!=null&&!k9)
m5.width=""+(m5.parentNode.offsetWidth+parseInt(d1.cellSpacing))+"px";
var m6=d1.parentNode;
var d3=this.GetCorner(f3);
if (k9&&m3!=null)
{
m3.style.height=""+d5.offsetHeight-parseInt(d1.cellSpacing)+"px";
if (d3!=null){
d3.parentNode.style.height=m3.style.height;
}
}
if (d3!=null&&!k9)
d3.width=""+(d3.parentNode.offsetWidth+parseInt(d1.cellSpacing))+"px";
if (m6!=null){
if (m2!=null){
m6.style.width=""+Math.max(f3.clientWidth-m2.offsetWidth+parseInt(d1.cellSpacing),1)+"px";
m6.style.height=""+Math.max(f8,1)+"px";
m6.style.width=""+Math.max(f3.clientWidth-m2.offsetWidth+parseInt(d1.cellSpacing),1)+"px";
}else {
m6.style.width=""+Math.max(f3.clientWidth,1)+"px";
m6.style.height=""+Math.max(f8,1)+"px";
m6.style.width=""+Math.max(f3.clientWidth,1)+"px";
}
}
var m7=0;
if (this.GetColFooter(f3)){
m7=this.GetColFooter(f3).offsetHeight;
}
if (l7!=null&&!m1){
if (d9!=null){
if (f3.style.position=="absolute"||f3.style.position=="relative"){
l7.style.position="absolute";
l7.style.top=""+(f3.clientHeight-Math.max(d9.offsetHeight,28)-l7.offsetHeight)+"px";
}else {
l7.style.position="absolute";
l7.style.top=""+(d1.parentNode.offsetTop+m7+d1.parentNode.offsetHeight)+"px";
}
}else {
if (f3.style.position=="absolute"||f3.style.position=="relative")
{
l7.style.position="absolute";
l7.style.top=""+(f3.clientHeight-l7.offsetHeight)+"px";
}else {
l7.style.position="absolute";
if (d9!=null)
l7.style.top=""+(this.GetOffsetTop(f3,f3,document.body)+f3.clientHeight-Math.max(d9.offsetHeight,28)-l7.offsetHeight)+"px";
else 
l7.style.top=""+(this.GetOffsetTop(f3,f3,document.body)+f3.clientHeight-l7.offsetHeight+1)+"px";
}
}
}
if (d9!=null)
{
if (f3.style.position=="absolute"||f3.style.position=="relative"){
d9.style.position="absolute";
d9.style.top=""+(f3.clientHeight-Math.max(d9.offsetHeight,28))+"px";
}else {
d9.style.position="absolute";
if (l7!=null&&!m1)
d9.style.top=""+(d1.parentNode.offsetTop+d1.parentNode.offsetHeight+l7.offsetHeight+m7)+"px";
else 
d9.style.top=""+(d1.parentNode.offsetTop+d1.parentNode.offsetHeight+m7)+"px";
}
}
if (m2!=null){
if (k9)m2.style.height=""+Math.max(m6.offsetHeight,1)+"px";
else m2.style.height=Math.max(m6.offsetHeight,1);
}
if (d1&&!d4){
d1.parentNode.parentNode.style.height=""+d1.parentNode.offsetHeight+"px";
}
return ;
if (this.GetParentSpread(f3)==null&&m3!=null){
var k1=0;
if (m2!=null){
k1=Math.max(f3.clientWidth-m2.offsetWidth,1);
}else {
k1=Math.max(f3.clientWidth,1);
}
m3.style.width=k1;
m3.parentNode.style.width=k1;
}
if (k9)
{
if (d1!=null){
d1.style.posTop=-d1.cellSpacing;
var m8=f3.clientWidth;
if (d4!=null)m8-=d4.parentNode.offsetWidth;
d1.parentNode.style.width=""+m8+"px";
}
if (d4!=null){
d4.style.position="relative";
d4.parentNode.style.position="relative";
d4.style.posTop=-d1.cellSpacing;
d4.width=""+(d4.parentNode.offsetWidth)+"px";
}
}else {
if (d1!=null){
var m8=f3.clientWidth;
if (d4!=null){
m8-=d4.parentNode.offsetWidth;
d4.width=""+(d4.parentNode.offsetWidth+parseInt(d1.cellSpacing))+"px";
}
d1.parentNode.style.width=""+m8+"px";
}
}
this.ScrollView(f3);
this.PaintFocusRect(f3);
var f9=f3;
while (f9!=null&&f9!=document.body){
if (f9.displaySetting!=null){
f9.style.display=f9.displaySetting;
f9.displaySetting=null;
}else {
break ;
}
f9=f9.parentNode;
}
}
this.InitSlideBar=function (f3,pager){
var m9=this.GetElementById(pager,f3.id+"_slideBar");
if (m9!=null){
var k9=this.IsXHTML(f3);
if (k9)
m9.style.height=Math.max(pager.offsetHeight,28)+"px";
else 
m9.style.height=(pager.offsetHeight-2)+"px";
var f9=pager.getElementsByTagName("TABLE");
if (f9!=null&&f9.length>0){
var n0=f9[0].rows[0];
var i6=n0.cells[0];
var n1=n0.cells[2];
f3.slideLeft=Math.max(107,i6.offsetWidth+1);
if (i6.style.paddingRight!="")f3.slideLeft+=parseInt(i6.style.paddingRight);
f3.slideRight=pager.offsetWidth-n1.offsetWidth-m9.offsetWidth-3;
if (n1.style.paddingRight!="")f3.slideRight-=parseInt(n1.style.paddingLeft);
var n2=parseInt(pager.getAttribute("curPage"));
var n3=parseInt(pager.getAttribute("totalPage"))-1;
if (n3==0)n3=1;
var n4=false;
var m8=Math.max(107,f3.slideLeft)+(n2/n3)*(f3.slideRight-f3.slideLeft);
if (pager.id.indexOf("pager1")>=0&&f3.style.position!="absolute"&&f3.style.position!="relative"){
m8+=this.GetOffsetLeft(f3,pager,document);
var n5=(this.GetOffsetTop(f3,i6,pager)+this.GetOffsetTop(f3,pager,document));
m9.style.top=n5+"px";
n4=true;
}
var l9=document.getElementById(f3.id+"_titleBar");
if (pager.id.indexOf("pager1")>=0&&!n4&&l9!=null){
var n5=l9.parentNode.parentNode.offsetHeight;
m9.style.top=n5+"px";
}
m9.style.left=m8+"px";
}
}
}
this.InitLayout=function (f3){
this.SizeSpread(f3);
this.SizeSpread(f3);
this.SizeSpread(f3);
}
this.GetRowByKey=function (f3,key,defaultValue){
if (key=="-1")
return -1;
var n6=this.GetViewport(f3);
if (n6!=null){
for (var j7=0;j7<n6.rows.length;j7++){
if (n6.rows[j7].getAttribute("FpKey")==key){
return j7;
}
}
}
if (defaultValue)return defaultValue;
if (n6!=null)
return 0;
else 
return -1;
}
this.GetColByKey=function (f3,key,defaultValue){
if (key=="-1")
return -1;
var n6=this.GetViewport(f3);
var n7=this.GetColGroup(n6);
if (n7==null||n7.childNodes.length==0)
n7=this.GetColGroup(this.GetColHeader(f3));
if (n7!=null){
for (var n8=0;n8<n7.childNodes.length;n8++){
var f9=n7.childNodes[n8];
if (f9.getAttribute("FpCol")==key){
return n8;
}
}
}
if (defaultValue)return defaultValue;
return 0;
}
this.IsRowSelected=function (f3,j7){
var n9=this.GetSelection(f3);
if (n9!=null){
var o0=n9.firstChild;
while (o0!=null){
var f7=parseInt(o0.getAttribute("rowIndex"));
var f6=parseInt(o0.getAttribute("rowcount"));
if (f7<=j7&&j7<f7+f6)
return true;
o0=o0.nextSibling;
}
}
}
this.InitSelection=function (f3){
var f7=0;
var i3=0;
var g6=this.GetData(f3);
if (g6==null)return ;
var g7=g6.getElementsByTagName("root")[0];
var o1=g7.getElementsByTagName("state")[0];
var n9=o1.getElementsByTagName("selection")[0];
var o2=o1.firstChild;
while (o2!=null&&o2.tagName!="activerow"&&o2.tagName!="ACTIVEROW"){
o2=o2.nextSibling;
}
if (o2!=null)
f7=this.GetRowByKey(f3,o2.innerHTML);
if (f7>=this.GetRowCount(f3))f7=0;
var o3=o1.firstChild;
while (o3!=null&&o3.tagName!="activecolumn"&&o3.tagName!="ACTIVECOLUMN"){
o3=o3.nextSibling;
}
if (o3!=null)
i3=this.GetColByKey(f3,o3.innerHTML);
if (f7<0)f7=0;
if (f7>=0||i3>=0){
var o4=g6;
if (this.GetParentSpread(f3)!=null){
var o5=this.GetTopSpread(f3);
if (o5.initialized)o4=this.GetData(o5);
g7=o4.getElementsByTagName("root")[0];
}
var o6=g7.getElementsByTagName("activechild")[0];
f3.e2=f7;f3.e3=i3;
if ((this.GetParentSpread(f3)==null&&(o6==null||o6.innerHTML==""))||(o6!=null&&f3.id==this.Trim(o6.innerHTML))){
this.UpdateAnchorCell(f3,f7,i3);
}else {
f3.e0=this.GetCellFromRowCol(f3,f7,i3);
}
}
var o0=n9.firstChild;
while (o0!=null){
var f7=this.GetRowByKey(f3,o0.getAttribute("row"));
var i3=this.GetColByKey(f3,o0.getAttribute("col"));
var f6=parseInt(o0.getAttribute("rowcount"));
var i1=parseInt(o0.getAttribute("colcount"));
o0.setAttribute("rowIndex",f7);
o0.setAttribute("colIndex",i3);
this.PaintSelection(f3,f7,i3,f6,i1,true);
o0=o0.nextSibling;
}
this.PaintFocusRect(f3);
}
this.TranslateKey=function (event){
event=this.GetEvent(event);
var o7=this.GetTarget(event);
try {
if (document.readyState!=null&&document.readyState!="complete")return ;
var f3=this.GetPageActiveSpread();
if (event.altKey&&event.keyCode==this.down&&typeof(o7.getAttribute("mccbparttype"))!="undefined"&&o7.getAttribute("mccbparttype")=="DropDownButton")return ;
if (typeof(f3.getAttribute("mcctCellType"))!="undefined"&&f3.getAttribute("mcctCellType")=="true")return ;
if (this.GetOperationMode(f3)=="RowMode"&&this.GetEnableRowEditTemplate(f3)=="true"&&this.IsInRowEditTemplate(f3,o7))return ;
if (f3!=null){
if (!this.IsChild(o7,this.GetTopSpread(f3)))return ;
this.KeyDown(f3,event);
var o8=false;
if (event.keyCode==this.tab){
var o9=this.GetProcessTab(f3);
o8=(o9=="true"||o9=="True");
}
if (o8)
this.CancelDefault(event);
}
}catch (h2){}
}
this.IsInRowEditTemplate=function (f3,o7){
while (o7&&o7.parentNode){
o7=o7.parentNode;
if (o7.tagName=="DIV"&&o7.id==f3.id+"_RowEditTemplateContainer")
return true;
}
return false;
}
this.KeyAction=function (key,ctrl,shift,alt,action){
this.key=key;
this.ctrl=ctrl;
this.shift=shift;
this.alt=alt;
this.action=action;
}
this.RemoveKeyMap=function (f3,keyCode,ctrl,shift,alt,action){
if (f3.keyMap==null)f3.keyMap=new Array();
var f4=f3.keyMap.length;
for (var f5=0;f5<f4;f5++){
var h4=f3.keyMap[f5];
if (h4!=null&&h4.key==keyCode&&h4.ctrl==ctrl&&h4.shift==shift&&h4.alt==alt){
for (var j1=f5+1;j1<f4;j1++){
f3.keyMap[j1-1]=f3.keyMap[j1];
}
f3.keyMap.length=f3.keyMap.length-1;
break ;
}
}
}
this.AddKeyMap=function (f3,keyCode,ctrl,shift,alt,action){
if (f3.keyMap==null)f3.keyMap=new Array();
var h4=this.GetKeyAction(f3,keyCode,ctrl,shift,alt);
if (h4!=null){
h4.action=action;
}else {
var f4=f3.keyMap.length;
f3.keyMap.length=f4+1;
f3.keyMap[f4]=new this.KeyAction(keyCode,ctrl,shift,alt,action);
}
}
this.GetKeyAction=function (f3,keyCode,ctrl,shift,alt){
if (f3.keyMap==null)f3.keyMap=new Array();
var f4=f3.keyMap.length;
for (var f5=0;f5<f4;f5++){
var h4=f3.keyMap[f5];
if (h4!=null&&h4.key==keyCode&&h4.ctrl==ctrl&&h4.shift==shift&&h4.alt==alt){
return h4;
}
}
return null;
}
this.MoveToPrevCell=function (f3){
var p0=this.EndEdit(f3);
if (!p0)return ;
var f7=f3.GetActiveRow();
var i3=f3.GetActiveCol();
this.MoveLeft(f3,f7,i3);
}
this.MoveToNextCell=function (f3){
var p0=this.EndEdit(f3);
if (!p0)return ;
var f7=f3.GetActiveRow();
var i3=f3.GetActiveCol();
this.MoveRight(f3,f7,i3);
}
this.MoveToNextRow=function (f3){
var p0=this.EndEdit(f3);
if (!p0)return ;
var f7=f3.GetActiveRow();
var i3=f3.GetActiveCol();
this.MoveDown(f3,f7,i3);
}
this.MoveToPrevRow=function (f3){
var p0=this.EndEdit(f3);
if (!p0)return ;
var f7=f3.GetActiveRow();
var i3=f3.GetActiveCol();
if (f7>0)
this.MoveUp(f3,f7,i3);
}
this.MoveToFirstColumn=function (f3){
var p0=this.EndEdit(f3);
if (!p0)return ;
var f7=f3.GetActiveRow();
if (f3.e0.parentNode.rowIndex>=0)
this.UpdateLeadingCell(f3,f7,0);
}
this.MoveToLastColumn=function (f3){
var p0=this.EndEdit(f3);
if (!p0)return ;
var f7=f3.GetActiveRow();
if (f3.e0.parentNode.rowIndex>=0){
i3=this.GetColCount(f3)-1;
this.UpdateLeadingCell(f3,f7,i3);
}
}
this.UpdatePostbackData=function (f3){
this.SaveData(f3);
}
this.PrepareData=function (o0){
var h5="";
if (o0!=null){
if (o0.nodeName=="#text")
h5=o0.nodeValue;
else {
h5=this.GetBeginData(o0);
var f9=o0.firstChild;
while (f9!=null){
var p1=this.PrepareData(f9);
if (p1!="")h5+=p1;
f9=f9.nextSibling;
}
h5+=this.GetEndData(o0);
}
}
return h5;
}
this.GetBeginData=function (o0){
var h5="<"+o0.nodeName.toLowerCase();
if (o0.attributes!=null){
for (var f5=0;f5<o0.attributes.length;f5++){
var p2=o0.attributes[f5];
if (p2.nodeName!=null&&p2.nodeName!=""&&p2.nodeName!="style"&&p2.nodeValue!=null&&p2.nodeValue!="")
h5+=(" "+p2.nodeName+"=\""+p2.nodeValue+"\"");
}
}
h5+=">";
return h5;
}
this.GetEndData=function (o0){
return "</"+o0.nodeName.toLowerCase()+">";
}
this.SaveData=function (f3){
if (f3==null)return ;
try {
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
var f9=this.PrepareData(g7);
var p3=document.getElementById(f3.id+"_data");
p3.value=encodeURIComponent(f9);
}catch (h2){
alert("e "+h2);
}
}
this.SetActiveSpread=function (event){
try {
event=this.GetEvent(event);
var o7=this.GetTarget(event);
var p4=this.GetSpread(o7,false);
var p5=this.GetPageActiveSpread();
if (this.editing&&(p4==null||(p4!=p5&&p4.getAttribute("mcctCellType")!="true"&&p5.getAttribute("mcctCellType")!="true"))){
if (o7!=this.b7&&this.b7!=null){
if (this.b7.blur!=null)this.b7.blur();
}
var p0=this.EndEdit();
if (!p0)return ;
}
var p6=false;
if (p4==null){
p4=this.GetSpread(o7,true);
p6=(p4!=null);
}
var i4=this.GetCell(o7,true);
if (i4==null&&p5!=null&&p5.f1){
this.SaveData(p5);
p5.f1=false;
}
if (p5!=null&&p5.f1&&(p4!=p5||p4==null||p6)){
this.SaveData(p5);
p5.f1=false;
}
if (p5!=null&&p5.f1&&p4==p5&&o7.tagName=="INPUT"&&(o7.type=="submit"||o7.type=="button"||o7.type=="image")){
this.SaveData(p5);
p5.f1=false;
}
if (p4!=null&&this.GetOperationMode(p4)=="ReadOnly")return ;
var o5=null;
if (p4==null){
if (p5==null)return ;
o5=this.GetTopSpread(p5);
this.SetActiveSpreadID(o5,"",null,false);
this.SetPageActiveSpread(null);
}else {
if (p4!=p5){
if (p5!=null){
o5=this.GetTopSpread(p5);
this.SetActiveSpreadID(o5,"",null,false);
}
if (p6){
o5=this.GetTopSpread(p4);
var k8=this.GetTopSpread(p5);
if (o5!=k8){
this.SetActiveSpreadID(o5,p4.id,p4.id,true);
this.SetPageActiveSpread(p4);
}else {
this.SetActiveSpreadID(o5,p5.id,p5.id,true);
this.SetPageActiveSpread(p5);
}
}else {
o5=this.GetTopSpread(p4);
this.SetPageActiveSpread(p4);
this.SetActiveSpreadID(o5,p4.id,p4.id,false);
}
}
}
}catch (h2){}
}
this.SetActiveSpreadID=function (f3,id,child,p6){
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
var g8=g7.getElementsByTagName("activespread")[0];
var p7=g7.getElementsByTagName("activechild")[0];
if (g8==null)return ;
if (p6&&p7!=null&&p7.nodeValue!=""){
g8.innerHTML=p7.innerHTML;
}else {
g8.innerHTML=id;
if (child!=null&&p7!=null)p7.innerHTML=child;
}
this.SaveData(f3);
f3.f1=false;
}
this.GetSpread=function (ele,incCmdBar){
var k1=ele;
while (k1!=null&&k1.tagName!="BODY"){
if (typeof(k1.getAttribute)!="function")break ;
var f3=k1.getAttribute("FpSpread");
if (f3==null)f3=k1.FpSpread;
if (f3=="Spread"){
if (!incCmdBar){
var f9=ele;
while (f9!=null&&f9!=k1){
if (f9.id==k1.id+"_commandBar"||f9.id==k1.id+"_pager1"||f9.id==k1.id+"_pager2")return null;
f9=f9.parentNode;
}
}
return k1;
}
k1=k1.parentNode;
}
return null;
}
this.ScrollViewport=function (event){
var f9=this.GetTarget(event);
var f3=this.GetTopSpread(f9);
if (f3!=null)this.ScrollView(f3);
}
this.GetActiveChildSheetView=function (f3){
var p5=this.GetPageActiveSheetView();
if (typeof(p5)=="undefined")return null;
var o5=this.GetTopSpread(f3);
var p8=this.GetTopSpread(p5);
if (p8!=o5)return null;
if (p5==p8)return null;
return p5;
}
this.ScrollTo=function (f3,j7,n8){
var i4=this.GetCellByRowCol(f3,j7,n8);
if (i4==null)return ;
var j5=this.GetViewport(f3).parentNode;
if (j5==null)return ;
j5.scrollTop=i4.offsetTop;
j5.scrollLeft=i4.offsetLeft;
}
this.ScrollView=function (f3){
var p4=this.GetTopSpread(f3);
var d4=this.GetParent(this.GetRowHeader(p4));
var d5=this.GetParent(this.GetColHeader(p4));
var l1=this.GetParent(this.GetColFooter(p4));
var j5=this.GetParent(this.GetViewport(p4));
var p9=(f3.oldScrollLeft!=j5.scrollLeft||f3.oldScrollTop!=j5.scrollTop);
f3.oldScrollLeft=j5.scrollLeft;
f3.oldScrollTop=j5.scrollTop;
if (d4!=null){
d4.scrollTop=j5.scrollTop;
}
if (d5!=null){
d5.scrollLeft=j5.scrollLeft;
}
if (l1!=null){
l1.scrollLeft=j5.scrollLeft;
}
if (this.GetParentSpread(f3)==null)this.SaveScrollbarState(f3,j5.scrollTop,j5.scrollLeft);
if (p9){
var h2=this.CreateEvent("Scroll");
this.FireEvent(f3,h2);
if (f3.frzRows!=0||f3.frzCols!=0)this.SyncMsgs(f3);
}
if (j5.scrollTop>0&&j5.scrollTop+j5.offsetHeight>=this.GetViewport(p4).offsetHeight){
if (!this.editing&&f3.getAttribute("loadOnDemand")=="true"){
if (f3.LoadState!=null)return ;
f3.LoadState=true;
this.SaveData(f3);
f3.CallBack("LoadOnDemand",true);
}
}
}
this.SaveScrollbarState=function (f3,scrollTop,scrollLeft){
if (this.GetParentSpread(f3)!=null)return ;
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
var q0=g7.getElementsByTagName("scrollTop")[0];
var q1=g7.getElementsByTagName("scrollLeft")[0];
if (f3.getAttribute("scrollContent")=="true")
if (q0!=null&&q1!=null)
if (q0.innerHTML!=scrollTop||q1.innerHTML!=scrollLeft)
this.ShowScrollingContent(f3,q0.innerHTML==scrollTop);
if (q0!=null)q0.innerHTML=scrollTop;
if (q1!=null)q1.innerHTML=scrollLeft;
}
this.LoadScrollbarState=function (f3){
return ;
if (this.GetParentSpread(f3)!=null)return ;
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
var q0=g7.getElementsByTagName("scrollTop")[0];
var q1=g7.getElementsByTagName("scrollLeft")[0];
var q2=0;
if (q0!=null&&q0.innerHTML!=""){
q2=parseInt(q0.innerHTML);
}else {
q2=0;
}
var q3=0;
if (q1!=null&&q1.innerHTML!=""){
q3=parseInt(q1.innerHTML);
}else {
q3=0;
}
var j5=this.GetParent(this.GetViewport(f3));
if (j5!=null){
if (!isNaN(q2))j5.scrollTop=q2;
if (!isNaN(q3))j5.scrollLeft=q3;
var d4=this.GetParent(this.GetRowHeader(f3));
var d5=this.GetParent(this.GetColHeader(f3));
var l1=this.GetParent(this.GetColFooter(f3));
if (l1!=null){
l1.scrollLeft=j5.scrollLeft;
}
if (d4!=null){
d4.scrollTop=j5.scrollTop;
}
if (d5!=null){
d5.scrollLeft=j5.scrollLeft;
}
}
}
this.GetParent=function (h2){
if (h2==null)
return null;
else 
return h2.parentNode;
}
this.GetViewport=function (f3){
return f3.d1;
}
this.GetCommandBar=function (f3){
return f3.d6;
}
this.GetRowHeader=function (f3){
return f3.d4;
}
this.GetColHeader=function (f3){
return f3.d5;
}
this.GetColFooter=function (f3){
return f3.colFooter;
}
this.GetFooterCorner=function (f3){
return f3.footerCorner;
}
this.GetCmdBtn=function (f3,id){
var p4=this.GetTopSpread(f3);
var q4=this.GetCommandBar(p4);
if (q4!=null)
return this.GetElementById(q4,p4.id+"_"+id);
else 
return null;
}
this.Range=function (){
this.type="Cell";
this.row=-1;
this.col=-1;
this.rowCount=0;
this.colCount=0;
}
this.SetRange=function (i9,type,j7,n8,f6,i1){
i9.type=type;
i9.row=j7;
i9.col=n8;
i9.rowCount=f6;
i9.colCount=i1;
if (type=="Row"){
i9.col=i9.colCount=-1;
}else if (type=="Column"){
i9.row=i9.rowCount=-1;
}else if (type=="Table"){
i9.col=i9.colCount=-1;i9.row=i9.rowCount=-1;
}
}
this.Margin=function (left,top,right,bottom){
this.left;
this.top;
this.right;
this.bottom;
}
this.GetRender=function (i4){
var f9=i4;
if (f9.firstChild!=null&&f9.firstChild.tagName!=null&&f9.firstChild.tagName!="BR")
return f9.firstChild;
if (f9.firstChild!=null&&f9.firstChild.value!=null){
f9=f9.firstChild;
}
return f9;
}
this.GetPreferredRowHeight=function (f3,f7){
var j8=this.CreateTestBox(f3);
f7=this.GetDisplayIndex(f3,f7);
var j5=this.GetViewport(f3);
var j9=0;
var q5=j5.rows[f7].offsetHeight;
var f4=j5.rows[f7].cells.length;
for (var f5=0;f5<f4;f5++){
var k0=j5.rows[f7].cells[f5];
var k2=this.GetRender(k0);
if (k2!=null){
j8.style.fontFamily=k2.style.fontFamily;
j8.style.fontSize=k2.style.fontSize;
j8.style.fontWeight=k2.style.fontWeight;
j8.style.fontStyle=k2.style.fontStyle;
}
var n8=this.GetColFromCell(f3,k0);
j8.style.posWidth=this.GetColWidthFromCol(f3,n8);
if (k2!=null&&k2.tagName=="SELECT"){
var f9="";
for (var j1=0;j1<k2.childNodes.length;j1++){
var q6=k2.childNodes[j1];
if (q6.text!=null&&q6.text.length>f9.length)f9=q6.text;
}
j8.innerHTML=f9;
}
else if (k2!=null&&k2.tagName=="INPUT")
j8.innerHTML=k2.value;
else 
{
j8.innerHTML=k0.innerHTML;
}
q5=j8.offsetHeight;
if (q5>j9)j9=q5;
}
return Math.max(0,j9)+3;
}
this.SetRowHeight2=function (f3,f7,height){
if (height<1){
height=1;
}
f7=this.GetDisplayIndex(f3,f7);
var c4=null;
if (this.GetRowHeader(f3)!=null)c4=this.GetRowHeader(f3).rows[f7];
if (c4!=null){
c4.style.posHeight=height;
c4.cells[0].style.posHeight=height;
}
var j5=this.GetViewport(f3);
if (c4!=null){
j5.rows[c4.rowIndex].cells[0].style.posHeight=c4.style.posHeight;
}else if (j5!=null){
j5.rows[f7].cells[0].style.posHeight=height;
c4=j5.rows[f7];
}
var q7=this.AddRowInfo(f3,c4.FpKey);
if (q7!=null){
this.SetRowHeight(f3,q7,c4.style.posHeight);
}
var j6=this.GetParentSpread(f3);
if (j6!=null)j6.UpdateRowHeight(f3);
this.SizeSpread(f3);
}
this.GetRowHeightInternal=function (f3,f7){
var c4=null;
if (this.GetRowHeader(f3)!=null)
c4=this.GetRowHeader(f3).rows[f7];
else if (this.GetViewport(f3)!=null)
c4=this.GetViewport(f3).rows[f7];
if (c4!=null)
return c4.offsetHeight;
else 
return 0;
}
this.GetCell=function (ele,noHeader,event){
var f9=ele;
while (f9!=null){
if (noHeader){
if ((f9.tagName=="TD"||f9.tagName=="TH")&&(f9.parentNode.getAttribute("FpSpread")=="r")){
return f9;
}
}else {
if ((f9.tagName=="TD"||f9.tagName=="TH")&&(f9.parentNode.getAttribute("FpSpread")=="r"||f9.parentNode.getAttribute("FpSpread")=="ch"||f9.parentNode.getAttribute("FpSpread")=="rh")){
return f9;
}
}
f9=f9.parentNode;
}
return null;
}
this.InRowHeader=function (f3,i4){
return (this.IsChild(i4,this.GetRowHeader(f3)));
}
this.InColHeader=function (f3,i4){
return (this.IsChild(i4,this.GetColHeader(f3)));
}
this.InColFooter=function (f3,i4){
return (this.IsChild(i4,this.GetColFooter(f3)));
}
this.IsHeaderCell=function (f3,i4){
return (i4!=null&&(i4.tagName=="TD"||i4.tagName=="TH")&&(i4.parentNode.getAttribute("FpSpread")=="ch"||i4.parentNode.getAttribute("FpSpread")=="rh"));
}
this.GetSizeColumn=function (f3,ele,event){
if (ele.tagName!="TD"||(this.GetColHeader(f3)==null))return null;
var n8=-1;
var f9=ele;
var q3=this.GetViewport(this.GetTopSpread(f3)).parentNode.scrollLeft+window.scrollX;
while (f9!=null&&f9.parentNode!=null&&f9.parentNode!=document.documentElement){
if (f9.parentNode.getAttribute("FpSpread")=="ch"){
var q8=this.GetOffsetLeft(f3,f9);
var q9=q8+f9.offsetWidth;
if (event.clientX+q3<q8+3){
n8=this.GetColFromCell(f3,f9)-1;
}
else if (event.clientX+q3>q9-4){
n8=this.GetColFromCell(f3,f9);
var r0=this.GetSpanCell(f9.parentNode.rowIndex,n8,f3.f0);
if (r0!=null){
n8=r0.col+r0.colCount-1;
}
}else {
n8=this.GetColFromCell(f3,f9);
var r0=this.GetSpanCell(f9.parentNode.rowIndex,n8,f3.f0);
if (r0!=null){
var k1=q8;
n8=-1;
for (var f5=r0.col;f5<r0.col+r0.colCount&&f5<this.GetColCount(f3);f5++){
if (this.IsChild(f9,this.GetColHeader(f3)))
k1+=parseInt(this.GetElementById(this.GetColHeader(f3),f3.id+"col"+f5).width);
if (event.clientX>k1-3&&event.clientX<k1+3){
n8=f5;
break ;
}
}
}else {
n8=-1;
}
}
if (isNaN(n8)||n8<0)return null;
var r1=0;
var r2=this.GetColCount(f3);
var r3=true;
var g0=null;
var i3=n8+1;
while (i3<r2){
var n7=this.GetColGroup(this.GetColHeader(f3));
if (i3<n7.childNodes.length)
r1=parseInt(n7.childNodes[i3].width);
if (r1>1){
r3=false;
break ;
}
i3++;
}
if (r3){
i3=n8+1;
while (i3<r2){
if (this.GetSizable(f3,i3)){
n8=i3;
break ;
}
i3++;
}
}
if (!this.GetSizable(f3,n8))return null;
if (this.IsChild(f9,this.GetColHeader(f3))){
return this.GetElementById(this.GetColHeader(f3),f3.id+"col"+n8);
}
}
f9=f9.parentNode;
}
return null;
}
this.GetColGroup=function (f9){
if (f9==null)return null;
var n7=f9.getElementsByTagName("COLGROUP");
if (n7!=null&&n7.length>0){
if (f9.colgroup!=null)return f9.colgroup;
var k8=new Object();
k8.childNodes=new Array();
for (var f5=0;f5<n7[0].childNodes.length;f5++){
if (n7[0].childNodes[f5]!=null&&n7[0].childNodes[f5].tagName=="COL"){
var f4=k8.childNodes.length;
k8.childNodes.length++;
k8.childNodes[f4]=n7[0].childNodes[f5];
}
}
f9.colgroup=k8;
return k8;
}else {
return null;
}
}
this.GetSizeRow=function (f3,ele,event){
var f6=this.GetRowCount(f3);
if (f6==0)return null;
var i4=this.GetCell(ele);
if (i4==null){
if (ele.getAttribute("FpSpread")=="rowpadding"){
if (event.clientY<3){
var f4=ele.parentNode.rowIndex;
if (f4>1){
var j7=ele.parentNode.parentNode.rows[f4-1];
if (this.GetSizable(f3,j7))
return j7;
}
}
}
var d3=this.GetCorner(f3);
if (d3!=null&&this.IsChild(ele,d3)){
if (event.clientY>ele.offsetHeight-4){
var r4=null;
var f4=0;
r4=this.GetRowHeader(f3);
if (r4!=null){
while (f4<r4.rows.length&&r4.rows[f4].offsetHeight<2&&!this.GetSizable(f3,r4.rows[f4]))
f4++;
if (f4<r4.rows.length&&this.GetSizable(f3,r4.rows[f4])&&r4.rows[f4].offsetHeight<2)
return r4.rows[f4];
}
}else {
}
}
return null;
}
var e9=f3.e9;
var e8=f3.e8;
var f9=i4;
var q2=this.GetViewport(this.GetTopSpread(f3)).parentNode.scrollTop+window.scrollY;
while (f9!=null&&f9!=document.documentElement){
if (f9.getAttribute("FpSpread")=="rh"){
var f4=-1;
var r5=this.GetOffsetTop(f3,f9);
var r6=r5+f9.offsetHeight;
if (event.clientY+q2<r5+3){
if (f9.rowIndex>1)
f4=f9.rowIndex-1;
}
else if (event.clientY+q2>r6-4){
var r0=this.GetSpanCell(this.GetRowFromCell(f3,i4),this.GetColFromCell(f3,i4),e9);
if (r0!=null){
var f8=r5;
for (var f5=r0.row;f5<r0.row+r0.rowCount;f5++){
if (this.GetRowHeader(f3).rows[f5].cells.length>0)
f8+=parseInt(this.GetRowHeader(f3).rows[f5].cells[0].style.height);
if (event.clientY>f8-3&&event.clientY<f8+3){
f4=f5;
break ;
}
}
}else {
if (f9.rowIndex>=0)f4=f9.rowIndex;
}
}
else {
break ;
}
var f8=0;
var f6=this.GetRowHeader(f3).rows.length;
var r7=true;
var r4=null;
r4=this.GetRowHeader(f3);
var f7=f4+1;
while (f7<f6){
if (r4.rows[f7].style.height!=null)f8=parseInt(r4.rows[f7].style.height);
else f8=parseInt(r4.rows[f7].offsetHeight);
if (f8>1){
r7=false;
break ;
}
f7++;
}
if (r7){
f7=f4+1;
while (f7<f6){
if (this.GetSizable(f3,this.GetRowHeader(f3).rows[f7])){
f4=f7;
break ;
}
f7++;
}
}
if (f4>=0&&this.GetSizable(f3,r4.rows[f4])){
return r4.rows[f4];
}
else if (event.clientY<3){
while (f4>0&&r4.rows[f4].offsetHeight==0&&!this.GetSizable(f3,r4.rows[f4]))
f4--;
if (f4>=0&&this.GetSizable(f3,r4.rows[f4]))
return r4.rows[f4];
else 
return null;
}
}
f9=f9.parentNode;
}
return null;
}
this.GetElementById=function (j6,id){
if (j6==null)return null;
var f9=j6.firstChild;
while (f9!=null){
if (f9.id==id||(typeof(f9.getAttribute)=="function"&&f9.getAttribute("name")==id))return f9;
var k8=this.GetElementById(f9,id)
if (k8!=null)return k8;
f9=f9.nextSibling;
}
return null;
}
this.GetSizable=function (f3,ele){
if (typeof(ele)=="number"){
var i4=this.GetElementById(this.GetColHeader(f3),f3.id+"col"+ele);
return (i4!=null&&(i4.getAttribute("Sizable")==null||i4.getAttribute("Sizable")=="True"));
}
return (ele!=null&&(ele.getAttribute("Sizable")==null||ele.getAttribute("Sizable")=="True"));
}
this.GetSpanWidth=function (f3,n8,r2){
var k1=0;
var g0=this.GetViewport(f3);
if (g0!=null){
var n7=this.GetColGroup(g0);
if (n7!=null){
for (var f5=n8;f5<n8+r2;f5++){
k1+=parseInt(n7.childNodes[f5].width);
}
}
}
return k1;
}
this.GetCellType=function (i4){
if (i4!=null&&i4.getAttribute("FpCellType")!=null)return i4.getAttribute("FpCellType");
if (i4!=null&&i4.getAttribute("FpRef")!=null){
var f9=document.getElementById(i4.getAttribute("FpRef"));
return f9.getAttribute("FpCellType");
}
if (i4!=null&&i4.getAttribute("FpCellType")!=null)return i4.getAttribute("FpCellType");
return "text";
}
this.GetCellType2=function (i4){
if (i4!=null&&i4.getAttribute("FpRef")!=null){
i4=document.getElementById(i4.getAttribute("FpRef"));
}
var k3=null;
if (i4!=null){
k3=i4.getAttribute("FpCellType");
if (k3=="readonly")k3=i4.getAttribute("CellType");
if (k3==null&&i4.getAttribute("CellType2")=="TagCloudCellType")
k3=i4.getAttribute("CellType2");
}
if (k3!=null)return k3;
return "text";
}
this.GetCellEditorID=function (f3,i4){
if (i4!=null&&i4.getAttribute("FpRef")!=null){
var f9=document.getElementById(i4.getAttribute("FpRef"));
return f9.getAttribute("FpEditorID");
}
if (i4.getAttribute("FpEditorID")!=null)
return i4.getAttribute("FpEditorID");
return f3.getAttribute("FpDefaultEditorID");
}
this.EditorMap=function (editorID,b7){
this.id=editorID;
this.b7=b7;
}
this.ValidatorMap=function (validatorID,validator){
this.id=validatorID;
this.validator=validator;
}
this.GetCellEditor=function (f3,editorID,noClone){
var b7=null;
for (var f5=0;f5<this.c9.length;f5++){
var r8=this.c9[f5];
if (r8.id==editorID){
b7=r8.b7;
break ;
}
}
if (b7==null){
b7=document.getElementById(editorID);
this.c9[this.c9.length]=new this.EditorMap(editorID,b7);
}
if (noClone)
return b7;
return b7.cloneNode(true);
}
this.GetCellValidatorID=function (f3,i4){
return null;
}
this.GetCellValidator=function (f3,validatorID){
return null;
}
this.GetTableRow=function (f3,f7){
var g7=this.GetData(f3).getElementsByTagName("root")[0];
var g6=g7.getElementsByTagName("data")[0];
var f9=g6.firstChild;
while (f9!=null){
if (f9.getAttribute("key")==""+f7)return f9;
f9=f9.nextSibling;
}
return null;
}
this.GetTableCell=function (j7,i3){
if (j7==null)return null;
var f9=j7.firstChild;
while (f9!=null){
if (f9.getAttribute("key")==""+i3)return f9;
f9=f9.nextSibling;
}
return null;
}
this.AddTableRow=function (f3,f7){
if (f7==null)return null;
var o0=this.GetTableRow(f3,f7);
if (o0!=null)return o0;
var g7=this.GetData(f3).getElementsByTagName("root")[0];
var g6=g7.getElementsByTagName("data")[0];
if (document.all!=null){
o0=this.GetData(f3).createNode("element","row","");
}else {
o0=document.createElement("row");
o0.style.display="none";
}
o0.setAttribute("key",f7);
g6.appendChild(o0);
return o0;
}
this.AddTableCell=function (j7,i3){
if (j7==null)return null;
var o0=this.GetTableCell(j7,i3);
if (o0!=null)return o0;
if (document.all!=null){
o0=this.GetData(f3).createNode("element","cell","");
}else {
o0=document.createElement("cell");
o0.style.display="none";
}
o0.setAttribute("key",i3);
j7.appendChild(o0);
return o0;
}
this.GetCellValue=function (f3,i4){
if (i4==null)return null;
var f7=this.GetRowKeyFromCell(f3,i4);
var i3=this.GetColKeyFromCell(f3,i4);
var r9=this.AddTableCell(this.AddTableRow(f3,f7),i3);
return r9.innerHTML;
}
this.HTMLEncode=function (s){
var s0=new String(s);
var s1=new RegExp("&","g");
s0=s0.replace(s1,"&amp;");
s1=new RegExp("<","g");
s0=s0.replace(s1,"&lt;");
s1=new RegExp(">","g");
s0=s0.replace(s1,"&gt;");
s1=new RegExp("\"","g");
s0=s0.replace(s1,"&quot;");
return s0;
}
this.HTMLDecode=function (s){
var s0=new String(s);
var s1=new RegExp("&amp;","g");
s0=s0.replace(s1,"&");
s1=new RegExp("&lt;","g");
s0=s0.replace(s1,"<");
s1=new RegExp("&gt;","g");
s0=s0.replace(s1,">");
s1=new RegExp("&quot;","g");
s0=s0.replace(s1,'"');
return s0;
}
this.SetCellValue=function (f3,i4,val,noEvent,recalc){
if (i4==null)return ;
var s2=this.GetCellType(i4);
if (s2=="readonly"&&i4.getAttribute("CellType2")!="MutuallyExclusiveCheckBoxCellType")return ;
var f7=this.GetRowKeyFromCell(f3,i4);
var i3=this.GetColKeyFromCell(f3,i4);
var r9=this.AddTableCell(this.AddTableRow(f3,f7),i3);
val=this.HTMLEncode(val);
val=this.HTMLEncode(val);
r9.innerHTML=val;
if (!noEvent){
var h2=this.CreateEvent("DataChanged");
h2.cell=i4;
h2.cellValue=val;
h2.row=f7;
h2.col=i3;
this.FireEvent(f3,h2);
}
var g9=this.GetCmdBtn(f3,"Update");
if (g9!=null&&g9.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(g9,false);
g9=this.GetCmdBtn(f3,"Cancel");
if (g9!=null&&g9.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(g9,false);
f3.f1=true;
if (recalc){
this.UpdateValues(f3);
}
}
this.GetSelectedRanges=function (f3){
var n9=this.GetSelection(f3);
var h5=new Array();
var o0=n9.firstChild;
while (o0!=null){
var i9=new this.Range();
this.GetRangeFromNode(f3,o0,i9);
var f9=h5.length;
h5.length=f9+1;
h5[f9]=i9;
o0=o0.nextSibling;
}
return h5;
}
this.GetSelectedRange=function (f3){
var i9=new this.Range();
var n9=this.GetSelection(f3);
var o0=n9.lastChild;
if (o0!=null){
this.GetRangeFromNode(f3,o0,i9);
}
return i9;
}
this.GetRangeFromNode=function (f3,o0,i9){
if (o0==null||f3==null||i9==null)return ;
var f7=this.GetRowByKey(f3,o0.getAttribute("row"));
var i3=this.GetColByKey(f3,o0.getAttribute("col"));
var f6=parseInt(o0.getAttribute("rowcount"));
var i1=parseInt(o0.getAttribute("colcount"));
var j5=this.GetViewport(f3);
if (j5!=null){
var s3=this.GetDisplayIndex(f3,f7);
for (var f5=s3;f5<s3+f6;f5++){
if (this.IsChildSpreadRow(f3,j5,f5))f6--;
}
}
var s4=null;
if (f7<0&&i3<0&&f6!=0&&i1!=0)
s4="Table";
else if (f7<0&&i3>=0&&i1>0)
s4="Column";
else if (i3<0&&f7>=0&&f6>0)
s4="Row";
else 
s4="Cell";
this.SetRange(i9,s4,f7,i3,f6,i1);
}
this.GetSelection=function (f3){
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
var o1=g7.getElementsByTagName("state")[0];
var s5=o1.getElementsByTagName("selection")[0];
return s5;
}
this.GetRowKeyFromRow=function (f3,f7){
if (f7<0)return null;
var g0=null;
g0=this.GetViewport(f3);
if (g0.rows[f7]==null)
return -1;
return g0.rows[f7].getAttribute("FpKey");
}
this.GetColKeyFromCol=function (f3,i3){
if (i3<0)return null;
var g0=this.GetViewport(f3);
var n7=this.GetColGroup(g0);
if (n7==null||n7.childNodes.length==0)
n7=this.GetColGroup(this.GetColHeader(f3));
if (n7!=null&&i3>=0&&i3<n7.childNodes.length){
return n7.childNodes[i3].getAttribute("FpCol");
}
return null;
}
this.GetRowKeyFromCell=function (f3,i4){
var f7=i4.parentNode.getAttribute("FpKey");
return f7;
}
this.GetColKeyFromCell=function (f3,i4){
var n8=this.GetColFromCell(f3,i4);
var g0=this.GetViewport(f3);
var n7=this.GetColGroup(g0);
if (n7!=null&&n8>=0&&n8<n7.childNodes.length){
return n7.childNodes[n8].getAttribute("FpCol");
}
return null;
}
this.SetSelection=function (f3,j7,n8,rowcount,colcount,addSelection){
if (!f3.initialized)return ;
var s6=j7;
var s7=n8;
if (j7!=null&&parseInt(j7)>=0){
j7=this.GetRowKeyFromRow(f3,j7);
if (j7!="newRow")
j7=parseInt(j7);
}
if (n8!=null&&parseInt(n8)>=0){
n8=parseInt(this.GetColKeyFromCol(f3,n8));
}
var o0=this.GetSelection(f3);
if (o0==null)return ;
if (addSelection==null)
addSelection=(f3.getAttribute("multiRange")=="true"&&!this.working);
var s8=o0.lastChild;
if (s8==null||addSelection){
if (document.all!=null){
s8=this.GetData(f3).createNode("element","range","");
}else {
s8=document.createElement('range');
s8.style.display="none";
}
o0.appendChild(s8);
}
s8.setAttribute("row",j7);
s8.setAttribute("col",n8);
s8.setAttribute("rowcount",rowcount);
s8.setAttribute("colcount",colcount);
s8.setAttribute("rowIndex",s6);
s8.setAttribute("colIndex",s7);
f3.f1=true;
this.PaintFocusRect(f3);
var g9=this.GetCmdBtn(f3,"Update");
this.UpdateCmdBtnState(g9,false);
var h2=this.CreateEvent("SelectionChanged");
this.FireEvent(f3,h2);
}
this.CreateSelectionNode=function (f3,j7,n8,rowcount,colcount,s6,s7){
var s8=document.createElement('range');
s8.style.display="none";
s8.setAttribute("row",j7);
s8.setAttribute("col",n8);
s8.setAttribute("rowcount",rowcount);
s8.setAttribute("colcount",colcount);
s8.setAttribute("rowIndex",s6);
s8.setAttribute("colIndex",s7);
return s8;
}
this.AddRowToSelection=function (f3,o0,j7){
var s6=j7;
if (typeof(j7)!="undefined"&&parseInt(j7)>=0){
j7=this.GetRowKeyFromRow(f3,j7);
if (j7!="newRow")
j7=parseInt(j7);
}
if (!this.IsRowSelected(f3,j7)&&!isNaN(j7))
{
var s8=this.CreateSelectionNode(f3,j7,-1,1,-1,s6,-1);
o0.appendChild(s8);
}
}
this.RemoveSelection=function (f3,j7,n8,rowcount,colcount){
var o0=this.GetSelection(f3);
if (o0==null)return ;
var s8=o0.firstChild;
while (s8!=null){
var f7=parseInt(s8.getAttribute("rowIndex"));
var f6=parseInt(s8.getAttribute("rowcount"));
if (f7<=j7&&j7<f7+f6){
o0.removeChild(s8);
for (var f5=f7;f5<f7+f6;f5++){
if (f5!=j7){
this.AddRowToSelection(f3,o0,f5);
}
}
break ;
}
s8=s8.nextSibling;
}
f3.f1=true;
var g9=this.GetCmdBtn(f3,"Update");
this.UpdateCmdBtnState(g9,false);
var h2=this.CreateEvent("SelectionChanged");
this.FireEvent(f3,h2);
}
this.GetColInfo=function (f3,i3){
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
var o1=g7.getElementsByTagName("state")[0];
var n8=o1.getElementsByTagName("colinfo")[0];
var f9=n8.firstChild;
while (f9!=null){
if (f9.getAttribute("key")==""+i3)return f9;
f9=f9.nextSibling;
}
return null;
}
this.GetColWidthFromCol=function (f3,i3){
var n7=this.GetColGroup(this.GetViewport(f3));
return parseInt(n7.childNodes[i3].width);
}
this.GetColWidth=function (colInfo){
if (colInfo==null)return null;
var o0=colInfo.getElementsByTagName("width")[0];
if (o0!=null)return o0.innerHTML;
return 0;
}
this.AddColInfo=function (f3,i3){
var o0=this.GetColInfo(f3,i3);
if (o0!=null)return o0;
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
var o1=g7.getElementsByTagName("state")[0];
var n8=o1.getElementsByTagName("colinfo")[0];
if (document.all!=null){
o0=this.GetData(f3).createNode("element","col","");
}else {
o0=document.createElement('col');
o0.style.display="none";
}
o0.setAttribute("key",i3);
n8.appendChild(o0);
return o0;
}
this.SetColWidth=function (f3,n8,width){
if (n8==null)return ;
n8=parseInt(n8);
var k9=this.IsXHTML(f3);
var s9=null;
if (this.GetViewport(f3)!=null){
var n7=this.GetColGroup(this.GetViewport(f3));
if (n7==null||n7.childNodes.length==0){
n7=this.GetColGroup(this.GetColHeader(f3));
}
s9=this.AddColInfo(f3,n7.childNodes[n8].getAttribute("FpCol"));
if (this.GetViewport(f3).cellSpacing=="0"&&this.GetColCount(f3)>1&&this.GetViewport(f3).rules!="rows"){
if (n8==0)width-=1;
}
if (width==0)width=1;
if (n7!=null)
n7.childNodes[n8].width=width;
this.SetWidthFix(this.GetViewport(f3),n8,width);
}
if (this.GetColHeader(f3)!=null){
if (this.GetViewport(f3)!=null){
if (this.GetViewport(f3).cellSpacing=="0"&&this.GetColCount(f3)>1&&this.GetViewport(f3).rules!="rows"){
if (k9){
if (n8==this.colCount-1)width-=1;
}
}
}
if (width<=0)width=1;
document.getElementById(f3.id+"col"+n8).width=width;
this.SetWidthFix(this.GetColHeader(f3),n8,width);
if (this.GetViewport(f3)!=null){
if (this.GetViewport(f3).cellSpacing=="0"&&this.GetColCount(f3)>1&&this.GetViewport(f3).rules!="rows"){
if (n8==this.GetColCount(f3)-1)width+=1;
}
}
}
if (this.GetColFooter(f3)!=null){
var n7=this.GetColGroup(this.GetColFooter(f3));
if (n7==null||n7.childNodes.length==0){
n7=this.GetColGroup(this.GetColHeader(f3));
}
s9=this.AddColInfo(f3,n7.childNodes[n8].getAttribute("FpCol"));
if (this.GetColFooter(f3).cellSpacing=="0"&&this.GetColCount(f3)>1&&this.GetColFooter(f3).rules!="rows"){
if (n8==0)width-=1;
}
if (width==0)width=1;
if (n7!=null)
n7.childNodes[n8].width=width;
this.SetWidthFix(this.GetColFooter(f3),n8,width);
}
var g2=this.GetTopSpread(f3);
this.SizeAll(g2);
this.Refresh(g2);
if (s9!=null){
var o0=s9.getElementsByTagName("width");
if (o0!=null&&o0.length>0)
o0[0].innerHTML=width;
else {
if (document.all!=null){
o0=this.GetData(f3).createNode("element","width","");
}else {
o0=document.createElement('width');
o0.style.display="none";
}
s9.appendChild(o0);
o0.innerHTML=width;
}
}
var g9=this.GetCmdBtn(f3,"Update");
if (g9!=null)this.UpdateCmdBtnState(g9,false);
f3.f1=true;
}
this.SetWidthFix=function (g0,n8,width){
if (g0==null||g0.rows.length==0)return ;
var f5=0;
var t0=0;
var k0=g0.rows[0].cells[0];
var t1=k0.colSpan;
if (t1==null)t1=1;
while (n8>t0+t1){
f5++;
t0=t0+t1;
k0=g0.rows[0].cells[f5];
t1=k0.colSpan;
if (t1==null)t1=1;
}
k0.width=width;
}
this.GetRowInfo=function (f3,f7){
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
var o1=g7.getElementsByTagName("state")[0];
var j7=o1.getElementsByTagName("rowinfo")[0];
var f9=j7.firstChild;
while (f9!=null){
if (f9.getAttribute("key")==""+f7)return f9;
f9=f9.nextSibling;
}
return null;
}
this.GetRowHeight=function (q7){
if (q7==null)return null;
var t2=q7.getElementsByTagName("height");
if (t2!=null&&t2.length>0)return t2[0].innerHTML;
return 0;
}
this.AddRowInfo=function (f3,f7){
var o0=this.GetRowInfo(f3,f7);
if (o0!=null)return o0;
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
var o1=g7.getElementsByTagName("state")[0];
var j7=o1.getElementsByTagName("rowinfo")[0];
if (document.all!=null){
o0=this.GetData(f3).createNode("element","row","");
}else {
o0=document.createElement('row');
o0.style.display="none";
}
o0.setAttribute("key",f7);
j7.appendChild(o0);
return o0;
}
this.GetTopSpread=function (h2)
{
if (h2==null)return null;
var h5=this.GetSpread(h2);
if (h5==null)return null;
var f9=h5.parentNode;
while (f9!=null&&f9.tagName!="BODY")
{
if (f9.getAttribute&&f9.getAttribute("FpSpread")=="Spread"){
if (f9.getAttribute("hierView")=="true")
h5=f9;
else 
break ;
}
f9=f9.parentNode;
}
return h5;
}
this.GetParentSpread=function (f3)
{
try {
var f9=f3.parentNode;
while (f9!=null&&f9.getAttribute&&f9.getAttribute("FpSpread")!="Spread")f9=f9.parentNode;
if (f9!=null&&f9.getAttribute&&f9.getAttribute("hierView")=="true")
return f9;
else 
return null;
}catch (h2){
return null;
}
}
this.SetRowHeight=function (f3,q7,height){
if (q7==null)return ;
var o0=q7.getElementsByTagName("height");
if (o0!=null&&o0.length>0)
o0[0].innerHTML=height;
else {
if (document.all!=null){
o0=this.GetData(f3).createNode("element","height","");
}else {
o0=document.createElement('height');
o0.style.display="none";
}
q7.appendChild(o0);
o0.innerHTML=height;
}
var g9=this.GetCmdBtn(f3,"Update");
if (g9!=null)this.UpdateCmdBtnState(g9,false);
f3.f1=true;
}
this.SetActiveRow=function (f3,j7){
if (this.GetRowCount(f3)<1)return ;
if (j7==null)j7=-1;
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
var o1=g7.getElementsByTagName("state")[0];
var o2=o1.firstChild;
while (o2!=null&&o2.tagName!="activerow"&&o2.tagName!="ACTIVEROW"){
o2=o2.nextSibling;
}
if (o2!=null)
o2.innerHTML=""+j7;
if (j7!=null&&f3.getAttribute("IsNewRow")!="true"&&f3.getAttribute("AllowInsert")=="true"){
var g9=this.GetCmdBtn(f3,"Insert");
this.UpdateCmdBtnState(g9,false);
g9=this.GetCmdBtn(f3,"Add");
this.UpdateCmdBtnState(g9,false);
}else {
var g9=this.GetCmdBtn(f3,"Insert");
this.UpdateCmdBtnState(g9,true);
g9=this.GetCmdBtn(f3,"Add");
this.UpdateCmdBtnState(g9,true);
}
if (j7!=null&&f3.getAttribute("IsNewRow")!="true"&&(f3.getAttribute("AllowDelete")==null||f3.getAttribute("AllowDelete")=="true")){
var g9=this.GetCmdBtn(f3,"Delete");
this.UpdateCmdBtnState(g9,(j7==-1));
}else {
var g9=this.GetCmdBtn(f3,"Delete");
this.UpdateCmdBtnState(g9,true);
}
f3.f1=true;
}
this.SetActiveCol=function (f3,n8){
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
var o1=g7.getElementsByTagName("state")[0];
var o3=o1.firstChild;
while (o3!=null&&o3.tagName!="activecolumn"&&o3.tagName!="ACTIVECOLUMN"){
o3=o3.nextSibling;
}
if (o3!=null)
o3.innerHTML=""+parseInt(n8);
f3.f1=true;
}
this.GetEditor=function (i4){
if (i4==null)return null;
var s2=this.GetCellType(i4);
if (s2=="readonly")return null;
var j4=i4.getElementsByTagName("DIV");
if (s2=="MultiColumnComboBoxCellType"){
if (j4!=null&&j4.length>0){
var f9=j4[0];
f9.type="div";
return f9;
}
}
var j4=i4.getElementsByTagName("INPUT");
if (j4!=null&&j4.length>0){
var f9=j4[0];
while (f9!=null&&f9.getAttribute&&f9.getAttribute("FpEditor")==null)
f9=f9.parentNode;
if (!f9.getAttribute)f9=null;
return f9;
}
j4=i4.getElementsByTagName("SELECT");
if (j4!=null&&j4.length>0){
var f9=j4[0];
return f9;
}
return null;
}
this.GetPageActiveSpread=function (){
var t3=document.documentElement.getAttribute("FpActiveSpread");
var f9=null;
if (t3!=null)f9=document.getElementById(t3);
return f9;
}
this.GetPageActiveSheetView=function (){
var t3=document.documentElement.getAttribute("FpActiveSheetView");
var f9=null;
if (t3!=null)f9=document.getElementById(t3);
return f9;
}
this.SetPageActiveSpread=function (f3){
if (f3==null)
document.documentElement.setAttribute("FpActiveSpread",null);
else {
document.documentElement.setAttribute("FpActiveSpread",f3.id);
document.documentElement.setAttribute("FpActiveSheetView",f3.id);
}
}
this.DoResize=function (event){
if (the_fpSpread.spreads==null)return ;
var f4=the_fpSpread.spreads.length;
for (var f5=0;f5<f4;f5++){
if (the_fpSpread.spreads[f5]!=null)the_fpSpread.SizeSpread(the_fpSpread.spreads[f5]);
}
}
this.DblClick=function (event){
var i4=this.GetCell(this.GetTarget(event),true,event);
var f3=this.GetSpread(i4);
if (i4!=null&&!this.IsHeaderCell(i4)&&this.GetOperationMode(f3)=="RowMode"&&this.GetEnableRowEditTemplate(f3)=="true"){
var t4=i4.getElementsByTagName("DIV");
if (t4!=null&&t4.length>0&&t4[0].id==f3.id+"_RowEditTemplateContainer")return ;
this.Edit(f3,this.GetRowKeyFromCell(f3,i4));
var g9=this.GetCmdBtn(f3,"Cancel");
if (g9!=null)
this.UpdateCmdBtnState(g9,false);
return ;
}
if (i4!=null&&!this.IsHeaderCell(i4)&&i4==f3.e0)this.StartEdit(f3,i4);
}
this.GetEvent=function (h2){
if (h2!=null)return h2;
return window.event;
}
this.GetTarget=function (h2){
h2=this.GetEvent(h2);
if (h2.target==document){
if (h2.currentTarget!=null)return h2.currentTarget;
}
if (h2.target!=null)return h2.target;
return h2.srcElement;
}
this.StartEdit=function (f3,editCell){
var t5=this.GetOperationMode(f3);
if (t5=="SingleSelect"||t5=="ReadOnly"||this.editing)return ;
if (t5=="RowMode"&&this.GetEnableRowEditTemplate(f3)=="true")return ;
var i4=editCell;
if (i4==null)i4=f3.e0;
if (i4==null)return ;
if (this.IsHeaderCell(f3,i4))return ;
this.renderAsEditor=-1;
var j4=this.GetEditor(i4);
if (j4!=null){
this.editing=true;
this.b7=j4;
this.renderAsEditor=1;
}
var k9=this.IsXHTML(f3);
if (i4!=null){
var f7=this.GetRowFromCell(f3,i4);
var i3=this.GetColFromCell(f3,i4);
var h2=this.CreateEvent("EditStart");
h2.cell=i4;
h2.row=this.GetSheetIndex(f3,f7);
h2.col=i3;
h2.cancel=false;
this.FireEvent(f3,h2);
if (h2.cancel)return ;
var s2=this.GetCellType(i4);
if (s2=="readonly")return ;
if (f3.e0!=i4){
f3.e0=i4;
this.SetActiveRow(f3,this.GetRowKeyFromCell(f3,i4));
this.SetActiveCol(f3,this.GetColKeyFromCell(f3,i4));
}
if (j4==null){
var k2=this.GetRender(i4);
var t6=this.GetValueFromRender(f3,k2);
if (t6==" ")t6="";
this.b8=t6;
this.b9=this.GetFormulaFromCell(i4);
if (this.b9!=null)t6=this.b9;
try {
if (k2!=i4){
k2.style.display="none";
}
else {
k2.innerHTML="";
}
}catch (h2){
return ;
}
var t7=this.GetCellEditorID(f3,i4);
if (t7!=null&&t7.length>0){
this.b7=this.GetCellEditor(f3,t7,true);
if (!this.b7.getAttribute("MccbId")&&!this.b7.getAttribute("Extenders"))
this.b7.style.display="inline";
else 
this.b7.style.display="block";
this.b7.id=t7+"Editor";
}else {
this.b7=document.createElement("INPUT");
this.b7.type="text";
}
this.b7.style.fontFamily=k2.style.fontFamily;
this.b7.style.fontSize=k2.style.fontSize;
this.b7.style.fontWeight=k2.style.fontWeight;
this.b7.style.fontStyle=k2.style.fontStyle;
this.b7.style.textDecoration=k2.style.textDecoration;
this.b7.style.position="";
if (k9){
var t8=i4.clientWidth-2;
var t9=parseInt(i4.style.paddingLeft);
if (!isNaN(t9))
t8-=t9;
t9=parseInt(i4.style.paddingRight);
if (!isNaN(t9))
t8-=t9;
this.b7.style.width=""+t8+"px";
}
else 
this.b7.style.width=i4.clientWidth-2;
this.SaveMargin(i4);
if (this.b7.tagName=="TEXTAREA")
this.b7.style.height=""+(i4.offsetHeight-4)+"px";
if ((this.b7.tagName=="INPUT"&&this.b7.type=="text")||this.b7.tagName=="TEXTAREA"){
if (this.b7.style.backgroundColor==""||this.b7.backColorSet!=null){
var u0="";
if (document.defaultView!=null&&document.defaultView.getComputedStyle!=null)u0=document.defaultView.getComputedStyle(i4,'').getPropertyValue("background-color");
if (u0!="")
this.b7.style.backgroundColor=u0;
else 
this.b7.style.backgroundColor=i4.bgColor;
this.b7.backColorSet=true;
}
if (this.b7.style.color==""||this.b7.colorSet!=null){
var u1="";
if (document.defaultView!=null&&document.defaultView.getComputedStyle!=null)u1=document.defaultView.getComputedStyle(i4,'').getPropertyValue("color");
this.b7.style.color=u1;
this.b7.colorSet=true;
}
this.b7.style.borderWidth="0px";
this.RestoreMargin(this.b7,false);
}
this.renderAsEditor=0;
i4.insertBefore(this.b7,i4.firstChild);
this.SetEditorValue(this.b7,t6,f3);
if (this.b7.offsetHeight<i4.clientHeight&&this.b7.tagName!="TEXTAREA"){
if (i4.vAlign=="middle")
this.b7.style.posTop+=(i4.clientHeight-this.b7.offsetHeight)/2;
else if (i4.vAlign=="bottom")
this.b7.style.posTop+=(i4.clientHeight-this.b7.offsetHeight);
}
this.SizeAll(this.GetTopSpread(f3));
}
this.SetEditorFocus(this.b7);
if (f3.getAttribute("EditMode")=="replace"){
if ((this.b7.tagName=="INPUT"&&this.b7.type=="text")||this.b7.tagName=="TEXTAREA")
this.b7.select();
}
this.editing=true;
var g9=this.GetCmdBtn(f3,"Update");
if (g9!=null&&g9.disabled)
this.UpdateCmdBtnState(g9,false);
g9=this.GetCmdBtn(f3,"Copy");
if (g9!=null&&!g9.disabled)
this.UpdateCmdBtnState(g9,true);
g9=this.GetCmdBtn(f3,"Paste");
if (g9!=null&&!g9.disabled)
this.UpdateCmdBtnState(g9,true);
g9=this.GetCmdBtn(f3,"Clear");
if (g9!=null&&!g9.disabled)
this.UpdateCmdBtnState(g9,true);
}
this.ScrollView(f3);
}
this.GetCurrency=function (validator){
var u2=validator.CurrencySymbol;
if (u2!=null)return u2;
var f9=document.getElementById(validator.id+"cs");
if (f9!=null){
return f9.innerText;
}
return "";
}
this.GetValueFromRender=function (f3,rd){
var k3=this.GetCellType2(this.GetCell(rd));
if (k3!=null){
if (k3=="text")k3="TextCellType";
var j3=null;
if (k3=="ExtenderCellType"){
j3=this.GetFunction(k3+"_getEditor")
if (j3!=null){
if (j3(rd)!=null)
j3=this.GetFunction(k3+"_getEditorValue");
else 
j3=null;
}
}else 
j3=this.GetFunction(k3+"_getValue");
if (j3!=null){
return j3(rd,f3);
}
}
var f9=rd;
while (f9.firstChild!=null&&f9.firstChild.nodeName!="#text")f9=f9.firstChild;
if (f9.innerHTML=="&nbsp;")return "";
var t6=f9.value;
if ((typeof(t6)=="undefined")&&k3=="readonly"&&f9.parentNode!=null&&f9.parentNode.getAttribute("CellType2")=="TagCloudCellType")
t6=f9.textContent;
if (t6==null){
t6=this.ReplaceAll(f9.innerHTML,"&nbsp;"," ");
t6=this.ReplaceAll(t6,"<br>"," ");
t6=this.HTMLDecode(t6);
}
return t6;
}
this.ReplaceAll=function (val,src,dest){
if (val==null)return val;
var u3=val.length;
while (true){
val=val.replace(src,dest);
if (val.length==u3)break ;
u3=val.length;
}
return val;
}
this.GetFormula=function (f3,f7,i3){
f7=this.GetDisplayIndex(f3,f7);
var i4=this.GetCellFromRowCol(f3,f7,i3);
var u4=this.GetFormulaFromCell(i4);
return u4;
}
this.SetFormula=function (f3,f7,i3,j3,recalc,clientOnly){
f7=this.GetDisplayIndex(f3,f7);
var i4=this.GetCellFromRowCol(f3,f7,i3);
i4.setAttribute("FpFormula",j3);
if (!clientOnly)
this.SetCellValue(f3,i4,j3,null,recalc);
}
this.GetFormulaFromCell=function (rd){
var t6=null;
if (rd.getAttribute("FpFormula")!=null){
t6=rd.getAttribute("FpFormula");
}
if (t6!=null)
t6=this.Trim(new String(t6));
return t6;
}
this.IsDouble=function (val,decimalchar,negsign,possign,minimumvalue,maximumvalue){
if (val==null||val.length==0)return true;
val=the_fpSpread.Trim(val);
if (val.length==0)return true;
if (negsign!=null)val=val.replace(negsign,"-");
if (possign!=null)val=val.replace(possign,"+");
if (val.charAt(val.length-1)=="-")val="-"+val.substring(0,val.length-1);
var u5=new RegExp("^\\s*([-\\+])?(\\d+)?(\\"+decimalchar+"(\\d+))?([eE]([-\\+])?(\\d+))?\\s*$");
var u6=val.match(u5);
if (u6==null)
return false;
if ((u6[2]==null||u6[2].length==0)&&(u6[4]==null||u6[4].length==0))return false;
var u7="";
if (u6[1]!=null&&u6[1].length>0)u7=u6[1];
if (u6[2]!=null&&u6[2].length>0)
u7+=u6[2];
else 
u7+="0";
if (u6[4]!=null&&u6[4].length>0)
u7+=("."+u6[4]);
if (u6[6]!=null&&u6[6].length>0){
u7+=("E"+u6[6]);
if (u6[7]!=null)
u7+=(u6[7]);
else 
u7+="0";
}
var u8=parseFloat(u7);
if (isNaN(u8))return false;
var f9=true;
if (minimumvalue!=null){
var u9=parseFloat(minimumvalue);
f9=(!isNaN(u9)&&u8>=u9);
}
if (f9&&maximumvalue!=null){
var j9=parseFloat(maximumvalue);
f9=(!isNaN(j9)&&u8<=j9);
}
return f9;
}
this.GetFunction=function (fn){
if (fn==null||fn=="")return null;
try {
var f9=eval(fn);
return f9;
}catch (h2){
return null;
}
}
this.SetValueToRender=function (rd,val,valueonly){
var j3=null;
var k3=this.GetCellType2(this.GetCell(rd));
if (k3!=null){
if (k3=="text")k3="TextCellType";
if (k3=="ExtenderCellType"){
j3=this.GetFunction(k3+"_getEditor")
if (j3!=null){
if (j3(rd)!=null)
j3=this.GetFunction(k3+"_setEditorValue");
else 
j3=null;
}
}else 
j3=this.GetFunction(k3+"_setValue");
}
if (j3!=null){
j3(rd,val);
}else {
if (typeof(rd.value)!="undefined"){
if (val==null)val="";
rd.value=val;
}else {
var f9=rd;
while (f9.firstChild!=null&&f9.firstChild.nodeName!="#text")f9=f9.firstChild;
f9.innerHTML=this.ReplaceAll(val," ","&nbsp;");
}
}
if ((valueonly==null||!valueonly)&&rd.getAttribute("FpFormula")!=null){
rd.setAttribute("FpFormula",val);
}
}
this.Trim=function (s3){
var u6=s3.match(new RegExp("^\\s*(\\S+(\\s+\\S+)*)\\s*$"));
return (u6==null)?"":u6[1];
}
this.GetOffsetLeft=function (f3,i4,j6){
var g0=j6;
if (g0==null)g0=this.GetViewportFromCell(f3,i4);
var q8=0;
var f9=i4;
while (f9!=null&&f9!=g0){
q8+=f9.offsetLeft;
f9=f9.offsetParent;
}
return q8;
}
this.GetOffsetTop=function (f3,i4,j6){
var g0=j6;
if (g0==null)g0=this.GetViewportFromCell(f3,i4);
var v0=0;
var f9=i4;
while (f9!=null&&f9!=g0){
v0+=f9.offsetTop;
f9=f9.offsetParent;
}
return v0;
}
this.SetEditorFocus=function (f9){
if (f9==null)return ;
var v1=true;
var i4=this.GetCell(f9,true);
var k3=this.GetCellType(i4);
if (k3!=null){
var j3=this.GetFunction(k3+"_setFocus");
if (j3!=null){
j3(f9);
v1=false;
}
}
if (v1){
try {
f9.focus();
}catch (h2){}
}
}
this.SetEditorValue=function (f9,val,f3){
var i4=this.GetCell(f9,true);
var k3=this.GetCellType(i4);
if (k3!=null){
var j3=this.GetFunction(k3+"_setEditorValue");
if (j3!=null){
j3(f9,val,f3);
return ;
}
}
k3=f9.getAttribute("FpEditor");
if (k3!=null){
var j3=this.GetFunction(k3+"_setEditorValue");
if (j3!=null){
j3(f9,val,f3);
return ;
}
}
f9.value=val;
}
this.GetEditorValue=function (f9){
var i4=this.GetCell(f9,true);
var k3=this.GetCellType(i4);
if (k3!=null){
var j3=this.GetFunction(k3+"_getEditorValue");
if (j3!=null){
return j3(f9);
}
}
k3=f9.getAttribute("FpEditor");
if (k3!=null){
var j3=this.GetFunction(k3+"_getEditorValue");
if (j3!=null){
return j3(f9);
}
}
if (f9.type=="checkbox"){
if (f9.checked)
return "True";
else 
return "False";
}
else 
{
return f9.value;
}
}
this.CreateMsg=function (){
if (this.validationMsg!=null)return ;
var f9=this.validationMsg=document.createElement("div");
f9.style.position="absolute";
f9.style.background="yellow";
f9.style.color="red";
f9.style.border="1px solid black";
f9.style.display="none";
f9.style.width="120px";
}
this.SetMsg=function (msg){
this.CreateMsg();
this.validationMsg.innerHTML=msg;
}
this.ShowMsg=function (show){
this.CreateMsg();
if (show){
this.validationMsg.style.display="block";
}
else 
this.validationMsg.style.display="none";
}
this.EndEdit=function (){
if (this.b7!=null&&this.b7.parentNode!=null){
var i4=this.GetCell(this.b7.parentNode);
var f3=this.GetSpread(i4,false);
if (f3==null)return true;
var v2=this.GetEditorValue(this.b7);
var v3=v2;
if (typeof(v2)=="string")v3=this.Trim(v2);
var v4=(f3.getAttribute("AcceptFormula")=="true"&&v3!=null&&v3.charAt(0)=='=');
var j4=(this.renderAsEditor!=0);
if (!v4&&!j4){
var v5=null;
var k3=this.GetCellType(i4);
if (k3!=null){
var j3=this.GetFunction(k3+"_isValid");
if (j3!=null){
v5=j3(i4,v2);
}
}
if (v5!=null&&v5!=""){
this.SetMsg(v5);
this.GetViewport(f3).parentNode.insertBefore(this.validationMsg,this.GetViewport(f3).parentNode.firstChild);
this.ShowMsg(true);
this.SetValidatorPos(f3);
this.b7.focus();
return false;
}else {
this.ShowMsg(false);
}
}
if (!j4){
i4.removeChild(this.b7);
var v6=this.GetRender(i4);
if (v6.style.display=="none")v6.style.display="block";
if (this.b9!=null&&this.b9==v2){
this.SetValueToRender(v6,this.b8,true);
}else {
this.SetValueToRender(v6,v2);
}
this.RestoreMargin(i4);
}
if ((this.b9!=null&&this.b9!=v2)||(this.b9==null&&this.b8!=v2)){
this.SetCellValue(f3,i4,v2);
if (v2!=null&&v2.length>0&&v2.indexOf("=")==0)i4.setAttribute("FpFormula",v2);
}
if (!j4)
this.SizeAll(this.GetTopSpread(f3));
this.b7=null;
this.editing=false;
var h2=this.CreateEvent("EditStopped");
h2.cell=i4;
this.FireEvent(f3,h2);
this.Focus(f3);
var v7=f3.getAttribute("autoCalc");
if (v7!="false"){
if ((this.b9!=null&&this.b9!=v2)||(this.b9==null&&this.b8!=v2)){
this.UpdateValues(f3);
}
}
}
this.renderAsEditor=-1;
return true;
}
this.SetValidatorPos=function (f3){
if (this.b7==null)return ;
var i4=this.GetCell(this.b7.parentNode);
if (i4==null)return ;
var f9=this.validationMsg;
if (f9!=null&&f9.style.display!="none"){
if (f9!=null){
f9.style.left=""+(i4.offsetLeft)+"px";
f9.style.top=""+(i4.offsetTop+i4.offsetHeight)+"px";
}
}
}
this.SaveMargin=function (editCell){
if (editCell.style.paddingLeft!=null&&editCell.style.paddingLeft!=""){
this.c2.left=editCell.style.paddingLeft;
editCell.style.paddingLeft=0;
}
if (editCell.style.paddingRight!=null&&editCell.style.paddingRight!=""){
this.c2.right=editCell.style.paddingRight;
editCell.style.paddingRight=0;
}
if (editCell.style.paddingTop!=null&&editCell.style.paddingTop!=""){
this.c2.top=editCell.style.paddingTop;
editCell.style.paddingTop=0;
}
if (editCell.style.paddingBottom!=null&&editCell.style.paddingBottom!=""){
this.c2.bottom=editCell.style.paddingBottom;
editCell.style.paddingBottom=0;
}
}
this.RestoreMargin=function (i4,reset){
if (this.c2.left!=null&&this.c2.left!=-1){
i4.style.paddingLeft=this.c2.left;
if (reset==null||reset)this.c2.left=-1;
}
if (this.c2.right!=null&&this.c2.right!=-1){
i4.style.paddingRight=this.c2.right;
if (reset==null||reset)this.c2.right=-1;
}
if (this.c2.top!=null&&this.c2.top!=-1){
i4.style.paddingTop=this.c2.top;
if (reset==null||reset)this.c2.top=-1;
}
if (this.c2.bottom!=null&&this.c2.bottom!=-1){
i4.style.paddingBottom=this.c2.bottom;
if (reset==null||reset)this.c2.bottom=-1;
}
}
this.PaintSelectedCell=function (f3,i4,select,anchor){
if (i4==null)return ;
var v8=anchor?f3.getAttribute("anchorBackColor"):f3.getAttribute("selectedBackColor");
if (select){
if (i4.getAttribute("bgColorBak")==null)
i4.setAttribute("bgColorBak",document.defaultView.getComputedStyle(i4,"").getPropertyValue("background-color"));
if (i4.bgColor1==null)
i4.bgColor1=i4.style.backgroundColor;
i4.style.backgroundColor=v8;
if (i4.getAttribute("bgSelImg"))
i4.style.backgroundImage=i4.getAttribute("bgSelImg");
}else {
if (i4.bgColor1!=null)
i4.style.backgroundColor="";
if (i4.bgColor1!=null&&i4.bgColor1!="")
i4.style.backgroundColor=i4.bgColor1;
i4.style.backgroundImage="";
if (i4.getAttribute("bgImg")!=null)
i4.style.backgroundImage=i4.getAttribute("bgImg");
}
}
this.PaintAnchorCell=function (f3){
var v9=this.GetOperationMode(f3);
if (f3.e0==null||(v9!="Normal"&&v9!="RowMode"))return ;
if (v9=="MultiSelect"||v9=="ExtendedSelect")return ;
if (!this.IsChild(f3.e0,f3))return ;
var w0=(this.GetTopSpread(f3).getAttribute("hierView")!="true");
if (f3.getAttribute("showFocusRect")=="false")w0=false;
if (w0){
this.PaintSelectedCell(f3,f3.e0,false);
this.PaintFocusRect(f3);
this.PaintAnchorCellHeader(f3,true);
return ;
}
var f9=f3.e0.parentNode.cells[0].firstChild;
if (f9!=null&&f9.nodeName!="#text"&&f9.getAttribute("FpSpread")=="Spread")return ;
this.PaintSelectedCell(f3,f3.e0,true,true);
this.PaintAnchorCellHeader(f3,true);
}
this.ClearSelection=function (f3,thisonly){
var w1=this.GetParentSpread(f3);
if (thisonly==null&&w1!=null&&w1.getAttribute("hierView")=="true"){
this.ClearSelection(w1);
return ;
}
var j5=this.GetViewport(f3);
var h9=this.GetRowCount(f3);
if (j5!=null&&j5.rows.length>h9){
for (var f5=0;f5<j5.rows.length;f5++){
if (j5.rows[f5].cells.length>0&&j5.rows[f5].cells[0]!=null&&j5.rows[f5].cells[0].firstChild!=null&&j5.rows[f5].cells[0].firstChild.nodeName!="#text"){
var f9=j5.rows[f5].cells[0].firstChild;
if (f9.getAttribute("FpSpread")=="Spread"){
this.ClearSelection(f9,true);
}
}
}
}
this.DoclearSelection(f3);
if (f3.e0!=null){
var t5=this.GetOperationMode(f3);
if (t5=="RowMode"||t5=="SingleSelect"||t5=="ExtendedSelect"||t5=="MultiSelect"){
var i5=this.GetRowFromCell(f3,f3.e0);
this.PaintSelection(f3,i5,-1,1,-1,false);
}
this.PaintSelectedCell(f3,f3.e0,false);
this.PaintAnchorCellHeader(f3,false);
}else {
var i4=this.GetCellFromRowCol(f3,1,0);
if (i4!=null)this.PaintSelectedCell(f3,i4,false);
}
this.PaintFocusRect(f3);
f3.selectedCols=[];
f3.f1=true;
}
this.SetSelectedRange=function (f3,f7,i3,f6,i1){
this.ClearSelection(f3);
var f7=this.GetDisplayIndex(f3,f7);
var w2=0;
var w3=f6;
var j5=this.GetViewport(f3);
if (j5!=null){
for (f5=f7;f5<j5.rows.length&&w2<w3;f5++){
if (this.IsChildSpreadRow(f3,j5,f5)){;
f6++;
}else {
w2++;
}
}
}
this.PaintSelection(f3,f7,i3,f6,i1,true);
this.SetSelection(f3,f7,i3,f6,i1);
}
this.AddSelection=function (f3,f7,i3,f6,i1){
var f7=this.GetDisplayIndex(f3,f7);
var w2=0;
var w3=f6;
var j5=this.GetViewport(f3);
if (j5!=null){
for (f5=f7;f5<j5.rows.length&&w2<w3;f5++){
if (this.IsChildSpreadRow(f3,j5,f5)){;
f6++;
}else {
w2++;
}
}
}
this.PaintSelection(f3,f7,i3,f6,i1,true);
this.SetSelection(f3,f7,i3,f6,i1,true);
}
this.SelectRow=function (f3,index,w2,select,ignoreAnchor){
f3.e4=index;
f3.e5=-1;
if (!ignoreAnchor)this.UpdateAnchorCell(f3,index,0,false);
f3.e6="r";
this.PaintSelection(f3,index,-1,w2,-1,select);
if (select)
{
this.SetSelection(f3,index,-1,w2,-1);
}else {
this.RemoveSelection(f3,index,-1,w2,-1);
this.PaintFocusRect(f3);
}
}
this.SelectColumn=function (f3,index,w2,select,ignoreAnchor){
f3.e4=-1;
f3.e5=index;
if (!ignoreAnchor)this.UpdateAnchorCell(f3,0,index,false);
f3.e6="c";
this.PaintSelection(f3,-1,index,-1,w2,select);
if (select)
{
this.SetSelection(f3,-1,index,-1,w2);
this.AddColSelection(f3,index);
}
}
this.AddColSelection=function (f3,index){
var w4=0;
for (var f5=0;f5<f3.selectedCols.length;f5++){
if (f3.selectedCols[f5]==index)return ;
if (index>f3.selectedCols[f5])w4=f5+1;
}
f3.selectedCols.length++;
for (var f5=f3.selectedCols.length-1;f5>w4;f5--)
f3.selectedCols[f5]=f3.selectedCols[f5-1];
f3.selectedCols[w4]=index;
}
this.IsColSelected=function (f3,s7){
for (var f5=0;f5<f3.selectedCols.length;f5++)
if (f3.selectedCols[f5]==s7)return true;
return false;
}
this.SyncColSelection=function (f3){
f3.selectedCols=[];
var w5=this.GetSelectedRanges(f3);
for (var f5=0;f5<w5.length;f5++){
var i9=w5[f5];
if (i9.type=="Column"){
for (var j1=i9.col;j1<i9.col+i9.colCount;j1++){
this.AddColSelection(f3,j1);
}
}
}
}
this.InitMovingCol=function (f3,s7,isGroupBar,o7){
if (f3.getAttribute("LayoutMode")&&s7==-1)return ;
if (this.GetOperationMode(f3)!="Normal"){
f3.selectedCols=[];
f3.selectedCols.push(s7);
}
if (isGroupBar){
this.dragCol=s7;
this.dragViewCol=this.GetColByKey(f3,s7);
}else {
this.dragCol=parseInt(this.GetSheetColIndex(f3,s7));
this.dragViewCol=s7;
}
var w6=this.GetMovingCol(f3);
if (isGroupBar){
this.ClearSelection(f3);
w6.innerHTML="";
var w7=document.createElement("DIV");
w7.innerHTML=o7.innerHTML;
w7.style.borderTop="0px solid";
w7.style.borderLeft="0px solid";
w7.style.borderRight="#808080 1px solid";
w7.style.borderBottom="#808080 1px solid";
w7.style.width=""+Math.max(this.GetPreferredCellWidth(f3,o7),80)+"px";
w6.appendChild(w7);
if (f3.getAttribute("DragColumnCssClass")==null){
w6.style.backgroundColor=o7.style.backgroundColor;
w6.style.paddingTop="1px";
w6.style.paddingBottom="1px";
}
w6.style.top="-50px";
w6.style.left="-100px";
}else {
var w8=0;
w6.style.top="0px";
w6.style.left="-1000px";
w6.style.display="";
w6.innerHTML="";
var w9=document.createElement("TABLE");
w6.appendChild(w9);
var j7=document.createElement("TR");
w9.appendChild(j7);
for (var f5=0;f5<f3.selectedCols.length;f5++){
var i4=document.createElement("TD");
j7.appendChild(i4);
var x0;
var x1;
if (f3.getAttribute("columnHeaderAutoTextIndex")!=null)
x0=parseInt(f3.getAttribute("columnHeaderAutoTextIndex"));
else 
x0=f3.getAttribute("ColHeaders")-1;
x1=f3.selectedCols[f5];
var x2=this.GetHeaderCellFromRowCol(f3,x0,x1,true);
if (x2.getAttribute("FpCellType")=="ExtenderCellType"&&x2.getElementsByTagName("DIV").length>0){
var x3=this.GetEditor(x2);
var x4=this.GetFunction("ExtenderCellType_getEditorValue");
if (x3!=null&&x4!=null){
i4.innerHTML=x4(x3);
}
}
else 
i4.innerHTML=x2.innerHTML;
i4.style.cssText=x2.style.cssText;
i4.style.borderTop="0px solid";
i4.style.borderLeft="0px solid";
i4.style.borderRight="#808080 1px solid";
i4.style.borderBottom="#808080 1px solid";
i4.style.whiteSpace="nowrap";
i4.setAttribute("align","center");
var k1=Math.max(this.GetPreferredCellWidth(f3,x2),80);
i4.style.width=""+k1+"px";
w8+=k1;
}
if (f3.getAttribute("DragColumnCssClass")==null){
w6.style.backgroundColor=f3.getAttribute("SelectedBackColor");
w6.style.tableLayout="fixed";
w6.style.width=""+w8+"px";
}
}
f3.selectedCols.context=[];
var x5=f3.selectedCols.context;
var q8=0;
n7=this.GetColGroup(this.GetColHeader(f3));
if (n7!=null){
for (var f5=0;f5<n7.childNodes.length;f5++){
var x6=n7.childNodes[f5].offsetWidth;
x5.push({left:q8,width:x6});
q8+=x6;
}
}
}
this.SelectTable=function (f3,select){
if (select)this.UpdateAnchorCell(f3,0,0,false);
f3.e6="t";
this.PaintSelection(f3,-1,-1,-1,-1,select);
if (select)
{
this.SetSelection(f3,-1,-1,-1,-1);
}
}
this.GetSpanCell=function (f7,i3,span){
if (span==null){
return null;
}
var w2=span.length;
for (var f5=0;f5<w2;f5++){
var r0=span[f5];
var x7=(r0.row<=f7&&f7<r0.row+r0.rowCount&&r0.col<=i3&&i3<r0.col+r0.colCount);
if (x7)return r0;
}
return null;
}
this.IsCovered=function (f3,f7,i3,span){
var r0=this.GetSpanCell(f7,i3,span);
if (r0==null){
return false;
}else {
if (r0.row==f7&&r0.col==i3)return false;
return true;
}
}
this.IsSpanCell=function (f3,f7,i3){
var e8=f3.e8;
var w2=e8.length;
for (var f5=0;f5<w2;f5++){
var r0=e8[f5];
var x7=(r0.row==f7&&r0.col==i3);
if (x7)return r0;
}
return null;
}
this.SelectRange=function (f3,f7,i3,f6,i1,select){
f3.e6="";
this.UpdateRangeSelection(f3,f7,i3,f6,i1,select);
if (select){
this.SetSelection(f3,f7,i3,f6,i1);
this.PaintAnchorCell(f3);
}
}
this.UpdateRangeSelection=function (f3,f7,i3,f6,i1,select){
var j5=this.GetViewport(f3);
this.UpdateRangeSelection(f3,f7,i3,f6,i1,select,j5);
}
this.GetSpanCells=function (f3,j5){
if (j5==this.GetViewport(f3))
return f3.e8;
else if (j5==this.GetColHeader(f3))
return f3.f0;
else if (j5==this.GetColFooter(f3))
return f3.footerSpanCells;
else if (j5==this.GetRowHeader(f3))
return f3.e9;
return null;
}
this.UpdateRangeSelection=function (f3,f7,i3,f6,i1,select,j5){
if (j5==null)return ;
for (var f5=f7;f5<f7+f6&&f5<j5.rows.length;f5++){
if (this.IsChildSpreadRow(f3,j5,f5))continue ;
var x8=this.GetCellIndex(f3,f5,i3,this.GetSpanCells(f3,j5));
for (var j1=0;j1<i1;j1++){
if (this.IsCovered(f3,f5,i3+j1,this.GetSpanCells(f3,j5)))continue ;
if (x8<j5.rows[f5].cells.length){
this.PaintSelectedCell(f3,j5.rows[f5].cells[x8],select);
}
x8++;
}
}
}
this.GetColFromCell=function (f3,i4){
if (i4==null)return -1;
var f7=this.GetRowFromCell(f3,i4);
return this.GetColIndex(f3,f7,i4.cellIndex,this.GetSpanCells(f3,i4.parentNode.parentNode.parentNode),false,this.IsChild(i4,this.GetRowHeader(f3)));
}
this.GetRowFromCell=function (f3,i4){
if (i4==null||i4.parentNode==null)return -1;
var f7=i4.parentNode.rowIndex;
return f7;
}
this.GetColIndex=function (f3,f5,cellIndex,span,frozArea,d4){
var x9=false;
var g0=this.GetViewport(f3);
if (g0!=null)x9=g0.parentNode.getAttribute("hiddenCells");
if (x9&&span==f3.e8){
return cellIndex;
}
var y0=0;
var w2=this.GetColCount(f3);
var y1=0;
if (d4){
y1=0;
var n7=null;
if (this.GetRowHeader(f3)!=null)
n7=this.GetColGroup(this.GetRowHeader(f3));
if (n7!=null)
w2=n7.childNodes.length;
}
for (var j1=y1;j1<w2;j1++){
if (this.IsCovered(f3,f5,j1,span))continue ;
if (y0==cellIndex){
return j1;
}
y0++;
}
return w2;
}
this.GetCellIndex=function (f3,f5,s7,span){
var x9=false;
var g0=this.GetViewport(f3);
if (g0!=null)x9=g0.parentNode.getAttribute("hiddenCells");
if (x9&&span==f3.e8){
return s7;
}else {
var y1=0;
var w2=s7;
var y0=0;
for (var j1=0;j1<w2;j1++){
if (this.IsCovered(f3,f5,y1+j1,span))continue ;
y0++;
}
return y0;
}
}
this.PaintSelections=function (f3,select){
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
var o1=g7.getElementsByTagName("state")[0];
var n9=o1.getElementsByTagName("selection")[0];
var o0=n9.firstChild;
while (o0!=null){
var f7=0;
var i3=0;
if (f3.getAttribute("LayoutMode")&&o0.getAttribute("row")!="-1"&&o0.getAttribute("col")!="-1"){
var i4=this.GetCellByRowCol2(f3,o0.getAttribute("row"),o0.getAttribute("col"));
if (i4){
f7=this.GetRowFromCell(f3,i4);
i3=this.GetColFromCell(f3,i4);
}
}
else if (f3.getAttribute("LayoutMode")&&o0.getAttribute("col")!="-1"&&o0.getAttribute("row")=="-1"&&o0.getAttribute("rowcount")=="-1"){
var j7=this.GetRowTemplateRowFromGroupCell(f3,parseInt(o0.getAttribute("col")));
var i4=this.GetCellByRowCol2(f3,j7,parseInt(o0.getAttribute("col")));
if (i4){
f7=parseInt(i4.parentNode.getAttribute("row"));
i3=this.GetColFromCell(f3,i4);
}
}
else {
f7=this.GetRowByKey(f3,o0.getAttribute("row"));
i3=this.GetColByKey(f3,o0.getAttribute("col"));
}
var f6=parseInt(o0.getAttribute("rowcount"));
var i1=parseInt(o0.getAttribute("colcount"));
o0.setAttribute("rowIndex",f7);
o0.setAttribute("colIndex",i3);
if (f3.getAttribute("LayoutMode")&&o0.getAttribute("col")>=0&&o0.getAttribute("row")>=0&&(o0.getAttribute("rowcount")>=1||o0.getAttribute("colcount")>=1)){
var y2=o0.nextSibling;
if (parseInt(o0.getAttribute("row"))!=parseInt(o2.innerHTML)||parseInt(o0.getAttribute("col"))!=parseInt(o3.innerHTML))n9.removeChild(o0);
o0=y2;
continue ;
}
if (f3.getAttribute("LayoutMode")&&o0.getAttribute("col")=="-1"&&o0.getAttribute("row")!=-1){
f6=parseInt(f3.getAttribute("layoutrowcount"));
}
if (f3.getAttribute("LayoutMode")&&o0.getAttribute("col")!="-1"&&o0.getAttribute("row")=="-1"&&o0.getAttribute("rowcount")=="-1")
this.PaintMultipleRowSelection(f3,f7,i3,1,1,select);
else 
this.PaintSelection(f3,f7,i3,f6,i1,select);
o0=o0.nextSibling;
}
}
this.NextCell=function (f3,event,key){
if (event.altKey)return ;
var y3=this.GetParent(this.GetViewport(f3));
if (f3.e0==null){
var j3=this.FireActiveCellChangingEvent(f3,0,0);
if (!j3){
f3.SetActiveCell(0,0);
var h2=this.CreateEvent("ActiveCellChanged");
h2.cmdID=f3.id;
h2.row=h2.Row=0;
h2.col=h2.Col=0;
this.FireEvent(f3,h2);
}
return ;
}
if (event.shiftKey&&key!=this.tab){
var q6=this.GetOperationMode(f3);
if (q6=="RowMode"||q6=="SingleSelect"||q6=="MultiSelect"||(q6=="Normal"&&this.GetSelectionPolicy(f3)=="Single"))return ;
var r0=this.GetSpanCell(f3.e2,f3.e3,this.GetSpanCells(f3,this.GetViewportFromCell(f3,f3.e0)));
switch (key){
case this.right:
var f7=f3.e2;
var i3=f3.e3+1;
if (r0!=null){
i3=r0.col+r0.colCount;
}
if (i3>this.GetColCount(f3)-1)return ;
f3.e3=i3;
f3.e1=this.GetCellFromRowCol(f3,f7,i3);
this.Select(f3,f3.e0,f3.e1);
break ;
case this.left:
var f7=f3.e2;
var i3=f3.e3-1;
if (r0!=null){
i3=r0.col-1;
}
r0=this.GetSpanCell(f7,i3,this.GetSpanCells(f3,this.GetViewportFromCell(f3,f3.e0)));
if (r0!=null){
if (this.IsSpanCell(f3,f7,r0.col))i3=r0.col;
}
if (i3<0)return ;
f3.e3=i3;
f3.e1=this.GetCellFromRowCol(f3,f7,i3);
this.Select(f3,f3.e0,f3.e1);
break ;
case this.down:
var f7=f3.e2+1;
var i3=f3.e3;
if (r0!=null){
f7=r0.row+r0.rowCount;
}
f7=this.GetNextRow(f3,f7);
if (f7>this.GetRowCountInternal(f3)-1)return ;
f3.e2=f7;
f3.e1=this.GetCellFromRowCol(f3,f7,i3);
this.Select(f3,f3.e0,f3.e1);
this.PaintSelections(f3,true);
break ;
case this.up:
var f7=f3.e2-1;
var i3=f3.e3;
if (r0!=null){
f7=r0.row-1;
}
f7=this.GetPrevRow(f3,f7);
r0=this.GetSpanCell(f7,i3,this.GetSpanCells(f3,this.GetViewportFromCell(f3,f3.e0)));
if (r0!=null){
if (this.IsSpanCell(f3,r0.row,i3))f7=r0.row;
}
if (f7<0)return ;
f3.e2=f7;
f3.e1=this.GetCellFromRowCol(f3,f7,i3);
this.Select(f3,f3.e0,f3.e1);
this.PaintSelections(f3,true);
break ;
case this.home:
if (f3.e0.parentNode.rowIndex>=0){
f3.e3=0;
f3.e1=this.GetCellFromRowCol(f3,f3.e2,f3.e3);
this.Select(f3,f3.e0,f3.e1);
}
break ;
case this.end:
if (f3.e0.parentNode.rowIndex>=0){
f3.e3=this.GetColCount(f3)-1;
f3.e1=this.GetCellFromRowCol(f3,f3.e2,f3.e3);
this.Select(f3,f3.e0,f3.e1);
}
break ;
case this.pdn:
if (this.GetViewport(f3)!=null&&f3.e0.parentNode.rowIndex>=0){
f7=0;
for (f7=0;f7<this.GetViewport(f3).rows.length;f7++){
if (this.GetViewport(f3).rows[f7].offsetTop+this.GetViewport(f3).rows[f7].offsetHeight>this.GetViewport(f3).parentNode.offsetHeight+this.GetViewport(f3).parentNode.scrollTop){
break ;
}
}
f7=this.GetNextRow(f3,f7);
if (f7<this.GetViewport(f3).rows.length){
this.GetViewport(f3).parentNode.scrollTop=this.GetViewport(f3).rows[f7].offsetTop;
f3.e2=f7;
}else {
f7=this.GetRowCountInternal(f3)-1;
f3.e2=f7;
}
f3.e1=this.GetCellFromRowCol(f3,f3.e2,f3.e3);
this.Select(f3,f3.e0,f3.e1);
}
break ;
case this.pup:
if (this.GetViewport(f3)!=null&&f3.e0.parentNode.rowIndex>0){
f7=0;
for (f7=0;f7<this.GetViewport(f3).rows.length;f7++){
if (this.GetViewport(f3).rows[f7].offsetTop+this.GetViewport(f3).rows[f7].offsetHeight>this.GetViewport(f3).parentNode.scrollTop){
break ;
}
}
if (f7<this.GetViewport(f3).rows.length){
var f8=0;
while (f7>0){
f8+=this.GetViewport(f3).rows[f7].offsetHeight;
if (f8>this.GetViewport(f3).parentNode.offsetHeight){
break ;
}
f7--;
}
f7=this.GetPrevRow(f3,f7);
if (f7>=0){
this.GetViewport(f3).parentNode.scrollTop=this.GetViewport(f3).rows[f7].offsetTop;
f3.e2=f7;
f3.e1=this.GetCellFromRowCol(f3,f3.e2,f3.e3);
this.Select(f3,f3.e0,f3.e1);
}
}
}
break ;
}
this.SyncColSelection(f3);
}else {
if (key==this.tab){
if (event.shiftKey)key=this.left;
else key=this.right;
}
var y4=f3.e0;
var f7=f3.e2;
var i3=f3.e3;
switch (key){
case this.right:
if (event.keyCode==this.tab){
var y5=f7;
var y6=i3;
do {
this.MoveRight(f3,f7,i3);
f7=f3.e2;
i3=f3.e3;
}while (!(y5==f7&&y6==i3)&&this.GetCellFromRowCol(f3,f7,i3).getAttribute("TabStop")!=null&&this.GetCellFromRowCol(f3,f7,i3).getAttribute("TabStop")=="false")
}
else {
this.MoveRight(f3,f7,i3);
}
break ;
case this.left:
if (event.keyCode==this.tab){
var y5=f7;
var y6=i3;
do {
this.MoveLeft(f3,f7,i3);
f7=f3.e2;
i3=f3.e3;
}while (!(y5==f7&&y6==i3)&&this.GetCellFromRowCol(f3,f7,i3).getAttribute("TabStop")!=null&&this.GetCellFromRowCol(f3,f7,i3).getAttribute("TabStop")=="false")
}
else {
this.MoveLeft(f3,f7,i3);
}
break ;
case this.down:
this.MoveDown(f3,f7,i3);
break ;
case this.up:
this.MoveUp(f3,f7,i3);
break ;
case this.home:
if (f3.e0.parentNode.rowIndex>=0){
this.UpdateLeadingCell(f3,f7,0);
}
break ;
case this.end:
if (f3.e0.parentNode.rowIndex>=0){
i3=this.GetColCount(f3)-1;
this.UpdateLeadingCell(f3,f7,i3);
}
break ;
case this.pdn:
if (this.GetViewport(f3)!=null&&f3.e0.parentNode.rowIndex>=0){
f7=0;
for (f7=0;f7<this.GetViewport(f3).rows.length;f7++){
if (this.GetViewport(f3).rows[f7].offsetTop+this.GetViewport(f3).rows[f7].offsetHeight>this.GetViewport(f3).parentNode.offsetHeight+this.GetViewport(f3).parentNode.scrollTop){
break ;
}
}
f7=this.GetNextRow(f3,f7);
if (f7<this.GetViewport(f3).rows.length){
var f9=this.GetViewport(f3).rows[f7].offsetTop;
this.UpdateLeadingCell(f3,f7,f3.e3);
this.GetViewport(f3).parentNode.scrollTop=f9;
}else {
f7=this.GetPrevRow(f3,this.GetRowCount(f3)-1);
this.UpdateLeadingCell(f3,f7,f3.e3);
}
}
break ;
case this.pup:
if (this.GetViewport(f3)!=null&&f3.e0.parentNode.rowIndex>=0){
f7=0;
for (f7=0;f7<this.GetViewport(f3).rows.length;f7++){
if (this.GetViewport(f3).rows[f7].offsetTop+this.GetViewport(f3).rows[f7].offsetHeight>this.GetViewport(f3).parentNode.scrollTop){
break ;
}
}
if (f7<this.GetViewport(f3).rows.length){
var f8=0;
while (f7>=0){
f8+=this.GetViewport(f3).rows[f7].offsetHeight;
if (f8>this.GetViewport(f3).parentNode.offsetHeight){
break ;
}
f7--;
}
f7=this.GetPrevRow(f3,f7);
if (f7>=0){
var f9=this.GetViewport(f3).rows[f7].offsetTop;
this.UpdateLeadingCell(f3,f7,f3.e3);
this.GetViewport(f3).parentNode.scrollTop=f9;
}
}
}
break ;
}
if (y4!=f3.e0){
var h2=this.CreateEvent("ActiveCellChanged");
h2.cmdID=f3.id;
h2.Row=h2.row=this.GetSheetIndex(f3,this.GetRowFromCell(f3,f3.e0));
h2.Col=h2.col=this.GetColFromCell(f3,f3.e0);
if (f3.getAttribute("LayoutMode"))
h2.InnerRow=h2.innerRow=f3.e0.parentNode.getAttribute("row");
this.FireEvent(f3,h2);
}
}
var i4=this.GetCellFromRowCol(f3,f3.e2,f3.e3);
if (key==this.left&&i4.offsetLeft<y3.scrollLeft){
if (i4.cellIndex>0)
y3.scrollLeft=f3.e0.offsetLeft;
else 
y3.scrollLeft=0;
}else if (i4.cellIndex==0){
y3.scrollLeft=0;
}
if (key==this.right&&i4.offsetLeft+i4.offsetWidth>y3.scrollLeft+y3.offsetWidth-10){
y3.scrollLeft+=i4.offsetWidth;
}
if (key==this.up&&i4.parentNode.offsetTop<y3.scrollTop){
if (i4.parentNode.rowIndex>1)
y3.scrollTop=i4.parentNode.offsetTop;
else 
y3.scrollTop=0;
}else if (i4.parentNode.rowIndex==1){
y3.scrollTop=0;
}
var y7=this.GetParent(this.GetViewport(f3));
y3=this.GetParent(this.GetViewport(f3));
if (key==this.down&&this.IsChild(i4,y3)&&i4.offsetTop+i4.offsetHeight>y3.scrollTop+y3.clientHeight){
y7.scrollTop+=i4.offsetHeight;
}
if (i4!=null&&i4.offsetWidth<y3.clientWidth){
if (this.IsChild(i4,y3)&&i4.offsetLeft+i4.offsetWidth>y3.scrollLeft+y3.clientWidth){
y7.scrollLeft=i4.offsetLeft+i4.offsetWidth-y3.clientWidth;
}
}
if (this.IsChild(i4,y3)&&i4.offsetTop+i4.offsetHeight>y3.scrollTop+y3.clientHeight&&i4.offsetHeight<y3.clientHeight){
y7.scrollTop=i4.offsetTop+i4.offsetHeight-y3.clientHeight;
}
if (i4.offsetTop<y3.scrollTop){
y7.scrollTop=i4.offsetTop;
}
this.ScrollView(f3);
this.EnableButtons(f3);
this.SaveData(f3);
if (f3.e0!=null){
var j4=this.GetEditor(f3.e0);
if (j4!=null){
if (j4.tagName!="SELECT")
j4.focus();
if (!j4.disabled&&(j4.type==null||j4.type=="checkbox"||j4.type=="radio"||j4.type=="text"||j4.type=="password")){
this.editing=true;
this.b7=j4;
this.b8=this.GetEditorValue(j4);
}
}
}
this.Focus(f3);
}
this.MoveUp=function (f3,f7,i3){
var f6=this.GetRowCountInternal(f3);
var i1=this.GetColCount(f3);
f7--;
f7=this.GetPrevRow(f3,f7);
if (f7>=0){
f3.e2=f7;
this.UpdateLeadingCell(f3,f3.e2,f3.e3);
}
}
this.MoveDown=function (f3,f7,i3){
var f6=this.GetRowCountInternal(f3);
var i1=this.GetColCount(f3);
var r0=this.GetSpanCell(f7,i3,this.GetSpanCells(f3,this.GetViewportFromCell(f3,f3.e0)));
if (r0!=null){
f7=r0.row+r0.rowCount;
}else {
f7++;
}
f7=this.GetNextRow(f3,f7);
if (f7==f6)f7=f6-1;
if (f7<f6){
f3.e2=f7;
this.UpdateLeadingCell(f3,f3.e2,f3.e3);
}
}
this.MoveLeft=function (f3,f7,i3){
var y8=f7;
var f6=this.GetRowCountInternal(f3);
var i1=this.GetColCount(f3);
var r0=this.GetSpanCell(f7,i3,this.GetSpanCells(f3,this.GetViewportFromCell(f3,f3.e0)));
if (r0!=null){
i3=r0.col-1;
}else {
i3--;
}
if (i3<0){
i3=i1-1;
f7--;
if (f7<0){
f7=f6-1;
}
f7=this.GetPrevRow(f3,f7);
f3.e2=f7;
}
var y9=this.UpdateLeadingCell(f3,f3.e2,i3);
if (y9)f3.e2=y8;
}
this.MoveRight=function (f3,f7,i3){
var y8=f7;
var f6=this.GetRowCountInternal(f3);
var i1=this.GetColCount(f3);
var r0=this.GetSpanCell(f7,i3,this.GetSpanCells(f3,this.GetViewportFromCell(f3,f3.e0)));
if (r0!=null){
i3=r0.col+r0.colCount;
}else {
i3++;
}
if (i3>=i1){
i3=0;
f7++;
if (f7>=f6)f7=0;
f3.e2=this.GetNextRow(f3,f7);
}
var y9=this.UpdateLeadingCell(f3,f3.e2,i3);
if (y9)f3.e2=y8;
}
this.UpdateLeadingCell=function (f3,f7,i3){
var z0=0;
if (f3.getAttribute("LayoutMode")){
z0=this.GetRowFromViewPort(f3,f7,i3);
f3.e0=this.GetCellFromRowCol(f3,f7,i3);
var z1=this.GetCellFromRowCol(f3,z0,i3);
if (z1)z0=z1.parentNode.getAttribute("row");
}
var j3=this.FireActiveCellChangingEvent(f3,f7,i3,z0);
if (!j3){
var v9=this.GetOperationMode(f3);
if (v9!="MultiSelect")
this.ClearSelection(f3);
f3.e3=i3;
f3.e2=f7;
f3.e5=i3;
f3.e4=f7;
this.UpdateAnchorCell(f3,f7,i3);
}
return j3;
}
this.GetPrevRow=function (f3,f7){
if (f7<0)return 0;
var j5=this.GetViewport(f3);
if (j5!=null){
while (f7>0&&j5.rows[f7].cells.length>0){
if (this.IsChildSpreadRow(f3,j5,f7))
f7--;
else 
break ;
}
}
if (j5!=null&&f7>=0&&f7<j5.rows.length){
if (j5.rows[f7].getAttribute("previewrow")){
f7--;
}
}
return f7;
}
this.GetNextRow=function (f3,f7){
var j5=this.GetViewport(f3);
while (j5!=null&&f7<j5.rows.length){
if (this.IsChildSpreadRow(f3,j5,f7))f7++;
else 
break ;
}
if (j5!=null&&f7>=0&&f7<j5.rows.length){
if (j5.rows[f7].getAttribute("previewrow")){
f7++;
}
}
return f7;
}
this.FireActiveCellChangingEvent=function (f3,j7,n8,innerRow){
var h2=this.CreateEvent("ActiveCellChanging");
h2.cancel=false;
h2.cmdID=f3.id;
h2.row=this.GetSheetIndex(f3,j7);
h2.col=n8;
if (f3.getAttribute("LayoutMode"))
h2.innerRow=innerRow;
this.FireEvent(f3,h2);
return h2.cancel;
}
this.GetSheetRowIndex=function (f3,f7){
f7=this.GetDisplayIndex(f3,f7);
if (f7<0)return -1;
var n0=this.GetViewport(f3).rows[f7];
if (n0!=null){
return n0.getAttribute("FpKey");
}else {
return -1;
}
}
this.GetSheetColIndex=function (f3,i3){
var n8=-1;
var n7=null;
var z2=this.GetColHeader(f3);
if (z2!=null&&z2.rows.length>0){
n7=this.GetColGroup(z2);
}else {
var g0=this.GetViewport(f3);
if (g0!=null&&g0.rows.length>0){
n7=this.GetColGroup(g0);
}
}
if (n7!=null&&i3>=0&&i3<n7.childNodes.length){
n8=n7.childNodes[i3].getAttribute("FpCol");
}
return n8;
}
this.GetCellByRowCol=function (f3,f7,i3){
f7=this.GetDisplayIndex(f3,f7);
return this.GetCellFromRowCol(f3,f7,i3);
}
this.GetHeaderCellFromRowCol=function (f3,f7,i3,d5){
if (f7<0||i3<0)return null;
var g0=null;
if (d5){
g0=this.GetColHeader(f3);
}else {
g0=this.GetRowHeader(f3);
}
var r0=this.GetSpanCell(f7,i3,this.GetSpanCells(f3,g0));
if (r0!=null){
f7=r0.row;
i3=r0.col;
}
var z3=this.GetCellIndex(f3,f7,i3,this.GetSpanCells(f3,g0));
return g0.rows[f7].cells[z3];
}
this.GetCellFromRowCol=function (f3,f7,i3,prevCell){
if (f7<0||i3<0)return null;
var g0=null;
{
g0=this.GetViewport(f3);
}
var e8=f3.e8;
var r0=this.GetSpanCell(f7,i3,e8);
if (r0!=null){
f7=r0.row;
i3=r0.col;
}
var z3=0;
var x9=false;
if (g0!=null)x9=g0.parentNode.getAttribute("hiddenCells");
if (prevCell!=null&&!x9){
if (prevCell.cellIndex<prevCell.parentNode.cells.length-1)
z3=prevCell.cellIndex+1;
}
else 
{
z3=this.GetCellIndex(f3,f7,i3,e8);
}
if (f7>=0&&f7<g0.rows.length)
return g0.rows[f7].cells[z3];
else 
return null;
}
this.GetHiddenValue=function (f3,f7,colName){
if (colName==null)return ;
f7=this.GetDisplayIndex(f3,f7);
var t6=null;
var g0=null;
g0=this.GetViewport(f3);
if (g0!=null&&f7>=0&&f7<g0.rows.length){
var n0=g0.rows[f7];
t6=n0.getAttribute("hv"+colName);
}
return t6;
}
this.GetValue=function (f3,f7,i3){
f7=this.GetDisplayIndex(f3,f7);
var i4=this.GetCellFromRowCol(f3,f7,i3);
var k2=this.GetRender(i4);
var t6=this.GetValueFromRender(f3,k2);
if (t6!=null)t6=this.Trim(t6.toString());
return t6;
}
this.SetValue=function (f3,f7,i3,v2,noEvent,recalc){
f7=this.GetDisplayIndex(f3,f7);
if (v2!=null&&typeof(v2)!="string")v2=new String(v2);
var i4=this.GetCellFromRowCol(f3,f7,i3);
if (this.ValidateCell(f3,i4,v2)){
this.SetCellValueFromView(i4,v2);
if (v2!=null){
this.SetCellValue(f3,i4,""+v2,noEvent,recalc);
}else {
this.SetCellValue(f3,i4,"",noEvent,recalc);
}
this.SizeSpread(f3);
}else {
if (f3.getAttribute("lcidMsg")!=null)
alert(f3.getAttribute("lcidMsg"));
else 
alert("Can't set the data into the cell. The data type is not correct for the cell.");
}
}
this.SetActiveCell=function (f3,f7,i3){
this.ClearSelection(f3,true);
f7=this.GetDisplayIndex(f3,f7);
this.UpdateAnchorCell(f3,f7,i3);
this.ResetLeadingCell(f3);
}
this.GetOperationMode=function (f3){
var v9=f3.getAttribute("OperationMode");
return v9;
}
this.SetOperationMode=function (f3,v9){
f3.setAttribute("OperationMode",v9);
}
this.GetEnableRowEditTemplate=function (f3){
var z4=f3.getAttribute("EnableRowEditTemplate");
return z4;
}
this.GetSelectionPolicy=function (f3){
var z5=f3.getAttribute("SelectionPolicy");
return z5;
}
this.UpdateAnchorCell=function (f3,f7,i3,select){
if (f7<0||i3<0)return ;
f3.e0=this.GetCellFromRowCol(f3,f7,i3);
if (f3.e0==null)return ;
this.SetActiveRow(f3,this.GetRowKeyFromCell(f3,f3.e0));
this.SetActiveCol(f3,this.GetColKeyFromCell(f3,f3.e0));
if (select==null||select){
var v9=this.GetOperationMode(f3);
if (v9=="RowMode"||v9=="SingleSelect"||v9=="ExtendedSelect")
this.SelectRow(f3,f7,1,true,true);
else if (v9!="MultiSelect")
this.SelectRange(f3,f7,i3,1,1,true);
else 
this.PaintFocusRect(f3);
}
}
this.ResetLeadingCell=function (f3){
if (f3.e0==null||!this.IsChild(f3.e0,f3))return ;
f3.e2=this.GetRowFromCell(f3,f3.e0);
f3.e3=this.GetColFromCell(f3,f3.e0);
this.SelectRange(f3.e2,f3.e3,1,1,true);
}
this.Edit=function (f3,j7){
var v9=this.GetOperationMode(f3);
if (v9!="RowMode")return ;
var t3=f3.getAttribute("name");
var z6=(f3.getAttribute("ajax")!="false");
if (z6){
if (FarPoint&&FarPoint.System.WebControl.MultiColumnComboBoxCellTypeUtilitis)
FarPoint.System.WebControl.MultiColumnComboBoxCellTypeUtilitis.CloseAll();
this.SyncData(t3,"Edit,"+j7,f3);
}
else 
__doPostBack(t3,"Edit,"+j7);
}
this.Update=function (f3){
if (this.editing&&this.GetOperationMode(f3)!="RowMode"&&this.GetEnableRowEditTemplate(f3)!="true")return ;
this.SaveData(f3);
var t3=f3.getAttribute("name");
__doPostBack(t3,"Update");
}
this.Cancel=function (f3){
var f9=document.getElementById(f3.id+"_data");
f9.value="";
this.SaveData(f3);
var t3=f3.getAttribute("name");
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"Cancel",f3);
else 
__doPostBack(t3,"Cancel");
}
this.Add=function (f3){
if (this.editing)return ;
var t3=null;
var p5=this.GetPageActiveSpread();
if (p5!=null){
t3=p5.getAttribute("name");
}else {
t3=f3.getAttribute("name");
}
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"Add",f3);
else 
__doPostBack(t3,"Add");
}
this.Insert=function (f3){
if (this.editing)return ;
var t3=null;
var p5=this.GetPageActiveSpread();
if (p5!=null){
t3=p5.getAttribute("name");
}else {
t3=f3.getAttribute("name");
}
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"Insert",f3);
else 
__doPostBack(t3,"Insert");
}
this.Delete=function (f3){
if (this.editing)return ;
var t3=null;
var p5=this.GetPageActiveSpread();
if (p5!=null){
t3=p5.getAttribute("name");
}else {
t3=f3.getAttribute("name");
}
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"Delete",f3);
else 
__doPostBack(t3,"Delete");
}
this.Print=function (f3){
if (this.editing)return ;
this.SaveData(f3);
if (document.printSpread==null){
var f9=document.createElement("IFRAME");
f9.name="printSpread";
f9.style.position="absolute";
f9.style.left="-10px";
f9.style.width="0px";
f9.style.height="0px";
document.printSpread=f9;
document.body.insertBefore(f9,null);
f9.addEventListener("load",function (){the_fpSpread.PrintSpread();},false);
}
var z7=document.getElementsByTagName("FORM");
if (z7!=null&&z7.length>0){
var j3=z7[0];
j3.__EVENTTARGET.value=f3.getAttribute("name");
j3.__EVENTARGUMENT.value="Print";
var z8=j3.target;
j3.target="printSpread";
j3.submit();
j3.target=z8;
}
}
this.PrintSpread=function (){
document.printSpread.focus();
document.printSpread.print();
window.focus();
var p5=this.GetPageActiveSpread();
if (p5!=null)this.Focus(p5);
}
this.GotoPage=function (f3,f4){
if (this.editing)return ;
var t3=f3.getAttribute("name");
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"Page,"+f4,f3);
else 
__doPostBack(t3,"Page,"+f4);
}
this.Next=function (f3){
if (this.editing)return ;
var t3=f3.getAttribute("name");
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"Next",f3);
else 
__doPostBack(t3,"Next");
}
this.Prev=function (f3){
if (this.editing)return ;
var t3=f3.getAttribute("name");
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"Prev",f3);
else 
__doPostBack(t3,"Prev");
}
this.GetViewportFromCell=function (f3,k0){
if (k0!=null){
var f9=k0;
while (f9!=null){
if (f9.tagName=="TABLE")break ;
f9=f9.parentNode;
}
if (f9==this.GetViewport(f3))
return f9;
}
return null;
}
this.IsChild=function (i4,j6){
if (i4==null||j6==null)return false;
var f9=i4.parentNode;
while (f9!=null){
if (f9==j6)return true;
f9=f9.parentNode;
}
return false;
}
this.GetCorner=function (f3){
return f3.d3;
}
this.Select=function (f3,cl1,cl2){
if (this.GetSpread(cl1)!=f3||this.GetSpread(cl2)!=f3)return ;
var i5=f3.e4;
var i6=f3.e5;
var z9=this.GetRowFromCell(f3,cl2);
var n1=0;
if (f3.e6=="r"){
n1=-1;
if (this.IsChild(cl2,this.GetColHeader(f3)))
z9=0;
}else if (f3.e6=="c"){
if (this.IsChild(cl2,this.GetRowHeader(f3)))
n1=0;
else 
n1=this.GetColFromCell(f3,cl2);
z9=-1;
}
else {
if (this.IsChild(cl2,this.GetColHeader(f3))){
z9=0;n1=this.GetColFromCell(f3,cl2);
}else if (this.IsChild(cl2,this.GetRowHeader(f3))){
n1=0;
}else {
n1=this.GetColFromCell(f3,cl2);
}
}
if (f3.e6=="t"){
i6=n1=i5=z9=-1;
}
var f9=Math.max(i5,z9);
i5=Math.min(i5,z9);
z9=f9;
f9=Math.max(i6,n1);
i6=Math.min(i6,n1);
n1=f9;
var i9=null;
var n9=this.GetSelection(f3);
var o0=n9.lastChild;
if (o0!=null){
var f7=this.GetRowByKey(f3,o0.getAttribute("row"));
var i3=this.GetColByKey(f3,o0.getAttribute("col"));
var f6=parseInt(o0.getAttribute("rowcount"));
var i1=parseInt(o0.getAttribute("colcount"));
i9=new this.Range();
this.SetRange(i9,"cell",f7,i3,f6,i1);
}
if (i9!=null&&i9.col==-1&&i9.row==-1)return ;
if (i9!=null&&i9.col==-1&&i9.row>=0){
if (i9.row>z9||i9.row+i9.rowCount-1<i5){
this.PaintSelection(f3,i9.row,i9.col,i9.rowCount,i9.colCount,false);
this.PaintSelection(f3,i5,i6,z9-i5+1,n1-i6+1,true);
}else {
if (i5>i9.row){
var f9=i5-i9.row;
this.PaintSelection(f3,i9.row,i9.col,f9,i9.colCount,false);
if (z9<i9.row+i9.rowCount-1){
this.PaintSelection(f3,z9,i9.col,i9.row+i9.rowCount-z9,i9.colCount,false);
}else {
this.PaintSelection(f3,i9.row+i9.rowCount,i9.col,z9-i9.row-i9.rowCount+1,i9.colCount,true);
}
}else {
this.PaintSelection(f3,i5,i9.col,i9.row-i5,i9.colCount,true);
if (z9<i9.row+i9.rowCount-1){
this.PaintSelection(f3,z9+1,i9.col,i9.row+i9.rowCount-z9-1,i9.colCount,false);
}else {
this.PaintSelection(f3,i9.row+i9.rowCount,i9.col,z9-i9.row-i9.rowCount+1,i9.colCount,true);
}
}
}
}else if (i9!=null&&i9.row==-1&&i9.col>=0){
if (i9.col>n1||i9.col+i9.colCount-1<i6){
this.PaintSelection(f3,i9.row,i9.col,i9.rowCount,i9.colCount,false);
this.PaintSelection(f3,i5,i6,z9-i5+1,n1-i6+1,true);
}else {
if (i6>i9.col){
this.PaintSelection(f3,i9.row,i9.col,i9.rowCount,i6-i9.col,false);
if (n1<i9.col+i9.colCount-1){
this.PaintSelection(f3,i9.row,n1,i9.rowCount,i9.col+i9.colCount-n1,false);
}else {
this.PaintSelection(f3,i9.row,i9.col+i9.colCount,i9.rowCount,n1-i9.col-i9.colCount,true);
}
}else {
this.PaintSelection(f3,i9.row,i6,i9.rowCount,i9.col-i6,true);
if (n1<i9.col+i9.colCount-1){
this.PaintSelection(f3,i9.row,n1+1,i9.rowCount,i9.col+i9.colCount-n1-1,false);
}else {
this.PaintSelection(f3,i9.row,i9.col+i9.colCount,i9.rowCount,n1-i9.col-i9.colCount+1,true);
}
}
}
}else if (i9!=null&&i9.row>=0&&i9.col>=0){
this.ExtendSelection(f3,i9,i5,i6,z9-i5+1,n1-i6+1);
}else {
this.PaintSelection(f3,i5,i6,z9-i5+1,n1-i6+1,true);
}
this.SetSelection(f3,i5,i6,z9-i5+1,n1-i6+1,i9==null);
}
this.ExtendSelection=function (f3,i9,newRow,newCol,newRowCount,newColCount)
{
var q8=Math.max(i9.col,newCol);
var q9=Math.min(i9.col+i9.colCount-1,newCol+newColCount-1);
var v0=Math.max(i9.row,newRow);
var aa0=Math.min(i9.row+i9.rowCount-1,newRow+newRowCount-1);
if (i9.row<v0){
this.PaintSelection(f3,i9.row,i9.col,v0-i9.row,i9.colCount,false);
}
if (i9.col<q8){
this.PaintSelection(f3,i9.row,i9.col,i9.rowCount,q8-i9.col,false);
}
if (i9.row+i9.rowCount-1>aa0){
this.PaintSelection(f3,aa0+1,i9.col,i9.row+i9.rowCount-aa0-1,i9.colCount,false);
}
if (i9.col+i9.colCount-1>q9){
this.PaintSelection(f3,i9.row,q9+1,i9.rowCount,i9.col+i9.colCount-q9-1,false);
}
if (newRow<v0){
this.PaintSelection(f3,newRow,newCol,v0-newRow,newColCount,true);
}
if (newCol<q8){
this.PaintSelection(f3,newRow,newCol,newRowCount,q8-newCol,true);
}
if (newRow+newRowCount-1>aa0){
this.PaintSelection(f3,aa0+1,newCol,newRow+newRowCount-aa0-1,newColCount,true);
}
if (newCol+newColCount-1>q9){
this.PaintSelection(f3,newRow,q9+1,newRowCount,newCol+newColCount-q9-1,true);
}
}
this.PaintAnchorCellHeader=function (f3,select){
var f7,i3;
f7=this.GetRowFromCell(f3,f3.e0);
i3=this.GetColFromCell(f3,f3.e0);
if (select&&f3.e0.getAttribute("group")!=null){
var r0=this.GetSpanCell(f7,i3,f3.e8);
if (r0!=null&&r0.colCount>1){
var aa1=this.GetSelectedRange(f3);
if (f7<aa1.row||f7>=aa1.row+aa1.rowCount||i3<aa1.col||i3>=aa1.col+aa1.colCount)
return ;
}
}
if (this.GetColHeader(f3)!=null)this.PaintHeaderSelection(f3,f7,i3,1,1,select,true);
if (this.GetRowHeader(f3)!=null)this.PaintHeaderSelection(f3,f7,i3,1,1,select,false);
}
this.LineIntersection=function (s1,i6,s2,n1){
var s3,h2;
s3=Math.max(s1,s2);
h2=Math.min(s1+i6,s2+n1);
if (s3<h2)
return {s:s3,c:h2-s3};
return null;
}
this.RangeIntersection=function (i5,i6,w3,cc1,z9,n1,rc2,cc2){
var aa2=this.LineIntersection(i5,w3,z9,rc2);
var aa3=this.LineIntersection(i6,cc1,n1,cc2);
if (aa2&&aa3)
return {row:aa2.s,col:aa3.s,rowCount:aa2.c,colCount:aa3.c};
return null;
}
this.PaintSelection=function (f3,f7,i3,f6,i1,select){
if (f7<0&&i3<0){
this.PaintCornerSelection(f3,select);
}
var aa4=false;
var aa5=false;
if (f7<0){
f7=0;
f6=this.GetRowCountInternal(f3);
}
if (i3<0){
i3=0;
i1=this.GetColCount(f3);
}
this.PaintViewportSelection(f3,f7,i3,f6,i1,select);
var n9=this.GetSelection(f3);
var o0;
var z9;
var n1;
var aa6;
var aa7;
var i9;
var aa8;
for (var f5=n9.childNodes.length-1;f5>=0;f5--){
o0=n9.childNodes[f5];
if (o0){
z9=parseInt(o0.getAttribute("rowIndex"));
n1=parseInt(o0.getAttribute("colIndex"));
aa6=parseInt(o0.getAttribute("rowcount"));
aa7=parseInt(o0.getAttribute("colcount"));
if (z9<0||aa6<0){z9=0;aa6=this.GetRowCountInternal(f3);}
if (n1<0||aa7<0){n1=0;aa7=this.GetColCount(f3);}
if (f5>=n9.childNodes.length-1){
if (f7<=z9&&f6>=aa6){
if (this.GetColHeader(f3)!=null&&this.GetOperationMode(f3)=="Normal"){
this.PaintHeaderSelection(f3,f7,i3,f6,i1,select,true);
aa4=true;
}
}
if (i3<=n1&&i1>=aa7){
if (this.GetRowHeader(f3)!=null){
this.PaintHeaderSelection(f3,f7,i3,f6,i1,select,false);
aa5=true;
}
}
if (!aa4&&!aa5){
if (this.GetColHeader(f3)!=null&&this.GetOperationMode(f3)=="Normal"){
this.PaintHeaderSelection(f3,f7,i3,f6,i1,select,true);
aa4=true;
}
if (this.GetRowHeader(f3)!=null){
this.PaintHeaderSelection(f3,f7,i3,f6,i1,select,false);
aa5=true;
}
}
}
else {
if (!select&&this.GetOperationMode(f3)=="Normal"){
i9=this.RangeIntersection(f7,i3,f6,i1,z9,n1,aa6,aa7);
if (i9){
this.PaintViewportSelection(f3,i9.row,i9.col,i9.rowCount,i9.colCount,true);
}
if (aa4){
aa8=this.LineIntersection(i3,i1,n1,aa7);
if (aa8)this.PaintHeaderSelection(f3,f7,aa8.s,f6,aa8.c,true,true);
}
if (aa5){
aa8=this.LineIntersection(f7,f6,z9,aa6);
if (aa8)this.PaintHeaderSelection(f3,aa8.s,i3,aa8.c,i1,true,false);
}
}
}
}
}
if (n9.childNodes.length<=0){
if (this.GetColHeader(f3)!=null&&this.GetOperationMode(f3)=="Normal")this.PaintHeaderSelection(f3,f7,i3,f6,i1,select,true);
if (this.GetRowHeader(f3)!=null)this.PaintHeaderSelection(f3,f7,i3,f6,i1,select,false);
}
this.PaintAnchorCell(f3);
}
this.PaintFocusRect=function (f3){
var h6=document.getElementById(f3.id+"_focusRectT");
if (h6==null)return ;
var aa9=this.GetSelectedRange(f3);
if (f3.e0==null&&(aa9==null||(aa9.rowCount==0&&aa9.colCount==0))){
h6.style.left="-1000px";
var t3=f3.id;
h6=document.getElementById(t3+"_focusRectB");
h6.style.left="-1000px";
h6=document.getElementById(t3+"_focusRectL");
h6.style.left="-1000px";
h6=document.getElementById(t3+"_focusRectR");
h6.style.left="-1000px";
return ;
}
var j2=this.GetOperationMode(f3);
if (j2=="RowMode"||j2=="SingleSelect"||j2=="MultiSelect"||j2=="ExtendedSelect"){
var f7=f3.GetActiveRow();
aa9=new this.Range();
this.SetRange(aa9,"Row",f7,-1,1,-1);
}else if (aa9==null||(aa9.rowCount==0&&aa9.colCount==0)){
var f7=f3.GetActiveRow();
var i3=f3.GetActiveCol();
aa9=new this.Range();
this.SetRange(aa9,"Cell",f7,i3,f3.e0.rowSpan,f3.e0.colSpan);
}
if (aa9.row<0){
aa9.row=0;
aa9.rowCount=this.GetRowCountInternal(f3);
}
if (aa9.col<0){
aa9.col=0;
aa9.colCount=this.GetColCount(f3);
}
var i4=this.GetCellFromRowCol(f3,aa9.row,aa9.col);
if (i4==null)return ;
if (aa9.rowCount==1&&aa9.colCount==1){
aa9.rowCount=i4.rowSpan;
aa9.colCount=i4.colSpan;
if (i4.colSpan>1&&!i4.getAttribute("group")){
var ab0=parseInt(i4.getAttribute("col"));
if (ab0!=aa9.col&&!isNaN(ab0))aa9.col=ab0;
}
}
var f9=this.GetOffsetTop(f3,i4);
var ab1=this.GetOffsetLeft(f3,i4);
if (i4.rowSpan>1){
aa9.row=i4.parentNode.rowIndex;
var i6=this.GetCellFromRowCol(f3,aa9.row,aa9.col+aa9.colCount-1);
if (i6!=null&&i6.parentNode.rowIndex>i4.parentNode.rowIndex){
f9=this.GetOffsetTop(f3,i6);
}
}
if (i4.colSpan>1){
var i6=this.GetCellFromRowCol(f3,aa9.row+aa9.rowCount-1,aa9.col);
var k8=this.GetOffsetLeft(f3,i6);
if (k8>ab1){
ab1=k8;
i4=i6;
}
}
var f8=0;
var i0=this.GetViewport(f3).rows;
for (var f7=aa9.row;f7<aa9.row+aa9.rowCount&&f7<i0.length;f7++){
f8+=i0[f7].offsetHeight;
if (f7>aa9.row)f8+=parseInt(this.GetViewport(f3).cellSpacing);
}
var k1=0;
var n7=this.GetColGroup(this.GetViewport(f3));
if (n7.childNodes==null||n7.childNodes.length==0)return ;
for (var i3=aa9.col;i3<aa9.col+aa9.colCount&&i3<n7.childNodes.length;i3++){
k1+=n7.childNodes[i3].offsetWidth;
if (i3>aa9.col)k1+=parseInt(this.GetViewport(f3).cellSpacing);
}
if (aa9.col>i4.cellIndex&&aa9.type=="Column"){
var n1=parseInt(i4.getAttribute("col"));
if (i4.getAttribute("group"))n1=i4.cellIndex;
for (var i3=n1;i3<aa9.col;i3++){
ab1+=n7.childNodes[i3].offsetWidth;
if (i3>n1)ab1+=parseInt(this.GetViewport(f3).cellSpacing);
}
}
if (aa9.row>0)f9-=2;
else f8-=2;
if (aa9.col>0)ab1-=2;
else k1-=2;
if (parseInt(this.GetViewport(f3).cellSpacing)>0){
f9+=1;ab1+=1;
}else {
k1+=1;
f8+=1;
}
if (k1<0)k1=0;
if (f8<0)f8=0;
h6.style.left=""+ab1+"px";
h6.style.top=""+f9+"px";
h6.style.width=""+k1+"px";
h6=document.getElementById(f3.id+"_focusRectB");
h6.style.left=""+ab1+"px";
h6.style.top=""+(f9+f8)+"px";
h6.style.width=""+k1+"px";
h6=document.getElementById(f3.id+"_focusRectL");
h6.style.left=""+ab1+"px";
h6.style.top=""+f9+"px";
h6.style.height=""+f8+"px";
h6=document.getElementById(f3.id+"_focusRectR");
h6.style.left=""+(ab1+k1)+"px";
h6.style.top=""+f9+"px";
h6.style.height=""+f8+"px";
}
this.PaintCornerSelection=function (f3,select){
var ab2=true;
if (f3.getAttribute("ShowHeaderSelection")=="false")ab2=false;
if (!ab2)return ;
var n6=this.GetCorner(f3);
if (n6!=null&&n6.rows.length>0){
for (var f5=0;f5<n6.rows.length;f5++){
for (var j1=0;j1<n6.rows[0].cells.length;j1++){
if (n6.rows[f5].cells[j1]!=null)
this.PaintSelectedCell(f3,n6.rows[f5].cells[j1],select);
}
}
}
}
this.PaintHeaderSelection=function (f3,f7,i3,f6,i1,select,d5){
var ab2=true;
if (f3.getAttribute("ShowHeaderSelection")=="false")ab2=false;
if (!ab2)return ;
var ab3=this.GetRowCountInternal(f3);
var ab4=this.GetColCount(f3);
if (d5){
if (this.GetColHeader(f3)==null)return ;
f7=0;
f6=ab3=this.GetColHeader(f3).rows.length;
}else {
if (this.GetRowHeader(f3)==null)return ;
i3=0;
i1=ab4=this.GetColGroup(this.GetRowHeader(f3)).childNodes.length;
}
var ab5=d5?f3.f0:f3.e9;
for (var f5=f7;f5<f7+f6&&f5<ab3;f5++){
if (!d5&&this.IsChildSpreadRow(f3,this.GetViewport(f3),f5))continue ;
for (var j1=i3;j1<i3+i1&&j1<ab4;j1++){
if (this.IsCovered(f3,f5,j1,ab5))continue ;
var i4=this.GetHeaderCellFromRowCol(f3,f5,j1,d5);
if (i4!=null)this.PaintSelectedCell(f3,i4,select);
}
}
}
this.PaintViewportSelection=function (f3,f7,i3,f6,i1,select){
var ab3=this.GetRowCountInternal(f3);
var ab4=this.GetColCount(f3);
for (var f5=f7;f5<f7+f6&&f5<ab3;f5++){
if (this.IsChildSpreadRow(f3,this.GetViewport(f3),f5))continue ;
var i4=null;
for (var j1=i3;j1<i3+i1&&j1<ab4;j1++){
if (this.IsCovered(f3,f5,j1,f3.e8))continue ;
i4=this.GetCellFromRowCol(f3,f5,j1,i4);
this.PaintSelectedCell(f3,i4,select);
}
}
}
this.Copy=function (f3){
var p5=this.GetPageActiveSpread();
if (p5!=null&&p5!=f3&&this.GetTopSpread(p5)==f3){
this.Copy(p5);
return ;
}
var n9=this.GetSelection(f3);
var o0=n9.lastChild;
if (o0!=null){
var f7=this.GetRowByKey(f3,o0.getAttribute("row"));
var i3=this.GetColByKey(f3,o0.getAttribute("col"));
var f6=parseInt(o0.getAttribute("rowcount"));
var i1=parseInt(o0.getAttribute("colcount"));
if (f7<0){
f7=0;
f6=this.GetRowCountInternal(f3);
}
if (i3<0){
i3=0;
i1=this.GetColCount(f3);
}
var g6="";
for (var f5=f7;f5<f7+f6;f5++){
if (this.IsChildSpreadRow(f3,this.GetViewport(f3),f5))continue ;
var i4=null;
for (var j1=i3;j1<i3+i1;j1++){
if (this.IsCovered(f3,f5,j1,f3.e8))
g6+="";
else 
{
i4=this.GetCellFromRowCol(f3,f5,j1,i4);
if (i4!=null&&i4.parentNode.getAttribute("previewrow")=="true")continue ;
var k3=this.GetCellType(i4);
if (k3=="TextCellType"&&i4.getAttribute("password")!=null)
g6+="";
else 
g6+=this.GetCellValueFromView(f3,i4);
}
if (j1+1<i3+i1)g6+="\t";
}
g6+="\r\n";
}
this.c8=g6;
}else {
if (f3.e0!=null){
var g6=this.GetCellValueFromView(f3,f3.e0);
this.c8=g6;
}
}
}
this.GetCellValueFromView=function (f3,i4){
var v2=null;
if (i4!=null){
var ab6=this.GetRender(i4);
v2=this.GetValueFromRender(f3,ab6);
if (v2==null||v2==" ")v2="";
}
return v2;
}
this.SetCellValueFromView=function (i4,v2,ignoreLock){
if (i4!=null){
var ab6=this.GetRender(i4);
var s2=this.GetCellType(i4);
if ((s2!="readonly"||ignoreLock)&&ab6!=null&&ab6.getAttribute("FpEditor")!="Button")
this.SetValueToRender(ab6,v2);
}
}
this.Paste=function (f3){
var p5=this.GetPageActiveSpread();
if (p5!=null&&p5!=f3&&this.GetTopSpread(p5)==f3){
this.Paste(p5);
return ;
}
if (f3.e0==null)return ;
var g6=this.c8;
if (g6==null)return ;
var g0=this.GetViewportFromCell(f3,f3.e0);
var f7=this.GetRowFromCell(f3,f3.e0);
var i3=this.GetColFromCell(f3,f3.e0);
var i1=this.GetColCount(f3);
var f6=this.GetRowCountInternal(f3);
var ab7=f7;
var x8=i3;
var ab8=new String(g6);
if (ab8.length==0)return ;
var f4=ab8.lastIndexOf("\r\n");
if (f4>=0&&f4==ab8.length-2)ab8=ab8.substring(0,f4);
var ab9=0;
var ac0=ab8.split("\r\n");
for (var f5=0;f5<ac0.length&&ab7<f6;f5++){
if (typeof(ac0[f5])=="string"){
ac0[f5]=ac0[f5].split("\t");
if (ac0[f5].length>ab9)ab9=ac0[f5].length;
}
ab7++;
}
ab7=this.GetSheetIndex(f3,f7);
for (var f5=0;f5<ac0.length&&ab7<f6;f5++){
var ac1=ac0[f5];
if (ac1!=null){
x8=i3;
var i4=null;
var z9=this.GetDisplayIndex(f3,ab7);
for (var j1=0;j1<ac1.length&&x8<i1;j1++){
if (!this.IsCovered(f3,z9,x8,f3.e8)){
i4=this.GetCellFromRowCol(f3,z9,x8,i4);
if (i4!=null&&i4.parentNode.getAttribute("previewrow")=="true")continue ;
if (i4==null)return ;
var ac2=ac1[j1];
if (!this.ValidateCell(f3,i4,ac2)){
if (f3.getAttribute("lcidMsg")!=null)
alert(f3.getAttribute("lcidMsg"));
else 
alert("Can't set the data into the cell. The data type is not correct for the cell.");
return ;
}
}
x8++;
}
}
ab7++;
}
if (ac0.length==0)return ;
ab7=this.GetSheetIndex(f3,f7);
for (var f5=0;f5<ac0.length&&ab7<f6;f5++){
x8=i3;
var ac1=ac0[f5];
var i4=null;
var z9=this.GetDisplayIndex(f3,ab7);
for (var j1=0;j1<ab9&&x8<i1;j1++){
if (!this.IsCovered(f3,z9,x8,f3.e8)){
i4=this.GetCellFromRowCol(f3,z9,x8,i4);
if (i4!=null&&i4.parentNode.getAttribute("previewrow")=="true")continue ;
var s2=this.GetCellType(i4);
var ab6=this.GetRender(i4);
if (s2!="readonly"&&ab6.getAttribute("FpEditor")!="Button"){
var ac2=null;
if (ac1!=null&&j1<ac1.length)ac2=ac1[j1];
this.SetCellValueFromView(i4,ac2);
if (ac2!=null){
this.SetCellValue(f3,i4,""+ac2);
}else {
this.SetCellValue(f3,i4,"");
}
}
}
x8++;
}
ab7++;
}
var v7=f3.getAttribute("autoCalc");
if (v7!="false"){
this.UpdateValues(f3);
}
var g2=this.GetTopSpread(f3);
var h0=document.getElementById(g2.id+"_textBox");
if (h0!=null){
h0.blur();
}
this.Focus(f3);
}
this.UpdateValues=function (f3){
if (f3.e7==null&&this.GetParentSpread(f3)==null&&f3.getAttribute("rowFilter")!="true"&&f3.getAttribute("hierView")!="true"&&f3.getAttribute("IsNewRow")!="true"){
this.SaveData(f3);
this.StorePostData(f3);
this.SyncData(f3.getAttribute("name"),"UpdateValues",f3);
}
}
this.ValidateCell=function (f3,i4,v2){
if (i4==null||v2==null||v2=="")return true;
var v5=null;
var k3=this.GetCellType(i4);
if (k3!=null){
var j3=this.GetFunction(k3+"_isValid");
if (j3!=null){
v5=j3(i4,v2);
}
}
return (v5==null||v5=="");
}
this.DoclearSelection=function (f3){
var n9=this.GetSelection(f3);
var o0=n9.lastChild;
while (o0!=null){
var f7=this.GetRowByKey(f3,o0.getAttribute("row"));
var i3=this.GetColByKey(f3,o0.getAttribute("col"));
var f6=parseInt(o0.getAttribute("rowcount"));
var i1=parseInt(o0.getAttribute("colcount"));
this.PaintSelection(f3,f7,i3,f6,i1,false);
n9.removeChild(o0);
o0=n9.lastChild;
}
}
this.Clear=function (f3){
var p5=this.GetPageActiveSpread();
if (p5!=null&&p5!=f3&&this.GetTopSpread(p5)==f3){
this.Clear(p5);
return ;
}
var s2=this.GetCellType(f3.e0);
if (s2=="readonly")return ;
var n9=this.GetSelection(f3);
var o0=n9.lastChild;
if (this.AnyReadOnlyCell(f3,o0)){
return ;
}
this.Copy(f3);
if (o0!=null){
var f7=this.GetRowByKey(f3,o0.getAttribute("row"));
var i3=this.GetColByKey(f3,o0.getAttribute("col"));
var f6=parseInt(o0.getAttribute("rowcount"));
var i1=parseInt(o0.getAttribute("colcount"));
if (f7<0){
f7=0;
f6=this.GetRowCountInternal(f3);
}
if (i3<0){
i3=0;
i1=this.GetColCount(f3);
}
for (var f5=f7;f5<f7+f6;f5++){
if (this.IsChildSpreadRow(f3,this.GetViewport(f3),f5))continue ;
var i4=null;
for (var j1=i3;j1<i3+i1;j1++){
if (!this.IsCovered(f3,f5,j1,f3.e8)){
i4=this.GetCellFromRowCol(f3,f5,j1,i4);
if (i4!=null&&i4.parentNode.getAttribute("previewrow")=="true")continue ;
var s2=this.GetCellType(i4);
if (s2!="readonly"){
var ac3=this.GetEditor(i4);
if (ac3!=null&&ac3.getAttribute("FpEditor")=="Button")continue ;
i4.removeAttribute("FpFormula");
this.SetCellValueFromView(i4,null);
this.SetCellValue(f3,i4,"");
}
}
}
}
var v7=f3.getAttribute("autoCalc");
if (v7!="false"){
this.UpdateValues(f3);
}
}
}
this.AnyReadOnlyCell=function (f3,o0){
if (o0!=null){
var f7=this.GetRowByKey(f3,o0.getAttribute("row"));
var i3=this.GetColByKey(f3,o0.getAttribute("col"));
var f6=parseInt(o0.getAttribute("rowcount"));
var i1=parseInt(o0.getAttribute("colcount"));
if (f7<0){
f7=0;
f6=this.GetRowCountInternal(f3);
}
if (i3<0){
i3=0;
i1=this.GetColCount(f3);
}
for (var f5=f7;f5<f7+f6;f5++){
if (this.IsChildSpreadRow(f3,this.GetViewport(f3),f5))continue ;
var i4=null;
for (var j1=i3;j1<i3+i1;j1++){
if (!this.IsCovered(f3,f5,j1,f3.e8)){
i4=this.GetCellFromRowCol(f3,f5,j1,i4);
var s2=this.GetCellType(i4);
if (s2=="readonly"){
return true;
}
}
}
}
}
return false;
}
this.MoveSliderBar=function (f3,h2){
var m9=this.GetElementById(this.activePager,f3.id+"_slideBar");
var f9=(h2.clientX-this.GetOffsetLeft(f3,f3,document.body)+window.scrollX-8);
if (f9<f3.slideLeft)f9=f3.slideLeft;
if (f9>f3.slideRight)f9=f3.slideRight;
var n3=parseInt(this.activePager.getAttribute("totalPage"))-1;
var ac4=parseInt(((f9-f3.slideLeft)/(f3.slideRight-f3.slideLeft))*n3)+1;
if (f3.style.position!="absolute"&&f3.style.position!="relative")
f9+=this.GetOffsetLeft(f3,f3,document.body)
m9.style.left=f9+"px";
return ac4;
}
this.MouseMove=function (event){
if (window.fpPostOn!=null)return ;
event=this.GetEvent(event);
var o7=this.GetTarget(event);
if (o7!=null&&o7.tagName=="scrollbar")
return ;
var f3=this.GetSpread(o7,true);
if (f3!=null&&this.dragSlideBar)
{
if (this.activePager!=null){
var ac4=this.MoveSliderBar(f3,event);
var ac5=this.GetElementById(this.activePager,f3.id+"_posIndicator");
ac5.innerHTML=this.activePager.getAttribute("pageText")+ac4;
}
return ;
}
if (this.working)f3=this.GetSpread(this.activeElement);
if (f3==null||(!this.working&&this.HitCommandBar(o7)))return ;
if (f3.getAttribute("OperationMode")=="ReadOnly")return ;
var k9=this.IsXHTML(f3);
if (this.working){
if (this.dragCol!=null&&this.dragCol>=0){
var w6=this.GetMovingCol(f3);
if (w6!=null){
if (w6.style.display=="none")w6.style.display="";
if (f3.style.position!="absolute"&&f3.style.position!="relative"){
w6.style.top=""+(event.clientY+window.scrollY)+"px";
w6.style.left=""+(event.clientX+window.scrollX+5)+"px";
}else {
w6.style.top=""+(event.clientY-this.GetOffsetTop(f3,f3,document.body)+window.scrollY)+"px";
w6.style.left=""+(event.clientX-this.GetOffsetLeft(f3,f3,document.body)+window.scrollX+5)+"px";
}
}
var g0=this.GetViewport(f3);
var ac6=document.body;
var ac7=this.GetGroupBar(f3);
var f9=-1;
var m8=event.clientX;
var v0=0;
var q8=0;
if (f3.style.position!="absolute"&&f3.style.position!="relative"){
v0=this.GetOffsetTop(f3,f3,document.body)-g0.parentNode.scrollTop;
q8=this.GetOffsetLeft(f3,f3,document.body)-g0.parentNode.scrollLeft;
m8+=Math.max(document.body.scrollLeft,document.documentElement.scrollLeft);
}else {
m8-=(this.GetOffsetLeft(f3,f3,document.body)-Math.max(document.body.scrollLeft,document.documentElement.scrollLeft));
}
var ac8=false;
var k9=this.IsXHTML(f3);
var ac9=k9?document.body.parentNode.scrollTop:document.body.scrollTop;
var l9=document.getElementById(f3.id+"_titleBar");
if (l9)ac9-=l9.parentNode.parentNode.offsetHeight;
if (this.GetPager1(f3)!=null)ac9-=this.GetPager1(f3).offsetHeight;
if (ac7!=null&&event.clientY<this.GetOffsetTop(f3,f3,document.body)-g0.parentNode.scrollTop+ac7.offsetHeight-ac9){
if (f3.style.position!="absolute"&&f3.style.position!="relative")
q8=this.GetOffsetLeft(f3,f3,document.body);
v0+=10;
ac8=true;
var w9=ac7.getElementsByTagName("TABLE")[0];
if (w9!=null){
for (var f5=0;f5<w9.rows[0].cells[0].childNodes.length;f5++){
var k1=w9.rows[0].cells[0].childNodes[f5].offsetWidth;
if (k1==null)continue ;
if (q8<=m8&&m8<q8+k1){
f9=f5;
break ;
}
q8+=k1;
}
}
if (f9==-1&&m8>=q8)f9=-2;
f3.targetCol=f9;
}else {
if (f3.style.position=="absolute"||f3.style.position=="relative")
q8=-g0.parentNode.scrollLeft;
if (this.GetRowHeader(f3)!=null)q8+=this.GetRowHeader(f3).offsetWidth;
if (ac7!=null)v0+=ac7.offsetHeight;
if (m8<q8){
f9=0;
}else {
var n7=this.GetColGroup(this.GetColHeader(f3));
if (n7!=null){
for (var f5=0;f5<n7.childNodes.length;f5++){
var k1=parseInt(n7.childNodes[f5].width);
if (k1==null)continue ;
if (q8<=m8&&m8<q8+k1){
f9=f5;
break ;
}
q8+=k1;
}
}
}
if (f9>=0&&f9!=this.dragViewCol){
if (this.dragViewCol<f9){
f9++;
if (f9<n7.childNodes.length)
q8+=k1;
}
}
q8-=5;
var ad0=parseInt(this.GetSheetColIndex(f3,f9));
if (ad0<0)ad0=f9;
f3.targetCol=ad0;
}
if (l9)v0+=l9.parentNode.parentNode.offsetHeight;
if (this.GetPager1(f3)!=null)v0+=this.GetPager1(f3).offsetHeight;
var ac5=this.GetPosIndicator(f3);
ac5.style.left=""+q8+"px";
ac5.style.top=""+v0+"px";
if (ac7!=null&&ac8&&ac7.getElementsByTagName("TABLE").length==0){
ac5.style.display="none";
}else {
if (ac8||f3.allowColMove)
ac5.style.display="";
else 
ac5.style.display="none";
}
return ;
}
if (this.c3==null&&this.c4==null){
if (f3.e0!=null){
var j5=this.GetParent(this.GetViewport(f3));
if (j5!=null){
var s4=f3.offsetTop+j5.offsetTop+j5.offsetHeight-10;
if (event.clientY>s4){
j5.scrollTop=j5.scrollTop+10;
this.ScrollView(f3);
}else if (event.clientY<f3.offsetTop+j5.offsetTop+5){
j5.scrollTop=j5.scrollTop-10;
this.ScrollView(f3);
}
var ad1=f3.offsetLeft+j5.offsetLeft+j5.offsetWidth-20;
if (event.clientX>ad1){
j5.scrollLeft=j5.scrollLeft+10;
this.ScrollView(f3);
}else if (event.clientX<f3.offsetLeft+j5.offsetLeft+5){
j5.scrollLeft=j5.scrollLeft-10;
this.ScrollView(f3);
}
}
var i4=this.GetCell(o7,null,event);
if (i4!=null&&i4!=f3.e1){
var j2=this.GetOperationMode(f3);
if (j2!="MultiSelect"){
if (j2=="SingleSelect"||j2=="RowMode"){
this.ClearSelection(f3);
var i5=this.GetRowFromCell(f3,i4);
this.UpdateAnchorCell(f3,i5,0);
this.SelectRow(f3,i5,1,true,true);
}else {
if (!(j2=="Normal"&&this.GetSelectionPolicy(f3)=="Single")){
this.Select(f3,f3.e0,i4);
this.SyncColSelection(f3);
}
}
f3.e1=i4;
}
}
}
}else if (this.c3!=null){
var ad2=event.clientX-this.c5;
var x6=parseInt(this.c3.width)+ad2;
var u9=0;
var ad3=(x6>u9);
if (ad3){
this.c3.width=x6;
var l4=parseInt(this.c3.getAttribute("index"));
this.SetWidthFix(this.GetColHeader(f3),l4,x6);
this.c5=event.clientX;
}
}else if (this.c4!=null){
var ad2=event.clientY-this.c6;
var ad4=parseInt(this.c4.style.height)+ad2;
var u9=0;
var ad3=(u9<ad4);
if (ad3){
this.c4.cells[0].style.posHeight=this.c4.cells[1].style.posHeight=(this.c4.cells[0].style.posHeight+ad2);
this.c6=event.clientY;
}
}
}else {
this.activeElement=o7;
if (this.activeElement==null||this.GetSpread(this.activeElement)!=f3)return ;
var o7=this.GetSizeColumn(f3,this.activeElement,event);
if (o7!=null){
this.c3=o7;
this.activeElement.style.cursor=this.GetResizeCursor(false);
}else {
var o7=this.GetSizeRow(f3,this.activeElement,event);
if (o7!=null){
this.c4=o7;
if (this.activeElement!=null&&this.activeElement.style!=null)this.activeElement.style.cursor=this.GetResizeCursor(true);
}else {
if (this.activeElement!=null&&this.activeElement.style!=null){
var i4=this.GetCell(this.activeElement);
if (i4!=null&&this.IsHeaderCell(f3,i4))this.activeElement.style.cursor="default";
if (this.activeElement!=null&&(this.activeElement.getAttribute("FpSpread")=="rowpadding"||this.activeElement.getAttribute("ControlType")=="chgrayarea"))
this.activeElement.style.cursor=this.GetgrayAreaCursor(f3);
}
}
}
}
}
this.GetgrayAreaCursor=function (f3){
if (f3.d2!=null&&f3.d2.style.cursor!=null){
if (f3.d2.style.cursor=="auto")
f3.d2.style.cursor="default";
return f3.d2.style.cursor;
}
else return "default";
}
this.GetResizeCursor=function (j7){
if (j7){
return "n-resize";
}else {
return "w-resize";
}
}
this.HitCommandBar=function (o7){
var f9=o7;
var f3=this.GetTopSpread(this.GetSpread(f9,true));
if (f3==null)return false;
var q4=this.GetCommandBar(f3);
while (f9!=null&&f9!=f3){
if (f9==q4)return true;
f9=f9.parentNode;
}
return false;
}
this.OpenWaitMsg=function (f3){
var j3=document.getElementById(f3.id+"_waitmsg");
if (j3==null)return ;
var k1=f3.offsetWidth;
var f8=f3.offsetHeight;
var j8=this.CreateTestBox(f3);
j8.style.fontFamily=j3.style.fontFamily;
j8.style.fontSize=j3.style.fontSize;
j8.style.fontWeight=j3.style.fontWeight;
j8.style.fontStyle=j3.style.fontStyle;
j8.innerHTML=j3.innerHTML;
j3.style.width=""+(j8.offsetWidth+2)+"px";
var ab1=Math.max(10,(k1-parseInt(j3.style.width))/2);
var f9=Math.max(10,(f8-parseInt(j3.style.height))/2);
if (f3.style.position!="absolute"&&f3.style.position!="relative"){
ab1+=f3.offsetLeft;
f9+=f3.offsetTop;
}
j3.style.top=""+f9+"px";
j3.style.left=""+ab1+"px";
j3.style.display="block";
}
this.CloseWaitMsg=function (f3){
var j3=document.getElementById(f3.id+"_waitmsg");
if (j3==null)return ;
j3.style.display="none";
this.Focus(f3);
}
this.MouseDown=function (event){
if (window.fpPostOn!=null)return ;
event=this.GetEvent(event);
var o7=this.GetTarget(event);
var f3=this.GetSpread(o7,true);
f3.mouseY=event.clientY;
var ad5=this.GetPageActiveSpread();
if (this.GetViewport(f3)==null)return ;
if (f3!=null&&o7.parentNode!=null&&o7.parentNode.getAttribute("name")==f3.id+"_slideBar"){
if (this.IsChild(o7,this.GetPager1(f3)))
this.activePager=this.GetPager1(f3);
else if (this.IsChild(o7,this.GetPager2(f3)))
this.activePager=this.GetPager2(f3);
if (this.activePager!=null){
var p0=true;
if (this.editing)p0=this.EndEdit(f3);
if (p0){
this.UpdatePostbackData(f3);
this.dragSlideBar=true;
}
}
return this.CancelDefault(event);
}
if (this.GetOperationMode(f3)=="ReadOnly")return ;
var k9=false;
if (f3!=null)k9=this.IsXHTML(f3);
if (this.editing&&f3.getAttribute("mcctCellType")!="true"){
var f9=this.GetCell(o7);
if (f9!=f3.e0){
var p0=this.EndEdit();
if (!p0)return ;
}else 
return ;
}
if (o7==this.GetParent(this.GetViewport(f3))){
if (this.GetTopSpread(ad5)!=f3){
this.SetActiveSpread(event);
}
return ;
}
var ad6=(ad5==f3);
this.SetActiveSpread(event);
ad5=this.GetPageActiveSpread();
if (this.HitCommandBar(o7))return ;
if (event.button==2)return ;
if (this.IsChild(o7,this.GetGroupBar(f3))){
var i6=parseInt(o7.id.replace(f3.id+"_group",""));
if (!isNaN(i6)){
this.dragCol=i6;
this.dragViewCol=this.GetColByKey(f3,i6);
var w6=this.GetMovingCol(f3);
w6.innerHTML=o7.innerHTML;
w6.style.width=""+Math.max(this.GetPreferredCellWidth(f3,o7),80)+"px";
if (f3.getAttribute("DragColumnCssClass")==null)
w6.style.backgroundColor=o7.style.backgroundColor;
w6.style.top="-50px";
w6.style.left="-100px";
this.working=true;
f3.dragFromGroupbar=true;
this.CancelDefault(event);
return ;
}
}
this.c3=this.GetSizeColumn(f3,o7,event);
if (this.c3!=null){
this.working=true;
this.c5=this.c6=event.clientX;
if (this.c3.style!=null)this.c3.style.cursor=this.GetResizeCursor(false);
this.activeElement=o7;
}else {
this.c4=this.GetSizeRow(f3,o7,event);
if (this.c4!=null){
this.working=true;
this.c5=this.c6=event.clientY;
this.c4.style.cursor=this.GetResizeCursor(true);
this.activeElement=o7;
}else {
var ad7=this.GetCell(o7,null,event);
if (ad7==null){
var d3=this.GetCorner(f3);
if (d3!=null&&this.IsChild(o7,d3)){
if (this.GetOperationMode(f3)=="Normal")
this.SelectTable(f3,true);
}
return ;
}
var ad8=this.GetColFromCell(f3,ad7);
if (ad7.parentNode.getAttribute("FpSpread")=="ch"&&this.GetColFromCell(f3,ad7)>=this.GetColCount(f3))return ;
if (ad7.parentNode.getAttribute("FpSpread")=="rh"&&this.IsChildSpreadRow(f3,this.GetViewport(f3),ad7.parentNode.rowIndex))return ;
if (ad7.parentNode.getAttribute("FpSpread")=="ch"&&(this.GetOperationMode(f3)=="RowMode"||this.GetOperationMode(f3)=="SingleSelect"||this.GetOperationMode(f3)=="ExtendedSelect")){
if (!f3.allowColMove&&!f3.allowGroup)
return ;
}else {
var o8=this.FireActiveCellChangingEvent(f3,this.GetRowFromCell(f3,ad7),this.GetColFromCell(f3,ad7),ad7.parentNode.getAttribute("row"));
if (o8)return ;
var v9=this.GetOperationMode(f3);
var g2=this.GetTopSpread(f3);
if (!event.ctrlKey||f3.getAttribute("multiRange")!="true"){
if (v9!="MultiSelect"){
if (!(
(f3.allowColMove||f3.allowGroup)&&ad7.parentNode.getAttribute("FpSpread")=="ch"&&
v9=="Normal"&&(f3.getAttribute("SelectionPolicy")=="Range"||f3.getAttribute("SelectionPolicy")=="MultiRange")&&
f3.selectedCols.length!=0&&this.IsColSelected(f3,ad8)
))
this.ClearSelection(f3);
}
}else {
if (v9!="ExtendedSelect"&&v9!="MultiSelect"){
if (f3.e0!=null)this.PaintSelectedCell(f3,f3.e0,true);
}
}
}
f3.e0=ad7;
var i4=f3.e0;
var y3=this.GetParent(this.GetViewport(f3));
if (y3!=null&&!this.IsControl(o7)&&(o7!=null&&o7.tagName!="scrollbar")){
if (this.IsChild(i4,y3)&&i4.offsetLeft+i4.offsetWidth>y3.scrollLeft+y3.clientWidth){
y3.scrollLeft=i4.offsetLeft+i4.offsetWidth-y3.clientWidth;
}
if (this.IsChild(i4,y3)&&i4.offsetTop+i4.offsetHeight>y3.scrollTop+y3.clientHeight&&i4.offsetHeight<y3.clientHeight){
y3.scrollTop=i4.offsetTop+i4.offsetHeight-y3.clientHeight;
}
if (i4.offsetTop<y3.scrollTop){
y3.scrollTop=i4.offsetTop;
}
if (i4.offsetLeft<y3.scrollLeft){
y3.scrollLeft=i4.offsetLeft;
}
this.ScrollView(f3);
}
if (ad7.parentNode.getAttribute("FpSpread")!="ch")this.SetActiveRow(f3,this.GetRowKeyFromCell(f3,f3.e0));
if (ad7.parentNode.getAttribute("FpSpread")=="rh")
this.SetActiveCol(f3,0);
else {
this.SetActiveCol(f3,this.GetColKeyFromCell(f3,f3.e0));
}
var v9=this.GetOperationMode(f3);
if (f3.e0.parentNode.getAttribute("FpSpread")=="r"){
if (v9=="ExtendedSelect"||v9=="MultiSelect"){
var ad9=this.IsRowSelected(f3,this.GetRowFromCell(f3,f3.e0));
if (ad9)
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,false,true);
else 
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,true,true);
}
else if (v9=="RowMode"||v9=="SingleSelect")
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,true,true);
else {
this.SelectRange(f3,this.GetRowFromCell(f3,f3.e0),this.GetColFromCell(f3,f3.e0),1,1,true);
}
f3.e4=this.GetRowFromCell(f3,f3.e0);
f3.e5=this.GetColFromCell(f3,f3.e0);
}else if (f3.e0.parentNode.getAttribute("FpSpread")=="ch"){
if (o7.tagName=="INPUT"||o7.tagName=="TEXTAREA"||o7.tagName=="SELECT")
return ;
var s7=this.GetColFromCell(f3,f3.e0);
if (f3.allowColMove||f3.allowGroup)
{
if (v9=="Normal"&&(f3.getAttribute("SelectionPolicy")=="Range"||f3.getAttribute("SelectionPolicy")=="MultiRange")){
if (this.IsColSelected(f3,s7)){
this.InitMovingCol(f3,s7);
}else 
this.SelectColumn(f3,s7,1,true);
}
}else {
if (v9=="Normal"||v9=="ReadOnly"){
this.SelectColumn(f3,s7,1,true);
}
else 
return ;
}
}else if (f3.e0.parentNode.getAttribute("FpSpread")=="rh"){
if (o7.tagName=="INPUT"||o7.tagName=="TEXTAREA"||o7.tagName=="SELECT")
return ;
if (v9=="ExtendedSelect"||v9=="MultiSelect"){
if (this.IsRowSelected(f3,this.GetRowFromCell(f3,f3.e0)))
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,false,true);
else 
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,true,true);
}else {
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,true);
}
}
if (f3.e0!=null){
var h2=this.CreateEvent("ActiveCellChanged");
h2.cmdID=f3.id;
h2.Row=h2.row=this.GetSheetIndex(f3,this.GetRowFromCell(f3,f3.e0));
h2.Col=h2.col=this.GetColFromCell(f3,f3.e0);
if (f3.getAttribute("LayoutMode"))
h2.InnerRow=h2.innerRow=f3.e0.parentNode.getAttribute("row");
this.FireEvent(f3,h2);
}
f3.e1=f3.e0;
if (f3.e0!=null){
f3.e2=this.GetRowFromCell(f3,f3.e0);
f3.e3=this.GetColFromCell(f3,f3.e0);
}
this.activeElement=o7;
this.working=true;
}
}
this.EnableButtons(f3);
if (!this.editing&&this.c4==null&&this.c3==null){
if (f3.e0!=null&&this.IsChild(f3.e0,f3)&&!this.IsHeaderCell(this.GetCell(o7))){
var j4=this.GetEditor(f3.e0);
if (j4!=null){
if (j4.type=="submit")this.SaveData(f3);
this.editing=(j4.type!="button"&&j4.type!="submit");
this.b7=j4;
this.b8=this.GetEditorValue(j4);
j4.focus();
}
}
}
if (!this.IsControl(o7)){
if (f3!=null)this.UpdatePostbackData(f3);
return this.CancelDefault(event);
}
}
this.GetMovingCol=function (f3){
var w6=document.getElementById(f3.id+"movingCol");
if (w6==null){
w6=document.createElement("DIV");
w6.style.display="none";
w6.style.position="absolute";
w6.style.top="0px";
w6.style.left="0px";
w6.id=f3.id+"movingCol";
w6.align="center";
f3.insertBefore(w6,null);
if (f3.getAttribute("DragColumnCssClass")!=null)
w6.className=f3.getAttribute("DragColumnCssClass");
else 
w6.style.border="1px solid black";
w6.style.MozOpacity=0.50;
}
return w6;
}
this.IsControl=function (f9){
return (f9!=null&&(f9.tagName=="INPUT"||f9.tagName=="TEXTAREA"||f9.tagName=="SELECT"||f9.tagName=="OPTION"));
}
this.EnableButtons=function (f3){
var s2=this.GetCellType(f3.e0);
var n9=this.GetSelection(f3);
var o0=n9.lastChild;
var t5=f3.getAttribute("OperationMode");
var ae0=t5=="ReadOnly"||t5=="SingleSelect"||s2=="readonly";
if (!ae0){
ae0=this.AnyReadOnlyCell(f3,o0);
}
if (ae0){
var g9=this.GetCmdBtn(f3,"Copy");
this.UpdateCmdBtnState(g9,o0==null);
var g6=this.c8;
g9=this.GetCmdBtn(f3,"Paste");
this.UpdateCmdBtnState(g9,(o0==null||g6==null));
g9=this.GetCmdBtn(f3,"Clear");
this.UpdateCmdBtnState(g9,true);
}else {
var g9=this.GetCmdBtn(f3,"Copy");
this.UpdateCmdBtnState(g9,o0==null);
var g6=this.c8;
g9=this.GetCmdBtn(f3,"Paste");
this.UpdateCmdBtnState(g9,(o0==null||g6==null));
g9=this.GetCmdBtn(f3,"Clear");
this.UpdateCmdBtnState(g9,o0==null);
}
}
this.CellClicked=function (i4){
var f3=this.GetSpread(i4);
if (f3!=null){
this.SaveData(f3);
}
}
this.UpdateCmdBtnState=function (g9,disabled){
if (g9==null)return ;
if (g9.tagName=="INPUT"){
var f9=g9.disabled;
if (f9==disabled)return ;
g9.disabled=disabled;
}else {
var f9=g9.getAttribute("disabled");
if (f9==disabled)return ;
g9.setAttribute("disabled",disabled);
}
if (g9.tagName=="IMG"){
var ae1=g9.getAttribute("disabledImg");
if (disabled&&ae1!=null&&ae1!=""){
if (g9.src.indexOf(ae1)<0)g9.src=ae1;
}else {
var ae2=g9.getAttribute("enabledImg");
if (g9.src.indexOf(ae2)<0)g9.src=ae2;
}
}
}
this.MouseUp=function (event){
if (window.fpPostOn!=null)return ;
event=this.GetEvent(event);
var o7=this.GetTarget(event);
var f3=this.GetSpread(o7,true);
if (f3==null&&!this.working&&!this.dragSlideBar){
return ;
}
if (this.dragSlideBar){
this.dragSlideBar=false;
if (f3==null)f3=this.GetSpread(this.activePager,true);
if (this.activePager!=null){
var ac4=this.MoveSliderBar(f3,event)-1;
this.activePager=null;
this.GotoPage(f3,ac4);
}
return ;
}
if (this.working&&(this.c3!=null||this.c4!=null)){
if (this.c3!=null)
f3=this.GetSpread(this.c3);
else 
f3=this.GetSpread(this.c4);
}
if (f3==null)return ;
if (this.GetViewport(f3)==null)return ;
var t5=this.GetOperationMode(f3);
if (t5=="ReadOnly")return ;
var j3=true;
if (this.working){
this.working=false;
if (this.dragCol!=null&&this.dragCol>=0){
var ae3=(this.IsChild(o7,this.GetGroupBar(f3))||o7==this.GetGroupBar(f3));
if (!ae3&&this.GetGroupBar(f3)!=null){
var ae4=event.clientX;
var ae5=event.clientY;
var q8=f3.offsetLeft;
var v0=f3.offsetTop;
var ae6=this.GetGroupBar(f3).offsetWidth;
var ae7=this.GetGroupBar(f3).offsetHeight;
var q3=window.scrollX;
var q2=window.scrollY;
var l9=document.getElementById(f3.id+"_titleBar");
if (l9)q2-=l9.parentNode.parentNode.offsetHeight;
if (this.GetPager1(f3)!=null)q2-=this.GetPager1(f3).offsetHeight;
ae3=(q8<=q3+ae4&&q3+ae4<=q8+ae6&&v0<=q2+ae5&&q2+ae5<=v0+ae7);
}
if (f3.dragFromGroupbar){
if (ae3){
if (f3.targetCol>0)
this.Regroup(f3,this.dragCol,parseInt((f3.targetCol+1)/2));
else 
this.Regroup(f3,this.dragCol,f3.targetCol);
}else {
this.Ungroup(f3,this.dragCol,f3.targetCol);
}
}else {
if (ae3){
if (f3.allowGroup){
if (f3.targetCol>0)
this.Group(f3,this.dragCol,parseInt((f3.targetCol+1)/2));
else 
this.Group(f3,this.dragCol,f3.targetCol);
}
}else if (f3.allowColMove){
if (f3.targetCol!=null){
var h2=this.CreateEvent("ColumnDragMove");
h2.cancel=false;
h2.col=f3.selectedCols;
this.FireEvent(f3,h2);
if (!h2.cancel){
this.MoveCol(f3,this.dragCol,f3.targetCol);
var h2=this.CreateEvent("ColumnDragMoveCompleted");
h2.col=f3.selectedCols;
this.FireEvent(f3,h2);
}
}
}
}
var w6=this.GetMovingCol(f3);
if (w6!=null)
w6.style.display="none";
this.dragCol=-1;
this.dragViewCol=-1;
var ac5=this.GetPosIndicator(f3);
if (ac5!=null)
ac5.style.display="none";
f3.dragFromGroupbar=false;
f3.targetCol=null;
this.c3=this.c4=null;
}
if (this.c3!=null){
j3=false;
var ad2=event.clientX-this.c5;
var x6=parseInt(this.c3.width);
var ae8=x6;
if (isNaN(x6))x6=0;
x6+=ad2;
if (x6<1)x6=1;
var l4=parseInt(this.c3.getAttribute("index"));
var ae9=this.GetColGroup(this.GetViewport(f3));
if (ae9!=null&&ae9.childNodes.length>0){
ae8=parseInt(ae9.childNodes[l4].width);
}else {
ae8=1;
}
if (this.GetViewport(f3).rules!="rows"){
if (l4==0)ae8+=1;
if (l4==parseInt(this.colCount)-1)ae8-=1;
}
if (x6!=ae8&&event.clientX!=this.c6){
this.SetColWidth(f3,l4,x6);
var h2=this.CreateEvent("ColWidthChanged");
h2.col=l4;
h2.width=x6;
this.FireEvent(f3,h2);
}
this.ScrollView(f3);
this.PaintFocusRect(f3);
}else if (this.c4!=null){
j3=false;
var ad2=event.clientY-this.c6;
var ad4=this.c4.offsetHeight+ad2;
if (ad4<1){
ad4=1;
ad2=1-this.c4.offsetHeight;
}
this.c4.cells[0].style.posHeight=this.c4.cells[1].style.posHeight=ad4;
this.c4.style.cursor="auto";
var j5=null;
j5=this.GetViewport(f3);
if (typeof(j5.rows[this.c4.rowIndex])!="undefined"&&
typeof(j5.rows[this.c4.rowIndex].cells[0])!="undefined")
{
j5.rows[this.c4.rowIndex].cells[0].style.height=this.c4.cells[0].style.height;
}
var q7=this.AddRowInfo(f3,this.c4.getAttribute("FpKey"));
if (q7!=null){
if (this.c4.cells[0])
this.SetRowHeight(f3,q7,parseInt(this.c4.cells[0].style.posHeight));
else 
this.SetRowHeight(f3,q7,parseInt(this.c4.style.height));
}
if (this.c5!=event.clientY){
var h2=this.CreateEvent("RowHeightChanged");
h2.row=this.GetRowFromCell(f3,this.c4.cells[0]);
h2.height=this.c4.offsetHeight;
this.FireEvent(f3,h2);
}
var j6=this.GetParentSpread(f3);
if (j6!=null)this.UpdateRowHeight(j6,f3);
var g2=this.GetTopSpread(f3);
this.SizeAll(g2);
this.Refresh(g2);
this.ScrollView(f3);
this.PaintFocusRect(f3);
}else {
}
if (this.activeElement!=null){
this.activeElement=null;
}
}
if (j3)j3=!this.IsControl(o7);
if (j3&&this.HitCommandBar(o7))return ;
var af0=false;
var n9=this.GetSelection(f3);
if (n9!=null){
var o0=n9.firstChild;
var i9=new this.Range();
if (o0!=null){
i9.row=this.GetRowByKey(f3,o0.getAttribute("row"));
i9.col=this.GetColByKey(f3,o0.getAttribute("col"));
i9.rowCount=parseInt(o0.getAttribute("rowcount"));
i9.colCount=parseInt(o0.getAttribute("colcount"));
}
switch (f3.e6){
case "":
var i0=this.GetViewport(f3).rows;
for (var f5=i9.row;f5<i9.row+i9.rowCount&&f5<i0.length;f5++){
if (i0[f5].cells.length>0&&i0[f5].cells[0].firstChild!=null&&i0[f5].cells[0].firstChild.nodeName!="#text"){
if (i0[f5].cells[0].firstChild.getAttribute("FpSpread")=="Spread"){
af0=true;
break ;
}
}
}
break ;
case "c":
var j5=this.GetViewport(f3);
for (var f5=0;f5<j5.rows.length;f5++){
if (this.IsChildSpreadRow(f3,j5,f5)){
af0=true;
break ;
}
}
break ;
case "r":
var j5=this.GetViewport(f3);
var w2=i9.rowCount;
for (var f5=i9.row;f5<i9.row+w2&&f5<j5.rows.length;f5++){
if (this.IsChildSpreadRow(f3,j5,f5)){
af0=true;
break ;
}
}
}
}
if (af0){
var g9=this.GetCmdBtn(f3,"Copy");
this.UpdateCmdBtnState(g9,true);
g9=this.GetCmdBtn(f3,"Paste");
this.UpdateCmdBtnState(g9,true);
g9=this.GetCmdBtn(f3,"Clear");
this.UpdateCmdBtnState(g9,true);
}
var k9=this.IsXHTML(f3);
if (k9){
var g2=this.GetTopSpread(f3);
var h0=document.getElementById(g2.id+"_textBox");
if (h0!=null){
h0.style.top=event.clientY-f3.offsetTop;
h0.style.left=event.clientX-f3.offsetLeft;
}
}
if (j3)this.Focus(f3);
}
this.UpdateRowHeight=function (j6,child){
var n0=child.parentNode;
while (n0!=null){
if (n0.tagName=="TR")break ;
n0=n0.parentNode;
}
var k9=this.IsXHTML(j6);
if (n0!=null){
var f4=n0.rowIndex;
if (this.GetRowHeader(j6)!=null){
var q5=0;
if (this.GetColHeader(child)!=null)q5=this.GetColHeader(child).offsetHeight;
if (this.GetRowHeader(child)!=null)q5+=this.GetRowHeader(child).offsetHeight;
if (!k9)q5-=this.GetViewport(j6).cellSpacing;
if (this.GetViewport(j6).cellSpacing==0){
this.GetRowHeader(j6).rows[f4].cells[0].style.posHeight=q5;
if (this.GetParentSpread(j6)!=null){
this.GetRowHeader(j6).parentNode.style.posHeight=this.GetRowHeader(j6).offsetHeight;
}
}
else 
this.GetRowHeader(j6).rows[f4].cells[0].style.posHeight=(q5+2);
this.GetViewport(j6).rows[f4].cells[0].style.posHeight=q5;
if (!k9)q5-=1;
child.style.posHeight=q5;
}
}
var af1=this.GetParentSpread(j6);
if (af1!=null)
this.UpdateRowHeight(af1,j6);
}
this.MouseOut=function (){
if (!this.working&&this.c3!=null&&this.c3.style!=null)this.c3.style.cursor="auto";
}
this.KeyDown=function (f3,event){
if (window.fpPostOn!=null)return ;
if (!f3.ProcessKeyMap(event)){
if (event.keyCode==this.enter)this.CancelDefault(event);
return ;
}
if (event.keyCode==this.space&&f3.e0!=null){
var v9=this.GetOperationMode(f3);
if (v9=="MultiSelect"){
if (this.IsRowSelected(f3,this.GetRowFromCell(f3,f3.e0)))
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,false,true);
else 
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,true,true);
return ;
}
}
var j4=false;
if (this.editing&&this.b7!=null){
var af2=this.GetEditor(this.b7);
j4=(af2!=null);
}
if (event.keyCode!=this.enter&&event.keyCode!=this.tab&&(this.editing&&!j4)&&this.b7.tagName=="SELECT")return ;
switch (event.keyCode){
case this.left:
case this.right:
if (j4){
var af3=this.b7.getAttribute("FpEditor");
if (this.editing&&af3=="ExtenderEditor"){
var af4=FpExtender.Util.getEditor(this.b7);
if (af4&&af4.type!="text")this.EndEdit();
}
if (af3!="RadioButton"&&af3!="ExtenderEditor")this.EndEdit();
}
if (!this.editing){
this.NextCell(f3,event,event.keyCode);
}
break ;
case this.up:
case this.down:
case this.enter:
if (this.b7!=null&&this.b7.tagName=="TEXTAREA")return ;
if (j4&&this.editing&&this.b7.getAttribute("FpEditor")=="ExtenderEditor"){
var af5=this.b7.getAttribute("Extenders");
if (af5&&af5.indexOf("AutoCompleteExtender")!=-1)return ;
}
if (event.keyCode==this.enter)this.CancelDefault(event);
if (this.editing){
var p0=this.EndEdit();
if (!p0)return ;
}
this.NextCell(f3,event,event.keyCode);
var g2=this.GetTopSpread(f3);
var h0=document.getElementById(g2.id+"_textBox");
if (this.enter==event.keyCode)h0.focus();
break ;
case this.tab:
if (this.editing){
var p0=this.EndEdit();
if (!p0)return ;
}
var o9=this.GetProcessTab(f3);
var af6=(o9=="true"||o9=="True");
if (af6)this.NextCell(f3,event,event.keyCode);
break ;
case this.shift:
break ;
case this.home:
case this.end:
case this.pup:
case this.pdn:
this.CancelDefault(event);
if (!this.editing){
this.NextCell(f3,event,event.keyCode);
}
break ;
default :
if (event.keyCode==67&&event.ctrlKey&&(!this.editing||j4))this.Copy(f3);
else if (event.keyCode==86&&event.ctrlKey&&(!this.editing||j4))this.Paste(f3);
else if (event.keyCode==88&&event.ctrlKey&&(!this.editing||j4))this.Clear(f3);
else if (!this.editing&&f3.e0!=null&&!this.IsHeaderCell(f3.e0)&&!event.ctrlKey&&!event.altKey){
this.StartEdit(f3,f3.e0);
}
break ;
}
}
this.GetProcessTab=function (f3){
var g2=this.GetTopSpread(f3);
return g2.getAttribute("ProcessTab");
}
this.ExpandRow=function (f3,j7){
var t3=f3.getAttribute("name");
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"ExpandView,"+j7,f3);
else 
__doPostBack(t3,"ExpandView,"+j7);
}
this.SortColumn=function (f3,column){
var t3=f3.getAttribute("name");
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"SortColumn,"+column,f3);
else 
__doPostBack(t3,"SortColumn,"+column);
}
this.Filter=function (event,f3){
var o7=this.GetTarget(event);
var f9=o7.value;
if (o7.tagName=="SELECT"){
var z5=new RegExp("\\s*");
var af7=new RegExp("\\S*");
var t6=o7[o7.selectedIndex].text;
var af8="";
var f5=0;
var f4=f9.length;
while (f4>0){
var i5=f9.match(z5);
if (i5!=null){
af8+=i5[0];
f5=i5[0].length;
f4-=f5;
f9=f9.substring(f5);
i5=f9.match(af7);
if (i5!=null){
f5=i5[0].length;
f4-=f5;
f9=f9.substring(f5);
}
}else {
break ;
}
i5=t6.match(af7);
if (i5!=null){
af8+=i5[0];
f5=i5[0].length;
t6=t6.substring(f5);
i5=t6.match(z5);
if (i5!=null){
f5=i5[0].length;
t6=t6.substring(f5);
}
}else {
break ;
}
}
f9=af8;
}
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(o7.name,f9,f3);
else 
__doPostBack(o7.name,f9);
}
this.MoveCol=function (f3,from,to){
var t3=f3.getAttribute("name");
if (f3.selectedCols&&f3.selectedCols.length>0){
var af9=[];
for (var f5=0;f5<f3.selectedCols.length;f5++)
af9[f5]=this.GetSheetColIndex(f3,f3.selectedCols[f5]);
var ag0=af9.join("+");
this.MoveMultiCol(f3,ag0,to);
return ;
}
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"MoveCol,"+from+","+to,f3);
else 
__doPostBack(t3,"MoveCol,"+from+","+to);
}
this.MoveMultiCol=function (f3,ag0,to){
var t3=f3.getAttribute("name");
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"MoveCol,"+ag0+","+to,f3);
else 
__doPostBack(t3,"MoveCol,"+ag0+","+to);
}
this.Group=function (f3,n8,toCol){
var t3=f3.getAttribute("name");
if (f3.selectedCols&&f3.selectedCols.length>0){
var af9=[];
for (var f5=0;f5<f3.selectedCols.length;f5++)
af9[f5]=this.GetSheetColIndex(f3,f3.selectedCols[f5]);
var ag0=af9.join("+");
this.GroupMultiCol(f3,ag0,toCol);
f3.selectedCols=[];
return ;
}
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"Group,"+n8+","+toCol,f3);
else 
__doPostBack(t3,"Group,"+n8+","+toCol);
}
this.GroupMultiCol=function (f3,ag0,toCol){
var t3=f3.getAttribute("name");
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"Group,"+ag0+","+toCol,f3);
else 
__doPostBack(t3,"Group,"+ag0+","+toCol);
}
this.Ungroup=function (f3,n8,toCol){
var t3=f3.getAttribute("name");
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"Ungroup,"+n8+","+toCol,f3);
else 
__doPostBack(t3,"Ungroup,"+n8+","+toCol);
}
this.Regroup=function (f3,fromCol,toCol){
var t3=f3.getAttribute("name");
var z6=(f3.getAttribute("ajax")!="false");
if (z6)
this.SyncData(t3,"Regroup,"+fromCol+","+toCol,f3);
else 
__doPostBack(t3,"Regroup,"+fromCol+","+toCol);
}
this.ProcessData=function (){
try {
var ag1=this;
ag1.removeEventListener("load",the_fpSpread.ProcessData,false);
var o7=window.srcfpspread;
o7=o7.split(":").join("_");
var ag2=window.fpcommand;
var ag3=document;
var ag4=ag3.getElementById(o7+"_buff");
if (ag4==null){
ag4=ag3.createElement("iframe");
ag4.id=o7+"_buff";
ag4.style.display="none";
ag3.body.appendChild(ag4);
}
var f3=ag3.getElementById(o7);
the_fpSpread.CloseWaitMsg(f3);
if (ag4==null)return ;
var ag5=ag1.responseText;
ag4.contentWindow.document.body.innerHTML=ag5;
var o9=ag4.contentWindow.document.getElementById(o7+"_values");
if (o9!=null){
var t2=o9.getElementsByTagName("data")[0];
var o0=t2.firstChild;
the_fpSpread.error=false;
f3.SuspendLayout(true);
while (o0!=null){
var f7=the_fpSpread.GetRowByKey(f3,o0.getAttribute("r"));
var i3=the_fpSpread.GetColByKey(f3,o0.getAttribute("c"));
var z8=the_fpSpread.GetValue(f3,f7,i3);
if (o0.innerHTML!=z8){
var j3=the_fpSpread.GetFormula(f3,f7,i3);
var k0=the_fpSpread.GetCellByRowCol(f3,f7,i3);
the_fpSpread.SetCellValueFromView(k0,o0.innerHTML,true);
k0.setAttribute("FpFormula",j3);
}
o0=o0.nextSibling;
}
the_fpSpread.ClearCellData(f3);
f3.ResumeLayout(true);
}else {
the_fpSpread.UpdateSpread(ag3,ag4,o7,ag5,ag2);
}
var z7=ag3.getElementsByTagName("FORM");
z7[0].__EVENTTARGET.value="";
z7[0].__EVENTARGUMENT.value="";
var z8=ag3.getElementsByName("__VIEWSTATE")[0];
var f9=ag4.contentWindow.document.getElementsByName("__VIEWSTATE")[0];
if (z8!=null&&f9!=null)z8.value=f9.value;
z8=ag3.getElementsByName("__EVENTVALIDATION");
f9=ag4.contentWindow.document.getElementsByName("__EVENTVALIDATION");
if (z8!=null&&f9!=null&&z8.length>0&&f9.length>0)
z8[0].value=f9[0].value;
ag4.contentWindow.document.location="about:blank";
window.fpPostOn=null;
e7=null;
}catch (h2){
window.fpPostOn=null;
e7=null;
}
the_fpSpread.ss=f3;
setTimeout(the_fpSpread.UpdateImages,0);
var f3=the_fpSpread.GetTopSpread(ag3.getElementById(o7));
var h2=the_fpSpread.CreateEvent("CallBackStopped");
h2.command=ag2;
the_fpSpread.FireEvent(f3,h2);
};
this.UpdateImages=function (){
if (the_fpSpread.ss==null)return ;
var ag6=document.images;
if (ag6!=null){
for (var f5=0;f5<ag6.length;f5++){
ag6[f5].src=ag6[f5].src;
}
}
var ag7=the_fpSpread.GetColHeader(the_fpSpread.ss);
if (ag7!=null&&ag7.rows.length>0&&ag7.rows[0].cells.length>0){
var f9=ag7.rows[0].cells[ag7.rows[0].cells.length-1];
f9.style.backgroundImage=f9.style.backgroundImage;
}
var ag8=the_fpSpread.GetRowHeader(the_fpSpread.ss);
if (ag8!=null&&ag8.rows.length>0&&ag8.rows[ag8.rows.length-1].cells.length>0){
var f9=ag8.rows[ag8.rows.length-1].cells[0];
f9.style.backgroundImage=f9.style.backgroundImage;
}
the_fpSpread.ss=null;
}
this.UpdateSpread=function (ag3,ag4,o7,ag5,ag2){
var f3=the_fpSpread.GetTopSpread(ag3.getElementById(o7));
var s3=ag4.contentWindow.document.getElementById(f3.id);
if (s3!=null){
if (typeof(Sys)!='undefined'){
FarPoint.System.ExtenderHelper.saveLoadedExtenderScripts(f3,ag2);
}
the_fpSpread.error=(s3.getAttribute("error")=="true");
f3.LoadState=null;
if (ag2=="LoadOnDemand"&&!the_fpSpread.error){
var ag9=this.GetElementById(f3,f3.id+"_data");
var ah0=this.GetElementById(s3,f3.id+"_data");
if (ag9!=null&&ah0!=null)ag9.setAttribute("data",ah0.getAttribute("data"));
var ah1=s3.getElementsByTagName("style");
if (ah1!=null){
for (var f5=0;f5<ah1.length;f5++){
if (ah1[f5]!=null&&ah1[f5].innerHTML!=null&&ah1[f5].innerHTML.indexOf(f3.id+"msgStyle")<0)
f3.appendChild(ah1[f5].cloneNode(true));
}
}
var ah2=this.GetElementById(f3,f3.id+"_LoadInfo");
var ah3=this.GetElementById(s3,f3.id+"_LoadInfo");
if (ah2!=null&&ah3!=null)ah2.value=ah3.value;
var ah4=false;
var ah5=this.GetElementById(s3,f3.id+"_rowHeader");
if (ah5!=null){
ah5=ah5.firstChild;
ah4=(ah5.rows.length>1);
var k6=this.GetRowHeader(f3);
this.LoadRows(k6,ah5,true);
}
var ah6=this.GetElementById(s3,f3.id+"_viewport");
if (ah6!=null){
ah4=(ah6.rows.length>0);
var g0=this.GetViewport(f3);
this.LoadRows(g0,ah6,false);
}
var ah7=f3.e2;
var ah8=f3.e3;
var o1=f3.e6;
the_fpSpread.Init(f3,ag2);
the_fpSpread.LoadScrollbarState(f3);
the_fpSpread.Focus(f3);
f3.e2=ah7;
f3.e3=ah8;
f3.e6=o1;
if (ah4)
f3.LoadState=null;
else 
f3.LoadState="complete";
if (typeof(Sys)!='undefined'){
FarPoint.System.ExtenderHelper.loadExtenderScripts(f3,ag4.contentWindow.document.documentElement);
}
}else {
f3.innerHTML=s3.innerHTML;
the_fpSpread.CopySpreadAttrs(s3,f3);
if (typeof(Sys)!='undefined'){
FarPoint.System.ExtenderHelper.loadExtenderScripts(f3,ag4.contentWindow.document.documentElement);
}
var ah9=ag4.contentWindow.document.getElementById(f3.id+"_initScript");
eval(ah9.value);
for (var f5=0;f5<ag4.contentWindow.document.styleSheets.length;f5++){
for (var j1=0;j1<ag4.contentWindow.document.styleSheets[f5].rules.length;j1++){
var ai0=ag4.contentWindow.document.styleSheets[f5].rules[j1];
var ai1={styleSheetIndex:-1,ruleIndex:-1};
for (var ai2=0;ai2<ag3.styleSheets.length;ai2++){
for (var ai3=0;ai3<ag3.styleSheets[ai2].rules.length;ai3++){
if (ag3.styleSheets[ai2].rules[ai3].selectorText==ai0.selectorText){
ai1.styleSheetIndex=ai2;
ai1.ruleIndex=ai3;
}
}
}
if (ai1.styleSheetIndex>-1&&ai1.ruleIndex>-1)
ag3.styleSheets[ai1.styleSheetIndex].deleteRule(ai1.ruleIndex);
ag3.styleSheets[0].addRule(ai0.selectorText,ai0.style.cssText);
}
}
}
}else {
the_fpSpread.error=true;
}
}
this.LoadRows=function (g0,ah6,isHeader){
if (g0==null||ah6==null)return ;
var ai4=g0.tBodies[0];
var w2=ah6.rows.length;
var ai5=null;
if (isHeader){
w2--;
if (ai4.rows.length>0)ai5=ai4.rows[ai4.rows.length-1];
}
for (var f5=0;f5<w2;f5++){
var ai6=ah6.rows[f5].cloneNode(false);
ai4.insertBefore(ai6,ai5);
ai6.innerHTML=ah6.rows[f5].innerHTML;
}
if (!isHeader){
for (var f5=0;f5<ah6.parentNode.childNodes.length;f5++){
var z2=ah6.parentNode.childNodes[f5];
if (z2!=ah6){
g0.parentNode.insertBefore(z2.cloneNode(true),null);
}
}
}
}
this.CopySpreadAttr=function (ab3,dest,attrName){
var ai7=ab3.getAttribute(attrName);
var ai8=dest.getAttribute(attrName);
if (ai7!=null||ai8!=null){
if (ai7==null)
dest.removeAttribute(attrName);
else 
dest.setAttribute(attrName,ai7);
}
}
this.CopySpreadAttrs=function (ab3,dest){
this.CopySpreadAttr(ab3,dest,"totalRowCount");
this.CopySpreadAttr(ab3,dest,"pageCount");
this.CopySpreadAttr(ab3,dest,"loadOnDemand");
this.CopySpreadAttr(ab3,dest,"allowGroup");
this.CopySpreadAttr(ab3,dest,"colMove");
this.CopySpreadAttr(ab3,dest,"showFocusRect");
this.CopySpreadAttr(ab3,dest,"FocusBorderColor");
this.CopySpreadAttr(ab3,dest,"FocusBorderStyle");
this.CopySpreadAttr(ab3,dest,"FpDefaultEditorID");
this.CopySpreadAttr(ab3,dest,"hierView");
this.CopySpreadAttr(ab3,dest,"IsNewRow");
this.CopySpreadAttr(ab3,dest,"cmdTop");
this.CopySpreadAttr(ab3,dest,"ProcessTab");
this.CopySpreadAttr(ab3,dest,"AcceptFormula");
this.CopySpreadAttr(ab3,dest,"EditMode");
this.CopySpreadAttr(ab3,dest,"AllowInsert");
this.CopySpreadAttr(ab3,dest,"AllowDelete");
this.CopySpreadAttr(ab3,dest,"error");
this.CopySpreadAttr(ab3,dest,"ajax");
this.CopySpreadAttr(ab3,dest,"autoCalc");
this.CopySpreadAttr(ab3,dest,"multiRange");
this.CopySpreadAttr(ab3,dest,"rowFilter");
this.CopySpreadAttr(ab3,dest,"OperationMode");
this.CopySpreadAttr(ab3,dest,"selectedForeColor");
this.CopySpreadAttr(ab3,dest,"selectedBackColor");
this.CopySpreadAttr(ab3,dest,"anchorBackColor");
this.CopySpreadAttr(ab3,dest,"columnHeaderAutoTextIndex");
this.CopySpreadAttr(ab3,dest,"SelectionPolicy");
this.CopySpreadAttr(ab3,dest,"ShowHeaderSelection");
this.CopySpreadAttr(ab3,dest,"EnableRowEditTemplate");
this.CopySpreadAttr(ab3,dest,"scrollContent");
this.CopySpreadAttr(ab3,dest,"scrollContentColumns");
this.CopySpreadAttr(ab3,dest,"scrollContentTime");
this.CopySpreadAttr(ab3,dest,"scrollContentMaxHeight");
dest.tabIndex=ab3.tabIndex;
if (dest.style!=null&&ab3.style!=null){
if (dest.style.width!=ab3.style.width)dest.style.width=ab3.style.width;
if (dest.style.height!=ab3.style.height)dest.style.height=ab3.style.height;
if (dest.style.border!=ab3.style.border)dest.style.border=ab3.style.border;
}
}
this.Clone=function (m8){
var f9=document.createElement(m8.tagName);
f9.id=m8.id;
var i3=m8.firstChild;
while (i3!=null){
var k8=this.Clone(i3);
f9.appendChild(k8);
i3=i3.nextSibling;
}
return f9;
}
this.FireEvent=function (f3,h2){
if (f3==null||h2==null)return ;
var g2=this.GetTopSpread(f3);
if (g2!=null){
h2.spread=f3;
g2.dispatchEvent(h2);
}
}
this.SyncData=function (t3,ag2,f3,asyncCallBack){
if (window.fpPostOn!=null){
return ;
}
var h2=this.CreateEvent("CallBackStart");
h2.cancel=false;
h2.command=ag2;
if (asyncCallBack==null)asyncCallBack=false;
h2.async=asyncCallBack;
if (f3==null){
var k8=t3.split(":").join("_");
f3=document.getElementById(k8);
}
if (f3!=null){
var g2=this.GetTopSpread(f3);
this.FireEvent(f3,h2);
}
if (h2.cancel){
the_fpSpread.ClearCellData(f3);
return ;
}
if (ag2!=null&&(ag2.indexOf("SelectView,")==0||ag2=="Next"||ag2=="Prev"||ag2.indexOf("Group,")==0||ag2.indexOf("Page,")==0))
f3.LoadState=null;
var ai9=h2.async;
if (ai9){
this.OpenWaitMsg(f3);
}
window.fpPostOn=true;
if (this.error)ag2="update";
try {
var z7=document.getElementsByTagName("FORM");
if (z7==null&&z7.length==0)return ;
z7[0].__EVENTTARGET.value=t3;
z7[0].__EVENTARGUMENT.value=encodeURIComponent(ag2);
var aj0=z7[0].action;
var f9;
if (aj0.indexOf("?")>-1){
f9="&";
}
else 
{
f9="?";
}
aj0=aj0+f9;
var g6=this.CollectData();
var ag5="";
var ag1=(window.XMLHttpRequest)?new XMLHttpRequest():new ActiveXObject("Microsoft.XMLHTTP");
if (ag1==null)return ;
ag1.open("POST",aj0,ai9);
ag1.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
if (f3!=null)
window.srcfpspread=f3.id;
else 
window.srcfpspread=t3;
window.fpcommand=ag2;
this.AttachEvent(ag1,"load",the_fpSpread.ProcessData,false);
ag1.send(g6);
}catch (h2){
window.fpPostOn=false;
e7=null;
}
};
this.CollectData=function (){
var z7=document.getElementsByTagName("FORM");
var f9;
var h5="fpcallback=true&";
for (var f5=0;f5<z7[0].elements.length;f5++){
f9=z7[0].elements[f5];
var aj1=f9.tagName.toLowerCase();
if (aj1=="input"){
var aj2=f9.type;
if (aj2=="hidden"||aj2=="text"||aj2=="password"||((aj2=="checkbox"||aj2=="radio")&&f9.checked)){
h5+=(f9.name+"="+encodeURIComponent(f9.value)+"&");
}
}else if (aj1=="select"){
if (f9.childNodes!=null){
for (var j1=0;j1<f9.childNodes.length;j1++){
var q6=f9.childNodes[j1];
if (q6!=null&&q6.tagName!=null&&q6.tagName.toLowerCase()=="option"&&q6.selected){
h5+=(f9.name+"="+encodeURIComponent(q6.value)+"&");
}
}
}
}else if (aj1=="textarea"){
h5+=(f9.name+"="+encodeURIComponent(f9.value)+"&");
}
}
return h5;
};
this.ClearCellData=function (f3){
var g6=this.GetData(f3);
var aj3=g6.getElementsByTagName("root")[0];
var g7=aj3.getElementsByTagName("data")[0];
if (g7==null)return null;
if (f3.e7!=null){
var j7=f3.e7.firstChild;
while (j7!=null){
var f7=j7.getAttribute("key");
var aj4=j7.firstChild;
while (aj4!=null){
var i3=aj4.getAttribute("key");
var aj5=g7.firstChild;
while (aj5!=null){
var i5=aj5.getAttribute("key");
if (f7==i5){
var aj6=false;
var aj7=aj5.firstChild;
while (aj7!=null){
var i6=aj7.getAttribute("key");
if (i3==i6){
aj5.removeChild(aj7);
aj6=true;
break ;
}
aj7=aj7.nextSibling;
}
if (aj6)break ;
}
aj5=aj5.nextSibling;
}
aj4=aj4.nextSibling;
}
j7=j7.nextSibling;
}
}
f3.e7=null;
var g9=this.GetCmdBtn(f3,"Cancel");
if (g9!=null)
this.UpdateCmdBtnState(g9,true);
}
this.StorePostData=function (f3){
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
var ac2=g7.getElementsByTagName("data")[0];
if (ac2!=null)f3.e7=ac2.cloneNode(true);
}
this.ShowMessage=function (f3,v5,j7,n8,time){
var f6=f3.GetRowCount();
var i1=f3.GetColCount();
if (j7==null||n8==null||j7<0||j7>=f6||n8<0||n8>=i1){
j7=-1;
n8=-1;
}
this.ShowMessageInner(f3,v5,j7,n8,time);
}
this.HideMessage=function (f3,j7,n8){
var f6=f3.GetRowCount();
var i1=f3.GetColCount();
if (j7==null||n8==null||j7<0||j7>=f6||n8<0||n8>=i1)
if (f3.msgList&&f3.msgList.centerMsg&&f3.msgList.centerMsg.msgBox.IsVisible)
f3.msgList.centerMsg.msgBox.Hide();
var aj8=this.GetMsgObj(f3,j7,n8);
if (aj8&&aj8.msgBox.IsVisible){
aj8.msgBox.Hide();
}
}
this.ShowMessageInner=function (f3,v5,j7,n8,time){
var aj8=this.GetMsgObj(f3,j7,n8);
if (aj8){
if (aj8.timer)
aj8.msgBox.Hide();
}
else 
aj8=this.CreateMsgObj(f3,j7,n8);
var aj9=aj8.msgBox;
aj9.Show(f3,this,v5);
if (time&&time>0)
aj8.timer=setTimeout(function (){aj9.Hide();},time);
this.SetMsgObj(f3,aj8);
}
this.GetMsgObj=function (f3,j7,n8){
var aj8;
var ak0=f3.msgList;
if (ak0){
if (j7==-1&&n8==-1)
aj8=ak0.centerMsg;
else if (j7==-2)
aj8=ak0.hScrollMsg;
else if (n8==-2)
aj8=ak0.vScrollMsg;
else {
if (ak0[j7])
aj8=ak0[j7][n8];
}
}
return aj8;
}
this.SetMsgObj=function (f3,aj8){
var ak0=f3.msgList;
if (aj8.row==-1&&aj8.col==-1)
ak0.centerMsg=aj8;
else if (aj8.row==-2)
ak0.hScrollMsg=aj8;
else if (aj8.col==-2)
ak0.vScrollMsg=aj8;
else {
if (!ak0[aj8.row])ak0[aj8.row]=new Array();
ak0[aj8.row][aj8.col]=aj8;
}
}
var ak1=null;
this.CreateMsgObj=function (f3,j7,n8){
var aj9=document.createElement("div");
var aj8={row:j7,col:n8,msgBox:aj9};
var ak2=null;
if (j7!=-2&&n8!=-2){
aj9.style.border="1px solid black";
aj9.style.background="yellow";
aj9.style.color="red";
}
else {
aj9.style.border="1px solid #55678e";
aj9.style.fontSize="small";
aj9.style.background="#E6E9ED";
aj9.style.color="#4c5b7f";
this.GetScrollingContentStyle(f3);
ak2=ak1;
}
if (ak2!=null){
if (ak2.fontFamily!=null)
aj9.style.fontFamily=ak2.fontFamily;
if (ak2.fontSize!=null)
aj9.style.fontSize=ak2.fontSize;
if (ak2.fontStyle!=null)
aj9.style.fontStyle=ak2.fontStyle;
if (ak2.fontVariant!=null)
aj9.style.fontVariant=ak2.fontVariant;
if (ak2.fontWeight!=null)
aj9.style.fontWeight=ak2.fontWeight;
if (ak2.backgroundColor!=null)
aj9.style.backgroundColor=ak2.backgroundColor;
if (ak2.color!=null)
aj9.style.color=ak2.color;
}
aj9.style.position="absolute";
aj9.style.overflow="hidden";
aj9.style.display="block";
aj9.style.marginLeft=0;
aj9.style.marginTop=2;
aj9.style.marginRight=0;
aj9.style.marginBottom=0;
aj9.msgObj=aj8;
aj9.Show=function (s3,fpObj,v5){
var w4=fpObj.GetMsgPos(s3,this.msgObj.row,this.msgObj.col);
var g1=fpObj.GetCommandBar(s3);
var ak3=fpObj.GetGroupBar(s3);
this.style.visibility="visible";
this.style.display="block";
if (v5){
this.style.left=""+0+"px";
this.style.top=""+0+"px";
this.style.width="auto";
this.innerHTML=v5;
}
var ak4=false;
var ak5=(s3.style.position=="relative"||s3.style.position=="absolute");
var ak6=w4.top;
var ak7=w4.left;
var q6=f3.offsetParent;
while ((q6.tagName=="TD"||q6.tagName=="TR"||q6.tagName=="TBODY"||q6.tagName=="TABLE")&&q6.style.position!="relative"&&q6.style.position!="absolute")
q6=q6.offsetParent;
if (this.msgObj.row>=0&&this.msgObj.col>=0){
if (!ak5&&ak4&&q6){
var ak8=fpObj.GetLocation(s3);
var ak9=fpObj.GetLocation(q6);
ak6+=ak8.y-ak9.y;
ak7+=ak8.x-ak9.x;
if (q6.tagName!="BODY"){
ak6-=fpObj.GetBorderWidth(q6,0);
ak7-=fpObj.GetBorderWidth(q6,3);
}
}
var al0=fpObj.GetViewPortByRowCol(s3,this.msgObj.row,this.msgObj.col);
if (!this.parentNode&&al0&&al0.parentNode)al0.parentNode.insertBefore(aj9,null);
var k1=this.offsetWidth;
this.style.left=""+ak7+"px";
if (!ak4&&al0&&al0.parentNode&&ak7+k1>al0.offsetWidth)
this.style.width=""+(w4.a7-2)+"px";
else if (parseInt(this.style.width)!=k1)
this.style.width=""+k1+"px";
if (!ak4&&al0!=null&&ak6>=al0.offsetHeight-2)ak6-=w4.a6+this.offsetHeight+3;
this.style.top=""+ak6+"px";
}
else {
if (!ak5&&q6){
var ak8=fpObj.GetLocation(s3);
var ak9=fpObj.GetLocation(q6);
ak6+=ak8.y-ak9.y;
ak7+=ak8.x-ak9.x;
if (q6.tagName!="BODY"){
ak6-=fpObj.GetBorderWidth(q6,0);
ak7-=fpObj.GetBorderWidth(q6,3);
}
}
var al1=20;
if (!this.parentNode)s3.insertBefore(aj9,null);
if (this.offsetWidth+al1<s3.offsetWidth)
ak7+=(s3.offsetWidth-this.offsetWidth-al1)/(this.msgObj.row==-2?1:2);
else 
this.style.width=""+(s3.offsetWidth-al1)+"px";
if (this.offsetHeight<s3.offsetHeight)
ak6+=(s3.offsetHeight-this.offsetHeight)/(this.msgObj.col==-2?1:2);
if (this.msgObj.col==-2){
var al2=fpObj.GetColFooter(s3);
if (al2)ak6-=al2.offsetHeight;
var g1=fpObj.GetCommandBar(s3);
if (g1)ak6-=g1.offsetHeight;
ak6-=al1;
}
this.style.top=""+ak6+"px";
this.style.left=""+ak7+"px";
}
this.IsVisible=true;
};
aj9.Hide=function (){
this.style.visibility="hidden";
this.style.display="none";
this.IsVisible=false;
if (this.msgObj.timer){
clearTimeout(this.msgObj.timer);
this.msgObj.timer=null;
}
this.innerHTML="";
};
return aj8;
}
this.GetLocation=function (ele){
if ((ele.window&&ele.window===ele)||ele.nodeType===9)return {x:0,y:0};
var al3=0;
var al4=0;
var al5=null;
var al6=null;
var al7=null;
for (var j6=ele;j6;al5=j6,al6=al7,j6=j6.offsetParent){
var aj1=j6.tagName;
al7=this.GetCurrentStyle2(j6);
if ((j6.offsetLeft||j6.offsetTop)&&
!((aj1==="BODY")&&
(!al6||al6.position!="absolute"))){
al3+=j6.offsetLeft;
al4+=j6.offsetTop;
}
if (al5!=null&&al7){
if ((aj1!="TABLE")&&(aj1!="TD")&&(aj1!="HTML")){
al3+=parseInt(al7.borderLeftWidth)||0;
al4+=parseInt(al7.borderTopWidth)||0;
}
if (aj1==="TABLE"&&
(al7.position==="relative"||al7.position==="absolute")){
al3+=parseInt(al7.marginLeft)||0;
al4+=parseInt(al7.marginTop)||0;
}
}
}
al7=this.GetCurrentStyle2(ele);
var al8=al7?al7.position:null;
if (!al8||(al8!="absolute")){
for (var j6=ele.parentNode;j6;j6=j6.parentNode){
aj1=j6.tagName;
if ((aj1!="BODY")&&(aj1!="HTML")&&(j6.scrollLeft||j6.scrollTop)){
al3-=(j6.scrollLeft||0);
al4-=(j6.scrollTop||0);
al7=this.GetCurrentStyle2(j6);
if (al7){
al3+=parseInt(al7.borderLeftWidth)||0;
al4+=parseInt(al7.borderTopWidth)||0;
}
}
}
}
return {x:al3,y:al4};
}
var al9=["borderTopWidth","borderRightWidth","borderBottomWidth","borderLeftWidth"];
var am0=["borderTopStyle","borderRightStyle","borderBottomStyle","borderLeftStyle"];
var am1;
this.GetBorderWidth=function (ele,side){
if (!this.GetBorderVisible(ele,side))return 0;
var n6=this.GetCurrentStyle(ele,al9[side]);
return this.ParseBorderWidth(n6);
}
this.GetBorderVisible=function (ele,side){
return this.GetCurrentStyle(ele,am0[side])!="none";
}
this.GetWindow=function (ele){
var ag3=ele.ownerDocument||ele.document||ele;
return ag3.defaultView||ag3.parentWindow;
}
this.GetCurrentStyle2=function (ele){
if (ele.nodeType===3)return null;
var k1=this.GetWindow(ele);
if (ele.documentElement)ele=ele.documentElement;
var am2=(k1&&(ele!=k1))?k1.getComputedStyle(ele,null):ele.style;
return am2;
}
this.GetCurrentStyle=function (ele,attribute,defaultValue){
var am3=null;
if (ele){
if (ele.currentStyle){
am3=ele.currentStyle[attribute];
}
else if (document.defaultView&&document.defaultView.getComputedStyle){
var am4=document.defaultView.getComputedStyle(ele,null);
if (am4){
am3=am4[attribute];
}
}
if (!am3&&ele.style.getPropertyValue){
am3=ele.style.getPropertyValue(attribute);
}
else if (!am3&&ele.style.getAttribute){
am3=ele.style.getAttribute(attribute);
}
}
if (!am3||am3==""||typeof(am3)==='undefined'){
if (typeof(defaultValue)!='undefined'){
am3=defaultValue;
}
else {
am3=null;
}
}
return am3;
}
this.ParseBorderWidth=function (n6){
if (!am1){
var am5={};
var am6=document.createElement('div');
am6.style.visibility='hidden';
am6.style.position='absolute';
am6.style.fontSize='1px';
document.body.appendChild(am6)
var am7=document.createElement('div');
am7.style.height='0px';
am7.style.overflow='hidden';
am6.appendChild(am7);
var am8=am6.offsetHeight;
am7.style.borderTop='solid black';
am7.style.borderTopWidth='thin';
am5['thin']=am6.offsetHeight-am8;
am7.style.borderTopWidth='medium';
am5['medium']=am6.offsetHeight-am8;
am7.style.borderTopWidth='thick';
am5['thick']=am6.offsetHeight-am8;
am6.removeChild(am7);
document.body.removeChild(am6);
am1=am5;
}
if (n6){
switch (n6){
case 'thin':
case 'medium':
case 'thick':
return am1[n6];
case 'inherit':
return 0;
}
var am9=this.ParseUnit(n6);
if (am9.type!='px')
throw new Error();
return am9.size;
}
return 0;
}
this.ParseUnit=function (n6){
if (!n6)
throw new Error();
n6=this.Trim(n6).toLowerCase();
var ab1=n6.length;
var s3=-1;
for (var f5=0;f5<ab1;f5++){
var z2=n6.substr(f5,1);
if ((z2<'0'||z2>'9')&&z2!='-'&&z2!='.'&&z2!=',')
break ;
s3=f5;
}
if (s3==-1)
throw new Error();
var aj2;
var an0;
if (s3<(ab1-1))
aj2=this.Trim(n6.substring(s3+1));
else 
aj2='px';
an0=parseFloat(n6.substr(0,s3+1));
if (aj2=='px'){
an0=Math.floor(an0);
}
return {size:an0,type:aj2};
}
this.GetViewPortByRowCol=function (f3,j7,n8){
var an1=null;
var g4=null;
var an2=null;
var n6=this.GetViewport(f3);
var i4=this.GetCellByRowCol(f3,j7,n8);
if (n6!=null&&this.IsChild(i4,n6))
return n6;
else if (an2!=null&&this.IsChild(i4,an2))
return an2;
else if (g4!=null&&this.IsChild(i4,g4))
return g4;
else if (an1!=null&&this.IsChild(i4,an1))
return an1;
return ;
}
this.GetMsgPos=function (f3,j7,n8){
if (j7<0||n8<0){
return {left:0,top:0};
}
else {
var an1=null;
var g4=null;
var an2=null;
var n6=this.GetViewport(f3);
var an3=this.GetGroupBar(f3);
var l9=document.getElementById(f3.id+"_titleBar");
var i4=this.GetCellByRowCol(f3,j7,n8);
var f9=i4.offsetTop+i4.offsetHeight;
var ab1=i4.offsetLeft;
if ((an1!=null||g4!=null)&&(this.IsChild(i4,an2)||this.IsChild(i4,n6))){
if (an1!=null)
f9+=an1.offsetHeight;
else 
f9+=g4.offsetHeight;
}
if ((an1!=null||an2!=null)&&(this.IsChild(i4,g4)||this.IsChild(i4,n6))){
if (an1!=null)
ab1+=an1.offsetWidth;
else 
ab1+=an2.offsetWidth;
}
if (n6!=null&&(an1||g4||an2)){
if (l9)f9+=l9.offsetHeight;
if (an3)f9+=an3.offsetHeight;
if (this.GetColHeader(f3))f9+=this.GetColHeader(f3).offsetHeight;
if (this.GetRowHeader(f3))ab1+=this.GetRowHeader(f3).offsetWidth;
}
if (n6!=null&&this.IsChild(i4,n6)){
if (g4)
f9-=n6.parentNode.scrollTop;
if (an2)
ab1-=n6.parentNode.scrollLeft;
}
if (an2!=null&&this.IsChild(i4,an2)){
f9-=an2.parentNode.scrollTop;
}
if (g4!=null&&this.IsChild(i4,g4)){
ab1-=g4.parentNode.scrollLeft;
}
var f8=i4.clientHeight;
var k1=i4.clientWidth;
return {left:ab1,top:f9,a6:f8,a7:k1};
}
}
this.SyncMsgs=function (f3){
if (!f3.msgList)return ;
for (f5 in f3.msgList){
if (f3.msgList[f5].constructor==Array){
for (j1 in f3.msgList[f5]){
if (f3.msgList[f5][j1]&&f3.msgList[f5][j1].msgBox&&f3.msgList[f5][j1].msgBox.IsVisible){
f3.msgList[f5][j1].msgBox.Show(f3,this);
}
}
}
}
}
this.GetCellInfo=function (f3,f7,i3,w4){
var g6=this.GetData(f3);
if (g6==null)return null;
var g7=g6.getElementsByTagName("root")[0];
if (g7==null)return null;
var o1=g7.getElementsByTagName("state")[0];
if (o1==null)return null;
var an4=o1.getElementsByTagName("cellinfo")[0];
if (an4==null)return null;
var f9=an4.firstChild;
while (f9!=null){
if ((f9.getAttribute("r")==""+f7)&&(f9.getAttribute("c")==""+i3)&&(f9.getAttribute("pos")==""+w4))return f9;
f9=f9.nextSibling;
}
return null;
}
this.AddCellInfo=function (f3,f7,i3,w4){
var o0=this.GetCellInfo(f3,f7,i3,parseInt(w4));
if (o0!=null)return o0;
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
if (g7==null)return null;
var o1=g7.getElementsByTagName("state")[0];
if (o1==null)return null;
var an4=o1.getElementsByTagName("cellinfo")[0];
if (an4==null)return null;
if (document.all!=null){
o0=g6.createNode("element","c","");
}else {
o0=document.createElement("c");
o0.style.display="none";
}
o0.setAttribute("r",f7);
o0.setAttribute("c",i3);
o0.setAttribute("pos",w4);
an4.appendChild(o0);
return o0;
}
this.setCellAttribute=function (f3,i4,attname,v2,noEvent,recalc){
if (i4==null)return ;
var f7=this.GetRowKeyFromCell(f3,i4);
var i3=this.GetColKeyFromCell(f3,i4);
if (typeof(f7)=="undefined")return ;
var w4=-1;
if (this.IsChild(i4,this.GetCorner(f3)))
w4=0;
else if (this.IsChild(i4,this.GetRowHeader(f3)))
w4=1;
else if (this.IsChild(i4,this.GetColHeader(f3)))
w4=2;
else if (this.IsChild(i4,this.GetViewport(f3)))
w4=3;
var r9=this.AddCellInfo(f3,f7,i3,w4);
r9.setAttribute(attname,v2);
if (!noEvent){
var h2=this.CreateEvent("DataChanged");
h2.cell=i4;
h2.cellValue=v2;
h2.row=f7;
h2.col=i3;
this.FireEvent(f3,h2);
}
var g9=this.GetCmdBtn(f3,"Update");
if (g9!=null&&g9.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(g9,false);
g9=this.GetCmdBtn(f3,"Cancel");
if (g9!=null&&g9.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(g9,false);
f3.f1=true;
if (recalc){
this.UpdateValues(f3);
}
}
this.updateCellLocked=function (i4,locked){
if (i4==null)return ;
var f9=i4.getAttribute("FpCellType")=="readonly";
if (f9==locked)return ;
var i3=i4.firstChild;
while (i3!=null){
if (typeof(i3.disabled)!="undefined")i3.disabled=locked;
i3=i3.nextSibling;
}
}
this.Cells=function (f3,f7,i3)
{
var an5=this.GetCellByRowCol(f3,f7,i3);
if (an5){
an5.GetValue=function (){
return the_fpSpread.GetValue(f3,f7,i3);
}
an5.SetValue=function (h3){
if (typeof(h3)=="undefined")return ;
if (this.parentNode.getAttribute("previewRow")!=null)return ;
the_fpSpread.SetValue(f3,f7,i3,h3);
the_fpSpread.SaveClientEditedDataRealTime();
}
an5.GetBackColor=function (){
if (this.getAttribute("bgColorBak")!=null)
return this.getAttribute("bgColorBak");
return document.defaultView.getComputedStyle(this,"").getPropertyValue("background-color");
}
an5.SetBackColor=function (h3){
if (typeof(h3)=="undefined")return ;
this.bgColor=h3;
this.setAttribute("bgColorBak",h3);
this.style.backgroundColor=h3;
the_fpSpread.setCellAttribute(f3,this,"bc",h3);
the_fpSpread.SaveClientEditedDataRealTime();
}
an5.GetForeColor=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("color");
}
an5.SetForeColor=function (h3){
if (typeof(h3)=="undefined")return ;
this.style.color=h3;
the_fpSpread.setCellAttribute(f3,this,"fc",h3);
the_fpSpread.SaveClientEditedDataRealTime();
}
an5.GetTabStop=function (){
return this.getAttribute("TabStop")!="false";
}
an5.SetTabStop=function (h3){
var an6=new String(h3);
if (an6.toLocaleLowerCase()=="false"){
this.setAttribute("TabStop","false");
the_fpSpread.setCellAttribute(f3,this,"ts","false");
the_fpSpread.SaveClientEditedDataRealTime();
}else {
this.removeAttribute("TabStop");
}
}
an5.GetCellType=function (){
var an7=the_fpSpread.GetCellType2(this);
if (an7=="text"||an7=="readonly")
{
an7=this.getAttribute("CellType2");
}
if (an7==null)
an7="GeneralCellType";
return an7;
}
an5.GetHAlign=function (){
var an8=document.defaultView.getComputedStyle(this,"").getPropertyValue("text-Align");
if (an8==""||an8=="undefined"||an8==null)
an8=this.style.textAlign;
if (an8==""||an8=="undefined"||an8==null)
an8=this.getAttribute("align");
if (an8!=null&&an8.indexOf("-webkit")!=-1)an8=an8.replace("-webkit-","");
return an8;
}
an5.SetHAlign=function (h3){
if (typeof(h3)=="undefined")return ;
this.style.textAlign=typeof(h3)=="string"?h3:h3.Name;
the_fpSpread.setCellAttribute(f3,this,"ha",typeof(h3)=="string"?h3:h3.Name);
the_fpSpread.SaveClientEditedDataRealTime();
}
an5.GetVAlign=function (){
var an9=document.defaultView.getComputedStyle(this,"").getPropertyValue("vertical-Align");
if (an9==""||an9=="undefined"||an9==null)
an9=this.style.verticalAlign;
if (an9==""||an9=="undefined"||an9==null)
an9=this.getAttribute("valign");
return an9;
}
an5.SetVAlign=function (h3){
if (typeof(h3)=="undefined")return ;
this.style.verticalAlign=typeof(h3)=="string"?h3:h3.Name;
the_fpSpread.setCellAttribute(f3,this,"va",typeof(h3)=="string"?h3:h3.Name);
the_fpSpread.SaveClientEditedDataRealTime();
}
an5.GetLocked=function (){
if (an5.GetCellType()=="ButtonCellType"||an5.GetCellType()=="TagCloudCellType"||an5.GetCellType()=="HyperLinkCellType")
return an5.getAttribute("Locked")=="1";
return the_fpSpread.GetCellType(this)=="readonly";
}
an5.GetFont_Name=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("font-family");
}
an5.SetFont_Name=function (h3){
if (typeof(h3)=="undefined")return ;
this.style.fontFamily=h3;
the_fpSpread.setCellAttribute(f3,this,"fn",h3);
the_fpSpread.SaveClientEditedDataRealTime();
}
an5.GetFont_Size=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("font-size");
}
an5.SetFont_Size=function (h3){
if (typeof(h3)=="undefined")return ;
if (typeof(h3)=="number")h3+="px";
this.style.fontSize=h3;
the_fpSpread.setCellAttribute(f3,this,"fs",h3);
the_fpSpread.SizeSpread(f3);
the_fpSpread.SaveClientEditedDataRealTime();
}
an5.GetFont_Bold=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("font-weight")=="bold"?true:false;
}
an5.SetFont_Bold=function (h3){
if (typeof(h3)=="undefined")return ;
this.style.fontWeight=h3==true?"bold":"normal";
the_fpSpread.setCellAttribute(f3,this,"fb",new String(h3).toLocaleLowerCase());
the_fpSpread.SaveClientEditedDataRealTime();
}
an5.GetFont_Italic=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("font-style")=="italic"?true:false;
}
an5.SetFont_Italic=function (h3){
if (typeof(h3)=="undefined")return ;
this.style.fontStyle=h3==true?"italic":"normal";
the_fpSpread.setCellAttribute(f3,this,"fi",new String(h3).toLocaleLowerCase());
the_fpSpread.SaveClientEditedDataRealTime();
}
an5.GetFont_Overline=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("overline")>=0?true:false;
}
an5.SetFont_Overline=function (h3){
if (h3){
var ao0=new String("overline");
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("line-through")>=0)
ao0+=" line-through"
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("underline")>=0)
ao0+=" underline"
this.style.textDecoration=ao0;
}
else {
var ao0=new String("");
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("line-through")>=0)
ao0+=" line-through"
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("underline")>=0)
ao0+=" underline"
if (ao0=="")ao0="none";
this.style.textDecoration=ao0;
}
the_fpSpread.setCellAttribute(f3,this,"fo",new String(h3).toLocaleLowerCase());
the_fpSpread.SaveClientEditedDataRealTime();
}
an5.GetFont_Strikeout=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("line-through")>=0?true:false;
}
an5.SetFont_Strikeout=function (h3){
if (h3){
var ao0=new String("line-through");
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("overline")>=0)
ao0+=" overline"
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("underline")>=0)
ao0+=" underline"
this.style.textDecoration=ao0;
}
else {
var ao0=new String("");
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("overline")>=0)
ao0+=" overline"
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("underline")>=0)
ao0+=" underline"
if (ao0=="")ao0="none";
this.style.textDecoration=ao0;
}
the_fpSpread.setCellAttribute(f3,this,"fk",new String(h3).toLocaleLowerCase());
the_fpSpread.SaveClientEditedDataRealTime();
}
an5.GetFont_Underline=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("underline")>=0?true:false;
}
an5.SetFont_Underline=function (h3){
if (h3){
var ao0=new String("underline");
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("overline")>=0)
ao0+=" overline"
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("line-through")>=0)
ao0+=" line-through"
this.style.textDecoration=ao0;
}
else {
var ao0=new String("");
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("overline")>=0)
ao0+=" overline"
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("line-through")>=0)
ao0+=" line-through"
if (ao0=="")ao0="none";
this.style.textDecoration=ao0;
}
the_fpSpread.setCellAttribute(f3,this,"fu",new String(h3).toLocaleLowerCase());
the_fpSpread.SaveClientEditedDataRealTime();
}
return an5;
}
return null;
}
this.getDomRow=function (f3,f7){
var f6=this.GetRowCount(f3);
if (f6==0)return null;
var i4=this.GetCellByRowCol(f3,f7,0);
if (i4){
var f4=i4.parentNode.rowIndex;
if (f4>=0){
var j7=i4.parentNode.parentNode.rows[f4];
if (this.GetSizable(f3,j7))
return j7;
}
return null;
}
}
this.setRowInfo_RowAttribute=function (f3,f7,attname,v2,recalc){
f7=parseInt(f7);
if (f7<0)return ;
var ao1=this.AddRowInfo(f3,f7);
ao1.setAttribute(attname,v2);
var g9=this.GetCmdBtn(f3,"Update");
if (g9!=null&&g9.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(g9,false);
g9=this.GetCmdBtn(f3,"Cancel");
if (g9!=null&&g9.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(g9,false);
f3.f1=true;
if (recalc){
this.UpdateValues(f3);
}
}
this.Rows=function (f3,f7)
{
var ao2=this.getDomRow(f3,f7);
if (ao2){
ao2.GetHeight=function (){
return the_fpSpread.GetRowHeightInternal(f3,f7);
}
ao2.SetHeight=function (ad4){
if (typeof(ad4)=="undefined")return ;
if (ad4<1)
ad4=1;
f7=the_fpSpread.GetDisplayIndex(f3,f7);
var c4=null;
if (the_fpSpread.GetRowHeader(f3)!=null)c4=the_fpSpread.GetRowHeader(f3).rows[f7];
if (c4!=null)c4.cells[0].style.posHeight=ad4;
var j5=the_fpSpread.GetViewport(f3);
if (c4==null)
c4=j5.rows[f7];
if (c4!=null)c4.cells[0].style.posHeight=ad4;
var q7=the_fpSpread.AddRowInfo(f3,c4.getAttribute("FpKey"));
if (q7!=null){
if (typeof(c4.cells[0].style.posHeight)=="undefined")
the_fpSpread.SetRowHeight(f3,q7,ad4);
else 
the_fpSpread.SetRowHeight(f3,q7,c4.cells[0].style.posHeight);
}
var j6=the_fpSpread.GetParentSpread(f3);
if (j6!=null)j6.UpdateRowHeight(f3);
the_fpSpread.SynRowHeight(f3,the_fpSpread.GetRowHeader(f3),j5,f7,true,false)
var g2=the_fpSpread.GetTopSpread(f3);
the_fpSpread.SizeAll(g2);
the_fpSpread.Refresh(g2);
the_fpSpread.SaveClientEditedDataRealTime();
}
return ao2;
}
return null;
}
this.setColInfo_ColumnAttribute=function (f3,i3,attname,v2,recalc){
i3=parseInt(i3);
if (i3<0)return ;
var ao3=this.AddColInfo(f3,i3);
ao3.setAttribute(attname,v2);
var g9=this.GetCmdBtn(f3,"Update");
if (g9!=null&&g9.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(g9,false);
g9=this.GetCmdBtn(f3,"Cancel");
if (g9!=null&&g9.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(g9,false);
f3.f1=true;
if (recalc){
this.UpdateValues(f3);
}
}
this.Columns=function (f3,i3)
{
var ao4={a4:this.GetColByKey(f3,parseInt(i3))};
if (ao4){
ao4.GetWidth=function (){
return the_fpSpread.GetColWidthFromCol(f3,i3);
}
ao4.SetWidth=function (h3){
if (typeof(h3)=="undefined")return ;
the_fpSpread.SetColWidth(f3,i3,h3);
the_fpSpread.SaveClientEditedDataRealTime();
}
return ao4;
}
return null;
}
this.GetTitleBar=function (f3){
try {
if (document.getElementById(f3.id+"_title")==null)return null;
var ao5=document.getElementById(f3.id+"_titleBar");
if (ao5!=null)ao5=document.getElementById(f3.id+"_title");
return ao5;
}
catch (ex){
return null;
}
}
this.CheckTitleInfo=function (f3){
var g6=this.GetData(f3);
if (g6==null)return null;
var g7=g6.getElementsByTagName("root")[0];
if (g7==null)return null;
var ao6=g7.getElementsByTagName("titleinfo")[0];
if (ao6==null)return null;
return ao6;
}
this.AddTitleInfo=function (f3){
var o0=this.CheckTitleInfo(f3);
if (o0!=null)return o0;
var g6=this.GetData(f3);
var g7=g6.getElementsByTagName("root")[0];
if (g7==null)return null;
if (document.all!=null){
o0=g6.createNode("element","titleinfo","");
}else {
o0=document.createElement("titleinfo");
o0.style.display="none";
}
g7.appendChild(o0);
return o0;
}
this.setTitleInfo_Attribute=function (f3,attname,v2,recalc){
var ao7=this.AddTitleInfo(f3);
ao7.setAttribute(attname,v2);
var g9=this.GetCmdBtn(f3,"Update");
if (g9!=null&&g9.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(g9,false);
g9=this.GetCmdBtn(f3,"Cancel");
if (g9!=null&&g9.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(g9,false);
f3.f1=true;
if (recalc){
this.UpdateValues(f3);
}
}
this.GetTitleInfo=function (f3)
{
var ao8=this.GetTitleBar(f3);
if (ao8){
ao8.GetHeight=function (){
return this.style.height;
}
ao8.SetHeight=function (h3){
this.style.height=parseInt(h3)+"px";
the_fpSpread.setTitleInfo_Attribute(f3,"ht",h3);
var g2=the_fpSpread.GetTopSpread(f3);
the_fpSpread.SizeAll(g2);
the_fpSpread.Refresh(g2);
the_fpSpread.SaveClientEditedDataRealTime();
}
ao8.GetVisible=function (){
return (document.defaultView.getComputedStyle(this,"").getPropertyValue("display")=="none")?false:true;
return document.defaultView.getComputedStyle(this,"").getPropertyValue("visibility");
}
ao8.SetVisible=function (h3){
this.style.display=h3?"":"none";
this.style.visibility=h3?"visible":"hidden";
the_fpSpread.setTitleInfo_Attribute(f3,"vs",new String(h3).toLocaleLowerCase());
var g2=the_fpSpread.GetTopSpread(f3);
the_fpSpread.SizeAll(g2);
the_fpSpread.Refresh(g2);
the_fpSpread.SaveClientEditedDataRealTime();
}
ao8.GetValue=function (){
return this.textContent;
}
ao8.SetValue=function (h3){
this.textContent=""+h3;
the_fpSpread.setTitleInfo_Attribute(f3,"tx",h3);
the_fpSpread.SaveClientEditedDataRealTime();
}
return ao8;
}
return null;
}
this.SaveClientEditedDataRealTime=function (){
var ao9=this.GetPageActiveSpread();
if (ao9!=null){
this.SaveData(ao9);
ao9.f1=false;
}
ao9=this.GetPageActiveSheetView();
if (ao9!=null){
this.SaveData(ao9);
ao9.f1=false;
}
}
var ap0="";
this.ShowScrollingContent=function (f3,hs){
var s0="";
var p4=this.GetTopSpread(f3);
var ap1=p4.getAttribute("scrollContentColumns");
var ap2=p4.getAttribute("scrollContentMaxHeight");
var ap3=p4.getAttribute("scrollContentTime");
var j5=this.GetViewport(p4);
var ap4=this.GetColGroup(j5);
var n6=this.GetParent(j5);
var ap5=0;
if (hs){
var ap6=n6.scrollLeft;
var d5=this.GetColHeader(p4);
var s7=0;
for (;s7<ap4.childNodes.length;s7++){
var i3=ap4.childNodes[s7];
ap5+=parseInt(i3.width);
if (ap5>ap6)break ;
}
var ap7=null;
if (ap7)s7+=this.GetColGroup(ap7).childNodes.length;
if (d5){
var s6=d5.rows.length-1;
if (f3.getAttribute("LayoutMode")==null)
s6=parseInt(d5.getAttribute("ColTextIndex"))?d5.getAttribute("ColTextIndex"):d5.rows.length-1;
var ap8=this.GetHeaderCellFromRowCol(p4,s6,s7,true);
if (ap8){
if (ap8.getAttribute("FpCellType")=="ExtenderCellType"&&ap8.getElementsByTagName("DIV").length>0){
var x3=this.GetEditor(ap8);
var x4=this.GetFunction("ExtenderCellType_getEditorValue");
if (x3!=null&&x4!=null){
s0="&nbsp;Column:&nbsp;"+x4(x3)+"&nbsp;";
}
}
else 
s0="&nbsp;Column:&nbsp;"+ap8.innerHTML+"&nbsp;";
}
}
if (s0.length<=0)s0="&nbsp;Column:&nbsp;"+(s7+1)+"&nbsp;";
}
else {
var o1=n6.scrollTop;
var d4=this.GetRowHeader(p4);
var s6=0;
var ap9=0;
var aq0=2;
for (var y5=0;y5<j5.rows.length;y5++){
var f7=j5.rows[y5];
ap5+=f7.offsetHeight;
if (ap5>o1){
if (f7.getAttribute("fpkey")==null&&f7.getAttribute("previewrow")!="true")
s6--;
else 
ap9=f7.offsetHeight;
break ;
}
if (f7.getAttribute("fpkey")!=null||f7.getAttribute("previewrow")=="true"){
s6++;
ap9=f7.offsetHeight;
}
}
var ap7=null;
if (ap7)s6+=ap7.rows.length;
if (f3.getAttribute("LayoutMode")==null&&ap1!=null&&ap1.length>0){
ap9=ap9>ap2?ap2:ap9;
var aq1=ap1.split(",");
var aq2=false;
for (var f5=0;f5<aq1.length;f5++){
var i3=parseInt(aq1[f5]);
if (i3==null||i3>=this.GetColCount(f3))continue ;
var i4=p4.GetCellByRowCol(s6,i3);
if (!i4||i4.getAttribute("col")!=null&&i4.getAttribute("col")!=i3)continue ;
var aq3=(i4.getAttribute("group")==1);
var ac4=(i4.parentNode.getAttribute("previewrow")=="true");
var h2=(i4.getAttribute("RowEditTemplate")!=null);
var k9=this.IsXHTML(f3);
if (!k9&&ap0==""){
this.GetScrollingContentStyle(f3);
if (ak1!=null){
if (ak1.fontFamily!=null&&ak1.fontFamily!="")ap0+="fontFamily:"+ak1.fontFamily+";";
if (ak1.fontSize!=null&&ak1.fontSize!="")ap0+="fontSize:"+ak1.fontSize+";";
if (ak1.fontStyle!=null&&ak1.fontStyle!="")ap0+="fontStyle:"+ak1.fontStyle+";";
if (ak1.fontVariant!=null&&ak1.fontVariant!="")ap0+="fontVariant:"+ak1.fontVariant+";";
if (ak1.fontWeight!=null&&ak1.fontWeight!="")ap0+="fontWeight:"+ak1.fontWeight+";";
if (ak1.backgroundColor!=null&&ak1.backgroundColor!="")ap0+="backgroundColor:"+ak1.backgroundColor+";";
if (ak1.color!=null&&ak1.color!="")ap0+="color:"+ak1.color;
}
}
if (!aq2){
s0+="<div style='overflow:hidden;height:"+ap9+"px;ScrollingContentWidth'><table cellPadding='0' cellSpacing='0' style='height:"+ap9+"px;"+(aq3?"":"table-layout:auto;")+ap0+"'><tr>";
}
s0+="<td style='width:"+(aq3?0:i4.offsetWidth)+"px;'>";
aq0+=i4.offsetWidth;
if (aq3)
s0+="&nbsp;<i>GroupBar:</i>&nbsp;"+i4.textContent+"&nbsp;";
else if (ac4)
s0+="&nbsp;<i>PreviewRow:</i>&nbsp;"+i4.textContent+"&nbsp;";
else if (h2){
var aq4=this.parseCell(f3,i4);
s0+="&nbsp;<i>RowEditTemplate:</i>&nbsp;"+aq4+"&nbsp;";
}
else {
if (i4.getAttribute("fpcelltype"))this.UpdateCellTypeDOM(i4);
if (i4.getAttribute("fpcelltype")=="MultiColumnComboBoxCellType"&&i4.childNodes[0]&&i4.childNodes[0].childNodes.length>0&&i4.childNodes[0].getAttribute("MccbId"))
s0+=p4.GetValue(s6,i3);
else if (i4.getAttribute("fpcelltype")=="RadioButtonListCellType"||i4.getAttribute("fpcelltype")=="ExtenderCellType"||i4.getAttribute("fpeditorid")!=null){
var aq5=this.parseCell(f3,i4);
s0+=aq5;
}
else 
s0+=i4.innerHTML;
}
s0+="</td>";
aq2=true;
if (aq3||ac4||h2)break ;
}
if (aq2){
s0=s0.replace("ScrollingContentWidth"," width:"+aq0+"px;");
s0+="</tr></table></div>";
}
}
if (s0.length<=0&&d4){
var s7=this.GetColGroup(d4).childNodes.length-1;
if (f3.getAttribute("LayoutMode")==null)
s7=d4.getAttribute("RowTextIndex")?parseInt(d4.getAttribute("RowTextIndex"))+1:this.GetColGroup(d4).childNodes.length-1;
var ap8=this.GetHeaderCellFromRowCol(f3,s6,s7,false);
if (ap8)s0="&nbsp;Row:&nbsp;"+ap8.textContent+"&nbsp;";
}
if (s0.length<=0)s0="&nbsp;Row:&nbsp;"+(s6+1)+"&nbsp;";
}
this.ShowMessageInner(p4,s0,(hs?-1:-2),(hs?-2:-1),ap3);
}
this.parseCell=function (f3,i4){
var s0=i4.innerHTML;
var p4=this.GetTopSpread(f3);
var aq6=p4.id;
if (s0.length>0){
s0=s0.replace(new RegExp("=\""+aq6,"g"),"=\""+aq6+"src");
s0=s0.replace(new RegExp("name="+aq6,"g"),"name="+aq6+"src");
}
return s0;
}
this.UpdateCellTypeDOM=function (i4){
for (var f5=0;f5<i4.childNodes.length;f5++){
if (i4.childNodes[f5].tagName&&(i4.childNodes[f5].tagName=="INPUT"||i4.childNodes[f5].tagName=="SELECT"))
this.UpdateDOM(i4.childNodes[f5]);
if (i4.childNodes[f5].childNodes&&i4.childNodes[f5].childNodes.length>0)
this.UpdateCellTypeDOM(i4.childNodes[f5]);
}
}
this.UpdateDOM=function (inputField){
if (typeof(inputField)=="string"){
inputField=document.getElementById(inputField);
}
if (inputField.type=="select-one"){
for (var f5=0;f5<inputField.options.length;f5++){
if (f5==inputField.selectedIndex){
inputField.options[inputField.selectedIndex].setAttribute("selected","selected");
}
}
}
else if (inputField.type=="text"){
inputField.setAttribute("value",inputField.value);
}
else if (inputField.type=="textarea"){
inputField.setAttribute("value",inputField.value);
}
else if ((inputField.type=="checkbox")||(inputField.type=="radio")){
if (inputField.checked){
inputField.setAttribute("checked","checked");
}else {
inputField.removeAttribute("checked");
}
}
}
this.GetScrollingContentStyle=function (f3){
if (ak1!=null)return ;
var f4=document.styleSheets.length;
for (var f5=0;f5<f4;f5++){
var aq7=document.styleSheets[f5];
for (var j1=0;j1<aq7.cssRules.length;j1++){
var aq8=aq7.cssRules[j1];
if (aq8.selectorText=="."+f3.id+"scrollContentStyle"||aq8.selectorText=="."+f3.id.toLowerCase()+"scrollcontentstyle"){
ak1=aq8.style;
break ;
}
}
if (ak1!=null)break ;
}
}
}
function CheckBoxCellType_setFocus(i4){
var j4=i4.getElementsByTagName("INPUT");
if (j4!=null&&j4.length>0&&j4[0].type=="checkbox"){
j4[0].focus();
}
}
function CheckBoxCellType_getCheckBoxEditor(i4){
var j4=i4.getElementsByTagName("INPUT");
if (j4!=null&&j4.length>0&&j4[0].type=="checkbox"){
return j4[0];
}
return null;
}
function CheckBoxCellType_isValid(i4,v2){
if (v2==null)return "";
v2=the_fpSpread.Trim(v2);
if (v2=="")return "";
if (v2.toLowerCase()=="true"||v2.toLowerCase()=="false")
return "";
else 
return "invalid value";
}
function CheckBoxCellType_getValue(v6,f3){
return CheckBoxCellType_getEditorValue(v6,f3);
}
function CheckBoxCellType_getEditorValue(v6,f3){
var i4=the_fpSpread.GetCell(v6);
var j4=CheckBoxCellType_getCheckBoxEditor(i4);
if (j4!=null&&j4.checked){
return "true";
}
return "false";
}
function CheckBoxCellType_setValue(v6,v2){
var i4=the_fpSpread.GetCell(v6);
var j4=CheckBoxCellType_getCheckBoxEditor(i4);
if (j4!=null){
j4.checked=(v2!=null&&v2.toLowerCase()=="true");
return ;
}
}
function IntegerCellType_getValue(v6){
var f9=v6;
while (f9.firstChild!=null&&f9.firstChild.nodeName!="#text")f9=f9.firstChild;
if (f9.innerHTML=="&nbsp;")return "";
var t6=f9.innerHTML;
v6=the_fpSpread.GetCell(v6);
if (v6.getAttribute("FpRef")!=null)v6=document.getElementById(v6.getAttribute("FpRef"));
var aq9=v6.getAttribute("groupchar");
if (aq9==null)aq9=",";
var u3=t6.length;
while (true){
t6=t6.replace(aq9,"");
if (t6.length==u3)break ;
u3=t6.length;
}
if (t6.charAt(0)=='('&&t6.charAt(t6.length-1)==')'){
var ar0=v6.getAttribute("negsign");
if (ar0==null)ar0="-";
t6=ar0+t6.substring(1,t6.length-1);
}
t6=the_fpSpread.ReplaceAll(t6,"&nbsp;"," ");
return t6;
}
function IntegerCellType_isValid(i4,v2){
if (v2==null||v2.length==0)return "";
v2=the_fpSpread.Trim(v2);
if (v2.length==0)return "";
var ap5=i4;
var ar1=i4.getAttribute("FpRef");
if (ar1!=null)ap5=document.getElementById(ar1);
var ar0=ap5.getAttribute("negsign");
var w4=ap5.getAttribute("possign");
if (ar0!=null)v2=v2.replace(ar0,"-");
if (w4!=null)v2=v2.replace(w4,"+");
if (v2.charAt(v2.length-1)=="-")v2="-"+v2.substring(0,v2.length-1);
var u5=new RegExp("^\\s*[-\\+]?\\d+\\s*$");
var p0=(v2.match(u5)!=null);
if (p0)p0=!isNaN(v2);
if (p0){
var u9=ap5.getAttribute("MinimumValue");
var j9=ap5.getAttribute("MaximumValue");
var u8=parseInt(v2);
if (u9!=null){
u9=parseInt(u9);
p0=(!isNaN(u9)&&u8>=u9);
}
if (p0&&j9!=null){
j9=parseInt(j9);
p0=(!isNaN(j9)&&u8<=j9);
}
}
if (!p0){
if (ap5.getAttribute("error")!=null)
return ap5.getAttribute("error");
else 
return "Integer";
}
return "";
}
function DoubleCellType_isValid(i4,v2){
if (v2==null||v2.length==0)return "";
var ap5=i4;
if (i4.getAttribute("FpRef")!=null)ap5=document.getElementById(i4.getAttribute("FpRef"));
var ar2=ap5.getAttribute("decimalchar");
if (ar2==null)ar2=".";
var aq9=ap5.getAttribute("groupchar");
if (aq9==null)aq9=",";
v2=the_fpSpread.Trim(v2);
var p0=true;
p0=(v2.length==0||v2.charAt(0)!=aq9);
if (p0){
var u3=v2.length;
while (true){
v2=v2.replace(aq9,"");
if (v2.length==u3)break ;
u3=v2.length;
}
}
var p0=true;
if (v2.length==0){
p0=false;
}else if (p0){
var ar0=ap5.getAttribute("negsign");
var w4=ap5.getAttribute("possign");
var u9=ap5.getAttribute("MinimumValue");
var j9=ap5.getAttribute("MaximumValue");
p0=the_fpSpread.IsDouble(v2,ar2,ar0,w4,u9,j9);
}
if (!p0){
if (ap5.getAttribute("error")!=null)
return ap5.getAttribute("error");
else 
return "Double";
}
return "";
}
function DoubleCellType_getValue(v6){
var f9=v6;
while (f9.firstChild!=null&&f9.firstChild.nodeName!="#text")f9=f9.firstChild;
if (f9.innerHTML=="&nbsp;")return "";
var t6=f9.innerHTML;
v6=the_fpSpread.GetCell(v6);
if (v6.getAttribute("FpRef")!=null)v6=document.getElementById(v6.getAttribute("FpRef"));
var aq9=v6.getAttribute("groupchar");
if (aq9==null)aq9=",";
var u3=t6.length;
while (true){
t6=t6.replace(aq9,"");
if (t6.length==u3)break ;
u3=t6.length;
}
if (t6.charAt(0)=='('&&t6.charAt(t6.length-1)==')'){
var ar0=v6.getAttribute("negsign");
if (ar0==null)ar0="-";
t6=ar0+t6.substring(1,t6.length-1);
}
t6=the_fpSpread.ReplaceAll(t6,"&nbsp;"," ");
return t6;
}
function CurrencyCellType_isValid(i4,v2){
if (v2!=null&&v2.length>0){
var ap5=i4;
if (i4.getAttribute("FpRef")!=null)ap5=document.getElementById(i4.getAttribute("FpRef"));
var u2=ap5.getAttribute("currencychar");
if (u2==null)u2="$";
v2=v2.replace(u2,"");
var aq9=ap5.getAttribute("groupchar");
if (aq9==null)aq9=",";
v2=the_fpSpread.Trim(v2);
var p0=true;
p0=(v2.length==0||v2.charAt(0)!=aq9);
if (p0){
var u3=v2.length;
while (true){
v2=v2.replace(aq9,"");
if (v2.length==u3)break ;
u3=v2.length;
}
}
if (v2.length==0){
p0=false;
}else if (p0){
var ar2=ap5.getAttribute("decimalchar");
if (ar2==null)ar2=".";
var ar0=ap5.getAttribute("negsign");
var w4=ap5.getAttribute("possign");
var u9=ap5.getAttribute("MinimumValue");
var j9=ap5.getAttribute("MaximumValue");
p0=the_fpSpread.IsDouble(v2,ar2,ar0,w4,u9,j9);
}
if (!p0){
if (ap5.getAttribute("error")!=null)
return ap5.getAttribute("error");
else 
return "Currency ("+u2+"100"+ar2+"10) ";
}
}
return "";
}
function CurrencyCellType_getValue(v6){
var f9=v6;
while (f9.firstChild!=null&&f9.firstChild.nodeName!="#text")f9=f9.firstChild;
if (f9.innerHTML=="&nbsp;")return "";
var t6=f9.innerHTML;
v6=the_fpSpread.GetCell(v6);
if (v6.getAttribute("FpRef")!=null)v6=document.getElementById(v6.getAttribute("FpRef"));
var u2=v6.getAttribute("currencychar");
if (u2!=null){
var ar3=document.createElement("SPAN");
ar3.innerHTML=u2;
u2=ar3.innerHTML;
}
if (u2==null)u2="$";
var aq9=v6.getAttribute("groupchar");
if (aq9==null)aq9=",";
t6=t6.replace(u2,"");
var u3=t6.length;
while (true){
t6=t6.replace(aq9,"");
if (t6.length==u3)break ;
u3=t6.length;
}
var ar0=v6.getAttribute("negsign");
if (ar0==null)ar0="-";
if (t6.charAt(0)=='('&&t6.charAt(t6.length-1)==')'){
t6=ar0+t6.substring(1,t6.length-1);
}
t6=the_fpSpread.ReplaceAll(t6,"&nbsp;"," ");
return t6;
}
function RegExpCellType_isValid(i4,v2){
if (v2==null||v2=="")
return "";
var ap5=i4;
if (i4.getAttribute("FpRef")!=null)ap5=document.getElementById(i4.getAttribute("FpRef"));
var ar4=new RegExp(ap5.getAttribute("fpexpression"));
var u6=v2.match(ar4);
var n6=(u6!=null&&u6.length>0&&v2==u6[0]);
if (!n6){
if (ap5.getAttribute("error")!=null)
return ap5.getAttribute("error");
else 
return "invalid";
}
return "";
}
function PercentCellType_getValue(v6){
var f9=v6;
while (f9.firstChild!=null&&f9.firstChild.nodeName!="#text")f9=f9.firstChild;
if (f9.innerHTML=="&nbsp;")return "";
f9=f9.innerHTML;
var i4=the_fpSpread.GetCell(v6);
var ap5=i4;
if (i4.getAttribute("FpRef")!=null)ap5=document.getElementById(i4.getAttribute("FpRef"));
var ar5=ap5.getAttribute("percentchar");
if (ar5==null)ar5="%";
f9=f9.replace(ar5,"");
var aq9=ap5.getAttribute("groupchar");
if (aq9==null)aq9=",";
var u3=f9.length;
while (true){
f9=f9.replace(aq9,"");
if (f9.length==u3)break ;
u3=f9.length;
}
var ar0=ap5.getAttribute("negsign");
var w4=ap5.getAttribute("possign");
f9=the_fpSpread.ReplaceAll(f9,"&nbsp;"," ");
var h5=f9;
if (ar0!=null)
f9=f9.replace(ar0,"-");
if (w4!=null)
f9=f9.replace(w4,"+");
var ar2=ap5.getAttribute("decimalchar");
if (ar2!=null)
f9=f9.replace(ar2,".");
if (!isNaN(f9))
return h5;
else 
return v6.innerHTML;
}
function PercentCellType_setValue(v6,v2){
var f9=v6;
while (f9.firstChild!=null&&f9.firstChild.nodeName!="#text")f9=f9.firstChild;
v6=f9;
if (v2!=null&&v2!=""){
var ap5=the_fpSpread.GetCell(v6);
if (ap5.getAttribute("FpRef")!=null)ap5=document.getElementById(ap5.getAttribute("FpRef"));
var ar5=ap5.getAttribute("percentchar");
if (ar5==null)ar5="%";
v2=v2.replace(" ","");
v2=v2.replace(ar5,"");
v6.innerHTML=v2+ar5;
}else {
v6.innerHTML="";
}
}
function PercentCellType_isValid(i4,v2){
if (v2!=null){
var ap5=the_fpSpread.GetCell(i4);
if (ap5.getAttribute("FpRef")!=null)ap5=document.getElementById(ap5.getAttribute("FpRef"));
var ar5=ap5.getAttribute("percentchar");
if (ar5==null)ar5="%";
v2=v2.replace(ar5,"");
var aq9=ap5.getAttribute("groupchar");
if (aq9==null)aq9=",";
var u3=v2.length;
while (true){
v2=v2.replace(aq9,"");
if (v2.length==u3)break ;
u3=v2.length;
}
var ar6=v2;
var ar0=ap5.getAttribute("negsign");
var w4=ap5.getAttribute("possign");
if (ar0!=null)v2=v2.replace(ar0,"-");
if (w4!=null)v2=v2.replace(w4,"+");
var ar2=ap5.getAttribute("decimalchar");
if (ar2!=null)
v2=v2.replace(ar2,".");
var p0=!isNaN(v2);
if (p0){
var ar7=ap5.getAttribute("MinimumValue");
var ar8=ap5.getAttribute("MaximumValue");
if (ar7!=null||ar8!=null){
var u9=parseFloat(ar7);
var j9=parseFloat(ar8);
p0=!isNaN(u9)&&!isNaN(j9);
if (p0){
if (ar2==null)ar2=".";
p0=the_fpSpread.IsDouble(ar6,ar2,ar0,w4,u9*100,j9*100);
}
}
}
if (!p0){
if (ap5.getAttribute("error")!=null)
return ap5.getAttribute("error");
else 
return "Percent:(ex,10"+ar5+")";
}
}
return "";
}
function ListBoxCellType_getValue(v6){
var f9=v6.getElementsByTagName("TABLE");
if (f9.length>0)
{
var i0=f9[0].rows;
for (var j1=0;j1<i0.length;j1++){
var i4=i0[j1].cells[0];
if (i4.selected=="true")
{
var ar9=i4;
while (ar9.firstChild!=null)ar9=ar9.firstChild;
var ap5=ar9.nodeValue;
return ap5;
}
}
}
return "";
}
function ListBoxCellType_setValue(v6,v2){
var f9=v6.getElementsByTagName("TABLE");
if (f9.length>0)
{
f9[0].style.width=(v6.clientWidth-6)+"px";
var i0=f9[0].rows;
for (var j1=0;j1<i0.length;j1++){
var i4=i0[j1].cells[0];
var ar9=i4;
while (ar9.firstChild!=null)ar9=ar9.firstChild;
var ap5=ar9.nodeValue;
if (ap5==v2){
i4.selected="true";
if (f9[0].parentNode.getAttribute("selectedBackColor")!="undefined")
i4.style.backgroundColor=f9[0].parentNode.getAttribute("selectedBackColor");
if (f9[0].parentNode.getAttribute("selectedForeColor")!="undefined")
i4.style.color=f9[0].parentNode.getAttribute("selectedForeColor");
}else {
i4.style.backgroundColor="";
i4.style.color="";
i4.selected="";
i4.bgColor="";
}
}
}
}
function TextCellType_getValue(v6){
var i4=the_fpSpread.GetCell(v6,true);
if (i4!=null&&i4.getAttribute("password")!=null){
if (i4!=null&&i4.getAttribute("value")!=null)
return i4.getAttribute("value");
else 
return "";
}else {
var f9=v6;
while (f9.firstChild!=null&&f9.firstChild.nodeName!="#text")f9=f9.firstChild;
if (f9.innerHTML=="&nbsp;")return "";
var f9=the_fpSpread.ReplaceAll(f9.innerHTML,"&nbsp;"," ");
var f9=the_fpSpread.ReplaceAll(f9,"<br>","\n");
return f9;
}
}
function TextCellType_setValue(v6,v2){
var i4=the_fpSpread.GetCell(v6,true);
if (i4==null)return ;
var f9=v6;
while (f9.firstChild!=null&&f9.firstChild.nodeName!="#text")f9=f9.firstChild;
v6=f9;
if (i4.getAttribute("password")!=null){
if (v2!=null&&v2!=""){
v2=v2.replace(" ","");
v6.innerHTML="";
for (var f5=0;f5<v2.length;f5++)
v6.innerHTML+="*";
i4.setAttribute("value",v2);
}else {
v6.innerHTML="";
i4.setAttribute("value","");
}
}else {
v2=the_fpSpread.ReplaceAll(v2,"\n","<br>");
v6.innerHTML=the_fpSpread.ReplaceAll(v2," ","&nbsp;");
}
}
function RadioButtonListCellType_getValue(v6){
var i4=the_fpSpread.GetCell(v6,true);
if (i4==null)return ;
var as0=i4.getElementsByTagName("INPUT");
for (var f5=0;f5<as0.length;f5++){
if (as0[f5].tagName=="INPUT"&&as0[f5].checked){
return as0[f5].value;
}
}
return "";
}
function RadioButtonListCellType_getEditorValue(v6){
return RadioButtonListCellType_getValue(v6);
}
function RadioButtonListCellType_setValue(v6,v2){
var i4=the_fpSpread.GetCell(v6,true);
if (i4==null)return ;
if (v2!=null)v2=the_fpSpread.Trim(v2);
var as0=i4.getElementsByTagName("INPUT");
for (var f5=0;f5<as0.length;f5++){
if (as0[f5].tagName=="INPUT"&&v2==the_fpSpread.Trim(as0[f5].value)){
as0[f5].checked=true;
break ;
}else {
if (as0[f5].checked)as0[f5].checked=false;
}
}
}
function RadioButtonListCellType_setFocus(v6){
var i4=the_fpSpread.GetCell(v6,true);
if (i4==null)return ;
var j4=i4.getElementsByTagName("INPUT");
if (j4==null)return ;
for (var f5=0;f5<j4.length;f5++){
if (j4[f5].type=="radio"&&j4[f5].checked){
j4[f5].focus();
return ;
}
}
}
function MultiColumnComboBoxCellType_setValue(v6,v2,f3){
var i4=the_fpSpread.GetCell(v6,true);
if (i4==null)return ;
var as1=i4.getElementsByTagName("DIV");
if (as1!=null&&as1.length>0){
var as2=i4.getElementsByTagName("input");
if (as2!=null&&as2.length>0)
as2[0].value=v2;
return ;
}
if (v2!=null&&v2!="")
v6.textContent=v2;
else 
v6.innerHTML="&nbsp;";
}
function MultiColumnComboBoxCellType_getValue(v6,f3){
var t6=v6.textContent;
var k0=the_fpSpread.GetCell(v6,true);
var as1=k0.getElementsByTagName("DIV");
if (as1!=null&&as1.length>0){
var as2=k0.getElementsByTagName("input");
if (as2!=null&&as2.length>0)
return as2[0].value;
return ;
}
if (!f3)return null;
var t7=the_fpSpread.GetCellEditorID(f3,k0);
var b7=null;
if (t7!=null&&typeof(t7)!="undefined"){
b7=the_fpSpread.GetCellEditor(f3,t7,true);
if (b7!=null){
var as3=b7.getAttribute("MccbId");
if (as3){
FarPoint.System.WebControl.MultiColumnComboBoxCellType.CheckInit(as3);
var as4=eval(as3+"_Obj");
if (as4!=null&&as4.SetText!=null){
as4.SetText(t6);
return t6;
}
}
}
return null;
}
return t6;
}
function MultiColumnComboBoxCellType_getEditorValue(v6,f3){
var i4=the_fpSpread.GetCell(v6,true);
if (i4==null)return ;
var as5=i4.getElementsByTagName("INPUT");
if (as5!=null&&as5.length>0){
var f9=as5[0];
return f9.value;
}
return null;
}
function MultiColumnComboBoxCellType_setFocus(v6){
var i4=the_fpSpread.GetCell(v6);
var f3=the_fpSpread.GetSpread(i4);
if (i4==null)return ;
var as6=i4.getElementsByTagName("DIV");
if (as6!=null&&as6.length>0){
var as3=as6[0].getAttribute("MccbId");
if (as3){
var as4=eval(as3+"_Obj");
if (as4!=null&&typeof(as4.FocusForEdit)!="undefined"){
as4.FocusForEdit();
}
}
}
}
function MultiColumnComboBoxCellType_setEditorValue(v6,editorValue,f3){
var i4=the_fpSpread.GetCell(v6,true);
if (i4==null)return ;
var t7=the_fpSpread.GetCellEditorID(f3,i4);
var b7=null;
if (t7!=null&&typeof(t7)!="undefined"){
b7=the_fpSpread.GetCellEditor(f3,t7,true);
if (b7!=null){
var as3=b7.getAttribute("MccbId");
if (as3){
FarPoint.System.WebControl.MultiColumnComboBoxCellType.CheckInit(as3);
var as4=eval(as3+"_Obj");
if (as4!=null&&as4.SetText!=null){
as4.SetText(editorValue);
}
}
}
}
}
function TagCloudCellType_getValue(v6,f3){
var t6=v6.textContent;
if (typeof(t6)!="undefined"&&t6!=null&&t6.length>0)
{
t6=the_fpSpread.ReplaceAll(t6,"<br>","");
t6=the_fpSpread.ReplaceAll(t6,"\n","");
t6=the_fpSpread.ReplaceAll(t6,"\t","");
var s1=new RegExp("\xA0","g");
t6=t6.replace(s1,String.fromCharCode(32));
t6=the_fpSpread.HTMLDecode(t6);
}
else 
t6="";
return t6;
}
if (typeof(Sys)!='undefined'&&typeof(Sys.Application)!='undefined'){
Sys.Application.notifyScriptLoaded();
}
