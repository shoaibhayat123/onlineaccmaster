//
//	Copyright?2005. FarPoint Technologies.	All rights reserved.
//

eval("var FarPoint={};");
FarPoint.System={};
FarPoint.System.CheckBrowserByName=function (browsername,version){
var f2=window.navigator.userAgent;
var f3=false;
var f4=(""+browsername).toLowerCase();
if ((f4.indexOf("ms")>=0)||(f4.indexOf("msie")>=0)||(f4.indexOf("ie")>=0))
f3=(f2.indexOf("MSIE")>=1);
else if ((f4.indexOf("safari")>=0)||(f4.indexOf("apple")>=0))
f3=(f2.indexOf("Safari")>=1);
else if ((f4.indexOf("ff")>=0)||(f4.indexOf("firefox")>=0))
f3=(f2.indexOf("Firefox")>=1);
return f3;
};
FarPoint.System.IsChild=function (parent,child){
if (child==null||parent==null)return false;
var f5=child.parentNode;
while (f5!=null){
if (f5==parent)return true;
f5=f5.parentNode;
}
return false;
};
FarPoint.System.FindElementById=function (ctl,id,ATTRI_ID){
if (ctl==null)return null;
var f6=ctl.getAttribute(ATTRI_ID);
if (f6==null)return null;
return document.getElementById(f6+id);
};
FarPoint.System.GetEvent=function (e){
if (e!=null)return e;
return window.event;
};
FarPoint.System.GetTarget=function (e){
e=FarPoint.System.GetEvent(e);
if (e.target==document&&e.currentTarget!=null)return e.currentTarget;
if (e.target!=null)return e.target;
return e.srcElement;
};
FarPoint.System.CancelDefault=function (e){
if (e.preventDefault!=null){
e.preventDefault();
e.stopPropagation();
}else {
e.cancelBubble=true;
e.returnValue=false;
}
return false;
};
FarPoint.System.GetMouseCoords=function (ev){
if (ev.pageX||ev.pageY){
return {x:ev.pageX,y:ev.pageY};
}
return {
x:ev.clientX+document.body.scrollLeft-document.body.clientLeft,
y:ev.clientY+document.body.scrollTop-document.body.clientTop
};
};
FarPoint.System.GetOffsetTop=function (ctl){
var f7=0;
var f5=ctl;
while (ctl){
if ((ctl.tagName!="HTML")&&(typeof(ctl.tagName)!="undefined"))
f7+=typeof(ctl.offsetTop)!="undefined"?ctl.offsetTop:0;
if (typeof(ctl.clientTop)=="number")f7+=ctl.clientTop;
ctl=ctl.offsetParent;
}
while (f5){
if ((f5.tagName!="HTML")&&(typeof(f5.tagName)!="undefined"))
f7-=typeof(f5.scrollTop)!="undefined"?f5.scrollTop:0;
f5=f5.parentNode;
}
return parseInt(f7);
};
FarPoint.System.GetOffsetLeft=function (ctl){
var f8=0;
while (ctl){
if ((ctl.tagName!="HTML")&&(typeof(ctl.tagName)!="undefined"))
f8+=typeof(ctl.offsetLeft)!="undefined"?ctl.offsetLeft:0-typeof(ctl.scrollLeft)!="undefined"?ctl.scrollLeft:0;
if (typeof(ctl.clientLeft)=="number")f8+=ctl.clientLeft;
ctl=ctl.offsetParent;
}
return parseInt(f8);
};
FarPoint.System.AttachEvent=function (target,event,handler,useCapture){
if (target==null||event==null||handler==null)return ;
if (target.addEventListener!=null){
target.addEventListener(event,handler,useCapture);
}else if (target.attachEvent!=null){
target.attachEvent("on"+event,handler);
}
};
FarPoint.System.DetachEvent=function (target,event,handler,useCapture){
if (target==null||event==null||handler==null)return ;
if (target.removeEventListener!=null){
target.removeEventListener(event,handler,useCapture);
}else if (target.detachEvent!=null){
target.detachEvent("on"+event,handler);
}
};
FarPoint.System.Track=function (msg){
if (!FarPoint.System.Config.Consts.$FLAG_ISDEBUG)return ;
if (document.getElementById("txtOutput")==null){
var f9=document.createElement("textarea");
f9.id="txtOutput";
f9.style.width="100%";
f9.style.height="100px";
if (f9.style.bottom!=null&&f9.style.right!=null){
f9.style.bottom="0px";
f9.style.right="0px";
}
f9.style.color="#00ff00";
f9.style.position="absolute";
f9.style.backgroundColor="black";
if (FarPoint.System.CheckBrowserByName("IE")){
window.onload=function (){
if (document.all&&document.body.readyState=="complete"){
document.body.appendChild(f9);
}
};
}else {
document.body.appendChild(f9);
}
}
var g0=document.getElementById("txtOutput");
if (g0!=null){
g0.value="&nbsp;&nbsp;"+msg+"\r\n"+g0.value;
}
};
FarPoint.System.Config={};
FarPoint.System.Config.Consts={};
var g1=FarPoint.System.Config.Consts;
g1.$FLAG_ISDEBUG=false;
g1.$LEFT=37;
g1.$RIGHT=39;
g1.$UP=38;
g1.$DOWN=40;
g1.$ENTER=13;
g1.$CANCEL=27;
g1.$PageUp=33;
g1.$PageDown=34;
g1.$Home=36;
g1.$End=35;
g1.$Tab=9;
FarPoint.System.WebControl={};
FarPoint.System.WebControl.MultiColumnComboBoxCellTypeUtilitis={};
var g2=FarPoint.System.WebControl.MultiColumnComboBoxCellTypeUtilitis;
var g3=g2.Consts={
$ATTRI_MULTICOMBO_PART_TYPE:"MccbPartType",
$ATTRI_LIST_ALIGNMENT:"MccbListAlignment",
$ATTRI_LIST_OFFSET:"MccbListOffset",
$ATTRI_LIST_WIDTH:"MccbListWidth",
$ATTRI_LIST_HEIGHT:"MccbListHEIGHT",
$ATTRI_COLUMN_EDIT:"MccbColumnEdit",
$ATTRI_COLUMN_DATA:"MccbColumnData",
$ATTRI_ID:"MccbId",
$ATTRI_LIST_MIN_HEIGHT:"MccbListMinHeight",
$ATTRI_LIST_MIN_WIDTH:"MccbListMinWidth",
$TYPE_DROPDOWNBUTTON:"DropDownButton",
$ID_BUTTON_OUTSIDE:"_DropDownButtonOutside",
$ID_BUTTON_INSIDE:"_DropDownButtonInside",
$ID_INPUT:"_Input",
$ID_CONTAINER:"_Container",
$ID_CONTAINER_DIV:"_ContainerDiv",
$ID_SPREAD:"_FpSpread",
$ID_STATUS_RESIZE:"_ResizeButton",
$OBJECT_SUFFIX:"_Obj"
};
g2.CloseAll=function (){
var g4=document.body.lastChild;
if (g4!=null&&g4.tagName!=null&&g4.tagName=="DIV"){
if (g4.id!=null&&g4.close&&g4.id.match(new RegExp(g3.$ID_CONTAINER_DIV+"$"))){
g4.close();
}
}
};
FarPoint.System.WebControl.MultiColumnComboBoxCellType=function (mcId){
this.mc=document.getElementById(mcId);
if (this.mc==null)return null;
var g5=true;
var g6=null;
var g7=false;
var g8=false;
var g9=-1;
var h0=0;
var h1=false;
var h2=0;
var h3=0;
var h4=50;
var h5=200;
var h6=false;
var h7=this;
this.Init=function (){
if (g5){
this.InitSpread();
this.setController();
var h8=0;
while (h8<12){
if (h8==0||h8==1||h8==3){
h8++;
continue ;
}
if (g6[h8]!=null&&g6[h8].event=="SelectionChanged"){
if (FarPoint.System.CheckBrowserByName("IE")){
this.getFpSpread().onSelectionChanged=g6[h8].handler;
h8++;
continue ;
}
}
this.SetHandler(g6,h8,0);
h8++;
}
var h9=this.getControl();
var i0=FarPoint.System.FindElementById(h9,g3.$ID_BUTTON_INSIDE,g3.$ATTRI_ID);
if (h9!=null&&i0!=null){
if (h9.offsetHeight-5>0)
i0.style.height=h9.offsetHeight-5;
}
this.setListWidth(parseInt(this.getControl().getAttribute(g3.$ATTRI_LIST_WIDTH)));
this.setListHeight(parseInt(this.getControl().getAttribute(g3.$ATTRI_LIST_HEIGHT)));
h9.Init=true;
g5=false;
}
}
this.Dispose=function (){
var i1=this.getController();
if (!i1)return ;
var h8=0;
while (g6[h8]!=null){
if (g6[h8].event=="SelectionChanged"){
if (FarPoint.System.CheckBrowserByName("IE")){
this.getFpSpread().onSelectionChanged=null;
h8++;
continue ;
}
}
this.SetHandler(g6,h8,1);
h8++;
}
}
this.getDragOffsetX=function (){
return h2;
}
this.setDragOffsetX=function (value){
h2=value;
}
this.getDragOffsetY=function (){
return h3;
}
this.setDragOffsetY=function (value){
h3=value;
}
this.getStatusBarHeight=function (){
return 13;
}
this.getIsDrag=function (){
return h1;
}
this.setIsDrag=function (value){
var i2=12;
h1=value;
if (h1){
this.SetHandler(g6,i2,0);
}else {
this.SetHandler(g6,i2,1);
}
}
this.getHostSpread=function (){
var i3=this.getFpSpread();
if (i3==null)return null;
var i4=FarPoint.System.CheckBrowserByName("IE")?i3.hostspread:i3.getAttribute("hostspread");
return document.getElementById(i4);
}
this.getControl=function (){
return this.mc;
}
this.getFpSpread=function (){
var i5=this.getContainer();
if (i5==null)return null;
return i5.getElementsByTagName("div")[0];
}
this.getContainer=function (){
var h9=this.getControl();
if (h9==null)return null;
return FarPoint.System.FindElementById(h9,g3.$ID_CONTAINER,g3.$ATTRI_ID);
}
this.getContainerDiv=function (){
var h9=this.getControl();
if (h9==null)return null;
return FarPoint.System.FindElementById(h9,g3.$ID_CONTAINER_DIV,g3.$ATTRI_ID);
}
this.getInputControl=function (){
var h9=this.getControl();
if (h9==null)return null;
return FarPoint.System.FindElementById(h9,g3.$ID_INPUT,g3.$ATTRI_ID);
}
this.getResizeButton=function (){
var h9=this.getControl();
if (h9==null)return null;
return FarPoint.System.FindElementById(h9,g3.$ID_STATUS_RESIZE,g3.$ATTRI_ID);
}
this.getController=function (){
return g6;
}
this.setController=function (){
if (g6==null){
g6={
1:{target:document,event:"mousedown",handler:function (event){h7.MouseDownOutside(event)},useCapture:false},
3:{target:document,event:"mouseup",handler:function (event){h7.MouseUpOutside(event)},useCapture:false},
12:{target:document,event:"mousemove",handler:function (event){h7.MouseMove(event)},useCapture:false},
4:{target:this.getControl(),event:"mousedown",handler:function (event){h7.MouseDown(event)},useCapture:false},
7:{target:this.getInputControl(),event:"keydown",handler:function (event){h7.OnInputKeyDown(event)},useCapture:FarPoint.System.CheckBrowserByName("IE")?false:true},
6:{target:this.getContainer(),event:"mousedown",handler:function (event){h7.CancelEvent(event)},useCapture:false},
9:{target:this.getFpSpread(),event:"SelectionChanged",handler:function (event){h7.OnSpreadSelectionChanged(event)},useCapture:false},
8:{target:this.getResizeButton(),event:"mousedown",handler:function (event){h7.ResizeButtonMouseDown(event)},useCapture:false}
};
if (FarPoint.System.CheckBrowserByName("IE")?typeof(this.getFpSpread().EnableClientScript)=="undefined":this.getFpSpread().getAttribute("EnableClientScript")==null){
g6[13]={target:FarPoint.System.CheckBrowserByName("IE")?this.getHostSpread():the_fpSpread.GetViewport(this.getHostSpread()).parentNode,event:"scroll",handler:function (event){h7.MccbctScroll(event)},useCapture:false};
}
}
}
this.getIsDrop=function (){
return g7;
}
this.setIsDrop=function (value){
g7=value;
}
this.getIsDroping=function (){
return g8;
}
this.setIsDroping=function (value){
g8=value;
}
this.getListAlignment=function (){
try {
var h8=this.getControl().getAttribute(g3.$ATTRI_LIST_ALIGNMENT);
return parseInt(h8);
}catch (exception ){
return 0;
}
}
this.getListOffset=function (){
try {
var h8=this.getControl().getAttribute(g3.$ATTRI_LIST_OFFSET);
return parseInt(h8);
}catch (exception ){
return 0;
}
}
this.getListWidth=function (){
return h4;
}
this.setListWidth=function (value){
if ((value<this.getListMinWidth())&&(value!=-1))
h4=this.getListMinWidth();
else {
if (!FarPoint.System.CheckBrowserByName("IE"))
if (value>2000)
value=2000;
h4=value;
}
}
this.getListHeight=function (){
return h5;
}
this.setListHeight=function (value){
if (value<this.getListMinHeight())
h5=this.getListMinHeight();
else 
h5=value;
}
this.getListMinWidth=function (){
try {
var h8=this.getControl().getAttribute(g3.$ATTRI_LIST_MIN_WIDTH);
return Math.min(Math.abs(parseInt(h8)),32767);
}catch (exception ){
return 50;
}
}
this.getListMinHeight=function (){
try {
var h8=this.getControl().getAttribute(g3.$ATTRI_LIST_MIN_HEIGHT);
return Math.min(Math.abs(parseInt(h8)),32767);
}catch (exception ){
return 50;
}
}
this.getEditColumnIndex=function (){
try {
var h8=this.getControl().getAttribute(g3.$ATTRI_COLUMN_EDIT);
return parseInt(h8);
}catch (exception ){
return 0;
}
}
this.setSelectedIndex=function (value){
g9=value;
}
this.getSelectedIndex=function (){
if (g9!=-1){
var i6=this.getFpSpread();
var i7=FarPoint.System.CheckBrowserByName("IE")?parseInt(i6.ActiveRow):parseInt(i6.GetActiveRow());
if (i6){
if (i7>=0){
this.setSelectedIndex(i7);
}
}
}
return g9;
}
this.setActiveColumnIndex=function (value){
h0=value;
}
this.getActiveColumnIndex=function (){
return h0;
}
this.getDataColumnIndex=function (){
try {
var h8=this.getControl().getAttribute(g3.$ATTRI_COLUMN_DATA);
return parseInt(h8);
}catch (exception ){
return 0;
}
}
this.FocusForEdit=function (){
var h9=this.getControl();
if (h9==null)return ;
if (h9.parentNode==null||typeof(h9.parentNode.tagName)=="undefined")return ;
if (h9.parentNode.tagName!="TD")return ;
if (h9.parentNode.getAttribute("FpCellType")!="MultiColumnComboBoxCellType")return ;
var i8=this.getInputControl();
if (i8!=null){
try {
i8.focus();
i8.select();
}catch (exception ){}
this.SetHandler(g6,7,1);
this.SetHandler(g6,7,0);
}
}
this.LockFocus=function (event){
var i9=this.getInputControl();
if (i9!=null&&typeof(i9.focus)!="undefined")
if (i9!=null){
try {
i9.focus();
i9.select();
}catch (exception ){}
}
}
this.MouseDown=function (event){
if (!FarPoint.System.CheckBrowserByName("IE")&&this.getControl().getAttribute("disabled")=="disabled")return FarPoint.System.CancelDefault(event);
if (!this.getIsDrop()&&event.button!=(FarPoint.System.CheckBrowserByName("IE")?1:0))return ;
var j0=FarPoint.System.GetTarget(event);
if (j0==null||j0.getAttribute(g3.$ATTRI_MULTICOMBO_PART_TYPE)!=g3.$TYPE_DROPDOWNBUTTON)return false;
this.setIsDroping(true);
var j1=this;
setTimeout(function (){j1.DropDown();},0);
}
this.DropDown=function (){
this.ShowHideContainer(!this.getIsDrop());
}
this.OnInputKeyDown=function (event){
if (event.altKey&&event.keyCode==g1.$DOWN){
this.ShowHideContainer(!this.getIsDrop());
FarPoint.System.CancelDefault(event);
return false;
}
switch (event.keyCode){
case g1.$UP:
if (this.getIsDrop()){
this.ChangeSelectedIndex(-1);
var j2=this.getFpSpread();
if (!FarPoint.System.CheckBrowserByName("IE")){
the_fpSpread.ScrollTo(j2,j2.GetActiveRow(),this.getActiveColumnIndex());
}else {
j2.ScrollTo(j2.ActiveRow,this.getActiveColumnIndex());
}
if (FarPoint.System.CheckBrowserByName("IE"))
FarPoint.System.CancelDefault(event);
}
if (!FarPoint.System.CheckBrowserByName("IE")){
FarPoint.System.CancelDefault(event);
}
break ;
case g1.$LEFT:
if (this.getIsDrop()){
this.ChangedActiveColumnIndex(true);
FarPoint.System.CancelDefault(event);
}
break ;
case g1.$DOWN:
if (this.getIsDrop()){
this.ChangeSelectedIndex(1);
var j2=this.getFpSpread();
if (!FarPoint.System.CheckBrowserByName("IE")){
the_fpSpread.ScrollTo(j2,j2.GetActiveRow(),this.getActiveColumnIndex());
}else {
j2.ScrollTo(j2.ActiveRow,this.getActiveColumnIndex());
}
if (FarPoint.System.CheckBrowserByName("IE"))
FarPoint.System.CancelDefault(event);
}
if (!FarPoint.System.CheckBrowserByName("IE")){
FarPoint.System.CancelDefault(event);
}
break ;
case g1.$RIGHT:
if (this.getIsDrop()){
this.ChangedActiveColumnIndex(false);
FarPoint.System.CancelDefault(event);
}
break ;
case g1.$ENTER:
if (this.getIsDrop()){
this.ShowHideContainer(false);
FarPoint.System.CancelDefault(event);
}
if (!FarPoint.System.CheckBrowserByName("safari")&&this.getFpSpread().getAttribute("EnableClientScript")=="0"){
return FarPoint.System.CancelDefault(event);
}
break ;
case g1.$CANCEL:
if (this.getIsDrop()){
this.ShowHideContainer(false);
}
FarPoint.System.CancelDefault(event);
break ;
case g1.$PageUp:
if (this.getIsDrop()){
this.ChangeSelectedIndex(null,1);
var j2=this.getFpSpread();
if (!FarPoint.System.CheckBrowserByName("IE")){
the_fpSpread.ScrollTo(j2,j2.GetActiveRow(),this.getActiveColumnIndex());
}else {
j2.ScrollTo(j2.ActiveRow,this.getActiveColumnIndex());
}
if (FarPoint.System.CheckBrowserByName("IE"))
FarPoint.System.CancelDefault(event);
}
if (!FarPoint.System.CheckBrowserByName("IE")){
FarPoint.System.CancelDefault(event);
}
break ;
case g1.$PageDown:
if (this.getIsDrop()){
this.ChangeSelectedIndex(null,2);
var j2=this.getFpSpread();
if (!FarPoint.System.CheckBrowserByName("IE")){
the_fpSpread.ScrollTo(j2,j2.GetActiveRow(),this.getActiveColumnIndex());
}else {
j2.ScrollTo(j2.ActiveRow,this.getActiveColumnIndex());
}
if (FarPoint.System.CheckBrowserByName("IE"))
FarPoint.System.CancelDefault(event);
}
if (!FarPoint.System.CheckBrowserByName("IE")){
FarPoint.System.CancelDefault(event);
}
break ;
case g1.$Home:
if (this.getIsDrop()){
this.ChangeSelectedIndex(null,3);
var j2=this.getFpSpread();
if (!FarPoint.System.CheckBrowserByName("IE")){
the_fpSpread.ScrollTo(j2,j2.GetActiveRow(),this.getActiveColumnIndex());
}else {
j2.ScrollTo(j2.ActiveRow,this.getActiveColumnIndex());
}
if (FarPoint.System.CheckBrowserByName("IE"))
FarPoint.System.CancelDefault(event);
}
if (!FarPoint.System.CheckBrowserByName("IE")){
FarPoint.System.CancelDefault(event);
}
break ;
case g1.$End:
if (this.getIsDrop()){
this.ChangeSelectedIndex(null,4);
var j2=this.getFpSpread();
if (!FarPoint.System.CheckBrowserByName("IE")){
the_fpSpread.ScrollTo(j2,j2.GetActiveRow(),this.getActiveColumnIndex());
}else {
j2.ScrollTo(j2.ActiveRow,this.getActiveColumnIndex());
}
if (FarPoint.System.CheckBrowserByName("IE"))
FarPoint.System.CancelDefault(event);
}
if (!FarPoint.System.CheckBrowserByName("IE")){
FarPoint.System.CancelDefault(event);
}
break ;
case g1.$Tab:
if (this.getIsDrop()){
this.ShowHideContainer(false);
if (!FarPoint.System.CheckBrowserByName("IE")){
FarPoint.System.CancelDefault(event);
if (FarPoint.System.CheckBrowserByName("FF")){
var j3=this.getControl().getElementsByTagName("input")[1];
if (j3!=null){
var j4=document.createEvent('KeyboardEvent');
j4.initKeyEvent('keydown',true,true,null,null,null,null,null,9,null);
setTimeout(function (){j3.dispatchEvent(j4)},0);
}
}
}
}
break ;
}
}
this.OnSpreadSelectionChanged=function (event){
var i6=this.getFpSpread();
if (i6==null)return ;
if (!FarPoint.System.CheckBrowserByName("IE")){
this.setSelectedIndex(i6.GetActiveRow());
}else {
this.setSelectedIndex(i6.ActiveRow);
}
var i8=this.getInputControl();
if (i8==null)return ;
if (this.getSelectedIndex()>=0&&this.getEditColumnIndex()>=0){
i8.value=i6.GetValue(this.getSelectedIndex(),this.getEditColumnIndex());
i8.select();
}
FarPoint.System.GetEvent(event).cancelBubble=true;
}
this.CancelEvent=function (event){
if (this.getIsDrop()){
this.LockFocus(event);
var j5=this;
setTimeout(function (){j5.LockFocus(event);},0);
}
return FarPoint.System.CancelDefault(event);
}
this.MouseDownOutside=function (event){
if (this.getIsDrag())
this.setIsDrag(false);
var j6=this.getContainerDiv();
var j7=this.getControl();
var j2=this.getFpSpread();
var j8=document.getElementById(j2.id+"_viewport");
var j0=FarPoint.System.GetTarget(event);
if (!FarPoint.System.IsChild(j6,j0)&&!FarPoint.System.IsChild(j7,j0)){
this.setIsDroping(false);
if (this.getIsDrop())
this.ShowHideContainer(false);
}
var j5=this;
setTimeout(function (){j5.LockFocus(event);},0);
}
this.MouseUpOutside=function (event){
var j0=FarPoint.System.GetTarget(event);
var j7=this.getControl();
if (this.getIsDroping()&&FarPoint.System.IsChild(j7,j0)){
this.setIsDroping(false);
return ;
}
if (this.getIsDrop()==false)return ;
if (this.getIsDrag()){
this.setIsDrag(false);
return ;
}
var j6=this.getContainerDiv();
var j2=this.getFpSpread();
var j8=document.getElementById(j2.id+"_viewport");
if (FarPoint.System.IsChild(j8,j0)||!FarPoint.System.IsChild(j6,j0)){
this.ShowHideContainer(false);
}
var j5=this;
setTimeout(function (){j5.LockFocus(event);},0);
}
this.ResizeButtonMouseDown=function (event){
this.LockFocus(event);
if (event.button!=(FarPoint.System.CheckBrowserByName("IE")?1:0))return ;
var i5=this.getContainer();
if (i5==null)return ;
var j9=FarPoint.System.GetMouseCoords(event);
this.setDragOffsetX(parseInt(i5.offsetLeft+i5.offsetWidth)-j9.x);
this.setDragOffsetY(parseInt(i5.offsetTop+i5.offsetHeight)-j9.y);
this.setIsDrag(true);
if (event.preventDefault)event.preventDefault();
event.returnValue=false;
event.cancelBubble=true;
return false;
}
this.MouseMove=function (event){
if (!this.getIsDrag())return ;
var i5=this.getContainer();
if (i5==null)return ;
var k0=this.getContainerDiv();
if (k0==null)return ;
var j9=FarPoint.System.GetMouseCoords(event);
var k1=j9.x-parseInt(i5.offsetLeft)+this.getDragOffsetX();
var k2=j9.y-parseInt(i5.offsetTop)+this.getDragOffsetY()-5;
if (k1>this.getListMinWidth()&&Math.abs(k1-i5.offsetWidth)>5){
k0.style.width=k1+"px";
i5.style.width=k1+"px";
this.setListWidth(k1+5);
}
if (k2>this.getListMinHeight()&&Math.abs(k2-i5.offsetHeight)>20){
var j2=this.getFpSpread();
if (j2!=null)
j2.style.height=""+(k2-this.getStatusBarHeight())+"px";
k0.style.height=(k2+5)+"px";
i5.style.height=k2+"px";
this.setListHeight(k2);
}
if (!FarPoint.System.CheckBrowserByName("IE")){
var k3=this.getFpSpread();
the_fpSpread.SizeSpread(k3)
the_fpSpread.Refresh(k3);
}
event.cancelBubble=true;
return false;
}
this.MccbctScroll=function (event){
var h9=this.getControl();
if (h9==null)return ;
var i5=this.getContainer();
if (i5==null)return ;
var k0=this.getContainerDiv();
if (k0==null)return ;
if (FarPoint.System.CheckBrowserByName("safari")&&h9.offsetHeight==0){
k0.style.top=(FarPoint.System.GetOffsetTop(h9)+25)-(this.GetSpreadClientData(this.getHostSpread(),1))+"px";
}else {
var k4=null;
if (FarPoint.System.CheckBrowserByName("IE")?typeof(this.getFpSpread().EnableClientScript)=="undefined":this.getFpSpread().getAttribute("EnableClientScript")==null)
k4=FarPoint.System.CheckBrowserByName("IE")?document.getElementById(this.getHostSpread().id+"_view"):document.getElementById(this.getHostSpread().id+"_viewport").parentNode;
var k5=FarPoint.System.IsChild(k4,h9)?(this.GetSpreadClientData(this.getHostSpread(),1)):0;
k0.style.top=(FarPoint.System.GetOffsetTop(h9)+h9.offsetHeight+2)-k5+"px";
}
}
this.GetAdjustorForScroll=function (){
var k6=0;var k7=0;
var f6=new String(this.getControl().getAttribute(g3.$ATTRI_ID));
k6=parseInt(f6.split(new RegExp("_"))[1]);
k7=parseInt(f6.split(new RegExp("_"))[2]);
var k8=f6.split(new RegExp("_"))[3];
var k9=this.getHostSpread();
var l0=FarPoint.System.CheckBrowserByName("IE");
var l1=FarPoint.System.CheckBrowserByName("safari");
var l2={left:0,top:0};
if (this.getFpSpread().getAttribute("EnableClientScript")=="0")
return l2;
if (l1){
if (k8!="sc"&&k8!="rh")
l2.left=this.GetSpreadClientData(k9,0);
if (k8!="ch"&&k8!="cf"&&k8!="sc")
l2.top=this.GetSpreadClientData(k9,1);
return l2;
}
var l3=l0?k9.getViewport():the_fpSpread.GetViewport(k9);
var l4=l0?k9.getViewport0():the_fpSpread.GetViewport0(k9);
var l5=l0?k9.getViewport1():the_fpSpread.GetViewport1(k9);
var l6=l0?k9.getViewport2():the_fpSpread.GetViewport2(k9);
var l7=0;var l8=0;
l7=l0?(l5!=null?l5.rows.length:0):k9.frzRows;
if (l0){
if (l4!=null){
var l9=l4.getElementsByTagName("COLGROUP");
if (l9!=null&&l9.length>0)
l8=l9[0].childNodes.length;
}else if (l6!=null){
var l9=l6.getElementsByTagName("COLGROUP");
if (l9!=null&&l9.length>0)
l8=l9[0].childNodes.length;
}
}else {
l8=k9.frzCols;
}
if ((k8!="ch"&&k8!="cf"&&k8!="sc")&&((l7>0&&(k6+1)>l7)||l7==0))
l2.top=this.GetSpreadClientData(k9,1);
if ((k8!="sc"&&k8!="rh")&&((l8>0&&(k7+1)>l8)||l8==0))
l2.left=this.GetSpreadClientData(k9,0);
return l2;
}
this.InitSpread=function (){
if (!FarPoint.System.CheckBrowserByName("IE")&&typeof(the_fpSpread)!="undefined"){
var j2=this.getFpSpread();
the_fpSpread.Init(j2);
the_fpSpread.SizeAll(j2);
j2.dispose=function (){
the_fpSpread.Dispose(j2);
}
}
}
this.IsContained=function (child){
return FarPoint.System.IsChild(this.getControl(),child)||FarPoint.System.IsChild(this.getContainer(),child);
}
this.GetActivePositonInDomTree=function (element){
if (element==null)return false;
while (element!=null&&element!=document.body){
if (element.tagName=="TR"&&element.getAttribute("FpSpread")!=null)return element.getAttribute("FpSpread");
element=element.parentNode;
}
return "";
}
this.GetSpreadClientData=function (j2,whichData){
if (this.getFpSpread().getAttribute("EnableClientScript")=="0")return 0;
var m0="";
var m1=0;
var m2=null;
if (FarPoint.System.CheckBrowserByName("ie")){
if (j2.GetParentSpread()!=null)return ;
m2=document.getElementById(j2.id+"_XMLDATA");
switch (whichData){
case 0:
m0="/root/scrollLeft";
break ;
case 1:
m0="/root/scrollTop";
break ;
}
var m1=m2.documentElement.selectSingleNode(m0);
if (m1!=null&&m1.text!=""){
m1=parseInt(m1.text);
}
}else {
if (the_fpSpread.GetParentSpread(j2)!=null)return ;
m2=the_fpSpread.GetData(j2);
var m3=m2.getElementsByTagName("root")[0];
switch (whichData){
case 0:
m0="scrollLeft";
break ;
case 1:
m0="scrollTop";
break ;
}
var m4=m3.getElementsByTagName(m0)[0];
if (m4!=null&&m4.innerHTML!=""){
m1=parseInt(m4.innerHTML);
}
}
if (isNaN(m1))m1=0;
return m1;
}
this.SetHandler=function (g6,index,method){
if (isNaN(index)||index<0)return ;
if ((typeof(g6[index])=="undefined")||(g6[index]==null))return ;
switch (method){
case 0:
FarPoint.System.AttachEvent(g6[index].target,g6[index].event,g6[index].handler,g6[index].useCapture);
break ;
case 1:
FarPoint.System.DetachEvent(g6[index].target,g6[index].event,g6[index].handler,g6[index].useCapture);
break ;
}
}
this.ChangeSelectedIndex=function (step,caseId){
var m5=0;
if (typeof(caseId)!="undefined")
m5=caseId;
var j2=this.getFpSpread();
if (!j2)return ;
var m6=j2.GetRowCount();
if (m6<=0)return ;
var m7=this.getSelectedIndex();
var m8=this.getActiveColumnIndex();
if (m8<0)m8=0;
switch (m5){
case 0:
m7+=step;
if ((m7<0)||(m7>=m6))return ;
break ;
case 1:
m7-=5;
m7=Math.max(m7,0);
break ;
case 2:
m7+=5;
m7=Math.min(m7,m6-1);
break ;
case 3:
m7=0;
break ;
case 4:
m7=m6-1;
break ;
}
this.setSelectedIndex(m7);
j2.SetActiveCell(m7,m8);
}
this.ChangedActiveColumnIndex=function (IsLeft){
var j2=this.getFpSpread();
if (!j2)return ;
var m9=j2.GetColCount();
if (m9<=0)return ;
var n0=FarPoint.System.CheckBrowserByName("IE")?j2.ActiveRow:j2.GetActiveRow();
var n1=this.getActiveColumnIndex();
if (isNaN(n1))n1=0;
n1=IsLeft?n1-1:n1+1;
if ((n1<0)||(n1>=m9)){
n1=Math.max(n1,0);
n1=Math.min(n1,m9-1);
this.setActiveColumnIndex(n1);
return ;
}
if (FarPoint.System.CheckBrowserByName("IE"))
j2.ScrollTo(n0,n1);
else 
the_fpSpread.ScrollTo(j2,n0,n1);
this.setActiveColumnIndex(n1);
}
this.ShowHideContainer=function (show){
var h9=this.getControl();
if (h9==null)return ;
var i5=this.getContainer();
if (i5==null)return ;
var k0=this.getContainerDiv();
if (k0==null)return ;
if (!FarPoint.System.CheckBrowserByName("IE")){
k0.style.display=(show?'block':'none');
k0.style.visibility=(show?'visible':'hidden');
}
if (show){
i5.style.height=this.getListHeight()+"px";
k0.style.height=(this.getListHeight()+5)+"px";
i5.style.top=(-this.getListHeight()*0.25)+"px";
var l2=this.GetAdjustorForScroll();
if (FarPoint.System.CheckBrowserByName("safari")&&h9.offsetHeight==0){
k0.style.top=(FarPoint.System.GetOffsetTop(h9)+25)-l2.top+"px";
}else {
k0.style.top=(FarPoint.System.GetOffsetTop(h9)+h9.offsetHeight)+"px";
}
var n2=FarPoint.System.GetOffsetLeft(h9);
if (this.getListAlignment()==0)
n2+=this.getListOffset();
else {
var n3=0;
if (this.getListWidth()!=-1)
n3=(this.getListWidth()-h9.parentNode.offsetWidth);
n2-=(this.getListOffset()+n3);
}
if (!isNaN(l2.left))
k0.style.left=n2-l2.left+"px";
else 
k0.style.left=n2+"px";
var n4=this.getListWidth();
if (n4<0)n4=Math.max(this.getListMinWidth(),h9.parentNode.offsetWidth);
i5.style.width=(n4+5)+"px";
k0.style.width=(n4+5)+"px";
document.body.appendChild(k0);
var n5=this;
k0.close=function (){
n5.ShowHideContainer(false);
};
this.SetHandler(g6,1,0);
this.SetHandler(g6,3,0);
this.SetHandler(g6,13,0);
}else {
k0.close=null;
h9.appendChild(k0);
k0.style.top=-10000;
k0.style.left=-10000;
this.SetHandler(g6,1,1);
this.SetHandler(g6,3,1);
this.SetHandler(g6,13,1);
}
var j2=this.getFpSpread();
if (show&&j2!=null){
if (document.documentMode){
j2.style.backgroundColor="white";
j2.style.position="relative";
}
j2.style.height=(parseInt(i5.style.height)-this.getStatusBarHeight())+"px";
}
this.setIsDrop(show);
if (!FarPoint.System.CheckBrowserByName("IE")){
if (j2!=null){
var d5=the_fpSpread.GetColHeader(j2);
if (d5!=null&&FarPoint.System.CheckBrowserByName("Firefox")){
d5.parentNode.style.height=""+(d5.offsetHeight-parseInt(d5.cellSpacing))+"px";
}
the_fpSpread.SizeAll(j2);
the_fpSpread.SizeAll(j2);
if (show){
if (this.getFpSpread().getAttribute("EnableClientScript")!="0"){
the_fpSpread.SetPageActiveSpread(j2);
the_fpSpread.SetActiveSpreadID(j2,j2.id,j2.id,false);
}
var m7=this.getSelectedIndex();
j2.SetActiveCell(m7,0);
this.setActiveColumnIndex(0);
if (j2.GetActiveRow()>-1)
the_fpSpread.ScrollTo(j2,j2.GetActiveRow(),0);
}else {
var n6=this.getHostSpread();
if (this.getFpSpread().getAttribute("EnableClientScript")!="0"){
the_fpSpread.SetPageActiveSpread(n6);
the_fpSpread.SetActiveSpreadID(n6,n6.id,n6.id,false);
}
}
}
}else {
if (show){
var m7=this.getSelectedIndex();
j2.SetActiveCell(m7,0);
this.setActiveColumnIndex(0);
if (j2.ActiveRow>-1)
j2.ScrollTo(j2.ActiveRow,0);
}
}
if (show){
var j1=this;
setTimeout(function (){j1.AnimShow(i5);},30);
}
var i8=this.getInputControl();
if (i8!=null){
try {
i8.focus();
i8.select();
if (i8.value.length>0&&this.getSelectedIndex()==-1){
this.SetText(i8.value);
}
}catch (exception ){}
}
if (FarPoint.System.CheckBrowserByName("IE")){
k0.style.display=(show?'block':'none');
k0.style.visibility=(show?'visible':'hidden');
if (document.documentMode)
{
j2.sizeSpread();
}
}
}
this.AnimShow=function (i5){
var n7=i5.offsetTop;
if (n7>=0)return ;
var n8=n7<-5?n7*0.25:0;
i5.style.top=n8+"px";
var j1=this;
setTimeout(function (){j1.AnimShow(i5);},30);
}
this.SetText=function (text){
var j2=this.getFpSpread();
if (j2==null)return ;
var n9=this.getEditColumnIndex();
var i8=this.getInputControl();
if (text==null){
if (i8!=null){
i8.value="";
this.setSelectedIndex(-1);
}
j2.SetActiveCell(-1,-1);
return ;
}
var o0=text.match(new RegExp("^\\s*(\\S+(\\s+\\S+)*)\\s*$"));
text=(o0==null)?"":o0[1];
if ((text.length==1&&text.charCodeAt(0)==160)||(text=="")||(text.length<=0)){
if (i8!=null){
i8.value="";
this.setSelectedIndex(-1);
}
j2.SetActiveCell(-1,-1);
return ;
}
if ((!FarPoint.System.CheckBrowserByName("IE"))&&typeof(j2.GetRowCount)=="undefined"){
var h8=0,m7=-1;
while ((h8<the_fpSpread.spreads.length)&&(m7==-1)){
if (the_fpSpread.spreads[h8].id==j2.id)m7=h8;
h8++;
}
the_fpSpread.spreads.splice(m7,1);
the_fpSpread.Init(document.getElementById(j2.id));
document.getElementById(j2.id).dispose=function (){
the_fpSpread.Dispose(document.getElementById(j2.id));
}
}
if (!FarPoint.System.CheckBrowserByName("IE")){
text=text.replace(new RegExp("\xA0","g"),String.fromCharCode(32));
}
var o1=j2.GetRowCount();
var m7=0;
for (;m7<o1;m7++){
try {
var o2=j2.GetValue(m7,n9);
if (o2==(FarPoint.System.CheckBrowserByName("IE")?text:the_fpSpread.Trim(text))){
j2.SetActiveCell(m7,n9);
break ;
}
}catch (exception ){
return ;
}
}
}
this.TestProps=function (){
if (!g1.$FLAG_ISDEBUG)return ;
}
this.Init();
}
FarPoint.System.WebControl.MultiColumnComboBoxCellType.CheckInit=function (id){
var h9=document.getElementById(id);
if (h9==null){
h9=document.getElementById(id+"Editor");
id+="Editor";
}
if (h9&&h9.Init)return ;
try {
var o3=eval(id+g3.$OBJECT_SUFFIX);
if (o3){
o3.Dispose();
delete o3;
}
}catch (exception ){}
var o4=id+g3.$OBJECT_SUFFIX+"=new FarPoint.System.WebControl.MultiColumnComboBoxCellType('"+id+"');";
eval(o4);
}
FarPoint.System._ExtenderHelper=function (){
this.ScriptHolderID="__PAGESCRIPT";
this.ScriptBlockID="__SCRIPTBLOCK";
this.StartupScriptID="__STARTUPSCRIPT";
this.CssLinksID="__CSSLINKS";
}
FarPoint.System._ExtenderHelper.prototype={
getExtenderScripts:function (){
var o5={};
var o6=document.getElementsByTagName("input");
for (var h8=0;h8<o6.length;h8++){
var o7=o6[h8].id.match(new RegExp("^(.+)_extender$"));
if (o7&&o7.length==2){
var o8=o7[1];
var o9=$get(o8);
if (o9&&(o9.FpSpread=="Spread"||o9.getAttribute("FpSpread")=="Spread")){
var p0=o6[h8].json||o6[h8].getAttribute("json");
var p1=eval("("+p0+")");
this.mergeExtenderScripts(o5,p1.extenderScripts);
}
}
}
return o5;
},
mergeExtenderScripts:function (j0,source){
for (var p2 in source){
var o2=source[p2];
if (!j0[p2]){
j0[p2]=o2;
}else {
for (var p3=0;p3<o2.length;p3++){
var p4=o2[p3];
if (!Array.contains(j0[p2],p4))j0[p2].push(p4);
}
}
}
},
getNeededExtenderScripts:function (newScripts,realScripts,loadedScripts){
var p5=[];
var p6=[];
for (var p2 in newScripts){
Array.addRange(p5,newScripts[p2]);
}
for (var p2 in loadedScripts){
Array.addRange(p6,loadedScripts[p2]);
}
var p7=[];
for (var h8=0;h8<realScripts.length;h8++){
var o2=realScripts[h8];
if (!Array.contains(p6,o2)&&Array.contains(p5,o2))
p7.push(o2);
else if (!this.checkInclude(o2)&&(o2.indexOf("_TSM_CombinedScripts_")>=0||o2.indexOf("_TSM_HiddenField_")>=0)){
p7.push(o2);
}
}
return p7;
},
checkInclude:function (src){
var p8=document.getElementsByTagName("script");
for (var h8=0;h8<p8.length;h8++){
var p9=p8[h8].getAttribute("src");
if (src==p9){
return true;
}else if (p9&&p9.indexOf("_TSM_CombinedScripts_")>=0&&p9.indexOf("_TSM_HiddenField_")>=0
&&src.indexOf("_TSM_CombinedScripts_")>=0&&src.indexOf("_TSM_HiddenField_")>=0){
return true;
}
}
return false;
},
get_scriptHolder:function (){
var q0=$get(this.ScriptHolderID);
if (!q0){
q0=document.createElement("div");
q0.id=this.ScriptHolderID;
q0.style.display="none";
document.body.appendChild(q0);
}
return q0;
},
saveLoadedExtenderScripts:function (j2,cmd){
var q0=this.get_scriptHolder();
q0.innerHTML="";
var q1=this.getExtenderScripts();
var p6=q0.loaded;
if (!p6)p6={};
this.mergeExtenderScripts(p6,q1);
q0.loaded=p6;
if (typeof(FpExtender)!='undefined'&&cmd!="LoadOnDemand")FpExtender.Util.disposeExtenders(j2);
},
processCss:function (buff){
var q2=document.getElementsByTagName("head");
if (!q2){
q2=document.createElement("head");
document.documentElement.insertBefore(q2,document.body)
}else {
q2=q2[0];
}
var q3=[];
var q4=q2.getElementsByTagName("link");
if (q4){
for (var h8=0;h8<q4.length;h8++){
var q5=q4[h8];
if (q5.getAttribute("type")=="text/css"){
q3.push(q5.getAttribute("href"));
}
}
}
var q6=$get(this.CssLinksID,buff);
if (q6){
q6=eval("("+q6.value+")");
q6=q6.cssLinks;
for (var h8=0;h8<q6.length;h8++){
var q7=q6[h8];
if (!Array.contains(q3,q7)){
var q5=document.createElement("link");
q5.type="text/css";
q5.rel="stylesheet";
q5.href=q7;
q2.appendChild(q5);
}
}
}
},
loadExtenderScripts:function (j2,buff){
if (Sys.Browser.agent!=Sys.Browser.InternetExplorer)
this.processCss(buff);
var q8=[];
var q9=[];
var r0=$get(this.ScriptBlockID,buff);
if (r0){
var r1=new RegExp("<script src=\"(.+)\" type=\"text\\/javascript\"><\\/script>","gm");
var r2;
while ((r2=r1.exec(r0.value))!=null){
q9.push(r2[1]);
}
}
var r3=this.getExtenderScripts();
var q0=this.get_scriptHolder();
var p7=this.getNeededExtenderScripts(r3,q9,q0.loaded);
for (var h8=0;h8<p7.length;h8++)q8.push({src:p7[h8]});
var r4=$get(this.StartupScriptID,buff);
var r5=Sys._ScriptLoader.getInstance();
for (var h8=0;h8<q8.length;h8++){
var r6=q8[h8].src;
if (r6)r5.queueScriptReference(r6);
}
r5.loadScripts(0,function (){
if (r4&&typeof(FpExtender)!=='undefined'){
var r7=false;
var r8=FpExtender.Util.getExtenderInitScripts(j2,r4.value);
for (var h8=0;h8<r8.length;h8++){
eval(r8[h8]);
if (r8[h8].indexOf("Sys.Application.initialize")!=-1)r7=true;
}
if (!r7){
var r9=Sys.Application.getComponents();
for (var h8 in r9){
if (r9[h8].get_id().indexOf(j2.id)==0&&FpExtender.ContainerBehavior.isInstanceOfType(r9[h8]))
r9[h8]._load();
}
}
}
},function (){
},null);
}
}
FarPoint.System.ExtenderHelper=new FarPoint.System._ExtenderHelper();
if (typeof(Sys)!='undefined'&&typeof(Sys.Application)!='undefined'){
Sys.Application.notifyScriptLoaded();
}
