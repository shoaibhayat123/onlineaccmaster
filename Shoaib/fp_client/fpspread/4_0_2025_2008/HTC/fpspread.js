//
//	Copyright?2005. FarPoint Technologies.	All rights reserved.
//
var the_fpSpread = new Fpoint_FPSpread();
function FpSpread_EventHandlers(){
var f2=the_fpSpread;
this.TranslateKeyPress=function (event){
f2.TranslateKeyPress(event);
}
this.TranslateKey=function (event){
f2.TranslateKey(event);
}
this.SetActiveSpread=function (event){
f2.SetActiveSpread(event);
}
this.MouseDown=function (event){
f2.MouseDown(event);
}
this.MouseUp=function (event){
f2.MouseUp(event);
}
this.MouseMove=function (event){
f2.MouseMove(event);
}
this.DblClick=function (event){
f2.DblClick(event);
}
this.HandleFirstKey=function (event){
f2.HandleFirstKey(event);
}
this.DoPropertyChange=function (event){
f2.DoPropertyChange(event);
}
this.CmdbarMouseOver=function (event){
f2.CmdbarMouseOver(event);
}
this.CmdbarMouseOut=function (event){
f2.CmdbarMouseOut(event);
}
this.ScrollViewport=function (event){
f2.ScrollViewport(event);
}
this.Focus=function (event){
var f3=event.target;
f2.Focus(f3);
}
this.docLoad=function (event){
if (f2.spreads==null||f2.spreads.length==0)return ;
var f4=f2.spreads.length;
for (var f5=0;f5<f4;f5++){
var f3=f2.spreads[f5];
var f6=f2.GetRowCount(f3);
if (f6>0){
var f7=f2.GetRowHeightInternal(f3,0);
f2.SetRowHeight2(f3,0,f7);
}
}
}
var f8=window.navigator.userAgent;
var f9=(f8.indexOf("Firefox/3.")>=0);
if (f9)
f2.AttachEvent(document,"keypress",this.TranslateKeyPress,true);
f2.AttachEvent(document,"keydown",this.TranslateKey,true);
f2.AttachEvent(document,"mouseup",this.MouseUp,false);
f2.AttachEvent(document,"mousedown",this.SetActiveSpread,false);
f2.AttachEvent(document,"keyup",this.HandleFirstKey,true);
f2.AttachEvent(window,"resize",f2.DoResize,false);
f2.AttachEvent(window,"DOMMouseScroll",f2.DocScroll,false);
f2.AttachEvent(window,"load",this.docLoad,false);
this.AttachEvents=function (f3){
f2.AttachEvent(f3,"mousedown",this.MouseDown,false);
f2.AttachEvent(f3,"mouseup",this.MouseUp,false);
f2.AttachEvent(f3,"mousemove",this.MouseMove,false);
f2.AttachEvent(f3,"dblclick",this.DblClick,false);
f2.AttachEvent(f3,"focus",this.Focus,false);
var g0=f2.GetViewport(f3);
if (g0!=null){
f2.AttachEvent(f2.GetViewport(f3).parentNode,"DOMAttrModified",this.DoPropertyChange,true);
f2.AttachEvent(f2.GetViewport(f3).parentNode,"scroll",this.ScrollViewport);
}
var g1=f2.GetCommandBar(f3);
if (g1!=null){
f2.AttachEvent(g1,"mouseover",this.CmdbarMouseOver,false);
f2.AttachEvent(g1,"mouseout",this.CmdbarMouseOut,false);
}
}
this.DetachEvents=function (f3){
f2.DetachEvent(f3,"mousedown",this.MouseDown,false);
f2.DetachEvent(f3,"mouseup",this.MouseUp,false);
f2.DetachEvent(f3,"mousemove",this.MouseMove,false);
f2.DetachEvent(f3,"dblclick",this.DblClick,false);
f2.DetachEvent(f3,"focus",this.Focus,false);
var g0=f2.GetViewport(f3);
if (g0!=null){
f2.DetachEvent(f2.GetViewport(f3).parentNode,"DOMAttrModified",this.DoPropertyChange,true);
f2.DetachEvent(f2.GetViewport(f3).parentNode,"scroll",this.ScrollViewport);
}
var g1=f2.GetCommandBar(f3);
if (g1!=null){
f2.DetachEvent(g1,"mouseover",this.CmdbarMouseOver,false);
f2.DetachEvent(g1,"mouseout",this.CmdbarMouseOut,false);
}
}
}
function Fpoint_FPSpread(){
this.working=false;
this.editing=false;
this.b7=null;
this.b8=null;
this.b9=null;
this.renderAsEditor=-1;
this.validationMsg=null;
this.c2=null;
this.c3=null;
this.c4=null;
this.c5=-1;
this.c6=-1;
this.activeElement=null;
this.c8=null;
this.c9=new Array();
this.error=false;
this.InitFields=function (f3){
if (this.c2==null)
this.c2=new this.Margin();
f3.d7=null;
f3.groupBar=null;
f3.d8=null;
f3.d9=null;
f3.e0=null;
f3.e1=null;
f3.e2=null;
f3.e3=null;
f3.e4=null;
f3.e5=null;
f3.e6="";
f3.oldScrollLeft=0;
f3.oldScrollTop=0;
f3.e7=null;
f3.f1=false;
f3.slideLeft=0;
f3.slideRight=0;
f3.setAttribute("rowCount",0);
f3.setAttribute("colCount",0);
f3.e8=new Array();
f3.e9=new Array();
f3.f0=new Array();
this.activePager=null;
this.dragSlideBar=false;
f3.allowColMove=(f3.getAttribute("colMove")=="true");
f3.allowGroup=(f3.getAttribute("allowGroup")=="true");
f3.selectedCols=[];
f3.msgList=new Array();
f3.mouseY=null;
f3.copymulticol=false;
f3.bSuspendLayout=null;
}
this.RegisterSpread=function (f3){
var g2=this.GetTopSpread(f3);
if (g2!=f3)return ;
if (this.spreads==null){
this.spreads=new Array();
}
var f4=this.spreads.length;
for (var f5=0;f5<f4;f5++){
if (this.spreads[f5]==f3)return ;
}
this.spreads.length=f4+1;
this.spreads[f4]=f3;
}
this.Init=function (f3,cmd){
if (f3==null)alert("spread is not defined!");
f3.initialized=false;
this.validationMsg=null;
this.c9=new Array();
this.RegisterSpread(f3);
this.InitFields(f3);
this.InitMethods(f3);
f3.d0=document.getElementById(f3.id+"_XMLDATA");
if (f3.d0==null){
f3.d0=document.createElement('XML');
f3.d0.id=f3.id+"_XMLDATA";
f3.d0.style.display="none";
document.body.insertBefore(f3.d0,null);
}
var g3=document.getElementById(f3.id+"_data");
if (g3!=null&&g3.getAttribute("data")!=null){
f3.d0.innerHTML=g3.getAttribute("data");
g3.value="";
}
this.SaveData(f3);
f3.d1=document.getElementById(f3.id+"_viewport");
if (f3.d1!=null){
f3.d2=f3.d1.parentNode;
}
f3.frozColHeader=document.getElementById(f3.id+"_frozColHeader");
f3.frozRowHeader=document.getElementById(f3.id+"_frozRowHeader");
f3.viewport0=document.getElementById(f3.id+"_viewport0");
f3.viewport1=document.getElementById(f3.id+"_viewport1");
f3.viewport2=document.getElementById(f3.id+"_viewport2");
f3.d3=document.getElementById(f3.id+"_corner");
if (f3.d3!=null&&f3.d3.childNodes.length>0){
f3.d3=f3.d3.getElementsByTagName("TABLE")[0];
}
f3.frzRows=f3.frzCols=0;
if (f3.viewport1!=null){
f3.frzRows=f3.viewport1.rows.length;
}
if (f3.viewport0!=null){
var g4=this.GetColGroup(f3.viewport0);
if (g4!=null)f3.frzCols=g4.childNodes.length;
}else if (f3.viewport2!=null){
var g4=this.GetColGroup(f3.viewport2);
if (g4!=null)f3.frzCols=g4.childNodes.length;
}
f3.d4=document.getElementById(f3.id+"_rowHeader");
if (f3.d4!=null)f3.d4=f3.d4.getElementsByTagName("TABLE")[0];
f3.d5=document.getElementById(f3.id+"_colHeader");
if (f3.d5!=null)f3.d5=f3.d5.getElementsByTagName("TABLE")[0];
f3.frozColFooter=document.getElementById(f3.id+"_frozColFooter");
f3.colFooter=document.getElementById(f3.id+"_colFooter");
if (f3.colFooter!=null)f3.colFooter=f3.colFooter.getElementsByTagName("TABLE")[0];
f3.footerCorner=document.getElementById(f3.id+"_footerCorner");
if (f3.footerCorner!=null&&f3.footerCorner.childNodes.length>0){
f3.footerCorner=f3.footerCorner.getElementsByTagName("TABLE")[0];
}
if (f3.frozColFooter!=null)f3.frozColFooter=f3.frozColFooter.getElementsByTagName("TABLE")[0];
var d6=f3.d6=document.getElementById(f3.id+"_commandBar");
if (f3.frozRowHeader!=null)f3.frozRowHeader=f3.frozRowHeader.getElementsByTagName("TABLE")[0];
if (f3.frozColHeader!=null)f3.frozColHeader=f3.frozColHeader.getElementsByTagName("TABLE")[0];
var g5=this.GetViewport(f3);
if (g5!=null){
f3.setAttribute("rowCount",g5.rows.length);
if (g5.rows.length==1)f3.setAttribute("rowCount",0);
f3.setAttribute("colCount",g5.getAttribute("cols"));
}
var e8=f3.e8;
var f0=f3.f0;
var e9=f3.e9;
this.InitSpan(f3,this.GetViewport0(f3),e8);
this.InitSpan(f3,this.GetViewport1(f3),e8);
this.InitSpan(f3,this.GetViewport2(f3),e8);
this.InitSpan(f3,this.GetViewport(f3),e8);
this.InitSpan(f3,this.GetColHeader(f3),f0);
this.InitSpan(f3,this.GetFrozColHeader(f3),f0);
this.InitSpan(f3,this.GetRowHeader(f3),e9);
this.InitSpan(f3,this.GetFrozRowHeader(f3),e9);
if (f3.frzRows!=0||f3.frzCols!=0){
var g6=0;
if (this.GetViewport1(f3)!=null)g6+=this.GetViewport1(f3).rows.length;
if (this.GetViewport(f3)!=null)g6+=this.GetViewport(f3).rows.length;
f3.setAttribute("rowCount",g6);
}
f3.style.overflow="hidden";
if (this.GetParentSpread(f3)==null){
this.LoadScrollbarState(f3);
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var g9=g8.getElementsByTagName("activespread")[0];
if (g9!=null&&g9.innerHTML!=""){
this.SetPageActiveSpread(document.getElementById(this.Trim(g9.innerHTML)));
}
}
this.InitLayout(f3);
f3.f1=true;
if (this.GetPageActiveSpread()==f3&&(f3.getAttribute("AllowInsert")=="false"||f3.getAttribute("IsNewRow")=="true")){
var h0=this.GetCmdBtn(f3,"Insert");
this.UpdateCmdBtnState(h0,true);
h0=this.GetCmdBtn(f3,"Add");
this.UpdateCmdBtnState(h0,true);
}
this.CreateTextbox(f3);
this.CreateFocusBorder(f3);
this.InitSelection(f3);
f3.initialized=true;
if (this.GetPageActiveSpread()==f3)
{
try {
if (document.activeElement==null||document.activeElement==f3||this.IsChild(document.activeElement,f3)){
if (cmd!="LoadOnDemand")this.Focus(f3);
}
}catch (e){}
}
this.SaveData(f3);
if (this.handlers==null)
this.handlers=new FpSpread_EventHandlers();
this.handlers.DetachEvents(f3);
this.handlers.AttachEvents(f3);
if (d6!=null&&f3.style.position==""){
d6.parentNode.style.backgroundColor=d6.style.backgroundColor;
d6.parentNode.style.borderTop=d6.style.borderTop;
}
this.CreateSizebar(f3);
this.SyncColSelection(f3);
}
this.Dispose=function (f3){
if (this.handlers==null)
this.handlers=new FpSpread_EventHandlers();
this.handlers.DetachEvents(f3);
}
this.CmdbarMouseOver=function (event){
var h1=this.GetTarget(event);
if (h1!=null&&h1.tagName=="IMG"&&h1.getAttribute("disabled")!="true"){
h1.style.backgroundColor="cyan";
}
}
this.CmdbarMouseOut=function (event){
var h1=this.GetTarget(event);
if (h1!=null&&h1.tagName=="IMG"){
h1.style.backgroundColor="";
}
}
this.DoPropertyChange=function (event){
if (event.attrName=="curpos"){
this.ScrollViewport(event);
}else if (this.c3==null&&this.c4==null&&event.attrName=="pageincrement"&&event.ctrlKey){
var f3=this.GetSpread(this.GetTarget(event));
if (f3!=null)
this.SizeAll(this.GetTopSpread(f3));
}
}
this.HandleFirstKey=function (){
var f3=this.GetPageActiveSpread();
if (f3==null)return ;
var g2=this.GetTopSpread(f3);
var h2=document.getElementById(g2.id+"_textBox");
if (h2!=null&&h2.value!=""){
var f8=window.navigator.userAgent;
var f9=(f8.indexOf("Firefox/3.")>=0);
if (f9&&this.b7!=null)
this.b7.value=this.b7.value+h2.value;
h2.value="";
}
}
this.IsXHTML=function (f3){
var g2=this.GetTopSpread(f3);
var h3=g2.getAttribute("strictMode");
return (h3!=null&&h3=="true");
}
this.GetData=function (f3){
return f3.d0;
}
this.AttachEvent=function (target,event,handler,useCapture){
if (target.addEventListener!=null){
target.addEventListener(event,handler,useCapture);
}else if (target.attachEvent!=null){
target.attachEvent("on"+event,handler);
}
}
this.DetachEvent=function (target,event,handler,useCapture){
if (target.removeEventListener!=null){
target.removeEventListener(event,handler,useCapture);
}else if (target.detachEvent!=null){
target.detachEvent("on"+event,handler);
}
}
this.CancelDefault=function (e){
if (e.preventDefault!=null){
e.preventDefault();
e.stopPropagation();
}else {
e.cancelBubble=false;
e.returnValue=false;
}
return false;
}
this.CreateEvent=function (name){
var h4=document.createEvent("Events")
h4.initEvent(name,true,true);
return h4;
}
this.Refresh=function (f3){
var h1=f3.style.display;
f3.style.display="none";
f3.style.display=h1;
}
this.InitMethods=function (f3){
var f2=this;
f3.Edit=function (){f2.Edit(this);}
f3.Update=function (){f2.Update(this);}
f3.Cancel=function (){f2.Cancel(this);}
f3.Clear=function (){f2.Clear(this);}
f3.Copy=function (){f2.Copy(this);}
f3.Paste=function (){f2.Paste(this);}
f3.Prev=function (){f2.Prev(this);}
f3.Next=function (){f2.Next(this);}
f3.Add=function (){f2.Add(this);}
f3.Insert=function (){f2.Insert(this);}
f3.Delete=function (){f2.Delete(this);}
f3.Print=function (){f2.Print(this);}
f3.StartEdit=function (cell){f2.StartEdit(this,cell);}
f3.EndEdit=function (){f2.EndEdit(this);}
f3.ClearSelection=function (){f2.ClearSelection(this);}
f3.GetSelectedRange=function (){return f2.GetSelectedRange(this);}
f3.SetSelectedRange=function (r,c,f6,cc,innerRow){f2.SetSelectedRange(this,r,c,f6,cc,innerRow);}
f3.GetSelectedRanges=function (){return f2.GetSelectedRanges(this);}
f3.AddSelection=function (r,c,f6,cc,innerRow){f2.AddSelection(this,r,c,f6,cc,innerRow);}
f3.AddSpan=function (r,c,f6,cc,spans){f2.AddSpan(this,r,c,f6,cc,spans);}
f3.RemoveSpan=function (r,c,spans){f2.RemoveSpan(this,r,c,spans);}
f3.GetActiveRow=function (){var h1=f2.GetRowFromCell(this,this.e0);if (h1<0)return h1;return f2.GetSheetIndex(this,h1);}
f3.GetActiveCol=function (){return f2.GetColFromCell(this,this.e0);}
f3.SetActiveCell=function (r,c){f2.SetActiveCell(this,r,c);}
f3.GetCellByRowCol=function (r,c){return f2.GetCellByRowCol(this,r,c);}
f3.GetValue=function (r,c){return f2.GetValue(this,r,c);}
f3.SetValue=function (r,c,v,noEvent,recalc){f2.SetValue(this,r,c,v,noEvent,recalc);}
f3.GetFormula=function (r,c){return f2.GetFormula(this,r,c);}
f3.SetFormula=function (r,c,f,recalc,clientOnly){f2.SetFormula(this,r,c,f,recalc,clientOnly);}
f3.GetHiddenValue=function (r,colName){return f2.GetHiddenValue(this,r,colName);}
f3.GetSheetRowIndex=function (r){return f2.GetSheetRowIndex(this,r);}
f3.GetSheetColIndex=function (c,innerRow){return f2.GetSheetColIndex(this,c,innerRow);}
f3.GetRowCount=function (){return f2.GetRowCount(this);}
f3.GetColCount=function (){return f2.GetColCount(this);}
f3.GetRowByKey=function (key){return f2.GetRowByKey(this,key,-1);}
f3.GetColByKey=function (key){return f2.GetColByKey(this,key,-1);}
f3.GetRowKeyFromRow=function (r){var h5=f2.GetRowKeyFromRow(this,r);return h5==null?-1:h5;}
f3.GetColKeyFromCol=function (c){var h5=f2.GetColKeyFromCol(this,c);return h5==null?-1:h5;}
f3.GetTotalRowCount=function (){return f2.GetTotalRowCount(this);}
f3.GetPageCount=function (){return f2.GetPageCount(this);}
f3.GetParentSpread=function (){return f2.GetParentSpread(this);}
f3.GetChildSpread=function (r,ri){return f2.GetChildSpread(this,r,ri);}
f3.GetChildSpreads=function (){return f2.GetChildSpreads(this);}
f3.GetParentRowIndex=function (){return f2.GetParentRowIndex(this);}
f3.GetActiveChildSheetView=function (){return f2.GetActiveChildSheetView(this);}
f3.GetSpread=function (h1){return f2.GetSpread(h1);}
f3.UpdatePostbackData=function (){f2.UpdatePostbackData(this);}
f3.SizeToFit=function (c){f2.SizeToFit(this,c);}
f3.SetColWidth=function (c,w){f2.SetColWidth(this,c,w);}
f3.GetPreferredRowHeight=function (r){return f2.GetPreferredRowHeight(this,r);}
f3.SetRowHeight2=function (r,f7){f2.SetRowHeight2(this,r,f7);}
f3.CallBack=function (cmd,asyncCallBack){f2.SyncData(this.getAttribute("name"),cmd,this,asyncCallBack);}
f3.AddKeyMap=function (keyCode,ctrl,shift,alt,action){f2.AddKeyMap(this,keyCode,ctrl,shift,alt,action);}
f3.RemoveKeyMap=function (keyCode,ctrl,shift,alt){f2.RemoveKeyMap(this,keyCode,ctrl,shift,alt);}
f3.MoveToPrevCell=function (){f2.MoveToPrevCell(this);}
f3.MoveToNextCell=function (){f2.MoveToNextCell(this);}
f3.MoveToNextRow=function (){f2.MoveToNextRow(this);}
f3.MoveToPrevRow=function (){f2.MoveToPrevRow(this);}
f3.MoveToFirstColumn=function (){f2.MoveToFirstColumn(this);}
f3.MoveToLastColumn=function (){f2.MoveToLastColumn(this);}
f3.ScrollTo=function (r,c){f2.ScrollTo(this,r,c);}
f3.focus=function (){f2.Focus(this);}
f3.SizeAll=function (){f2.SizeAll(this);}
f3.ShowMessage=function (msg,r,c,time){return f2.ShowMessage(this,msg,r,c,time);}
f3.HideMessage=function (r,c){return f2.HideMessage(this,r,c);}
f3.ProcessKeyMap=function (event){
if (this.keyMap!=null){
var f4=this.keyMap.length;
for (var f5=0;f5<f4;f5++){
var h6=this.keyMap[f5];
if (event.keyCode==h6.key&&event.ctrlKey==h6.ctrl&&event.shiftKey==h6.shift&&event.altKey==h6.alt){
var h7=false;
if (typeof(h6.action)=="function")
h7=h6.action();
else 
h7=eval(h6.action);
return h7;
}
}
}
return true;
}
f3.Cells=function (r,c){return f2.Cells(this,r,c);}
f3.Rows=function (r,c){return f2.Rows(this,r,c);}
f3.Columns=function (r,c){return f2.Columns(this,r,c);}
f3.GetTitleInfo=function (r,c){return f2.GetTitleInfo(this,r,c);}
f3.SizeSpread=function (f3){return f2.SizeSpread(f3);}
f3.SortColumn=function (column){return f2.SortColumn(this,column);}
f3.SuspendLayout=function (){f2.SuspendLayout(this);}
f3.ResumeLayout=function (performLayout){f2.ResumeLayout(this,performLayout);}
}
this.CreateTextbox=function (f3){
var g2=this.GetTopSpread(f3);
var h2=document.getElementById(g2.id+"_textBox");
if (h2==null)
{
h2=document.createElement("INPUT");
h2.type="text";
h2.setAttribute("autocomplete","off");
h2.style.position="absolute";
h2.style.borderWidth=0;
h2.style.top=""+(f3.clientHeight-20)+"px";
h2.style.left="-100px";
if (f3.style.position!="absolute"&&f3.style.position!="relative")
{
var h8=this.GetOffsetTop(f3,f3,document)+f3.clientHeight;
h2.style.top=h8+"px";
}
h2.style.width="0px";
h2.style.height="1px";
if (f3.tabIndex!=null)
h2.tabIndex=f3.tabIndex;
h2.id=f3.id+"_textBox";
f3.insertBefore(h2,f3.firstChild);
}
}
this.CreateSizebar=function (f3){
f3.sizeBar=document.getElementById(f3.id+"_sizeBar");
if (f3.sizeBar==null&&(f3.frzRows>0||f3.frzCols>0))
{
f3.sizeBar=document.createElement("img");
f3.sizeBar.style.position="absolute";
f3.sizeBar.style.borderWidth=1;
f3.sizeBar.style.top="0px";
f3.sizeBar.style.left="-400px";
f3.sizeBar.style.width="2px";
f3.sizeBar.style.height="400px";
f3.sizeBar.style.background="black";
f3.sizeBar.id=f3.id+"_sizeBar";
var h9=this.GetViewport(f3).parentNode;
h9.insertBefore(f3.sizeBar,null);
}
}
this.CreateLineBorder=function (f3,id){
var i0=document.getElementById(id);
if (i0==null)
{
i0=document.createElement('div');
i0.style.position="absolute";
i0.style.left="-1000px";
i0.style.top="0px";
i0.style.overflow="hidden";
i0.style.border="1px solid black";
if (f3.getAttribute("FocusBorderColor")!=null)
i0.style.borderColor=f3.getAttribute("FocusBorderColor");
if (f3.getAttribute("FocusBorderStyle")!=null)
i0.style.borderStyle=f3.getAttribute("FocusBorderStyle");
i0.id=id;
var h9=this.GetViewport(f3).parentNode;
h9.insertBefore(i0,null);
}
return i0;
}
this.CreateFocusBorder=function (f3){
if (f3.frzRows>0||f3.frzCols>0)return ;
if (this.GetTopSpread(f3).getAttribute("hierView")=="true")return ;
if (this.GetTopSpread(f3).getAttribute("showFocusRect")=="false")return ;
if (this.GetViewport(f3)==null)return ;
var i0=this.CreateLineBorder(f3,f3.id+"_focusRectT");
i0.style.height=0;
i0=this.CreateLineBorder(f3,f3.id+"_focusRectB");
i0.style.height=0;
i0=this.CreateLineBorder(f3,f3.id+"_focusRectL");
i0.style.width=0;
i0=this.CreateLineBorder(f3,f3.id+"_focusRectR");
i0.style.width=0;
}
this.GetPosIndicator=function (f3){
var i1=f3.posIndicator;
if (i1==null)
i1=this.CreatePosIndicator(f3);
else if (i1.parentNode!=f3)
f3.insertBefore(i1,null);
return i1;
}
this.CreatePosIndicator=function (f3){
var i1=document.createElement("img");
i1.style.position="absolute";
i1.style.top="0px";
i1.style.left="-400px";
i1.style.width="10px";
i1.style.height="10px";
i1.style.zIndex=1000;
i1.id=f3.id+"_posIndicator";
if (f3.getAttribute("clienturl")!=null)
i1.src=f3.getAttribute("clienturl")+"down.gif";
else 
i1.src=f3.getAttribute("clienturlres");
f3.insertBefore(i1,null);
f3.posIndicator=i1;
return i1;
}
this.InitSpan=function (f3,g0,spans){
if (g0!=null){
var g6=0;
if (g0==this.GetViewport(f3))
g6=g0.rows.length;
var i2=g0.rows;
var i3=this.GetColCount(f3);
for (var i4=0;i4<i2.length;i4++){
if (this.IsChildSpreadRow(f3,g0,i4)){
if (g0==this.GetViewport(f3))g6--;
}else {
var i5=i2[i4].cells;
for (var i6=0;i6<i5.length;i6++){
var i7=i5[i6];
if (i7!=null&&((i7.rowSpan!=null&&i7.rowSpan>1)||(i7.colSpan!=null&&i7.colSpan>1))){
var i8=this.GetRowFromCell(f3,i7);
var i9=parseInt(i7.getAttribute("scol"));
if (i9<i3){
this.AddSpan(f3,i8,i9,i7.rowSpan,i7.colSpan,spans);
}
}
}
}
}
if (g0==this.GetViewport(f3))f3.setAttribute("rowCount",g6);
}
}
this.GetColWithSpan=function (f3,i4,spans,i6){
var j0=0;
var j1=0;
if (i6==0){
while (this.IsCovered(f3,i4,j1,spans))
{
j1++;
}
}
for (var f5=0;f5<spans.length;f5++){
if (spans[f5].rowCount>1&&(spans[f5].col<=i6||i6==0&&spans[f5].col<j1)&&i4>=spans[f5].row&&i4<spans[f5].row+spans[f5].rowCount)
j0+=spans[f5].colCount;
}
return j0;
}
this.AddSpan=function (f3,i4,i6,f6,i3,spans){
if (spans==null)spans=f3.e8;
var j2=new this.Range();
this.SetRange(j2,"Cell",i4,i6,f6,i3);
spans.push(j2);
this.PaintFocusRect(f3);
}
this.RemoveSpan=function (f3,i4,i6,spans){
if (spans==null)spans=f3.e8;
for (var f5=0;f5<spans.length;f5++){
var j2=spans[f5];
if (j2.row==i4&&j2.col==i6){
var j3=spans.length-1;
for (var j4=f5;j4<j3;j4++){
spans[j4]=spans[j4+1];
}
spans.length=spans.length-1;
break ;
}
}
this.PaintFocusRect(f3);
}
this.Focus=function (f3){
if (this.editing)return ;
this.SetPageActiveSpread(f3);
var j5=this.GetOperationMode(f3);
if (f3.e0==null&&j5!="MultiSelect"&&j5!="ExtendedSelect"&&f3.GetRowCount()>0&&f3.GetColCount()>0){
var j6=this.FireActiveCellChangingEvent(f3,0,0,0);
if (!j6){
f3.SetActiveCell(0,0);
var h4=this.CreateEvent("ActiveCellChanged");
h4.cmdID=f3.id;
h4.row=h4.Row=0;
h4.col=h4.Col=0;
if (f3.getAttribute("LayoutMode"))
h4.InnerRow=h4.innerRow=0;
this.FireEvent(f3,h4);
}
}
var g2=this.GetTopSpread(f3);
var h2=document.getElementById(g2.id+"_textBox");
var j7=null,backLeft=null;
if (h2!=null){
j7=h2.style.top;
backLeft=h2.style.left;
if (this.IsStaticPos(f3)){
h2.style.top=""+(Math.max(document.documentElement.scrollTop,document.body.scrollTop)+10)+"px";
h2.style.left=""+(Math.max(document.documentElement.scrollLeft,document.body.scrollLeft)+10)+"px";
}else {
h2.style.top=""+(Math.max(document.documentElement.scrollTop,document.body.scrollTop)-this.GetOffsetTop(f3,f3,document.body)+10)+"px";
h2.style.left=""+(Math.max(document.documentElement.scrollLeft,document.body.scrollLeft)-this.GetOffsetLeft(f3,f3,document.body)+10)+"px";
}
}
if (f3.e0!=null){
var j8=this.GetEditor(f3.e0);
if (j8==null){
if (h2!=null){
if (this.activeElement!=h2){
try {h2.focus();h2.value="";}catch (h4){}
}
}
}else {
if (j8.tagName!="SELECT")j8.focus();
this.SetEditorFocus(j8);
}
}else {
if (h2!=null){
try {h2.focus();h2.value="";}catch (h4){}
}
}
if (h2!=null){
h2.style.top=j7;
h2.style.left=backLeft;
}
this.EnableButtons(f3);
}
this.GetTotalRowCount=function (f3){
var h1=parseInt(f3.getAttribute("totalRowCount"));
if (isNaN(h1))h1=0;
return h1;
}
this.GetPageCount=function (f3){
var h1=parseInt(f3.getAttribute("pageCount"));
if (isNaN(h1))h1=0;
return h1;
}
this.GetColCount=function (f3){
var h1=parseInt(f3.getAttribute("colCount"));
if (isNaN(h1))h1=0;
return f3.frzCols+h1;
}
this.GetRowCount=function (f3){
var h1=parseInt(f3.getAttribute("rowCount"));
if (isNaN(h1))h1=0;
return h1;
}
this.GetRowCountInternal=function (f3){
var h1=parseInt(this.GetViewport(f3).rows.length);
if (isNaN(h1))h1=0;
return f3.frzRows+h1;
}
this.IsChildSpreadRow=function (f3,view,i4){
if (f3==null||view==null)return false;
if (i4>=1&&i4<view.rows.length){
var j9=view.rows[i4].getAttribute("isCSR");
if (j9!=null){
if (j9=="true")
return true;
else 
return false;
}
if (view.rows[i4].cells.length>0&&view.rows[i4].cells[0]!=null&&view.rows[i4].cells[0].firstChild!=null){
var h1=view.rows[i4].cells[0].firstChild;
if (h1.nodeName!="#text"&&h1.getAttribute("FpSpread")=="Spread"){
view.rows[i4].setAttribute("isCSR","true");
return true;
}
}
view.rows[i4].setAttribute("isCSR","false");
}
return false;
}
this.GetChildSpread=function (f3,row,rindex){
var k0=this.GetViewport(f3);
if (k0!=null){
var i4=this.GetDisplayIndex(f3,row)+1;
if (typeof(rindex)=="number")i4+=rindex;
if (i4>=1&&i4<k0.rows.length){
if (k0.rows[i4].cells.length>0&&k0.rows[i4].cells[0]!=null&&k0.rows[i4].cells[0].firstChild!=null){
var h1=k0.rows[i4].cells[0].firstChild;
if (h1.nodeName!="#text"&&h1.getAttribute("FpSpread")=="Spread"){
return h1;
}
}
}
}
return null;
}
this.GetChildSpreads=function (f3){
var f5=0;
var h7=new Array();
var k0=this.GetViewport(f3);
if (k0!=null){
for (var i4=1;i4<k0.rows.length;i4++){
if (k0.rows[i4].cells.length>0&&k0.rows[i4].cells[0]!=null&&k0.rows[i4].cells[0].firstChild!=null){
var h1=k0.rows[i4].cells[0].firstChild;
if (h1.nodeName!="#text"&&h1.getAttribute("FpSpread")=="Spread"){
h7.length=f5+1;
h7[f5]=h1;
f5++;
}
}
}
}
return h7;
}
this.GetDisplayIndex=function (f3,row){
if (row<0)return -1;
var f5=0;
var i4=0;
var k1=this.GetViewport0(f3);
if (k1==null)k1=this.GetViewport1(f3);
if (k1!=null){
if (row<k1.rows.length){
return row;
}
i4=k1.rows.length;
}
var k0=this.GetViewport(f3);
if (k0!=null){
for (f5=0;f5<k0.rows.length;f5++){
if (this.IsChildSpreadRow(f3,k0,f5))continue ;
if (i4==row)break ;
i4++;
}
}
if (k1!=null)f5+=k1.rows.length;
return f5;
}
this.GetSheetIndex=function (f3,row,d1){
var f5=0
var i4=0;
var k0=d1;
if (k0==null)k0=this.GetViewport(f3);
if (k0!=null){
if (row<0||row>=f3.frzRows+k0.rows.length)return -1;
for (f5=0;f5<row;f5++){
if (this.IsChildSpreadRow(f3,k0,f5))continue ;
i4++;
}
}
return i4;
}
this.GetParentRowIndex=function (f3){
var k2=this.GetParentSpread(f3);
if (k2==null)return -1;
var k0=this.GetViewport(k2);
if (k0==null)return -1;
var k3=f3.parentNode.parentNode;
var f5=k3.rowIndex-1;
for (;f5>0;f5--){
if (this.IsChildSpreadRow(k2,k0,f5))continue ;
else 
break ;
}
return this.GetSheetIndex(k2,f5,k0);
}
this.CreateTestBox=function (f3){
var k4=document.getElementById(f3.id+"_testBox");
if (k4==null)
{
k4=document.createElement("span");
k4.style.position="absolute";
k4.style.borderWidth=0;
k4.style.top="-500px";
k4.style.left="-100px";
k4.id=f3.id+"_testBox";
f3.insertBefore(k4,f3.firstChild);
}
return k4;
}
this.SizeToFit=function (f3,i6){
if (i6==null||i6<0)i6=0;
var g0=this.GetViewport(f3);
if (g0!=null){
var k4=this.CreateTestBox(f3);
var i2=g0.rows;
var k5=0;
for (var i4=0;i4<i2.length;i4++){
if (!this.IsChildSpreadRow(f3,g0,i4)){
var k6=this.GetCellFromRowCol(f3,i4,i6);
if (k6.colSpan>1)continue ;
var k7=this.GetPreferredCellWidth(f3,k6,k4);
if (k7>k5)k5=k7;
}
}
this.SetColWidth(f3,i6,k5);
}
}
this.GetPreferredCellWidth=function (f3,k6,k4){
if (k4==null)k4=this.CreateTestBox(f3);
var k8=this.GetRender(f3,k6);
var k9=this.GetCellType(k6);
var l0=this.GetEditor(k6);
if (k8!=null){
k4.style.fontFamily=k8.style.fontFamily;
k4.style.fontSize=k8.style.fontSize;
k4.style.fontWeight=k8.style.fontWeight;
k4.style.fontStyle=k8.style.fontStyle;
}
if (k8!=null&&k9=="MultiColumnComboBoxCellType"){
var l1=k6.getElementsByTagName("Table")[0];
if (l1!=null){
k4.innerHTML=this.GetEditorValue(l0)+"1";
}
}
else {
k4.innerHTML=k6.innerHTML;
}
var k7=k4.offsetWidth+8;
if (k6.style.paddingLeft!=null&&k6.style.paddingLeft.length>0)
k7+=parseInt(k6.style.paddingLeft);
if (k6.style.paddingRight!=null&&k6.style.paddingRight.length>0)
k7+=parseInt(k6.style.paddingRight);
return k7;
}
this.GetHierBar=function (f3){
if (f3.d7==null)f3.d7=document.getElementById(f3.id+"_hierBar");
return f3.d7;
}
this.GetGroupBar=function (f3){
if (f3.groupBar==null)f3.groupBar=document.getElementById(f3.id+"_groupBar");
return f3.groupBar;
}
this.GetPager1=function (f3){
if (f3.d8==null)f3.d8=document.getElementById(f3.id+"_pager1");
return f3.d8;
}
this.GetPager2=function (f3){
if (f3.d9==null)f3.d9=document.getElementById(f3.id+"_pager2");
return f3.d9;
}
this.SynRowHeight=function (f3,d4,g0,i4,updateParent,header){
if (d4==null||g0==null)return ;
var l2=d4.rows[i4].offsetHeight;
var h9=g0.rows[i4].offsetHeight;
if (l2==h9&&i4>0)return ;
var l3=this.IsXHTML(f3);
if (i4==0&&!l3){
l2+=d4.rows[i4].offsetTop;
h9+=g0.rows[i4].offsetTop;
}
if (l3)g0.rows[i4].style.height="";
var f7=Math.max(l2,h9);
if (d4.rows[i4].style.height=="")d4.rows[i4].style.height=""+l2+"px";
if (g0.rows[i4].style.height=="")g0.rows[i4].style.height=""+h9+"px";
if (this.IsChildSpreadRow(f3,g0,i4)){
d4.rows[i4].style.height=f7;
return ;
}
if (f7>0){
if (l3){
if (f7==l2)
g0.rows[i4].style.height=""+(parseInt(g0.rows[i4].style.height)+(f7-h9))+"px";
else 
d4.rows[i4].style.height=""+(parseInt(d4.rows[i4].style.height)+(f7-l2))+"px";
}else {
if (header&&g0.rows.length>=2&&g0.cellSpacing=="0"){
if (i4==0)
if (f7==l2)
g0.rows[i4].style.height=""+(parseInt(g0.rows[i4].style.height)+(f7-h9))+"px";
else 
d4.rows[i4].style.height=""+(parseInt(d4.rows[i4].style.height)+(f7-l2))+"px";
else {
if (f3.frzRows>0&&i4==f3.frzRows-1&&d4==this.GetFrozRowHeader(f3)){
l2+=this.GetRowHeader(f3).rows[0].offsetTop;
h9+=this.GetViewport(f3).rows[0].offsetTop;
d4.rows[i4].style.height=""+(parseInt(d4.rows[i4].style.height)+(Math.max(l2,h9)-l2))+"px";
}else {
d4.rows[i4].style.height=""+f7+"px";
g0.rows[i4].style.height=""+f7+"px";
}
}
}else {
if (f7==l2)
g0.rows[i4].style.height=""+(parseInt(g0.rows[i4].style.height)+(f7-h9))+"px";
else 
d4.rows[i4].style.height=""+(parseInt(d4.rows[i4].style.height)+(f7-l2))+"px";
}
}
}
if (updateParent){
var k2=this.GetParentSpread(f3);
if (k2!=null)this.UpdateRowHeight(k2,f3);
}
}
this.SizeAll=function (f3){
var l4=this.GetChildSpreads(f3);
if (l4!=null&&l4.length>0){
for (var f5=0;f5<l4.length;f5++){
this.SizeAll(l4[f5]);
}
}
this.SizeSpread(f3);
if (this.GetParentSpread(f3)!=null)
this.Refresh(f3);
}
this.EnsureAllRowHeights=function (f3){
if (this.GetFrozColHeader(f3)!=null&&this.GetColHeader(f3)!=null){
for (var f5=0;f5<this.GetFrozColHeader(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetFrozColHeader(f3),this.GetColHeader(f3),f5,false,false);
}
}
if (this.GetFrozColFooter(f3)!=null&&this.GetColFooter(f3)!=null){
for (var f5=0;f5<this.GetFrozColFooter(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetFrozColFooter(f3),this.GetColFooter(f3),f5,false,false);
}
}
if (this.GetViewport0(f3)!=null&&this.GetViewport1(f3)!=null){
for (var f5=0;f5<this.GetViewport1(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetViewport0(f3),this.GetViewport1(f3),f5,false,false);
this.SynRowHeight(f3,this.GetViewport0(f3),this.GetViewport1(f3),f5,false,false);
}
}
if (this.GetViewport(f3)!=null&&this.GetViewport2(f3)!=null){
for (var f5=0;f5<this.GetViewport(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetViewport2(f3),this.GetViewport(f3),f5,false,false);
this.SynRowHeight(f3,this.GetViewport2(f3),this.GetViewport(f3),f5,false,false);
}
}
if (this.GetFrozRowHeader(f3)!=null){
for (var f5=0;f5<this.GetViewport1(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetFrozRowHeader(f3),this.GetViewport1(f3),f5,false,true);
this.SynRowHeight(f3,this.GetFrozRowHeader(f3),this.GetViewport0(f3),f5,false,true);
}
}
if (this.GetRowHeader(f3)!=null){
for (var f5=0;f5<this.GetViewport(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetRowHeader(f3),this.GetViewport(f3),f5,false,true);
this.SynRowHeight(f3,this.GetRowHeader(f3),this.GetViewport2(f3),f5,false,true);
}
}
if (this.GetFrozColHeader(f3)!=null&&this.GetColHeader(f3)!=null){
for (var f5=0;f5<this.GetFrozColHeader(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetFrozColHeader(f3),this.GetColHeader(f3),f5,false,false);
}
}
if (this.GetViewport0(f3)!=null&&this.GetViewport1(f3)!=null){
for (var f5=0;f5<this.GetViewport1(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetViewport0(f3),this.GetViewport1(f3),f5,false,false);
this.SynRowHeight(f3,this.GetViewport0(f3),this.GetViewport1(f3),f5,false,false);
}
}
if (this.GetViewport(f3)!=null&&this.GetViewport2(f3)!=null){
for (var f5=0;f5<this.GetViewport(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetViewport2(f3),this.GetViewport(f3),f5,false,false);
this.SynRowHeight(f3,this.GetViewport2(f3),this.GetViewport(f3),f5,false,false);
}
}
if (this.GetFrozRowHeader(f3)!=null){
for (var f5=0;f5<this.GetViewport1(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetFrozRowHeader(f3),this.GetViewport1(f3),f5,false,true);
this.SynRowHeight(f3,this.GetFrozRowHeader(f3),this.GetViewport0(f3),f5,false,true);
}
}
if (this.GetRowHeader(f3)!=null){
for (var f5=0;f5<this.GetViewport(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetRowHeader(f3),this.GetViewport(f3),f5,false,true);
this.SynRowHeight(f3,this.GetRowHeader(f3),this.GetViewport2(f3),f5,false,true);
}
}
if (this.GetCorner(f3)!=null){
if (this.GetCorner(f3).getAttribute("allowTableCorner")!=null){
if (this.GetCorner(f3)!=null&&this.GetColHeader(f3)!=null){
for (var f5=0;f5<this.GetCorner(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetCorner(f3),this.GetColHeader(f3),f5,false,false);
}
}
if (this.GetFrozColHeader(f3)!=null&&this.GetCorner(f3)!=null){
for (var f5=0;f5<this.GetCorner(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetCorner(f3),this.GetFrozColHeader(f3),f5,false,false);
}
}
if (this.GetCorner(f3)!=null&&this.GetColHeader(f3)!=null){
for (var f5=0;f5<this.GetCorner(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetCorner(f3),this.GetColHeader(f3),f5,false,false);
}
}
if (this.GetFrozColHeader(f3)!=null&&this.GetCorner(f3)!=null){
for (var f5=0;f5<this.GetCorner(f3).rows.length;f5++){
this.SynRowHeight(f3,this.GetCorner(f3),this.GetFrozColHeader(f3),f5,false,false);
}
}
}
}
}
this.SuspendLayout=function (f3){
f3.bSuspendLayout=true;
}
this.ResumeLayout=function (f3,performLayout){
f3.bSuspendLayout=false;
if (performLayout)this.SizeSpread(f3);
}
this.SizeSpread=function (f3,skipRowHeight){
if (f3.bSuspendLayout)return ;
if (f3.offsetHeight==0){
var h1=f3;
while (h1!=null&&h1!=document.body){
h1.displaySetting=h1.style.display;
h1.style.display="";
h1=h1.parentNode;
}
}
if (f3.clientHeight==0||f3.clientWidth==0)return ;
var l3=this.IsXHTML(f3);
var d1=this.GetViewport(f3);
if (d1==null)return ;
var d5=this.GetColHeader(f3);
if (d5!=null&&d5.rows.length>0){
for (var f5=0;f5<d5.rows.length;f5++){
var l5=d5.rows[f5];
l5.style.height=""+l5.offsetHeight+"px";
}
}
if (skipRowHeight==null)this.EnsureAllRowHeights(f3);
var d5=this.GetColHeader(f3);
var l6=this.GetColGroup(d1);
var l7=this.GetColGroup(d5);
if (l6!=null&&l6.childNodes.length>0&&l7!=null&&l7.childNodes.length>0){
var l8=-1;
if (this.c3!=null)l8=parseInt(this.c3.getAttribute("index"));
if ((this.c3==null||l8==0)&&f3.frzCols>=0){
l7.childNodes[0].width=(l6.childNodes[0].offsetLeft+l6.childNodes[0].offsetWidth-d1.cellSpacing);
}
}
var l9=this.GetFrozColHeader(f3);
if (l6!=null&&l6.childNodes.length>0&&l9!=null){
var m0=0;
var m1=this.GetColGroup(this.GetViewport2(f3));
for (var f5=0;f5<m1.childNodes.length;f5++)m0+=m1.childNodes[f5].offsetWidth;
l9.parentNode.parentNode.style.width=""+(m0+l6.childNodes[0].offsetLeft)+"Px";
}
this.SyncMsgs(f3);
if (f3.frzCols>0){
var m2=this.GetFrozColHeader(f3);
if (m2!=null){
var m3=parseInt(m2.parentNode.parentNode.style.width);
if (m2!=null)
m2.parentNode.style.width=""+m3+"px";
var m4=this.GetFrozColFooter(f3);
if (m4!=null)
m4.parentNode.style.width=""+m3+"px";
}
}
if (skipRowHeight==null)this.EnsureAllRowHeights(f3);
var d4=this.GetRowHeader(f3);
var d5=this.GetColHeader(f3);
var m5=this.GetColFooter(f3);
var k2=this.GetParentSpread(f3);
if (k2!=null)this.UpdateRowHeight(k2,f3);
var f7=f3.clientHeight;
var m6=this.IsStaticPos(f3);
var m7=this.GetCommandBar(f3);
if (m7!=null){
m7.style.width=""+f3.clientWidth+"px";
if (m6){
m7.parentNode.style.borderTop="1px solid white";
m7.parentNode.style.backgroundColor=m7.style.backgroundColor;
}
var m8=this.GetElementById(m7,f3.id+"_cmdTable");
if (m8!=null){
if (m6&&(m8.style.height==""||parseInt(m8.style.height)<27)){
m8.style.height=""+(m8.offsetHeight+3)+"px";
}
if (!l3&&parseInt(d1.cellSpacing)>0)
m8.parentNode.style.height=""+(m8.offsetHeight+3)+"px";
f7-=Math.max(m8.parentNode.offsetHeight,m8.offsetHeight);
}
if (m8.offsetHeight>m8.parentNode.offsetHeight)f7+=2;
if (m6)
m7.style.position="";
}
var d5=this.GetColHeader(f3);
if (d5!=null){
d5.parentNode.style.height=""+(d5.offsetHeight-parseInt(d5.cellSpacing))+"px";
f7-=d5.parentNode.offsetHeight;
}
var m5=this.GetColFooter(f3);
if (m5!=null){
f7-=m5.offsetHeight;
m5.parentNode.style.height=""+(m5.offsetHeight)+"px";
}
var d7=this.GetHierBar(f3);
if (d7!=null){
f7-=d7.offsetHeight;
}
var m9=this.GetGroupBar(f3);
if (m9!=null){
f7-=m9.offsetHeight;
}
var d8=this.GetPager1(f3);
if (d8!=null){
f7-=d8.offsetHeight;
this.InitSlideBar(f3,d8);
}
if (!l3&&f3.frzRows>0&&d4){
var l2=d4.rows[0].offsetTop;
var h9=this.GetViewport(f3).rows[0].offsetTop;
f7-=(h9-l2);
}
var n0=(f3.getAttribute("cmdTop")=="true");
var d9=this.GetPager2(f3);
if (d9!=null){
d9.style.width=""+(f3.clientWidth-10)+"px";
f7-=Math.max(d9.offsetHeight,28);
this.InitSlideBar(f3,d9);
}
var n1=null;
if (d4!=null)n1=d4.parentNode;
var n2=null;
if (d5!=null)n2=d5.parentNode;
var n3=null;
if (m5!=null)n3=m5.parentNode;
var n4=this.GetFooterCorner(f3);
if (n3!=null){
n3.style.height=""+m5.offsetHeight-parseInt(d1.cellSpacing)+"px";
if (n4!=null){
n4.parentNode.style.height=n3.style.height;
}
}
if (n4!=null&&!l3)
n4.width=""+(n4.parentNode.offsetWidth+parseInt(d1.cellSpacing))+"px";
var d3=this.GetCorner(f3);
if (n2!=null){
if (!f3.initialized)
n2.style.height=""+d5.offsetHeight-parseInt(d1.cellSpacing)+"px";
if (d3!=null){
d3.parentNode.style.height=n2.style.height;
}
}
if (d3!=null&&!l3)
d3.width=""+(d3.parentNode.offsetWidth+parseInt(d1.cellSpacing))+"px";
var n5=0;
if (this.GetColFooter(f3)){
n5=this.GetColFooter(f3).offsetHeight;
}
if (m7!=null&&!n0){
if (d9!=null){
if (!m6){
m7.style.position="absolute";
m7.style.top=""+(f3.clientHeight-Math.max(d9.offsetHeight,28)-m7.offsetHeight)+"px";
}else {
m7.style.top=""+(d1.parentNode.offsetTop+n5+d1.parentNode.offsetHeight)+"px";
}
}else {
if (!m6){
m7.style.position="absolute";
m7.style.top=""+(f3.clientHeight-m7.offsetHeight)+"px";
}else {
if (d9!=null)
m7.style.top=""+(this.GetOffsetTop(f3,f3,document.body)+f3.clientHeight-Math.max(d9.offsetHeight,28)-m7.offsetHeight)+"px";
else 
m7.style.top=""+(this.GetOffsetTop(f3,f3,document.body)+f3.clientHeight-m7.offsetHeight+1)+"px";
}
}
}
if (d9!=null){
if (!m6){
d9.style.position="absolute";
d9.style.top=""+(f3.clientHeight-Math.max(d9.offsetHeight,28))+"px";
}else {
if (m7!=null&&!n0)
d9.style.top=""+(d1.parentNode.offsetTop+d1.parentNode.offsetHeight+m7.offsetHeight+n5)+"px";
else 
d9.style.top=""+(d1.parentNode.offsetTop+d1.parentNode.offsetHeight+n5)+"px";
}
}
var n6=this.GetViewport0(f3);
var n7=this.GetViewport1(f3);
var n8=this.GetViewport2(f3);
if (n7!=null){
var n9=Math.min(f7,n7.offsetHeight-n7.cellSpacing);
n7.parentNode.style.height=""+Math.max(0,n9)+"px";
if (n6!=null){
n6.parentNode.style.height=n7.parentNode.style.height;
n6.parentNode.style.width=""+(n6.offsetWidth-n6.cellSpacing)+"px"
}
}
if (n8!=null){
n8.parentNode.style.width=""+(n8.offsetWidth-n8.cellSpacing)+"px"
}
var o0=this.GetFrozRowHeader(f3);
if (o0!=null){
o0.parentNode.style.overflow="hidden";
var o1=Math.min(f7,o0.offsetHeight-o0.cellSpacing);
o0.parentNode.style.height=""+Math.max(0,o1)+"px";
}
var o2=f3.clientWidth;
if (d4!=null)o2-=d4.parentNode.offsetWidth;
if (n8!=null)o2-=n8.offsetWidth;
else if (n6!=null)o2-=n6.offsetWidth;
if (n6!=null)f7-=n6.offsetHeight;
else if (n7!=null)f7-=n7.offsetHeight;
if (f3.frzRows>0)f7+=parseInt(d1.cellSpacing);
if (!l3)f7+=parseInt(d1.cellSpacing);
f7-=1;
var o3=document.getElementById(f3.id+"_titleBar");
if (o3)f7-=o3.parentNode.parentNode.offsetHeight;
d1.parentNode.style.height=""+Math.max(f7,1)+"px";
if (f3.frzCols>0)o2+=parseInt(d1.cellSpacing);
d1.parentNode.style.width=""+(o2+parseInt(d1.cellSpacing))+"px";
if (n7!=null){
n7.parentNode.style.width=""+(d1.parentNode.clientWidth)+"px";
}
if (n8!=null)n8.parentNode.style.height=""+(d1.parentNode.clientHeight)+"px";
if (n1!=null){
n1.style.height=""+Math.max(d1.parentNode.offsetHeight,1)+"px";
}
if (this.GetParentSpread(f3)==null&&n2!=null&&n1!=null&&f3.frzCols==0){
var k7=0;
if (n1!=null){
k7=Math.max(f3.clientWidth-n1.offsetWidth,1);
}else {
k7=Math.max(f3.clientWidth,1);
}
n2.style.width=k7;
n2.parentNode.style.width=k7;
}
this.ScrollView(f3);
this.PaintFocusRect(f3);
if (d1&&!d4&&!n6&&!n7&&!n8){
d1.parentNode.parentNode.parentNode.style.height=""+d1.parentNode.offsetHeight+"px";
}
var h1=f3;
while (h1!=null&&h1!=document.body){
if (h1.displaySetting!=null){
h1.style.display=h1.displaySetting;
h1.displaySetting=null;
}else {
break ;
}
h1=h1.parentNode;
}
}
this.IsStaticPos=function (f3){
var m6=(f3.style.position!="absolute"&&f3.style.position!="relative");
if (m6&&document.defaultView!=null&&document.defaultView.getComputedStyle!=null){
var o4=document.defaultView.getComputedStyle(f3,'').getPropertyValue("position");
m6=(o4!="absolute"&&o4!="relative");
}
return m6;
}
this.InitSlideBar=function (f3,pager){
var o5=this.GetElementById(pager,f3.id+"_slideBar");
if (o5!=null){
var l3=this.IsXHTML(f3);
if (l3)
o5.style.height=Math.max(pager.offsetHeight,28)+"px";
else 
o5.style.height=(pager.offsetHeight-2)+"px";
var h1=pager.getElementsByTagName("TABLE");
if (h1!=null&&h1.length>0){
var l5=h1[0].rows[0];
var i9=l5.cells[0];
var o6=l5.cells[2];
f3.slideLeft=Math.max(107,i9.offsetWidth+1);
if (i9.style.paddingRight!="")f3.slideLeft+=parseInt(i9.style.paddingRight);
f3.slideRight=pager.offsetWidth-o6.offsetWidth-o5.offsetWidth-3;
if (o6.style.paddingRight!="")f3.slideRight-=parseInt(o6.style.paddingLeft);
var o7=parseInt(pager.getAttribute("curPage"));
var o8=parseInt(pager.getAttribute("totalPage"))-1;
if (o8==0)o8=1;
var o9=false;
var o2=Math.max(107,f3.slideLeft)+(o7/o8)*(f3.slideRight-f3.slideLeft);
if (pager.id.indexOf("pager1")>=0&&f3.style.position!="absolute"&&f3.style.position!="relative"){
o2+=this.GetOffsetLeft(f3,pager,document);
var h8=(this.GetOffsetTop(f3,i9,pager)+this.GetOffsetTop(f3,pager,document));
o5.style.top=h8+"px";
o9=true;
}
else if (pager.id.indexOf("pager2")>=0&&f3.style.position!="absolute"&&f3.style.position!="relative"){
var h8=(this.GetOffsetTop(f3,i9,pager)+this.GetOffsetTop(f3,pager,document));
o5.style.top=h8+"px";
}
var o3=document.getElementById(f3.id+"_titleBar");
if (pager.id.indexOf("pager1")>=0&&!o9&&o3!=null){
var h8=o3.parentNode.parentNode.offsetHeight;
o5.style.top=h8+"px";
}
o5.style.left=o2+"px";
}
}
}
this.InitLayout=function (f3){
this.SizeSpread(f3);
this.SizeSpread(f3);
}
this.GetRowByKey=function (f3,key,defaultValue){
if (key=="-1")
return -1;
var p0=this.GetViewport1(f3);
if (p0!=null){
var f6=p0.rows.length;
var i2=p0.rows;
for (var k3=0;k3<f6;k3++){
if (i2[k3].getAttribute("FpKey")==key){
return k3;
}
}
}
var p1=this.GetViewport(f3);
if (p1!=null){
var f6=p1.rows.length;
var i2=p1.rows;
for (var k3=0;k3<f6;k3++){
if (i2[k3].getAttribute("FpKey")==key){
if (p0!=null)k3+=p0.rows.length;
return k3;
}
}
}
if (defaultValue)return defaultValue;
if (p1!=null)
return 0;
else 
return -1;
}
this.GetColByKey=function (f3,key,defaultValue){
if (key=="-1")
return -1;
var p2=null;
var p0=this.GetViewport0(f3);
if (p0==null||p0.rows.length==0)p0=this.GetViewport2(f3);
if (p0!=null){
p2=this.GetColGroup(p0);
if (p2!=null){
for (var p3=0;p3<p2.childNodes.length;p3++){
var h1=p2.childNodes[p3];
if (h1.getAttribute("FpCol")==key){
return p3;
}
}
}
}
var p1=this.GetViewport(f3);
var g4=this.GetColGroup(p1);
if (g4==null||g4.childNodes.length==0)
g4=this.GetColGroup(this.GetColHeader(f3));
if (g4!=null){
for (var p3=0;p3<g4.childNodes.length;p3++){
var h1=g4.childNodes[p3];
if (h1.getAttribute("FpCol")==key){
if (p2!=null){
p3+=p2.childNodes.length;
}
return p3;
}
}
}
if (defaultValue)return defaultValue;
return 0;
}
this.IsRowSelected=function (f3,k3){
var p4=this.GetSelection(f3);
if (p4!=null){
var p5=p4.firstChild;
while (p5!=null){
var i4;
var f6;
var p6=this.GetOperationMode(f3);
if (f3.getAttribute("LayoutMode")&&(p6=="ExtendedSelect"||p6=="MultiSelect")){
var p7=parseInt(p5.getAttribute("row"));
i4=this.GetFirstRowFromKey(f3,p7);
}
else 
i4=parseInt(p5.getAttribute("rowIndex"));
if (f3.getAttribute("LayoutMode")&&(p6=="ExtendedSelect"||p6=="MultiSelect"))
f6=parseInt(f3.getAttribute("layoutrowcount"));
else 
f6=parseInt(p5.getAttribute("rowcount"));
if (i4<=k3&&k3<i4+f6)
return true;
p5=p5.nextSibling;
}
}
}
this.InitSelection=function (f3){
var i4=0;
var i6=0;
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var p8=g8.getElementsByTagName("state")[0];
var p4=p8.getElementsByTagName("selection")[0];
var p9=p8.firstChild;
while (p9!=null&&p9.tagName!="activerow"&&p9.tagName!="ACTIVEROW"){
p9=p9.nextSibling;
}
if (p9!=null&&!f3.getAttribute("LayoutMode"))
i4=this.GetRowByKey(f3,p9.innerHTML);
if (i4>=this.GetRowCount(f3))i4=0;
var q0=p8.firstChild;
while (q0!=null&&q0.tagName!="activecolumn"&&q0.tagName!="ACTIVECOLUMN"){
q0=q0.nextSibling;
}
if (q0!=null&&!f3.getAttribute("LayoutMode"))
i6=this.GetColByKey(f3,q0.innerHTML);
if (f3.getAttribute("LayoutMode")&&p9!=null&&q0!=null){
i4=parseInt(p9.innerHTML);
i6=parseInt(q0.innerHTML);
var i7;
if (i4!=-1&&i6!=-1)i7=this.GetCellByRowCol2(f3,p9.innerHTML,q0.innerHTML);
if (i7){
i4=this.GetRowFromCell(f3,i7);
i6=this.GetColFromCell(f3,i7);
}
}
if (i4<0)i4=0;
if (i4>=0||i6>=0){
var q1=g7;
if (this.GetParentSpread(f3)!=null){
var q2=this.GetTopSpread(f3);
if (q2.initialized)q1=this.GetData(q2);
g8=q1.getElementsByTagName("root")[0];
}
var q3=g8.getElementsByTagName("activechild")[0];
f3.e2=i4;f3.e3=i6;
if ((this.GetParentSpread(f3)==null&&(q3==null||q3.innerHTML==""))||(q3!=null&&f3.id==this.Trim(q3.innerHTML))){
this.UpdateAnchorCell(f3,i4,i6);
}else {
f3.e0=this.GetCellFromRowCol(f3,i4,i6);
}
}
var p5=p4.firstChild;
while (p5!=null){
var i4=0;
var i6=0;
if (f3.getAttribute("LayoutMode")&&p5.getAttribute("row")!="-1"&&p5.getAttribute("col")!="-1"){
var i7=this.GetCellByRowCol2(f3,p5.getAttribute("row"),p5.getAttribute("col"));
if (i7){
i4=this.GetRowFromCell(f3,i7);
i6=this.GetColFromCell(f3,i7);
}
}
else if (f3.getAttribute("LayoutMode")&&p5.getAttribute("col")!="-1"&&p5.getAttribute("row")=="-1"&&p5.getAttribute("rowcount")=="-1"){
var k3=this.GetRowTemplateRowFromGroupCell(f3,parseInt(p5.getAttribute("col")));
var i7=this.GetCellByRowCol2(f3,k3,parseInt(p5.getAttribute("col")));
if (i7){
i4=parseInt(i7.parentNode.getAttribute("row"));
i6=this.GetColFromCell(f3,i7);
}
}
else {
i4=this.GetRowByKey(f3,p5.getAttribute("row"));
i6=this.GetColByKey(f3,p5.getAttribute("col"));
}
var f6=parseInt(p5.getAttribute("rowcount"));
var i3=parseInt(p5.getAttribute("colcount"));
p5.setAttribute("rowIndex",i4);
p5.setAttribute("colIndex",i6);
if (f3.getAttribute("LayoutMode")&&p5.getAttribute("col")>=0&&p5.getAttribute("row")>=0&&(p5.getAttribute("rowcount")>=1||p5.getAttribute("colcount")>=1)){
var q4=p5.nextSibling;
if (parseInt(p5.getAttribute("row"))!=parseInt(p9.innerHTML)||parseInt(p5.getAttribute("col"))!=parseInt(q0.innerHTML))p4.removeChild(p5);
p5=q4;
continue ;
}
if (f3.getAttribute("LayoutMode")&&p5.getAttribute("col")=="-1"&&p5.getAttribute("row")!=-1){
f6=parseInt(f3.getAttribute("layoutrowcount"));
}
if (f3.getAttribute("LayoutMode")&&p5.getAttribute("col")!="-1"&&p5.getAttribute("row")=="-1"&&p5.getAttribute("rowcount")=="-1")
this.PaintMultipleRowSelection(f3,i4,i6,1,1,true);
else 
this.PaintSelection(f3,i4,i6,f6,i3,true);
p5=p5.nextSibling;
}
this.PaintFocusRect(f3);
}
this.TranslateKeyPress=function (event){
if (event.ctrlKey&&!event.altKey){
this.TranslateKey(event);
}
var f3=this.GetPageActiveSpread();
if (f3!=null&&!this.editing&&event.keyCode==event.DOM_VK_RETURN)this.CancelDefault(event);
}
this.TranslateKey=function (event){
event=this.GetEvent(event);
var q5=this.GetTarget(event);
try {
if (document.readyState!=null&&document.readyState!="complete")return ;
var f3=this.GetPageActiveSpread();
if (f3==null)return ;
if (typeof(f3.getAttribute("mcctCellType"))!="undefined"&&f3.getAttribute("mcctCellType")=="true")return ;
if (this.GetOperationMode(f3)=="RowMode"&&this.GetEnableRowEditTemplate(f3)=="true"&&this.IsInRowEditTemplate(f3,q5))return ;
if (f3!=null){
if (event.keyCode==229){
this.CancelDefault(event);
return ;
}
if (q5.tagName!="HTML"&&!this.IsChild(q5,this.GetTopSpread(f3)))return ;
this.KeyDown(f3,event);
var q6=false;
if (event.keyCode==event.DOM_VK_TAB){
var q7=this.GetProcessTab(f3);
q6=(q7=="true"||q7=="True");
}
if (q6)
this.CancelDefault(event);
}
}catch (h4){}
}
this.IsInRowEditTemplate=function (f3,q5){
while (q5&&q5.parentNode){
q5=q5.parentNode;
if (q5.tagName=="DIV"&&q5.id==f3.id+"_RowEditTemplateContainer")
return true;
}
return false;
}
this.KeyAction=function (key,ctrl,shift,alt,action){
this.key=key;
this.ctrl=ctrl;
this.shift=shift;
this.alt=alt;
this.action=action;
}
this.RemoveKeyMap=function (f3,keyCode,ctrl,shift,alt,action){
if (f3.keyMap==null)f3.keyMap=new Array();
var f4=f3.keyMap.length;
for (var f5=0;f5<f4;f5++){
var h6=f3.keyMap[f5];
if (h6!=null&&h6.key==keyCode&&h6.ctrl==ctrl&&h6.shift==shift&&h6.alt==alt){
for (var j4=f5+1;j4<f4;j4++){
f3.keyMap[j4-1]=f3.keyMap[j4];
}
f3.keyMap.length=f3.keyMap.length-1;
break ;
}
}
}
this.AddKeyMap=function (f3,keyCode,ctrl,shift,alt,action){
if (f3.keyMap==null)f3.keyMap=new Array();
var h6=this.GetKeyAction(f3,keyCode,ctrl,shift,alt);
if (h6!=null){
h6.action=action;
}else {
var f4=f3.keyMap.length;
f3.keyMap.length=f4+1;
f3.keyMap[f4]=new this.KeyAction(keyCode,ctrl,shift,alt,action);
}
}
this.GetKeyAction=function (f3,keyCode,ctrl,shift,alt){
if (f3.keyMap==null)f3.keyMap=new Array();
var f4=f3.keyMap.length;
for (var f5=0;f5<f4;f5++){
var h6=f3.keyMap[f5];
if (h6!=null&&h6.key==keyCode&&h6.ctrl==ctrl&&h6.shift==shift&&h6.alt==alt){
return h6;
}
}
return null;
}
this.MoveToPrevCell=function (f3){
var q8=this.EndEdit(f3);
if (!q8)return ;
var i4=f3.GetActiveRow();
var i6=f3.GetActiveCol();
this.MoveLeft(f3,i4,i6);
}
this.MoveToNextCell=function (f3){
var q8=this.EndEdit(f3);
if (!q8)return ;
var q9=f3.e0;
var i4=f3.GetActiveRow();
var i6=f3.GetActiveCol();
this.MoveRight(f3,i4,i6);
if (q9!=f3.e0){
var h4=this.CreateEvent("ActiveCellChanged");
h4.cmdID=f3.id;
h4.Row=h4.row=this.GetSheetIndex(f3,this.GetRowFromCell(f3,f3.e0));
h4.Col=h4.col=this.GetColFromCell(f3,f3.e0);
if (f3.getAttribute("LayoutMode"))
h4.InnerRow=h4.innerRow=f3.e0.parentNode.getAttribute("row");
this.FireEvent(f3,h4);
}
var r0=this.GetParent(this.GetViewport(f3));
var i7=this.GetCellFromRowCol(f3,f3.e2,f3.e3);
if (i7.cellIndex==0){
r0.scrollLeft=0;
}
if (i7.offsetLeft+i7.offsetWidth>r0.scrollLeft+r0.offsetWidth-10){
r0.scrollLeft+=i7.offsetWidth;
}
else if (i7.cellIndex>=0){
if (f3.e0.offsetLeft>r0.offsetWidth)
r0.scrollLeft=f3.e0.offsetLeft-r0.offsetWidth;
else 
r0.scrollLeft=0;
}
if (i7.parentNode.rowIndex==1&&f3.virtualTop==0){
r0.scrollTop=0;
}
var r1=this.GetParent(this.GetViewport(f3));
r0=this.GetParent(this.GetViewport(f3));
if (i7!=null&&i7.offsetWidth<r0.clientWidth){
if ((this.IsChild(i7,r0)||this.IsChild(i7,this.GetViewport1(f3)))&&i7.offsetLeft+i7.offsetWidth>r0.scrollLeft+r0.clientWidth){
r1.scrollLeft=i7.offsetLeft+i7.offsetWidth-r0.clientWidth;
}
}
if ((this.IsChild(i7,r0)||this.IsChild(i7,this.GetViewport1(f3)))&&i7.offsetTop+i7.offsetHeight>r0.scrollTop+r0.clientHeight&&i7.offsetHeight<r0.clientHeight-f3.virtualTop){
r1.scrollTop=i7.offsetTop+i7.offsetHeight-r0.clientHeight;
}
if (i7.offsetTop<r0.scrollTop-f3.virtualTop){
r1.scrollTop=i7.offsetTop==0?f3.virtualTop:i7.offsetTop;
}
}
this.MoveToNextRow=function (f3){
var q8=this.EndEdit(f3);
if (!q8)return ;
var i4=f3.GetActiveRow();
var i6=f3.GetActiveCol();
this.MoveDown(f3,i4,i6);
}
this.MoveToPrevRow=function (f3){
var q8=this.EndEdit(f3);
if (!q8)return ;
var i4=f3.GetActiveRow();
var i6=f3.GetActiveCol();
if (i4>0)
this.MoveUp(f3,i4,i6);
}
this.MoveToFirstColumn=function (f3){
var q8=this.EndEdit(f3);
if (!q8)return ;
var i4=f3.GetActiveRow();
if (f3.e0.parentNode.rowIndex>=0)
this.UpdateLeadingCell(f3,i4,0);
}
this.MoveToLastColumn=function (f3){
var q8=this.EndEdit(f3);
if (!q8)return ;
var i4=f3.GetActiveRow();
if (f3.e0.parentNode.rowIndex>=0){
i6=this.GetColCount(f3)-1;
this.UpdateLeadingCell(f3,i4,i6);
}
}
this.UpdatePostbackData=function (f3){
this.SaveData(f3);
}
this.PrepareData=function (p5){
var h7="";
if (p5!=null){
if (p5.nodeName=="#text")
h7=p5.nodeValue;
else {
h7=this.GetBeginData(p5);
var h1=p5.firstChild;
while (h1!=null){
var r2=this.PrepareData(h1);
if (r2!="")h7+=r2;
h1=h1.nextSibling;
}
h7+=this.GetEndData(p5);
}
}
return h7;
}
this.GetBeginData=function (p5){
var h7="<"+p5.nodeName.toLowerCase();
if (p5.attributes!=null){
for (var f5=0;f5<p5.attributes.length;f5++){
var r3=p5.attributes[f5];
if (r3.nodeName!=null&&r3.nodeName!=""&&r3.nodeName!="style"&&r3.nodeValue!=null&&r3.nodeValue!="")
h7+=(" "+r3.nodeName+"=\""+r3.nodeValue+"\"");
}
}
h7+=">";
return h7;
}
this.GetEndData=function (p5){
return "</"+p5.nodeName.toLowerCase()+">";
}
this.SaveData=function (f3){
if (f3==null)return ;
try {
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var h1=this.PrepareData(g8);
var r4=document.getElementById(f3.id+"_data");
r4.value=encodeURIComponent(h1);
}catch (h4){
alert("e "+h4);
}
}
this.SetActiveSpread=function (event){
try {
event=this.GetEvent(event);
var q5=this.GetTarget(event);
var r5=this.GetSpread(q5,false);
var r6=this.GetPageActiveSpread();
if (this.isExtenderPopupControl(q5))return ;
if (this.editing&&(r5==null||(r5!=r6&&r5.getAttribute("mcctCellType")!="true"&&r6.getAttribute("mcctCellType")!="true"))){
if (q5!=this.b7&&this.b7!=null){
if (this.b7.blur!=null)this.b7.blur();
}
var q8=this.EndEdit();
if (!q8)return ;
}
var r7=false;
if (r5==null){
r5=this.GetSpread(q5,true);
r7=(r5!=null);
}
var i7=this.GetCell(q5,true);
if (i7==null&&r6!=null&&r6.f1){
this.SaveData(r6);
r6.f1=false;
}
if (r6!=null&&r6.f1&&(r5!=r6||r5==null||r7)){
this.SaveData(r6);
r6.f1=false;
}
if (r6!=null&&r6.f1&&r5==r6&&q5.tagName=="INPUT"&&(q5.type=="submit"||q5.type=="button"||q5.type=="image")){
this.SaveData(r6);
r6.f1=false;
}
if (r5!=null&&this.GetOperationMode(r5)=="ReadOnly")return ;
var q2=null;
if (r5==null){
if (r6==null)return ;
q2=this.GetTopSpread(r6);
this.SetActiveSpreadID(q2,"",null,false);
this.SetPageActiveSpread(null);
}else {
if (r5!=r6){
if (r6!=null){
q2=this.GetTopSpread(r6);
this.SetActiveSpreadID(q2,"",null,false);
}
if (r7){
q2=this.GetTopSpread(r5);
var r8=this.GetTopSpread(r6);
if (q2!=r8){
this.SetActiveSpreadID(q2,r5.id,r5.id,true);
this.SetPageActiveSpread(r5);
}else {
this.SetActiveSpreadID(q2,r6.id,r6.id,true);
this.SetPageActiveSpread(r6);
}
}else {
q2=this.GetTopSpread(r5);
this.SetPageActiveSpread(r5);
this.SetActiveSpreadID(q2,r5.id,r5.id,false);
}
}
}
}catch (h4){}
}
this.isExtenderPopupControl=function (q5){
return q5.id.indexOf("CalendarExtender")!=-1;
}
this.SetActiveSpreadID=function (f3,id,child,r7){
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var g9=g8.getElementsByTagName("activespread")[0];
var r9=g8.getElementsByTagName("activechild")[0];
if (g9==null)return ;
if (r7&&r9!=null&&r9.nodeValue!=""){
g9.innerHTML=r9.innerHTML;
}else {
g9.innerHTML=id;
if (child!=null&&r9!=null)r9.innerHTML=child;
}
this.SaveData(f3);
f3.f1=false;
}
this.GetSpread=function (ele,incCmdBar){
var k7=ele;
while (k7!=null&&k7.tagName!="BODY"){
if (typeof(k7.getAttribute)!="function")break ;
var f3=k7.getAttribute("FpSpread");
if (f3==null)f3=k7.FpSpread;
if (f3=="Spread"){
if (!incCmdBar){
var h1=ele;
while (h1!=null&&h1!=k7){
if (h1.id==k7.id+"_commandBar"||h1.id==k7.id+"_pager1"||h1.id==k7.id+"_pager2")return null;
h1=h1.parentNode;
}
}
return k7;
}
k7=k7.parentNode;
}
return null;
}
this.GetActiveChildSheetView=function (f3){
var r6=this.GetPageActiveSheetView();
if (typeof(r6)=="undefined")return null;
var q2=this.GetTopSpread(f3);
var s0=this.GetTopSpread(r6);
if (s0!=q2)return null;
if (r6==s0)return null;
return r6;
}
this.ScrollViewport=function (event){
var h1=this.GetTarget(event);
var f3=this.GetTopSpread(h1);
if (f3!=null)this.ScrollView(f3);
}
this.ScrollTo=function (f3,k3,p3){
var i7=this.GetCellByRowCol(f3,k3,p3);
if (i7==null)return ;
var k0=this.GetViewport(f3).parentNode;
if (k0==null)return ;
k0.scrollTop=i7.offsetTop;
k0.scrollLeft=i7.offsetLeft;
}
this.ScrollView=function (f3){
var r5=this.GetTopSpread(f3);
var d4=this.GetParent(this.GetRowHeader(r5));
var d5=this.GetParent(this.GetColHeader(r5));
var m5=this.GetParent(this.GetColFooter(r5));
var k0=this.GetParent(this.GetViewport(r5));
var s1=(f3.oldScrollLeft!=k0.scrollLeft||f3.oldScrollTop!=k0.scrollTop);
f3.oldScrollLeft=k0.scrollLeft;
f3.oldScrollTop=k0.scrollTop;
if (d4!=null){
d4.scrollTop=k0.scrollTop;
}
if (d5!=null){
d5.scrollLeft=k0.scrollLeft;
}
if (m5!=null){
m5.scrollLeft=k0.scrollLeft;
}
var s2=this.GetViewport0(f3);
var s3=this.GetViewport1(f3);
var s4=this.GetViewport2(f3);
if (s4!=null){
s4.parentNode.scrollTop=k0.scrollTop;
}
if (s3!=null){
s3.parentNode.scrollLeft=k0.scrollLeft;
}
if (this.GetParentSpread(f3)==null)this.SaveScrollbarState(f3,k0.scrollTop,k0.scrollLeft);
if (s1){
var h4=this.CreateEvent("Scroll");
this.FireEvent(f3,h4);
if (f3.frzRows!=0||f3.frzCols!=0)this.SyncMsgs(f3);
}
if (k0.scrollTop>0&&k0.scrollTop+k0.offsetHeight>=this.GetViewport(r5).offsetHeight){
if (f3.initialized&&!this.editing&&f3.getAttribute("loadOnDemand")=="true"){
if (f3.LoadState!=null)return ;
f3.LoadState=true;
this.SaveData(f3);
setTimeout(f3.CallBack("LoadOnDemand",true),0);
}
}
}
this.SaveScrollbarState=function (f3,scrollTop,scrollLeft){
if (this.GetParentSpread(f3)!=null)return ;
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var s5=g8.getElementsByTagName("scrollTop")[0];
var s6=g8.getElementsByTagName("scrollLeft")[0];
if (f3.getAttribute("scrollContent"))
if (s5!=null&&s6!=null)
if (s5.innerHTML!=scrollTop||s6.innerHTML!=scrollLeft)
this.ShowScrollingContent(f3,s5.innerHTML==scrollTop);
if (s5!=null)s5.innerHTML=scrollTop;
if (s6!=null)s6.innerHTML=scrollLeft;
}
this.LoadScrollbarState=function (f3){
if (this.GetParentSpread(f3)!=null)return ;
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var s5=g8.getElementsByTagName("scrollTop")[0];
var s6=g8.getElementsByTagName("scrollLeft")[0];
var s7=0;
if (s5!=null&&s5.innerHTML!=""){
s7=parseInt(s5.innerHTML);
}else {
s7=0;
}
var s8=0;
if (s6!=null&&s6.innerHTML!=""){
s8=parseInt(s6.innerHTML);
}else {
s8=0;
}
var k0=this.GetParent(this.GetViewport(f3));
if (k0!=null){
if (!isNaN(s7))k0.scrollTop=s7;
if (!isNaN(s8))k0.scrollLeft=s8;
var d4=this.GetParent(this.GetRowHeader(f3));
var d5=this.GetParent(this.GetColHeader(f3));
var m5=this.GetParent(this.GetColFooter(f3));
if (m5!=null){
m5.scrollLeft=k0.scrollLeft;
}
if (d4!=null){
d4.scrollTop=k0.scrollTop;
}
if (d5!=null){
d5.scrollLeft=k0.scrollLeft;
}
}
}
this.GetParent=function (h4){
if (h4==null)
return null;
else 
return h4.parentNode;
}
this.GetViewport=function (f3){
return f3.d1;
}
this.GetFrozColHeader=function (f3){
return f3.frozColHeader;
}
this.GetColFooter=function (f3){
return f3.colFooter;
}
this.GetFrozColFooter=function (f3){
return f3.frozColFooter;
}
this.GetTopTable=function (f3){
return f3.getElementsByTagName("TABLE")[0];
}
this.GetFrozRowHeader=function (f3){
return f3.frozRowHeader;
}
this.GetViewport0=function (f3){
return f3.viewport0;
}
this.GetViewport1=function (f3){
return f3.viewport1;
}
this.GetViewport2=function (f3){
return f3.viewport2;
}
this.GetCommandBar=function (f3){
return f3.d6;
}
this.GetRowHeader=function (f3){
return f3.d4;
}
this.GetColHeader=function (f3){
return f3.d5;
}
this.GetCmdBtn=function (f3,id){
var r5=this.GetTopSpread(f3);
var s9=this.GetCommandBar(r5);
if (s9!=null)
return this.GetElementById(s9,r5.id+"_"+id);
else 
return null;
}
this.Range=function (){
this.type="Cell";
this.row=-1;
this.col=-1;
this.rowCount=0;
this.colCount=0;
this.innerRow=0;
}
this.SetRange=function (j2,type,k3,p3,f6,i3,innerRow){
j2.type=type;
j2.row=k3;
j2.col=p3;
j2.rowCount=f6;
j2.colCount=i3;
j2.innerRow=innerRow;
if (type=="Row"){
j2.col=j2.colCount=-1;
}else if (type=="Column"){
j2.row=j2.rowCount=-1;
}else if (type=="Table"){
j2.col=j2.colCount=-1;j2.row=j2.rowCount=-1;
}
}
this.Margin=function (left,top,right,bottom){
this.left;
this.top;
this.right;
this.bottom;
}
this.GetRender=function (i7){
var h1=i7;
if (h1.firstChild!=null&&h1.firstChild.tagName!=null&&h1.firstChild.tagName!="BR")
return h1.firstChild;
if (h1.firstChild!=null&&h1.firstChild.value!=null){
h1=h1.firstChild;
}
return h1;
}
this.GetPreferredRowHeight=function (f3,i4){
var k4=this.CreateTestBox(f3);
i4=this.GetDisplayIndex(f3,i4);
var k0=this.GetViewport(f3);
var k5=0;
var t0=k0.rows[i4].offsetHeight;
var f4=k0.rows[i4].cells.length;
for (var f5=0;f5<f4;f5++){
var k6=k0.rows[i4].cells[f5];
var k8=this.GetRender(k6);
if (k8!=null){
k4.style.fontFamily=k8.style.fontFamily;
k4.style.fontSize=k8.style.fontSize;
k4.style.fontWeight=k8.style.fontWeight;
k4.style.fontStyle=k8.style.fontStyle;
}
var p3=this.GetColFromCell(f3,k6);
k4.style.posWidth=this.GetColWidthFromCol(f3,p3);
if (k8!=null&&k8.tagName=="SELECT"){
var h1="";
for (var j4=0;j4<k8.childNodes.length;j4++){
var t1=k8.childNodes[j4];
if (t1.text!=null&&t1.text.length>h1.length)h1=t1.text;
}
k4.innerHTML=h1;
}
else if (k8!=null&&k8.tagName=="INPUT")
k4.innerHTML=k8.value;
else 
{
k4.innerHTML=k6.innerHTML;
}
t0=k4.offsetHeight;
if (t0>k5)k5=t0;
}
return Math.max(0,k5)+3;
}
this.SetRowHeight2=function (f3,i4,height){
if (height<1){
height=1;
}
i4=this.GetDisplayIndex(f3,i4);
var c4=null;
var i0=false;
if (i4<f3.frzRows){
i0=true;
if (this.GetFrozRowHeader(f3)!=null)c4=this.GetFrozRowHeader(f3).rows[i4];
}else {
i4-=f3.frzRows;
if (this.GetRowHeader(f3)!=null)c4=this.GetRowHeader(f3).rows[i4];
}
if (c4!=null)c4.style.height=""+height+"px";
if (i0){
var k0=this.GetViewport0(f3);
if (k0!=null){
if (c4!=null){
k0.rows[c4.rowIndex].style.height=""+(c4.offsetHeight-k0.rows[0].offsetTop)+"px";
}else {
k0.rows[i4].style.height=""+height+"px";
c4=k0.rows[i4];
}
}
k0=this.GetViewport1(f3);
if (k0!=null){
if (c4!=null){
k0.rows[c4.rowIndex].style.height=""+(c4.offsetHeight-k0.rows[0].offsetTop)+"px";
}else {
k0.rows[i4].style.height=""+height+"px";
c4=k0.rows[i4];
}
}
}else {
var k0=this.GetViewport(f3);
if (k0!=null){
if (c4!=null){
k0.rows[c4.rowIndex].style.height=c4.style.height;
}else {
k0.rows[i4].style.height=""+height+"px";
c4=k0.rows[i4];
}
}
k0=this.GetViewport2(f3);
if (k0!=null){
if (c4!=null){
k0.rows[c4.rowIndex].style.height=c4.style.height;
}else {
k0.rows[i4].style.height=""+height+"px";
c4=k0.rows[i4];
}
}
}
var t2=this.AddRowInfo(f3,c4.getAttribute("FpKey"));
if (t2!=null){
if (typeof(c4.style.posHeight)=="undefined")
c4.style.posHeight=height;
this.SetRowHeight(f3,t2,c4.style.posHeight);
}
var k2=this.GetParentSpread(f3);
if (k2!=null)k2.UpdateRowHeight(f3);
this.SizeSpread(f3);
}
this.GetRowHeightInternal=function (f3,i4){
var c4=null;
if (i4<f3.frzRows){
if (this.GetFrozRowHeader(f3)!=null)
c4=this.GetFrozRowHeader(f3).rows[i4];
else if (this.GetViewport1(f3)!=null)
c4=this.GetViewport1(f3).rows[i4];
}else {
i4-=f3.frzRows;
if (this.GetRowHeader(f3)!=null)
c4=this.GetRowHeader(f3).rows[i4];
else if (this.GetViewport(f3)!=null)
c4=this.GetViewport(f3).rows[i4];
}
if (c4!=null)
return c4.offsetHeight;
else 
return 0;
}
this.GetCell=function (ele,noHeader,event){
var h1=ele;
while (h1!=null){
if (noHeader){
if ((h1.tagName=="TD"||h1.tagName=="TH")&&(h1.parentNode.getAttribute("FpSpread")=="r")){
return h1;
}
}else {
if ((h1.tagName=="TD"||h1.tagName=="TH")&&(h1.parentNode.getAttribute("FpSpread")=="r"||h1.parentNode.getAttribute("FpSpread")=="ch"||h1.parentNode.getAttribute("FpSpread")=="rh")){
return h1;
}
}
h1=h1.parentNode;
}
return null;
}
this.InRowHeader=function (f3,i7){
return (this.IsChild(i7,this.GetFrozRowHeader(f3))||this.IsChild(i7,this.GetRowHeader(f3)));
}
this.InColHeader=function (f3,i7){
return (this.IsChild(i7,this.GetFrozColHeader(f3))||this.IsChild(i7,this.GetColHeader(f3)));
}
this.InColFooter=function (f3,i7){
return (this.IsChild(i7,this.GetFrozColFooter(f3))||this.IsChild(i7,this.GetColFooter(f3)));
}
this.IsHeaderCell=function (f3,i7){
return (i7!=null&&(i7.tagName=="TD"||i7.tagName=="TH")&&(i7.parentNode.getAttribute("FpSpread")=="ch"||i7.parentNode.getAttribute("FpSpread")=="rh"));
}
this.InFrozCols=function (f3,i7){
return (this.IsChild(i7,this.GetFrozColHeader(f3))||this.IsChild(i7,this.GetViewport0(f3))||this.IsChild(i7,this.GetViewport2(f3)));
}
this.InFrozRows=function (f3,i7){
(this.IsChild(i7,this.GetFrozRowHeader(f3))||this.IsChild(i7,this.GetViewport0(f3))||this.IsChild(i7,this.GetViewport1(f3)));
}
this.GetSizeColumn=function (f3,ele,event){
if (ele.tagName!="TD"||(this.GetColHeader(f3)==null))return null;
var p3=-1;
var h1=ele;
var s8=this.GetViewport(this.GetTopSpread(f3)).parentNode.scrollLeft+window.scrollX;
while (h1!=null&&h1.parentNode!=null&&h1.parentNode!=document.documentElement){
if (h1.parentNode.getAttribute("FpSpread")=="ch"){
var t3=this.GetOffsetLeft(f3,h1,document.body);
var t4=t3+h1.offsetWidth;
if (event.clientX+s8<t3+3){
p3=this.GetColFromCell(f3,h1)-1;
}
else if (event.clientX+s8>t4-4){
p3=this.GetColFromCell(f3,h1);
var t5=this.GetSpanCell(h1.parentNode.rowIndex,p3,f3.f0);
if (t5!=null){
p3=t5.col+t5.colCount-1;
}
}else {
p3=this.GetColFromCell(f3,h1);
var t5=this.GetSpanCell(h1.parentNode.rowIndex,p3,f3.f0);
if (t5!=null){
var k7=t3;
p3=-1;
for (var f5=t5.col;f5<t5.col+t5.colCount&&f5<this.GetColCount(f3);f5++){
if (this.IsChild(h1,this.GetColHeader(f3)))
k7+=parseInt(this.GetElementById(this.GetColHeader(f3),f3.id+"col"+f5).width);
else 
k7+=parseInt(this.GetElementById(this.GetFrozColHeader(f3),f3.id+"col"+f5).width);
if (event.clientX>k7-3&&event.clientX<k7+3){
p3=f5;
break ;
}
}
}else {
p3=-1;
}
}
if (isNaN(p3)||p3<0)return null;
var t6=0;
var t7=this.GetColCount(f3);
var t8=true;
var g0=null;
var i6=p3+1;
while (i6<t7){
var g4=this.GetColGroup(this.GetColHeader(f3));
if (i6>=f3.frzCols){
var g4=this.GetColGroup(this.GetColHeader(f3));
if (i6-f3.frzCols<g4.childNodes.length)
t6=parseInt(g4.childNodes[i6-f3.frzCols].width);
}else {
var g4=this.GetColGroup(this.GetFrozColHeader(f3));
if (i6<g4.childNodes.length)
t6=parseInt(g4.childNodes[i6].width);
}
if (t6>1){
t8=false;
break ;
}
i6++;
}
if (t8){
i6=p3+1;
while (i6<t7){
if (this.GetSizable(f3,i6)){
p3=i6;
break ;
}
i6++;
}
}
if (!this.GetSizable(f3,p3))return null;
if (this.IsChild(h1,this.GetColHeader(f3))){
if (event.offsetX<3&&h1.cellIndex==0&&this.GetFrozColHeader(f3)!=null){
return this.GetElementById(this.GetFrozColHeader(f3),f3.id+"col"+(f3.frzCols-1));
}else {
return this.GetElementById(this.GetColHeader(f3),f3.id+"col"+p3);
}
}else {
return this.GetElementById(this.GetFrozColHeader(f3),f3.id+"col"+p3);
}
}
h1=h1.parentNode;
}
return null;
}
this.GetColGroup=function (h1){
if (h1==null)return null;
var g4=h1.getElementsByTagName("COLGROUP");
if (g4!=null&&g4.length>0){
if (h1.colgroup!=null)return h1.colgroup;
var r8=new Object();
r8.childNodes=new Array();
for (var f5=0;f5<g4[0].childNodes.length;f5++){
if (g4[0].childNodes[f5]!=null&&g4[0].childNodes[f5].tagName=="COL"){
var f4=r8.childNodes.length;
r8.childNodes.length++;
r8.childNodes[f4]=g4[0].childNodes[f5];
}
}
h1.colgroup=r8;
return r8;
}else {
return null;
}
}
this.GetSizeRow=function (f3,ele,event){
var f6=this.GetRowCount(f3);
if (f6==0)return null;
if (f3.getAttribute("LayoutMode"))return null;
var i7=this.GetCell(ele);
if (i7==null){
if (ele.getAttribute("FpSpread")=="rowpadding"){
if (event.clientY<3){
var f4=ele.parentNode.rowIndex;
if (f4>1){
var k3=ele.parentNode.parentNode.rows[f4-1];
if (this.GetSizable(f3,k3))
return k3;
}
}
}
var d3=this.GetCorner(f3);
if (d3!=null&&this.IsChild(ele,d3)){
if (event.clientY>ele.offsetHeight-4){
var t9=null;
var f4=0;
t9=this.GetRowHeader(f3);
if (t9!=null){
while (f4<t9.rows.length&&t9.rows[f4].offsetHeight<2&&!this.GetSizable(f3,t9.rows[f4]))
f4++;
if (f4<t9.rows.length&&this.GetSizable(f3,t9.rows[f4])&&t9.rows[f4].offsetHeight<2)
return t9.rows[f4];
}
}else {
}
}
return null;
}
var e9=f3.e9;
var e8=f3.e8;
var u0=this.IsChild(i7,this.GetFrozRowHeader(f3));
var h1=i7;
var s7=this.GetViewport(this.GetTopSpread(f3)).parentNode.scrollTop+window.scrollY;
while (h1!=null&&h1!=document.documentElement){
if (h1.getAttribute("FpSpread")=="rh"){
var f4=-1;
var u1=this.GetOffsetTop(f3,h1,document.body);
var u2=u1+h1.offsetHeight;
if (event.clientY+s7<u1+3){
if (h1.rowIndex>1)
f4=h1.rowIndex-1;
else if (h1.rowIndex==0&&!u0&&this.GetFrozRowHeader(f3)!=null){
u0=true;
f4=h1.frzRows-1;
}
}
else if ((u0&&event.clientY>u2-4)||(!u0&&event.clientY+s7>u2-4)){
var t5=this.GetSpanCell(this.GetRowFromCell(f3,i7),this.GetColFromCell(f3,i7),e9);
if (t5!=null){
var f7=u1;
for (var f5=t5.row;f5<t5.row+t5.rowCount;f5++){
f7+=parseInt(this.GetRowHeader(f3).rows[f5].style.height);
if (event.clientY>f7-3&&event.clientY<f7+3){
f4=f5;
break ;
}
}
}else {
if (h1.rowIndex>=0)f4=h1.rowIndex;
}
}
else {
break ;
}
var f7=0;
var f6=this.GetRowHeader(f3).rows.length;
if (u0)f6=this.GetFrozRowHeader(f3).rows.length;
var u3=true;
var t9=null;
if (u0)
t9=this.GetFrozRowHeader(f3);
else 
t9=this.GetRowHeader(f3);
var i4=f4+1;
while (i4<f6){
if (t9.rows[i4].style.height!=null)f7=parseInt(t9.rows[i4].style.height);
else f7=parseInt(t9.rows[i4].offsetHeight);
if (f7>1){
u3=false;
break ;
}
i4++;
}
if (u3){
i4=f4+1;
while (i4<f6){
if (this.GetSizable(f3,this.GetRowHeader(f3).rows[i4])){
f4=i4;
break ;
}
i4++;
}
}
if (f4>=0&&this.GetSizable(f3,t9.rows[f4])){
return t9.rows[f4];
}
else if (event.clientY<3){
while (f4>0&&t9.rows[f4].offsetHeight==0&&!this.GetSizable(f3,t9.rows[f4]))
f4--;
if (f4>=0&&this.GetSizable(f3,t9.rows[f4]))
return t9.rows[f4];
else 
return null;
}
}
h1=h1.parentNode;
}
return null;
}
this.GetElementById=function (k2,id){
if (k2==null)return null;
var h1=k2.firstChild;
while (h1!=null){
if (h1.id==id||(typeof(h1.getAttribute)=="function"&&h1.getAttribute("name")==id))return h1;
var r8=this.GetElementById(h1,id)
if (r8!=null)return r8;
h1=h1.nextSibling;
}
return null;
}
this.GetSizable=function (f3,ele){
if (typeof(ele)=="number"){
var i7=null;
if (ele<f3.frzCols)
i7=this.GetElementById(this.GetFrozColHeader(f3),f3.id+"col"+ele);
else 
i7=this.GetElementById(this.GetColHeader(f3),f3.id+"col"+ele);
return (i7!=null&&(i7.getAttribute("Sizable")==null||i7.getAttribute("Sizable")=="True"));
}
return (ele!=null&&(ele.getAttribute("Sizable")==null||ele.getAttribute("Sizable")=="True"));
}
this.GetSpanWidth=function (f3,p3,t7){
var k7=0;
var g0=this.GetViewport(f3);
if (g0!=null){
var g4=this.GetColGroup(g0);
if (g4!=null){
for (var f5=p3;f5<p3+t7;f5++){
k7+=parseInt(g4.childNodes[f5].width);
}
}
}
return k7;
}
this.GetCellType=function (i7){
if (i7!=null&&i7.getAttribute("FpCellType")!=null)return i7.getAttribute("FpCellType");
if (i7!=null&&i7.getAttribute("FpRef")!=null){
var h1=document.getElementById(i7.getAttribute("FpRef"));
return h1.getAttribute("FpCellType");
}
if (i7!=null&&i7.getAttribute("FpCellType")!=null)return i7.getAttribute("FpCellType");
return "text";
}
this.GetCellType2=function (i7){
if (i7!=null&&i7.getAttribute("FpRef")!=null){
i7=document.getElementById(i7.getAttribute("FpRef"));
}
var k9=null;
if (i7!=null){
k9=i7.getAttribute("FpCellType");
if (k9=="readonly")k9=i7.getAttribute("CellType");
if (k9==null&&i7.getAttribute("CellType2")=="TagCloudCellType")
k9=i7.getAttribute("CellType2");
}
if (k9!=null)return k9;
return "text";
}
this.GetCellEditorID=function (f3,i7){
if (i7!=null&&i7.getAttribute("FpRef")!=null){
var h1=document.getElementById(i7.getAttribute("FpRef"));
return h1.getAttribute("FpEditorID");
}
if (i7.getAttribute("FpEditorID")!=null)
return i7.getAttribute("FpEditorID");
return f3.getAttribute("FpDefaultEditorID");
}
this.EditorMap=function (editorID,b7){
this.id=editorID;
this.b7=b7;
}
this.ValidatorMap=function (validatorID,validator){
this.id=validatorID;
this.validator=validator;
}
this.GetCellEditor=function (f3,editorID,noClone){
var b7=null;
for (var f5=0;f5<this.c9.length;f5++){
var u4=this.c9[f5];
if (u4.id==editorID){
b7=u4.b7;
break ;
}
}
if (b7==null){
b7=document.getElementById(editorID);
this.c9[this.c9.length]=new this.EditorMap(editorID,b7);
}
if (noClone)
return b7;
return b7.cloneNode(true);
}
this.GetCellValidatorID=function (f3,i7){
return null;
}
this.GetCellValidator=function (f3,validatorID){
return null;
}
this.GetTableRow=function (f3,i4){
var g8=this.GetData(f3).getElementsByTagName("root")[0];
var g7=g8.getElementsByTagName("data")[0];
var h1=g7.firstChild;
while (h1!=null){
if (h1.getAttribute("key")==""+i4)return h1;
h1=h1.nextSibling;
}
return null;
}
this.GetTableCell=function (k3,i6){
if (k3==null)return null;
var h1=k3.firstChild;
while (h1!=null){
if (h1.getAttribute("key")==""+i6)return h1;
h1=h1.nextSibling;
}
return null;
}
this.AddTableRow=function (f3,i4){
if (i4==null)return null;
var p5=this.GetTableRow(f3,i4);
if (p5!=null)return p5;
var g8=this.GetData(f3).getElementsByTagName("root")[0];
var g7=g8.getElementsByTagName("data")[0];
if (document.all!=null){
p5=this.GetData(f3).createNode("element","row","");
}else {
p5=document.createElement("row");
p5.style.display="none";
}
p5.setAttribute("key",i4);
g7.appendChild(p5);
return p5;
}
this.AddTableCell=function (k3,i6){
if (k3==null)return null;
var p5=this.GetTableCell(k3,i6);
if (p5!=null)return p5;
if (document.all!=null){
p5=this.GetData(f3).createNode("element","cell","");
}else {
p5=document.createElement("cell");
p5.style.display="none";
}
p5.setAttribute("key",i6);
k3.appendChild(p5);
return p5;
}
this.GetCellValue=function (f3,i7){
if (i7==null)return null;
var i4=this.GetRowKeyFromCell(f3,i7);
var i6=f3.getAttribute("LayoutMode")?this.GetColKeyFromCell2(f3,i7):this.GetColKeyFromCell(f3,i7);
var u5=this.AddTableCell(this.AddTableRow(f3,i4),i6);
return u5.innerHTML;
}
this.HTMLEncode=function (s){
var u6=new String(s);
var u7=new RegExp("&","g");
u6=u6.replace(u7,"&amp;");
u7=new RegExp("<","g");
u6=u6.replace(u7,"&lt;");
u7=new RegExp(">","g");
u6=u6.replace(u7,"&gt;");
u7=new RegExp("\"","g");
u6=u6.replace(u7,"&quot;");
return u6;
}
this.HTMLDecode=function (s){
var u6=new String(s);
var u7=new RegExp("&amp;","g");
u6=u6.replace(u7,"&");
u7=new RegExp("&lt;","g");
u6=u6.replace(u7,"<");
u7=new RegExp("&gt;","g");
u6=u6.replace(u7,">");
u7=new RegExp("&nbsp;","g");
u6=u6.replace(u7," ");
u7=new RegExp("&quot;","g");
u6=u6.replace(u7,'"');
return u6;
}
this.SetCellValue=function (f3,i7,val,noEvent,recalc){
if (i7==null)return ;
var u8=this.GetCellType(i7);
if (u8=="readonly"&&i7.getAttribute("CellType2")!="MutuallyExclusiveCheckBoxCellType")return ;
var i4=this.GetRowKeyFromCell(f3,i7);
var i6=f3.getAttribute("LayoutMode")?this.GetColKeyFromCell2(f3,i7):this.GetColKeyFromCell(f3,i7);
var u5=this.AddTableCell(this.AddTableRow(f3,i4),i6);
val=this.HTMLEncode(val);
val=this.HTMLEncode(val);
u5.innerHTML=val;
if (!noEvent){
var h4=this.CreateEvent("DataChanged");
h4.cell=i7;
h4.cellValue=val;
h4.row=i4;
h4.col=i6;
this.FireEvent(f3,h4);
}
var h0=this.GetCmdBtn(f3,"Update");
if (h0!=null&&h0.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(h0,false);
h0=this.GetCmdBtn(f3,"Cancel");
if (h0!=null&&h0.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(h0,false);
f3.f1=true;
if (recalc){
this.UpdateValues(f3);
}
}
this.GetSelectedRanges=function (f3){
var p4=this.GetSelection(f3);
var h7=new Array();
var p5=p4.firstChild;
while (p5!=null){
var j2=new this.Range();
this.GetRangeFromNode(f3,p5,j2);
var h1=h7.length;
h7.length=h1+1;
h7[h1]=j2;
p5=p5.nextSibling;
}
return h7;
}
this.GetSelectedRange=function (f3){
var j2=new this.Range();
var p4=this.GetSelection(f3);
var p5=p4.lastChild;
if (p5!=null){
this.GetRangeFromNode(f3,p5,j2);
}
return j2;
}
this.GetRangeFromNode=function (f3,p5,j2){
if (p5==null||f3==null||j2==null)return ;
var i4;
var i6;
if (f3.getAttribute("LayoutMode")){
i4=parseInt(p5.getAttribute("rowIndex"));
i6=parseInt(p5.getAttribute("colIndex"));
}
else {
i4=this.GetRowByKey(f3,p5.getAttribute("row"));
i6=this.GetColByKey(f3,p5.getAttribute("col"));
}
var f6=parseInt(p5.getAttribute("rowcount"));
var i3=parseInt(p5.getAttribute("colcount"));
var k0=this.GetViewport(f3);
if (k0!=null){
var u9=this.GetDisplayIndex(f3,i4);
for (var f5=u9;f5<u9+f6;f5++){
if (this.IsChildSpreadRow(f3,k0,f5))f6--;
}
}
var v0;
if (f3.getAttribute("LayoutMode")){
var p3=parseInt(p5.getAttribute("col"));
if (p3!=-1&&parseInt(p5.getAttribute("row"))==-1&&f6==-1){
i4=parseInt(p5.getAttribute("row"));
v0=parseInt(p5.getAttribute("rowIndex"));
}
}
var v1=null;
if (i4<0&&i6<0&&f6!=0&&i3!=0)
v1="Table";
else if (i4<0&&i6>=0&&i3>0)
v1="Column";
else if (i6<0&&i4>=0&&f6>0)
v1="Row";
else 
v1="Cell";
this.SetRange(j2,v1,i4,i6,f6,i3,v0);
}
this.GetSelection=function (f3){
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var p8=g8.getElementsByTagName("state")[0];
var v2=p8.getElementsByTagName("selection")[0];
return v2;
}
this.GetRowKeyFromRow=function (f3,i4){
if (i4<0)return null;
var g0=null;
if (i4<f3.frzRows){
g0=this.GetViewport0(f3);
if (g0==null)g0=this.GetViewport1(f3);
}else {
g0=this.GetViewport2(f3);
if (g0==null)g0=this.GetViewport(f3);
}
if (i4>=f3.frzRows){
i4-=f3.frzRows;
}
if (g0.rows[i4]==null)
return -1;
return g0.rows[i4].getAttribute("FpKey");
}
this.GetColKeyFromCol=function (f3,i6){
if (i6<0)return null;
var g0=null;
if (i6>=f3.frzCols){
g0=this.GetViewport1(f3);
if (g0==null)g0=this.GetViewport(f3);
if (g0==null)g0=this.GetColHeader(f3);
}else {
g0=this.GetViewport0(f3);
if (g0==null)g0=this.GetViewport2(f3);
if (g0==null)g0=this.GetFrozColHeader(f3);
}
if (i6>=f3.frzCols)
i6=i6-f3.frzCols;
var g4=this.GetColGroup(g0);
if (g4!=null&&i6>=0&&i6<g4.childNodes.length){
return g4.childNodes[i6].getAttribute("FpCol");
}
return null;
}
this.GetRowKeyFromCell=function (f3,i7){
var i4=i7.parentNode.getAttribute("FpKey");
return i4;
}
this.GetColKeyFromCell=function (f3,i7){
var p3=this.GetColFromCell(f3,i7);
if (p3>=f3.frzCols){
g0=this.GetViewport(f3);
if (g0==null||!this.IsChild(i7,g0))g0=this.GetViewport1(f3);
var g4=this.GetColGroup(g0);
if (g4!=null&&p3-f3.frzCols>=0&&p3-f3.frzCols<g4.childNodes.length){
return g4.childNodes[p3-f3.frzCols].getAttribute("FpCol");
}
}else {
g0=this.GetViewport0(f3);
if (g0==null||!this.IsChild(i7,g0))g0=this.GetViewport2(f3);
var g4=this.GetColGroup(g0);
if (g4!=null&&p3>=0&&p3<g4.childNodes.length){
return g4.childNodes[p3].getAttribute("FpCol");
}
}
}
this.GetRowTemplateRowFromGroupCell=function (f3,i7,isColHeader){
var v3=this.GetColCount(f3);
var d5=this.GetColHeader(f3);
if ((!f3.allowGroup||isColHeader)&&d5!=null){
for (var f5=0;f5<d5.rows.length;f5++){
for (var j4=0;j4<v3;j4++){
var v4=d5.rows[f5].cells[j4];
var v5=isNaN(i7)?parseInt(i7.getAttribute("col")):i7;
if (v4!=null&&i7!=null&&parseInt(v4.getAttribute("col"))==v5)
return f5;
}
}
}
var p0=this.GetViewport1(f3);
if (p0!=null){
for (var k3=0;k3<p0.rows.length;k3++){
for (var p3=0;p3<p0.rows[k3].cells.length;p3++){
var v6=isNaN(i7)?parseInt(i7.getAttribute("col")):i7;
if (parseInt(p0.rows[k3].cells[p3].getAttribute("col"))==v6&&p0.rows[k3].cells[p3].getAttribute("group")==null)
return parseInt(p0.rows[k3].getAttribute("FpKey"));
}
}
}
var p1=this.GetViewport(f3);
if (p1!=null){
for (var k3=0;k3<p1.rows.length;k3++){
for (var p3=0;p3<p1.rows[k3].cells.length;p3++){
var v6=isNaN(i7)?parseInt(i7.getAttribute("col")):i7;
if (parseInt(p1.rows[k3].cells[p3].getAttribute("col"))==v6&&p1.rows[k3].cells[p3].getAttribute("group")==null)
return parseInt(p1.rows[k3].getAttribute("FpKey"));
}
}
}
return -1;
}
this.GetColTemplateRowFromGroupCell=function (f3,colIndex){
var v3=this.GetColCount(f3);
var d5=this.GetColHeader(f3);
var v7=this.GetRowTemplateRowFromGroupCell(f3,colIndex);
var i7=null
if (d5==null)return -1;
for (var f5=0;f5<d5.rows.length;f5++){
for (var j4=0;j4<v3;j4++){
var v4=d5.rows[f5].cells[j4];
if (v4!=null&&parseInt(v4.getAttribute("col"))==colIndex){
i7=v4;
break ;
}
}
}
return this.GetColFromCell(f3,i7);
}
this.GetColKeyFromCell2=function (f3,i7){
if (!i7)return -1;
if (i7.getAttribute("col"))
return i7.getAttribute("col")=="-1"?0:parseInt(i7.getAttribute("col"));
else 
return this.GetColKeyFromCell(f3,i7);
}
this.GetColKeyFromCol2=function (f3,k3,p3){
var i7=this.GetCellFromRowCol(f3,k3,p3);
if (i7)
return this.GetColKeyFromCell2(f3,i7);
return p3;
}
this.GetCellByRowCol2=function (f3,i4,i6){
if (i4==null||i6==null||i4.length<=0||i4=="-1"||i6.length<=0||i6=="-1")
return null;
var g5=this.GetViewport1(f3);
if (g5!=null){
for (var k3=0;k3<g5.rows.length;k3++){
if (g5.rows[k3].getAttribute("FpKey")==i4){
for (var p3=0;p3<g5.rows[k3].cells.length;p3++){
if (g5.rows[k3].cells[p3].getAttribute("col")==i6)
return g5.rows[k3].cells[p3];
}
}
}
}
var p1=this.GetViewport(f3);
if (p1!=null){
for (var k3=0;k3<p1.rows.length;k3++){
if (p1.rows[k3].getAttribute("FpKey")==i4){
for (var p3=0;p3<p1.rows[k3].cells.length;p3++){
if (p1.rows[k3].cells[p3].getAttribute("col")==i6)
return p1.rows[k3].cells[p3];
}
}
}
}
return null;
}
this.GetRowTemplateRowFromCell=function (f3,i7){
if (!i7)return -1;
try {
var i4
if (i7.getAttribute("group")!=null)
i4=this.GetRowTemplateRowFromGroupCell(f3,i7);
else 
i4=parseInt(i7.parentNode.getAttribute("row"));
return i4;
}
catch (h4){
return -1;
}
}
this.PaintMultipleRowSelection=function (f3,i4,i6,f6,i3,select){
var v8=this.GetRowCountInternal(f3);
var v3=this.GetColCount(f3);
var v9=true;
for (var f5=i4;f5<v8;f5++){
if (this.IsChildSpreadRow(f3,this.GetViewport(f3),f5))continue ;
var i7=null;
for (var j4=i6;j4<i6+i3&&j4<v3;j4++){
if (this.IsCovered(f3,f5,j4,f3.e8))continue ;
i7=this.GetCellFromRowCol(f3,f5,j4,i7);
if (i7!=null&&parseInt(i7.parentNode.getAttribute("row"))==i4){
this.PaintViewportSelection(f3,f5,j4,f6,i3,select);
if (this.GetColHeader(f3)!=null&&this.GetOperationMode(f3)=="Normal"&&v9)this.PaintHeaderSelection(f3,f5,j4,f6,i3,select,true);
if (this.GetRowHeader(f3)!=null)this.PaintHeaderSelection(f3,f5,j4,f6,i3,select,false);
v9=false;
}
}
}
this.PaintAnchorCell(f3);
}
this.GetFirstRowFromKey=function (f3,rowKey){
var g5=this.GetViewport1(f3)
if (g5!=null){
for (var k3=0;k3<g5.rows.length;k3++){
if (g5.rows[k3].getAttribute("FpKey")==rowKey){
return k3;
}
}
}
var p1=this.GetViewport(f3)
if (p1!=null){
for (var k3=0;k3<p1.rows.length;k3++){
if (p1.rows[k3].getAttribute("FpKey")==rowKey){
return (f3.frzRows!=null)?f3.frzRows+k3:k3;
}
}
}
return null;
}
this.GetFirstMultiRowFromViewport=function (f3,k3,isColHeader){
var g0=null;
var w0=null;
if (k3<f3.frzRows)
g0=this.GetViewport1(f3);
else 
g0=this.GetViewport(f3);
if (!isColHeader)
w0=this.GetRowKeyFromRow(f3,k3);
var w1=parseInt(f3.getAttribute("layoutrowcount"));
var w2;
for (var f5=0;f5<g0.rows.length;f5++){
w2=0;
if (w0!=null){
if (g0.rows[f5].getAttribute("FpKey")==w0)
return ((f3.frzRows!=null&&k3<f3.frzRows)?f5:f5+f3.frzRows);
}
else {
for (var j4=f5+1;j4<g0.rows.length;j4++){
if (g0.rows[f5]!=null&&g0.rows[j4]!=null&&g0.rows[f5].getAttribute("FpKey")==g0.rows[j4].getAttribute("FpKey"))
w2++;
if (w2==(w1-1))
return f5;
}
}
}
}
this.GetRowFromViewPort=function (f3,i4,i6){
if (i4<0||i6<0)return null;
var g0=null;
if (i4<f3.frzRows)
g0=this.GetViewport1(f3);
else 
g0=this.GetViewport(f3);
if (i4>=0&&i4<g0.rows.length){
for (var f5=0;f5<g0.rows.length;f5++){
if (g0.rows[f5].getAttribute("row")!=null&&parseInt(g0.rows[f5].getAttribute("row"))==i4)
return f5;
}
}
return 0;
}
this.GetDisplayIndex2=function (f3,v7){
if (!f3.allowGroup)
return (v7!=null)?this.GetDisplayIndex(f3,v7):0;
else {
var g5=this.GetViewport1(f3);
if (g5!=null){
for (var k3=v7;k3<g5.rows.length;k3++){
if (g5.rows(k3).getAttribute("row")==v7){
return k3;
}
}
}
var p1=this.GetViewport(f3);
if (p1!=null){
for (var k3=v7;k3<p1.rows.length;k3++){
if (IsChildSpreadRow(p1,k3))continue ;
if (p1.rows(k3).getAttribute("row")==v7){
return k3;
}
}
}
}
return -1;
}
this.SetSelection=function (f3,k3,p3,rowcount,colcount,addSelection,rowIndex2,colIndex2){
if (!f3.initialized)return ;
var v7=k3;
var w3=(colIndex2==null)?p3:colIndex2;
if (k3!=null&&parseInt(k3)>=0){
k3=this.GetRowKeyFromRow(f3,k3);
if (k3!="newRow")
k3=parseInt(k3);
}
if (p3!=null&&parseInt(p3)>=0){
if (f3.getAttribute("LayoutMode"))
p3=parseInt(this.GetColKeyFromCol2(f3,v7,p3));
else 
p3=parseInt(this.GetColKeyFromCol(f3,p3));
}
if (f3.getAttribute("LayoutMode")&&rowIndex2!=null)
v7=rowIndex2;
var p5=this.GetSelection(f3);
if (p5==null)return ;
if (addSelection==null)
addSelection=(f3.getAttribute("multiRange")=="true"&&!this.working);
var w4=p5.lastChild;
if (w4==null||addSelection){
if (document.all!=null){
w4=this.GetData(f3).createNode("element","range","");
}else {
w4=document.createElement('range');
w4.style.display="none";
}
p5.appendChild(w4);
}
w4.setAttribute("row",k3);
w4.setAttribute("col",p3);
w4.setAttribute("rowcount",rowcount);
w4.setAttribute("colcount",colcount);
w4.setAttribute("rowIndex",v7);
w4.setAttribute("colIndex",w3);
f3.f1=true;
this.PaintFocusRect(f3);
var h0=this.GetCmdBtn(f3,"Update");
this.UpdateCmdBtnState(h0,false);
var h4=this.CreateEvent("SelectionChanged");
this.FireEvent(f3,h4);
}
this.CreateSelectionNode=function (f3,k3,p3,rowcount,colcount,v7,w3){
var w4=document.createElement('range');
w4.style.display="none";
w4.setAttribute("row",k3);
w4.setAttribute("col",p3);
w4.setAttribute("rowcount",rowcount);
w4.setAttribute("colcount",colcount);
w4.setAttribute("rowIndex",v7);
w4.setAttribute("colIndex",w3);
return w4;
}
this.AddRowToSelection=function (f3,p5,k3){
var p6=this.GetOperationMode(f3);
if (f3.getAttribute("LayoutMode")&&(p6=="ExtendedSelect"||p6=="MultiSelect"))return ;
var v7=k3;
if (typeof(k3)!="undefined"&&parseInt(k3)>=0){
k3=this.GetRowKeyFromRow(f3,k3);
if (k3!="newRow")
k3=parseInt(k3);
}
if (!this.IsRowSelected(f3,k3)&&!isNaN(k3))
{
var w4=this.CreateSelectionNode(f3,k3,-1,1,-1,v7,-1);
p5.appendChild(w4);
}
}
this.RemoveSelection=function (f3,k3,p3,rowcount,colcount){
var p5=this.GetSelection(f3);
if (p5==null)return ;
var w4=p5.firstChild;
while (w4!=null){
var i4;
var f6;
var p6=this.GetOperationMode(f3);
if (f3.getAttribute("LayoutMode")&&(p6=="ExtendedSelect"||p6=="MultiSelect")){
var p7=parseInt(w4.getAttribute("row"));
i4=this.GetFirstRowFromKey(f3,p7);
}
else 
i4=parseInt(w4.getAttribute("rowIndex"));
if (f3.getAttribute("LayoutMode")&&(p6=="ExtendedSelect"||p6=="MultiSelect"))
f6=parseInt(f3.getAttribute("layoutrowcount"));
else 
f6=parseInt(w4.getAttribute("rowcount"));
if (i4<=k3&&k3<i4+f6){
p5.removeChild(w4);
for (var f5=i4;f5<i4+f6;f5++){
if (f5!=k3){
this.AddRowToSelection(f3,p5,f5);
}
}
break ;
}
w4=w4.nextSibling;
}
f3.f1=true;
var h0=this.GetCmdBtn(f3,"Update");
this.UpdateCmdBtnState(h0,false);
var h4=this.CreateEvent("SelectionChanged");
this.FireEvent(f3,h4);
}
this.GetColInfo=function (f3,i6){
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var p8=g8.getElementsByTagName("state")[0];
var p3=p8.getElementsByTagName("colinfo")[0];
var h1=p3.firstChild;
while (h1!=null){
if (h1.getAttribute("key")==""+i6)return h1;
h1=h1.nextSibling;
}
return null;
}
this.GetColWidthFromCol=function (f3,i6){
var g4=this.GetColGroup(this.GetViewport(f3));
return parseInt(g4.childNodes[i6].width);
}
this.GetColWidth=function (colInfo){
if (colInfo==null)return null;
var p5=colInfo.getElementsByTagName("width")[0];
if (p5!=null)return p5.innerHTML;
return 0;
}
this.AddColInfo=function (f3,i6){
var p5=this.GetColInfo(f3,i6);
if (p5!=null)return p5;
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var p8=g8.getElementsByTagName("state")[0];
var p3=p8.getElementsByTagName("colinfo")[0];
if (document.all!=null){
p5=this.GetData(f3).createNode("element","col","");
}else {
p5=document.createElement('col');
p5.style.display="none";
}
p5.setAttribute("key",i6);
p3.appendChild(p5);
return p5;
}
this.SetColWidth=function (f3,p3,width,oldWidth){
if (p3==null)return ;
p3=parseInt(p3);
var l3=this.IsXHTML(f3);
var w5=null;
if (p3<f3.frzCols){
if (this.GetViewport0(f3)!=null){
var g4=this.GetColGroup(this.GetViewport0(f3));
if (g4==null||g4.childNodes.length==0){
g4=this.GetColGroup(this.GetFrozColHeader(f3));
}
w5=this.AddColInfo(f3,g4.childNodes[p3].getAttribute("FpCol"));
if (width==0)width=1;
if (g4!=null){
if (oldWidth==null)oldWidth=g4.childNodes[p3].width;
g4.childNodes[p3].width=width;
}
this.SetWidthFix(this.GetViewport0(f3),p3,width);
}
if (this.GetFrozColFooter(f3)!=null){
var g4=this.GetColGroup(this.GetFrozColFooter(f3));
if (g4==null||g4.childNodes.length==0){
g4=this.GetColGroup(this.GetFrozColHeader(f3));
}
if (width==0)width=1;
if (g4!=null){
if (oldWidth==null)oldWidth=g4.childNodes[p3].width;
g4.childNodes[p3].width=width;
}
this.SetWidthFix(this.GetFrozColFooter(f3),p3,width);
}
if (this.GetViewport2(f3)!=null){
var g4=this.GetColGroup(this.GetViewport2(f3));
if (g4==null||g4.childNodes.length==0){
g4=this.GetColGroup(this.GetFrozColHeader(f3));
}
w5=this.AddColInfo(f3,g4.childNodes[p3].getAttribute("FpCol"));
if (width==0)width=1;
if (g4!=null){
if (oldWidth==null)oldWidth=g4.childNodes[p3].width;
g4.childNodes[p3].width=width;
}
this.SetWidthFix(this.GetViewport2(f3),p3,width);
}
if (this.GetFrozColHeader(f3)!=null){
var w6=parseInt(this.GetFrozColHeader(f3).parentNode.parentNode.style.width);
this.GetFrozColHeader(f3).parentNode.parentNode.style.width=(w6+width-oldWidth)+"px";
if (this.GetViewport(f3)!=null){
if (this.GetViewport(f3).cellSpacing=="0"&&this.GetColCount(f3)>1&&this.GetViewport(f3).rules!="rows"){
if (l3){
if (p3==this.colCount-1)width-=1;
}
}
}
if (width<=0)width=1;
document.getElementById(f3.id+"col"+p3).width=width;
this.SetWidthFix(this.GetFrozColHeader(f3),p3,width);
if (this.GetViewport(f3)!=null){
if (this.GetViewport(f3).cellSpacing=="0"&&this.GetColCount(f3)>1&&this.GetViewport(f3).rules!="rows"){
if (p3==this.GetColCount(f3)-1)width+=1;
}
}
}
}else {
if (this.GetViewport1(f3)!=null){
var g4=this.GetColGroup(this.GetViewport1(f3));
if (g4==null||g4.childNodes.length==0){
g4=this.GetColGroup(this.GetColHeader(f3));
}
w5=this.AddColInfo(f3,g4.childNodes[p3-f3.frzCols].getAttribute("FpCol"));
if (this.GetViewport1(f3).cellSpacing=="0"&&this.GetColCount(f3)>1&&this.GetViewport1(f3).rules!="rows"){
if (p3==0)width-=1;
}
if (width==0)width=1;
if (g4!=null)
g4.childNodes[p3-f3.frzCols].width=width;
this.SetWidthFix(this.GetViewport1(f3),p3-f3.frzCols,width);
var d5=this.GetColHeader(f3);
var l6=this.GetColGroup(this.GetViewport(f3));
var l7=this.GetColGroup(d5);
if (l6!=null&&l6.childNodes.length>0&&l7!=null&&l7.childNodes.length>0){
g4=this.GetColGroup(this.GetColHeader(f3));
if (g4!=null){
if (p3==f3.frzCols&&f3.frzCols>0)
width=width+l6.childNodes[0].offsetLeft;
g4.childNodes[p3-f3.frzCols].width=width;
}
}
this.SetWidthFix(this.GetColHeader(f3),p3-f3.frzCols,width);
}
if (this.GetViewport(f3)!=null){
var g4=this.GetColGroup(this.GetViewport(f3));
if (g4==null||g4.childNodes.length==0){
g4=this.GetColGroup(this.GetColHeader(f3));
}
w5=this.AddColInfo(f3,g4.childNodes[p3-f3.frzCols].getAttribute("FpCol"));
if (this.GetViewport(f3).cellSpacing=="0"&&this.GetColCount(f3)>1&&this.GetViewport(f3).rules!="rows"){
if (p3==0)width-=1;
}
if (width==0)width=1;
if (g4!=null)
g4.childNodes[p3-f3.frzCols].width=width;
this.SetWidthFix(this.GetViewport(f3),p3-f3.frzCols,width);
}
if (this.GetColHeader(f3)!=null){
if (this.GetViewport(f3)!=null){
if (this.GetViewport(f3).cellSpacing=="0"&&this.GetColCount(f3)>1&&this.GetViewport(f3).rules!="rows"){
if (p3==f3.frzCols&&f3.frzCols>0)width-=1;
if (p3==this.colCount-1)width-=1;
}
}
if (width<=0)width=1;
document.getElementById(f3.id+"col"+p3).width=width;
this.SetWidthFix(this.GetColHeader(f3),p3-f3.frzCols,width);
if (this.GetViewport(f3)!=null){
if (this.GetViewport(f3).cellSpacing=="0"&&this.GetColCount(f3)>1&&this.GetViewport(f3).rules!="rows"){
if (p3==this.GetColCount(f3)-1)width+=1;
}
}
}
if (this.GetColFooter(f3)!=null){
var g4=this.GetColGroup(this.GetColFooter(f3));
if (g4==null||g4.childNodes.length==0){
g4=this.GetColGroup(this.GetColHeader(f3));
}
w5=this.AddColInfo(f3,g4.childNodes[p3-f3.frzCols].getAttribute("FpCol"));
if (this.GetColFooter(f3).cellSpacing=="0"&&this.GetColCount(f3)>1&&this.GetColFooter(f3).rules!="rows"){
if (p3==0)width-=1;
}
if (width==0)width=1;
if (g4!=null)
g4.childNodes[p3-f3.frzCols].width=width;
this.SetWidthFix(this.GetColFooter(f3),p3-f3.frzCols,width);
}
}
var g2=this.GetTopSpread(f3);
this.SizeAll(g2);
this.Refresh(g2);
if (p3<f3.frzCols&&this.GetFrozColHeader(f3)!=null){
var w6=parseInt(this.GetFrozColHeader(f3).parentNode.parentNode.style.width);
this.GetFrozColHeader(f3).parentNode.parentNode.style.width=(w6+width-oldWidth)+"px";
var w7=this.GetColGroup(this.GetTopTable(f3));
if (w7!=null){
var w8=this.GetFrozColHeader(f3).parentNode.parentNode.cellIndex;
var w6=parseInt(w7.childNodes[1].width);
var l2=this.GetRowHeader(f3);
if (l2==null&&isNaN(w6)){
w6=parseInt(w7.childNodes[0].width);
}
w7.childNodes[w8].width=(w6+width-oldWidth)+"px";
}
}
if (w5!=null){
var p5=w5.getElementsByTagName("width");
if (p5!=null&&p5.length>0)
p5[0].innerHTML=width;
else {
if (document.all!=null){
p5=this.GetData(f3).createNode("element","width","");
}else {
p5=document.createElement('width');
p5.style.display="none";
}
w5.appendChild(p5);
p5.innerHTML=width;
}
}
var h0=this.GetCmdBtn(f3,"Update");
if (h0!=null)this.UpdateCmdBtnState(h0,false);
f3.f1=true;
}
this.SetWidthFix=function (g0,p3,width){
if (g0==null||g0.rows.length==0)return ;
var f5=0;
var w9=0;
var k6=g0.rows[0].cells[0];
var x0=k6.colSpan;
if (x0==null)x0=1;
while (p3>=w9+x0){
f5++;
w9=w9+x0;
k6=g0.rows[0].cells[f5];
x0=k6.colSpan;
if (x0==null)x0=1;
}
k6.width=width;
}
this.GetRowInfo=function (f3,i4){
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var p8=g8.getElementsByTagName("state")[0];
var k3=p8.getElementsByTagName("rowinfo")[0];
var h1=k3.firstChild;
while (h1!=null){
if (h1.getAttribute("key")==""+i4)return h1;
h1=h1.nextSibling;
}
return null;
}
this.GetRowHeight=function (t2){
if (t2==null)return null;
var x1=t2.getElementsByTagName("height");
if (x1!=null&&x1.length>0)return x1[0].innerHTML;
return 0;
}
this.AddRowInfo=function (f3,i4){
var p5=this.GetRowInfo(f3,i4);
if (p5!=null)return p5;
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var p8=g8.getElementsByTagName("state")[0];
var k3=p8.getElementsByTagName("rowinfo")[0];
if (document.all!=null){
p5=this.GetData(f3).createNode("element","row","");
}else {
p5=document.createElement('row');
p5.style.display="none";
}
p5.setAttribute("key",i4);
k3.appendChild(p5);
return p5;
}
this.GetTopSpread=function (h4)
{
if (h4==null)return null;
var h7=this.GetSpread(h4);
if (h7==null)return null;
var h1=h7.parentNode;
while (h1!=null&&h1.tagName!="BODY")
{
if (h1.getAttribute("FpSpread")=="Spread"){
if (h1.getAttribute("hierView")=="true")
h7=h1;
else 
break ;
}
h1=h1.parentNode;
}
return h7;
}
this.GetParentSpread=function (f3)
{
var x2=f3.getAttribute("parentSpread");
if (x2!=null){
if (x2.length<=0)
return null;
else 
return document.getElementById(x2);
}
else {
try {
var h1=f3.parentNode;
while (h1!=null&&h1!=document&&h1.getAttribute("FpSpread")!="Spread")h1=h1.parentNode;
if (h1!=null&&h1!=document&&h1.getAttribute("hierView")=="true"){
f3.setAttribute("parentSpread",h1.id);
return h1;
}
else {
f3.setAttribute("parentSpread","");
return null;
}
}catch (h4){
f3.setAttribute("parentSpread","");
return null;
}
}
}
this.SetRowHeight=function (f3,t2,height){
if (t2==null)return ;
var p5=t2.getElementsByTagName("height");
if (p5!=null&&p5.length>0)
p5[0].innerHTML=height;
else {
if (document.all!=null){
p5=this.GetData(f3).createNode("element","height","");
}else {
p5=document.createElement('height');
p5.style.display="none";
}
t2.appendChild(p5);
p5.innerHTML=height;
}
var h0=this.GetCmdBtn(f3,"Update");
if (h0!=null)this.UpdateCmdBtnState(h0,false);
f3.f1=true;
}
this.SetActiveRow=function (f3,k3){
if (this.GetRowCount(f3)<1)return ;
if (k3==null)k3=-1;
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var p8=g8.getElementsByTagName("state")[0];
var p9=p8.firstChild;
while (p9!=null&&p9.tagName!="activerow"&&p9.tagName!="ACTIVEROW"){
p9=p9.nextSibling;
}
if (p9!=null)
p9.innerHTML=""+k3;
if (k3!=null&&f3.getAttribute("IsNewRow")!="true"&&f3.getAttribute("AllowInsert")=="true"){
var h0=this.GetCmdBtn(f3,"Insert");
this.UpdateCmdBtnState(h0,false);
h0=this.GetCmdBtn(f3,"Add");
this.UpdateCmdBtnState(h0,false);
}else {
var h0=this.GetCmdBtn(f3,"Insert");
this.UpdateCmdBtnState(h0,true);
h0=this.GetCmdBtn(f3,"Add");
this.UpdateCmdBtnState(h0,true);
}
if (k3!=null&&f3.getAttribute("IsNewRow")!="true"&&(f3.getAttribute("AllowDelete")==null||f3.getAttribute("AllowDelete")=="true")){
var h0=this.GetCmdBtn(f3,"Delete");
this.UpdateCmdBtnState(h0,(k3==-1));
}else {
var h0=this.GetCmdBtn(f3,"Delete");
this.UpdateCmdBtnState(h0,true);
}
f3.f1=true;
}
this.SetActiveCol=function (f3,p3){
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var p8=g8.getElementsByTagName("state")[0];
var q0=p8.firstChild;
while (q0!=null&&q0.tagName!="activecolumn"&&q0.tagName!="ACTIVECOLUMN"){
q0=q0.nextSibling;
}
if (q0!=null)
q0.innerHTML=""+parseInt(p3);
f3.f1=true;
}
this.GetEditor=function (i7){
if (i7==null)return null;
var u8=this.GetCellType(i7);
if (u8=="readonly")return null;
var j8=i7.getElementsByTagName("DIV");
if (u8=="MultiColumnComboBoxCellType"){
if (j8!=null&&j8.length>0){
var h1=j8[0];
h1.type="div";
return h1;
}
}
j8=i7.getElementsByTagName("INPUT");
if (j8!=null&&j8.length>0){
var h1=j8[0];
while (h1!=null&&h1.getAttribute&&h1.getAttribute("FpEditor")==null)
h1=h1.parentNode;
if (h1!=null&&!h1.getAttribute)h1=null;
return h1;
}
j8=i7.getElementsByTagName("SELECT");
if (j8!=null&&j8.length>0){
var h1=j8[0];
return h1;
}
return null;
}
this.GetPageActiveSpread=function (){
var x3=document.documentElement.getAttribute("FpActiveSpread");
var h1=null;
if (x3!=null)h1=document.getElementById(x3);
return h1;
}
this.GetPageActiveSheetView=function (){
var x3=document.documentElement.getAttribute("FpActiveSheetView");
var h1=null;
if (x3!=null)h1=document.getElementById(x3);
return h1;
}
this.SetPageActiveSpread=function (f3){
if (f3==null)
document.documentElement.setAttribute("FpActiveSpread",null);
else {
document.documentElement.setAttribute("FpActiveSpread",f3.id);
document.documentElement.setAttribute("FpActiveSheetView",f3.id);
}
}
this.DoResize=function (event){
if (the_fpSpread.spreads==null)return ;
var f4=the_fpSpread.spreads.length;
for (var f5=0;f5<f4;f5++){
if (the_fpSpread.spreads[f5]!=null)the_fpSpread.SizeSpread(the_fpSpread.spreads[f5]);
}
}
this.DocScroll=function (event){
if (the_fpSpread.spreads==null||!the_fpSpread.editing)return ;
var f3=the_fpSpread.GetPageActiveSpread();
if (f3!=null)f3.EndEdit();
}
this.DblClick=function (event){
var i7=this.GetCell(this.GetTarget(event),true,event);
var f3=this.GetSpread(i7);
if (i7!=null&&!this.IsHeaderCell(i7)&&this.GetOperationMode(f3)=="RowMode"&&this.GetEnableRowEditTemplate(f3)=="true"&&!f3.getAttribute("LayoutMode")){
var x4=i7.getElementsByTagName("DIV");
if (x4!=null&&x4.length>0&&x4[0].id==f3.id+"_RowEditTemplateContainer")return ;
this.Edit(f3,this.GetRowKeyFromCell(f3,i7));
var h0=this.GetCmdBtn(f3,"Cancel");
if (h0!=null)
this.UpdateCmdBtnState(h0,false);
return ;
}
if (i7!=null&&!this.IsHeaderCell(i7)&&i7==f3.e0)this.StartEdit(f3,i7);
}
this.GetEvent=function (h4){
if (h4!=null)return h4;
return window.event;
}
this.GetTarget=function (h4){
h4=this.GetEvent(h4);
if (h4.target==document){
if (h4.currentTarget!=null)return h4.currentTarget;
}
if (h4.target!=null)return h4.target;
return h4.srcElement;
}
this.StartEdit=function (f3,editCell){
var x5=this.GetOperationMode(f3);
if (x5=="SingleSelect"||x5=="ReadOnly"||this.editing)return ;
if (x5=="RowMode"&&this.GetEnableRowEditTemplate(f3)=="true"&&!f3.getAttribute("LayoutMode"))return ;
var i7=editCell;
if (i7==null)i7=f3.e0;
if (i7==null)return ;
if (this.IsHeaderCell(f3,i7))return ;
this.renderAsEditor=-1;
var j8=this.GetEditor(i7);
if (j8!=null){
this.editing=true;
this.b7=j8;
this.renderAsEditor=1;
}
var l3=this.IsXHTML(f3);
if (i7!=null){
var i4=this.GetRowFromCell(f3,i7);
var i6=this.GetColFromCell(f3,i7);
var h4=this.CreateEvent("EditStart");
h4.cell=i7;
h4.row=this.GetSheetIndex(f3,i4);
h4.col=i6;
h4.cancel=false;
this.FireEvent(f3,h4);
if (h4.cancel)return ;
var u8=this.GetCellType(i7);
if (u8=="readonly")return ;
if (f3.e0!=i7){
f3.e0=i7;
this.SetActiveRow(f3,this.GetRowKeyFromCell(f3,i7));
this.SetActiveCol(f3,f3.getAttribute("LayoutMode")?this.GetColKeyFromCell2(i7):this.GetColKeyFromCell(f3,i7));
}
if (j8==null){
var k8=this.GetRender(i7);
var x6=this.GetValueFromRender(f3,k8);
if (x6==" ")x6="";
this.b8=x6;
this.b9=this.GetFormulaFromCell(i7);
if (this.b9!=null)x6=this.b9;
try {
if (k8!=i7){
k8.style.display="none";
}
else {
k8.innerHTML="";
}
}catch (h4){
return ;
}
var x7=this.GetCellEditorID(f3,i7);
if (x7!=null&&x7.length>0){
this.b7=this.GetCellEditor(f3,x7,true);
if (!this.b7.getAttribute("MccbId")&&!this.b7.getAttribute("Extenders"))
this.b7.style.display="inline";
else 
this.b7.style.display="block";
}else {
this.b7=document.createElement("INPUT");
this.b7.type="text";
}
this.b7.style.fontFamily=k8.style.fontFamily;
this.b7.style.fontSize=k8.style.fontSize;
this.b7.style.fontWeight=k8.style.fontWeight;
this.b7.style.fontStyle=k8.style.fontStyle;
this.b7.style.textDecoration=k8.style.textDecoration;
this.b7.style.position="";
if (l3){
var m0=i7.clientWidth-2;
var x8=parseInt(i7.style.paddingLeft);
if (!isNaN(x8))
m0-=x8;
x8=parseInt(i7.style.paddingRight);
if (!isNaN(x8))
m0-=x8;
this.b7.style.width=""+m0+"px";
}
else 
this.b7.style.width=i7.clientWidth-2;
this.SaveMargin(i7);
if (this.b7.tagName=="TEXTAREA")
this.b7.style.height=""+(i7.offsetHeight-4)+"px";
if ((this.b7.tagName=="INPUT"&&this.b7.type=="text")||this.b7.tagName=="TEXTAREA"){
if (this.b7.style.backgroundColor==""||this.b7.backColorSet!=null){
var x9="";
if (document.defaultView!=null&&document.defaultView.getComputedStyle!=null)x9=document.defaultView.getComputedStyle(i7,'').getPropertyValue("background-color");
if (x9!="")
this.b7.style.backgroundColor=x9;
else 
this.b7.style.backgroundColor=i7.bgColor;
this.b7.backColorSet=true;
}
if (this.b7.style.color==""||this.b7.colorSet!=null){
var y0="";
if (document.defaultView!=null&&document.defaultView.getComputedStyle!=null)y0=document.defaultView.getComputedStyle(i7,'').getPropertyValue("color");
this.b7.style.color=y0;
this.b7.colorSet=true;
}
this.b7.style.borderWidth="0px";
this.RestoreMargin(this.b7,false);
}
this.renderAsEditor=0;
i7.insertBefore(this.b7,i7.firstChild);
this.SetEditorValue(this.b7,x6,f3);
if (this.b7.offsetHeight<i7.clientHeight&&this.b7.tagName!="TEXTAREA"){
if (i7.vAlign=="middle")
this.b7.style.posTop+=(i7.clientHeight-this.b7.offsetHeight)/2;
else if (i7.vAlign=="bottom")
this.b7.style.posTop+=(i7.clientHeight-this.b7.offsetHeight);
}
this.SizeAll(this.GetTopSpread(f3));
}
this.SetEditorFocus(this.b7);
if (f3.getAttribute("EditMode")=="replace"){
if ((this.b7.tagName=="INPUT"&&this.b7.type=="text")||this.b7.tagName=="TEXTAREA")
this.b7.select();
}
this.editing=true;
var h0=this.GetCmdBtn(f3,"Update");
if (h0!=null&&h0.disabled)
this.UpdateCmdBtnState(h0,false);
h0=this.GetCmdBtn(f3,"Copy");
if (h0!=null&&!h0.disabled)
this.UpdateCmdBtnState(h0,true);
h0=this.GetCmdBtn(f3,"Paste");
if (h0!=null&&!h0.disabled)
this.UpdateCmdBtnState(h0,true);
h0=this.GetCmdBtn(f3,"Clear");
if (h0!=null&&!h0.disabled)
this.UpdateCmdBtnState(h0,true);
}
this.ScrollView(f3);
}
this.GetCurrency=function (validator){
var y1=validator.CurrencySymbol;
if (y1!=null)return y1;
var h1=document.getElementById(validator.id+"cs");
if (h1!=null){
return h1.innerHTML;
}
return "";
}
this.GetValueFromRender=function (f3,rd){
var k9=this.GetCellType2(this.GetCell(rd));
if (k9!=null){
if (k9=="text")k9="TextCellType";
var j6=null;
if (k9=="ExtenderCellType"){
j6=this.GetFunction(k9+"_getEditor")
if (j6!=null){
if (j6(rd)!=null)
j6=this.GetFunction(k9+"_getEditorValue");
else 
j6=null;
}
}else 
j6=this.GetFunction(k9+"_getValue");
if (j6!=null){
return j6(rd,f3);
}
}
var h1=rd;
while (h1.firstChild!=null&&h1.firstChild.nodeName!="#text")h1=h1.firstChild;
if (h1.innerHTML=="&nbsp;")return "";
var x6=h1.value;
if ((typeof(x6)=="undefined")&&k9=="readonly"&&h1.parentNode!=null&&h1.parentNode.getAttribute("CellType2")=="TagCloudCellType")
x6=h1.textContent;
if (x6==null){
x6=this.ReplaceAll(h1.innerHTML,"&nbsp;"," ");
x6=this.ReplaceAll(x6,"<br>"," ");
x6=this.HTMLDecode(x6);
}
return x6;
}
this.ReplaceAll=function (val,v8,dest){
if (val==null)return val;
var y2=val.length;
while (true){
val=val.replace(v8,dest);
if (val.length==y2)break ;
y2=val.length;
}
return val;
}
this.GetFormula=function (f3,i4,i6){
i4=this.GetDisplayIndex(f3,i4);
var i7=this.GetCellFromRowCol(f3,i4,i6);
var y3=this.GetFormulaFromCell(i7);
return y3;
}
this.SetFormula=function (f3,i4,i6,j6,recalc,clientOnly){
i4=this.GetDisplayIndex(f3,i4);
var i7=this.GetCellFromRowCol(f3,i4,i6);
i7.setAttribute("FpFormula",j6);
if (!clientOnly)
this.SetCellValue(f3,i7,j6,null,recalc);
}
this.GetFormulaFromCell=function (rd){
var x6=null;
if (rd.getAttribute("FpFormula")!=null){
x6=rd.getAttribute("FpFormula");
}
if (x6!=null)
x6=this.Trim(new String(x6));
return x6;
}
this.IsDouble=function (val,decimalchar,negsign,possign,minimumvalue,maximumvalue){
if (val==null||val.length==0)return true;
val=the_fpSpread.Trim(val);
if (val.length==0)return true;
if (negsign!=null)val=val.replace(negsign,"-");
if (possign!=null)val=val.replace(possign,"+");
if (val.charAt(val.length-1)=="-")val="-"+val.substring(0,val.length-1);
var y4=new RegExp("^\\s*([-\\+])?(\\d+)?(\\"+decimalchar+"(\\d+))?([eE]([-\\+])?(\\d+))?\\s*$");
var y5=val.match(y4);
if (y5==null)
return false;
if ((y5[2]==null||y5[2].length==0)&&(y5[4]==null||y5[4].length==0))return false;
var y6="";
if (y5[1]!=null&&y5[1].length>0)y6=y5[1];
if (y5[2]!=null&&y5[2].length>0)
y6+=y5[2];
else 
y6+="0";
if (y5[4]!=null&&y5[4].length>0)
y6+=("."+y5[4]);
if (y5[6]!=null&&y5[6].length>0){
y6+=("E"+y5[6]);
if (y5[7]!=null)
y6+=(y5[7]);
else 
y6+="0";
}
var y7=parseFloat(y6);
if (isNaN(y7))return false;
var h1=true;
if (minimumvalue!=null){
var y8=parseFloat(minimumvalue);
h1=(!isNaN(y8)&&y7>=y8);
}
if (h1&&maximumvalue!=null){
var k5=parseFloat(maximumvalue);
h1=(!isNaN(k5)&&y7<=k5);
}
return h1;
}
this.GetFunction=function (fn){
if (fn==null||fn=="")return null;
try {
var h1=eval(fn);
return h1;
}catch (h4){
return null;
}
}
this.SetValueToRender=function (rd,val,valueonly){
var j6=null;
var k9=this.GetCellType2(this.GetCell(rd));
if (k9!=null){
if (k9=="text")k9="TextCellType";
if (k9=="ExtenderCellType"){
j6=this.GetFunction(k9+"_getEditor")
if (j6!=null){
if (j6(rd)!=null)
j6=this.GetFunction(k9+"_setEditorValue");
else 
j6=null;
}
}else 
j6=this.GetFunction(k9+"_setValue");
}
if (j6!=null){
j6(rd,val);
}else {
if (typeof(rd.value)!="undefined"){
if (val==null)val="";
rd.value=val;
}else {
var h1=rd;
while (h1.firstChild!=null&&h1.firstChild.nodeName!="#text")h1=h1.firstChild;
h1.innerHTML=this.ReplaceAll(val," ","&nbsp;");
}
}
if (rd.tagName=="NOBR")rd=this.GetCell(rd);
if ((valueonly==null||!valueonly)&&rd.getAttribute("FpFormula")!=null){
rd.setAttribute("FpFormula",val);
}
}
this.Trim=function (u9){
var y5=u9.match(new RegExp("^\\s*(\\S+(\\s+\\S+)*)\\s*$"));
return (y5==null)?"":y5[1];
}
this.GetOffsetLeft=function (f3,i7,k2){
var g0=k2;
if (g0==null)g0=this.GetViewportFromCell(f3,i7);
var t3=0;
var h1=i7;
while (h1!=null&&h1!=g0&&this.IsChild(h1,g0)){
t3+=h1.offsetLeft;
h1=h1.offsetParent;
}
return t3;
}
this.GetOffsetTop=function (f3,i7,k2){
var g0=k2;
if (g0==null)g0=this.GetViewportFromCell(f3,i7);
var y9=0;
var h1=i7;
while (h1!=null&&h1!=g0&&this.IsChild(h1,g0)){
y9+=h1.offsetTop;
h1=h1.offsetParent;
}
return y9;
}
this.SetEditorFocus=function (h1){
if (h1==null)return ;
var z0=true;
var i7=this.GetCell(h1,true);
var k9=this.GetCellType(i7);
if (k9!=null){
var j6=this.GetFunction(k9+"_setFocus");
if (j6!=null){
j6(h1);
z0=false;
}
}
if (z0){
try {
h1.focus();
}catch (h4){}
}
}
this.SetEditorValue=function (h1,val,f3){
var i7=this.GetCell(h1,true);
var k9=this.GetCellType(i7);
if (k9!=null){
var j6=this.GetFunction(k9+"_setEditorValue");
if (j6!=null){
j6(h1,val,f3);
return ;
}
}
k9=h1.getAttribute("FpEditor");
if (k9!=null){
var j6=this.GetFunction(k9+"_setEditorValue");
if (j6!=null){
j6(h1,val,f3);
return ;
}
}
h1.value=val;
}
this.GetEditorValue=function (h1){
var i7=this.GetCell(h1,true);
var k9=this.GetCellType(i7);
if (k9!=null){
var j6=this.GetFunction(k9+"_getEditorValue");
if (j6!=null){
return j6(h1);
}
}
k9=h1.getAttribute("FpEditor");
if (k9!=null){
var j6=this.GetFunction(k9+"_getEditorValue");
if (j6!=null){
return j6(h1);
}
}
if (h1.type=="checkbox"){
if (h1.checked)
return "True";
else 
return "False";
}
else {
return h1.value;
}
}
this.CreateMsg=function (){
if (this.validationMsg!=null)return ;
var h1=this.validationMsg=document.createElement("div");
h1.style.position="absolute";
h1.style.background="yellow";
h1.style.color="red";
h1.style.border="1px solid black";
h1.style.display="none";
}
this.SetMsg=function (msg){
this.CreateMsg();
this.validationMsg.innerHTML=msg;
this.validationMsg.width=this.validationMsg.offsetWidth+6;
}
this.ShowMsg=function (show){
this.CreateMsg();
if (show){
this.validationMsg.style.display="block";
}
else 
this.validationMsg.style.display="none";
}
this.EndEdit=function (){
if (this.b7!=null&&this.b7.parentNode!=null){
if (typeof(ExtenderCellType_validateValue)!="undefined")
ExtenderCellType_validateValue(this.b7);
var i7=this.GetCell(this.b7.parentNode);
var f3=this.GetSpread(i7,false);
if (f3==null)return true;
var z1=this.GetEditorValue(this.b7);
var z2=z1;
if (typeof(z1)=="string")z2=this.Trim(z1);
var z3=(f3.getAttribute("AcceptFormula")=="true"&&z2!=null&&z2.charAt(0)=='=');
var j8=(this.renderAsEditor!=0);
if (!z3&&!j8){
var z4=null;
var k9=this.GetCellType(i7);
if (k9!=null){
var j6=this.GetFunction(k9+"_isValid");
if (j6!=null){
z4=j6(i7,z1);
}
}
if (z4!=null&&z4!=""){
this.SetMsg(z4);
this.GetViewport(f3).parentNode.insertBefore(this.validationMsg,this.GetViewport(f3).parentNode.firstChild);
this.ShowMsg(true);
this.SetValidatorPos(f3);
this.b7.focus();
return false;
}else {
this.ShowMsg(false);
}
}
if (!j8){
i7.removeChild(this.b7);
this.b7.style.display="none";
this.GetViewport(f3).parentNode.appendChild(this.b7);
this.SetEditorValue(this.b7,"",f3);
var z5=this.GetRender(i7);
if (z5.style.display=="none")z5.style.display="block";
if (this.b9!=null&&this.b9==z1){
this.SetValueToRender(z5,this.b8,true);
}else {
this.SetValueToRender(z5,z1);
}
this.RestoreMargin(i7);
}
if ((this.b9!=null&&this.b9!=z1)||(this.b9==null&&this.b8!=z1)){
this.SetCellValue(f3,i7,z1);
if (z1!=null&&z1.length>0&&z1.indexOf("=")==0)i7.setAttribute("FpFormula",z1);
}
if (!j8)
this.SizeAll(this.GetTopSpread(f3));
this.b7=null;
this.editing=false;
var h4=this.CreateEvent("EditStopped");
h4.cell=i7;
this.FireEvent(f3,h4);
this.Focus(f3);
var z6=f3.getAttribute("autoCalc");
if (z6!="false"){
if ((this.b9!=null&&this.b9!=z1)||(this.b9==null&&this.b8!=z1)){
this.UpdateValues(f3);
}
}
}
this.renderAsEditor=-1;
return true;
}
this.SetValidatorPos=function (f3){
if (this.b7==null)return ;
var i7=this.GetCell(this.b7.parentNode);
if (i7==null)return ;
var h1=this.validationMsg;
if (h1!=null&&h1.style.display!="none"){
var p0=this.GetViewport0(f3);
var g5=this.GetViewport1(f3);
var z7=this.GetViewport2(f3);
var g0=this.GetViewport(f3);
var r8=0;
if (this.GetColHeader(f3)!=null)r8=this.GetColHeader(f3).offsetHeight;
if ((p0!=null||g5!=null)&&(this.IsChild(i7,z7)||this.IsChild(i7,g0))){
if (p0!=null)
r8+=p0.offsetHeight;
else 
r8+=g5.offsetHeight;
}
var z8=0;
if (this.GetRowHeader(f3)!=null)z8=this.GetRowHeader(f3).offsetWidth;
if ((p0!=null||z7!=null)&&(this.IsChild(i7,g5)||this.IsChild(i7,g0)))
{
if (p0!=null)
z8+=p0.offsetWidth;
else 
z8+=z7.offsetWidth;
}
if (f3.frzRows==0&&f3.frzCols==0){
r8=0;
z8=0;
}else {
if (g0!=null&&this.IsChild(i7,g0)){
r8-=g0.parentNode.scrollTop;
z8-=g0.parentNode.scrollLeft;
}
}
h1.style.left=""+(z8+i7.offsetLeft)+"px";
h1.style.top=""+(r8+i7.offsetTop+i7.offsetHeight)+"px";
if (i7.offsetTop+i7.offsetHeight+h1.offsetHeight+16>h1.parentNode.offsetHeight)
h1.style.top=""+(r8+i7.offsetTop-h1.offsetHeight-1)+"px";
}
}
this.SaveMargin=function (editCell){
if (editCell.style.paddingLeft!=null&&editCell.style.paddingLeft!=""){
this.c2.left=editCell.style.paddingLeft;
editCell.style.paddingLeft=0;
}
if (editCell.style.paddingRight!=null&&editCell.style.paddingRight!=""){
this.c2.right=editCell.style.paddingRight;
editCell.style.paddingRight=0;
}
if (editCell.style.paddingTop!=null&&editCell.style.paddingTop!=""){
this.c2.top=editCell.style.paddingTop;
editCell.style.paddingTop=0;
}
if (editCell.style.paddingBottom!=null&&editCell.style.paddingBottom!=""){
this.c2.bottom=editCell.style.paddingBottom;
editCell.style.paddingBottom=0;
}
}
this.RestoreMargin=function (i7,reset){
if (this.c2.left!=null&&this.c2.left!=-1){
i7.style.paddingLeft=this.c2.left;
if (reset==null||reset)this.c2.left=-1;
}
if (this.c2.right!=null&&this.c2.right!=-1){
i7.style.paddingRight=this.c2.right;
if (reset==null||reset)this.c2.right=-1;
}
if (this.c2.top!=null&&this.c2.top!=-1){
i7.style.paddingTop=this.c2.top;
if (reset==null||reset)this.c2.top=-1;
}
if (this.c2.bottom!=null&&this.c2.bottom!=-1){
i7.style.paddingBottom=this.c2.bottom;
if (reset==null||reset)this.c2.bottom=-1;
}
}
this.PaintSelectedCell=function (f3,i7,select,anchor){
if (i7==null)return ;
var z9=anchor?f3.getAttribute("anchorBackColor"):f3.getAttribute("selectedBackColor");
if (select){
if (i7.getAttribute("bgColorBak")==null)
i7.setAttribute("bgColorBak",document.defaultView.getComputedStyle(i7,"").getPropertyValue("background-color"));
if (i7.bgColor1==null)
i7.bgColor1=i7.style.backgroundColor;
i7.style.backgroundColor=z9;
if (i7.getAttribute("bgSelImg"))
i7.style.backgroundImage=i7.getAttribute("bgSelImg");
}else {
if (i7.bgColor1!=null)
i7.style.backgroundColor="";
if (i7.bgColor1!=null&&i7.bgColor1!="")
i7.style.backgroundColor=i7.bgColor1;
i7.style.backgroundImage="";
if (i7.getAttribute("bgImg")!=null)
i7.style.backgroundImage=i7.getAttribute("bgImg");
}
}
this.PaintAnchorCell=function (f3){
var p6=this.GetOperationMode(f3);
if (f3.e0==null||(p6!="Normal"&&p6!="RowMode"))return ;
if (p6=="MultiSelect"||p6=="ExtendedSelect")return ;
if (!this.IsChild(f3.e0,f3))return ;
var aa0=(f3.frzRows==0&&f3.frzCols==0)&&(this.GetTopSpread(f3).getAttribute("hierView")!="true");
if (f3.getAttribute("showFocusRect")=="false")aa0=false;
if (aa0){
this.PaintSelectedCell(f3,f3.e0,false);
this.PaintFocusRect(f3);
this.PaintAnchorCellHeader(f3,true);
return ;
}
var h1=f3.e0.parentNode.cells[0].firstChild;
if (h1!=null&&h1.nodeName!="#text"&&h1.getAttribute("FpSpread")=="Spread")return ;
this.PaintSelectedCell(f3,f3.e0,true,true);
this.PaintAnchorCellHeader(f3,true);
}
this.ClearSelection=function (f3,thisonly){
var aa1=this.GetParentSpread(f3);
if (thisonly==null&&aa1!=null&&aa1.getAttribute("hierView")=="true"){
this.ClearSelection(aa1);
return ;
}
var k0=this.GetViewport(f3);
var g6=this.GetRowCount(f3);
if (k0!=null&&k0.rows.length>g6){
for (var f5=0;f5<k0.rows.length;f5++){
if (k0.rows[f5].cells.length>0&&k0.rows[f5].cells[0]!=null&&k0.rows[f5].cells[0].firstChild!=null&&k0.rows[f5].cells[0].firstChild.nodeName!="#text"){
var h1=k0.rows[f5].cells[0].firstChild;
if (h1.getAttribute("FpSpread")=="Spread"){
this.ClearSelection(h1,true);
}
}
}
}
this.DoclearSelection(f3);
if (f3.e0!=null){
var x5=this.GetOperationMode(f3);
if (x5=="RowMode"||x5=="SingleSelect"||x5=="ExtendedSelect"||x5=="MultiSelect"){
var i8=this.GetRowFromCell(f3,f3.e0);
this.PaintSelection(f3,i8,-1,1,-1,false);
}
this.PaintSelectedCell(f3,f3.e0,false);
this.PaintAnchorCellHeader(f3,false);
}else {
var i7=this.GetCellFromRowCol(f3,1,0);
if (i7!=null)this.PaintSelectedCell(f3,i7,false);
}
this.PaintFocusRect(f3);
f3.selectedCols=[];
f3.f1=true;
}
this.SetSelectedRange=function (f3,i4,i6,f6,i3,v0){
this.ClearSelection(f3);
var i4=this.GetDisplayIndex(f3,i4);
var w2=0;
var aa2=f6;
var k0=this.GetViewport(f3);
if (k0!=null){
for (f5=i4;f5<k0.rows.length&&w2<aa2;f5++){
if (this.IsChildSpreadRow(f3,k0,f5)){;
f6++;
}else {
w2++;
}
}
}
var aa3=null;
var a4=null;
if (f3.getAttribute("LayoutMode")){
if (i6>=0&&f6<0){
if (i3!=1)return ;
var k3=this.GetDisplayIndex2(f3,v0);
var i7=this.GetCellByRowCol(f3,k3,i6);
if (i7!=null&&parseInt(i7.getAttribute("col"))!=-1){
i6=parseInt(i7.getAttribute("col"));
aa3=parseInt(i7.parentNode.getAttribute("row"));
a4=this.GetColFromCell(f3,i7);
}
else 
return ;
this.PaintMultipleRowSelection(f3,aa3,a4,1,i3,true);
}
else if (i4>=0&&i3<0){
if (f6>parseInt(f3.getAttribute("layoutrowcount")))return ;
var aa4=parseInt(this.GetRowKeyFromRow(f3,i4));
var i4=parseInt(this.GetFirstRowFromKey(f3,aa4));
this.UpdateAnchorCell(f3,i4,0,true);
f6=parseInt(f3.getAttribute("layoutrowcount"));
this.PaintSelection(i4,i6,f6,i3,true);
}
else if (i4>=0&&i6>=0&&(i3>1||f6>1))
return ;
}
else 
this.PaintSelection(f3,i4,i6,f6,i3,true);
this.SetSelection(f3,i4,i6,f6,i3,null,aa3,a4);
}
this.AddSelection=function (f3,i4,i6,f6,i3,v0){
var i4=this.GetDisplayIndex(f3,i4);
var w2=0;
var aa2=f6;
var k0=this.GetViewport(f3);
if (k0!=null){
for (f5=i4;f5<k0.rows.length&&w2<aa2;f5++){
if (this.IsChildSpreadRow(f3,k0,f5)){;
f6++;
}else {
w2++;
}
}
}
var aa3;
var a4;
if (f3.getAttribute("LayoutMode")){
if (i6>=0&&f6<0){
if (i3!=1)return ;
var k3=this.GetDisplayIndex2(f3,v0);
var i7=this.GetCellByRowCol(f3,k3,i6);
if (i7!=null&&parseInt(i7.getAttribute("col"))!=-1){
aa3=parseInt(i7.parentNode.getAttribute("row"));
a4=this.GetColFromCell(f3,i7);
i6=parseInt(i7.getAttribute("col"));
}
else 
return ;
this.PaintMultipleRowSelection(f3,aa3,a4,1,i3,true);
}
else if (i4>=0&&i3<0){
if (f6>parseInt(f3.getAttribute("layoutrowcount")))return ;
var aa4=parseInt(this.GetRowKeyFromRow(f3,i4));
var i4=parseInt(this.GetFirstRowFromKey(f3,aa4));
if (f3.allowGroup){
this.ClearSelection(f3);
this.UpdateAnchorCell(f3,i4,0,true);
}
f6=parseInt(f3.getAttribute("layoutrowcount"));
this.PaintSelection(f3,i4,i6,f6,i3,true);
}
else if (i4>=0&&i6>=0&&(i3>1||f6>1))
return ;
}
else 
this.PaintSelection(f3,i4,i6,f6,i3,true);
this.SetSelection(f3,i4,i6,f6,i3,true,aa3,a4);
}
this.SelectRow=function (f3,index,w2,select,ignoreAnchor){
f3.e4=index;
f3.e5=-1;
if (!ignoreAnchor)this.UpdateAnchorCell(f3,index,0,false);
f3.e6="r";
var aa5=w2;
if (f3.getAttribute("LayoutMode")){
aa5=parseInt(f3.getAttribute("layoutrowcount"));
}
this.PaintSelection(f3,index,-1,aa5,-1,select);
if (select)
{
this.SetSelection(f3,index,-1,w2,-1);
}else {
this.RemoveSelection(f3,index,-1,w2,-1);
this.PaintFocusRect(f3);
}
}
this.SelectColumn=function (f3,index,w2,select,ignoreAnchor){
f3.e4=-1;
f3.e5=index;
if (!ignoreAnchor){
var i4=0;
var aa6=index;
if (f3.getAttribute("LayoutMode")){
var i7=f3.e0;
if (parseInt(f3.e0.getAttribute("col"))==-1)return ;
if (i7){
i4=this.GetRowFromCell(f3,i7);
aa6=this.GetColFromCell(f3,i7);
}
f3.copymulticol=true;
}
this.UpdateAnchorCell(f3,i4,aa6,false,true);
}
f3.e6="c";
if (!f3.getAttribute("LayoutMode"))
this.PaintSelection(f3,-1,aa6,-1,w2,select);
else 
this.PaintMultipleRowSelection(f3,i4,aa6,1,w2,select);
if (select)
{
this.SetSelection(f3,-1,index,-1,w2,null,i4,aa6);
this.AddColSelection(f3,index);
}
}
this.AddColSelection=function (f3,index){
var aa7=0;
for (var f5=0;f5<f3.selectedCols.length;f5++){
if (f3.selectedCols[f5]==index)return ;
if (index>f3.selectedCols[f5])aa7=f5+1;
}
f3.selectedCols.length++;
for (var f5=f3.selectedCols.length-1;f5>aa7;f5--)
f3.selectedCols[f5]=f3.selectedCols[f5-1];
f3.selectedCols[aa7]=index;
}
this.IsColSelected=function (f3,w3){
for (var f5=0;f5<f3.selectedCols.length;f5++)
if (f3.selectedCols[f5]==w3)return true;
return false;
}
this.SyncColSelection=function (f3){
f3.selectedCols=[];
var aa8=this.GetSelectedRanges(f3);
for (var f5=0;f5<aa8.length;f5++){
var j2=aa8[f5];
if (j2.type=="Column"){
for (var j4=j2.col;j4<j2.col+j2.colCount;j4++){
this.AddColSelection(f3,j4);
}
}
}
}
this.InitMovingCol=function (f3,w3,isGroupBar,q5){
if (f3.getAttribute("LayoutMode")&&w3==-1)return ;
if (this.GetOperationMode(f3)!="Normal"){
f3.selectedCols=[];
f3.selectedCols.push(w3);
}
if (isGroupBar){
this.dragCol=w3;
this.dragViewCol=this.GetColByKey(f3,w3);
}else {
if (f3.getAttribute("LayoutMode"))
this.dragCol=this.GetColTemplateRowFromGroupCell(f3,w3);
else 
this.dragCol=parseInt(this.GetSheetColIndex(f3,w3));
this.dragViewCol=w3;
}
var aa9=this.GetMovingCol(f3);
if (isGroupBar){
this.ClearSelection(f3);
aa9.innerHTML="";
var ab0=document.createElement("DIV");
ab0.innerHTML=q5.innerHTML;
ab0.style.borderTop="0px solid";
ab0.style.borderLeft="0px solid";
ab0.style.borderRight="#808080 1px solid";
ab0.style.borderBottom="#808080 1px solid";
ab0.style.width=""+Math.max(this.GetPreferredCellWidth(f3,q5),80)+"px";
aa9.appendChild(ab0);
if (f3.getAttribute("DragColumnCssClass")==null){
aa9.style.backgroundColor=q5.style.backgroundColor;
aa9.style.paddingTop="1px";
aa9.style.paddingBottom="1px";
}
aa9.style.top="-50px";
aa9.style.left="-100px";
}else {
var ab1=0;
aa9.style.top="0px";
aa9.style.left="-1000px";
aa9.style.display="";
aa9.innerHTML="";
var ab2=document.createElement("TABLE");
aa9.appendChild(ab2);
var k3=document.createElement("TR");
ab2.appendChild(k3);
for (var f5=0;f5<f3.selectedCols.length;f5++){
var i7=document.createElement("TD");
k3.appendChild(i7);
var ab3;
var ab4;
if (f3.getAttribute("LayoutMode")){
ab3=this.GetRowTemplateRowFromGroupCell(f3,w3,true);
ab4=this.GetColTemplateRowFromGroupCell(f3,w3);
}
else {
if (f3.getAttribute("columnHeaderAutoTextIndex")!=null)
ab3=parseInt(f3.getAttribute("columnHeaderAutoTextIndex"));
else 
ab3=f3.getAttribute("ColHeaders")-1;
ab4=f3.selectedCols[f5];
}
var ab5=this.GetHeaderCellFromRowCol(f3,ab3,ab4,true);
if (ab5.getAttribute("FpCellType")=="ExtenderCellType"&&ab5.getElementsByTagName("DIV").length>0){
var ab6=this.GetEditor(ab5);
var ab7=this.GetFunction("ExtenderCellType_getEditorValue");
if (ab6!=null&&ab7!=null){
i7.innerHTML=ab7(ab6);
}
}
else 
i7.innerHTML=ab5.innerHTML;
i7.style.cssText=ab5.style.cssText;
i7.style.borderTop="0px solid";
i7.style.borderLeft="0px solid";
i7.style.borderRight="#808080 1px solid";
i7.style.borderBottom="#808080 1px solid";
i7.setAttribute("align","center");
var k7=Math.max(this.GetPreferredCellWidth(f3,ab5),80);
i7.style.width=""+k7+"px";
ab1+=k7;
}
if (f3.getAttribute("DragColumnCssClass")==null){
aa9.style.backgroundColor=f3.getAttribute("SelectedBackColor");
aa9.style.tableLayout="fixed";
aa9.style.width=""+ab1+"px";
}
}
f3.selectedCols.context=[];
var ab8=f3.selectedCols.context;
var t3=0;
var g4=this.GetColGroup(this.GetFrozColHeader(f3));
if (g4!=null){
for (var f5=0;f5<g4.childNodes.length&&f5<f3.frzCols;f5++){
var ab9=g4.childNodes[f5].offsetWidth;
ab8.push({left:t3,width:ab9});
t3+=ab9;
}
}
g4=this.GetColGroup(this.GetColHeader(f3));
if (g4!=null){
for (var f5=0;f5<g4.childNodes.length;f5++){
var ab9=g4.childNodes[f5].offsetWidth;
ab8.push({left:t3,width:ab9});
t3+=ab9;
}
}
}
this.SelectTable=function (f3,select){
if (select)this.UpdateAnchorCell(f3,0,0,false);
f3.e6="t";
this.PaintSelection(f3,-1,-1,-1,-1,select);
if (select)
{
this.SetSelection(f3,-1,-1,-1,-1);
}
}
this.GetSpanCell=function (i4,i6,span){
if (span==null){
return null;
}
var w2=span.length;
for (var f5=0;f5<w2;f5++){
var t5=span[f5];
var ac0=(t5.row<=i4&&i4<t5.row+t5.rowCount&&t5.col<=i6&&i6<t5.col+t5.colCount);
if (ac0)return t5;
}
return null;
}
this.IsCovered=function (f3,i4,i6,span){
var t5=this.GetSpanCell(i4,i6,span);
if (t5==null){
return false;
}else {
if (t5.row==i4&&t5.col==i6)return false;
return true;
}
}
this.IsSpanCell=function (f3,i4,i6){
var e8=f3.e8;
var w2=e8.length;
for (var f5=0;f5<w2;f5++){
var t5=e8[f5];
var ac0=(t5.row==i4&&t5.col==i6);
if (ac0)return t5;
}
return null;
}
this.SelectRange=function (f3,i4,i6,f6,i3,select){
f3.e6="";
this.UpdateRangeSelection(f3,i4,i6,f6,i3,select);
if (select){
this.SetSelection(f3,i4,i6,f6,i3);
this.PaintAnchorCell(f3);
}
}
this.UpdateRangeSelection=function (f3,i4,i6,f6,i3,select){
var k0=this.GetViewport(f3);
this.UpdateRangeSelection(f3,i4,i6,f6,i3,select,k0);
}
this.GetSpanCells=function (f3,k0){
if (k0==this.GetViewport(f3)||k0==this.GetViewport1(f3)||k0==this.GetViewport2(f3)||k0==this.GetViewport0(f3))
return f3.e8;
else if (k0==this.GetColHeader(f3)||k0==this.GetFrozColHeader(f3))
return f3.f0;
else if (k0==this.GetRowHeader(f3)||k0==this.GetFrozRowHeader(f3))
return f3.e9;
return null;
}
this.UpdateRangeSelection=function (f3,i4,i6,f6,i3,select,k0){
if (k0==null)return ;
for (var f5=i4;f5<i4+f6&&f5<k0.rows.length;f5++){
if (this.IsChildSpreadRow(f3,k0,f5))continue ;
var ac1=this.GetCellIndex(f3,f5,i6,this.GetSpanCells(f3,k0));
for (var j4=0;j4<i3;j4++){
if (this.IsCovered(f3,f5,i6+j4,this.GetSpanCells(f3,k0)))continue ;
if (ac1<k0.rows[f5].cells.length){
this.PaintSelectedCell(f3,k0.rows[f5].cells[ac1],select);
}
ac1++;
}
}
}
this.GetColFromCell=function (f3,i7){
if (i7==null)return -1;
var i4=this.GetRowFromCell(f3,i7);
return this.GetColIndex(f3,i4,i7.cellIndex,this.GetSpanCells(f3,i7.parentNode.parentNode.parentNode),this.InFrozCols(f3,i7),this.IsChild(i7,this.GetFrozRowHeader(f3))||this.IsChild(i7,this.GetRowHeader(f3)));
}
this.GetRowFromCell=function (f3,i7){
if (i7==null||i7.parentNode==null)return -1;
var i4=i7.parentNode.rowIndex;
if (f3.frzRows>0&&(this.IsChild(i7,this.GetViewport2(f3))||this.IsChild(i7,this.GetViewport(f3))||this.IsChild(i7,this.GetRowHeader(f3)))){
i4+=f3.frzRows;
}
return i4;
}
this.GetColIndex=function (f3,f5,v6,span,frozArea,d4){
var ac2=0;
var w2=this.GetColCount(f3);
var ac3=f3.frzCols;
if (frozArea){
w2=f3.frzCols;
ac3=0;
}else if (d4){
ac3=0;
var g4=null;
if (this.GetFrozRowHeader(f3)!=null)
g4=this.GetColGroup(this.GetFrozRowHeader(f3));
else if (this.GetRowHeader(f3)!=null)
g4=this.GetColGroup(this.GetRowHeader(f3));
if (g4!=null)
w2=g4.childNodes.length;
}
for (var j4=ac3;j4<w2;j4++){
if (this.IsCovered(f3,f5,j4,span))continue ;
if (ac2==v6){
return j4;
}
ac2++;
}
return w2;
}
this.GetCellIndex=function (f3,f5,w3,span){
var ac4=false;
var g0=this.GetViewport(f3);
if (g0!=null)ac4=g0.parentNode.getAttribute("hiddenCells");
if (ac4&&span==f3.e8){
if (span!=f3.e9&&w3>=f3.frzCols){
return w3-f3.frzCols;
}
return w3;
}else {
var ac3=0;
var w2=w3;
if (span!=f3.e9&&w3>=f3.frzCols){
ac3=f3.frzCols;
w2=w3-f3.frzCols;
}
var ac2=0;
for (var j4=0;j4<w2;j4++){
if (this.IsCovered(f3,f5,ac3+j4,span))continue ;
ac2++;
}
return ac2;
}
}
this.PaintSelections=function (f3,select){
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var p8=g8.getElementsByTagName("state")[0];
var p4=p8.getElementsByTagName("selection")[0];
var p5=p4.firstChild;
while (p5!=null){
var i4=0;
var i6=0;
if (f3.getAttribute("LayoutMode")&&p5.getAttribute("row")!="-1"&&p5.getAttribute("col")!="-1"){
var i7=this.GetCellByRowCol2(f3,p5.getAttribute("row"),p5.getAttribute("col"));
if (i7){
i4=this.GetRowFromCell(f3,i7);
i6=this.GetColFromCell(f3,i7);
}
}
else if (f3.getAttribute("LayoutMode")&&p5.getAttribute("col")!="-1"&&p5.getAttribute("row")=="-1"&&p5.getAttribute("rowcount")=="-1"){
var k3=this.GetRowTemplateRowFromGroupCell(f3,parseInt(p5.getAttribute("col")));
var i7=this.GetCellByRowCol2(f3,k3,parseInt(p5.getAttribute("col")));
if (i7){
i4=parseInt(i7.parentNode.getAttribute("row"));
i6=this.GetColFromCell(f3,i7);
}
}
else {
i4=this.GetRowByKey(f3,p5.getAttribute("row"));
i6=this.GetColByKey(f3,p5.getAttribute("col"));
}
var f6=parseInt(p5.getAttribute("rowcount"));
var i3=parseInt(p5.getAttribute("colcount"));
p5.setAttribute("rowIndex",i4);
p5.setAttribute("colIndex",i6);
if (f3.getAttribute("LayoutMode")&&p5.getAttribute("col")>=0&&p5.getAttribute("row")>=0&&(p5.getAttribute("rowcount")>=1||p5.getAttribute("colcount")>=1)){
var q4=p5.nextSibling;
if (parseInt(p5.getAttribute("row"))!=parseInt(p9.innerHTML)||parseInt(p5.getAttribute("col"))!=parseInt(q0.innerHTML))p4.removeChild(p5);
p5=q4;
continue ;
}
if (f3.getAttribute("LayoutMode")&&p5.getAttribute("col")=="-1"&&p5.getAttribute("row")!=-1){
f6=parseInt(f3.getAttribute("layoutrowcount"));
}
if (f3.getAttribute("LayoutMode")&&p5.getAttribute("col")!="-1"&&p5.getAttribute("row")=="-1"&&p5.getAttribute("rowcount")=="-1")
this.PaintMultipleRowSelection(f3,i4,i6,1,1,select);
else 
this.PaintSelection(f3,i4,i6,f6,i3,select);
p5=p5.nextSibling;
}
}
this.NextCell=function (f3,event,key){
if (event.altKey)return ;
var r0=this.GetParent(this.GetViewport(f3));
if (f3.e0==null){
var j6=this.FireActiveCellChangingEvent(f3,0,0);
if (!j6){
f3.SetActiveCell(0,0);
var h4=this.CreateEvent("ActiveCellChanged");
h4.cmdID=f3.id;
h4.row=h4.Row=0;
h4.col=h4.Col=0;
this.FireEvent(f3,h4);
}
return ;
}
if (event.shiftKey&&key!=event.DOM_VK_TAB){
this.CancelDefault(event);
var t1=this.GetOperationMode(f3);
if (t1=="RowMode"||t1=="SingleSelect"||t1=="MultiSelect"||(t1=="Normal"&&this.GetSelectionPolicy(f3)=="Single"))return ;
var t5=this.GetSpanCell(f3.e2,f3.e3,this.GetSpanCells(f3,this.GetViewportFromCell(f3,f3.e0)));
switch (key){
case event.DOM_VK_RIGHT:
var i4=f3.e2;
var i6=f3.e3+1;
if (t5!=null){
i6=t5.col+t5.colCount;
}
if (i6>this.GetColCount(f3)-1)return ;
f3.e3=i6;
f3.e1=this.GetCellFromRowCol(f3,i4,i6);
this.Select(f3,f3.e0,f3.e1);
break ;
case event.DOM_VK_LEFT:
var i4=f3.e2;
var i6=f3.e3-1;
if (t5!=null){
i6=t5.col-1;
}
t5=this.GetSpanCell(i4,i6,this.GetSpanCells(f3,this.GetViewportFromCell(f3,f3.e0)));
if (t5!=null){
if (this.IsSpanCell(f3,i4,t5.col))i6=t5.col;
}
if (i6<0)return ;
f3.e3=i6;
f3.e1=this.GetCellFromRowCol(f3,i4,i6);
this.Select(f3,f3.e0,f3.e1);
break ;
case event.DOM_VK_DOWN:
var i4=f3.e2+1;
var i6=f3.e3;
if (t5!=null){
i4=t5.row+t5.rowCount;
}
i4=this.GetNextRow(f3,i4);
if (i4>this.GetRowCountInternal(f3)-1)return ;
f3.e2=i4;
f3.e1=this.GetCellFromRowCol(f3,i4,i6);
this.Select(f3,f3.e0,f3.e1);
this.PaintSelections(f3,true);
break ;
case event.DOM_VK_UP:
var i4=f3.e2-1;
var i6=f3.e3;
if (t5!=null){
i4=t5.row-1;
}
i4=this.GetPrevRow(f3,i4);
t5=this.GetSpanCell(i4,i6,this.GetSpanCells(f3,this.GetViewportFromCell(f3,f3.e0)));
if (t5!=null){
if (this.IsSpanCell(f3,t5.row,i6))i4=t5.row;
}
if (i4<0)return ;
f3.e2=i4;
f3.e1=this.GetCellFromRowCol(f3,i4,i6);
this.Select(f3,f3.e0,f3.e1);
this.PaintSelections(f3,true);
break ;
case event.DOM_VK_HOME:
if (f3.e0.parentNode.rowIndex>=0){
f3.e3=0;
f3.e1=this.GetCellFromRowCol(f3,f3.e2,f3.e3);
this.Select(f3,f3.e0,f3.e1);
}
break ;
case event.DOM_VK_END:
if (f3.e0.parentNode.rowIndex>=0){
f3.e3=this.GetColCount(f3)-1;
f3.e1=this.GetCellFromRowCol(f3,f3.e2,f3.e3);
this.Select(f3,f3.e0,f3.e1);
}
break ;
case event.DOM_VK_PAGE_DOWN:
if (this.GetViewport(f3)!=null&&f3.e0.parentNode.rowIndex>=0){
i4=0;
for (i4=0;i4<this.GetViewport(f3).rows.length;i4++){
if (this.GetViewport(f3).rows[i4].offsetTop+this.GetViewport(f3).rows[i4].offsetHeight>this.GetViewport(f3).parentNode.offsetHeight+this.GetViewport(f3).parentNode.scrollTop){
break ;
}
}
i4=this.GetNextRow(f3,i4);
if (i4<this.GetViewport(f3).rows.length){
this.GetViewport(f3).parentNode.scrollTop=this.GetViewport(f3).rows[i4].offsetTop;
f3.e2=i4;
}else {
i4=this.GetRowCountInternal(f3)-1;
f3.e2=i4;
}
f3.e1=this.GetCellFromRowCol(f3,f3.e2,f3.e3);
this.Select(f3,f3.e0,f3.e1);
}
break ;
case event.DOM_VK_PAGE_UP:
if (this.GetViewport(f3)!=null&&f3.e0.parentNode.rowIndex>0){
i4=0;
for (i4=0;i4<this.GetViewport(f3).rows.length;i4++){
if (this.GetViewport(f3).rows[i4].offsetTop+this.GetViewport(f3).rows[i4].offsetHeight>this.GetViewport(f3).parentNode.scrollTop){
break ;
}
}
if (i4<this.GetViewport(f3).rows.length){
var f7=0;
while (i4>0){
f7+=this.GetViewport(f3).rows[i4].offsetHeight;
if (f7>this.GetViewport(f3).parentNode.offsetHeight){
break ;
}
i4--;
}
i4=this.GetPrevRow(f3,i4);
if (i4>=0){
this.GetViewport(f3).parentNode.scrollTop=this.GetViewport(f3).rows[i4].offsetTop;
f3.e2=i4;
f3.e1=this.GetCellFromRowCol(f3,f3.e2,f3.e3);
this.Select(f3,f3.e0,f3.e1);
}
}
}
break ;
}
this.SyncColSelection(f3);
}else {
var ac5=(key==event.DOM_VK_TAB);
if (key==event.DOM_VK_TAB){
if (event.shiftKey)key=event.DOM_VK_LEFT;
else key=event.DOM_VK_RIGHT;
}
var q9=f3.e0;
var i4=f3.e2;
var i6=f3.e3;
switch (key){
case event.DOM_VK_RIGHT:
if (event.keyCode==event.DOM_VK_TAB){
var ac6=i4;
var ac7=i6;
do {
this.MoveRight(f3,i4,i6);
i4=f3.e2;
i6=f3.e3;
}while (!(ac6==i4&&ac7==i6)&&this.GetCellFromRowCol(f3,i4,i6).getAttribute("TabStop")!=null&&this.GetCellFromRowCol(f3,i4,i6).getAttribute("TabStop")=="false")
}
else {
this.MoveRight(f3,i4,i6);
}
break ;
case event.DOM_VK_LEFT:
if (event.keyCode==event.DOM_VK_TAB){
var ac6=i4;
var ac7=i6;
do {
this.MoveLeft(f3,i4,i6);
i4=f3.e2;
i6=f3.e3;
}while (!(ac6==i4&&ac7==i6)&&this.GetCellFromRowCol(f3,i4,i6).getAttribute("TabStop")!=null&&this.GetCellFromRowCol(f3,i4,i6).getAttribute("TabStop")=="false")
}
else {
this.MoveLeft(f3,i4,i6);
}
break ;
case event.DOM_VK_DOWN:
this.MoveDown(f3,i4,i6);
break ;
case event.DOM_VK_UP:
this.MoveUp(f3,i4,i6);
break ;
case event.DOM_VK_HOME:
if (f3.e0.parentNode.rowIndex>=0){
this.UpdateLeadingCell(f3,i4,0);
}
break ;
case event.DOM_VK_END:
if (f3.e0.parentNode.rowIndex>=0){
i6=this.GetColCount(f3)-1;
this.UpdateLeadingCell(f3,i4,i6);
}
break ;
case event.DOM_VK_PAGE_DOWN:
if (this.GetViewport(f3)!=null&&f3.e0.parentNode.rowIndex>=0){
i4=0;
for (i4=0;i4<this.GetViewport(f3).rows.length;i4++){
if (this.GetViewport(f3).rows[i4].offsetTop+this.GetViewport(f3).rows[i4].offsetHeight>this.GetViewport(f3).parentNode.offsetHeight+this.GetViewport(f3).parentNode.scrollTop){
break ;
}
}
i4=this.GetNextRow(f3,i4);
if (i4<this.GetViewport(f3).rows.length){
var h1=this.GetViewport(f3).rows[i4].offsetTop;
this.UpdateLeadingCell(f3,i4,f3.e3);
this.GetViewport(f3).parentNode.scrollTop=h1;
}else {
i4=this.GetPrevRow(f3,this.GetRowCount(f3)-1);
this.UpdateLeadingCell(f3,i4,f3.e3);
}
}
break ;
case event.DOM_VK_PAGE_UP:
if (this.GetViewport(f3)!=null&&f3.e0.parentNode.rowIndex>=0){
i4=0;
for (i4=0;i4<this.GetViewport(f3).rows.length;i4++){
if (this.GetViewport(f3).rows[i4].offsetTop+this.GetViewport(f3).rows[i4].offsetHeight>this.GetViewport(f3).parentNode.scrollTop){
break ;
}
}
if (i4<this.GetViewport(f3).rows.length){
var f7=0;
while (i4>=0){
f7+=this.GetViewport(f3).rows[i4].offsetHeight;
if (f7>this.GetViewport(f3).parentNode.offsetHeight){
break ;
}
i4--;
}
i4=this.GetPrevRow(f3,i4);
if (i4>=0){
var h1=this.GetViewport(f3).rows[i4].offsetTop;
this.UpdateLeadingCell(f3,i4,f3.e3);
this.GetViewport(f3).parentNode.scrollTop=h1;
}
}
}
break ;
}
if (q9!=f3.e0){
var h4=this.CreateEvent("ActiveCellChanged");
h4.cmdID=f3.id;
h4.Row=h4.row=this.GetSheetIndex(f3,this.GetRowFromCell(f3,f3.e0));
h4.Col=h4.col=this.GetColFromCell(f3,f3.e0);
if (f3.getAttribute("LayoutMode"))
h4.InnerRow=h4.innerRow=f3.e0.parentNode.getAttribute("row");
this.FireEvent(f3,h4);
}
var t1=this.GetOperationMode(f3);
if (t1=="ExtendedSelect")this.SelectRow(f3,f3.e2,1,true,true);
}
var i7=this.GetCellFromRowCol(f3,f3.e2,f3.e3);
if (key==event.DOM_VK_LEFT&&i7.offsetLeft<r0.scrollLeft){
if (i7.cellIndex>0)
r0.scrollLeft=f3.e0.offsetLeft;
else 
r0.scrollLeft=0;
}else if (i7.cellIndex==0){
r0.scrollLeft=0;
}
if (key==event.DOM_VK_RIGHT&&i7.offsetLeft+i7.offsetWidth>r0.scrollLeft+r0.offsetWidth-10){
r0.scrollLeft+=i7.offsetWidth;
}
else if (key==event.DOM_VK_RIGHT&&i7.cellIndex>=0){
if (f3.e0.offsetLeft>r0.offsetWidth)
r0.scrollLeft=f3.e0.offsetLeft-r0.offsetWidth;
else 
r0.scrollLeft=0;
}
if (key==event.DOM_VK_UP&&i7.parentNode.offsetTop<r0.scrollTop){
if (i7.parentNode.rowIndex>1)
r0.scrollTop=i7.parentNode.offsetTop;
else 
r0.scrollTop=0;
}else if (i7.parentNode.rowIndex==1){
r0.scrollTop=0;
}
var r1=this.GetParent(this.GetViewport(f3));
r0=this.GetParent(this.GetViewport(f3));
if (key==event.DOM_VK_DOWN&&(this.IsChild(i7,r0)||this.IsChild(i7,this.GetViewport2(f3)))&&i7.offsetTop+i7.offsetHeight>r0.scrollTop+r0.clientHeight){
r1.scrollTop+=i7.offsetHeight;
}
if (i7!=null&&i7.offsetWidth<r0.clientWidth){
if ((this.IsChild(i7,r0)||this.IsChild(i7,this.GetViewport1(f3)))&&i7.offsetLeft+i7.offsetWidth>r0.scrollLeft+r0.clientWidth){
r1.scrollLeft=i7.offsetLeft+i7.offsetWidth-r0.clientWidth;
}
}
if ((this.IsChild(i7,r0)||this.IsChild(i7,this.GetViewport1(f3)))&&i7.offsetTop+i7.offsetHeight>r0.scrollTop+r0.clientHeight&&i7.offsetHeight<r0.clientHeight){
r1.scrollTop=i7.offsetTop+i7.offsetHeight-r0.clientHeight;
}
if (i7.offsetTop<r0.scrollTop){
r1.scrollTop=i7.offsetTop;
}
this.ScrollView(f3);
this.EnableButtons(f3);
this.SaveData(f3);
var j6=true;
if (f3.e0!=null){
var j8=this.GetEditor(f3.e0);
if (j8!=null){
if (event.shiftKey&&!ac5){
j8.blur();
}else {
this.SetEditorFocus(j8);
if (!j8.disabled&&(j8.type==null||j8.type=="checkbox"||j8.type=="radio"||j8.type=="text"||j8.type=="password"||j8.tagName=="SELECT")){
this.editing=true;
this.b7=j8;
this.b8=this.GetEditorValue(j8);
}
}
j6=false;
}
}
if (j6)this.Focus(f3);
}
this.MoveUp=function (f3,i4,i6){
var f6=this.GetRowCountInternal(f3);
var i3=this.GetColCount(f3);
i4--;
i4=this.GetPrevRow(f3,i4);
if (i4>=0){
f3.e2=i4;
this.UpdateLeadingCell(f3,f3.e2,f3.e3);
}
}
this.MoveDown=function (f3,i4,i6){
var f6=this.GetRowCountInternal(f3);
var i3=this.GetColCount(f3);
var t5=this.GetSpanCell(i4,i6,this.GetSpanCells(f3,this.GetViewportFromCell(f3,f3.e0)));
if (t5!=null){
i4=t5.row+t5.rowCount;
}else {
i4++;
}
i4=this.GetNextRow(f3,i4);
if (i4==f6)i4=f6-1;
if (i4<f6){
f3.e2=i4;
this.UpdateLeadingCell(f3,f3.e2,f3.e3);
}
}
this.MoveLeft=function (f3,i4,i6){
var ac8=i4;
var f6=this.GetRowCountInternal(f3);
var i3=this.GetColCount(f3);
var t5=this.GetSpanCell(i4,i6,this.GetSpanCells(f3,this.GetViewportFromCell(f3,f3.e0)));
if (t5!=null){
i6=t5.col-1;
}else {
i6--;
}
if (i6<0){
i6=i3-1;
i4--;
if (i4<0){
i4=f6-1;
}
i4=this.GetPrevRow(f3,i4);
if (i4<0){
i4=f6-1;
}
i4=this.GetPrevRow(f3,i4);
f3.e2=i4;
}
var ac9=this.UpdateLeadingCell(f3,f3.e2,i6);
if (ac9)f3.e2=ac8;
}
this.MoveRight=function (f3,i4,i6){
var ac8=i4;
var f6=this.GetRowCountInternal(f3);
var i3=this.GetColCount(f3);
var t5=this.GetSpanCell(i4,i6,this.GetSpanCells(f3,this.GetViewportFromCell(f3,f3.e0)));
if (t5!=null){
i6=t5.col+t5.colCount;
}else {
i6++;
}
if (i6>=i3){
i6=0;
i4++;
if (i4>=f6)i4=0;
i4=this.GetNextRow(f3,i4);
if (i4>=f6)i4=0;
i4=this.GetNextRow(f3,i4);
f3.e2=i4;
}
var ac9=this.UpdateLeadingCell(f3,f3.e2,i6);
if (ac9)f3.e2=ac8;
}
this.UpdateLeadingCell=function (f3,i4,i6){
var ad0=0;
if (f3.getAttribute("LayoutMode")){
ad0=this.GetRowFromViewPort(f3,i4,i6);
var ad1=this.GetCellFromRowCol(f3,ad0,i6);
if (ad1)ad0=ad1.parentNode.getAttribute("row");
}
var j6=this.FireActiveCellChangingEvent(f3,i4,i6,ad0);
if (!j6){
var p6=this.GetOperationMode(f3);
if (p6!="MultiSelect")
this.ClearSelection(f3);
f3.e3=i6;
f3.e2=i4;
f3.e5=i6;
f3.e4=i4;
this.UpdateAnchorCell(f3,i4,i6);
}
return j6;
}
this.GetPrevRow=function (f3,i4){
if (i4<0)return 0;
var k0=this.GetViewport(f3);
if (i4<f3.frzRows){
k0=this.GetViewport0(f3);
if (k0==null)k0=this.GetViewport1(f3);
}
while (k0!=null&&i4<k0.rows.length){
if (this.IsChildSpreadRow(f3,k0,i4))
i4--;
else 
break ;
}
var j0=0;
if (i4>=f3.frzRows){
i4=i4-f3.frzRows;
j0=f3.frzRows;
}
if (f3.frzCols<this.GetColCount(f3)){
var z7=this.GetViewport2(f3);
while ((z7==null||i4<z7.rows.length&&z7.rows[i4].cells.length==0)&&k0!=null&&i4>0&&k0.rows[i4].cells.length==0)i4--;
}
if (k0!=null&&i4>=0&&i4<k0.rows.length){
if (k0.rows[i4].getAttribute("previewrow")){
i4--;
}
}
return i4+j0;
}
this.GetNextRow=function (f3,i4){
var k0=this.GetViewport(f3);
if (i4<f3.frzRows){
k0=this.GetViewport0(f3);
if (k0==null)k0=this.GetViewport1(f3);
}
while (k0!=null&&i4<k0.rows.length){
if (this.IsChildSpreadRow(f3,k0,i4))i4++;
else 
break ;
}
var j0=0;
if (i4>=f3.frzRows){
i4=i4-f3.frzRows;
j0=f3.frzRows;
}
if (f3.frzCols<this.GetColCount(f3)){
var z7=this.GetViewport2(f3);
while ((z7==null||i4<z7.rows.length&&z7.rows[i4].cells.length==0)&&k0!=null&&i4<k0.rows.length&&k0.rows[i4].cells.length==0)i4++;
}
if (k0!=null&&i4>=0&&i4<k0.rows.length){
if (k0.rows[i4].getAttribute("previewrow")){
i4++;
}
}
return i4+j0;
}
this.FireActiveCellChangedEvent=function (f3,k3,p3){
var h4=this.CreateEvent("ActiveCellChanged");
h4.cmdID=f3.id;
h4.row=this.GetSheetIndex(f3,k3);
h4.col=p3;
this.FireEvent(f3,h4);
}
this.FireActiveCellChangingEvent=function (f3,k3,p3,v0){
var h4=this.CreateEvent("ActiveCellChanging");
h4.cancel=false;
h4.cmdID=f3.id;
h4.row=this.GetSheetIndex(f3,k3);
h4.col=p3;
if (f3.getAttribute("LayoutMode"))
h4.innerRow=v0;
this.FireEvent(f3,h4);
return h4.cancel;
}
this.GetSheetRowIndex=function (f3,i4){
i4=this.GetDisplayIndex(f3,i4);
if (i4<0)return -1;
var l5=null;
if (f3.frzRows>0){
if (i4>=f3.frzRows&&this.GetViewport(f3)!=null){
l5=this.GetViewport(f3).rows[i4-f3.frzRows];
}else if (i4<f3.frzRows&&this.GetViewport1(f3)!=null){
l5=this.GetViewport1(f3).rows[i4];
}
}else {
l5=this.GetViewport(f3).rows[i4];
}
if (l5!=null){
return l5.getAttribute("FpKey");
}else {
return -1;
}
}
this.GetSheetColIndex=function (f3,i6,v0){
var p3=-1;
if (f3.getAttribute("LayoutMode")){
var i4=this.GetDisplayIndex2(f3,v0);
var i7=this.GetCellByRowCol(f3,i4,i6);
if (i7!=null)
p3=parseInt(i7.getAttribute("col"));
}
else {
var g4=null;
if (f3.frzCols>0&&i6<f3.frzCols){
var m2=this.GetFrozColHeader(f3);
if (m2!=null&&m2.rows.length>0){
g4=this.GetColGroup(m2);
}else {
var n8=this.GetViewport2(f3);
if (n8!=null&&n8.rows.length>0){
g4=this.GetColGroup(n8);
}else {
var n6=this.GetViewport0(f3);
if (n6!=null&&n6.rows.length>0){
g4=this.GetColGroup(n6);
}
}
}
if (g4!=null&&i6>=0&&i6<g4.childNodes.length)p3=g4.childNodes[i6].getAttribute("FpCol");
}else {
var ad2=this.GetColHeader(f3);
if (ad2!=null&&ad2.rows.length>0){
g4=this.GetColGroup(ad2);
}else {
var g0=this.GetViewport(f3);
if (g0!=null&&g0.rows.length>0){
g4=this.GetColGroup(g0);
}else {
var n7=this.GetViewport1(f3);
if (n7!=null&&n7.rows.length>0){
g4=this.GetColGroup(n7);
}
}
}
if (g4!=null&&i6-f3.frzCols>=0&&i6-f3.frzCols<g4.childNodes.length){
p3=g4.childNodes[i6-f3.frzCols].getAttribute("FpCol");
}
}
}
return p3;
}
this.GetCellByRowCol=function (f3,i4,i6){
i4=this.GetDisplayIndex(f3,i4);
return this.GetCellFromRowCol(f3,i4,i6);
}
this.GetHeaderCellFromRowCol=function (f3,i4,i6,d5){
if (i4<0||i6<0)return null;
var g0=null;
if (d5){
if (i6<f3.frzCols){
g0=this.GetFrozColHeader(f3);
}else {
g0=this.GetColHeader(f3);
}
}else {
if (i4<f3.frzRows){
g0=this.GetFrozRowHeader(f3);
}else {
g0=this.GetRowHeader(f3);
}
}
var t5=this.GetSpanCell(i4,i6,this.GetSpanCells(f3,g0));
if (t5!=null){
i4=t5.row;
i6=t5.col;
}
var v5=this.GetCellIndex(f3,i4,i6,this.GetSpanCells(f3,g0));
if (!d5){
if (i4>=f3.frzRows){
i4-=f3.frzRows;
}
}
return g0.rows[i4].cells[v5];
}
this.GetCellFromRowCol=function (f3,i4,i6,prevCell){
if (i4<0||i6<0)return null;
var g0=null;
if (i4<f3.frzRows){
if (i6<f3.frzCols){
g0=this.GetViewport0(f3);
}else {
g0=this.GetViewport1(f3);
}
}else {
if (i6<f3.frzCols){
g0=this.GetViewport2(f3);
}else {
g0=this.GetViewport(f3);
}
}
var e8=f3.e8;
var t5=this.GetSpanCell(i4,i6,e8);
if (t5!=null){
i4=t5.row;
i6=t5.col;
}
var v5=0;
var ac4=false;
if (g0!=null)ac4=g0.parentNode.getAttribute("hiddenCells");
if (prevCell!=null&&!ac4){
if (prevCell.cellIndex<prevCell.parentNode.cells.length-1)
v5=prevCell.cellIndex+1;
}
else 
{
v5=this.GetCellIndex(f3,i4,i6,e8);
}
if (i4>=f3.frzRows){
i4-=f3.frzRows;
}
if (i4>=0&&i4<g0.rows.length)
return g0.rows[i4].cells[v5];
else 
return null;
}
this.GetHiddenValue=function (f3,i4,colName){
if (colName==null)return ;
i4=this.GetDisplayIndex(f3,i4);
var x6=null;
var g0=null;
g0=this.GetViewport(f3);
if (g0!=null&&i4>=0&&i4<g0.rows.length){
var l5=g0.rows[i4];
x6=l5.getAttribute("hv"+colName);
}
return x6;
}
this.GetValue=function (f3,i4,i6){
i4=this.GetDisplayIndex(f3,i4);
var i7=this.GetCellFromRowCol(f3,i4,i6);
var k8=this.GetRender(i7);
var x6=this.GetValueFromRender(f3,k8);
if (x6!=null)x6=this.Trim(x6.toString());
return x6;
}
this.SetValue=function (f3,i4,i6,z1,noEvent,recalc){
i4=this.GetDisplayIndex(f3,i4);
if (z1!=null&&typeof(z1)!="string")z1=new String(z1);
var i7=this.GetCellFromRowCol(f3,i4,i6);
if (this.ValidateCell(f3,i7,z1)){
this.SetCellValueFromView(i7,z1);
if (z1!=null){
this.SetCellValue(f3,i7,""+z1,noEvent,recalc);
}else {
this.SetCellValue(f3,i7,"",noEvent,recalc);
}
this.SizeSpread(f3);
}else {
if (f3.getAttribute("lcidMsg")!=null)
alert(f3.getAttribute("lcidMsg"));
else 
alert("Can't set the data into the cell. The data type is not correct for the cell.");
}
}
this.SetActiveCell=function (f3,i4,i6){
this.ClearSelection(f3,true);
i4=this.GetDisplayIndex(f3,i4);
this.UpdateAnchorCell(f3,i4,i6);
this.ResetLeadingCell(f3);
}
this.GetOperationMode=function (f3){
var p6=f3.getAttribute("OperationMode");
return p6;
}
this.SetOperationMode=function (f3,p6){
f3.setAttribute("OperationMode",p6);
}
this.GetEnableRowEditTemplate=function (f3){
var ad3=f3.getAttribute("EnableRowEditTemplate");
return ad3;
}
this.GetSelectionPolicy=function (f3){
var ad4=f3.getAttribute("SelectionPolicy");
return ad4;
}
this.UpdateAnchorCell=function (f3,i4,i6,select,isColHeader){
if (i4<0||i6<0)return ;
if (f3.getAttribute("LayoutMode")&&f3.allowGroup&&isColHeader)
i4=this.GetRowFromViewPort(f3,i4,i6);
f3.e0=this.GetCellFromRowCol(f3,i4,i6);
if (f3.e0==null)return ;
this.SetActiveRow(f3,this.GetRowKeyFromCell(f3,f3.e0));
this.SetActiveCol(f3,f3.getAttribute("LayoutMode")?this.GetColKeyFromCell2(f3,f3.e0):this.GetColKeyFromCell(f3,f3.e0));
if (select==null||select){
var p6=this.GetOperationMode(f3);
if (p6=="RowMode"||p6=="SingleSelect")
this.SelectRow(f3,i4,1,true,true);
else if (p6!="MultiSelect"&&p6!="ExtendedSelect")
this.SelectRange(f3,i4,i6,1,1,true);
else 
this.PaintFocusRect(f3);
}
}
this.ResetLeadingCell=function (f3){
if (f3.e0==null||!this.IsChild(f3.e0,f3))return ;
f3.e2=this.GetRowFromCell(f3,f3.e0);
f3.e3=this.GetColFromCell(f3,f3.e0);
this.SelectRange(f3.e2,f3.e3,1,1,true);
}
this.Edit=function (f3,k3){
var p6=this.GetOperationMode(f3);
if (p6!="RowMode")return ;
var x3=f3.getAttribute("name");
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5){
if (FarPoint&&FarPoint.System.WebControl.MultiColumnComboBoxCellTypeUtilitis)
FarPoint.System.WebControl.MultiColumnComboBoxCellTypeUtilitis.CloseAll();
this.SyncData(x3,"Edit,"+k3,f3);
}
else 
__doPostBack(x3,"Edit,"+k3);
}
this.Update=function (f3){
if (this.editing&&this.GetOperationMode(f3)!="RowMode"&&this.GetEnableRowEditTemplate(f3)!="true")return ;
this.SaveData(f3);
var x3=f3.getAttribute("name");
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"Update",f3);
else 
__doPostBack(x3,"Update");
}
this.Cancel=function (f3){
var h1=document.getElementById(f3.id+"_data");
h1.value="";
this.SaveData(f3);
var x3=f3.getAttribute("name");
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"Cancel",f3);
else 
__doPostBack(x3,"Cancel");
}
this.Add=function (f3){
if (this.editing)return ;
var x3=null;
var r6=this.GetPageActiveSpread();
if (r6!=null){
x3=r6.getAttribute("name");
}else {
x3=f3.getAttribute("name");
}
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"Add",f3);
else 
__doPostBack(x3,"Add");
}
this.Insert=function (f3){
if (this.editing)return ;
var x3=null;
var r6=this.GetPageActiveSpread();
if (r6!=null){
x3=r6.getAttribute("name");
}else {
x3=f3.getAttribute("name");
}
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"Insert",f3);
else 
__doPostBack(x3,"Insert");
}
this.Delete=function (f3){
if (this.editing)return ;
var x3=null;
var r6=this.GetPageActiveSpread();
if (r6!=null){
x3=r6.getAttribute("name");
}else {
x3=f3.getAttribute("name");
}
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"Delete",f3);
else 
__doPostBack(x3,"Delete");
}
this.Print=function (f3){
if (this.editing)return ;
this.SaveData(f3);
if (document.printSpread==null){
var h1=document.createElement("IFRAME");
h1.name="printSpread";
h1.style.position="absolute";
h1.style.left="-10px";
h1.style.width="0px";
h1.style.height="0px";
document.printSpread=h1;
document.body.insertBefore(h1,null);
h1.addEventListener("load",function (){the_fpSpread.PrintSpread();},false);
}
var ad6=this.GetForm(f3);
if (ad6==null)return ;
{
var j6=ad6;
j6.__EVENTTARGET.value=f3.getAttribute("name");
j6.__EVENTARGUMENT.value="Print";
var ad7=j6.target;
j6.target="printSpread";
j6.submit();
j6.target=ad7;
}
}
this.PrintSpread=function (){
document.printSpread.contentWindow.focus();
document.printSpread.contentWindow.print();
window.focus();
var r6=this.GetPageActiveSpread();
if (r6!=null)this.Focus(r6);
}
this.GotoPage=function (f3,f4){
if (this.editing)return ;
var x3=f3.getAttribute("name");
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"Page,"+f4,f3);
else 
__doPostBack(x3,"Page,"+f4);
}
this.Next=function (f3){
if (this.editing)return ;
var x3=f3.getAttribute("name");
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"Next",f3);
else 
__doPostBack(x3,"Next");
}
this.Prev=function (f3){
if (this.editing)return ;
var x3=f3.getAttribute("name");
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"Prev",f3);
else 
__doPostBack(x3,"Prev");
}
this.GetViewportFromCell=function (f3,k6){
if (k6!=null){
var h1=k6;
while (h1!=null){
if (h1.tagName=="TABLE")break ;
h1=h1.parentNode;
}
if (h1==this.GetViewport(f3)||h1==this.GetViewport0(f3)||h1==this.GetViewport1(f3)||h1==this.GetViewport2(f3))
return h1;
}
return null;
}
this.IsChild=function (i7,k2){
if (i7==null||k2==null)return false;
var h1=i7.parentNode;
while (h1!=null){
if (h1==k2)return true;
h1=h1.parentNode;
}
return false;
}
this.GetCorner=function (f3){
return f3.d3;
}
this.GetFooterCorner=function (f3){
return f3.footerCorner;
}
this.Select=function (f3,cl1,cl2){
if (this.GetSpread(cl1)!=f3||this.GetSpread(cl2)!=f3)return ;
var i8=f3.e4;
var i9=f3.e5;
var ad8=this.GetRowFromCell(f3,cl2);
var o6=0;
if (f3.e6=="r"){
o6=-1;
if (this.IsChild(cl2,this.GetColHeader(f3)))
ad8=0;
}else if (f3.e6=="c"){
if (this.IsChild(cl2,this.GetRowHeader(f3)))
o6=0;
else 
o6=this.GetColFromCell(f3,cl2);
ad8=-1;
}
else {
if (this.IsChild(cl2,this.GetColHeader(f3))){
ad8=0;o6=this.GetColFromCell(f3,cl2);
}else if (this.IsChild(cl2,this.GetRowHeader(f3))){
o6=0;
}else {
o6=this.GetColFromCell(f3,cl2);
}
}
if (f3.e6=="t"){
i9=o6=i8=ad8=-1;
}
var h1=Math.max(i8,ad8);
i8=Math.min(i8,ad8);
ad8=h1;
h1=Math.max(i9,o6);
i9=Math.min(i9,o6);
o6=h1;
var j2=null;
var p4=this.GetSelection(f3);
var p5=p4.lastChild;
if (p5!=null){
var i4=this.GetRowByKey(f3,p5.getAttribute("row"));
var i6=this.GetColByKey(f3,p5.getAttribute("col"));
var f6=parseInt(p5.getAttribute("rowcount"));
var i3=parseInt(p5.getAttribute("colcount"));
j2=new this.Range();
this.SetRange(j2,"cell",i4,i6,f6,i3);
}
if (j2!=null&&j2.col==-1&&j2.row==-1)return ;
if (j2!=null&&j2.col==-1&&j2.row>=0){
if (j2.row>ad8||j2.row+j2.rowCount-1<i8){
this.PaintSelection(f3,j2.row,j2.col,j2.rowCount,j2.colCount,false);
this.PaintSelection(f3,i8,i9,ad8-i8+1,o6-i9+1,true);
}else {
if (i8>j2.row){
var h1=i8-j2.row;
this.PaintSelection(f3,j2.row,j2.col,h1,j2.colCount,false);
if (ad8<j2.row+j2.rowCount-1){
this.PaintSelection(f3,ad8,j2.col,j2.row+j2.rowCount-ad8,j2.colCount,false);
}else {
this.PaintSelection(f3,j2.row+j2.rowCount,j2.col,ad8-j2.row-j2.rowCount+1,j2.colCount,true);
}
}else {
this.PaintSelection(f3,i8,j2.col,j2.row-i8,j2.colCount,true);
if (ad8<j2.row+j2.rowCount-1){
this.PaintSelection(f3,ad8+1,j2.col,j2.row+j2.rowCount-ad8-1,j2.colCount,false);
}else {
this.PaintSelection(f3,j2.row+j2.rowCount,j2.col,ad8-j2.row-j2.rowCount+1,j2.colCount,true);
}
}
}
}else if (j2!=null&&j2.row==-1&&j2.col>=0){
if (j2.col>o6||j2.col+j2.colCount-1<i9){
this.PaintSelection(f3,j2.row,j2.col,j2.rowCount,j2.colCount,false);
this.PaintSelection(f3,i8,i9,ad8-i8+1,o6-i9+1,true);
}else {
if (i9>j2.col){
this.PaintSelection(f3,j2.row,j2.col,j2.rowCount,i9-j2.col,false);
if (o6<j2.col+j2.colCount-1){
this.PaintSelection(f3,j2.row,o6,j2.rowCount,j2.col+j2.colCount-o6,false);
}else {
this.PaintSelection(f3,j2.row,j2.col+j2.colCount,j2.rowCount,o6-j2.col-j2.colCount,true);
}
}else {
this.PaintSelection(f3,j2.row,i9,j2.rowCount,j2.col-i9,true);
if (o6<j2.col+j2.colCount-1){
this.PaintSelection(f3,j2.row,o6+1,j2.rowCount,j2.col+j2.colCount-o6-1,false);
}else {
this.PaintSelection(f3,j2.row,j2.col+j2.colCount,j2.rowCount,o6-j2.col-j2.colCount+1,true);
}
}
}
}else if (j2!=null&&j2.row>=0&&j2.col>=0){
this.ExtendSelection(f3,j2,i8,i9,ad8-i8+1,o6-i9+1);
}else {
this.PaintSelection(f3,i8,i9,ad8-i8+1,o6-i9+1,true);
}
this.SetSelection(f3,i8,i9,ad8-i8+1,o6-i9+1,j2==null);
}
this.ExtendSelection=function (f3,j2,newRow,newCol,newRowCount,newColCount)
{
var t3=Math.max(j2.col,newCol);
var t4=Math.min(j2.col+j2.colCount-1,newCol+newColCount-1);
var y9=Math.max(j2.row,newRow);
var ad9=Math.min(j2.row+j2.rowCount-1,newRow+newRowCount-1);
if (j2.row<y9){
this.PaintSelection(f3,j2.row,j2.col,y9-j2.row,j2.colCount,false);
}
if (j2.col<t3){
this.PaintSelection(f3,j2.row,j2.col,j2.rowCount,t3-j2.col,false);
}
if (j2.row+j2.rowCount-1>ad9){
this.PaintSelection(f3,ad9+1,j2.col,j2.row+j2.rowCount-ad9-1,j2.colCount,false);
}
if (j2.col+j2.colCount-1>t4){
this.PaintSelection(f3,j2.row,t4+1,j2.rowCount,j2.col+j2.colCount-t4-1,false);
}
if (newRow<y9){
this.PaintSelection(f3,newRow,newCol,y9-newRow,newColCount,true);
}
if (newCol<t3){
this.PaintSelection(f3,newRow,newCol,newRowCount,t3-newCol,true);
}
if (newRow+newRowCount-1>ad9){
this.PaintSelection(f3,ad9+1,newCol,newRow+newRowCount-ad9-1,newColCount,true);
}
if (newCol+newColCount-1>t4){
this.PaintSelection(f3,newRow,t4+1,newRowCount,newCol+newColCount-t4-1,true);
}
}
this.PaintAnchorCellHeader=function (f3,select){
var i4,i6;
i4=this.GetRowFromCell(f3,f3.e0);
i6=this.GetColFromCell(f3,f3.e0);
if (select&&f3.e0.getAttribute("group")!=null){
var t5=this.GetSpanCell(i4,i6,f3.e8);
if (t5!=null&&t5.colCount>1){
var ae0=this.GetSelectedRange(f3);
if (i4<ae0.row||i4>=ae0.row+ae0.rowCount||i6<ae0.col||i6>=ae0.col+ae0.colCount)
return ;
}
}
if (this.GetColHeader(f3)!=null)this.PaintHeaderSelection(f3,i4,i6,1,1,select,true);
if (this.GetRowHeader(f3)!=null)this.PaintHeaderSelection(f3,i4,i6,1,1,select,false);
}
this.LineIntersection=function (s1,i9,s2,o6){
var u9,h4;
u9=Math.max(s1,s2);
h4=Math.min(s1+i9,s2+o6);
if (u9<h4)
return {s:u9,c:h4-u9};
return null;
}
this.RangeIntersection=function (i8,i9,aa2,cc1,ad8,o6,rc2,cc2){
var ae1=this.LineIntersection(i8,aa2,ad8,rc2);
var ae2=this.LineIntersection(i9,cc1,o6,cc2);
if (ae1&&ae2)
return {row:ae1.s,col:ae2.s,rowCount:ae1.c,colCount:ae2.c};
return null;
}
this.PaintSelection=function (f3,i4,i6,f6,i3,select){
if (i4<0&&i6<0){
this.PaintCornerSelection(f3,select);
}
var ae3=false;
var ae4=false;
var v7;
var w3;
var ae5;
if (i4<0){
v7=i4;
i4=0;
f6=this.GetRowCountInternal(f3);
}
if (i6<0){
w3=i6;
i6=0;
i3=this.GetColCount(f3);
}
this.PaintViewportSelection(f3,i4,i6,f6,i3,select);
var p4=this.GetSelection(f3);
var p5;
var ad8;
var o6;
var ae6;
var ae7;
var ae8=0;
var ae9=0;
var j2;
var af0;
for (var f5=p4.childNodes.length-1;f5>=0;f5--){
p5=p4.childNodes[f5];
if (p5){
ad8=parseInt(p5.getAttribute("rowIndex"));
o6=parseInt(p5.getAttribute("colIndex"));
ae6=parseInt(p5.getAttribute("rowcount"));
ae7=parseInt(p5.getAttribute("colcount"));
if (ad8<0||ae6<0){ad8=0;ae6=this.GetRowCountInternal(f3);}
if (o6<0||ae7<0){o6=0;ae7=this.GetColCount(f3);}
if (ae8<ae6)
ae8=ae6;
if (ae9<ae7)
ae9=ae7;
if (f5>=p4.childNodes.length-1){
if (i4<=ad8&&f6>=ae6||f3.getAttribute("LayoutMode")&&i3==1&&i4<parseInt(f3.getAttribute("layoutrowcount"))){
if (this.GetColHeader(f3)!=null&&this.GetOperationMode(f3)=="Normal"){
this.PaintHeaderSelection(f3,i4,i6,f6,i3,select,true);
ae3=true;
}
}
if (i6<=o6&&i3>=ae7){
if (this.GetRowHeader(f3)!=null){
this.PaintHeaderSelection(f3,i4,i6,f6,i3,select,false);
ae4=true;
}
}
if (!ae3&&!ae4){
if (this.GetColHeader(f3)!=null&&this.GetOperationMode(f3)=="Normal"){
this.PaintHeaderSelection(f3,i4,i6,f6,i3,select,true);
ae3=true;
}
if (this.GetRowHeader(f3)!=null){
this.PaintHeaderSelection(f3,i4,i6,f6,i3,select,false);
ae4=true;
}
}
}
else {
if (!select&&this.GetOperationMode(f3)=="Normal"&&!f3.getAttribute("LayoutMode")){
j2=this.RangeIntersection(i4,i6,f6,i3,ad8,o6,ae6,ae7);
if (j2){
this.PaintViewportSelection(f3,j2.row,j2.col,j2.rowCount,j2.colCount,true);
}
if (ae3){
af0=this.LineIntersection(i6,i3,o6,ae7);
if (af0)this.PaintHeaderSelection(f3,i4,af0.s,f6,af0.c,true,true);
}
if (ae4){
af0=this.LineIntersection(i4,f6,ad8,ae6);
if (af0)this.PaintHeaderSelection(f3,af0.s,i6,af0.c,i3,true,false);
}
}
}
}
}
if (v7!=null||w3!=null){
if ((ae8<f6&&v7<0)||(ae9<i3&&w3<0))
ae5=true;
}
if (p4.childNodes.length<=0||(f3.getAttribute("SelectionPolicy")=="MultiRange"&&ae5)){
if (this.GetColHeader(f3)!=null&&this.GetOperationMode(f3)=="Normal")this.PaintHeaderSelection(f3,i4,i6,f6,i3,select,true);
if (this.GetRowHeader(f3)!=null)this.PaintHeaderSelection(f3,i4,i6,f6,i3,select,false);
}
this.PaintAnchorCell(f3);
}
this.PaintFocusRect=function (f3){
var i0=document.getElementById(f3.id+"_focusRectT");
if (i0==null)return ;
var af1=this.GetSelectedRange(f3);
if (f3.e0==null&&(af1==null||(af1.rowCount==0&&af1.colCount==0))){
i0.style.left="-1000px";
var x3=f3.id;
i0=document.getElementById(x3+"_focusRectB");
i0.style.left="-1000px";
i0=document.getElementById(x3+"_focusRectL");
i0.style.left="-1000px";
i0=document.getElementById(x3+"_focusRectR");
i0.style.left="-1000px";
return ;
}
var j5=this.GetOperationMode(f3);
if (j5=="RowMode"||j5=="SingleSelect"||j5=="MultiSelect"||j5=="ExtendedSelect"){
var i4=f3.GetActiveRow();
if (f3.getAttribute("layoutMode"))
i4=this.GetFirstMultiRowFromViewport(f3,i4,false);
af1=new this.Range();
this.SetRange(af1,"Row",i4,-1,1,-1);
}else if (af1==null||(af1.rowCount==0&&af1.colCount==0)){
var i4=f3.GetActiveRow();
var i6=f3.GetActiveCol();
af1=new this.Range();
this.SetRange(af1,"Cell",i4,i6,f3.e0.rowSpan,f3.e0.colSpan);
}
if (af1.row<0){
af1.row=0;
af1.rowCount=this.GetRowCountInternal(f3);
}
if (af1.col<0){
af1.col=0;
af1.colCount=this.GetColCount(f3);
if (f3.getAttribute("LayoutMode")&&af1.rowCount<parseInt(f3.getAttribute("layoutrowcount"))&&af1.type=="Row")af1.rowCount=parseInt(f3.getAttribute("layoutrowcount"));
}
var v7;
if (f3.getAttribute("LayoutMode"))
v7=(af1.innerRow!=null)?af1.innerRow:af1.row;
else 
v7=af1.row;
var i7=this.GetCellFromRowCol(f3,v7,af1.col);
if (i7==null)return ;
if (af1.rowCount==1&&af1.colCount==1){
af1.rowCount=i7.rowSpan;
af1.colCount=i7.colSpan;
if (i7.colSpan>1&&!i7.getAttribute("group")&&!f3.getAttribute("LayoutMode")){
var af2=parseInt(i7.getAttribute("col"));
if (af2!=af1.col&&!isNaN(af2))af1.col=af2;
}
}
var h1=this.GetOffsetTop(f3,i7);
var af3=this.GetOffsetLeft(f3,i7);
if (i7.rowSpan>1){
v7=i7.parentNode.rowIndex;
var i9=this.GetCellFromRowCol(f3,v7,af1.col+af1.colCount-1);
if (i9!=null&&i9.parentNode.rowIndex>i7.parentNode.rowIndex){
h1=this.GetOffsetTop(f3,i9);
}
if (f3.getAttribute("LayoutMode")&&af1.rowCount<i7.rowSpan&&(af1.type=="Column"||af1.type=="Row"))af1.rowCount=i7.rowSpan;
}
if (i7.colSpan>1){
var i9=this.GetCellFromRowCol(f3,v7+af1.rowCount-1,af1.col);
var r8=this.GetOffsetLeft(f3,i9);
if (r8>af3){
af3=r8;
i7=i9;
}
if (f3.getAttribute("LayoutMode")&&af1.colCount<i7.colSpan&&(af1.type=="Column"||af1.type=="Row"))af1.colCount=i7.colSpan;
}
var f7=0;
var i2=this.GetViewport(f3).rows;
for (var i4=v7;i4<v7+af1.rowCount&&i4<i2.length;i4++){
f7+=i2[i4].offsetHeight;
if (i4>v7)f7+=parseInt(this.GetViewport(f3).cellSpacing);
}
var k7=0;
var g4=this.GetColGroup(this.GetViewport(f3));
if (g4.childNodes==null||g4.childNodes.length==0)return ;
for (var i6=af1.col;i6<af1.col+af1.colCount&&i6<g4.childNodes.length;i6++){
k7+=g4.childNodes[i6].offsetWidth;
if (i6>af1.col)k7+=parseInt(this.GetViewport(f3).cellSpacing);
}
if (af1.col>i7.cellIndex&&af1.type=="Column"){
var o6=(f3.getAttribute("LayoutMode")!=null)?parseInt(i7.getAttribute("scol")):parseInt(i7.getAttribute("col"));
if (i7.getAttribute("group"))o6=i7.cellIndex;
for (var i6=o6;i6<af1.col;i6++){
af3+=g4.childNodes[i6].offsetWidth;
if (i6>o6)af3+=parseInt(this.GetViewport(f3).cellSpacing);
}
}
if (af1.row>0)h1-=2;
else f7-=2;
if (af1.col>0)af3-=2;
else k7-=2;
if (parseInt(this.GetViewport(f3).cellSpacing)>0){
h1+=1;af3+=1;
}else {
k7+=1;
f7+=1;
}
if (k7<0)k7=0;
if (f7<0)f7=0;
i0.style.left=""+af3+"px";
i0.style.top=""+h1+"px";
i0.style.width=""+k7+"px";
i0=document.getElementById(f3.id+"_focusRectB");
i0.style.left=""+af3+"px";
i0.style.top=""+(h1+f7)+"px";
i0.style.width=""+k7+"px";
i0=document.getElementById(f3.id+"_focusRectL");
i0.style.left=""+af3+"px";
i0.style.top=""+h1+"px";
i0.style.height=""+f7+"px";
i0=document.getElementById(f3.id+"_focusRectR");
i0.style.left=""+(af3+k7)+"px";
i0.style.top=""+h1+"px";
i0.style.height=""+f7+"px";
}
this.PaintCornerSelection=function (f3,select){
var af4=true;
if (this.GetTopSpread(f3).getAttribute("ShowHeaderSelection")=="false")af4=false;
if (!af4)return ;
var p1=this.GetCorner(f3);
if (p1!=null&&p1.rows.length>0){
for (var f5=0;f5<p1.rows.length;f5++){
for (var j4=0;j4<p1.rows[f5].cells.length;j4++){
if (p1.rows[f5].cells[j4]!=null)
this.PaintSelectedCell(f3,p1.rows[f5].cells[j4],select);
}
}
}
}
this.PaintHeaderSelection=function (f3,i4,i6,f6,i3,select,d5){
var af4=true;
if (this.GetTopSpread(f3).getAttribute("ShowHeaderSelection")=="false")af4=false;
if (!af4)return ;
var af5=d5?f3.f0:f3.e9;
if (f3.getAttribute("LayoutMode")&&d5){
if (f6>parseInt(f3.getAttribute("layoutrowcount")))
f6=parseInt(f3.getAttribute("layoutrowcount"));
var af6=this.GetCellFromRowCol(f3,i4,i6);
if (f3.allowGroup&&af6.getAttribute("group")!=null)
i4=this.GetFirstMultiRowFromViewport(f3,i4,true);
for (var f5=i4;f5<i4+f6;f5++){
for (var j4=i6;j4<i6+i3;j4++){
var af7=this.GetCellFromRowCol(f3,f5,j4);
if (af7){
var p7=this.GetRowTemplateRowFromCell(f3,af7);
if (!isNaN(p7)){
if (p7>=parseInt(f3.getAttribute("layoutrowcount")))p7=parseInt(f3.getAttribute("layoutrowcount"))-1;
var i7=this.GetHeaderCellFromRowCol(f3,p7,j4,d5);
if (i7!=null&&this.GetRowTemplateRowFromCell(f3,i7)==p7)this.PaintSelectedCell(f3,i7,select);
}
}
}
}
}
else {
var v8=this.GetRowCountInternal(f3);
var v3=this.GetColCount(f3);
if (d5){
if (this.GetColHeader(f3)==null)return ;
i4=0;
f6=v8=this.GetColHeader(f3).rows.length;
}else {
if (this.GetRowHeader(f3)==null)return ;
i6=0;
i3=v3=this.GetColGroup(this.GetRowHeader(f3)).childNodes.length;
}
}
if (f3.getAttribute("LayoutMode")&&f3.getAttribute("OperationMode")!="Normal"&&!d5)
i4=this.GetFirstMultiRowFromViewport(f3,i4,false);
if (f3.getAttribute("LayoutMode")&&f3.e0!=null&&f3.e0.getAttribute("group")!=null&&!d5&&f6!=v8)
f6=1;
for (var f5=i4;f5<i4+f6&&f5<v8;f5++){
if (!d5&&this.IsChildSpreadRow(f3,this.GetViewport(f3),f5))continue ;
for (var j4=i6;j4<i6+i3&&j4<v3;j4++){
if (!f3.getAttribute("LayoutMode")&&this.IsCovered(f3,f5,j4,af5))continue ;
var i7=this.GetHeaderCellFromRowCol(f3,f5,j4,d5);
if (i7!=null)this.PaintSelectedCell(f3,i7,select);
}
}
}
this.PaintViewportSelection=function (f3,i4,i6,f6,i3,select){
var v8=this.GetRowCountInternal(f3);
var v3=this.GetColCount(f3);
if (f3.getAttribute("LayoutMode")&&f3.getAttribute("OperationMode")!="Normal"&&f6==parseInt(f3.getAttribute("layoutrowcount")))
i4=this.GetFirstMultiRowFromViewport(f3,i4,false);
if (f3.getAttribute("LayoutMode")&&f3.e0!=null&&f3.e0.getAttribute("group")!=null&&f6!=v8)
f6=1;
for (var f5=i4;f5<i4+f6&&f5<v8;f5++){
if (this.IsChildSpreadRow(f3,this.GetViewport(f3),f5))continue ;
var i7=null;
for (var j4=i6;j4<i6+i3&&j4<v3;j4++){
if (this.IsCovered(f3,f5,j4,f3.e8))continue ;
i7=this.GetCellFromRowCol(f3,f5,j4,i7);
this.PaintSelectedCell(f3,i7,select);
}
}
}
this.Copy=function (f3){
var r6=this.GetPageActiveSpread();
if (r6!=null&&r6!=f3&&this.GetTopSpread(r6)==f3){
this.Copy(r6);
return ;
}
var p4=this.GetSelection(f3);
var p5=p4.lastChild;
if (p5!=null){
var i4;
var i6;
var f6;
var i3;
f3.copymulticol=false;
if (f3.getAttribute("LayoutMode")&&p5.getAttribute("col")!="-1"&&p5.getAttribute("row")=="-1"&&p5.getAttribute("rowcount")=="-1"){
var i7=f3.e0;
if (i7){
i4=i7.parentNode.getAttribute("row");
i6=this.GetColFromCell(f3,i7);
f6=this.GetRowCountInternal(f3);
i3=parseInt(p5.getAttribute("colcount"));
f3.copymulticol=true;
}
}
else if (f3.getAttribute("LayoutMode")&&p5.getAttribute("col")=="-1"&&p5.getAttribute("row")!=-1){
var w0=parseInt(p5.getAttribute("row"));
i4=this.GetFirstRowFromKey(f3,w0);
i6=parseInt(p5.getAttribute("colIndex"));
f6=parseInt(f3.getAttribute("layoutrowcount"));
}
else {
i4=f3.getAttribute("LayoutMode")?parseInt(p5.getAttribute("rowIndex")):this.GetRowByKey(f3,p5.getAttribute("row"));
i6=f3.getAttribute("LayoutMode")?parseInt(p5.getAttribute("colIndex")):this.GetColByKey(f3,p5.getAttribute("col"));
f6=parseInt(p5.getAttribute("rowcount"));
i3=parseInt(p5.getAttribute("colcount"));
}
if (i4<0){
i4=0;
f6=this.GetRowCountInternal(f3);
}
if (i6<0){
i6=0;
i3=this.GetColCount(f3);
}
var g7="";
for (var f5=i4;f5<i4+f6;f5++){
if (this.IsChildSpreadRow(f3,this.GetViewport(f3),f5))continue ;
var i7=null;
for (var j4=i6;j4<i6+i3;j4++){
if (this.IsCovered(f3,f5,j4,f3.e8))
g7+="";
else 
{
i7=this.GetCellFromRowCol(f3,f5,j4,i7);
if (f3.getAttribute("LayoutMode")&&f3.copymulticol&&(i7==null||(i7.parentNode.getAttribute("row"))!=i4))continue ;
if (i7!=null&&i7.parentNode.getAttribute("previewrow")!=null)continue ;
var k9=this.GetCellType(i7);
if (k9=="TextCellType"&&i7.getAttribute("password")!=null)
g7+="";
else 
g7+=this.GetCellValueFromView(f3,i7);
}
if (j4+1<i6+i3)g7+="\t";
}
g7+="\r\n";
}
this.c8=g7;
}else {
if (f3.e0!=null){
var g7=this.GetCellValueFromView(f3,f3.e0);
this.c8=g7;
}
}
}
this.GetCellValueFromView=function (f3,i7){
var z1=null;
if (i7!=null){
var af8=this.GetRender(i7);
z1=this.GetValueFromRender(f3,af8);
if (z1==null||z1==" ")z1="";
}
return z1;
}
this.SetCellValueFromView=function (i7,z1,ignoreLock){
if (i7!=null){
var af8=this.GetRender(i7);
var u8=this.GetCellType(i7);
if ((u8!="readonly"||ignoreLock)&&af8!=null&&af8.getAttribute("FpEditor")!="Button")
this.SetValueToRender(af8,z1);
}
}
this.Paste=function (f3){
var r6=this.GetPageActiveSpread();
if (r6!=null&&r6!=f3&&this.GetTopSpread(r6)==f3){
this.Paste(r6);
return ;
}
if (f3.e0==null)return ;
var g7=this.c8;
if (g7==null)return ;
var g0=this.GetViewportFromCell(f3,f3.e0);
var i4=this.GetRowFromCell(f3,f3.e0);
var i6=this.GetColFromCell(f3,f3.e0);
var i3=this.GetColCount(f3);
var f6=this.GetRowCountInternal(f3);
var af9=i4;
var ac1=i6;
var ag0=new String(g7);
if (ag0.length==0)return ;
var f4=ag0.lastIndexOf("\r\n");
if (f4>=0&&f4==ag0.length-2)ag0=ag0.substring(0,f4);
var ag1=0;
var ag2=ag0.split("\r\n");
for (var f5=0;f5<ag2.length&&af9<f6;f5++){
if (typeof(ag2[f5])=="string"){
ag2[f5]=ag2[f5].split("\t");
if (ag2[f5].length>ag1)ag1=ag2[f5].length;
}
af9++;
}
af9=this.GetSheetIndex(f3,i4);
for (var f5=0;f5<ag2.length&&af9<f6;f5++){
var ag3=ag2[f5];
if (ag3!=null){
ac1=i6;
var i7=null;
var ad8=this.GetDisplayIndex(f3,af9);
for (var j4=0;j4<ag3.length&&ac1<i3;j4++){
if (!this.IsCovered(f3,ad8,ac1,f3.e8)){
i7=this.GetCellFromRowCol(f3,ad8,ac1,i7);
if (i7==null)return ;
if (f3.getAttribute("LayoutMode")&&f3.copymulticol&&parseInt(i7.parentNode.getAttribute("row"))!=parseInt(f3.e0.parentNode.getAttribute("row")))continue ;
if (i7!=null&&i7.parentNode.getAttribute("previewrow")!=null)continue ;
var ag4=ag3[j4];
if (!this.ValidateCell(f3,i7,ag4)){
if (f3.getAttribute("lcidMsg")!=null)
alert(f3.getAttribute("lcidMsg"));
else 
alert("Can't set the data into the cell. The data type is not correct for the cell.");
return ;
}
}
ac1++;
}
}
af9++;
}
if (ag2.length==0)return ;
af9=this.GetSheetIndex(f3,i4);
for (var f5=0;f5<ag2.length&&af9<f6;f5++){
ac1=i6;
var ag3=ag2[f5];
var i7=null;
var ad8=this.GetDisplayIndex(f3,af9);
for (var j4=0;j4<ag1&&ac1<i3;j4++){
if (!this.IsCovered(f3,ad8,ac1,f3.e8)){
i7=this.GetCellFromRowCol(f3,ad8,ac1,i7);
if (f3.getAttribute("LayoutMode")&&f3.copymulticol&&parseInt(i7.parentNode.getAttribute("row"))!=parseInt(f3.e0.parentNode.getAttribute("row")))continue ;
if (i7!=null&&i7.parentNode.getAttribute("previewrow")!=null)continue ;
var u8=this.GetCellType(i7);
var af8=this.GetRender(i7);
if (u8!="readonly"&&af8.getAttribute("FpEditor")!="Button"){
var ag4=null;
if (ag3!=null&&j4<ag3.length)ag4=ag3[j4];
this.SetCellValueFromView(i7,ag4);
if (ag4!=null){
this.SetCellValue(f3,i7,""+ag4);
}else {
this.SetCellValue(f3,i7,"");
}
}
}
ac1++;
}
af9++;
}
var z6=f3.getAttribute("autoCalc");
if (z6!="false"){
this.UpdateValues(f3);
}
var g2=this.GetTopSpread(f3);
var h2=document.getElementById(g2.id+"_textBox");
if (h2!=null){
h2.blur();
}
this.Focus(f3);
this.SizeSpread(f3);
}
this.UpdateValues=function (f3){
if (f3.e7==null&&this.GetParentSpread(f3)==null&&f3.getAttribute("rowFilter")!="true"&&f3.getAttribute("hierView")!="true"&&f3.getAttribute("IsNewRow")!="true"){
this.SaveData(f3);
this.StorePostData(f3);
this.SyncData(f3.getAttribute("name"),"UpdateValues",f3);
}
}
this.ValidateCell=function (f3,i7,z1){
if (i7==null||z1==null||z1=="")return true;
var z4=null;
var k9=this.GetCellType(i7);
if (k9!=null){
var j6=this.GetFunction(k9+"_isValid");
if (j6!=null){
z4=j6(i7,z1);
}
}
return (z4==null||z4=="");
}
this.DoclearSelection=function (f3){
var p4=this.GetSelection(f3);
var p5=p4.lastChild;
while (p5!=null){
var i4=f3.getAttribute("LayoutMode")?parseInt(p5.getAttribute("rowIndex")):this.GetRowByKey(f3,p5.getAttribute("row"));
var i6=f3.getAttribute("LayoutMode")?parseInt(p5.getAttribute("colIndex")):this.GetColByKey(f3,p5.getAttribute("col"));
var f6=parseInt(p5.getAttribute("rowcount"));
var i3=parseInt(p5.getAttribute("colcount"));
if (f3.getAttribute("LayoutMode")&&i4!=-1&&(i3==-1||f3.getAttribute("OperationMode")!="Normal")){
f6=parseInt(f3.getAttribute("layoutrowcount"));
this.PaintSelection(f3,i4,-1,f6,-1,false);
}
if (f3.getAttribute("LayoutMode")&&i6!=-1&&(f6==-1||f3.getAttribute("OperationMode")!="Normal")){
var k3=this.GetRowTemplateRowFromGroupCell(f3,parseInt(p5.getAttribute("col")));
var i7=this.GetCellByRowCol2(f3,k3,parseInt(p5.getAttribute("col")));
if (i7){
i4=parseInt(i7.parentNode.getAttribute("row"));
i6=this.GetColFromCell(f3,i7);
}
this.PaintMultipleRowSelection(f3,i4,i6,1,i3,false);
}
else 
this.PaintSelection(f3,i4,i6,f6,i3,false);
p4.removeChild(p5);
p5=p4.lastChild;
}
}
this.Clear=function (f3){
var r6=this.GetPageActiveSpread();
if (r6!=null&&r6!=f3&&this.GetTopSpread(r6)==f3){
this.Clear(r6);
return ;
}
var u8=this.GetCellType(f3.e0);
if (u8=="readonly")return ;
var p4=this.GetSelection(f3);
var p5=p4.lastChild;
if (this.AnyReadOnlyCell(f3,p5)){
return ;
}
this.Copy(f3);
if (p5!=null){
var i4;
var i6;
var f6;
var i3;
var ag5=false;
if (f3.getAttribute("LayoutMode")&&p5.getAttribute("col")!="-1"&&p5.getAttribute("row")=="-1"&&p5.getAttribute("rowcount")=="-1"){
var i7=f3.e0;
if (i7){
i4=i7.parentNode.getAttribute("row");
i6=this.GetColFromCell(f3,i7);
f6=this.GetRowCountInternal(f3);
i3=parseInt(p5.getAttribute("colcount"));
ag5=true;
}
}
else if (f3.getAttribute("LayoutMode")&&p5.getAttribute("col")=="-1"&&p5.getAttribute("row")!=-1){
var w0=parseInt(p5.getAttribute("row"));
i4=this.GetFirstRowFromKey(f3,w0);
i6=parseInt(p5.getAttribute("colIndex"));
f6=parseInt(f3.getAttribute("layoutrowcount"));
}
else {
i4=f3.getAttribute("LayoutMode")?parseInt(p5.getAttribute("rowIndex")):this.GetRowByKey(f3,p5.getAttribute("row"));
i6=f3.getAttribute("LayoutMode")?parseInt(p5.getAttribute("colIndex")):this.GetColByKey(f3,p5.getAttribute("col"));
f6=parseInt(p5.getAttribute("rowcount"));;
i3=parseInt(p5.getAttribute("colcount"));
}
if (i4<0){
i4=0;
f6=this.GetRowCountInternal(f3);
}
if (i6<0){
i6=0;
i3=this.GetColCount(f3);
}
for (var f5=i4;f5<i4+f6;f5++){
if (this.IsChildSpreadRow(f3,this.GetViewport(f3),f5))continue ;
var i7=null;
for (var j4=i6;j4<i6+i3;j4++){
if (!this.IsCovered(f3,f5,j4,f3.e8)){
i7=this.GetCellFromRowCol(f3,f5,j4,i7);
if (f3.getAttribute("LayoutMode")&&ag5&&(i7==null||(i7.parentNode.getAttribute("row"))!=i4))continue ;
if (i7!=null&&i7.parentNode.getAttribute("previewrow")!=null)continue ;
var u8=this.GetCellType(i7);
if (u8!="readonly"){
var ag6=this.GetEditor(i7);
if (ag6!=null&&ag6.getAttribute("FpEditor")=="Button")continue ;
i7.removeAttribute("FpFormula");
this.SetCellValueFromView(i7,null);
this.SetCellValue(f3,i7,"");
}
}
}
}
var z6=f3.getAttribute("autoCalc");
if (z6!="false"){
this.UpdateValues(f3);
}
}
}
this.AnyReadOnlyCell=function (f3,p5){
if (p5!=null){
var i4=this.GetRowByKey(f3,p5.getAttribute("row"));
var i6=this.GetColByKey(f3,p5.getAttribute("col"));
var f6=parseInt(p5.getAttribute("rowcount"));
var i3=parseInt(p5.getAttribute("colcount"));
if (i4<0){
i4=0;
f6=this.GetRowCountInternal(f3);
}
if (i6<0){
i6=0;
i3=this.GetColCount(f3);
}
for (var f5=i4;f5<i4+f6;f5++){
if (this.IsChildSpreadRow(f3,this.GetViewport(f3),f5))continue ;
var i7=null;
for (var j4=i6;j4<i6+i3;j4++){
if (!this.IsCovered(f3,f5,j4,f3.e8)){
i7=this.GetCellFromRowCol(f3,f5,j4,i7);
var u8=this.GetCellType(i7);
if (u8=="readonly"){
return true;
}
}
}
}
}
return false;
}
this.GetViewportFromPoint=function (f3,o2,h8){
var z8=l2=0;
var r8=t2=0;
var m0=w6=0;
var t0=h2=0;
var n6=this.GetViewport0(f3);
var n7=this.GetViewport1(f3);
var n8=this.GetViewport2(f3);
var g0=this.GetViewport(f3);
if (n6!=null){
z8=this.GetOffsetLeft(f3,n6,document.body);
m0=n6.offsetWidth;
r8=this.GetOffsetTop(f3,n6,document.body);
t0=n6.offsetHeight;
}
if (n8!=null){
z8=this.GetOffsetLeft(f3,n8,document.body);
m0=n8.offsetWidth;
t2=this.GetOffsetTop(f3,n8,document.body);
h2=n8.offsetHeight;
}
if (n7!=null){
l2=this.GetOffsetLeft(f3,n7,document.body);
w6=n7.offsetWidth;
r8=this.GetOffsetTop(f3,n7,document.body);
t0=n7.offsetHeight;
}
if (g0!=null){
l2=this.GetOffsetLeft(f3,g0,document.body);
w6=g0.offsetWidth;
t2=this.GetOffsetTop(f3,g0,document.body);
h2=g0.offsetHeight;
}
if (z8<o2&&o2<l2){
if (r8<h8&&h8<t2)return n6;
else if (t2<h8&&h8<t2+w6)return n8;
}else if (l2<o2&&o2<l2+w6){
if (r8<h8&&h8<t2)return n7;
else if (t2<h8&&h8<t2+w6)return g0;
}
return null;
}
this.GetCellFromPoint=function (f3,o2,h8,g0){
var t3=this.GetOffsetLeft(f3,g0,document.body);
var y9=this.GetOffsetTop(f3,g0,document.body);
if (o2<t3||h8<y9){
return null;
}else {
var i2=g0.rows;
var ag7=null;
for (var i4=0;i4<i2.length;i4++){
var l5=i2[i4];
y9+=l5.offsetHeight;
if (h8<y9){
ag7=l5;
break ;
}
}
if (ag7!=null){
for (var i6=0;i6<ag7.cells.length;i6++){
var ag8=ag7.cells[i6];
t3+=ag8.offsetWidth;
if (o2<t3){
return ag8;
}
}
}
}
return null;
}
this.MoveSliderBar=function (f3,h4){
var o5=this.GetElementById(this.activePager,f3.id+"_slideBar");
var h1=(h4.clientX-this.GetOffsetLeft(f3,f3,document.body)+window.scrollX-8);
if (h1<f3.slideLeft)h1=f3.slideLeft;
if (h1>f3.slideRight)h1=f3.slideRight;
var o8=parseInt(this.activePager.getAttribute("totalPage"))-1;
var o4=parseInt(((h1-f3.slideLeft)/(f3.slideRight-f3.slideLeft))*o8)+1;
if (f3.style.position!="absolute"&&f3.style.position!="relative")
h1+=this.GetOffsetLeft(f3,f3,document.body)
o5.style.left=h1+"px";
return o4;
}
this.MouseMove=function (event){
if (window.fpPostOn!=null)return ;
event=this.GetEvent(event);
var q5=this.GetTarget(event);
if (q5!=null&&q5.tagName=="scrollbar")
return ;
if (q5.parentNode!=null&&q5.parentNode.getAttribute("previewrow"))
return ;
var f3=this.GetSpread(q5,true);
if (f3!=null&&this.dragSlideBar)
{
if (this.activePager!=null){
var o4=this.MoveSliderBar(f3,event);
var ag9=this.GetElementById(this.activePager,f3.id+"_posIndicator");
ag9.innerHTML=this.activePager.getAttribute("pageText")+o4;
}
return ;
}
if (this.working)f3=this.GetSpread(this.activeElement);
if (f3==null||(!this.working&&this.HitCommandBar(q5)))return ;
if (f3.getAttribute("OperationMode")=="ReadOnly")return ;
var l3=this.IsXHTML(f3);
if (this.working){
if (this.dragCol!=null&&this.dragCol>=0){
var aa9=this.GetMovingCol(f3);
if (aa9!=null){
if (aa9.style.display=="none")aa9.style.display="";
if (f3.style.position!="absolute"&&f3.style.position!="relative"){
aa9.style.top=""+(event.clientY+window.scrollY)+"px";
aa9.style.left=""+(event.clientX+window.scrollX+5)+"px";
}else {
aa9.style.top=""+(event.clientY-this.GetOffsetTop(f3,f3,document.body)+window.scrollY)+"px";
aa9.style.left=""+(event.clientX-this.GetOffsetLeft(f3,f3,document.body)+window.scrollX+5)+"px";
}
}
var g0=this.GetViewport(f3);
var ah0=document.body;
var ah1=this.GetGroupBar(f3);
var h1=-1;
var o2=event.clientX;
var y9=0;
var t3=0;
if (f3.style.position!="absolute"&&f3.style.position!="relative"){
y9=this.GetOffsetTop(f3,f3,document.body)-g0.parentNode.scrollTop;
t3=this.GetOffsetLeft(f3,f3,document.body)-g0.parentNode.scrollLeft;
o2+=Math.max(document.body.scrollLeft,document.documentElement.scrollLeft);
}else {
o2-=(this.GetOffsetLeft(f3,f3,document.body)-Math.max(document.body.scrollLeft,document.documentElement.scrollLeft));
}
var ah2=false;
var l3=this.IsXHTML(f3);
var ah3=l3?document.body.parentNode.scrollTop:document.body.scrollTop;
var o3=document.getElementById(f3.id+"_titleBar");
if (o3)ah3-=o3.parentNode.parentNode.offsetHeight;
if (this.GetPager1(f3)!=null)ah3-=this.GetPager1(f3).offsetHeight;
if (ah1!=null&&event.clientY<this.GetOffsetTop(f3,f3,document.body)-g0.parentNode.scrollTop+ah1.offsetHeight-ah3){
if (f3.style.position!="absolute"&&f3.style.position!="relative")
t3=this.GetOffsetLeft(f3,f3,document.body);
y9+=10;
ah2=true;
var ab2=ah1.getElementsByTagName("TABLE")[0];
if (ab2!=null){
for (var f5=0;f5<ab2.rows[0].cells[0].childNodes.length;f5++){
var k7=ab2.rows[0].cells[0].childNodes[f5].offsetWidth;
if (k7==null)continue ;
if (t3<=o2&&o2<t3+k7){
h1=f5;
break ;
}
t3+=k7;
}
}
if (h1==-1&&o2>=t3)h1=-2;
f3.targetCol=h1;
}else {
if (f3.style.position=="absolute"||f3.style.position=="relative")
t3=-g0.parentNode.scrollLeft;
if (this.GetRowHeader(f3)!=null)t3+=this.GetRowHeader(f3).offsetWidth;
if (ah1!=null)y9+=ah1.offsetHeight;
if (o2<t3){
h1=0;
}else {
var ab8=f3.selectedCols.context;
if (ab8){
for (var f5=0;f5<ab8.length;f5++){
if (ab8[f5].left+t3<=o2&&o2<ab8[f5].left+t3+ab8[f5].width){
h1=f5;
}
}
if (this.IsColSelected(f3,h1)){
while (this.IsColSelected(f3,h1)&&this.IsColSelected(f3,h1-1))h1--;
}else {
if (this.IsColSelected(f3,h1-1))h1++;
}
if (h1<0)h1=0;
if (h1>=ab8.length)h1=ab8.length-1;
t3+=ab8[h1].left;
}
}
t3-=5;
var ah4=parseInt(this.GetSheetColIndex(f3,h1));
if (ah4<0)ah4=h1;
f3.targetCol=ah4;
}
if (o3)y9+=o3.parentNode.parentNode.offsetHeight;
if (this.GetPager1(f3)!=null)y9+=this.GetPager1(f3).offsetHeight;
var ag9=this.GetPosIndicator(f3);
ag9.style.left=""+t3+"px";
ag9.style.top=""+y9+"px";
if (ah1!=null&&ah2&&ah1.getElementsByTagName("TABLE").length==0){
ag9.style.display="none";
}else {
if (ah2||f3.allowColMove)
ag9.style.display="";
else 
ag9.style.display="none";
}
var k0=this.GetParent(this.GetViewport(f3));
if (k0!=null){
var ah5=this.GetOffsetLeft(f3,f3,document.body)+k0.offsetLeft+k0.offsetWidth-20;
var ah6=0;
var n8=this.GetViewport2(f3);
if (n8!=null){
ah6=n8.offsetWidth;
ah5+=ah6;
}
if (event.clientX>ah5){
k0.scrollLeft=k0.scrollLeft+10;
this.ScrollView(f3);
this.UpdatePostbackData(f3);
}else if (event.clientX<this.GetOffsetLeft(f3,f3,document.body)+k0.offsetLeft+ah6+5){
k0.scrollLeft=k0.scrollLeft-10;
this.ScrollView(f3);
this.UpdatePostbackData(f3);
}
}
return ;
}
if (this.c3==null&&this.c4==null){
if (f3.e0!=null){
var k0=this.GetParent(this.GetViewport(f3));
if (k0!=null){
var m6=this.IsStaticPos(f3);
var ah7=-Math.max(document.body.parentNode.scrollTop,document.body.scrollTop);
var ah8=-Math.max(document.body.parentNode.scrollLeft,document.body.scrollLeft);
if (!m6||f3.frzRows>0||f3.frzCols>0){
ah7+=this.GetOffsetTop(f3,f3,document.body);
ah8+=this.GetOffsetLeft(f3,f3,document.body);
}
var j6=(this.IsChild(f3.e0,this.GetViewport(f3))||this.IsChild(f3.e0,this.GetViewport2(f3)));
if (j6){
var v1=k0.offsetTop+k0.offsetHeight-10+ah7;
var ah9=0;
var n7=this.GetViewport1(f3);
if (n7!=null){
ah9=this.GetViewport1(f3).offsetHeight;
v1+=ah9;
}
if (event.clientY>v1){
k0.scrollTop=k0.scrollTop+10;
this.ScrollView(f3);
}else if (event.clientY<k0.offsetTop+ah9+5+ah7){
k0.scrollTop=k0.scrollTop-10;
this.ScrollView(f3);
}
}
var j6=(this.IsChild(f3.e0,this.GetViewport(f3))||this.IsChild(f3.e0,this.GetViewport1(f3)));
if (j6){
var ah5=k0.offsetLeft+k0.offsetWidth-20+ah8;
var ah6=0;
var n8=this.GetViewport2(f3);
if (n8!=null){
ah6=n8.offsetWidth;
ah5+=ah6;
}
if (event.clientX>ah5){
k0.scrollLeft=k0.scrollLeft+10;
this.ScrollView(f3);
}else if (event.clientX<k0.offsetLeft+ah6+5+ah8){
k0.scrollLeft=k0.scrollLeft-10;
this.ScrollView(f3);
}
}
}
var i7=this.GetCell(q5,null,event);
if (i7==null&&q5!=null){
var g0=this.GetViewportFromPoint(f3,event.clientX,event.clientY);
if (g0!=null)
i7=this.GetCellFromPoint(f3,event.clientX,event.clientY,g0);
}
if (i7!=null&&i7!=f3.e1){
var j5=this.GetOperationMode(f3);
if (j5!="MultiSelect"){
if (!this.InColHeader(f3,i7)&&(j5=="SingleSelect"||j5=="RowMode")){
var i8=this.GetRowFromCell(f3,i7);
var ai0=this.GetColFromCell(f3,i7);
var ac9=this.FireActiveCellChangingEvent(f3,i8,ai0);
if (ac9)return ;
this.ClearSelection(f3);
this.UpdateAnchorCell(f3,i8,ai0);
this.SelectRow(f3,i8,1,true,true);
this.FireActiveCellChangedEvent(f3,i8,ai0);
}else {
if (j5!="SingleSelect"&&j5!="RowMode"&&!(j5=="Normal"&&this.GetSelectionPolicy(f3)=="Single")&&!f3.getAttribute("LayoutMode")){
this.Select(f3,f3.e0,i7);
this.SyncColSelection(f3);
}
}
f3.e1=i7;
}
}
}
}else if (this.c3!=null){
var ai1=event.clientX-this.c5;
var ab9=parseInt(this.c3.width)+ai1;
var y8=0;
var ai2=(ab9>y8);
if (ai2){
if (f3.frzRows>0||f3.frzCols>0){
var j0=0;
if (!l3)j0+=parseInt(f3.style.borderWidth);
var ai3=this.GetViewport(f3).parentNode;
if (f3.style.position!="relative"&&f3.style.position!="absolute")
f3.sizeBar.style.left=(event.clientX-this.GetOffsetLeft(ai3.offsetParent,document.body)-j0+window.scrollX)+"px";
else 
f3.sizeBar.style.left=(event.clientX-this.GetOffsetLeft(f3,f3,document.body)-j0+window.scrollX)+"px";
}else {
this.c3.width=ab9;
var l8=parseInt(this.c3.getAttribute("index"));
if (this.IsChild(this.c3,this.GetFrozColHeader(f3)))
this.SetWidthFix(this.GetFrozColHeader(f3),l8,ab9);
else 
this.SetWidthFix(this.GetColHeader(f3),l8-f3.frzCols,ab9);
this.c5=event.clientX;
}
}
}else if (this.c4!=null){
var ai1=event.clientY-this.c6;
var ai4=parseInt(this.c4.style.height)+ai1;
var y8=0;
var ai2=(y8<ai4);
if (ai2){
if (f3.frzRows>0||f3.frzCols>0){
var j0=0;
if (!l3)j0+=parseInt(f3.style.borderWidth);
if (f3.style.position=="relative"||f3.style.position=="absolute")
f3.sizeBar.style.top=(event.clientY-this.GetOffsetTop(f3,f3,document.body)-j0+window.scrollY)+"px";
else 
f3.sizeBar.style.top=(event.clientY-j0+window.scrollY)+"px";
}else {
this.c4.style.height=""+(parseInt(this.c4.style.height)+ai1)+"px";
this.c6=event.clientY;
}
}
}
}else {
this.activeElement=q5;
if (this.activeElement==null||this.GetSpread(this.activeElement)!=f3)return ;
var q5=this.GetSizeColumn(f3,this.activeElement,event);
if (q5!=null){
this.c3=q5;
this.activeElement.style.cursor=this.GetResizeCursor(false);
}else {
var q5=this.GetSizeRow(f3,this.activeElement,event);
if (q5!=null){
this.c4=q5;
if (this.activeElement!=null&&this.activeElement.style!=null)this.activeElement.style.cursor=this.GetResizeCursor(true);
}else {
if (this.activeElement!=null&&this.activeElement.style!=null){
var i7=this.GetCell(this.activeElement);
if (i7!=null&&this.IsHeaderCell(f3,i7)){
if (this.activeElement.getAttribute("FpSpread")=="rowpadding"||this.activeElement.getAttribute("ControlType")=="chgrayarea")
this.activeElement.style.cursor=this.GetgrayAreaCursor(f3);
else 
this.activeElement.style.cursor="default";
}else {
if (this.activeElement!=null&&this.activeElement.style!=null&&(this.activeElement.getAttribute("FpSpread")=="rowpadding"||this.activeElement.getAttribute("ControlType")=="chgrayarea"))
this.activeElement.style.cursor=this.GetgrayAreaCursor(f3);
}
}
}
}
}
}
this.GetgrayAreaCursor=function (f3){
if (f3.d2!=null&&f3.d2.style.cursor!=null){
if (f3.d2.style.cursor=="auto")
f3.d2.style.cursor="default";
return f3.d2.style.cursor;
}
else return "default";
}
this.GetResizeCursor=function (k3){
if (k3){
return "n-resize";
}else {
return "w-resize";
}
}
this.HitCommandBar=function (q5){
var h1=q5;
var f3=this.GetTopSpread(this.GetSpread(h1,true));
if (f3==null)return false;
var s9=this.GetCommandBar(f3);
while (h1!=null&&h1!=f3){
if (h1==s9)return true;
h1=h1.parentNode;
}
return false;
}
this.OpenWaitMsg=function (f3){
var j6=document.getElementById(f3.id+"_waitmsg");
if (j6==null)return ;
var k7=f3.offsetWidth;
var f7=f3.offsetHeight;
var k4=this.CreateTestBox(f3);
k4.style.fontFamily=j6.style.fontFamily;
k4.style.fontSize=j6.style.fontSize;
k4.style.fontWeight=j6.style.fontWeight;
k4.style.fontStyle=j6.style.fontStyle;
k4.innerHTML=j6.innerHTML;
j6.style.width=""+(k4.offsetWidth+2)+"px";
var af3=Math.max(10,(k7-parseInt(j6.style.width))/2);
var h1=Math.max(10,(f7-parseInt(j6.style.height))/2);
if (f3.style.position!="absolute"&&f3.style.position!="relative"){
af3+=this.GetOffsetLeft(f3,f3,document.body);
h1+=this.GetOffsetTop(f3,f3,document.body);
}
j6.style.top=""+h1+"px";
j6.style.left=""+af3+"px";
j6.style.display="block";
}
this.CloseWaitMsg=function (f3){
var j6=document.getElementById(f3.id+"_waitmsg");
if (j6==null)return ;
j6.style.display="none";
}
this.MouseDown=function (event){
if (window.fpPostOn!=null)return ;
event=this.GetEvent(event);
var q5=this.GetTarget(event);
var f3=this.GetSpread(q5,true);
f3.mouseY=event.clientY;
var ai5=this.GetPageActiveSpread();
if (this.GetViewport(f3)==null)return ;
if (f3!=null&&q5.parentNode!=null&&q5.parentNode.getAttribute("name")==f3.id+"_slideBar"){
if (this.IsChild(q5,this.GetPager1(f3)))
this.activePager=this.GetPager1(f3);
else if (this.IsChild(q5,this.GetPager2(f3)))
this.activePager=this.GetPager2(f3);
if (this.activePager!=null){
var q8=true;
if (this.editing)q8=this.EndEdit(f3);
if (q8){
this.UpdatePostbackData(f3);
this.dragSlideBar=true;
}
}
return this.CancelDefault(event);
}
if (f3!=null)f3.working=false;
if (this.GetOperationMode(f3)=="ReadOnly")return ;
var l3=false;
if (f3!=null)l3=this.IsXHTML(f3);
if (this.editing&&f3.getAttribute("mcctCellType")!="true"){
var h1=this.GetCell(q5);
if (h1!=f3.e0){
var q8=this.EndEdit();
if (!q8)return ;
}else 
return ;
}
if (q5==this.GetParent(this.GetViewport(f3))){
if (this.GetTopSpread(ai5)!=f3){
this.SetActiveSpread(event);
}
return ;
}
var ai6=(ai5==f3);
this.SetActiveSpread(event);
ai5=this.GetPageActiveSpread();
if (this.HitCommandBar(q5))return ;
if (event.button==2)return ;
if (this.IsChild(q5,this.GetGroupBar(f3))){
var i9=parseInt(q5.id.replace(f3.id+"_group",""));
if (!isNaN(i9)){
this.InitMovingCol(f3,i9,true,q5);
this.working=true;
f3.dragFromGroupbar=true;
this.CancelDefault(event);
return ;
}
}
if (this.IsInRowEditTemplate(f3,q5)){
return ;
}
this.c3=this.GetSizeColumn(f3,q5,event);
if (this.c3!=null){
this.working=true;
this.c5=this.c6=event.clientX;
if (this.c3.style!=null)this.c3.style.cursor=this.GetResizeCursor(false);
this.activeElement=q5;
if (f3.frzRows>0||f3.frzCols>0){
var ai3=this.GetViewport0(f3);
if (ai3==null)ai3=this.GetViewport1(f3);
if (ai3==null)ai3=this.GetViewport(f3);
ai3=ai3.parentNode;
var r8=0;
if (f3.style.position!="relative"&&f3.style.position!="absolute")r8=this.GetOffsetTop(f3,f3,document.body);
if (this.GetColHeader(f3)!=null)
f3.sizeBar.style.top=""+(r8+this.GetOffsetTop(f3,ai3,f3)-this.GetColHeader(f3).offsetHeight)+"px";
else 
f3.sizeBar.style.top=""+(r8+this.GetOffsetTop(f3,ai3,f3))+"px";
var j0=0;
if (!l3)j0+=parseInt(f3.style.borderWidth);
if (f3.style.position!="relative"&&f3.style.position!="absolute")
f3.sizeBar.style.left=(this.c5-this.GetOffsetLeft(ai3.offsetParent,document.body)-j0+window.scrollX)+"px";
else 
f3.sizeBar.style.left=(this.c5-this.GetOffsetLeft(f3,f3,document.body)-j0+window.scrollX)+"px";
var ai7=0;
if (this.GetViewport0(f3)!=null)ai7=this.GetViewport0(f3).parentNode.offsetHeight;
if (ai7==0&&this.GetViewport1(f3)!=null)ai7=this.GetViewport1(f3).parentNode.offsetHeight;
if (this.GetViewport(f3)!=null)ai7+=this.GetViewport(f3).parentNode.offsetHeight;
if (this.GetColHeader(f3)!=null)ai7+=this.GetColHeader(f3).offsetHeight;
f3.sizeBar.style.height=""+ai7+"px";
f3.sizeBar.style.width="2px";
}
}else {
this.c4=this.GetSizeRow(f3,q5,event);
if (this.c4!=null){
this.working=true;
this.c5=this.c6=event.clientY;
this.c4.style.cursor=this.GetResizeCursor(true);
this.activeElement=q5;
f3.working=true;
if (f3.frzRows>0||f3.frzCols>0){
var ai3=this.GetViewport0(f3);
if (ai3==null)ai3=this.GetViewport1(f3);
if (ai3==null)ai3=this.GetViewport(f3);
ai3=ai3.parentNode;
var j0=0;
if (!l3)j0+=parseInt(f3.style.borderWidth);
if (f3.style.position=="relative"||f3.style.position=="absolute"){
f3.sizeBar.style.left="0px";
f3.sizeBar.style.top=(this.c6-this.GetOffsetTop(f3,f3,document.body)-j0+window.scrollY)+"px";
}else {
f3.sizeBar.style.left=""+this.GetOffsetLeft(f3,f3,document.body)+"px";
f3.sizeBar.style.top=(this.c6-j0+window.scrollY)+"px";
}
f3.sizeBar.style.height="2px";
f3.sizeBar.style.width=""+f3.offsetWidth+"px";
}
}else {
var ai8=this.GetCell(q5,null,event);
if (ai8==null){
var d3=this.GetCorner(f3);
if (d3!=null&&this.IsChild(q5,d3)){
if (this.GetOperationMode(f3)=="Normal")
this.SelectTable(f3,true);
}
return ;
}
var ai0=this.GetColFromCell(f3,ai8);
if (ai8.parentNode.getAttribute("FpSpread")=="ch"&&ai0>=this.GetColCount(f3))return ;
if (ai8.parentNode.getAttribute("FpSpread")=="rh"&&this.IsChildSpreadRow(f3,this.GetViewport(f3),ai8.parentNode.rowIndex))return ;
if (ai8.parentNode.getAttribute("FpSpread")=="ch"&&this.GetOperationMode(f3)!="Normal"){
if (f3.allowColMove||f3.allowGroup){
if (f3.getAttribute("LayoutMode"))ai0=parseInt(ai8.getAttribute("col"));
this.InitMovingCol(f3,ai0);
}
this.working=true;
this.activeElement=q5;
return this.CancelDefault(event);
}
if (ai8.parentNode.getAttribute("FpSpread")=="ch"&&(this.GetOperationMode(f3)=="RowMode"||this.GetOperationMode(f3)=="SingleSelect"||this.GetOperationMode(f3)=="ExtendedSelect")){
if (!f3.allowColMove&&!f3.allowGroup)
return ;
}else {
var q6=this.FireActiveCellChangingEvent(f3,this.GetRowFromCell(f3,ai8),ai0,ai8.parentNode.getAttribute("row"));
if (q6)return ;
var p6=this.GetOperationMode(f3);
var g2=this.GetTopSpread(f3);
if (!event.ctrlKey||f3.getAttribute("multiRange")!="true"){
if (p6!="MultiSelect"){
if (!(
(f3.allowColMove||f3.allowGroup)&&ai8.parentNode.getAttribute("FpSpread")=="ch"&&
p6=="Normal"&&(f3.getAttribute("SelectionPolicy")=="Range"||f3.getAttribute("SelectionPolicy")=="MultiRange")&&
f3.selectedCols.length!=0&&this.IsColSelected(f3,ai0)
))
this.ClearSelection(f3);
}
}else {
if (p6!="ExtendedSelect"&&p6!="MultiSelect"){
if (f3.e0!=null)this.PaintSelectedCell(f3,f3.e0,true);
}
}
}
f3.e0=ai8;
var i7=f3.e0;
var r0=this.GetParent(this.GetViewport(f3));
if (r0!=null&&!this.IsControl(q5)&&(q5!=null&&q5.tagName!="scrollbar")){
if (this.IsChild(i7,r0)&&i7.offsetLeft+i7.offsetWidth>r0.scrollLeft+r0.clientWidth){
r0.scrollLeft=i7.offsetLeft+i7.offsetWidth-r0.clientWidth;
}
if ((this.IsChild(i7,r0)||this.IsChild(i7,this.GetViewport2(f3)))&&i7.offsetTop+i7.offsetHeight>r0.scrollTop+r0.clientHeight&&i7.offsetHeight<r0.clientHeight){
r0.scrollTop=i7.offsetTop+i7.offsetHeight-r0.clientHeight;
}
if (this.IsChild(i7,this.GetViewport(f3))){
if (i7.offsetTop<r0.scrollTop){
r0.scrollTop=i7.offsetTop;
}
if (i7.offsetLeft<r0.scrollLeft){
r0.scrollLeft=i7.offsetLeft;
}
}
this.ScrollView(f3);
}
if (ai8.parentNode.getAttribute("FpSpread")!="ch")this.SetActiveRow(f3,this.GetRowKeyFromCell(f3,f3.e0));
if (ai8.parentNode.getAttribute("FpSpread")=="rh")
this.SetActiveCol(f3,0);
else {
this.SetActiveCol(f3,f3.getAttribute("LayoutMode")?this.GetColKeyFromCell2(f3,f3.e0):this.GetColKeyFromCell(f3,f3.e0));
}
var p6=this.GetOperationMode(f3);
if (f3.e0.parentNode.getAttribute("FpSpread")=="r"){
if (p6=="ExtendedSelect"||p6=="MultiSelect"){
var ai9=this.IsRowSelected(f3,this.GetRowFromCell(f3,f3.e0));
if (ai9)
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,false,true);
else 
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,true,true);
}
else if (p6=="RowMode"||p6=="SingleSelect")
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,true,true);
else {
this.SelectRange(f3,this.GetRowFromCell(f3,f3.e0),ai0,1,1,true);
}
f3.e4=this.GetRowFromCell(f3,f3.e0);
f3.e5=ai0;
}else if (f3.e0.parentNode.getAttribute("FpSpread")=="ch"){
if (q5.tagName=="INPUT"||q5.tagName=="TEXTAREA"||q5.tagName=="SELECT")
return ;
var w3=ai0;
if (f3.allowColMove||f3.allowGroup)
{
if (p6=="Normal"&&(f3.getAttribute("SelectionPolicy")=="Range"||f3.getAttribute("SelectionPolicy")=="MultiRange")){
if (this.IsColSelected(f3,w3))this.InitMovingCol(f3,w3);
else this.SelectColumn(f3,w3,1,true);
}else {
if (f3.getAttribute("LayoutMode"))
w3=parseInt(f3.e0.getAttribute("col"));
if (p6=="Normal"||p6=="ReadOnly")
this.SelectColumn(f3,w3,1,true);
else f3.selectedCols.push(w3);
this.InitMovingCol(f3,w3);
}
}else {
if (p6=="Normal"||p6=="ReadOnly"){
if (f3.getAttribute("LayoutMode"))
w3=parseInt(f3.e0.getAttribute("col"));
this.SelectColumn(f3,w3,1,true);
}
else 
return ;
}
}else if (f3.e0.parentNode.getAttribute("FpSpread")=="rh"){
if (q5.tagName=="INPUT"||q5.tagName=="TEXTAREA"||q5.tagName=="SELECT")
return ;
if (p6=="ExtendedSelect"||p6=="MultiSelect"){
if (this.IsRowSelected(f3,this.GetRowFromCell(f3,f3.e0)))
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,false,true);
else 
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,true,true);
}else {
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,true);
}
}
f3.e1=f3.e0;
if (f3.e0!=null){
f3.e2=this.GetRowFromCell(f3,f3.e0);
f3.e3=ai0;
}
this.activeElement=q5;
this.working=true;
if (f3.e0!=null){
var h4=this.CreateEvent("ActiveCellChanged");
h4.cmdID=f3.id;
h4.Row=h4.row=this.GetSheetIndex(f3,this.GetRowFromCell(f3,f3.e0));
h4.Col=h4.col=ai0;
if (f3.getAttribute("LayoutMode"))
h4.InnerRow=h4.innerRow=f3.e0.parentNode.getAttribute("row");
this.FireEvent(f3,h4);
}
}
}
this.EnableButtons(f3);
if (this.dragCol!=null&&this.dragCol>=0&&!this.IsControl(q5))this.Focus(f3);
if (!this.editing&&this.c4==null&&this.c3==null){
if (f3.e0!=null&&this.IsChild(f3.e0,f3)&&!this.IsHeaderCell(this.GetCell(q5))){
var j8=this.GetEditor(f3.e0);
if (j8!=null){
if (j8.type=="submit")this.SaveData(f3);
this.editing=(j8.type!="button"&&j8.type!="submit");
this.b7=j8;
this.b8=this.GetEditorValue(j8);
if (j8.focus)j8.focus();
}
}
}
if (!this.IsControl(q5)){
if (f3!=null)this.UpdatePostbackData(f3);
return this.CancelDefault(event);
}
}
this.GetMovingCol=function (f3){
var aa9=document.getElementById(f3.id+"movingCol");
if (aa9==null){
aa9=document.createElement("DIV");
aa9.style.display="none";
aa9.style.position="absolute";
aa9.style.top="0px";
aa9.style.left="0px";
aa9.id=f3.id+"movingCol";
aa9.align="center";
f3.insertBefore(aa9,null);
if (f3.getAttribute("DragColumnCssClass")!=null)
aa9.className=f3.getAttribute("DragColumnCssClass");
else 
aa9.style.border="1px solid black";
aa9.style.MozOpacity=0.50;
}
return aa9;
}
this.IsControl=function (h1){
return (h1!=null&&(h1.tagName=="INPUT"||h1.tagName=="TEXTAREA"||h1.tagName=="SELECT"||h1.tagName=="OPTION"));
}
this.EnableButtons=function (f3){
var u8=this.GetCellType(f3.e0);
var p4=this.GetSelection(f3);
var p5=p4.lastChild;
var x5=f3.getAttribute("OperationMode");
var aj0=x5=="ReadOnly"||x5=="SingleSelect"||u8=="readonly";
if (!aj0){
aj0=this.AnyReadOnlyCell(f3,p5);
}
if (aj0){
var h0=this.GetCmdBtn(f3,"Copy");
this.UpdateCmdBtnState(h0,p5==null);
var g7=this.c8;
h0=this.GetCmdBtn(f3,"Paste");
this.UpdateCmdBtnState(h0,(p5==null||g7==null));
h0=this.GetCmdBtn(f3,"Clear");
this.UpdateCmdBtnState(h0,true);
}else {
var h0=this.GetCmdBtn(f3,"Copy");
this.UpdateCmdBtnState(h0,p5==null);
var g7=this.c8;
h0=this.GetCmdBtn(f3,"Paste");
this.UpdateCmdBtnState(h0,(p5==null||g7==null));
h0=this.GetCmdBtn(f3,"Clear");
this.UpdateCmdBtnState(h0,p5==null);
}
}
this.CellClicked=function (i7){
var f3=this.GetSpread(i7);
if (f3!=null){
this.SaveData(f3);
}
}
this.UpdateCmdBtnState=function (h0,disabled){
if (h0==null)return ;
if (h0.tagName=="INPUT"){
var h1=h0.disabled;
if (h1==disabled)return ;
h0.disabled=disabled;
}else {
var h1=h0.getAttribute("disabled");
if (h1==disabled)return ;
h0.setAttribute("disabled",disabled);
}
if (h0.tagName=="IMG"){
var aj1=h0.getAttribute("disabledImg");
if (disabled&&aj1!=null&&aj1!=""){
if (h0.src.indexOf(aj1)<0)h0.src=aj1;
}else {
var aj2=h0.getAttribute("enabledImg");
if (h0.src.indexOf(aj2)<0)h0.src=aj2;
}
}
}
this.MouseUp=function (event){
if (window.fpPostOn!=null)return ;
event=this.GetEvent(event);
var q5=this.GetTarget(event);
var f3=this.GetSpread(q5,true);
if (f3==null&&!this.working&&!this.dragSlideBar){
return ;
}
if (this.dragSlideBar){
this.dragSlideBar=false;
if (f3==null)f3=this.GetSpread(this.activePager,true);
if (this.activePager!=null){
var o4=this.MoveSliderBar(f3,event)-1;
this.activePager=null;
this.GotoPage(f3,o4);
}
return ;
}
if (this.working&&(this.c3!=null||this.c4!=null)){
if (this.c3!=null)
f3=this.GetSpread(this.c3);
else 
f3=this.GetSpread(this.c4);
}
if (this.working&&this.c3==null&&this.c4==null)
f3=this.GetPageActiveSpread();
if (f3==null)return ;
if (this.GetViewport(f3)==null)return ;
var x5=this.GetOperationMode(f3);
if (x5=="ReadOnly")return ;
var j6=true;
if (this.working){
this.working=false;
if (this.dragCol!=null&&this.dragCol>=0){
var aj3=(this.IsChild(q5,this.GetGroupBar(f3))||q5==this.GetGroupBar(f3));
if (!aj3&&this.GetGroupBar(f3)!=null){
var aj4=event.clientX;
var aj5=event.clientY;
var t3=this.GetOffsetLeft(f3,f3,document.body);
var y9=this.GetOffsetTop(f3,f3,document.body);
var aj6=this.GetGroupBar(f3).offsetWidth;
var aj7=this.GetGroupBar(f3).offsetHeight;
var s8=window.scrollX;
var s7=window.scrollY;
var o3=document.getElementById(f3.id+"_titleBar");
if (o3)s7-=o3.parentNode.parentNode.offsetHeight;
if (this.GetPager1(f3)!=null)s7-=this.GetPager1(f3).offsetHeight;
aj3=(t3<=s8+aj4&&s8+aj4<=t3+aj6&&y9<=s7+aj5&&s7+aj5<=y9+aj7);
}
if (f3.dragFromGroupbar){
if (aj3){
if (f3.targetCol>0)
this.Regroup(f3,this.dragCol,parseInt((f3.targetCol+1)/2));
else 
this.Regroup(f3,this.dragCol,f3.targetCol);
}else {
this.Ungroup(f3,this.dragCol,f3.targetCol);
}
}else {
if (aj3){
if (f3.allowGroup){
if (f3.targetCol>0)
this.Group(f3,this.dragCol,parseInt((f3.targetCol+1)/2));
else 
this.Group(f3,this.dragCol,f3.targetCol);
}
}else if (f3.allowColMove){
if (f3.targetCol!=null){
var h4=this.CreateEvent("ColumnDragMove");
h4.cancel=false;
h4.col=f3.selectedCols;
this.FireEvent(f3,h4);
if (!h4.cancel){
this.MoveCol(f3,this.dragCol,f3.targetCol);
var h4=this.CreateEvent("ColumnDragMoveCompleted");
h4.col=f3.selectedCols;
this.FireEvent(f3,h4);
}
}
}
}
var aa9=this.GetMovingCol(f3);
if (aa9!=null)
aa9.style.display="none";
this.dragCol=-1;
this.dragViewCol=-1;
var ag9=this.GetPosIndicator(f3);
if (ag9!=null)
ag9.style.display="none";
f3.dragFromGroupbar=false;
f3.targetCol=null;
this.c3=this.c4=null;
}
if (this.c3!=null){
if (f3.sizeBar!=null)f3.sizeBar.style.left="-400px";
j6=false;
var ai1=event.clientX-this.c5;
var ab9=parseInt(this.c3.width);
var aj8=ab9;
if (isNaN(ab9))ab9=0;
ab9+=ai1;
if (ab9<1)ab9=1;
var l8=parseInt(this.c3.getAttribute("index"));
var w7=this.GetColGroup(this.GetViewport(f3));
if (this.IsChild(this.c3,this.GetFrozColHeader(f3))){
w7=this.GetColGroup(this.GetViewport0(f3));
if (w7==null)w7=this.GetColGroup(this.GetViewport2(f3));
}
if (w7!=null&&w7.childNodes.length>0){
if (this.IsChild(this.c3,this.GetColHeader(f3)))
aj8=parseInt(w7.childNodes[l8-f3.frzCols].width);
else 
aj8=parseInt(w7.childNodes[l8].width);
}else {
aj8=1;
}
if (this.GetViewport(f3).rules!="rows"){
if (l8==parseInt(this.colCount)-1)aj8-=1;
}
if (ab9!=aj8&&event.clientX!=this.c6){
this.SetColWidth(f3,l8,ab9,aj8);
var h4=this.CreateEvent("ColWidthChanged");
h4.col=l8;
h4.width=ab9;
this.FireEvent(f3,h4);
}
this.ScrollView(f3);
this.PaintFocusRect(f3);
}else if (this.c4!=null){
if (f3.sizeBar!=null){f3.sizeBar.style.left="-400px";f3.sizeBar.style.width="2px";}
j6=false;
var ai1=event.clientY-this.c6;
var ai4=this.c4.offsetHeight+ai1;
if (ai4<1){
ai4=1;
ai1=1-this.c4.offsetHeight;
}
this.c4.style.height=""+ai4+"px";
this.c4.style.cursor="auto";
var k0=null;
if (this.IsChild(this.c4,this.GetFrozRowHeader(f3))){
k0=this.GetViewport1(f3);
}else {
k0=this.GetViewport(f3);
}
if (k0.rows.length>=2&&k0.cellSpacing=="0"&&f3.frzRow==0){
if (this.c4.rowIndex==0)
k0.rows[0].style.height=""+(parseInt(this.c4.style.height)-1)+"px";
else 
if (this.c4.rowIndex==k0.rows.length-1)
k0.rows[this.c4.rowIndex].style.height=""+(parseInt(this.c4.style.height)+1)+"px";
else 
k0.rows[this.c4.rowIndex].style.height=this.c4.style.height;
}else {
k0.rows[this.c4.rowIndex].style.height=""+(this.c4.offsetHeight-k0.rows[0].offsetTop)+"px";
}
var aj9=this.GetViewport2(f3);
if (this.IsChild(this.c4,this.GetFrozRowHeader(f3))){
aj9=this.GetViewport0(f3);
}
if (aj9!=null)
aj9.rows[this.c4.rowIndex].style.height=k0.rows[this.c4.rowIndex].style.height;
if (this.IsChild(this.c4,this.GetFrozRowHeader(f3))){
this.GetFrozRowHeader(f3).parentNode.parentNode.parentNode.style.posHeight+=ai1;
}
var t2=this.AddRowInfo(f3,this.c4.getAttribute("FpKey"));
if (t2!=null){
this.SetRowHeight(f3,t2,parseInt(this.c4.style.height));
}
if (this.c5!=event.clientY){
var h4=this.CreateEvent("RowHeightChanged");
h4.row=this.GetRowFromCell(f3,this.c4.cells[0]);
h4.height=this.c4.offsetHeight;
this.FireEvent(f3,h4);
}
var k2=this.GetParentSpread(f3);
if (k2!=null)this.UpdateRowHeight(k2,f3);
var g2=this.GetTopSpread(f3);
this.SizeAll(g2);
this.Refresh(g2);
this.ScrollView(f3);
this.PaintFocusRect(f3);
}else {
}
if (this.activeElement!=null){
this.activeElement=null;
}
}
if (j6)j6=!this.IsControl(q5);
if (j6&&this.HitCommandBar(q5))return ;
var ak0=false;
var p4=this.GetSelection(f3);
if (p4!=null){
var p5=p4.firstChild;
var j2=new this.Range();
if (p5!=null){
j2.row=this.GetRowByKey(f3,p5.getAttribute("row"));
j2.col=this.GetColByKey(f3,p5.getAttribute("col"));
j2.rowCount=parseInt(p5.getAttribute("rowcount"));
j2.colCount=parseInt(p5.getAttribute("colcount"));
}
switch (f3.e6){
case "":
var i2=this.GetViewport(f3).rows;
for (var f5=j2.row;f5<j2.row+j2.rowCount&&f5<i2.length;f5++){
if (i2[f5].cells.length>0&&i2[f5].cells[0].firstChild!=null&&i2[f5].cells[0].firstChild.nodeName!="#text"){
if (i2[f5].cells[0].firstChild.getAttribute("FpSpread")=="Spread"){
ak0=true;
break ;
}
}
}
break ;
case "c":
var k0=this.GetViewport(f3);
for (var f5=0;f5<k0.rows.length;f5++){
if (this.IsChildSpreadRow(f3,k0,f5)){
ak0=true;
break ;
}
}
break ;
case "r":
var k0=this.GetViewport(f3);
var w2=j2.rowCount;
for (var f5=j2.row;f5<j2.row+w2&&f5<k0.rows.length;f5++){
if (this.IsChildSpreadRow(f3,k0,f5)){
ak0=true;
break ;
}
}
}
}
if (ak0){
var h0=this.GetCmdBtn(f3,"Copy");
this.UpdateCmdBtnState(h0,true);
h0=this.GetCmdBtn(f3,"Paste");
this.UpdateCmdBtnState(h0,true);
h0=this.GetCmdBtn(f3,"Clear");
this.UpdateCmdBtnState(h0,true);
}
var g2=this.GetTopSpread(f3);
if (g2.style.position!="absolute"&&g2.style.position!="relative"){
var h2=document.getElementById(g2.id+"_textBox");
if (h2!=null){
h2.style.top=""+(window.scrollY)+"px";
h2.style.left=""+(window.scrollX)+"px";
}
}else {
var h2=document.getElementById(g2.id+"_textBox");
if (h2!=null&&this.GetCell(q5)!=null){
if (f3.e1!=null){
var ak1=this.GetViewportFromCell(f3,f3.e1).parentNode;
h2.style.top=""+(ak1.offsetTop+f3.e1.offsetTop-ak1.scrollTop)+"px";
h2.style.left=""+(ak1.offsetLeft+f3.e1.offsetLeft-ak1.scrollLeft)+"px";
}
else if (f3.e0!=null){
var ak1=this.GetViewportFromCell(f3,f3.e0).parentNode;
h2.style.top=""+(ak1.offsetTop+f3.e0.offsetTop-ak1.scrollTop)+"px";
h2.style.left=""+(ak1.offsetLeft+f3.e0.offsetLeft-ak1.scrollLeft)+"px";
}else {
h2.style.top="0px";
h2.style.left="0px";
}
}
}
if (j6&&q5!=this.GetViewport(f3).parentNode)this.Focus(f3);
}
this.UpdateRowHeight=function (k2,child){
var l5=child.parentNode;
while (l5!=null){
if (l5.tagName=="TR")break ;
l5=l5.parentNode;
}
var l3=this.IsXHTML(k2);
if (l5!=null){
var f4=l5.rowIndex;
if (this.GetRowHeader(k2)!=null){
var t0=0;
if (this.GetColHeader(child)!=null)t0=this.GetColHeader(child).offsetHeight;
if (this.GetRowHeader(child)!=null)t0+=this.GetRowHeader(child).offsetHeight;
var ak2=document.getElementById(child.id+"_titleBar");
if (ak2!=null)t0+=ak2.offsetHeight;
if (this.GetPager1(child)!=null)t0+=this.GetPager1(child).offsetHeight;
if (!l3)t0-=this.GetViewport(k2).cellSpacing;
if (this.GetViewport(k2).cellSpacing==0){
this.GetRowHeader(k2).rows[f4].style.height=""+(t0+1)+"px";
if (this.GetParentSpread(k2)!=null){
this.GetRowHeader(k2).parentNode.style.height=""+this.GetRowHeader(k2).offsetHeight+"px";
}
}
else 
this.GetRowHeader(k2).rows[f4].style.height=""+(t0+2)+"px";
this.GetViewport(k2).rows[f4].style.height=""+t0+"px";
child.style.height=""+t0+"px";
}
}
var ak3=this.GetParentSpread(k2);
if (ak3!=null)
this.UpdateRowHeight(ak3,k2);
}
this.MouseOut=function (){
if (!this.working&&this.c3!=null&&this.c3.style!=null)this.c3.style.cursor="auto";
}
this.KeyDown=function (f3,event){
if (window.fpPostOn!=null)return ;
if (!f3.ProcessKeyMap(event))return ;
if (event.keyCode==event.DOM_VK_SPACE&&f3.e0!=null){
var p6=this.GetOperationMode(f3);
if (p6=="MultiSelect"){
if (this.IsRowSelected(f3,this.GetRowFromCell(f3,f3.e0)))
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,false,true);
else 
this.SelectRow(f3,this.GetRowFromCell(f3,f3.e0),1,true,true);
return ;
}
}
var j8=false;
if (this.editing&&this.b7!=null){
var ak4=this.GetEditor(this.b7);
j8=(ak4!=null);
}
if (event.keyCode!=event.DOM_VK_LEFT&&event.keyCode!=event.DOM_VK_RIGHT&&event.keyCode!=event.DOM_VK_RETURN&&event.keyCode!=event.DOM_VK_TAB&&(this.editing&&!j8)&&this.b7.tagName=="SELECT")return ;
if (this.editing&&this.b7!=null&&this.b7.getAttribute("MccbId")){
var ak5=eval(this.b7.getAttribute("MccbId")+"_Obj");
if (event.altKey&&event.keyCode==event.DOM_VK_DOWN)return ;
if (ak5!=null&&ak5.getIsDrop!=null&&ak5.getIsDrop())return ;
}
switch (event.keyCode){
case event.DOM_VK_LEFT:
case event.DOM_VK_RIGHT:
if (j8){
var ak6=this.b7.getAttribute("FpEditor");
if (this.editing&&ak6=="ExtenderEditor"){
var ak7=FpExtender.Util.getEditor(this.b7);
if (ak7&&ak7.type!="text")this.EndEdit();
}
if (ak6!="RadioButton"&&ak6!="ExtenderEditor")this.EndEdit();
}
if (!this.editing){
this.NextCell(f3,event,event.keyCode);
}
break ;
case event.DOM_VK_UP:
case event.DOM_VK_DOWN:
case event.DOM_VK_RETURN:
if (this.b7!=null&&(this.b7.tagName=="TEXTAREA"||this.b7.tagName=="SELECT"))return ;
if (j8&&this.editing&&this.b7.getAttribute("FpEditor")=="ExtenderEditor"){
var ak8=this.b7.getAttribute("Extenders");
if (ak8&&ak8.indexOf("AutoCompleteExtender")!=-1)return ;
}
if (event.keyCode==event.DOM_VK_RETURN)this.CancelDefault(event);
if (this.editing){
var q8=this.EndEdit();
if (!q8)return ;
}
if (event.keyCode!=event.DOM_VK_RETURN)this.NextCell(f3,event,event.keyCode);
var g2=this.GetTopSpread(f3);
var h2=document.getElementById(g2.id+"_textBox");
if (event.DOM_VK_RETURN==event.keyCode)h2.focus();
break ;
case event.DOM_VK_TAB:
if (this.editing){
var q8=this.EndEdit();
if (!q8)return ;
}
var q7=this.GetProcessTab(f3);
var ak9=(q7=="true"||q7=="True");
if (ak9)this.NextCell(f3,event,event.keyCode);
break ;
case event.DOM_VK_SHIFT:
break ;
case event.DOM_VK_HOME:
case event.DOM_VK_END:
case event.DOM_VK_PAGE_UP:
case event.DOM_VK_PAGE_DOWN:
if (!this.editing){
this.NextCell(f3,event,event.keyCode);
}
break ;
default :
var f8=window.navigator.userAgent;
var z7=(f8.indexOf("Firefox/2.")>=0);
if (z7){
if (event.keyCode==67&&event.ctrlKey&&(!this.editing||j8))this.Copy(f3);
else if (event.keyCode==86&&event.ctrlKey&&(!this.editing||j8))this.Paste(f3);
else if (event.keyCode==88&&event.ctrlKey&&(!this.editing||j8))this.Clear(f3);
else if (!this.editing&&f3.e0!=null&&!this.IsHeaderCell(f3.e0)&&!event.ctrlKey&&!event.altKey){
this.StartEdit(f3,f3.e0);
}
}else {
if (event.charCode==99&&event.ctrlKey&&(!this.editing||j8))this.Copy(f3);
else if (event.charCode==118&&event.ctrlKey&&(!this.editing||j8))this.Paste(f3);
else if (event.charCode==120&&event.ctrlKey&&(!this.editing||j8))this.Clear(f3);
else if (!this.editing&&f3.e0!=null&&!this.IsHeaderCell(f3.e0)&&!event.ctrlKey&&!event.altKey){
this.StartEdit(f3,f3.e0);
}
}
break ;
}
}
this.GetProcessTab=function (f3){
var g2=this.GetTopSpread(f3);
return g2.getAttribute("ProcessTab");
}
this.ExpandRow=function (f3,k3){
var x3=f3.getAttribute("name");
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"ExpandView,"+k3,f3);
else 
__doPostBack(x3,"ExpandView,"+k3);
}
this.SortColumn=function (f3,column,v0){
var x3=f3.getAttribute("name");
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"SortColumn,"+column,f3);
else 
__doPostBack(x3,"SortColumn,"+column);
}
this.Filter=function (event,f3){
var q5=this.GetTarget(event);
var h1=q5.value;
if (q5.tagName=="SELECT"){
var ad4=new RegExp("\\s*");
var al0=new RegExp("\\S*");
var x6=q5[q5.selectedIndex].text;
var al1="";
var f5=0;
var f4=h1.length;
while (f4>0){
var i8=h1.match(ad4);
if (i8!=null){
al1+=i8[0];
f5=i8[0].length;
f4-=f5;
h1=h1.substring(f5);
i8=h1.match(al0);
if (i8!=null){
f5=i8[0].length;
f4-=f5;
h1=h1.substring(f5);
}
}else {
break ;
}
i8=x6.match(al0);
if (i8!=null){
al1+=i8[0];
f5=i8[0].length;
x6=x6.substring(f5);
i8=x6.match(ad4);
if (i8!=null){
f5=i8[0].length;
x6=x6.substring(f5);
}
}else {
break ;
}
}
h1=al1;
}
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5){
this.SyncData(q5.name,h1,f3);
f3.LoadState=null;
}
else 
__doPostBack(q5.name,h1);
}
this.MoveCol=function (f3,from,to){
var x3=f3.getAttribute("name");
if (f3.selectedCols&&f3.selectedCols.length>0){
var al2=[];
for (var f5=0;f5<f3.selectedCols.length;f5++)
al2[f5]=this.GetSheetColIndex(f3,f3.selectedCols[f5]);
var al3=al2.join("+");
this.MoveMultiCol(f3,al3,to);
return ;
}
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"MoveCol,"+from+","+to,f3);
else 
__doPostBack(x3,"MoveCol,"+from+","+to);
}
this.MoveMultiCol=function (f3,al3,to){
var x3=f3.getAttribute("name");
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"MoveCol,"+al3+","+to,f3);
else 
__doPostBack(x3,"MoveCol,"+al3+","+to);
}
this.Group=function (f3,p3,toCol){
var x3=f3.getAttribute("name");
if (f3.selectedCols&&f3.selectedCols.length>0){
var al2=[];
for (var f5=0;f5<f3.selectedCols.length;f5++)
if (f3.getAttribute("LayoutMode"))
al2[f5]=parseInt(f3.selectedCols[f5]);
else 
al2[f5]=this.GetSheetColIndex(f3,f3.selectedCols[f5]);
var al3=al2.join("+");
this.GroupMultiCol(f3,al3,toCol);
f3.selectedCols=[];
return ;
}
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"Group,"+p3+","+toCol,f3);
else 
__doPostBack(x3,"Group,"+p3+","+toCol);
}
this.GroupMultiCol=function (f3,al3,toCol){
var x3=f3.getAttribute("name");
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"Group,"+al3+","+toCol,f3);
else 
__doPostBack(x3,"Group,"+al3+","+toCol);
}
this.Ungroup=function (f3,p3,toCol){
var x3=f3.getAttribute("name");
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"Ungroup,"+p3+","+toCol,f3);
else 
__doPostBack(x3,"Ungroup,"+p3+","+toCol);
}
this.Regroup=function (f3,fromCol,toCol){
var x3=f3.getAttribute("name");
var ad5=(f3.getAttribute("ajax")!="false");
if (ad5)
this.SyncData(x3,"Regroup,"+fromCol+","+toCol,f3);
else 
__doPostBack(x3,"Regroup,"+fromCol+","+toCol);
}
this.ProcessData=function (){
try {
var al4=this;
al4.removeEventListener("load",the_fpSpread.ProcessData,false);
var q5=window.srcfpspread;
q5=q5.split(":").join("_");
var al5=window.fpcommand;
var al6=document;
var al7=al6.getElementById(q5+"_buff");
if (al7==null){
al7=al6.createElement("iframe");
al7.id=q5+"_buff";
al7.style.display="none";
al6.body.appendChild(al7);
}
var f3=al6.getElementById(q5);
the_fpSpread.CloseWaitMsg(f3);
if (al7==null)return ;
var al8=al4.responseText;
al7.contentWindow.document.body.innerHTML=al8;
var q7=al7.contentWindow.document.getElementById(q5+"_values");
if (q7!=null){
var x1=q7.getElementsByTagName("data")[0];
var p5=x1.firstChild;
the_fpSpread.error=false;
f3.SuspendLayout(true);
while (p5!=null){
var i4=the_fpSpread.GetRowByKey(f3,p5.getAttribute("r"));
var i6=the_fpSpread.GetColByKey(f3,p5.getAttribute("c"));
var ad7=the_fpSpread.GetValue(f3,i4,i6);
if (p5.innerHTML!=ad7){
var j6=the_fpSpread.GetFormula(f3,i4,i6);
var k6=the_fpSpread.GetCellByRowCol(f3,i4,i6);
the_fpSpread.SetCellValueFromView(k6,p5.innerHTML,true);
k6.setAttribute("FpFormula",j6);
}
p5=p5.nextSibling;
}
the_fpSpread.ClearCellData(f3);
f3.ResumeLayout(true);
}else {
the_fpSpread.UpdateSpread(al6,al7,q5,al8,al5);
}
var ad6=the_fpSpread.GetForm(f3);
ad6.__EVENTTARGET.value="";
ad6.__EVENTARGUMENT.value="";
var ad7=al6.getElementsByName("__VIEWSTATE")[0];
var h1=al7.contentWindow.document.getElementsByName("__VIEWSTATE")[0];
if (ad7!=null&&h1!=null)ad7.value=h1.value;
ad7=al6.getElementsByName("__EVENTVALIDATION");
h1=al7.contentWindow.document.getElementsByName("__EVENTVALIDATION");
if (ad7!=null&&h1!=null&&ad7.length>0&&h1.length>0)
ad7[0].value=h1[0].value;
al7.contentWindow.document.location="about:blank";
window.fpPostOn=null;
e7=null;
}catch (h4){
window.fpPostOn=null;
e7=null;
}
var f3=the_fpSpread.GetTopSpread(al6.getElementById(q5));
var h4=the_fpSpread.CreateEvent("CallBackStopped");
h4.command=al5;
the_fpSpread.FireEvent(f3,h4);
};
this.UpdateSpread=function (al6,al7,q5,al8,al5){
var f3=the_fpSpread.GetTopSpread(al6.getElementById(q5));
var u9=al7.contentWindow.document.getElementById(f3.id);
if (u9!=null){
if (typeof(Sys)!='undefined'){
FarPoint.System.ExtenderHelper.saveLoadedExtenderScripts(f3,al5);
}
the_fpSpread.error=(u9.getAttribute("error")=="true");
f3.LoadState=null;
if (al5=="LoadOnDemand"&&!the_fpSpread.error){
var al9=this.GetElementById(f3,f3.id+"_data");
var am0=this.GetElementById(u9,f3.id+"_data");
if (al9!=null&&am0!=null)al9.setAttribute("data",am0.getAttribute("data"));
var am1=u9.getElementsByTagName("style");
if (am1!=null){
for (var f5=0;f5<am1.length;f5++){
if (am1[f5]!=null&&am1[f5].innerHTML!=null&&am1[f5].innerHTML.indexOf(f3.id+"msgStyle")<0)
f3.appendChild(am1[f5].cloneNode(true));
}
}
var am2=this.GetElementById(f3,f3.id+"_LoadInfo");
var am3=this.GetElementById(u9,f3.id+"_LoadInfo");
if (am2!=null&&am3!=null)am2.value=am3.value;
var am4=false;
var am5=this.GetElementById(u9,f3.id+"_rowHeader");
if (am5!=null){
am5=am5.firstChild;
am4=(am5.rows.length>1);
var l2=this.GetRowHeader(f3);
this.LoadRows(l2,am5,true);
}
var am6=this.GetElementById(u9,f3.id+"_viewport2");
if (am6!=null){
am4=(am6.rows.length>0);
var g0=this.GetViewport2(f3);
this.LoadRows(g0,am6,false);
}
am6=this.GetElementById(u9,f3.id+"_viewport");
if (am6!=null){
am4=(am6.rows.length>0);
var g0=this.GetViewport(f3);
this.LoadRows(g0,am6,false);
}
var e4=f3.e4;
var e5=f3.e5;
var am7=f3.e2;
var am8=f3.e3;
var p8=f3.e6;
the_fpSpread.Init(f3,al5);
the_fpSpread.LoadScrollbarState(f3);
if (al5!="LoadOnDemand")the_fpSpread.Focus(f3);
f3.e4=e4;
f3.e5=e5;
f3.e2=am7;
f3.e3=am8;
f3.e6=p8;
if (am4)
f3.LoadState=null;
else 
f3.LoadState="complete";
if (typeof(Sys)!='undefined'){
FarPoint.System.ExtenderHelper.loadExtenderScripts(f3,al7.contentWindow.document.documentElement);
}
}else {
f3.innerHTML=u9.innerHTML;
the_fpSpread.CopySpreadAttrs(u9,f3);
if (typeof(Sys)!='undefined'){
FarPoint.System.ExtenderHelper.loadExtenderScripts(f3,al7.contentWindow.document.documentElement);
}
var am9=al7.contentWindow.document.getElementById(f3.id+"_initScript");
eval(am9.value);
}
}else {
the_fpSpread.error=true;
var an0=f3.getAttribute("errorPage");
if (an0!=null&&an0.length>0){
window.location.href=an0;
}
}
}
this.LoadRows=function (g0,am6,isHeader){
if (g0==null||am6==null)return ;
var an1=g0.tBodies[0];
var w2=am6.rows.length;
var an2=null;
if (isHeader){
w2--;
if (an1.rows.length>0)an2=an1.rows[an1.rows.length-1];
}
for (var f5=0;f5<w2;f5++){
var an3=am6.rows[f5].cloneNode(false);
an1.insertBefore(an3,an2);
an3.innerHTML=am6.rows[f5].innerHTML;
}
if (!isHeader){
for (var f5=0;f5<am6.parentNode.childNodes.length;f5++){
var ad2=am6.parentNode.childNodes[f5];
if (ad2!=am6){
g0.parentNode.insertBefore(ad2.cloneNode(true),null);
}
}
}
}
this.CopySpreadAttr=function (v8,dest,attrName){
var an4=v8.getAttribute(attrName);
var an5=dest.getAttribute(attrName);
if (an4!=null||an5!=null){
if (an4==null)
dest.removeAttribute(attrName);
else 
dest.setAttribute(attrName,an4);
}
}
this.CopySpreadAttrs=function (v8,dest){
this.CopySpreadAttr(v8,dest,"totalRowCount");
this.CopySpreadAttr(v8,dest,"pageCount");
this.CopySpreadAttr(v8,dest,"loadOnDemand");
this.CopySpreadAttr(v8,dest,"allowGroup");
this.CopySpreadAttr(v8,dest,"colMove");
this.CopySpreadAttr(v8,dest,"showFocusRect");
this.CopySpreadAttr(v8,dest,"FocusBorderColor");
this.CopySpreadAttr(v8,dest,"FocusBorderStyle");
this.CopySpreadAttr(v8,dest,"FpDefaultEditorID");
this.CopySpreadAttr(v8,dest,"hierView");
this.CopySpreadAttr(v8,dest,"IsNewRow");
this.CopySpreadAttr(v8,dest,"cmdTop");
this.CopySpreadAttr(v8,dest,"ProcessTab");
this.CopySpreadAttr(v8,dest,"AcceptFormula");
this.CopySpreadAttr(v8,dest,"EditMode");
this.CopySpreadAttr(v8,dest,"AllowInsert");
this.CopySpreadAttr(v8,dest,"AllowDelete");
this.CopySpreadAttr(v8,dest,"error");
this.CopySpreadAttr(v8,dest,"ajax");
this.CopySpreadAttr(v8,dest,"autoCalc");
this.CopySpreadAttr(v8,dest,"multiRange");
this.CopySpreadAttr(v8,dest,"rowFilter");
this.CopySpreadAttr(v8,dest,"OperationMode");
this.CopySpreadAttr(v8,dest,"selectedForeColor");
this.CopySpreadAttr(v8,dest,"selectedBackColor");
this.CopySpreadAttr(v8,dest,"anchorBackColor");
this.CopySpreadAttr(v8,dest,"columnHeaderAutoTextIndex");
this.CopySpreadAttr(v8,dest,"EnableRowEditTemplate");
this.CopySpreadAttr(v8,dest,"scrollContent");
this.CopySpreadAttr(v8,dest,"scrollContentColumns");
this.CopySpreadAttr(v8,dest,"scrollContentTime");
this.CopySpreadAttr(v8,dest,"scrollContentMaxHeight");
this.CopySpreadAttr(v8,dest,"SelectionPolicy");
this.CopySpreadAttr(v8,dest,"ShowHeaderSelection");
this.CopySpreadAttr(v8,dest,"layoutMode");
this.CopySpreadAttr(v8,dest,"layoutRowCount");
dest.tabIndex=v8.tabIndex;
if (dest.style!=null&&v8.style!=null){
if (dest.style.width!=v8.style.width)dest.style.width=v8.style.width;
if (dest.style.height!=v8.style.height)dest.style.height=v8.style.height;
if (dest.style.border!=v8.style.border)dest.style.border=v8.style.border;
}
}
this.Clone=function (o2){
var h1=document.createElement(o2.tagName);
h1.id=o2.id;
var i6=o2.firstChild;
while (i6!=null){
var r8=this.Clone(i6);
h1.appendChild(r8);
i6=i6.nextSibling;
}
return h1;
}
this.FireEvent=function (f3,h4){
if (f3==null||h4==null)return ;
var g2=this.GetTopSpread(f3);
if (g2!=null){
h4.spread=f3;
g2.dispatchEvent(h4);
}
}
this.GetForm=function (f3)
{
var j6=f3.parentNode;
while (j6!=null&&j6.tagName!="FORM")j6=j6.parentNode;
return j6;
}
this.SyncData=function (x3,al5,f3,asyncCallBack){
if (window.fpPostOn!=null){
return ;
}
this.editing=false;
var h4=this.CreateEvent("CallBackStart");
h4.cancel=false;
h4.command=al5;
if (asyncCallBack==null)asyncCallBack=false;
h4.async=asyncCallBack;
if (f3==null){
var r8=x3.split(":").join("_");
f3=document.getElementById(r8);
}
if (f3!=null){
var g2=this.GetTopSpread(f3);
this.FireEvent(f3,h4);
}
if (h4.cancel){
the_fpSpread.ClearCellData(f3);
return ;
}
if (al5!=null&&(al5.indexOf("SelectView,")==0||al5=="Next"||al5=="Prev"||al5.indexOf("Group,")==0||al5.indexOf("Page,")==0))
f3.LoadState=null;
var an6=h4.async;
if (an6){
this.OpenWaitMsg(f3);
}
window.fpPostOn=true;
if (this.error)al5="update";
try {
var ad6=this.GetForm(f3);
if (ad6==null)return ;
ad6.__EVENTTARGET.value=x3;
ad6.__EVENTARGUMENT.value=encodeURIComponent(al5);
var an7=ad6.action;
var h1;
if (an7.indexOf("?")>-1){
h1="&";
}
else 
{
h1="?";
}
an7=an7+h1;
var g7=this.CollectData(f3);
var al8="";
var al4=(window.XMLHttpRequest)?new XMLHttpRequest():new ActiveXObject("Microsoft.XMLHTTP");
if (al4==null)return ;
al4.open("POST",an7,an6);
al4.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
if (f3!=null)
window.srcfpspread=f3.id;
else 
window.srcfpspread=x3;
window.fpcommand=al5;
this.AttachEvent(al4,"load",the_fpSpread.ProcessData,false);
al4.send(g7);
}catch (h4){
window.fpPostOn=false;
e7=null;
}
};
this.CollectData=function (f3){
var ad6=this.GetForm(f3);
var h1;
var h7="fpcallback=true&";
for (var f5=0;f5<ad6.elements.length;f5++){
h1=ad6.elements[f5];
var an8=h1.tagName.toLowerCase();
if (an8=="input"){
var an9=h1.type;
if (an9=="hidden"||an9=="text"||an9=="password"||((an9=="checkbox"||an9=="radio")&&h1.checked)){
h7+=(h1.name+"="+encodeURIComponent(h1.value)+"&");
}
}else if (an8=="select"){
if (h1.childNodes!=null){
for (var j4=0;j4<h1.childNodes.length;j4++){
var t1=h1.childNodes[j4];
if (t1!=null&&t1.tagName!=null&&t1.tagName.toLowerCase()=="option"&&t1.selected){
h7+=(h1.name+"="+encodeURIComponent(t1.value)+"&");
}
}
}
}else if (an8=="textarea"){
h7+=(h1.name+"="+encodeURIComponent(h1.value)+"&");
}
}
return h7;
};
this.ClearCellData=function (f3){
var g7=this.GetData(f3);
var ao0=g7.getElementsByTagName("root")[0];
var g8=ao0.getElementsByTagName("data")[0];
if (g8==null)return null;
if (f3.e7!=null){
var k3=f3.e7.firstChild;
while (k3!=null){
var i4=k3.getAttribute("key");
var ao1=k3.firstChild;
while (ao1!=null){
var i6=ao1.getAttribute("key");
var ao2=g8.firstChild;
while (ao2!=null){
var i8=ao2.getAttribute("key");
if (i4==i8){
var ao3=false;
var ao4=ao2.firstChild;
while (ao4!=null){
var i9=ao4.getAttribute("key");
if (i6==i9){
ao2.removeChild(ao4);
ao3=true;
break ;
}
ao4=ao4.nextSibling;
}
if (ao3)break ;
}
ao2=ao2.nextSibling;
}
ao1=ao1.nextSibling;
}
k3=k3.nextSibling;
}
}
f3.e7=null;
var h0=this.GetCmdBtn(f3,"Cancel");
if (h0!=null)
this.UpdateCmdBtnState(h0,true);
}
this.StorePostData=function (f3){
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
var ag4=g8.getElementsByTagName("data")[0];
if (ag4!=null)f3.e7=ag4.cloneNode(true);
}
this.ShowMessage=function (f3,z4,k3,p3,time){
var f6=f3.GetRowCount();
var i3=f3.GetColCount();
if (k3==null||p3==null||k3<0||k3>=f6||p3<0||p3>=i3){
k3=-1;
p3=-1;
}
this.ShowMessageInner(f3,z4,k3,p3,time);
}
this.HideMessage=function (f3,k3,p3){
var f6=f3.GetRowCount();
var i3=f3.GetColCount();
if (k3==null||p3==null||k3<0||k3>=f6||p3<0||p3>=i3)
if (f3.msgList&&f3.msgList.centerMsg&&f3.msgList.centerMsg.msgBox.IsVisible)
f3.msgList.centerMsg.msgBox.Hide();
var ao5=this.GetMsgObj(f3,k3,p3);
if (ao5&&ao5.msgBox.IsVisible){
ao5.msgBox.Hide();
}
}
this.ShowMessageInner=function (f3,z4,k3,p3,time){
var ao5=this.GetMsgObj(f3,k3,p3);
if (ao5){
if (ao5.timer)
ao5.msgBox.Hide();
}
else 
ao5=this.CreateMsgObj(f3,k3,p3);
var ao6=ao5.msgBox;
ao6.Show(f3,this,z4);
if (time&&time>0)
ao5.timer=setTimeout(function (){ao6.Hide();},time);
this.SetMsgObj(f3,ao5);
}
this.GetMsgObj=function (f3,k3,p3){
var ao5;
var ao7=f3.msgList;
if (ao7){
if (k3==-1&&p3==-1)
ao5=ao7.centerMsg;
else if (k3==-2)
ao5=ao7.hScrollMsg;
else if (p3==-2)
ao5=ao7.vScrollMsg;
else {
if (ao7[k3])
ao5=ao7[k3][p3];
}
}
return ao5;
}
this.SetMsgObj=function (f3,ao5){
var ao7=f3.msgList;
if (ao5.row==-1&&ao5.col==-1)
ao7.centerMsg=ao5;
else if (ao5.row==-2)
ao7.hScrollMsg=ao5;
else if (ao5.col==-2)
ao7.vScrollMsg=ao5;
else {
if (!ao7[ao5.row])ao7[ao5.row]=new Array();
ao7[ao5.row][ao5.col]=ao5;
}
}
var ao8=null;
this.CreateMsgObj=function (f3,k3,p3){
var ao6=document.createElement("div");
var ao5={row:k3,col:p3,msgBox:ao6};
var ao9=null;
if (k3!=-2&&p3!=-2){
ao6.style.border="1px solid black";
ao6.style.background="yellow";
ao6.style.color="red";
}
else {
ao6.style.border="1px solid #55678e";
ao6.style.fontSize="small";
ao6.style.background="#E6E9ED";
ao6.style.color="#4c5b7f";
this.GetScrollingContentStyle(f3);
ao9=ao8;
}
if (ao9!=null){
if (ao9.fontFamily!=null)
ao6.style.fontFamily=ao9.fontFamily;
if (ao9.fontSize!=null)
ao6.style.fontSize=ao9.fontSize;
if (ao9.fontStyle!=null)
ao6.style.fontStyle=ao9.fontStyle;
if (ao9.fontVariant!=null)
ao6.style.fontVariant=ao9.fontVariant;
if (ao9.fontWeight!=null)
ao6.style.fontWeight=ao9.fontWeight;
if (ao9.backgroundColor!=null)
ao6.style.backgroundColor=ao9.backgroundColor;
if (ao9.color!=null)
ao6.style.color=ao9.color;
}
ao6.style.position="absolute";
ao6.style.overflow="hidden";
ao6.style.display="block";
ao6.style.marginLeft=0;
ao6.style.marginTop=2;
ao6.style.marginRight=0;
ao6.style.marginBottom=0;
ao6.msgObj=ao5;
ao6.Show=function (u9,fpObj,z4){
var aa7=fpObj.GetMsgPos(u9,this.msgObj.row,this.msgObj.col);
var g1=fpObj.GetCommandBar(u9);
var ap0=fpObj.GetGroupBar(u9);
this.style.visibility="visible";
this.style.display="block";
if (z4){
this.style.left=""+0+"px";
this.style.top=""+0+"px";
this.style.width="auto";
this.innerHTML=z4;
}
var p0=fpObj.GetViewport0(u9);
var g5=fpObj.GetViewport1(u9);
var z7=fpObj.GetViewport2(u9);
var ap1=(p0||g5||z7);
var ap2=(u9.style.position=="relative"||u9.style.position=="absolute");
var ap3=aa7.top;
var ap4=aa7.left;
var t1=f3.offsetParent;
while ((t1.tagName=="TD"||t1.tagName=="TR"||t1.tagName=="TBODY"||t1.tagName=="TABLE")&&t1.style.position!="relative"&&t1.style.position!="absolute")
t1=t1.offsetParent;
if (this.msgObj.row>=0&&this.msgObj.col>=0){
if (!ap2&&ap1&&t1){
var ap5=fpObj.GetLocation(u9);
var ap6=fpObj.GetLocation(t1);
ap3+=ap5.y-ap6.y;
ap4+=ap5.x-ap6.x;
if (t1.tagName!="BODY"){
ap3-=fpObj.GetBorderWidth(t1,0);
ap4-=fpObj.GetBorderWidth(t1,3);
}
}
var ap7=fpObj.GetViewPortByRowCol(u9,this.msgObj.row,this.msgObj.col);
if (!this.parentNode&&ap7&&ap7.parentNode)ap7.parentNode.insertBefore(ao6,null);
var k7=this.offsetWidth;
this.style.left=""+ap4+"px";
if (!ap1&&ap7&&ap7.parentNode&&ap4+k7>ap7.offsetWidth)
this.style.width=""+(aa7.a7-2)+"px";
else if (parseInt(this.style.width)!=k7)
this.style.width=""+(k7-2)+"px";
if (!ap1&&ap7!=null&&ap3>=ap7.offsetHeight-2)ap3-=aa7.a6+this.offsetHeight;
this.style.top=""+ap3+"px";
}
else {
if (!ap2&&t1){
var ap5=fpObj.GetLocation(u9);
var ap6=fpObj.GetLocation(t1);
ap3+=ap5.y-ap6.y;
ap4+=ap5.x-ap6.x;
if (t1.tagName!="BODY"){
ap3-=fpObj.GetBorderWidth(t1,0);
ap4-=fpObj.GetBorderWidth(t1,3);
}
}
var ap8=20;
if (!this.parentNode)u9.insertBefore(ao6,null);
if (this.offsetWidth+ap8<u9.offsetWidth)
ap4+=(u9.offsetWidth-this.offsetWidth-ap8)/(this.msgObj.row==-2?1:2);
else 
this.style.width=""+(u9.offsetWidth-ap8)+"px";
if (this.offsetHeight<u9.offsetHeight)
ap3+=(u9.offsetHeight-this.offsetHeight)/(this.msgObj.col==-2?1:2);
if (this.msgObj.col==-2){
var ap9=fpObj.GetColFooter(u9);
if (ap9)ap3-=ap9.offsetHeight;
var g1=fpObj.GetCommandBar(u9);
if (g1)ap3-=g1.offsetHeight;
ap3-=ap8;
}
this.style.top=""+ap3+"px";
this.style.left=""+ap4+"px";
}
this.IsVisible=true;
};
ao6.Hide=function (){
this.style.visibility="hidden";
this.style.display="none";
this.IsVisible=false;
if (this.msgObj.timer){
clearTimeout(this.msgObj.timer);
this.msgObj.timer=null;
}
this.innerHTML="";
};
return ao5;
}
this.GetLocation=function (ele){
if ((ele.window&&ele.window===ele)||ele.nodeType===9)return {x:0,y:0};
var aq0=0;
var aq1=0;
var aq2=null;
var aq3=null;
var aq4=null;
for (var k2=ele;k2;aq2=k2,aq3=aq4,k2=k2.offsetParent){
var an8=k2.tagName;
aq4=this.GetCurrentStyle2(k2);
if ((k2.offsetLeft||k2.offsetTop)&&
!((an8==="BODY")&&
(!aq3||aq3.position!="absolute"))){
aq0+=k2.offsetLeft;
aq1+=k2.offsetTop;
}
if (aq2!=null&&aq4){
if ((an8!="TABLE")&&(an8!="TD")&&(an8!="HTML")){
aq0+=parseInt(aq4.borderLeftWidth)||0;
aq1+=parseInt(aq4.borderTopWidth)||0;
}
if (an8==="TABLE"&&
(aq4.position==="relative"||aq4.position==="absolute")){
aq0+=parseInt(aq4.marginLeft)||0;
aq1+=parseInt(aq4.marginTop)||0;
}
}
}
aq4=this.GetCurrentStyle2(ele);
var aq5=aq4?aq4.position:null;
if (!aq5||(aq5!="absolute")){
for (var k2=ele.parentNode;k2;k2=k2.parentNode){
an8=k2.tagName;
if ((an8!="BODY")&&(an8!="HTML")&&(k2.scrollLeft||k2.scrollTop)){
aq0-=(k2.scrollLeft||0);
aq1-=(k2.scrollTop||0);
aq4=this.GetCurrentStyle2(k2);
if (aq4){
aq0+=parseInt(aq4.borderLeftWidth)||0;
aq1+=parseInt(aq4.borderTopWidth)||0;
}
}
}
}
return {x:aq0,y:aq1};
}
var aq6=["borderTopWidth","borderRightWidth","borderBottomWidth","borderLeftWidth"];
var aq7=["borderTopStyle","borderRightStyle","borderBottomStyle","borderLeftStyle"];
var aq8;
this.GetBorderWidth=function (ele,side){
if (!this.GetBorderVisible(ele,side))return 0;
var p1=this.GetCurrentStyle(ele,aq6[side]);
return this.ParseBorderWidth(p1);
}
this.GetBorderVisible=function (ele,side){
return this.GetCurrentStyle(ele,aq7[side])!="none";
}
this.GetWindow=function (ele){
var al6=ele.ownerDocument||ele.document||ele;
return al6.defaultView||al6.parentWindow;
}
this.GetCurrentStyle2=function (ele){
if (ele.nodeType===3)return null;
var k7=this.GetWindow(ele);
if (ele.documentElement)ele=ele.documentElement;
var aq9=(k7&&(ele!=k7))?k7.getComputedStyle(ele,null):ele.style;
return aq9;
}
this.GetCurrentStyle=function (ele,attribute,defaultValue){
var ar0=null;
if (ele){
if (ele.currentStyle){
ar0=ele.currentStyle[attribute];
}
else if (document.defaultView&&document.defaultView.getComputedStyle){
var ar1=document.defaultView.getComputedStyle(ele,null);
if (ar1){
ar0=ar1[attribute];
}
}
if (!ar0&&ele.style.getPropertyValue){
ar0=ele.style.getPropertyValue(attribute);
}
else if (!ar0&&ele.style.getAttribute){
ar0=ele.style.getAttribute(attribute);
}
}
if (!ar0||ar0==""||typeof(ar0)==='undefined'){
if (typeof(defaultValue)!='undefined'){
ar0=defaultValue;
}
else {
ar0=null;
}
}
return ar0;
}
this.ParseBorderWidth=function (p1){
if (!aq8){
var ar2={};
var ar3=document.createElement('div');
ar3.style.visibility='hidden';
ar3.style.position='absolute';
ar3.style.fontSize='1px';
document.body.appendChild(ar3)
var ar4=document.createElement('div');
ar4.style.height='0px';
ar4.style.overflow='hidden';
ar3.appendChild(ar4);
var ar5=ar3.offsetHeight;
ar4.style.borderTop='solid black';
ar4.style.borderTopWidth='thin';
ar2['thin']=ar3.offsetHeight-ar5;
ar4.style.borderTopWidth='medium';
ar2['medium']=ar3.offsetHeight-ar5;
ar4.style.borderTopWidth='thick';
ar2['thick']=ar3.offsetHeight-ar5;
ar3.removeChild(ar4);
document.body.removeChild(ar3);
aq8=ar2;
}
if (p1){
switch (p1){
case 'thin':
case 'medium':
case 'thick':
return aq8[p1];
case 'inherit':
return 0;
}
var ar6=this.ParseUnit(p1);
if (ar6.type!='px')
throw new Error();
return ar6.size;
}
return 0;
}
this.ParseUnit=function (p1){
if (!p1)
throw new Error();
p1=this.Trim(p1).toLowerCase();
var af3=p1.length;
var u9=-1;
for (var f5=0;f5<af3;f5++){
var ad2=p1.substr(f5,1);
if ((ad2<'0'||ad2>'9')&&ad2!='-'&&ad2!='.'&&ad2!=',')
break ;
u9=f5;
}
if (u9==-1)
throw new Error();
var an9;
var ar7;
if (u9<(af3-1))
an9=this.Trim(p1.substring(u9+1));
else 
an9='px';
ar7=parseFloat(p1.substr(0,u9+1));
if (an9=='px'){
ar7=Math.floor(ar7);
}
return {size:ar7,type:an9};
}
this.GetViewPortByRowCol=function (f3,k3,p3){
var p0=this.GetViewport0(f3);
var g5=this.GetViewport1(f3);
var z7=this.GetViewport2(f3);
var p1=this.GetViewport(f3);
var i7=this.GetCellByRowCol(f3,k3,p3);
if (p1!=null&&this.IsChild(i7,p1))
return p1;
else if (z7!=null&&this.IsChild(i7,z7))
return z7;
else if (g5!=null&&this.IsChild(i7,g5))
return g5;
else if (p0!=null&&this.IsChild(i7,p0))
return p0;
return ;
}
this.GetMsgPos=function (f3,k3,p3){
if (k3<0||p3<0){
return {left:0,top:0};
}
else {
var p0=this.GetViewport0(f3);
var g5=this.GetViewport1(f3);
var z7=this.GetViewport2(f3);
var p1=this.GetViewport(f3);
var ar8=this.GetGroupBar(f3);
var o3=document.getElementById(f3.id+"_titleBar");
var i7=this.GetCellByRowCol(f3,k3,p3);
var h1=i7.offsetTop+i7.clientHeight;
var af3=i7.offsetLeft;
if ((p0!=null||g5!=null)&&(this.IsChild(i7,z7)||this.IsChild(i7,p1))){
if (p0!=null)
h1+=p0.offsetHeight;
else 
h1+=g5.offsetHeight;
}
if ((p0!=null||z7!=null)&&(this.IsChild(i7,g5)||this.IsChild(i7,p1))){
if (p0!=null)
af3+=p0.offsetWidth;
else 
af3+=z7.offsetWidth;
}
if (p1!=null&&(p0||g5||z7)){
if (o3)h1+=o3.offsetHeight;
if (ar8)h1+=ar8.offsetHeight;
if (this.GetColHeader(f3))h1+=this.GetColHeader(f3).offsetHeight;
if (this.GetRowHeader(f3))af3+=this.GetRowHeader(f3).offsetWidth;
}
if (p1!=null&&this.IsChild(i7,p1)){
if (g5||z7)
h1-=p1.parentNode.scrollTop;
if (g5||z7)
af3-=p1.parentNode.scrollLeft;
}
if (z7!=null&&this.IsChild(i7,z7)){
h1-=z7.parentNode.scrollTop;
}
if (g5!=null&&this.IsChild(i7,g5)){
af3-=g5.parentNode.scrollLeft;
}
var f7=i7.clientHeight;
var k7=i7.clientWidth;
return {left:af3,top:h1,a6:f7,a7:k7};
}
}
this.SyncMsgs=function (f3){
if (!f3.msgList)return ;
for (f5 in f3.msgList){
if (f3.msgList[f5].constructor==Array){
for (j4 in f3.msgList[f5]){
if (f3.msgList[f5][j4]&&f3.msgList[f5][j4].msgBox&&f3.msgList[f5][j4].msgBox.IsVisible){
f3.msgList[f5][j4].msgBox.Show(f3,this);
}
}
}
}
}
this.GetCellInfo=function (f3,i4,i6,aa7){
var g7=this.GetData(f3);
if (g7==null)return null;
var g8=g7.getElementsByTagName("root")[0];
if (g8==null)return null;
var p8=g8.getElementsByTagName("state")[0];
if (p8==null)return null;
var ar9=p8.getElementsByTagName("cellinfo")[0];
if (ar9==null)return null;
var h1=ar9.firstChild;
while (h1!=null){
if ((h1.getAttribute("r")==""+i4)&&(h1.getAttribute("c")==""+i6)&&(h1.getAttribute("pos")==""+aa7))return h1;
h1=h1.nextSibling;
}
return null;
}
this.AddCellInfo=function (f3,i4,i6,aa7){
var p5=this.GetCellInfo(f3,i4,i6,parseInt(aa7));
if (p5!=null)return p5;
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
if (g8==null)return null;
var p8=g8.getElementsByTagName("state")[0];
if (p8==null)return null;
var ar9=p8.getElementsByTagName("cellinfo")[0];
if (ar9==null)return null;
if (document.all!=null){
p5=g7.createNode("element","c","");
}else {
p5=document.createElement("c");
p5.style.display="none";
}
p5.setAttribute("r",i4);
p5.setAttribute("c",i6);
p5.setAttribute("pos",aa7);
ar9.appendChild(p5);
return p5;
}
this.setCellAttribute=function (f3,i7,attname,z1,noEvent,recalc){
if (i7==null)return ;
var i4=this.GetRowKeyFromCell(f3,i7);
var i6=f3.getAttribute("LayoutMode")?this.GetColKeyFromCell2(f3,i7):this.GetColKeyFromCell(f3,i7);
if (typeof(i4)=="undefined")return ;
var aa7=-1;
if (this.IsChild(i7,this.GetCorner(f3)))
aa7=0;
else if (this.IsChild(i7,this.GetRowHeader(f3))||this.IsChild(i7,this.GetFrozColHeader(f3)))
aa7=1;
else if (this.IsChild(i7,this.GetColHeader(f3))||this.IsChild(i7,this.GetFrozColHeader(f3)))
aa7=2;
else if (this.IsChild(i7,this.GetViewport(f3))||this.IsChild(i7,this.GetViewport0(f3))||this.IsChild(i7,this.GetViewport1(f3))||this.IsChild(i7,this.GetViewport2(f3)))
aa7=3;
var u5=this.AddCellInfo(f3,i4,i6,aa7);
u5.setAttribute(attname,z1);
if (!noEvent){
var h4=this.CreateEvent("DataChanged");
h4.cell=i7;
h4.cellValue=z1;
h4.row=i4;
h4.col=i6;
this.FireEvent(f3,h4);
}
var h0=this.GetCmdBtn(f3,"Update");
if (h0!=null&&h0.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(h0,false);
h0=this.GetCmdBtn(f3,"Cancel");
if (h0!=null&&h0.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(h0,false);
f3.f1=true;
if (recalc){
this.UpdateValues(f3);
}
}
this.updateCellLocked=function (i7,locked){
if (i7==null)return ;
var h1=i7.getAttribute("FpCellType")=="readonly";
if (h1==locked)return ;
var i6=i7.firstChild;
while (i6!=null){
if (typeof(i6.disabled)!="undefined")i6.disabled=locked;
i6=i6.nextSibling;
}
}
this.Cells=function (f3,i4,i6)
{
var as0=this.GetCellByRowCol(f3,i4,i6);
if (as0){
as0.GetValue=function (){
return the_fpSpread.GetValue(f3,i4,i6);
}
as0.SetValue=function (h5){
if (typeof(h5)=="undefined")return ;
if (this.parentNode.getAttribute("previewRow")!=null)return ;
the_fpSpread.SetValue(f3,i4,i6,h5);
the_fpSpread.SaveClientEditedDataRealTime();
}
as0.GetBackColor=function (){
if (this.getAttribute("bgColorBak")!=null)
return this.getAttribute("bgColorBak");
return document.defaultView.getComputedStyle(this,"").getPropertyValue("background-color");
}
as0.SetBackColor=function (h5){
if (typeof(h5)=="undefined")return ;
this.bgColor=h5;
this.setAttribute("bgColorBak",h5);
this.style.backgroundColor=h5;
the_fpSpread.setCellAttribute(f3,this,"bc",h5);
the_fpSpread.SaveClientEditedDataRealTime();
}
as0.GetForeColor=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("color");
}
as0.SetForeColor=function (h5){
if (typeof(h5)=="undefined")return ;
this.style.color=h5;
the_fpSpread.setCellAttribute(f3,this,"fc",h5);
the_fpSpread.SaveClientEditedDataRealTime();
}
as0.GetTabStop=function (){
return this.getAttribute("TabStop")!="false";
}
as0.SetTabStop=function (h5){
if (typeof(h5)=="undefined")return ;
var as1=new String(h5);
if (as1.toLocaleLowerCase()=="false"){
this.setAttribute("TabStop","false");
the_fpSpread.setCellAttribute(f3,this,"ts","false");
the_fpSpread.SaveClientEditedDataRealTime();
}else {
this.removeAttribute("TabStop");
}
}
as0.GetCellType=function (){
var as2=the_fpSpread.GetCellType2(this);
if (as2=="text"||as2=="readonly")
{
as2=this.getAttribute("CellType2");
}
if (as2==null)
as2="GeneralCellType";
return as2;
}
as0.GetHAlign=function (){
var as3=document.defaultView.getComputedStyle(this,"").getPropertyValue("text-Align");
if (as3==""||as3=="undefined"||as3==null){
as3=this.style.textAlign;
}
if (as3==""||as3=="undefined"||as3==null)
as3=this.getAttribute("align");
if (as3=="start")as3="left";
if (as3!=null&&as3.indexOf("-moz")!=-1)as3=as3.replace("-moz-","");
return as3;
}
as0.SetHAlign=function (h5){
if (typeof(h5)=="undefined")return ;
this.style.textAlign=typeof(h5)=="string"?h5:h5.Name;
the_fpSpread.setCellAttribute(f3,this,"ha",typeof(h5)=="string"?h5:h5.Name);
the_fpSpread.SaveClientEditedDataRealTime();
}
as0.GetVAlign=function (){
var as4=document.defaultView.getComputedStyle(this,"").getPropertyValue("vertical-Align");
if (as4==""||as4=="undefined"||as4==null)
as4=this.style.verticalAlign;
if (as4==""||as4=="undefined"||as4==null)
as4=this.getAttribute("valign");
return as4;
}
as0.SetVAlign=function (h5){
if (typeof(h5)=="undefined")return ;
this.style.verticalAlign=typeof(h5)=="string"?h5:h5.Name;
the_fpSpread.setCellAttribute(f3,this,"va",typeof(h5)=="string"?h5:h5.Name);
the_fpSpread.SaveClientEditedDataRealTime();
}
as0.GetLocked=function (){
if (as0.GetCellType()=="ButtonCellType"||as0.GetCellType()=="TagCloudCellType"||as0.GetCellType()=="HyperLinkCellType")
return as0.getAttribute("Locked")=="1";
return the_fpSpread.GetCellType(this)=="readonly";
}
as0.GetFont_Name=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("font-family");
}
as0.SetFont_Name=function (h5){
if (typeof(h5)=="undefined")return ;
this.style.fontFamily=h5;
the_fpSpread.setCellAttribute(f3,this,"fn",h5);
the_fpSpread.SaveClientEditedDataRealTime();
}
as0.GetFont_Size=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("font-size");
}
as0.SetFont_Size=function (h5){
if (typeof(h5)=="undefined")return ;
if (typeof(h5)=="number")h5+="px";
this.style.fontSize=h5;
the_fpSpread.setCellAttribute(f3,this,"fs",h5);
the_fpSpread.SizeSpread(f3);
the_fpSpread.SaveClientEditedDataRealTime();
}
as0.GetFont_Bold=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("font-weight")=="bold"?true:false;
}
as0.SetFont_Bold=function (h5){
if (typeof(h5)=="undefined")return ;
this.style.fontWeight=h5==true?"bold":"normal";
the_fpSpread.setCellAttribute(f3,this,"fb",new String(h5).toLocaleLowerCase());
the_fpSpread.SaveClientEditedDataRealTime();
}
as0.GetFont_Italic=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("font-style")=="italic"?true:false;
}
as0.SetFont_Italic=function (h5){
if (typeof(h5)=="undefined")return ;
this.style.fontStyle=h5==true?"italic":"normal";
the_fpSpread.setCellAttribute(f3,this,"fi",new String(h5).toLocaleLowerCase());
the_fpSpread.SaveClientEditedDataRealTime();
}
as0.GetFont_Overline=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("overline")>=0?true:false;
}
as0.SetFont_Overline=function (h5){
if (h5){
var as5=new String("overline");
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("line-through")>=0)
as5+=" line-through"
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("underline")>=0)
as5+=" underline"
this.style.textDecoration=as5;
}
else {
var as5=new String("");
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("line-through")>=0)
as5+=" line-through"
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("underline")>=0)
as5+=" underline"
if (as5=="")as5="none";
this.style.textDecoration=as5;
}
the_fpSpread.setCellAttribute(f3,this,"fo",new String(h5).toLocaleLowerCase());
the_fpSpread.SaveClientEditedDataRealTime();
}
as0.GetFont_Strikeout=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("line-through")>=0?true:false;
}
as0.SetFont_Strikeout=function (h5){
if (h5){
var as5=new String("line-through");
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("overline")>=0)
as5+=" overline"
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("underline")>=0)
as5+=" underline"
this.style.textDecoration=as5;
}
else {
var as5=new String("");
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("overline")>=0)
as5+=" overline"
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("underline")>=0)
as5+=" underline"
if (as5=="")as5="none";
this.style.textDecoration=as5;
}
the_fpSpread.setCellAttribute(f3,this,"fk",new String(h5).toLocaleLowerCase());
the_fpSpread.SaveClientEditedDataRealTime();
}
as0.GetFont_Underline=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("underline")>=0?true:false;
}
as0.SetFont_Underline=function (h5){
if (h5){
var as5=new String("underline");
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("overline")>=0)
as5+=" overline"
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("line-through")>=0)
as5+=" line-through"
this.style.textDecoration=as5;
}
else {
var as5=new String("");
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("overline")>=0)
as5+=" overline"
if (document.defaultView.getComputedStyle(this,"").getPropertyValue("text-decoration").indexOf("line-through")>=0)
as5+=" line-through"
if (as5=="")as5="none";
this.style.textDecoration=as5;
}
the_fpSpread.setCellAttribute(f3,this,"fu",new String(h5).toLocaleLowerCase());
the_fpSpread.SaveClientEditedDataRealTime();
}
return as0;
}
return null;
}
this.getDomRow=function (f3,i4){
var f6=this.GetRowCount(f3);
if (f6==0)return null;
var i7=this.GetCellByRowCol(f3,i4,0);
if (i7){
var f4=i7.parentNode.rowIndex;
if (f4>=0){
var k3=i7.parentNode.parentNode.rows[f4];
if (this.GetSizable(f3,k3))
return k3;
}
return null;
}
}
this.setRowInfo_RowAttribute=function (f3,i4,attname,z1,recalc){
i4=parseInt(i4);
if (i4<0)return ;
var as6=this.AddRowInfo(f3,i4);
as6.setAttribute(attname,z1);
var h0=this.GetCmdBtn(f3,"Update");
if (h0!=null&&h0.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(h0,false);
h0=this.GetCmdBtn(f3,"Cancel");
if (h0!=null&&h0.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(h0,false);
f3.f1=true;
if (recalc){
this.UpdateValues(f3);
}
}
this.Rows=function (f3,i4)
{
var as7=this.getDomRow(f3,i4);
if (as7){
as7.GetHeight=function (){
return the_fpSpread.GetRowHeightInternal(f3,i4);
}
as7.SetHeight=function (h5){
if (typeof(h5)=="undefined")return ;
the_fpSpread.SetRowHeight2(f3,i4,parseInt(h5));
the_fpSpread.SaveClientEditedDataRealTime();
}
return as7;
}
return null;
}
this.setColInfo_ColumnAttribute=function (f3,i6,attname,z1,recalc){
i6=parseInt(i6);
if (i6<0)return ;
var as8=this.AddColInfo(f3,i6);
as8.setAttribute(attname,z1);
var h0=this.GetCmdBtn(f3,"Update");
if (h0!=null&&h0.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(h0,false);
h0=this.GetCmdBtn(f3,"Cancel");
if (h0!=null&&h0.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(h0,false);
f3.f1=true;
if (recalc){
this.UpdateValues(f3);
}
}
this.Columns=function (f3,i6)
{
var as9={a4:this.GetColByKey(f3,parseInt(i6))};
if (as9){
as9.GetWidth=function (){
return the_fpSpread.GetColWidthFromCol(f3,i6);
}
as9.SetWidth=function (h5){
if (typeof(h5)=="undefined")return ;
the_fpSpread.SetColWidth(f3,i6,h5);
the_fpSpread.SaveClientEditedDataRealTime();
}
return as9;
}
return null;
}
this.GetTitleBar=function (f3){
try {
if (document.getElementById(f3.id+"_title")==null)return null;
var ak2=document.getElementById(f3.id+"_titleBar");
if (ak2!=null)ak2=document.getElementById(f3.id+"_title");
return ak2;
}
catch (ex){
return null;
}
}
this.CheckTitleInfo=function (f3){
var g7=this.GetData(f3);
if (g7==null)return null;
var g8=g7.getElementsByTagName("root")[0];
if (g8==null)return null;
var at0=g8.getElementsByTagName("titleinfo")[0];
if (at0==null)return null;
return at0;
}
this.AddTitleInfo=function (f3){
var p5=this.CheckTitleInfo(f3);
if (p5!=null)return p5;
var g7=this.GetData(f3);
var g8=g7.getElementsByTagName("root")[0];
if (g8==null)return null;
if (document.all!=null){
p5=g7.createNode("element","titleinfo","");
}else {
p5=document.createElement("titleinfo");
p5.style.display="none";
}
g8.appendChild(p5);
return p5;
}
this.setTitleInfo_Attribute=function (f3,attname,z1,recalc){
var at1=this.AddTitleInfo(f3);
at1.setAttribute(attname,z1);
var h0=this.GetCmdBtn(f3,"Update");
if (h0!=null&&h0.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(h0,false);
h0=this.GetCmdBtn(f3,"Cancel");
if (h0!=null&&h0.getAttribute("disabled")!=null)
this.UpdateCmdBtnState(h0,false);
f3.f1=true;
if (recalc){
this.UpdateValues(f3);
}
}
this.GetTitleInfo=function (f3)
{
var at2=this.GetTitleBar(f3);
if (at2){
at2.GetHeight=function (){
return document.defaultView.getComputedStyle(this,"").getPropertyValue("height");
}
at2.SetHeight=function (h5){
this.style.height=parseInt(h5)+"px";
the_fpSpread.setTitleInfo_Attribute(f3,"ht",h5);
var g2=the_fpSpread.GetTopSpread(f3);
the_fpSpread.SizeAll(g2);
the_fpSpread.Refresh(g2);
the_fpSpread.SaveClientEditedDataRealTime();
}
at2.GetVisible=function (){
return (document.defaultView.getComputedStyle(this,"").getPropertyValue("display")=="none")?false:true;
return document.defaultView.getComputedStyle(this,"").getPropertyValue("visibility");
}
at2.SetVisible=function (h5){
this.style.display=h5?"":"none";
this.style.visibility=h5?"visible":"hidden";
the_fpSpread.setTitleInfo_Attribute(f3,"vs",new String(h5).toLocaleLowerCase());
var g2=the_fpSpread.GetTopSpread(f3);
the_fpSpread.SizeAll(g2);
the_fpSpread.Refresh(g2);
the_fpSpread.SaveClientEditedDataRealTime();
}
at2.GetValue=function (){
return this.textContent;
}
at2.SetValue=function (h5){
this.textContent=""+h5;
the_fpSpread.setTitleInfo_Attribute(f3,"tx",h5);
the_fpSpread.SaveClientEditedDataRealTime();
}
return at2;
}
return null;
}
this.SaveClientEditedDataRealTime=function (){
var at3=this.GetPageActiveSpread();
if (at3!=null){
this.SaveData(at3);
at3.f1=false;
}
at3=this.GetPageActiveSheetView();
if (at3!=null){
this.SaveData(at3);
at3.f1=false;
}
}
var at4="";
this.ShowScrollingContent=function (f3,hs){
var u6="";
var r5=this.GetTopSpread(f3);
var at5=r5.getAttribute("scrollContentColumns");
var at6=r5.getAttribute("scrollContentMaxHeight");
var at7=r5.getAttribute("scrollContentTime");
var k0=this.GetViewport(r5);
var at8=this.GetColGroup(k0);
var p1=this.GetParent(k0);
var at9=0;
if (hs){
var au0=p1.scrollLeft;
var d5=this.GetColHeader(r5);
var w3=0;
for (;w3<at8.childNodes.length;w3++){
var i6=at8.childNodes[w3];
at9+=i6.offsetWidth;
if (at9>au0)break ;
}
var au1=this.GetViewport2(r5);
if (au1)w3+=this.GetColGroup(au1).childNodes.length;
if (d5){
var v7=d5.rows.length-1;
if (f3.getAttribute("LayoutMode")==null)
v7=d5.getAttribute("ColTextIndex")?d5.getAttribute("ColTextIndex"):d5.rows.length-1;
var au2=this.GetHeaderCellFromRowCol(r5,v7,w3,true);
if (au2){
if (au2.getAttribute("FpCellType")=="ExtenderCellType"&&au2.getElementsByTagName("DIV").length>0){
var ab6=this.GetEditor(au2);
var ab7=this.GetFunction("ExtenderCellType_getEditorValue");
if (ab6!=null&&ab7!=null){
u6="&nbsp;Column:&nbsp;"+ab7(ab6)+"&nbsp;";
}
}
else 
u6="&nbsp;Column:&nbsp;"+au2.innerHTML+"&nbsp;";
}
}
if (u6.length<=0)u6="&nbsp;Column:&nbsp;"+(w3+1)+"&nbsp;"
}
else {
var p8=p1.scrollTop;
var d4=this.GetRowHeader(r5);
var v7=0;
var au3=0;
var au4=2;
for (var ac6=0;ac6<k0.rows.length;ac6++){
var i4=k0.rows[ac6];
at9+=i4.offsetHeight;
if (at9>p8){
if (i4.getAttribute("fpkey")==null&&!i4.getAttribute("previewrow"))
v7--;
else 
au3=i4.offsetHeight;
break ;
}
if (i4.getAttribute("fpkey")!=null||i4.getAttribute("previewrow")){
v7++;
au3=i4.offsetHeight;
}
}
var au1=this.GetViewport1(r5);
if (au1)v7+=au1.rows.length;
if (f3.getAttribute("LayoutMode")==null&&at5!=null&&at5.length>0){
au3=au3>at6?at6:au3;
var au5=at5.split(",");
var au6=false;
for (var f5=0;f5<au5.length;f5++){
var i6=parseInt(au5[f5]);
if (i6==null||i6>=this.GetColCount(f3))continue ;
var i7=r5.GetCellByRowCol(v7,i6);
if (!i7||i7.getAttribute("col")!=null&&i7.getAttribute("col")!=i6)continue ;
var au7=(i7.getAttribute("group")==1);
var o4=(i7.parentNode.getAttribute("previewrow")!=null);
var h4=(i7.getAttribute("RowEditTemplate")!=null);
var l3=this.IsXHTML(f3);
if (!l3&&at4==""){
this.GetScrollingContentStyle(f3);
if (ao8!=null){
if (ao8.fontFamily!=null&&ao8.fontFamily!="")at4+="fontFamily:"+ao8.fontFamily+";";
if (ao8.fontSize!=null&&ao8.fontSize!="")at4+="fontSize:"+ao8.fontSize+";";
if (ao8.fontStyle!=null&&ao8.fontStyle!="")at4+="fontStyle:"+ao8.fontStyle+";";
if (ao8.fontVariant!=null&&ao8.fontVariant!="")at4+="fontVariant:"+ao8.fontVariant+";";
if (ao8.fontWeight!=null&&ao8.fontWeight!="")at4+="fontWeight:"+ao8.fontWeight+";";
if (ao8.backgroundColor!=null&&ao8.backgroundColor!="")at4+="backgroundColor:"+ao8.backgroundColor+";";
if (ao8.color!=null&&ao8.color!="")at4+="color:"+ao8.color;
}
}
if (!au6){
u6+="<div style='overflow:hidden;height:"+au3+"px;ScrollingContentWidth'><table cellPadding='0' cellSpacing='0' style='height:"+au3+"px;"+(au7?"":"table-layout:auto;")+at4+"'><tr>";
}
u6+="<td style='width:"+(au7?0:i7.offsetWidth)+"px;'>";
au4+=i7.offsetWidth;
if (au7)
u6+="&nbsp;<i>GroupBar:</i>&nbsp;"+i7.textContent+"&nbsp;";
else if (o4)
u6+="&nbsp;<i>PreviewRow:</i>&nbsp;"+i7.textContent+"&nbsp;";
else if (h4){
var au8=this.parseCell(f3,i7);
u6+="&nbsp;<i>RowEditTemplate:</i>&nbsp;"+au8+"&nbsp;"
}
else {
if (i7.getAttribute("fpcelltype"))this.UpdateCellTypeDOM(i7);
if (i7.getAttribute("fpcelltype")=="MultiColumnComboBoxCellType"&&i7.childNodes[0]&&i7.childNodes[0].childNodes.length>0&&i7.childNodes[0].getAttribute("MccbId"))
u6+=r5.GetValue(v7,i6);
else if (i7.getAttribute("fpcelltype")=="RadioButtonListCellType"||i7.getAttribute("fpcelltype")=="ExtenderCellType"||i7.getAttribute("fpeditorid")!=null){
var au9=this.parseCell(f3,i7);
u6+=au9;
}
else 
u6+=i7.innerHTML;
}
u6+="</td>";
au6=true;
if (au7||o4||h4)break ;
}
if (au6){
u6=u6.replace("ScrollingContentWidth"," width:"+au4+"px;");
u6+="</tr></table></div>";
}
}
if (u6.length<=0&&d4){
var w3=this.GetColGroup(d4).childNodes.length-1;
if (f3.getAttribute("LayoutMode")==null)
w3=d4.getAttribute("RowTextIndex")?parseInt(d4.getAttribute("RowTextIndex")):this.GetColGroup(d4).childNodes.length-1;
var k3=this.GetDisplayIndex(f3,v7);
var au2=this.GetHeaderCellFromRowCol(f3,k3,w3,false);
if (au2)u6="&nbsp;Row:&nbsp;"+au2.textContent+"&nbsp;";
}
if (u6.length<=0){
var f6=(f3.getAttribute("layoutrowcount")!=null)?parseInt(f3.getAttribute("layoutrowcount")):1;
u6="&nbsp;Row:&nbsp;"+(parseInt(v7/f6)+1)+"&nbsp;";
}
}
this.ShowMessageInner(r5,u6,(hs?-1:-2),(hs?-2:-1),at7);
}
this.parseCell=function (f3,i7){
var u6=i7.innerHTML;
var r5=this.GetTopSpread(f3);
var av0=r5.id;
if (u6.length>0){
u6=u6.replace(new RegExp("=\""+av0,"g"),"=\""+av0+"src");
u6=u6.replace(new RegExp("name="+av0,"g"),"name="+av0+"src");
}
return u6;
}
this.UpdateCellTypeDOM=function (i7){
for (var f5=0;f5<i7.childNodes.length;f5++){
if (i7.childNodes[f5].tagName&&(i7.childNodes[f5].tagName=="INPUT"||i7.childNodes[f5].tagName=="SELECT"))
this.UpdateDOM(i7.childNodes[f5]);
if (i7.childNodes[f5].childNodes&&i7.childNodes[f5].childNodes.length>0)
this.UpdateCellTypeDOM(i7.childNodes[f5]);
}
}
this.UpdateDOM=function (inputField){
if (typeof(inputField)=="string"){
inputField=document.getElementById(inputField);
}
if (inputField.type=="select-one"){
for (var f5=0;f5<inputField.options.length;f5++){
if (f5==inputField.selectedIndex){
inputField.options[inputField.selectedIndex].setAttribute("selected","selected");
}
}
}
else if (inputField.type=="text"){
inputField.setAttribute("value",inputField.value);
}
else if (inputField.type=="textarea"){
inputField.setAttribute("value",inputField.value);
}
else if ((inputField.type=="checkbox")||(inputField.type=="radio")){
if (inputField.checked){
inputField.setAttribute("checked","checked");
}else {
inputField.removeAttribute("checked");
}
}
}
this.GetScrollingContentStyle=function (f3){
if (ao8!=null)return ;
var f4=document.styleSheets.length;
for (var f5=0;f5<f4;f5++){
var av1=document.styleSheets[f5];
for (var j4=0;j4<av1.cssRules.length;j4++){
var av2=av1.cssRules[j4];
if (av2.selectorText=="."+f3.id+"scrollContentStyle"){
ao8=av2.style;
break ;
}
}
if (ao8!=null)break ;
}
}
}
function ComboBoxCellType_setValue(z5,z1,f3){
var i7=the_fpSpread.GetCell(z5);
if (i7==null)return ;
var av3=i7.getElementsByTagName("SELECT");
if (av3!=null&&av3.length>0){
av3[0].value=z1;
return 
}
var x7=the_fpSpread.GetCellEditorID(f3,i7);
var b7=null;
if (x7!=null&&typeof(x7)!="undefined"){
b7=the_fpSpread.GetCellEditor(f3,x7,true);
if (b7!=null){
b7.value=z1;
if (b7.selectedIndex>=0&&b7.selectedIndex<b7.options.length)
z1=b7.options[b7.selectedIndex].text;
if (z1!=null&&z1!="")
z5.innerHTML=z1;
else 
z5.innerHTML="&nbsp;";
}
}
}
function ComboBoxCellType_getValue(z5,f3){
var x6=z5.innerHTML;
var k6=the_fpSpread.GetCell(z5);
var av3=k6.getElementsByTagName("SELECT");
if (av3!=null&&av3.length>0){
return av3[0].value;
}
var x7=the_fpSpread.GetCellEditorID(f3,k6);
var b7=null;
if (x7!=null&&typeof(x7)!="undefined"){
b7=the_fpSpread.GetCellEditor(f3,x7,true);
if (b7!=null){
var f4=b7.options.length;
for (var f5=0;f5<f4;f5++){
if (b7.options[f5].text==x6){
return b7.options[f5].value;
}
}
return null;
}
}
return x6;
}
function CheckBoxCellType_setFocus(i7){
var j8=i7.getElementsByTagName("INPUT");
if (j8!=null&&j8.length>0&&j8[0].type=="checkbox"){
j8[0].focus();
}
}
function CheckBoxCellType_getCheckBoxEditor(i7){
var j8=i7.getElementsByTagName("INPUT");
if (j8!=null&&j8.length>0&&j8[0].type=="checkbox"){
return j8[0];
}
return null;
}
function CheckBoxCellType_isValid(i7,z1){
if (z1==null)return "";
z1=the_fpSpread.Trim(z1);
if (z1=="")return "";
if (z1.toLowerCase()=="true"||z1.toLowerCase()=="false")
return "";
else 
return "invalid value";
}
function CheckBoxCellType_getValue(z5,f3){
return CheckBoxCellType_getEditorValue(z5,f3);
}
function CheckBoxCellType_getEditorValue(z5,f3){
var i7=the_fpSpread.GetCell(z5);
var j8=CheckBoxCellType_getCheckBoxEditor(i7);
if (j8!=null&&j8.checked){
return "true";
}
return "false";
}
function CheckBoxCellType_setValue(z5,z1){
var i7=the_fpSpread.GetCell(z5);
var j8=CheckBoxCellType_getCheckBoxEditor(i7);
if (j8!=null){
j8.checked=(z1!=null&&z1.toLowerCase()=="true");
return ;
}
}
function IntegerCellType_getValue(z5){
var h1=z5;
while (h1.firstChild!=null&&h1.firstChild.nodeName!="#text")h1=h1.firstChild;
if (h1.innerHTML=="&nbsp;")return "";
var x6=h1.innerHTML;
z5=the_fpSpread.GetCell(z5);
if (z5.getAttribute("FpRef")!=null)z5=document.getElementById(z5.getAttribute("FpRef"));
var av4=z5.getAttribute("groupchar");
if (av4==null)av4=",";
var y2=x6.length;
while (true){
x6=x6.replace(av4,"");
if (x6.length==y2)break ;
y2=x6.length;
}
if (x6.charAt(0)=='('&&x6.charAt(x6.length-1)==')'){
var av5=z5.getAttribute("negsign");
if (av5==null)av5="-";
x6=av5+x6.substring(1,x6.length-1);
}
x6=the_fpSpread.ReplaceAll(x6,"&nbsp;"," ");
return x6;
}
function IntegerCellType_isValid(i7,z1){
if (z1==null||z1.length==0)return "";
z1=the_fpSpread.Trim(z1)
if (z1.length==0)return "";
var at9=i7;
var av6=i7.getAttribute("FpRef");
if (av6!=null)at9=document.getElementById(av6);
var av5=at9.getAttribute("negsign");
var aa7=at9.getAttribute("possign");
if (av5!=null)z1=z1.replace(av5,"-");
if (aa7!=null)z1=z1.replace(aa7,"+");
if (z1.charAt(z1.length-1)=="-")z1="-"+z1.substring(0,z1.length-1);
var y4=new RegExp("^\\s*[-\\+]?\\d+\\s*$");
var q8=(z1.match(y4)!=null);
if (q8)q8=!isNaN(z1);
if (q8){
var y8=at9.getAttribute("MinimumValue");
var k5=at9.getAttribute("MaximumValue");
var y7=parseInt(z1);
if (y8!=null){
y8=parseInt(y8);
q8=(!isNaN(y8)&&y7>=y8);
}
if (q8&&k5!=null){
k5=parseInt(k5);
q8=(!isNaN(k5)&&y7<=k5);
}
}
if (!q8){
if (at9.getAttribute("error")!=null)
return at9.getAttribute("error");
else 
return "Integer";
}
return "";
}
function DoubleCellType_isValid(i7,z1){
if (z1==null||z1.length==0)return "";
var at9=i7;
if (i7.getAttribute("FpRef")!=null)at9=document.getElementById(i7.getAttribute("FpRef"));
var av7=at9.getAttribute("decimalchar");
if (av7==null)av7=".";
var av4=at9.getAttribute("groupchar");
if (av4==null)av4=",";
z1=the_fpSpread.Trim(z1);
var q8=true;
q8=(z1.length==0||z1.charAt(0)!=av4);
if (q8){
var f4=z1.indexOf(av7);
if (f4>=0){
f4=z1.indexOf(av4,f4);
q8=(f4<0);
}
}
if (q8){
var y2=z1.length;
while (true){
z1=z1.replace(av4,"");
if (z1.length==y2)break ;
y2=z1.length;
}
}
if (z1.length==0){
q8=false;
}else if (q8){
var av5=at9.getAttribute("negsign");
var aa7=at9.getAttribute("possign");
var y8=at9.getAttribute("MinimumValue");
var k5=at9.getAttribute("MaximumValue");
q8=the_fpSpread.IsDouble(z1,av7,av5,aa7,y8,k5);
}
if (!q8){
if (at9.getAttribute("error")!=null)
return at9.getAttribute("error");
else 
return "Double";
}
return "";
}
function DoubleCellType_getValue(z5){
var h1=z5;
while (h1.firstChild!=null&&h1.firstChild.nodeName!="#text")h1=h1.firstChild;
if (h1.innerHTML=="&nbsp;")return "";
var x6=h1.innerHTML;
z5=the_fpSpread.GetCell(z5);
if (z5.getAttribute("FpRef")!=null)z5=document.getElementById(z5.getAttribute("FpRef"));
var av4=z5.getAttribute("groupchar");
if (av4==null)av4=",";
var y2=x6.length;
while (true){
x6=x6.replace(av4,"");
if (x6.length==y2)break ;
y2=x6.length;
}
if (x6.charAt(0)=='('&&x6.charAt(x6.length-1)==')'){
var av5=z5.getAttribute("negsign");
if (av5==null)av5="-";
x6=av5+x6.substring(1,x6.length-1);
}
x6=the_fpSpread.ReplaceAll(x6,"&nbsp;"," ");
return x6;
}
function CurrencyCellType_isValid(i7,z1){
if (z1!=null&&z1.length>0){
var at9=i7;
if (i7.getAttribute("FpRef")!=null)at9=document.getElementById(i7.getAttribute("FpRef"));
var y1=at9.getAttribute("currencychar");
if (y1==null)y1="$";
z1=z1.replace(y1,"");
var av4=at9.getAttribute("groupchar");
if (av4==null)av4=",";
var av7=at9.getAttribute("decimalchar");
if (av7==null)av7=".";
z1=the_fpSpread.Trim(z1);
var q8=true;
q8=(z1.length==0||z1.charAt(0)!=av4);
if (q8){
var f4=z1.indexOf(av7);
if (f4>=0){
f4=z1.indexOf(av4,f4);
q8=(f4<0);
}
}
if (q8){
var y2=z1.length;
while (true){
z1=z1.replace(av4,"");
if (z1.length==y2)break ;
y2=z1.length;
}
}
var q8=true;
if (z1.length==0){
q8=false;
}else if (q8){
var av5=at9.getAttribute("negsign");
var aa7=at9.getAttribute("possign");
var y8=at9.getAttribute("MinimumValue");
var k5=at9.getAttribute("MaximumValue");
q8=the_fpSpread.IsDouble(z1,av7,av5,aa7,y8,k5);
}
if (!q8){
if (at9.getAttribute("error")!=null)
return at9.getAttribute("error");
else 
return "Currency ("+y1+"100"+av7+"10) ";
}
}
return "";
}
function CurrencyCellType_getValue(z5){
var h1=z5;
while (h1.firstChild!=null&&h1.firstChild.nodeName!="#text")h1=h1.firstChild;
if (h1.innerHTML=="&nbsp;")return "";
var x6=h1.innerHTML;
z5=the_fpSpread.GetCell(z5);
if (z5.getAttribute("FpRef")!=null)z5=document.getElementById(z5.getAttribute("FpRef"));
var y1=z5.getAttribute("currencychar");
if (y1!=null){
var av8=document.createElement("SPAN");
av8.innerHTML=y1;
y1=av8.innerHTML;
}
if (y1==null)y1="$";
var av4=z5.getAttribute("groupchar");
if (av4==null)av4=",";
x6=x6.replace(y1,"");
var y2=x6.length;
while (true){
x6=x6.replace(av4,"");
if (x6.length==y2)break ;
y2=x6.length;
}
var av5=z5.getAttribute("negsign");
if (av5==null)av5="-";
if (x6.charAt(0)=='('&&x6.charAt(x6.length-1)==')'){
x6=av5+x6.substring(1,x6.length-1);
}
x6=the_fpSpread.ReplaceAll(x6,"&nbsp;"," ");
return x6;
}
function RegExpCellType_isValid(i7,z1){
if (z1==null||z1=="")
return "";
var at9=i7;
if (i7.getAttribute("FpRef")!=null)at9=document.getElementById(i7.getAttribute("FpRef"));
var av9=new RegExp(at9.getAttribute("fpexpression"));
var y5=z1.match(av9);
var p1=(y5!=null&&y5.length>0&&z1==y5[0]);
if (!p1){
if (at9.getAttribute("error")!=null)
return at9.getAttribute("error");
else 
return "invalid";
}
return "";
}
function PercentCellType_getValue(z5){
var h1=z5;
while (h1.firstChild!=null&&h1.firstChild.nodeName!="#text")h1=h1.firstChild;
if (h1.innerHTML=="&nbsp;")return "";
h1=h1.innerHTML;
var i7=the_fpSpread.GetCell(z5);
var at9=i7;
if (i7.getAttribute("FpRef")!=null)at9=document.getElementById(i7.getAttribute("FpRef"));
var aw0=at9.getAttribute("percentchar");
if (aw0==null)aw0="%";
h1=h1.replace(aw0,"");
var av4=at9.getAttribute("groupchar");
if (av4==null)av4=",";
var y2=h1.length;
while (true){
h1=h1.replace(av4,"");
if (h1.length==y2)break ;
y2=h1.length;
}
var av5=at9.getAttribute("negsign");
var aa7=at9.getAttribute("possign");
h1=the_fpSpread.ReplaceAll(h1,"&nbsp;"," ");
var h7=h1;
if (av5!=null)
h1=h1.replace(av5,"-");
if (aa7!=null)
h1=h1.replace(aa7,"+");
var av7=at9.getAttribute("decimalchar");
if (av7!=null)
h1=h1.replace(av7,".");
if (!isNaN(h1))
return h7;
else 
return z5.innerHTML;
}
function PercentCellType_setValue(z5,z1){
var h1=z5;
while (h1.firstChild!=null&&h1.firstChild.nodeName!="#text")h1=h1.firstChild;
z5=h1;
if (z1!=null&&z1!=""){
var at9=the_fpSpread.GetCell(z5);
if (at9.getAttribute("FpRef")!=null)at9=document.getElementById(at9.getAttribute("FpRef"));
var aw0=at9.getAttribute("percentchar");
if (aw0==null)aw0="%";
z1=z1.replace(" ","");
z1=z1.replace(aw0,"");
z5.innerHTML=z1+aw0;
}else {
z5.innerHTML="";
}
}
function PercentCellType_isValid(i7,z1){
if (z1!=null){
var at9=the_fpSpread.GetCell(i7);
if (at9.getAttribute("FpRef")!=null)at9=document.getElementById(at9.getAttribute("FpRef"));
var aw0=at9.getAttribute("percentchar");
if (aw0==null)aw0="%";
z1=z1.replace(aw0,"");
var av4=at9.getAttribute("groupchar");
if (av4==null)av4=",";
var y2=z1.length;
while (true){
z1=z1.replace(av4,"");
if (z1.length==y2)break ;
y2=z1.length;
}
var aw1=z1;
var av5=at9.getAttribute("negsign");
var aa7=at9.getAttribute("possign");
if (av5!=null)z1=z1.replace(av5,"-");
if (aa7!=null)z1=z1.replace(aa7,"+");
var av7=at9.getAttribute("decimalchar");
if (av7!=null)
z1=z1.replace(av7,".");
var q8=!isNaN(z1);
if (q8){
var aw2=at9.getAttribute("MinimumValue");
var aw3=at9.getAttribute("MaximumValue");
if (aw2!=null||aw3!=null){
var y8=parseFloat(aw2);
var k5=parseFloat(aw3);
q8=!isNaN(y8)&&!isNaN(k5);
if (q8){
if (av7==null)av7=".";
q8=the_fpSpread.IsDouble(aw1,av7,av5,aa7,y8*100,k5*100);
}
}
}
if (!q8){
if (at9.getAttribute("error")!=null)
return at9.getAttribute("error");
else 
return "Percent:(ex,10"+aw0+")";
}
}
return "";
}
function ListBoxCellType_getValue(z5){
var h1=z5.getElementsByTagName("TABLE");
if (h1.length>0)
{
var i2=h1[0].rows;
for (var j4=0;j4<i2.length;j4++){
var i7=i2[j4].cells[0];
if (i7.selected=="true")
{
var aw4=i7;
while (aw4.firstChild!=null)aw4=aw4.firstChild;
var at9=aw4.nodeValue;
return at9;
}
}
}
return "";
}
function ListBoxCellType_setValue(z5,z1){
var h1=z5.getElementsByTagName("TABLE");
if (h1.length>0)
{
h1[0].style.width=(z5.clientWidth-6)+"px";
var i2=h1[0].rows;
for (var j4=0;j4<i2.length;j4++){
var i7=i2[j4].cells[0];
var aw4=i7;
while (aw4.firstChild!=null)aw4=aw4.firstChild;
var at9=aw4.nodeValue;
if (at9==z1){
i7.selected="true";
if (h1[0].parentNode.getAttribute("selectedBackColor")!="undefined")
i7.style.backgroundColor=h1[0].parentNode.getAttribute("selectedBackColor");
if (h1[0].parentNode.getAttribute("selectedForeColor")!="undefined")
i7.style.color=h1[0].parentNode.getAttribute("selectedForeColor");
}else {
i7.style.backgroundColor="";
i7.style.color="";
i7.selected="";
i7.bgColor="";
}
}
}
}
function TextCellType_getValue(z5){
var i7=the_fpSpread.GetCell(z5,true);
if (i7!=null&&i7.getAttribute("password")!=null){
if (i7!=null&&i7.getAttribute("value")!=null)
return i7.getAttribute("value");
else 
return "";
}else {
var h1=z5;
while (h1.firstChild!=null&&h1.firstChild.nodeName!="#text")h1=h1.firstChild;
if (h1.innerHTML=="&nbsp;")return "";
if (h1!=null){
if (h1.tagName=="INPUT")
h1=h1.value;
else 
h1=the_fpSpread.HTMLDecode(h1.innerHTML);
}
h1=the_fpSpread.ReplaceAll(h1,"<br>","\n");
return h1;
}
}
function TextCellType_setValue(z5,z1){
var i7=the_fpSpread.GetCell(z5,true);
if (i7==null)return ;
var h1=z5;
while (h1.firstChild!=null&&h1.firstChild.nodeName!="#text")h1=h1.firstChild;
z5=h1;
if (i7.getAttribute("password")!=null){
if (z1!=null&&z1!=""){
z1=z1.replace(" ","");
z5.innerHTML="";
for (var f5=0;f5<z1.length;f5++)
z5.innerHTML+="*";
i7.setAttribute("value",z1);
}else {
z5.innerHTML="";
i7.setAttribute("value","");
}
}else {
if (z1!=null)z1=the_fpSpread.HTMLEncode(z1);
z1=the_fpSpread.ReplaceAll(z1,"\n","<br>");
z5.innerHTML=z1;
}
}
function TextCellType_setEditorValue(h1,z1){
if (z1!=null)z1=the_fpSpread.HTMLDecode(z1);
h1.value=z1;
}
function RadioButtonListCellType_getValue(z5){
var i7=the_fpSpread.GetCell(z5,true);
if (i7==null)return ;
var aw5=i7.getElementsByTagName("INPUT");
for (var f5=0;f5<aw5.length;f5++){
if (aw5[f5].tagName=="INPUT"&&aw5[f5].checked){
return aw5[f5].value;
}
}
return "";
}
function RadioButtonListCellType_getEditorValue(z5){
return RadioButtonListCellType_getValue(z5);
}
function RadioButtonListCellType_setValue(z5,z1){
var i7=the_fpSpread.GetCell(z5,true);
if (i7==null)return ;
if (z1!=null)z1=the_fpSpread.Trim(z1);
var aw5=i7.getElementsByTagName("INPUT");
for (var f5=0;f5<aw5.length;f5++){
if (aw5[f5].tagName=="INPUT"&&z1==the_fpSpread.Trim(aw5[f5].value)){
aw5[f5].checked=true;
break ;
}else {
if (aw5[f5].checked)aw5[f5].checked=false;
}
}
}
function RadioButtonListCellType_setFocus(z5){
var i7=the_fpSpread.GetCell(z5,true);
if (i7==null)return ;
var j8=i7.getElementsByTagName("INPUT");
if (j8==null)return ;
for (var f5=0;f5<j8.length;f5++){
if (j8[f5].type=="radio"&&j8[f5].checked){
j8[f5].focus();
return ;
}
}
}
function MultiColumnComboBoxCellType_setValue(z5,z1,f3){
var i7=the_fpSpread.GetCell(z5,true);
if (i7==null)return ;
var av3=i7.getElementsByTagName("DIV");
if (av3!=null&&av3.length>0){
var aw6=i7.getElementsByTagName("input");
if (aw6!=null&&aw6.length>0)
aw6[0].value=z1;
return ;
}
if (z1!=null&&z1!="")
z5.textContent=z1;
else 
z5.innerHTML="&nbsp;";
}
function MultiColumnComboBoxCellType_getValue(z5,f3){
var x6=z5.textContent;
var k6=the_fpSpread.GetCell(z5,true);
var av3=k6.getElementsByTagName("DIV");
if (av3!=null&&av3.length>0){
var aw6=k6.getElementsByTagName("input");
if (aw6!=null&&aw6.length>0)
return aw6[0].value;
return ;
}
if (!f3)return null;
var x7=the_fpSpread.GetCellEditorID(f3,k6);
var b7=null;
if (x7!=null&&typeof(x7)!="undefined"){
b7=the_fpSpread.GetCellEditor(f3,x7,true);
if (b7!=null){
var aw7=b7.getAttribute("MccbId");
if (aw7){
FarPoint.System.WebControl.MultiColumnComboBoxCellType.CheckInit(aw7);
var ak5=eval(aw7+"_Obj");
if (ak5!=null&&ak5.SetText!=null){
ak5.SetText(x6);
return x6;
}
}
}
return null;
}
return x6;
}
function MultiColumnComboBoxCellType_getEditorValue(z5,f3){
var i7=the_fpSpread.GetCell(z5,true);
if (i7==null)return ;
var aw8=i7.getElementsByTagName("INPUT");
if (aw8!=null&&aw8.length>0){
var h1=aw8[0];
return h1.value;
}
return null;
}
function MultiColumnComboBoxCellType_setFocus(z5){
var i7=the_fpSpread.GetCell(z5);
var f3=the_fpSpread.GetSpread(i7);
if (i7==null)return ;
var aw9=i7.getElementsByTagName("DIV");
if (aw9!=null&&aw9.length>0){
var aw7=aw9[0].getAttribute("MccbId");
if (aw7){
var ak5=eval(aw7+"_Obj");
if (ak5!=null&&typeof(ak5.FocusForEdit)!="undefined"){
ak5.FocusForEdit();
}
}
}
}
function MultiColumnComboBoxCellType_setEditorValue(z5,editorValue,f3){
var i7=the_fpSpread.GetCell(z5,true);
if (i7==null)return ;
var x7=the_fpSpread.GetCellEditorID(f3,i7);
var b7=null;
if (x7!=null&&typeof(x7)!="undefined"){
b7=the_fpSpread.GetCellEditor(f3,x7,true);
if (b7!=null){
var aw7=b7.getAttribute("MccbId");
if (aw7){
FarPoint.System.WebControl.MultiColumnComboBoxCellType.CheckInit(aw7);
var ak5=eval(aw7+"_Obj");
if (ak5!=null&&ak5.SetText!=null){
ak5.SetText(editorValue);
}
}
}
}
}
function TagCloudCellType_getValue(z5,f3){
var x6=z5.textContent;
if (typeof(x6)!="undefined"&&x6!=null&&x6.length>0)
{
x6=the_fpSpread.ReplaceAll(x6,"<br>","");
x6=the_fpSpread.ReplaceAll(x6,"\n","");
x6=the_fpSpread.ReplaceAll(x6,"\t","");
var u7=new RegExp("\xA0","g");
x6=x6.replace(u7,String.fromCharCode(32));
x6=the_fpSpread.HTMLDecode(x6);
}
else 
x6="";
return x6;
}
if (typeof(Sys)!='undefined'&&typeof(Sys.Application)!='undefined'){
Sys.Application.notifyScriptLoaded();
}
