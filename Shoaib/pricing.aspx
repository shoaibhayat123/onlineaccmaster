﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StaticPages.master" AutoEventWireup="true"
    CodeFile="pricing.aspx.cs" Inherits="pricing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content_wraper5">
        <div id="content_midd5">
            <div class="accmiddle-bg">
                <div class="top-heading">
                    <h1>
                        Choose the Plan That’s Best for Your Business</h1>
                </div>
                <div class="slider-backgourng">
                    <div class="pricing-iamge-box">
                        <div class="currently-books">
                            <div class="books-title">
                                <h1>
                                    Setup and Activation Request :</h1>
                            </div>
                            <div class="books-details">
                                <span class="bookdtl">Initial one-time activation fee is applicable in initial setup
                                    of all versions. Different versions have different charges, to get details of charges
                                    mail your request to <a href="mailto:sales@accmaster.com">sales@accmaster.com</a></span><br />
                                
                            </div>
                        </div>
                        <div class="pricing-iamge-bor-right-section">
                            <div class="pricing-image-right-1">
                                <div class="online-image-content">
                                    <div class="onlinesimle-image" style="height: 67px">
                                        <img src="images/ie-icon.png" alt="" />
                                    </div>
                                    <div class="online-simple-heading">
                                        <span class="onlisimhe">Online AccMaster
                                            <br />
                                            Simple</span>
                                        <ul>
                                            <li><a href="Register.aspx">Try It Free</a></li>
                                        </ul>
                                    </div>
                                    <div class="no-conect-require">
                                        <span class="nocere">No contract,<br />
                                            cancel anytime</span>
                                    </div>
                                    <div class="free-days-30">
                                        <span class="freeda">30 days free, then
                                            <br />
                                            $<asp:Label ID="lblSimple" runat="server"></asp:Label></span>
                                    </div>
                                </div>
                            </div>
                            <div class="pricing-image-right-1">
                                <div class="online-image-content">
                                    <div class="onlinesimle-image" style="height: 67px">
                                        <img src="images/ie-icon.png" alt="" />
                                    </div>
                                    <div class="online-simple-heading2">
                                        <span class="onlisimhe">Online AccMaster<br />
                                            Basic</span>
                                        <ul>
                                            <li><a href="Register.aspx">Try It Free</a></li>
                                        </ul>
                                    </div>
                                    <div class="no-conect-require">
                                        <span class="nocere">No contract,<br />
                                            cancel anytime</span>
                                    </div>
                                    <div class="free-days-30">
                                        <span class="freeda">30 days free, then
                                            <br />
                                            $<asp:Label ID="lblBasic" runat="server"></asp:Label>/mo</span>
                                    </div>
                                </div>
                            </div>
                            <div class="pricing-image-right-1">
                                <div class="online-image-content">
                                    <div class="onlinesimle-image" style="height: 67px">
                                        <img src="images/ie-icon.png" alt="" />
                                    </div>
                                    <div class="online-simple-heading3">
                                        <span class="onlisimhe">Online AccMaster
                                            <br />
                                            <span class="withpay" style="color: #505050;">Extra</span></span>
                                        <ul>
                                            <li><a href="Register.aspx">Try It Free</a></li>
                                        </ul>
                                    </div>
                                    <div class="no-conect-require">
                                        <span class="nocere">No contract,<br />
                                            cancel anytime</span>
                                    </div>
                                    <div class="free-days-30">
                                        <span class="freeda">30 days free, then
                                            <br />
                                            $<asp:Label ID="lblExtra" runat="server"></asp:Label>/mo</span>
                                    </div>
                                </div>
                            </div>
                            <div class="pricing-image-right-1">
                                <div class="online-image-content">
                                    <div class="onlinesimle-image" style="height: 67px">
                                        <img src="images/ie-icon.png" alt="" />
                                    </div>
                                    <div class="online-simple-heading4">
                                        <span class="onlisimhe">Online AccMaster
                                            <br />
                                            <span class="withpay" style="color: #505050;">Plus Plus</span></span>
                                        <ul>
                                            <li><a href="Register.aspx">Try It Free</a></li>
                                        </ul>
                                    </div>
                                    <div class="no-conect-require">
                                        <span class="nocere">No contract,<br />
                                            cancel anytime</span>
                                    </div>
                                    <div class="free-days-30">
                                        <span class="freeda">30 days free, then
                                            <br />
                                            $<asp:Label ID="lblPlus" runat="server"></asp:Label>/mo</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="width: 994px; margin: 0px auto;">
                    <div class="background" style="background: #FFFFFF; width: 994px; float: left;">
                        <div style="float: left; background: none repeat scroll 0 0 #FFFFFF; width: 994px;
                            padding-bottom: 30px">
                            <table width="90%" border="1px" style="border-collapse: collapse; margin: auto;">
                                <tr>
                                    <td colspan="5" style="text-align: center; text-decoration: underline; background-color: #0F5744;
                                        color: White; text-decoration: underline; font-size: 17px; font-weight: bold;
                                        height: 35px;">
                                        Feature List
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Access (No. of Users Licenses)
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <asp:Label ID="lblSimpleUser" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <asp:Label ID="lblBasicUser" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <asp:Label ID="lblExtraUser" runat="server"></asp:Label>
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <asp:Label ID="lblPlusUser" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Requires no Installations.
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Compatibility on all web Browsers and Operating Systems (OS / Browser Independent)
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Mobile Access
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        24 Hrs. Free E-mail Support
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Password Protected
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Multiuser Environment
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Secured
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Auto Backup Facility
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Free from Database / System crashes
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="text-align: center; text-decoration: underline; background-color: #0F5744;
                                        color: White; text-decoration: underline; font-size: 17px; font-weight: bold;
                                        height: 35px;">
                                        Organizational Highlights
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Accounting
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Costing and Controlling
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Inventory Management
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Sales and Procurement
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        HR management
                                    </td>
                                    <td colspan="4" rowspan="5" style="text-align: center; vertical-align: middle">
                                        <b style="text-decoration: underline">Under Development</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; text-align: left; padding-left: 70px;">
                                        Manage Employees & Controllers
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; text-align: left; padding-left: 70px;">
                                        Time Tracking
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; text-align: left; padding-left: 70px;">
                                        Payroll Management
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; text-align: left; padding-left: 70px;">
                                        Human Resource Time Management
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Taxation Issues (Regional Taxes)
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Reports related to all the above define modules
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Graphical Analysis
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        <img src="images/check-box.png" width="15" height="16" alt="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" style="text-align: center; text-decoration: underline; background-color: #0F5744;
                                        color: White; text-decoration: underline; font-size: 17px; font-weight: bold;
                                        height: 35px;">
                                        Customized Industry Specific Solution. (Please ask for an quotation)
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Manufacturing
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        Optional
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        IP law firms
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        Optional
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Automobile Trading (NEW / USED)
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        Optional
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Retails
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        Optional
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Logistics and Warehousing
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        Optional
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Travels and Tours.
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        Optional
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        POS (FMCG)
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        Optional
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        BAR CODE Compatibility
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        Optional
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 35%; padding-left: 10px; font-weight: bold;">
                                        Supply Chain Management
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 15%; text-align: center;">
                                        Optional
                                    </td>
                                    <td style="width: 20%; text-align: center;">
                                        Optional
                                    </td>
                                </tr>
                            </table>
                        </div>
               
</asp:Content>
