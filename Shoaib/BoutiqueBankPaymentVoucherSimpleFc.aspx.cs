﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class BoutiqueBankPaymentVoucherSimpleFc : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();
    int Last = 0;
    int Corrent = 0;
    int FirstRecord = 0;
    int LastRecord = 0;
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();

        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        if (!IsPostBack)
        {
            Session["s"] = null;
            txtVoucherNo.Focus();
            FillAllLedgerAc();
            DataTable dt = new DataTable();
            dgGallery.DataSource = dt;
            dgGallery.DataBind();
            Session["Corrent"] = null;
            Session["FirstRecord"] = null;
            Session["LastRecord"] = null;
            Session["BankReceiptSummary"] = null;
            if (Convert.ToString(Request["addnew"]) != "1")
            {

                bindpagging();
                if (Convert.ToString(Request["addnew"]) != "1")
                {

                    bindpagging();
                    if (Request["VoucherSimpleMainId"] == null)
                    {
                        BindBankReceipt();
                    }
                    else
                    {
                        DataTable dtInvoiceSummary = new DataTable();
                        dtInvoiceSummary = (DataTable)Session["BankReceiptSummary"];
                        DataView dv = new DataView(dtInvoiceSummary);
                        dv.RowFilter = "id=" + Request["VoucherSimpleMainId"];
                        dtInvoiceSummary = dv.ToTable();
                        if (dtInvoiceSummary.Rows.Count > 0)
                        {
                            Session["Corrent"] = Convert.ToInt32(dtInvoiceSummary.Rows[0]["row"]);
                            BindBankReceipt();
                        }
                        else
                        {
                            Response.Redirect("default.aspx");
                        }
                    }

                }
                else
                {
                    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                    objSalesInvoice.VoucherSimpleMainId = 0;
                    DataTable dtBRV = new DataTable();
                    objSalesInvoice.DataFrom = "BPVFC";
                    dtBRV = objSalesInvoice.getVoucherSimpleMain();
                    if (dtBRV.Rows.Count > 0)
                    {
                        int Last = Convert.ToInt32(dtBRV.Rows.Count) - 1;
                        ddlBankAC.SelectedValue = Convert.ToString(dtBRV.Rows[Last]["BankAcId"]);
                        txtVoucherNo.Text = Convert.ToString(Convert.ToDecimal(dtBRV.Rows[Last]["VoucherNo"]) + 1);
                    }
                    spanPgging.Style.Add("display", "none");
                    txtVoucherDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
                    txtChqDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
                }

            }
            else
            {
                objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objSalesInvoice.VoucherSimpleMainId = 0;
                DataTable dtBPV = new DataTable();
                objSalesInvoice.DataFrom = "BPVFC";
                dtBPV = objSalesInvoice.getVoucherSimpleMain();
                if (dtBPV.Rows.Count > 0)
                {
                    int Last = Convert.ToInt32(dtBPV.Rows.Count) - 1;
                    ddlBankAC.SelectedValue = Convert.ToString(dtBPV.Rows[Last]["BankAcId"]);
                    txtVoucherNo.Text = Convert.ToString(Convert.ToDecimal(dtBPV.Rows[Last]["VoucherNo"]) + 1);
                    txtVoucherDate.Text = Convert.ToDateTime(dtBPV.Rows[Last]["VoucherDate"]).ToString("dd MMM yyyy");
                    txtChqDate.Text = Convert.ToDateTime(dtBPV.Rows[Last]["VoucherDate"]).ToString("dd MMM yyyy");
                }

                else
                {
                    txtVoucherDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
                    txtChqDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
                    txtVoucherNo.Text = "1";
                }
                spanPgging.Style.Add("display", "none");

                //txtVoucherDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");

            }
        }
        SetUserRight();
    }


    #endregion
    #region Fill Accounts

    protected void FillAllLedgerAc()
    {
        
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtLedger = new DataTable();
            dtLedger = objSalesInvoice.getAcFileAllLedger();
            if (dtLedger.Rows.Count > 0)
            {

                ddlAcdesc.DataSource = dtLedger;
                ddlAcdesc.DataTextField = "Title";
                ddlAcdesc.DataValueField = "Id";
                ddlAcdesc.DataBind();
                ddlAcdesc.Items.Insert(0, new ListItem("Select A/C Title", "0"));



            }


            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtClient = new DataTable();
            dtClient = objSalesInvoice.getAcFileByBankaccount();
            if (dtClient.Rows.Count > 0)
            {

                ddlBankAC.DataSource = dtClient;
                ddlBankAC.DataTextField = "Title";
                ddlBankAC.DataValueField = "Id";
                ddlBankAC.DataBind();
                ddlBankAC.Items.Insert(0, new ListItem("Select Bank A/C ", "0"));

            }

        

    }
    #endregion

    #region Add Details
    protected void btn_saveInvoice_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
        if (Sessions.FromDate != null)
        {
            if (Session["s"] != null)
            {
                if ((Convert.ToDateTime(txtVoucherDate.Text) >= Convert.ToDateTime(Sessions.FromDate)) && (Convert.ToDateTime(txtVoucherDate.Text) <= Convert.ToDateTime(Sessions.ToDate)))
                {
                    if (Convert.ToDecimal(lblAmount.Text) >= 0)
                    {
                        if (hdnVoucherSimpleMainId.Value == "")
                        {
                            hdnVoucherSimpleMainId.Value = "0";
                        }
                        /************ Add Invoice Summary **************/
                        if (Convert.ToInt32(hdnVoucherSimpleMainId.Value) > 0)
                        {
                            objSalesInvoice.id = Convert.ToInt32(hdnVoucherSimpleMainId.Value);
                        }
                        else
                        {
                            objSalesInvoice.id = 0;
                        }
                        objSalesInvoice.VoucherNo = Convert.ToDecimal(txtVoucherNo.Text);
                        objSalesInvoice.VoucherDate = Convert.ToDateTime(txtVoucherDate.Text);
                        objSalesInvoice.BankAcId = Convert.ToInt32(ddlBankAC.SelectedValue);
                        objSalesInvoice.ChqNo = Convert.ToString(txtChqNo.Text);
                        objSalesInvoice.ChqDate = Convert.ToDateTime(txtChqDate.Text);
                        objSalesInvoice.Description = Convert.ToString(txtDesForMain.Text);
                        objSalesInvoice.DataFrom = "BPVFC";
                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                        objSalesInvoice.TotalAmount = Convert.ToDecimal(lblAmount.Text);
                        objSalesInvoice.TotalAmountFC = Convert.ToDecimal(lblFcamount.Text);
                        DataTable dtresult = new DataTable();
                        dtresult = objSalesInvoice.AddEditVoucherSimpleMain();

                        /************ Add Trans ***************/

                        objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["id"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                        objSalesInvoice.DataFrom = "BPVFC";
                        objSalesInvoice.DeleteTrans();


                        objSalesInvoice.TransId = 0;
                        objSalesInvoice.TransactionDate = Convert.ToDateTime(txtVoucherDate.Text);
                        objSalesInvoice.AcFileId = Convert.ToInt32(ddlBankAC.SelectedValue);
                        objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(lblAmount.Text);
                        objSalesInvoice.Description = Convert.ToString(txtDesForMain.Text);
                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                        objSalesInvoice.DataFrom = "BPVFC";
                        objSalesInvoice.InvoiceNo = txtVoucherNo.Text;
                        objSalesInvoice.ChqNo = Convert.ToString(txtChqNo.Text);
                        objSalesInvoice.ChqDate = Convert.ToDateTime(txtChqDate.Text);
                        objSalesInvoice.FCAmount = 0 - Convert.ToDecimal(lblFcamount.Text);
                        if (Convert.ToDecimal(lblFcamount.Text) > 0)
                        {
                            objSalesInvoice.FCRate = Convert.ToDecimal(Convert.ToDecimal(lblAmount.Text) / Convert.ToDecimal(lblFcamount.Text));
                        }
                        else
                        {
                            objSalesInvoice.FCRate = 0;
                        }

                        if (Session["s"] != null)
                        {
                            DataTable dt = (DataTable)Session["s"];
                            if (dt.Rows.Count > 0)
                            {
                                objSalesInvoice.ReceiptNo = Convert.ToString(dt.Rows[0]["RNumber"].ToString());
                            }
                        }

                        objSalesInvoice.AddEditTrans();




                        /************ Add Invoice Detail **************/
                        if (dtresult.Rows.Count > 0)
                        {
                            if (Session["s"] != null)
                            {
                                DataTable dt = new DataTable();
                                dt = (DataTable)Session["s"];
                                if (dt.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        objSalesInvoice.id = 0;
                                        objSalesInvoice.AccountId = Convert.ToInt32(dt.Rows[i]["AccountId"].ToString());
                                        objSalesInvoice.Amount = Convert.ToDecimal(dt.Rows[i]["Amount"].ToString());
                                        objSalesInvoice.RNumber = Convert.ToString(dt.Rows[i]["RNumber"].ToString());
                                        objSalesInvoice.Description = Convert.ToString(dt.Rows[i]["Description"].ToString());
                                        objSalesInvoice.VoucherSimpleMainId = Convert.ToInt32(dtresult.Rows[0]["id"].ToString()); /***** dtresult.Rows[0] is for VoucherSimpleMainId which is new****/
                                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                        objSalesInvoice.DataFrom = "BPVFC";
                                        DataTable dtInvoiceSummary = new DataTable();
                                        objSalesInvoice.FCAmount = Convert.ToDecimal(dt.Rows[i]["FCAmount"].ToString());
                                        objSalesInvoice.FCRate = Convert.ToDecimal(dt.Rows[i]["FCRate"].ToString());
                                        dtInvoiceSummary = objSalesInvoice.AddEditVoucherSimpleDetail();

                                        /********* Add in trans **************/
                                        objSalesInvoice.TransId = 0;
                                        objSalesInvoice.TransactionDate = Convert.ToDateTime(txtVoucherDate.Text);
                                        objSalesInvoice.AcFileId = Convert.ToInt32(dt.Rows[i]["AccountId"].ToString());
                                        objSalesInvoice.TransAmt = Convert.ToDecimal(dt.Rows[i]["Amount"].ToString());
                                        objSalesInvoice.Description = Convert.ToString(dt.Rows[i]["Description"].ToString());
                                        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                        objSalesInvoice.DataFrom = "BPVFC";
                                        objSalesInvoice.InvoiceNo = txtVoucherNo.Text;

                                        objSalesInvoice.FCAmount = Convert.ToDecimal(dt.Rows[i]["FCAmount"].ToString());
                                        objSalesInvoice.FCRate = Convert.ToDecimal(dt.Rows[i]["FCRate"].ToString());

                                        objSalesInvoice.ChqNo = Convert.ToString(txtChqNo.Text);
                                        objSalesInvoice.ChqDate = Convert.ToDateTime(txtChqDate.Text);
                                        objSalesInvoice.ReceiptNo = Convert.ToString(dt.Rows[i]["RNumber"].ToString());
                                        objSalesInvoice.AddEditTrans();


                                    }
                                }
                            }

                        }


                        if (Convert.ToInt32(hdnVoucherSimpleMainId.Value) > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Updated.');location.href='BoutiqueBankPaymentVoucherSimpleFc.aspx';", true);

                        }

                        else
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Saved.');location.href='BoutiqueBankPaymentVoucherSimpleFc.aspx';", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Invalid Invoice Amount.');", true);

                    }

                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Date out of accouting year.');", true);

                }

            }


            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please Fill  Details.');", true);
            }
        }
    }


    #endregion

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("BoutiqueBankPaymentVoucherSimpleFc.aspx"); 
    }
    #endregion
    #region Add New Bill details
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
        //      int flag=0;
        //if (Convert.ToDecimal(txtFcRate.Text) == 0 && Convert.ToDecimal(txtFcAmount.Text) == 0)
        //{

        //    flag = 1;
        //}

        //if ((Convert.ToDecimal(txtFcRate.Text) * Convert.ToDecimal(txtFcAmount.Text)) ==  Convert.ToDecimal(txtAmount.Text))
        
        //{

        //    flag = 1;

        //}


        //if (flag == 1)
        //{

            filltable();
            if (hdndetail.Value == "")
            {
                DataRow row;
                if (dttbl.Rows.Count > 0)
                {

                    int maxid = 0;
                    for (int i = 0; i < dttbl.Rows.Count; i++)
                    {
                        if (dttbl.Rows[i]["Id"].ToString() != "")
                        {
                            if (maxid < Convert.ToInt32(dttbl.Rows[i]["Id"].ToString()))
                            {
                                maxid = Convert.ToInt32(dttbl.Rows[i]["Id"].ToString());
                            }
                        }
                    }

                    row = dttbl.NewRow();
                    row["Id"] = maxid + 1;
                    row["AccountId"] = Convert.ToInt32(ddlAcdesc.SelectedValue.ToString());
                    row["AccountDesc"] = Convert.ToString(ddlAcdesc.SelectedItem.Text);
                    row["Amount"] = Convert.ToDecimal(txtAmount.Text);
                    row["RNumber"] = Convert.ToString(txtNumber.Text).ToUpper();
                    row["Description"] = Convert.ToString(txtDescriptionDetail.Text);
                    row["FCAmount"] = Convert.ToDecimal(txtFcAmount.Text);
                    row["FCRate"] = Convert.ToDecimal(txtFcRate.Text);


                }
                else
                {

                    row = dttbl.NewRow();
                    row["Id"] = 1;
                    row["AccountId"] = Convert.ToInt32(ddlAcdesc.SelectedValue.ToString());
                    row["AccountDesc"] = Convert.ToString(ddlAcdesc.SelectedItem.Text);
                    row["Amount"] = Convert.ToDecimal(txtAmount.Text);
                    row["RNumber"] = Convert.ToString(txtNumber.Text).ToUpper();
                    row["Description"] = Convert.ToString(txtDescriptionDetail.Text);
                    row["FCAmount"] = Convert.ToDecimal(txtFcAmount.Text);
                    row["FCRate"] = Convert.ToDecimal(txtFcRate.Text);


                }
                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
            }
            else
            {
                for (int i = 0; i < dttbl.Rows.Count; i++)
                {
                    if (dttbl.Rows[i]["Id"].ToString() == hdndetail.Value)
                    {
                        dttbl.Rows[i]["AccountId"] = Convert.ToInt32(ddlAcdesc.SelectedValue);
                        dttbl.Rows[i]["AccountDesc"] = Convert.ToString(ddlAcdesc.SelectedItem.Text);
                        dttbl.Rows[i]["Amount"] = Convert.ToDecimal(txtAmount.Text);
                        dttbl.Rows[i]["RNumber"] = Convert.ToString(txtNumber.Text).ToUpper();
                        dttbl.Rows[i]["Description"] = Convert.ToString(txtDescriptionDetail.Text);
                        dttbl.Rows[i]["FCAmount"] = Convert.ToDecimal(txtFcAmount.Text);
                        dttbl.Rows[i]["FCRate"] = Convert.ToDecimal(txtFcRate.Text);

                    }
                }
            }


            Session.Add("s", dttbl);
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            ShowTotal();
            if (dttbl.Rows.Count > 0)
            {
                dgGallery.Visible = true;
            }

            txtDesForMain.Text = txtDescriptionDetail.Text;
            ClearInvoiceDetail();
            ddlAcdesc.Focus();

        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please Enter Valid amount.');", true);
        //}
    }
    #endregion

    #region Grid Event
    protected void dgGallery_RowDataBound(object sender, DataGridItemEventArgs e)
    {




    }
    protected void dgGallery_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        dttbl = (DataTable)Session["s"];
        if (e.CommandName == "Delete")
        {
            int j = 0;
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    dttbl.Rows.RemoveAt(i);
                }
            }
            dttbl.AcceptChanges();
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            Session.Add("s", dttbl);
            ShowTotal();

        }
        if (e.CommandName == "Edit")
        {
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {

                    ddlAcdesc.SelectedValue = dttbl.Rows[i]["AccountId"].ToString();
                    txtAmount.Text = dttbl.Rows[i]["Amount"].ToString();
                    txtNumber.Text = dttbl.Rows[i]["RNumber"].ToString();
                    txtDescriptionDetail.Text = dttbl.Rows[i]["Description"].ToString();
                    hdndetail.Value = dttbl.Rows[i]["Id"].ToString();
                    txtFcAmount.Text = dttbl.Rows[i]["FcAmount"].ToString();
                    txtFcRate.Text = dttbl.Rows[i]["FcRate"].ToString();

                }
            }
        }



    }
    #endregion

    #region ClearInvoiceDetail
    protected void ClearInvoiceDetail()
    {
        ddlAcdesc.SelectedValue = "0";
        txtDescriptionDetail.Text = "";
        txtAmount.Text = "";
        txtNumber.Text = "";
        txtDescriptionDetail.Text = "";
        hdndetail.Value = "";
        txtFcRate.Text = "0.00";
        txtFcAmount.Text = "0.00";
    }
    #endregion

    #region Show Total
    protected void ShowTotal()
    {
        Decimal Amount = 0;
        Decimal Fcamount = 0;
        DataTable dt = new DataTable();
        dt = (DataTable)Session["s"];
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Amount += Convert.ToDecimal(dt.Rows[i]["Amount"]);
                Fcamount += Convert.ToDecimal(dt.Rows[i]["Fcamount"]);
            }
        }
        lblAmount.Text = String.Format("{0:C}", Amount).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        lblFcamount.Text = String.Format("{0:C}", Fcamount).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');


    }
    #endregion
    #region Pagging
    private void bindpagging()
    {
        Last = 0;
        Corrent = 0;
        FirstRecord = 0;
        LastRecord = 0;
        //  int row = Convert.ToInt32(Request["row"]);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.VoucherSimpleMainId = 0;
        DataTable dt = new DataTable();
        objSalesInvoice.DataFrom = "BPVFC";
        dt = objSalesInvoice.getVoucherSimpleMain();
        if (dt.Rows.Count > 0)
        {
            Session["BankReceiptSummary"] = dt;
            Last = dt.Rows.Count;
            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            Session["FirstRecord"] = FirstRecord;
            Session["LastRecord"] = LastRecord;
            Session["Corrent"] = LastRecord;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "location.href='BoutiqueBankPaymentVoucherSimpleFc.aspx?addnew=1';", true);
        }


    }
    protected void lnkFirst_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["FirstRecord"];
        BindBankReceipt();

    }
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["FirstRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) - 1;
            BindBankReceipt();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["LastRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) + 1;
            BindBankReceipt();
        }
    }
    protected void lnLast_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["LastRecord"];
        BindBankReceipt();
    }
    #endregion

    #region Fill Bank Receipt summary
    protected void BindBankReceipt()
    {
        if (Session["BankReceiptSummary"] != null)
        {
            DataTable dtBankReceiptSummary = (DataTable)Session["BankReceiptSummary"];
            if (dtBankReceiptSummary.Rows.Count > 0)
            {

                DataView DvBankReceiptSummary = dtBankReceiptSummary.DefaultView;
                DvBankReceiptSummary.RowFilter = "Row = '" + Session["Corrent"].ToString() + "'";
                dtBankReceiptSummary = DvBankReceiptSummary.ToTable();
                if (dtBankReceiptSummary.Rows.Count > 0)
                {
                    txtVoucherNo.Text = Convert.ToString(dtBankReceiptSummary.Rows[0]["VoucherNo"]);
                    txtVoucherDate.Text = Convert.ToDateTime(dtBankReceiptSummary.Rows[0]["VoucherDate"].ToString()).ToString("dd MMM yyyy");
                    ddlBankAC.SelectedValue = Convert.ToString(dtBankReceiptSummary.Rows[0]["BankAcId"]);
                    txtChqNo.Text = Convert.ToString(dtBankReceiptSummary.Rows[0]["ChqNo"]);
                    txtChqDate.Text = Convert.ToDateTime(dtBankReceiptSummary.Rows[0]["ChqDate"].ToString()).ToString("dd MMM yyyy");
                    txtDesForMain.Text = Convert.ToString(dtBankReceiptSummary.Rows[0]["Description"]);
                    lblAmount.Text = Convert.ToString(String.Format("{0:C}", dtBankReceiptSummary.Rows[0]["TotalAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    lblFcamount.Text = Convert.ToString(String.Format("{0:C}", dtBankReceiptSummary.Rows[0]["TotalAmountFC"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    hdnVoucherSimpleMainId.Value = Convert.ToString(dtBankReceiptSummary.Rows[0]["id"]);
                    fillBankReceiptDetails(Convert.ToInt32(dtBankReceiptSummary.Rows[0]["id"]));





                }
            }

        }

    }
    #endregion
    protected void fillBankReceiptDetails(int pid)
    {
        Session["s"] = null;

        DataTable dtBPVSimpleDetail = new DataTable();
        objSalesInvoice.VoucherSimpleMainId = pid;
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objSalesInvoice.DataFrom = "BPVFC";
        dtBPVSimpleDetail = objSalesInvoice.getVoucherSimpleDetail();

        if (dtBPVSimpleDetail.Rows.Count > 0)
        {
            for (int i = 0; i < dtBPVSimpleDetail.Rows.Count; i++)
            {
                filltable();
                DataRow row;
                row = dttbl.NewRow();
                row["Id"] = i + 1;
                row["AccountId"] = Convert.ToInt32(dtBPVSimpleDetail.Rows[i]["AccountId"]);
                row["AccountDesc"] = Convert.ToString(dtBPVSimpleDetail.Rows[i]["AccountDesc"]);
                row["Amount"] = Convert.ToDecimal(dtBPVSimpleDetail.Rows[i]["Amount"]);
                row["RNumber"] = Convert.ToString(dtBPVSimpleDetail.Rows[i]["RNumber"]);
                row["Description"] = Convert.ToString(dtBPVSimpleDetail.Rows[i]["Description"]);
                row["FCRate"] = Convert.ToDecimal(dtBPVSimpleDetail.Rows[i]["FCRate"]);
                row["FCAmount"] = Convert.ToDecimal(dtBPVSimpleDetail.Rows[i]["FCAmount"]);

                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
                Session.Add("s", dttbl);
            }
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            dgGallery.Visible = true;

        }
        else
        {
            dgGallery.Visible = false;
        }

    }


    #region Fill session Table
    public void filltable()
    {
        if (Session["s"] == null)
        {
            DataColumn column = new DataColumn();
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "AccountId";
            column.Caption = "AccountId";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "AccountDesc";
            column.Caption = "AccountDesc";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Amount";
            column.Caption = "Amount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "RNumber";
            column.Caption = "RNumber";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Description";
            column.Caption = "Description";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "FCAmount";
            column.Caption = "FCAmount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "FCRate";
            column.Caption = "FCRate";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

        }
        else
        {
            dttbl = (DataTable)Session["s"];
        }
    }
    #endregion


    #region Delete BankReceiptVoucher
    protected void btnDeleteInvoice_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(hdnVoucherSimpleMainId.Value) > 0)
        {
            int result = 0;
            objSalesInvoice.VoucherSimpleMainId = Convert.ToInt32(hdnVoucherSimpleMainId.Value);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.DataFrom = "BPVFC";
            result = objSalesInvoice.deleteAllVoucher();
            Session["BankReceiptSummary"] = null;
            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record deleted successfully.');location.href='BoutiqueBankPaymentVoucherSimpleFc.aspx';", true);
            }
        }

    }
    #endregion
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        string urllast = "BoutiquePrintBankPaymentVoucherSimpleFc.aspx?VoucherSimpleMainId=" + hdnVoucherSimpleMainId.Value;
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,status=yes' );", true);
    }
    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }

            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
            {
                btnDeleteInvoice.Visible = false;
            }
            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

                btnPrint.Visible = false;

            }

            if (Convert.ToString(dt.Rows[0]["IsAdd"]) != "True")
            {
                lblbutton.Enabled = false;
                if (Convert.ToString(Request["addnew"]) == "1")
                {

                    ImageButton2.Visible = false;
                }
            }



            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {
                if (Convert.ToString(Request["addnew"]) != "1")
                {

                    ImageButton2.Visible = false;
                }
            }

        }
    }
    protected void lblbutton_Click(object sender, EventArgs e)
    {

        Response.Redirect("BoutiqueBankPaymentVoucherSimpleFc.aspx?addnew=1");

    } 
}