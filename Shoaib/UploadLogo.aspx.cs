﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Microsoft.VisualBasic;
using System.Drawing;
using System.Drawing.Imaging;

public partial class UploadLogo : System.Web.UI.Page
{
    #region DataMembers & varriables
    SetCustomers objCust = new SetCustomers();
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (!IsPostBack)
        {
           
            if (Sessions.CustomerCode != null && Sessions.CustomerCode != "")
            {
                 FillData();
            }
        }
        SetUserRight();
    }
    #endregion

    #region Update Date Format
    protected void btn_save_Click(object sender, EventArgs e)
    {

        if (flPicture.HasFile == true)
        {
            if (IsValidFile(flPicture.FileName))
            {
                //Determine type and filename of uploaded image
                string UploadedImageType = flPicture.PostedFile.ContentType.ToString().ToLower();
                string UploadedImageFileName = flPicture.PostedFile.FileName;

                //Create an image object from the uploaded file
                System.Drawing.Image UploadedImage = System.Drawing.Image.FromStream(flPicture.PostedFile.InputStream);

                //Determine width and height of uploaded image
                float UploadedImageWidth = UploadedImage.PhysicalDimension.Width;
                float UploadedImageHeight = UploadedImage.PhysicalDimension.Height;

                //Check that image does not exceed maximum dimension settings
                if (UploadedImageWidth > 220 || UploadedImageHeight > 110)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please select 220*110px image!!!');", true);
                
                    // Response.Write("Please select 1000*165px image!");
                }

                else
                {
                    string CompanyLogo = UploadImage(flPicture);
                  objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                  objCust.CompanyLogo = CompanyLogo;
                  objCust.UpdateDateFormat();
                  ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Logo successful updated!');", true);
                  Response.Redirect("UploadLogo.aspx");
                }
            }

        }
    }

    protected bool IsValidFile(string FileName)
    {
        bool functionReturnValue = false;
        string strExtension = null;
        strExtension = System.IO.Path.GetExtension(FileName);

        if ((strExtension == ".jpg" | strExtension == ".JPG" | strExtension == ".jpeg" | strExtension == ".JPEG" | strExtension == ".gif" | strExtension == ".GIF" | strExtension == ".bmp" | strExtension == ".BMP" | strExtension == ".png" | strExtension == ".PNG"))
        {
            functionReturnValue = true;
        }
        else
        {
            functionReturnValue = false;
        }
        return functionReturnValue;
    }
    #endregion

    #region Fill Logo 
    private void FillData()
    {
        DataSet ds = new DataSet();
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ds = objCust.GetDateFormat();
        if (Convert.ToString(ds.Tables[0].Rows[0]["CompanyLogo"].ToString()) != "")
        {
            imgLogo.ImageUrl = "~/images/Logo/" + ds.Tables[0].Rows[0]["CompanyLogo"].ToString();

        }
        else
        {
            imgLogo.Style.Add("display", "none");
        }


    }
    #endregion


    private string UploadImage(FileUpload FileUpload1)
    {

        if (FileUpload1.HasFile == true)
        {
            if (FileUpload1.PostedFile.FileName != "")
            {
                string strImage = null;
                string FileName = string.Empty;
                string strExtension = "";

                string TempPath = "~/images/Logo/";
                strExtension = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                string Extension = System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                if (Extension == ".jpg" || Extension == ".jpeg" || Extension == ".png" || Extension == ".gif")
                {
                    string strImageName = System.Guid.NewGuid().ToString().Replace("-", "") + strExtension;
                    string StrImagePath = Server.MapPath("~/images/Logo/");
                    FileUpload1.PostedFile.SaveAs(Server.MapPath(TempPath + strImageName));                 
                    strImage = strImageName;                  
                    return strImage;

                }
                else
                {
                    Page.RegisterStartupScript("Msg1", "<script>alert('Please select file with .jpg/.jpeg/.png/.gif extension');</script>");
                    return "";

                }
            }
        }
        else
        {
            Page.RegisterStartupScript("Msg1", "<script>alert('Please Upload Image');</script>");
        }
        return "";

    }

    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }
            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }

            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {

                btn_save.Enabled = false;

            }

        }
    }
}
