﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Careers : System.Web.UI.Page
{
    Car objCar = new Car();
    protected void Page_Load(object sender, EventArgs e)
    {
        FillAddress();
    }
    protected void FillAddress()
    {
        objCar.id = 1;
        DataTable dt = objCar.getAccAddress();
        if (dt.Rows.Count > 0)
        {
            ltrlAddress.Text = dt.Rows[0]["Address"].ToString();
        }
    }
}