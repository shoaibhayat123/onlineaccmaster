﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class CreateUser : System.Web.UI.Page
{
    clsCookie ck = new clsCookie();
    User objUser = new User();
    SetCustomers objCust = new SetCustomers();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Sessions.CustomerCode == "")
            {
                if (ck.GetCookieValue("CustomerCode") != "")
                {
                    Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));

                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
            if (Sessions.CustomerCode != null)
            {
                txtCustomerCode.Text = Sessions.CustomerCode.ToString();
                FillAllUser();
                BindPages();
            }

           
        }
    }

    protected void BindPages()
    {
        objCust.BusinessTypeId = Convert.ToInt32(Sessions.IndId);
        objCust.CatId = 0;
        DataTable dt = new DataTable();
        dt = objCust.getSubMenuFront();
        DataView dv = dt.DefaultView;
        dv.RowFilter = "SubCatid <> 4";
        dt = dv.ToTable();

        dgGallery.DataSource = dt;
        dgGallery.DataBind();



        for (int i = 0; i < dt.Rows.Count; i++)
        {
            CheckBox chkAccess = (CheckBox)dgGallery.Items[i].FindControl("chkAccess");
            if (dt.Rows[i]["SubCatid"].ToString() == "2" || dt.Rows[i]["SubCatid"].ToString() == "3" || dt.Rows[i]["SubCatid"].ToString() == "4" || dt.Rows[i]["SubCatid"].ToString() == "9" || dt.Rows[i]["SubCatid"].ToString() == "64" || dt.Rows[i]["SubCatid"].ToString() == "66" || dt.Rows[i]["SubCatid"].ToString() == "67" || dt.Rows[i]["SubCatid"].ToString() == "68" || dt.Rows[i]["SubCatid"].ToString() == "69" || dt.Rows[i]["SubCatid"].ToString() == "70" || dt.Rows[i]["SubCatid"].ToString() == "71" || dt.Rows[i]["SubCatid"].ToString() == "72" || dt.Rows[i]["SubCatid"].ToString() == "73" || dt.Rows[i]["SubCatid"].ToString() == "81" || dt.Rows[i]["SubCatid"].ToString() == "82" || dt.Rows[i]["SubCatid"].ToString() == "83" || dt.Rows[i]["SubCatid"].ToString() == "84" || dt.Rows[i]["SubCatid"].ToString() == "85")
            {
                chkAccess.Checked = true;
            }

        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
       
        hdnButtonText.Value = "";
        objUser.CustomerCode = decimal.Parse(txtCustomerCode.Text);

        objUser.NewUserID = txtNewUserId.Text;
        objUser.UserPassword = txtNewPassword.Text;
        if (hdnUserId.Value.ToString() == "")
        {
            objUser.UserRole = "User";
            int retVal = objUser.SetUser();
            if (retVal > 0)
            {


                for (int i = 0; i < dgGallery.Items.Count; i++)
                {
                    CheckBox chkAccess = (CheckBox)dgGallery.Items[i].FindControl("chkAccess");
                    CheckBox chkAdd = (CheckBox)dgGallery.Items[i].FindControl("chkAdd");
                    CheckBox chkEdit = (CheckBox)dgGallery.Items[i].FindControl("chkEdit");
                    CheckBox chkDelete = (CheckBox)dgGallery.Items[i].FindControl("chkDelete");
                    CheckBox chkPrint = (CheckBox)dgGallery.Items[i].FindControl("chkPrint");
                    HiddenField hdId = (HiddenField)dgGallery.Items[i].FindControl("hdnSubCatid");
                    objUser.IsAccess = Convert.ToBoolean(chkAccess.Checked);
                    objUser.IsAdd = Convert.ToBoolean(chkAdd.Checked);
                    objUser.IsEditRight = Convert.ToBoolean(chkEdit.Checked);
                    objUser.IsDelete = Convert.ToBoolean(chkDelete.Checked);
                    objUser.IsPrint = Convert.ToBoolean(chkPrint.Checked);
                    objUser.Userid_UserRole = Convert.ToDecimal(retVal);
                    objUser.MenuId = Convert.ToInt32(hdId.Value);
                    DataTable dt = objUser.AddEditAccessRight();

                }

                BindPages();

                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('User created Successfuly');", true);

            }
            else if (retVal == -2)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('User add limit is completed.');", true);

            }
            else if (retVal == -1)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('UserId already define. Please use different userid.');", true);

            }

        }

        else
        {
            objUser.id = Convert.ToInt32(hdnUserId.Value);
            objUser.UpdateUsers();

            objUser.Userid_UserRole = Convert.ToDecimal(hdnUserId.Value); ;
            objUser.deleteaccessRight();

            for (int i = 0; i < dgGallery.Items.Count; i++)
            {
                CheckBox chkAccess = (CheckBox)dgGallery.Items[i].FindControl("chkAccess");
                CheckBox chkAdd = (CheckBox)dgGallery.Items[i].FindControl("chkAdd");
                CheckBox chkEdit = (CheckBox)dgGallery.Items[i].FindControl("chkEdit");
                CheckBox chkDelete = (CheckBox)dgGallery.Items[i].FindControl("chkDelete");
                CheckBox chkPrint = (CheckBox)dgGallery.Items[i].FindControl("chkPrint");
                HiddenField hdId = (HiddenField)dgGallery.Items[i].FindControl("hdnSubCatid");

                objUser.MenuId = Convert.ToInt32(hdId.Value);
                if (Convert.ToInt32(hdId.Value) == 2 || Convert.ToInt32(hdId.Value) == 85)
                {
                    objUser.IsAccess = true;
                }
                else
                {
                    objUser.IsAccess = Convert.ToBoolean(chkAccess.Checked);
                }

                objUser.IsAdd = Convert.ToBoolean(chkAdd.Checked);
                objUser.IsEditRight = Convert.ToBoolean(chkEdit.Checked);
                objUser.IsDelete = Convert.ToBoolean(chkDelete.Checked);
                objUser.IsPrint = Convert.ToBoolean(chkPrint.Checked);
                objUser.Userid_UserRole = Convert.ToDecimal(hdnUserId.Value);
                objUser.MenuId = Convert.ToInt32(hdId.Value);
                objUser.id = 0;
                DataTable dt = objUser.AddEditAccessRight();

            }

            BindPages();
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('User Detail has been Updated Successfuly');", true);
        }

        txtCustomerCode.Text = Sessions.CustomerCode.ToString();
        clear();
        FillAllUser();

        //if (dgGallery.Rows.Count >= 0)
        //{
        //    string abc = dgGallery.Rows[2].Cells[3].FindControl(
        //}

    }

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        clear();
    }
    #endregion

    #region Blank Controls
    private void clear()
    {
        txtNewPassword.Text = "";
        txtNewUserId.Text = "";
        hdnUserId.Value = "";
        txtNewPassword.Attributes.Add("value", "");
    }
    #endregion


    #region Fill Cost Center Setup
    protected void FillData(int id)
    {
        objUser.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objUser.id = id;
        DataTable tblAlluser = objUser.getAllUser();
        if (tblAlluser.Rows.Count > 0)
        {
            txtNewUserId.Text = Convert.ToString(tblAlluser.Rows[0]["UserID"].ToString());
            txtNewPassword.Attributes.Add("value", Convert.ToString(tblAlluser.Rows[0]["UserPassword"].ToString()));
            hdnUserId.Value = Convert.ToString(tblAlluser.Rows[0]["ID"].ToString());

            objUser.Userid_UserRole = id;
            DataTable tblUserRight = objUser.GetaccessRight();
            if (tblUserRight.Rows.Count > 0)
            {
                for (int i = 0; i < tblUserRight.Rows.Count ; i++)
                {
                    CheckBox chkAccess = (CheckBox)dgGallery.Items[i].FindControl("chkAccess");
                    CheckBox chkAdd = (CheckBox)dgGallery.Items[i].FindControl("chkAdd");
                    CheckBox chkEdit = (CheckBox)dgGallery.Items[i].FindControl("chkEdit");
                    CheckBox chkDelete = (CheckBox)dgGallery.Items[i].FindControl("chkDelete");
                    CheckBox chkPrint = (CheckBox)dgGallery.Items[i].FindControl("chkPrint");
                    HiddenField hdId = (HiddenField)dgGallery.Items[i].FindControl("hdnSubCatid");

                    chkAccess.Checked = Convert.ToBoolean(tblUserRight.Rows[i]["IsAccess"]);
                    chkAdd.Checked = Convert.ToBoolean(tblUserRight.Rows[i]["IsAdd"]);
                    chkEdit.Checked = Convert.ToBoolean(tblUserRight.Rows[i]["IsEdit"]);
                    chkDelete.Checked = Convert.ToBoolean(tblUserRight.Rows[i]["IsDelete"]);
                    chkPrint.Checked = Convert.ToBoolean(tblUserRight.Rows[i]["IsPrint"]);

                }

            }


        }

    }
    #endregion

    #region Fill Cost Center Setup
    protected void FillAllUser()
    {

        objUser.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objUser.id = 0;
        DataTable tblAlluser = objUser.getAllUser();
        if (tblAlluser.Rows.Count > 0)
        {
            if (tblAlluser.Rows.Count > 30)
            {
                grdCustomer.AllowPaging = true;
            }
            grdCustomer.Visible = true;
            grdCustomer.DataSource = tblAlluser;
            grdCustomer.DataBind();
            lblError.Visible = false;


            objUser.CustomerId = Convert.ToInt32(Sessions.CustomerCode);
            DataSet ds = new DataSet();
            ds = objUser.GetCustomerUserDetails();
            if (ds.Tables[0].Rows.Count > 0)
            {
                lblUser.Text = "Total Users &nbsp; " + tblAlluser.Rows.Count.ToString() + "/" + ds.Tables[0].Rows[0]["NumberOfUsers"].ToString(); 

            }
        }
        else
        {
            grdCustomer.Visible = false;
            lblError.Visible = true;
        }


    }
    #endregion

    #region GridView Events
    protected void grdCustomer_RowCommand(object sender, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            objUser.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
            objUser.id = int.Parse(e.CommandArgument.ToString());
            DataTable dt = objUser.DeleteUser();
            if (Convert.ToInt32(dt.Rows[0]["result"]) > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('User has been deleted Successfuly');", true);
            }

            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('You can not delete this user.');", true);
            }
            FillAllUser();
            clear();

        }

        if (e.CommandName == "Edit")
        {
            FillData(int.Parse(e.CommandArgument.ToString()));

        }
    }
    protected void grdCustomer_RowDataBound(object sender, DataGridItemEventArgs e)
    {
    }

    protected void grdCustomer_PageIndexChanged(object sender, DataGridPageChangedEventArgs e)
    {
        //grdCustomer.CurrentPageIndex = e.NewPageIndex;
        //FillAllUser();

    }
    #endregion
}
