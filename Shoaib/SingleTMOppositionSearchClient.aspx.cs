﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class SingleTMOppositionSearchClient : System.Web.UI.Page
{

    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    // SalesInvoices ObjTmcountry = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();
    DataTable dttbl2 = new DataTable();
    TmCountry ObjTmcountry = new TmCountry();
    int Last = 0;
    int Corrent = 0;
    int FirstRecord = 0;
    int LastRecord = 0;
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessionsClient();

        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        if (!IsPostBack)
        {
            txtOppsitionNo.Focus();
            Session["s"] = null;
            Session["s2"] = null;
            FillClient();
            FillStatus();
            BindYear();


            Session["Corrent"] = null;
            Session["FirstRecord"] = null;
            Session["LastRecord"] = null;
            Session["OppositionSummary"] = null;



            if (Convert.ToString(Request["addnew"]) != "1")
            {
                bindpagging();

                if (Request["TmOppositionId"] == null)
                {
                    // BindOpposition();
                }
                else
                {
                    DataTable dtInvoiceSummary = new DataTable();
                    dtInvoiceSummary = (DataTable)Session["OppositionSummary"];
                    DataView dv = new DataView(dtInvoiceSummary);
                    dv.RowFilter = "TmOppositionId=" + Request["TmOppositionId"];
                    dtInvoiceSummary = dv.ToTable();
                    if (dtInvoiceSummary.Rows.Count > 0)
                    {
                        Session["Corrent"] = Convert.ToInt32(dtInvoiceSummary.Rows[0]["row"]);
                        BindOpposition();
                    }
                    else
                    {
                        Response.Redirect("default.aspx");
                    }
                }
            }


            if (Request["TmOppositionId"] == null && Convert.ToString(Request["addnew"]) == "1")
            {

                spanPgging.Style.Add("display", "none");
                // btnDeleteInvoice.Style.Add("display", "none");
            }
        }
       // SetUserRight();
    }


    #endregion

    #region Add Details
    protected void btn_saveInvoice_Click(object sender, ImageClickEventArgs e)
    {

        if (Sessions.FromDate != null)
        {
            //if (Session["s"] != null)
            //{

            if (hdnOppositionMainId.Value == "")
            {
                hdnOppositionMainId.Value = "0";
            }
            /************ Add Invoice Summary **************/
            if (Convert.ToInt32(hdnOppositionMainId.Value) > 0)
            {
                ObjTmcountry.TmOppositionId = Convert.ToInt32(hdnOppositionMainId.Value);
            }
            else
            {
                ObjTmcountry.TmOppositionId = 0;
            }
            ObjTmcountry.OppositionNo = txtOppsitionNo.Text;
            ObjTmcountry.Year = Convert.ToInt32(ddlYear.SelectedValue);
            ObjTmcountry.OppositionType = ddlOppositiontype.SelectedValue.ToString();
            ObjTmcountry.OppositionStatus = ddlstatusOpposition.SelectedValue.ToString();
            ObjTmcountry.OppositionRemark = txtRemarksOpposition.Text;
            if (txtTradeMarkJournalNo.Text != "")
            {
                ObjTmcountry.TmJounalNo = Convert.ToInt32(txtTradeMarkJournalNo.Text);
            }
            if (txtJournalDate.Text != "")
            {
                ObjTmcountry.JournalDate = Convert.ToDateTime(txtJournalDate.Text);

            }
            if (txtDateOfPublication.Text != "")
            {
                ObjTmcountry.PublicationDate = Convert.ToDateTime(txtDateOfPublication.Text);
            }
            if (txtDateOfExtension.Text != "")
            {
                ObjTmcountry.Extension1 = Convert.ToDateTime(txtDateOfExtension.Text);
            }
            if (txtDateOfExtension2.Text != "")
            {
                ObjTmcountry.Extension2 = Convert.ToDateTime(txtDateOfExtension2.Text);
            }
            if (txtLastDateOfOpposition.Text != "")
            {
                ObjTmcountry.LastDateOfOpposition = Convert.ToDateTime(txtLastDateOfOpposition.Text);
            }
            ObjTmcountry.TmCustomerId = Convert.ToInt32(ddlOurClient.SelectedValue);
            if (txtApplicationNo.Text != "")
            {
                ObjTmcountry.ApplicationNum = Convert.ToInt32(txtApplicationNo.Text);
            }
            ObjTmcountry.TradeMark = txttradeMark.Text;

            ObjTmcountry.TmFileStatusId = Convert.ToInt32(ddlStatus.SelectedValue);

            if (txtApplicationNoDetails.Text != "")
            {
                ObjTmcountry.OtherSideTmApplicationNum = Convert.ToInt32(txtApplicationNoDetails.Text);
            }
            ObjTmcountry.OtherSideTmTradeMark = txtTradeMarkDetails.Text;

            ObjTmcountry.OtherSideTmClass = txtApplicationclassDetails.Text;
            if (txtApplicationFilledOn.Text != "")
            {
                ObjTmcountry.OtherSideTmFilledOn = Convert.ToDateTime(txtApplicationFilledOn.Text);
            }
            ObjTmcountry.OtherSideApplicationUser = txtApplicationUser.Text;
            ObjTmcountry.OthersideClientName = txtSideClientName.Text;
            ObjTmcountry.OtherSideClientAddress = txtSideClientAddress.Text;
            ObjTmcountry.OtherSideClientAdvocateName = txtOtherSideAdvocateName.Text;
            ObjTmcountry.OtherSideClientAdvocateAddress = txtOtherSideAdvocateAddress.Text;
            ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            ObjTmcountry.Reminder = txtReminder.Text;
            if (txtReminderDate.Text != "")
            {
                ObjTmcountry.ReminderDate = Convert.ToDateTime(txtReminderDate.Text);
            }
            ObjTmcountry.TmOppositionFileStatusId = Convert.ToInt32(ddlstatusOpposition.SelectedValue.ToString());
            DataTable dtresult = new DataTable();
            dtresult = ObjTmcountry.AddEditTmOpposition();


            /************ Add Invoice Detail **************/
            if (dtresult.Rows.Count > 0)
            {
                if (Session["s"] != null)
                {

                    DataTable dt = new DataTable();
                    dt = (DataTable)Session["s"];
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {

                            ObjTmcountry.TmHearingDetailId = 0;
                            ObjTmcountry.HearingDate = Convert.ToDateTime(dt.Rows[i]["HearingDate"].ToString());
                            ObjTmcountry.Before = Convert.ToString(dt.Rows[i]["Before"].ToString());
                            ObjTmcountry.Comment = Convert.ToString(dt.Rows[i]["Comment"].ToString());
                            ObjTmcountry.TmOppositionId = Convert.ToInt32(dtresult.Rows[0]["TmOppositionId"]);
                            ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            int DetailResult = ObjTmcountry.AddEditTmOppositionHearingDetails();
                        }
                    }
                }

            }
            if (dtresult.Rows.Count > 0)
            {
                if (Session["s2"] != null)
                {

                    DataTable dt = new DataTable();
                    dt = (DataTable)Session["s2"];
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {

                            ObjTmcountry.TmHearingDetailId = 0;
                            ObjTmcountry.DetailsDate = Convert.ToDateTime(dt.Rows[i]["DetailsDate"].ToString());
                            ObjTmcountry.DetailsRemark = Convert.ToString(dt.Rows[i]["DetailsRemark"].ToString());
                            ObjTmcountry.TmOppositionId = Convert.ToInt32(dtresult.Rows[0]["TmOppositionId"]);
                            ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            int DetailResult = ObjTmcountry.AddEditTmOppositionDetails();
                        }
                    }
                }

            }

            if (Convert.ToInt32(hdnOppositionMainId.Value) > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Updated.');location.href='TmOpposition.aspx';", true);

            }

            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Saved.');location.href='TmOpposition.aspx';", true);
            }


            //}


            //else
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please Fill  Details.');", true);
            //}
        }
    }
    #endregion
        
    #region Grid Event
    protected void dgGallery_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Label)e.Item.FindControl("lblHearingDate")).Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "HearingDate").ToString());

        }
    }
    protected void dgGallery_ItemCommand(object source, DataGridCommandEventArgs e)
    {

    }
    protected void dgForm_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Label)e.Item.FindControl("lblFillingDate")).Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "FillingDate").ToString());

        }



    }

    protected void dgdocument_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            string ImageName = Convert.ToString(DataBinder.Eval(e.Item.DataItem, "Image"));
            if (ImageName.ToLower().Contains(".jpg") || ImageName.ToLower().Contains(".jpeg") || ImageName.ToLower().Contains(".png") || ImageName.ToLower().Contains(".gif"))
            {

            }
            else
            {
                HtmlGenericControl d1 = ((HtmlGenericControl)e.Item.FindControl("d1"));
                d1.Style.Add("display", "none");

                //d1.Attributes.Remove("class");
                d1.InnerText = "";

                HtmlGenericControl imageDocument = ((HtmlGenericControl)e.Item.FindControl("imageDocument"));
                imageDocument.Style.Add("display", "");
            }




        }



    }
    protected void dgGallery2_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Label)e.Item.FindControl("lblDate")).Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "DetailsDate").ToString());

        }
    }
    protected void dgGallery2_ItemCommand(object source, DataGridCommandEventArgs e)
    {


    }
    #endregion

    #region ClearInvoiceDetail
    protected void ClearInvoiceDetail()
    {


    }
    protected void ClearInvoiceDetail2()
    {

    }
    #endregion

    #region Pagging
    private void bindpagging()
    {
        Last = 0;
        Corrent = 0;
        FirstRecord = 0;
        LastRecord = 0;
        //  int row = Convert.ToInt32(Request["row"]);
        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.TmOppositionId = 0;
        DataTable dt = new DataTable();
        DataSet ds = ObjTmcountry.getTmOpposition();
        dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            Session["OppositionSummary"] = dt;
            Last = dt.Rows.Count;
            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            Session["FirstRecord"] = FirstRecord;
            Session["LastRecord"] = LastRecord;
            Session["Corrent"] = LastRecord;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "location.href='TmOpposition.aspx?addnew=1';", true);
        }

    }
    protected void lnkFirst_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["FirstRecord"];
        BindOpposition();

    }
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["FirstRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) - 1;
            BindOpposition();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["LastRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) + 1;
            BindOpposition();
        }
    }
    protected void lnLast_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["LastRecord"];
        BindOpposition();
    }
    #endregion

    #region Fill Bank Receipt summary
    protected void BindOpposition()
    {
        if (Session["OppositionSummary"] != null)
        {
            DataTable dtOppositionSummary = (DataTable)Session["OppositionSummary"];
            if (dtOppositionSummary.Rows.Count > 0)
            {

                DataView DvOppositionSummary = dtOppositionSummary.DefaultView;
                DvOppositionSummary.RowFilter = "Row = '" + Session["Corrent"].ToString() + "'";
                dtOppositionSummary = DvOppositionSummary.ToTable();
                if (dtOppositionSummary.Rows.Count > 0)
                {

                    hdnOppositionMainId.Value = Convert.ToString(dtOppositionSummary.Rows[0]["TmOppositionId"]);
                    txtOppsitionNo.Text = Convert.ToString(dtOppositionSummary.Rows[0]["OppositionNo"]);
                    ddlYear.SelectedValue = Convert.ToString(dtOppositionSummary.Rows[0]["Year"]);
                    ddlOppositiontype.SelectedValue = Convert.ToString(dtOppositionSummary.Rows[0]["OppositionType"]);
                    ddlstatusOpposition.SelectedValue = Convert.ToString(dtOppositionSummary.Rows[0]["TmOppositionFileStatusId"]);
                    txtRemarksOpposition.Text = Convert.ToString(dtOppositionSummary.Rows[0]["OppositionRemark"]);
                    txtTradeMarkJournalNo.Text = Convert.ToString(dtOppositionSummary.Rows[0]["TmJounalNo"]);
                    if (Convert.ToString(dtOppositionSummary.Rows[0]["JournalDate"]) != "")
                    {
                        txtJournalDate.Text = Convert.ToDateTime(dtOppositionSummary.Rows[0]["JournalDate"].ToString()).ToString("dd MMM yyyy");
                    }
                    if (Convert.ToString(dtOppositionSummary.Rows[0]["PublicationDate"]) != "")
                    {
                        txtDateOfPublication.Text = Convert.ToDateTime(dtOppositionSummary.Rows[0]["PublicationDate"].ToString()).ToString("dd MMM yyyy");
                    }
                    if (Convert.ToString(dtOppositionSummary.Rows[0]["Extension1"]) != "")
                    {
                        txtDateOfExtension.Text = Convert.ToDateTime(dtOppositionSummary.Rows[0]["Extension1"].ToString()).ToString("dd MMM yyyy");
                    }

                    if (Convert.ToString(dtOppositionSummary.Rows[0]["Extension2"]) != "")
                    {
                        txtDateOfExtension2.Text = Convert.ToDateTime(dtOppositionSummary.Rows[0]["Extension2"].ToString()).ToString("dd MMM yyyy");
                    }

                    if (Convert.ToString(dtOppositionSummary.Rows[0]["LastDateOfOpposition"]) != "")
                    {
                        txtLastDateOfOpposition.Text = Convert.ToDateTime(dtOppositionSummary.Rows[0]["LastDateOfOpposition"].ToString()).ToString("dd MMM yyyy");
                    }
                    ddlOurClient.SelectedValue = Convert.ToString(dtOppositionSummary.Rows[0]["TmCustomerId"]);
                    txtApplicationNo.Text = Convert.ToString(dtOppositionSummary.Rows[0]["ApplicationNum"]);
                    txttradeMark.Text = Convert.ToString(dtOppositionSummary.Rows[0]["TradeMark"]);
                    ddlStatus.SelectedValue = Convert.ToString(dtOppositionSummary.Rows[0]["TmFileStatusId"]);
                    txtApplicationNoDetails.Text = Convert.ToString(dtOppositionSummary.Rows[0]["OtherSideTmApplicationNum"]);
                    txtTradeMarkDetails.Text = Convert.ToString(dtOppositionSummary.Rows[0]["OtherSideTmTradeMark"]);
                    txtApplicationclassDetails.Text = Convert.ToString(dtOppositionSummary.Rows[0]["OtherSideTmClass"]);
                    if (Convert.ToString(dtOppositionSummary.Rows[0]["OtherSideTmFilledOn"]) != "")
                    {
                        txtApplicationFilledOn.Text = Convert.ToDateTime(dtOppositionSummary.Rows[0]["OtherSideTmFilledOn"].ToString()).ToString("dd MMM yyyy");
                    }
                    txtApplicationUser.Text = Convert.ToString(dtOppositionSummary.Rows[0]["OtherSideApplicationUser"]);
                    txtSideClientName.Text = Convert.ToString(dtOppositionSummary.Rows[0]["OthersideClientName"]);
                    txtSideClientAddress.Text = Convert.ToString(dtOppositionSummary.Rows[0]["OtherSideClientAddress"]);
                    txtOtherSideAdvocateName.Text = Convert.ToString(dtOppositionSummary.Rows[0]["OtherSideClientAdvocateName"]);
                    txtOtherSideAdvocateAddress.Text = Convert.ToString(dtOppositionSummary.Rows[0]["OtherSideClientAdvocateAddress"]);
                    txtReminder.Text = Convert.ToString(dtOppositionSummary.Rows[0]["Reminder"]);
                    if (Convert.ToString(dtOppositionSummary.Rows[0]["ReminderDate"]) != "")
                    {
                        txtReminderDate.Text = Convert.ToDateTime(dtOppositionSummary.Rows[0]["ReminderDate"].ToString()).ToString("dd MMM yyyy");
                    }
                    txtFileNo.Text = Convert.ToString(dtOppositionSummary.Rows[0]["FileNo"]);
                    if (Convert.ToString(dtOppositionSummary.Rows[0]["OppositionDate"]) != "")
                    {
                        txtOppositionDate.Text = Convert.ToDateTime(dtOppositionSummary.Rows[0]["OppositionDate"].ToString()).ToString("dd MMM yyyy");
                    }
                    if (Convert.ToString(dtOppositionSummary.Rows[0]["OtherSideJournalDate"]) != "")
                    {
                        txtOtherJournalDate.Text = Convert.ToDateTime(dtOppositionSummary.Rows[0]["OtherSideJournalDate"].ToString()).ToString("dd MMM yyyy");
                    }
                    if (Convert.ToString(dtOppositionSummary.Rows[0]["OtherSidePublicationDate"]) != "")
                    {
                        txtOtherPublicationDate.Text = Convert.ToDateTime(dtOppositionSummary.Rows[0]["OtherSidePublicationDate"].ToString()).ToString("dd MMM yyyy");
                    }
                  
                    hdnOppositionMainId.Value = Convert.ToString(dtOppositionSummary.Rows[0]["TmOppositionId"]);

                    fillOppositionDetails(Convert.ToInt32(dtOppositionSummary.Rows[0]["TmOppositionId"]));
                    fillOppositionDetails2(Convert.ToInt32(dtOppositionSummary.Rows[0]["TmOppositionId"]));
                    FillDocument(Convert.ToInt32(dtOppositionSummary.Rows[0]["TmOppositionId"]));
                    FillForm(Convert.ToInt32(dtOppositionSummary.Rows[0]["TmOppositionId"]));



                }
            }

        }

    }
    #endregion
    protected void fillOppositionDetails(int pid)
    {
        Session["s"] = null;

        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.TmOppositionId = pid;
        DataSet ds = ObjTmcountry.getTmOppositionHearingDetails();

        DataTable dtInvoice = new DataTable();
        dtInvoice = ds.Tables[0];
        if (dtInvoice.Rows.Count > 0)
        {
            for (int i = 0; i < dtInvoice.Rows.Count; i++)
            {
                filltable();
                DataRow row;
                row = dttbl.NewRow();
                row["Id"] = i + 1;
                row["HearingDate"] = Convert.ToDateTime(dtInvoice.Rows[i]["HearingDate"]);
                row["Before"] = Convert.ToString(dtInvoice.Rows[i]["Before"]);
                row["Comment"] = Convert.ToString(dtInvoice.Rows[i]["Comment"]);
                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
                Session.Add("s", dttbl);
            }
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            dgGallery.Visible = true;

        }
        else
        {
            dgGallery.Visible = false;
            trHearing1.Style.Add("display", "none");
            trHearing2.Style.Add("display", "none");
        }

    }
    protected void fillOppositionDetails2(int pid)
    {
        Session["s2"] = null;

        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.TmOppositionId = pid;
        DataSet ds = ObjTmcountry.getTmOppositionDetails();

        DataTable dtInvoice = new DataTable();
        dtInvoice = ds.Tables[0];
        if (dtInvoice.Rows.Count > 0)
        {
            for (int i = 0; i < dtInvoice.Rows.Count; i++)
            {

                filltable2();
                DataRow row;
                row = dttbl2.NewRow();
                row["Id"] = i + 1;
                row["DetailsDate"] = Convert.ToDateTime(dtInvoice.Rows[i]["DetailsDate"]);
                row["DetailsRemark"] = Convert.ToString(dtInvoice.Rows[i]["DetailsRemark"]);
                dttbl2.Rows.Add(row);
                dttbl2.AcceptChanges();
                Session.Add("s2", dttbl2);

            }
            dgGallery2.DataSource = dttbl2;
            dgGallery2.DataBind();
            dgGallery2.Visible = true;

        }
        else
        {
            dgGallery2.Visible = false;

            trOpposition1.Style.Add("display", "none");
            trOpposition2.Style.Add("display", "none");
        }

    }
    protected void FillDocument(int pid)
    {

        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.TmDocumentId = 0;
        DataSet dsRenewal = ObjTmcountry.getTmOppositionDocument();
        DataTable dt3 = new DataTable();
        dt3 = dsRenewal.Tables[0];
        if (dt3.Rows.Count > 0)
        {
            DataView dv = new DataView(dt3);
            dv.RowFilter = "TmOppositionId=" + pid;
            dt3 = dv.ToTable();
            if (dt3.Rows.Count > 0)
            {
                ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                ObjTmcountry.TmOppositionDocumentId = Convert.ToInt32(dt3.Rows[0]["TmOppositionDocumentId"]);
                DataSet ds1 = ObjTmcountry.getTmOppositionDocumentDetails();

                DataTable dt = new DataTable();
                DataView dv1 = new DataView(ds1.Tables[0]);
                dv1.RowFilter = "IsShowtoClient<>'false'";
                dt = dv1.ToTable();


                if (dt.Rows.Count > 0)
                {
                    dgdocument.DataSource = dt;
                    dgdocument.DataBind();
                    dgdocument.Visible = true;
                }

                else
                {


                    Document1.Style.Add("display", "none");
                    Document2.Style.Add("display", "none");

                }
            }
            else
            {


                Document1.Style.Add("display", "none");
                Document2.Style.Add("display", "none");

            }
        }
        else
        {


            Document1.Style.Add("display", "none");
            Document2.Style.Add("display", "none");

        }

    }
    protected void FillForm(int pid)
    {

        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.TmOppositionFormId = 0;
        DataSet dsRenewal = ObjTmcountry.getTmOppositionFormMain();

        DataTable dt3 = new DataTable();
        dt3 = dsRenewal.Tables[0];
        if (dt3.Rows.Count > 0)
        {
            DataView dv = new DataView(dt3);
            dv.RowFilter = "TmOppositionId=" + pid;
            dt3 = dv.ToTable();
            if (dt3.Rows.Count > 0)
            {
                ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                ObjTmcountry.TmOppositionFormId = Convert.ToInt32(dt3.Rows[0]["TmOppositionFormId"]);
                DataSet ds1 = ObjTmcountry.getTmOppositionFormDetails();
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    dgForm.DataSource = ds1.Tables[0];
                    dgForm.DataBind();
                    dgForm.Visible = true;
                }

                else
                {


                    trForm1.Style.Add("display", "none");
                    trForm2.Style.Add("display", "none");

                }
            }
            else
            {


                trForm1.Style.Add("display", "none");
                trForm2.Style.Add("display", "none");

            }
        }
        else
        {


            trForm1.Style.Add("display", "none");
            trForm2.Style.Add("display", "none");

        }

    }
    #region Fill session Table
    public void filltable()
    {
        if (Session["s"] == null)
        {
            DataColumn column = new DataColumn();
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.DateTime");
            column.ColumnName = "HearingDate";
            column.Caption = "HearingDate";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);



            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Before";
            column.Caption = "Before";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Comment";
            column.Caption = "Comment";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);




        }
        else
        {
            dttbl = (DataTable)Session["s"];
        }
    }
    public void filltable2()
    {
        if (Session["s2"] == null)
        {
            DataColumn column = new DataColumn();
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl2.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.DateTime");
            column.ColumnName = "DetailsDate";
            column.Caption = "DetailsDate";
            column.ReadOnly = false;
            dttbl2.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "DetailsRemark";
            column.Caption = "DetailsRemark";
            column.ReadOnly = false;
            dttbl2.Columns.Add(column);


        }
        else
        {
            dttbl2 = (DataTable)Session["s2"];
        }
    }
    #endregion





    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }

            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
            {
                //    btnDeleteInvoice.Enabled = false;
            }


            if (Convert.ToString(dt.Rows[0]["IsAdd"]) != "True")
            {
                //lblbutton.Visible = false;
                if (Convert.ToString(Request["addnew"]) == "1")
                {

                    // ImageButton2.Visible = false;
                }
            }



            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {
                if (Convert.ToString(Request["addnew"]) != "1")
                {

                    // ImageButton2.Visible = false;
                }
            }

        }
    }
    protected void lblbutton_Click(object sender, EventArgs e)
    {
        Response.Redirect("TmOpposition.aspx?addnew=1");

    }



    #region fil Client & Fill Status
    protected void FillClient()
    {
        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        ObjTmcountry.TmCustomerId = 0;
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();

        ds = ObjTmcountry.GetTmCustomerMaindetails();
        dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {
            ddlOurClient.DataSource = dt;
            ddlOurClient.DataTextField = "TmCustomerName";
            ddlOurClient.DataValueField = "TmCustomerId";
            ddlOurClient.DataBind();
            ddlOurClient.Items.Insert(0, new ListItem("Select Client", "0"));
        }

    }
    protected void FillStatus()
    {
        ObjTmcountry.TmFileStatusId = 0;
        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = ObjTmcountry.GetTmFileStatus();
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlStatus.DataSource = ds.Tables[0];
            ddlStatus.DataTextField = "TmFileStatus";
            ddlStatus.DataValueField = "TmFileStatusId";
            ddlStatus.DataBind();
            ddlStatus.Items.Insert(0, new ListItem("Select Status", "0"));

            ddlstatusOpposition.DataSource = ds.Tables[0];
            ddlstatusOpposition.DataTextField = "TmFileStatus";
            ddlstatusOpposition.DataValueField = "TmFileStatusId";
            ddlstatusOpposition.DataBind();
            ddlstatusOpposition.Items.Insert(0, new ListItem("Select Status", "0"));

        }

    }
    #endregion

    protected void BindYear()
    {
        ArrayList YearArrayList = new ArrayList();
        for (int i = 0; i < 91; i++)
        {
            int year = 1950 + i;
            YearArrayList.Add(year.ToString());
        }
        ddlYear.DataSource = YearArrayList;
        ddlYear.DataBind();
        ddlYear.Items.Insert(0, (new ListItem("Select Year", "0")));
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

        ObjTmcountry.CustomerCode = decimal.Parse(Sessions.CustomerCode);

        DataSet ds = ObjTmcountry.SearchTmOpposition();
        DataTable dt = new DataTable();

        dt = ds.Tables[0];
        if (dt.Rows.Count > 0)
        {

            DataView dv = new DataView(dt);

            dv.RowFilter = "TmCustomerName = '" + Convert.ToString(Sessions.UseLoginName) + "'";
            dt = dv.ToTable();
            dv.Table = dt;
            if (txtSearch.Text != "")
            {

                if (ddlSearchBy.SelectedValue.ToString() == "OppRect")
                {
                    dv.RowFilter = "OppositionNo LIKE '%" + txtSearch.Text + "%'";
                }
                if (ddlSearchBy.SelectedValue.ToString() == "ApplicationNum")
                {
                    dv.RowFilter = "ApplicationNum=" + txtSearch.Text;
                }

                if (ddlSearchBy.SelectedValue.ToString() == "TradeMark")
                {
                    dv.RowFilter = "TradeMark LIKE '%" + txtSearch.Text + "%'";
                }
            }
            dt = dv.ToTable();

            dgagentSearch.DataSource = dt;
            dgagentSearch.DataBind();
        }

    }
    protected void grdCustomer_RowCommand(object sender, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "View")
        {
            Response.Redirect("SingleTMOppositionSearchClient.aspx?addnew=" + e.CommandArgument.ToString());
        }
        if (e.CommandName == "Edit")
        {
            Response.Redirect("SingleTMOppositionSearchClient.aspx?TmOppositionId=" + e.CommandArgument.ToString());
        }
    }
}