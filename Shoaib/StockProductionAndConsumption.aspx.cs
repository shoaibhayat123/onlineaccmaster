﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class StockProductionAndConsumption : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();
    DataTable dttbl2 = new DataTable();
    int Last = 0;
    int Corrent = 0;
    int FirstRecord = 0;
    int LastRecord = 0;

    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        if (!IsPostBack)
        {
            txtVoucherNo.Focus();
            FillCostCenter();
            FillItemDesc();
            DataTable dt = new DataTable();
            dgGallery.DataSource = dt;
            dgGallery.DataBind();
            dgGallery2.DataSource = dt;
            dgGallery2.DataBind();
            Session["Corrent"] = null;
            Session["FirstRecord"] = null;
            Session["LastRecord"] = null;
            Session["InvoiceSummary"] = null;
            ViewState["s"] = null;
            ViewState["s2"] = null;
            if (Convert.ToString(Request["addnew"]) != "1")
            {
                bindpagging();
                if (Request["VoucherSimpleMainId"] == null)
                {
                    BindInvoice();
                }
                else
                {
                    DataTable dtInvoiceSummary = new DataTable();
                    dtInvoiceSummary = (DataTable)Session["InvoiceSummary"];
                    DataView dv = new DataView(dtInvoiceSummary);
                    dv.RowFilter = "ProdNConsMainId=" + Request["VoucherSimpleMainId"];
                    dtInvoiceSummary = dv.ToTable();
                    if (dtInvoiceSummary.Rows.Count > 0)
                    {
                        Session["Corrent"] = Convert.ToInt32(dtInvoiceSummary.Rows[0]["row"]);
                        BindInvoice();
                    }
                    else
                    {
                        Response.Redirect("default.aspx");
                    }
                }
            }
            else
            {
                Session["InvoiceSummary"] = null;
                objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objSalesInvoice.ProdNConsMainId = 0;
                objSalesInvoice.DataFrom = "PRODNCONS";
                DataTable dtInvoiceNewDetails = new DataTable();
                dtInvoiceNewDetails = objSalesInvoice.getProdNConsMain();
                if (dtInvoiceNewDetails.Rows.Count > 0)
                {
                    int Last = Convert.ToInt32(dtInvoiceNewDetails.Rows.Count) - 1;
                    txtVoucherNo.Text = Convert.ToString(Convert.ToDecimal(dtInvoiceNewDetails.Rows[Last]["VoucherNo"]) + 1);
                    txtInvoiceDate.Text = Convert.ToDateTime(dtInvoiceNewDetails.Rows[Last]["VoucherDate"]).ToString("dd MMM yyyy");
                }
                else
                {
                    txtVoucherNo.Text = "1";
                    txtInvoiceDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
                }

                spanPgging.Style.Add("display", "none");
                btnDeleteInvoice.Style.Add("display", "none");
            }
        }

        SetUserRight();

    }
    #endregion

    #region Fill Item Description
    protected void FillItemDesc()
    {
        objCust.ItemId = 0;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlItemDesc.DataSource = ds.Tables[0];
            ddlItemDesc.DataTextField = "ItemDesc";
            ddlItemDesc.DataValueField = "ItemId";
            ddlItemDesc.DataBind();
            ddlItemDesc.Items.Insert(0, new ListItem("Select  Item", "0"));


            ddlItemDesc2.DataSource = ds.Tables[0];
            ddlItemDesc2.DataTextField = "ItemDesc";
            ddlItemDesc2.DataValueField = "ItemId";
            ddlItemDesc2.DataBind();
            ddlItemDesc2.Items.Insert(0, new ListItem("Select  Item", "0"));



        }

    }
    #endregion

    #region Fill Cost Center
    protected void FillCostCenter()
    {
       
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.CostCenterCode = 0;
            DataSet ds = new DataSet();
            ds = objCust.GetCostCenter();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlCostCenter.DataSource = ds.Tables[0];
                ddlCostCenter.DataTextField = "CostCenterName";
                ddlCostCenter.DataValueField = "CostCenterCode";
                ddlCostCenter.DataBind();
                ddlCostCenter.Items.Insert(0, new ListItem("Select cost center From", "0"));




            }

       

    }
    #endregion

    #region Fill  Client A/C
    protected void FillClientAC()
    {


    }
    #endregion

    #region Fill  FillSales A/c
    protected void FillSalesAc()
    {
       
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtLedger = new DataTable();
            dtLedger = objSalesInvoice.getAcFileAllLedger();
            if (dtLedger.Rows.Count > 0)
            {

            }

        

    }
    #endregion

    #region Add Details

    protected void btn_saveInvoice_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
        if (Sessions.FromDate != null)
        {
            if ( ViewState["s2"] != null)
            {

                if ((Convert.ToDateTime(txtInvoiceDate.Text) >= Convert.ToDateTime(Sessions.FromDate)) && (Convert.ToDateTime(txtInvoiceDate.Text) <= Convert.ToDateTime(Sessions.ToDate)))
                {

                    if (hdnProdNConsMainId.Value == "")
                    {
                        hdnProdNConsMainId.Value = "0";
                    }
                    /************ Add Invoice Summary **************/
                    if (Convert.ToInt32(hdnProdNConsMainId.Value) > 0)
                    {
                        objSalesInvoice.ProdNConsMainId = Convert.ToInt32(hdnProdNConsMainId.Value);
                    }
                    else
                    {
                        objSalesInvoice.ProdNConsMainId = 0;
                    }
                    objSalesInvoice.VoucherNo = Convert.ToInt32(txtVoucherNo.Text.ToUpper());
                    objSalesInvoice.VoucherDate = Convert.ToDateTime(txtInvoiceDate.Text);
                    objSalesInvoice.Description = txtDescription.Text;
                    objSalesInvoice.DataFrom = "PRODNCONS";
                    objSalesInvoice.StoreId = Convert.ToInt32(ddlCostCenter.SelectedValue);
                    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                    DataTable dt1 = new DataTable();
                    dt1 = (DataTable)ViewState["s"];
                    if (dt1.Rows.Count >0)
                    {
                      
                        decimal Quantity = (decimal)dt1.Compute("SUM(Quantity)", "");
                        objSalesInvoice.TotalquantityProduction = Quantity;
                    }
                    DataTable dt2 = new DataTable();
                    dt2 = (DataTable)ViewState["s2"];
                    decimal Quantity2 = (decimal)dt2.Compute("SUM(Quantity)", "");
                    objSalesInvoice.TotalquantityConsumption = Quantity2;



                    DataTable dtresult = new DataTable();
                    dtresult = objSalesInvoice.AddEditProdNConsDetail();

                    /************ Add Invoice Detail **************/
                    if (dtresult.Rows.Count > 0)
                    {
                        if (ViewState["s"] != null)
                        {
                            DataTable dt = new DataTable();
                            dt = (DataTable)ViewState["s"];
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    objSalesInvoice.ItemId = 0;
                                    objSalesInvoice.ItemCode = Convert.ToInt32(dt.Rows[i]["ItemCode"].ToString());
                                    objSalesInvoice.Quantity = Convert.ToDecimal(dt.Rows[i]["Quantity"].ToString());
                                    objSalesInvoice.ProdNConsMainId = Convert.ToInt32(dtresult.Rows[0]["ProdNConsMainId"].ToString()); /***** dtresult.Rows[0] is for ProdNConsMainId which is new****/
                                    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                    objSalesInvoice.DataFrom = "PRODNCONS";
                                    objSalesInvoice.storeId = Convert.ToInt32(ddlCostCenter.SelectedValue);
                                    objSalesInvoice.TransQty = Convert.ToDecimal(dt.Rows[i]["Quantity"].ToString());
                                    DataTable dtInvoiceSummary = new DataTable();
                                    dtInvoiceSummary = objSalesInvoice.AddEditInvoiceDetailStock();
                                }
                            }
                        }

                        if (ViewState["s2"] != null)
                        {
                            DataTable dt = new DataTable();
                            dt = (DataTable)ViewState["s2"];
                            if (dt.Rows.Count > 0)
                            {
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    objSalesInvoice.ItemId = 0;
                                    objSalesInvoice.ItemCode = Convert.ToInt32(dt.Rows[i]["ItemCode"].ToString());
                                    objSalesInvoice.Quantity = Convert.ToDecimal(dt.Rows[i]["Quantity"].ToString());
                                    objSalesInvoice.ProdNConsMainId = Convert.ToInt32(dtresult.Rows[0]["ProdNConsMainId"].ToString()); /***** dtresult.Rows[0] is for ProdNConsMainId which is new****/
                                    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                    objSalesInvoice.DataFrom = "PRODNCONS";
                                    objSalesInvoice.storeId = Convert.ToInt32(ddlCostCenter.SelectedValue);
                                    objSalesInvoice.TransQty = 0 - Convert.ToDecimal(dt.Rows[i]["Quantity"].ToString());
                                    DataTable dtInvoiceSummary = new DataTable();
                                    dtInvoiceSummary = objSalesInvoice.AddEditInvoiceDetailStock();
                                }
                            }
                        }

                    }


                    if (Convert.ToInt32(hdnProdNConsMainId.Value) > 0)
                    {
                        ViewState["s2"] = null;
                        ViewState["s"] = null;
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Updated.');location.href='StockProductionAndConsumption.aspx';", true);

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Saved.');location.href='StockProductionAndConsumption.aspx';", true);
                    }
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Date out of accouting year.');", true);

                }

            }


            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please Fill  Details.');", true);
            }
        }
    }
    #endregion

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockProductionAndConsumption.aspx");
    }
    #endregion

    #region Fill session Table
    public void filltable()
    {
        if (ViewState["s"] == null)
        {

            DataColumn column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "ItemCode";
            column.Caption = "ItemCode";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ItemDesc";
            column.Caption = "ItemDesc";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Quantity";
            column.Caption = "Quantity";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);




        }
        else
        {
            dttbl = (DataTable)ViewState["s"];
        }
    }
    public void filltable2()
    {
        if (ViewState["s2"] == null)
        {

            DataColumn column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "ItemCode";
            column.Caption = "ItemCode";
            column.ReadOnly = false;
            dttbl2.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl2.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ItemDesc";
            column.Caption = "ItemDesc";
            column.ReadOnly = false;
            dttbl2.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Quantity";
            column.Caption = "Quantity";
            column.ReadOnly = false;
            dttbl2.Columns.Add(column);




        }
        else
        {
            dttbl2 = (DataTable)ViewState["s2"];
        }
    }
    #endregion

    #region Add New Bill details
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
        if (Convert.ToDecimal(txtQuantity.Text) > 0)
        {

            filltable();
            if (hdndetail1.Value == "")
            {
                DataRow row;
                if (dttbl.Rows.Count > 0)
                {
                    int maxid = 0;
                    for (int i = 0; i < dttbl.Rows.Count; i++)
                    {
                        if (dttbl.Rows[i]["Id"].ToString() != "")
                        {
                            if (maxid < Convert.ToInt32(dttbl.Rows[i]["Id"].ToString()))
                            {
                                maxid = Convert.ToInt32(dttbl.Rows[i]["Id"].ToString());
                            }
                        }
                    }

                    row = dttbl.NewRow();
                    row["Id"] = maxid + 1;
                    row["ItemCode"] = Convert.ToInt32(ddlItemDesc.SelectedValue.ToString());
                    row["ItemDesc"] = Convert.ToString(ddlItemDesc.SelectedItem.Text);
                    row["Quantity"] = Convert.ToDecimal(txtQuantity.Text);



                }
                else
                {

                    row = dttbl.NewRow();
                    row["Id"] = 1;
                    row["ItemCode"] = Convert.ToInt32(ddlItemDesc.SelectedValue.ToString());
                    row["ItemDesc"] = Convert.ToString(ddlItemDesc.SelectedItem.Text);
                    row["Quantity"] = Convert.ToDecimal(txtQuantity.Text);

                }
                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
            }

            else
            {

                for (int i = 0; i < dttbl.Rows.Count; i++)
                {
                    if (dttbl.Rows[i]["Id"].ToString() == hdndetail1.Value)
                    {

                        dttbl.Rows[i]["ItemCode"] = Convert.ToInt32(ddlItemDesc.SelectedValue.ToString());
                        dttbl.Rows[i]["ItemDesc"] = Convert.ToString(ddlItemDesc.SelectedItem.Text);
                        dttbl.Rows[i]["Quantity"] = Convert.ToDecimal(txtQuantity.Text);


                    }
                }
            }
            ViewState.Add("s", dttbl);
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            ShowTotal();
            if (dttbl.Rows.Count > 0)
            {
                dgGallery.Visible = true;
            }

            ClearInvoiceDetail();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Invalid Quantity.');", true);
        }
    }

    protected void btnAddNew2_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
        if (Convert.ToDecimal(txtQuantity2.Text) > 0)
        {

            filltable2();
            if (hdndetail2.Value == "")
            {
                DataRow row;
                if (dttbl2.Rows.Count > 0)
                {
                    int maxid = 0;
                    for (int i = 0; i < dttbl2.Rows.Count; i++)
                    {
                        if (dttbl2.Rows[i]["Id"].ToString() != "")
                        {
                            if (maxid < Convert.ToInt32(dttbl2.Rows[i]["Id"].ToString()))
                            {
                                maxid = Convert.ToInt32(dttbl2.Rows[i]["Id"].ToString());
                            }
                        }
                    }

                    row = dttbl2.NewRow();
                    row["Id"] = maxid + 1;
                    row["ItemCode"] = Convert.ToInt32(ddlItemDesc2.SelectedValue.ToString());
                    row["ItemDesc"] = Convert.ToString(ddlItemDesc2.SelectedItem.Text);
                    row["Quantity"] = Convert.ToDecimal(txtQuantity2.Text);



                }
                else
                {

                    row = dttbl2.NewRow();
                    row["Id"] = 1;
                    row["ItemCode"] = Convert.ToInt32(ddlItemDesc2.SelectedValue.ToString());
                    row["ItemDesc"] = Convert.ToString(ddlItemDesc2.SelectedItem.Text);
                    row["Quantity"] = Convert.ToDecimal(txtQuantity2.Text);

                }
                dttbl2.Rows.Add(row);
                dttbl2.AcceptChanges();
            }

            else
            {

                for (int i = 0; i < dttbl2.Rows.Count; i++)
                {
                    if (dttbl2.Rows[i]["Id"].ToString() == hdndetail2.Value)
                    {

                        dttbl2.Rows[i]["ItemCode"] = Convert.ToInt32(ddlItemDesc2.SelectedValue.ToString());
                        dttbl2.Rows[i]["ItemDesc"] = Convert.ToString(ddlItemDesc2.SelectedItem.Text);
                        dttbl2.Rows[i]["Quantity"] = Convert.ToDecimal(txtQuantity2.Text);

                    }
                }
            }

            ViewState.Add("s2", dttbl2);
            dgGallery2.DataSource = dttbl2;
            dgGallery2.DataBind();
            ShowTotal();
            if (dttbl2.Rows.Count > 0)
            {
                dgGallery2.Visible = true;
            }

            ClearInvoiceDetail2();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Invalid Quantity.');", true);
        }
    }
    #endregion

    #region Grid Event
    protected void dgGallery_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            Label lblTotalQuantity = (Label)e.Item.FindControl("lblTotalQuantity");
            if (dttbl.Rows.Count > 0)
            {
                dgGallery.ShowFooter = true;
                decimal Quantity = (decimal)dttbl.Compute("SUM(Quantity)", "");
                lblTotalQuantity.Text = String.Format("{0:C}", Quantity).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            }
            else
            {
                dgGallery.ShowFooter = false;
                lblTotalQuantity.Text = "0.00";
            }
        }

    }
    protected void dgGallery2_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Footer)
        {
            Label lblTotalQuantity = (Label)e.Item.FindControl("lblTotalQuantity");
            if (dttbl2.Rows.Count > 0)
            {
                dgGallery2.ShowFooter = true;
                decimal Quantity = (decimal)dttbl2.Compute("SUM(Quantity)", "");
                lblTotalQuantity.Text = String.Format("{0:C}", Quantity).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
            }
            else
            {
                dgGallery2.ShowFooter = false;
                lblTotalQuantity.Text = "0.00";
            }
        }


    }
    protected void dgGallery_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        dttbl = (DataTable)ViewState["s"];
        if (e.CommandName == "Delete")
        {
            int j = 0;
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    dttbl.Rows.RemoveAt(i);
                }
            }
            dttbl.AcceptChanges();
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            ViewState.Add("s", dttbl);
            ShowTotal();

        }

        if (e.CommandName == "Edit")
        {
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {


                    ddlItemDesc.SelectedValue = dttbl.Rows[i]["ItemCode"].ToString();
                    txtQuantity.Text = dttbl.Rows[i]["Quantity"].ToString();
                    hdndetail1.Value = dttbl.Rows[i]["Id"].ToString();




                }
            }
        }
    }
    protected void dgGallery2_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        dttbl2 = (DataTable)ViewState["s2"];
        if (e.CommandName == "Delete")
        {
            int j = 0;
            for (int i = 0; i < dttbl2.Rows.Count; i++)
            {
                if (dttbl2.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    dttbl2.Rows.RemoveAt(i);
                }
            }
            dttbl2.AcceptChanges();
            dgGallery2.DataSource = dttbl2;
            dgGallery2.DataBind();
            ViewState.Add("s2", dttbl2);
            ShowTotal();

        }

        if (e.CommandName == "Edit")
        {
            for (int i = 0; i < dttbl2.Rows.Count; i++)
            {
                if (dttbl2.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {

                    ddlItemDesc2.SelectedValue = dttbl2.Rows[i]["ItemCode"].ToString();
                    txtQuantity2.Text = dttbl2.Rows[i]["Quantity"].ToString();
                    hdndetail2.Value = dttbl2.Rows[i]["Id"].ToString();

                }
            }
        }
    }
    #endregion

    #region ClearInvoiceDetail
    protected void ClearInvoiceDetail()
    {
        ddlItemDesc.SelectedValue = "0";
        txtQuantity.Text = "";
        hdndetail1.Value = "";


    }
    protected void ClearInvoiceDetail2()
    {
        ddlItemDesc2.SelectedValue = "0";
        txtQuantity2.Text = "";
        hdndetail2.Value = "";
    }
    #endregion

    #region Show Total
    protected void ShowTotal()
    {

    }
    #endregion

    #region Pagging
    private void bindpagging()
    {
        Last = 0;
        Corrent = 0;
        FirstRecord = 0;
        LastRecord = 0;
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.ProdNConsMainId = 0;
        DataTable dt = new DataTable();
        objSalesInvoice.DataFrom = "PRODNCONS";
        dt = objSalesInvoice.getProdNConsMain();
        if (dt.Rows.Count > 0)
        {
            Session["InvoiceSummary"] = dt;
            Last = dt.Rows.Count;
            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            Session["FirstRecord"] = FirstRecord;
            Session["LastRecord"] = LastRecord;
            Session["Corrent"] = LastRecord;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "location.href='StockProductionAndConsumption.aspx?addnew=1';", true);
        }


    }
    protected void lnkFirst_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["FirstRecord"];
        BindInvoice();

    }
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["FirstRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) - 1;
            BindInvoice();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["LastRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) + 1;
            BindInvoice();
        }
    }
    protected void lnLast_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["LastRecord"];
        BindInvoice();
    }
    #endregion

    #region Fill invoice summary
    protected void BindInvoice()
    {
        if (Session["InvoiceSummary"] != null)
        {
            DataTable dtInvoiceSummary = (DataTable)Session["InvoiceSummary"];
            if (dtInvoiceSummary.Rows.Count > 0)
            {

                DataView DvInvoiceSummary = dtInvoiceSummary.DefaultView;
                DvInvoiceSummary.RowFilter = "Row = '" + Session["Corrent"].ToString() + "'";
                dtInvoiceSummary = DvInvoiceSummary.ToTable();
                if (dtInvoiceSummary.Rows.Count > 0)
                {
                    txtVoucherNo.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["VoucherNo"]);
                    txtInvoiceDate.Text = Convert.ToDateTime(dtInvoiceSummary.Rows[0]["VoucherDate"].ToString()).ToString("dd MMM yyyy");
                    ddlCostCenter.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["StoreId"]);
                    hdnProdNConsMainId.Value = Convert.ToString(dtInvoiceSummary.Rows[0]["ProdNConsMainId"]);
                    txtDescription.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["Description"]);
                    fillInvoiceDetails(Convert.ToInt32(dtInvoiceSummary.Rows[0]["ProdNConsMainId"]));
                    fillInvoiceDetails2(Convert.ToInt32(dtInvoiceSummary.Rows[0]["ProdNConsMainId"]));
                    hdnProdNConsMainId.Value = Convert.ToString(dtInvoiceSummary.Rows[0]["ProdNConsMainId"]);


                }
            }

        }
    }
    #endregion

    #region fillInvoiceDetails
    protected void fillInvoiceDetails(int pid)
    {
        ViewState["s"] = null;

        DataTable dtInvoice = new DataTable();
        objSalesInvoice.InvoiceSummaryId = pid;
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objSalesInvoice.DataFrom = "PRODNCONS";
        dtInvoice = objSalesInvoice.getInvoiceDetailsProduction();

        if (dtInvoice.Rows.Count > 0)
        {
            for (int i = 0; i < dtInvoice.Rows.Count; i++)
            {
                filltable();
                DataRow row;
                row = dttbl.NewRow();
                row["Id"] = i + 1;
                row["ItemCode"] = Convert.ToInt32(dtInvoice.Rows[i]["ItemCode"]);
                row["ItemDesc"] = Convert.ToString(dtInvoice.Rows[i]["ItemDesc"]);
                row["Quantity"] = Convert.ToDecimal(dtInvoice.Rows[i]["Quantity"]);
                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
                ViewState.Add("s", dttbl);
            }
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            dgGallery.Visible = true;

        }
        else
        {
            dgGallery.Visible = false;
        }

    }
    protected void fillInvoiceDetails2(int pid)
    {
        ViewState["s2"] = null;

        DataTable dtInvoice = new DataTable();
        objSalesInvoice.InvoiceSummaryId = pid;
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objSalesInvoice.DataFrom = "PRODNCONS";
        dtInvoice = objSalesInvoice.getInvoiceDetailsConsumption();

        if (dtInvoice.Rows.Count > 0)
        {
            for (int i = 0; i < dtInvoice.Rows.Count; i++)
            {
                filltable2();
                DataRow row;
                row = dttbl2.NewRow();
                row["Id"] = i + 1;
                row["ItemCode"] = Convert.ToInt32(dtInvoice.Rows[i]["ItemCode"]);
                row["ItemDesc"] = Convert.ToString(dtInvoice.Rows[i]["ItemDesc"]);
                row["Quantity"] = Convert.ToDecimal(dtInvoice.Rows[i]["Quantity"]);
                dttbl2.Rows.Add(row);
                dttbl2.AcceptChanges();
                ViewState.Add("s2", dttbl2);
            }
            dgGallery2.DataSource = dttbl2;
            dgGallery2.DataBind();
            dgGallery2.Visible = true;

        }
        else
        {
            dgGallery2.Visible = false;
        }

    }
    #endregion

    #region Delete Invoice
    protected void btnDeleteInvoice_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(hdnProdNConsMainId.Value) > 0)
        {
            int result = 0;
            objSalesInvoice.ProdNConsMainId = Convert.ToInt32(hdnProdNConsMainId.Value);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.DataFrom = "PRODNCONS";
            result = objSalesInvoice.deleteAllProdNConsMain();
            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record deleted successfully.');location.href='StockProductionAndConsumption.aspx';", true);
            }
        }

    }
    #endregion

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        string urllast = "PrintStockProductionAndConsumption.aspx?ProdNConsMainId=" + Convert.ToString(hdnProdNConsMainId.Value);
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);

    }

    protected void btnPrintBill_Click(object sender, EventArgs e)
    {
        //string urllast = "PrintSalesTaxInvoiceBill.aspx?ProdNConsMainId=" + Convert.ToString(hdnProdNConsMainId.Value);
        //ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "' );", true);

    }

    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }

            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
            {
                btnDeleteInvoice.Visible = false;
            }
            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {
                btnPrint.Visible = false;

            }

            if (Convert.ToString(dt.Rows[0]["IsAdd"]) != "True")
            {
                lblbutton.Visible = false;
                if (Convert.ToString(Request["addnew"]) == "1")
                {

                    ImageButton2.Visible = false;
                }
            }


            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {
                if (Convert.ToString(Request["addnew"]) != "1")
                {

                    ImageButton2.Visible = false;
                }
            }

        }
    }

    protected void lblbutton_Click(object sender, EventArgs e)
    {
        Response.Redirect("StockProductionAndConsumption.aspx?addnew=1");
    }
}
