﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class ServiceInvoiceReport : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        if (!IsPostBack)
        {
            txtInvoiceNo.Focus();
            FillClientAC();
            FillCostCenter();
            txtInvoiceDateFrom.Text = Convert.ToDateTime(Sessions.FromDate).ToString("dd MMM yyyy");
            txtInvoiceDateTo.Text = System.DateTime.Now.ToString("dd MMM yyyy");
        }
    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        objSalesInvoice.invDateTo = Convert.ToDateTime(txtInvoiceDateTo.Text);
        objSalesInvoice.InvDateFrom = Convert.ToDateTime(txtInvoiceDateFrom.Text);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.DebitAcId = Convert.ToInt32(ddlDebit.SelectedValue);
        objSalesInvoice.CreditAcId = Convert.ToInt32(ddlCredit.SelectedValue);
        objSalesInvoice.DataFrom = "SERINV";

        if (txtInvoiceNo.Text != "")
        {
            objSalesInvoice.ServiceInvoiceNo = Convert.ToInt32(txtInvoiceNo.Text);
        }
        else
        {
            objSalesInvoice.ServiceInvoiceNo = 0;
        }
        objSalesInvoice.CostCenterCode = Convert.ToInt32(ddlCostCenter.SelectedValue);

        DataTable dt = new DataTable();
        dt = objSalesInvoice.SearchServiceInvoice();
        dgGallery.DataSource = dt;
        dgGallery.DataBind();
    }

    #region Fill  Client A/C
    protected void FillClientAC()
    {
          objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtClient = new DataTable();
            dtClient = objSalesInvoice.getAcFileBySundryDebtorOnly();
            if (dtClient.Rows.Count > 0)
            {
                ddlDebit.DataSource = dtClient;
                ddlDebit.DataTextField = "Title";
                ddlDebit.DataValueField = "Id";
                ddlDebit.DataBind();
                ddlDebit.Items.Insert(0, new ListItem("Select Debit A/c", "0"));
            }

            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataTable dtLedgerAll = new DataTable();
            dtLedgerAll = objSalesInvoice.getAcFileAllLedger();
            if (dtLedgerAll.Rows.Count > 0)
            {
                ddlCredit.DataSource = dtLedgerAll;
                ddlCredit.DataTextField = "Title";
                ddlCredit.DataValueField = "Id";
                ddlCredit.DataBind();
                ddlCredit.Items.Insert(0, new ListItem("Select Credit A/C ", "0"));
            }
       

    }
    #endregion

    protected void dgGallery_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            if (DataBinder.Eval(e.Row.DataItem, "InvDate").ToString() != "")
            {
                Label lblInvDate = (Label)e.Row.FindControl("lblinvDate");
                lblInvDate.Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Row.DataItem, "InvDate").ToString());
            }

            if (Sessions.UserRole.ToString() != "Admin")
            {


                Button btnEdit = (Button)e.Row.FindControl("btnEdit");

                string PageUrl = Request.Url.ToString();
                string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
                for (int i = 0; i < splitPageUrl.Length; i++)
                {
                    if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                    {
                        PageUrl = Convert.ToString(splitPageUrl[i]);
                    }
                }

                User ObjUser = new User();
                ObjUser.PageName = PageUrl;
                ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
                DataTable dt = ObjUser.getPageUserRights();
                if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
                {
                    Response.Redirect(Request.UrlReferrer.ToString());
                }

                if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
                {

                   // btnEdit.Visible = false;

                }

            }

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            objSalesInvoice.invDateTo = Convert.ToDateTime(txtInvoiceDateTo.Text);
            objSalesInvoice.InvDateFrom = Convert.ToDateTime(txtInvoiceDateFrom.Text);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.DebitAcId = Convert.ToInt32(ddlDebit.SelectedValue);
            objSalesInvoice.CreditAcId = Convert.ToInt32(ddlCredit.SelectedValue);
            objSalesInvoice.DataFrom = "SERINV";
            if (txtInvoiceNo.Text != "")
            {
                objSalesInvoice.ServiceInvoiceNo = Convert.ToInt32(txtInvoiceNo.Text);
            }
            else
            {
                objSalesInvoice.ServiceInvoiceNo = 0;
            }
            objSalesInvoice.CostCenterCode = Convert.ToInt32(ddlCostCenter.SelectedValue);
            DataTable dt = new DataTable();
            dt = objSalesInvoice.SearchServiceInvoice();

            Label lblTotalGrossAmount = (Label)e.Row.FindControl("lblTotalGrossAmount");
            decimal TotalGrossAmount = (decimal)dt.Compute("SUM(TotalAmount)", "");          
            lblTotalGrossAmount.Text = String.Format("{0:C}", TotalGrossAmount).Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

          


        }

    }

    #region Fill Cost Center
    protected void FillCostCenter()
    {
      
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objCust.CostCenterCode = 0;
            DataSet ds = new DataSet();
            ds = objCust.GetCostCenter();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlCostCenter.DataSource = ds.Tables[0];
                ddlCostCenter.DataTextField = "CostCenterName";
                ddlCostCenter.DataValueField = "CostCenterCode";
                ddlCostCenter.DataBind();
                ddlCostCenter.Items.Insert(0, new ListItem("Select cost center", "0"));

            }

        

    }
    #endregion


    protected void grdCustomer_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "EditCustomer")
        {
            Response.Redirect("ServiceInvoiceReport.aspx?InvoiceSummaryId=" + e.CommandArgument.ToString());
        }

    } 
}
