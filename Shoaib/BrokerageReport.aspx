﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true" CodeFile="BrokerageReport.aspx.cs" Inherits="BrokerageReport" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Brokerage Report"></asp:Label>

    <script type="text/javascript">
  
        function Count(text,long) 
        {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength)
            { 
                text.value = text.value.substring(0,maxlength);
                
            }
        }
          
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: relative; margin: 0px auto; padding: 0;">
                <div style="position: absolute; top: -100px; left: 37%">
                    <img src="images/loader.gif" />
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnInvoiceId" runat="server" />
            <%--<table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                width="68%">
                <tr>
                    <td style="text-align: left; ">
                        <span style="float: left; padding-right:15px">Item :</span>
                        <asp:DropDownList ID="ddlItem" runat="server" Style="width: 270px">
                        </asp:DropDownList>
                    </td>
                    <td style="text-align: left" align="left">
                        Transaction Date From:
                        <asp:TextBox ID="txtInvoiceDateFrom" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateFrom'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateFrom'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill Transaction Date From." Text="*" ControlToValidate="txtInvoiceDateFrom"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtInvoiceDateFrom" ValidationGroup="AddInvoice" ErrorMessage="Invalid Transaction Date From !!"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                    <td style="text-align: left" align="left">
                        To :
                        <asp:TextBox ID="txtInvoiceDateTo" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateTo'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateTo'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill Invoice Date." Text="*" ControlToValidate="txtInvoiceDateTo"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtInvoiceDateTo" ValidationGroup="AddInvoice" ErrorMessage="Invalid Transaction Date From !!"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <tr>
                        <td>
                            Sold To :
                            <asp:DropDownList ID="ddlSoldTo" runat="server" Style="width: 270px">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2" style="text-align: left">
                            <asp:Button ID="btnsearch" Text="Search" runat="server" OnClick="btnsearch_Click" />
                        </td>
                    </tr>
            </table>--%>
            <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                      Item :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                      <asp:DropDownList ID="ddlItem" runat="server" Style="width: 270px">
                        </asp:DropDownList>
                    </td>
                </tr>
                  <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                      Sold To :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                     <asp:DropDownList ID="ddlSoldTo" runat="server" Style="width: 270px">
                            </asp:DropDownList>
                      
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                        Transaction Date From:
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                        <asp:TextBox ID="txtInvoiceDateFrom" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateFrom'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateFrom'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill Transaction Date From." Text="*" ControlToValidate="txtInvoiceDateFrom"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtInvoiceDateFrom" ValidationGroup="AddInvoice" ErrorMessage="Invalid Transaction Date From !!"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
              

                <tr>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                         Transaction Date To:
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 5px; text-align: left">
                     <asp:TextBox ID="txtInvoiceDateTo" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateTo'),this);"
                            runat="server" Width="100px"></asp:TextBox>
                        <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                            onclick="scwShow(scwID('ctl00_MainHome_txtInvoiceDateTo'),this);" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill Invoice Date." Text="*" ControlToValidate="txtInvoiceDateTo"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtInvoiceDateTo" ValidationGroup="AddInvoice" ErrorMessage="Invalid Transaction Date From !!"
                            Display="None"></asp:RegularExpressionValidator> <span style="float: right; padding-right: 65px">
                            <asp:Button ID="btnsearch" Text="Search" TabIndex="6" runat="server" OnClick="btnsearch_Click" /></span>
                    </td>
                </tr>
                
            </table>
            <table cellspacing="0" style="float: left; padding: 0 10px 10px 10px;" border="0"
                cellpadding="0" width="90%">
                <tr>
                    <td colspan="3">
                        <asp:GridView ID="dgGallery" Width="100%" runat="server" AutoGenerateColumns="False"
                            AllowPaging="true" PageSize="500000" PagerSettings-Visible="false" PagerSettings-Mode="Numeric"
                            HeaderStyle-CssClass="gridheader" AlternatingRowStyle-CssClass="gridAlternateItem" OnRowCommand="grdCustomer_RowCommand"
                            OnRowDataBound="dgGallery_RowDataBound" GridLines="none" BorderColor="black"
                            FooterStyle-CssClass="gridFooter" BorderStyle="Dotted" BorderWidth="1" RowStyle-CssClass="gridItem"
                            ShowFooter="true">
                            <EmptyDataTemplate>
                                <div style="padding-bottom: 10px; width: 100%; text-align: center; font-family: Gisha;
                                    font-size: 20px; background-color: #E1F5FD; color: #110F0F;">
                                    <div style="padding-top: 10px; padding-bottom: 5px;">
                                        No record found
                                    </div>
                                </div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="S.No.">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              
                                <asp:TemplateField HeaderText="Sold To">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Eval("Title")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Item">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="15%" HorizontalAlign="Left" />
                                    <ItemTemplate>
                                        <%# Eval("ItemDesc")%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Transaction Date">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblinvDate" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                              
                                <asp:TemplateField HeaderText="Quantity">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("Lbs")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                      <asp:Label ID="lblTotalLbs" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Sale Rate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("SaleRate")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                 
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Sale Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("SaleAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                       <asp:Label ID="lblTotalSaleAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Purchese Rate">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("PurcheseRate")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                            
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Purchase Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("PurchaseAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                      <asp:Label ID="lblTotalPurchaseAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="P&L Amount">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle Font-Size="15px" />
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <ItemTemplate>
                                        <%#String.Format("{0:C}", Eval("PLAmount")).Replace('$', ' ')%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                       <asp:Label ID="lblPLAmount" runat="server"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>
                              
                                
                                <asp:TemplateField HeaderText="Edit">
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <ItemTemplate>
                                       <%-- <a href='Brokerage.aspx?InvoiceSummaryId=<%#Eval("BrokerageId") %>'>
                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" /></a>--%>

                                               <asp:Button ID="btnEdit" CommandName="EditCustomer" CommandArgument='<%#Eval("BrokerageId") %>' runat="server" Text="Edit" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


