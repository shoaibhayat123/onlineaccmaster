﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

public partial class F_Sql : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           // txtConStr.Text = ConfigurationManager.AppSettings["LPSConnection"].ToString();

        }
    }



    protected void btnLogin_Click(object sender, EventArgs e)
    {
        if (txtUserName.Text.ToLower() == "admin" && txtPassword.Text.ToLower() == "ubwebs@#@#")
        {
            Session["ConStr"] = txtConStr.Text;

            trMessage.Visible = false;
            tblLogin.Visible = false;
            tblQuery.Visible = true;
        }
        else
        {
            trMessage.Visible = true;
            tblLogin.Visible = true;
            tblQuery.Visible = false;
        }
    }

    protected void btnSub_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["ConStr"] != null)
            {
                SqlConnection cn = new SqlConnection(Session["ConStr"].ToString());
                cn.Open();
                SqlDataAdapter da = new SqlDataAdapter(txtQuery.Text, cn);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dgResult.DataSource = ds;
                dgResult.DataBind();
                trError.Visible = false;
                txtQuery.Text = string.Empty;
            }
            else
            {
                tblLogin.Visible = true;
                tblQuery.Visible = false;
            }
        }
        catch(Exception ex)
        {
            trError.Visible = true;
            lblError.Text = ex.ToString();
        
        }

    }
}
