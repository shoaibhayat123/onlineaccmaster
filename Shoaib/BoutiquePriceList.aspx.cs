﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class BoutiquePriceList : System.Web.UI.Page
{
    #region DataMembers & Varriables
    SetCustomers objCust = new SetCustomers();
    clsCookie ck = new clsCookie();
    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        else
        {
            Sessions.CustomerCode = Sessions.CustomerCode;
        }

        if (!IsPostBack)
        {
            FillMainCategory();
        }
    }

    #region Fill Main Category
    protected void FillMainCategory()
    {

        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.MainCategoryCode = 0;
        DataSet ds = new DataSet();
        ds = objCust.GetMainCategory();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlMainCategory.DataSource = ds.Tables[0];
            ddlMainCategory.DataTextField = "MainCategoryName";
            ddlMainCategory.DataValueField = "MainCategoryCode";
            ddlMainCategory.DataBind();
            ddlMainCategory.Items.Insert(0, new ListItem("Select Brand", "0"));
        }


    }
    #endregion
    #region Fill Sub Category

    protected void FillSubCategory()
    {
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.MainCategoryCode = Convert.ToInt32(ddlMainCategory.SelectedValue);
        DataSet ds = new DataSet();
        ds = objCust.GetSubCategoryByMainCategory();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlSubCategory.DataSource = ds.Tables[0];
            ddlSubCategory.DataTextField = "SubCategoryName";
            ddlSubCategory.DataValueField = "SubCategoryCode";
            ddlSubCategory.DataBind();
            ddlSubCategory.Items.Insert(0, new ListItem("Select Label", "0"));
        }

    }
    #endregion

    protected void ddlMainCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillSubCategory();
        ddlSubCategory.Focus();
    }


    protected void btnsearch_Click(object sender, EventArgs e)
    {

       string urllast ="" ;
       if (Convert.ToInt32(ddlMainCategory.SelectedValue) > 0)
       {
           urllast = "PrintBoutiquePriceList.aspx?MainCategoryCode=" + ddlMainCategory.SelectedValue + "&SubCategoryCode=" + ddlSubCategory.SelectedValue;

       }
       else
       {
           urllast = "PrintBoutiquePriceList.aspx?MainCategoryCode=" + ddlMainCategory.SelectedValue + "&SubCategoryCode=0" ;
       }
       ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);


        
        



    }
}