﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;

public partial class TestDbBackup : System.Web.UI.Page
{
    //Metioned here your database name
    string dbname = "ladak_7860008";
    SqlConnection sqlcon = new SqlConnection();
    SqlCommand sqlcmd = new SqlCommand();
    SqlDataAdapter da = new SqlDataAdapter();
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Mentioned Connection string make sure that user id and password sufficient previlages
        sqlcon.ConnectionString = @"Data Source=ADMIN\SQLEXPRESS;Initial Catalog=AccmasterTransDB;Integrated Security=True;";

        //Enter destination directory where backup file stored
        string destdir = "D:\\backupdb";

        //Check that directory already there otherwise create 
        if (!System.IO.Directory.Exists(destdir))
        {
            System.IO.Directory.CreateDirectory("D:\\backupdb");
        }
        //try
        //{
            //Open connection
            sqlcon.Open();
            //query to take backup database
            sqlcmd = new SqlCommand("backup database AccmasterTransDB to disk='" + destdir + "\\AccmasterTransDB.bak'", sqlcon);
            sqlcmd.ExecuteNonQuery();
            //Close connection
            sqlcon.Close();
            Response.Write("Backup database successfully");
        //}
        //catch (Exception ex)
        //{
        //    Response.Write("Error During backup database!");
        //}
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
       
    }
}