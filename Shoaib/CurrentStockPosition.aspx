﻿<%@ Page Language="C#" MasterPageFile="~/Innermaster.master" AutoEventWireup="true"
    CodeFile="CurrentStockPosition.aspx.cs" Inherits="CurrentStockPosition" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Current Stock Position"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0">
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; width: 85px;">
                As On :
            </td>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                <asp:TextBox ID="txtstartDate" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtstartDate'),this);"
                    runat="server" Width="100px"></asp:TextBox>
                <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                    onclick="scwShow(scwID('ctl00_MainHome_txtstartDate'),this);" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic"
                    ErrorMessage="Please Fill As On." Text="*" ControlToValidate="txtstartDate" ValidationGroup="Ledger"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                    ControlToValidate="txtstartDate" ValidationGroup="Ledger" ErrorMessage="Invalid As On !!"
                    Display="None"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
                Report Type :
            </td>
            <td>
                <asp:RadioButton ID="rdWith" GroupName="report" Checked="true" runat="server" Text="With 0 balance" />
                <asp:RadioButton ID="rdbWithOut" GroupName="report" runat="server" Text="Without  0 balance" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
            </td>
            <td>
                <asp:RadioButton ID="rdbWithValue" GroupName="reportType" Checked="true" runat="server"
                    Text="With Value" />
                <asp:RadioButton ID="rdbWithoutValue" GroupName="reportType" runat="server" Text="Without Value" />
              
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="padding: 2px 2px 2px 8px;">
            </td>
            <td>
            <asp:CheckBox ID="chkMinStockLevel"  Text="Min Stock Level" runat="server" />
              <asp:Button ID="btnGenerate" Text="Generate" ValidationGroup="Ledger" runat="server"
                    OnClick="btnGenerate_Click" />
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="true"
                    ShowSummary="false" ValidationGroup="Ledger" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
        </tr>
    </table>
</asp:Content>
