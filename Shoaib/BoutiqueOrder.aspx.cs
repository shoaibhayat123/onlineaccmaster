﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

public partial class BoutiqueOrder : System.Web.UI.Page
{
    #region DataMembers & Varriables
    clsCookie ck = new clsCookie();
    SetCustomers objCust = new SetCustomers();
    SalesInvoices objSalesInvoice = new SalesInvoices();
    AccFiles objFile = new AccFiles();
    DataTable dttbl = new DataTable();
    int Last = 0;
    int Corrent = 0;
    int FirstRecord = 0;
    int LastRecord = 0;
    string IpAddress ="";
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
         IpAddress = HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
        UpdateSession objUpdateSession = new UpdateSession();
        objUpdateSession.UpdateSessions();
        if (Sessions.CustomerCode == "")
        {
            if (ck.GetCookieValue("CustomerCode") != "")
            {
                Sessions.CustomerCode = Convert.ToString(ck.GetCookieValue("CustomerCode"));
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        if (!IsPostBack)
        {
            ViewState["s"] = null;

            FillAllLedgerAc();
            FillCostCenter();
            FillClientAC();
            FillSalesAc();
            FillItemDesc();
            FillCashReceived(1);
            FillCardChargesaccount();
            txtInvNo.Focus();
            DataTable dt = new DataTable();
            dgGallery.DataSource = dt;
            dgGallery.DataBind();
            Session["Corrent"] = null;
            Session["FirstRecord"] = null;
            Session["LastRecord"] = null;
            Session["InvoiceSummary"] = null;
            if (Convert.ToString(Request["addnew"]) != "1")
            {

                bindpagging();
                if (Request["InvoiceSummaryId"] == null)
                {
                    BindInvoice();
                }
                else
                {
                    DataTable dtInvoiceSummary = new DataTable();
                    dtInvoiceSummary = (DataTable)Session["InvoiceSummary"];
                    DataView dv = new DataView(dtInvoiceSummary);
                    dv.RowFilter = "InvoiceSummaryId=" + Request["InvoiceSummaryId"];
                    dtInvoiceSummary = dv.ToTable();
                    if (dtInvoiceSummary.Rows.Count > 0)
                    {
                        Session["Corrent"] = Convert.ToInt32(dtInvoiceSummary.Rows[0]["row"]);
                        BindInvoice();
                    }
                    else
                    {
                        Response.Redirect("default.aspx");
                    }
                }

            }
            else
            {
                objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                objSalesInvoice.InvoiceSummaryId = 0;
                objSalesInvoice.DataFrom = "BOUTORDER";
                DataTable dtInvoiceNewDetails = new DataTable();
                dtInvoiceNewDetails = objSalesInvoice.getBoutiqueOrderSummary();
                if (dtInvoiceNewDetails.Rows.Count > 0)
                {
                    int Last = Convert.ToInt32(dtInvoiceNewDetails.Rows.Count) - 1;
                    ddlSaleAC.SelectedValue = Convert.ToString(dtInvoiceNewDetails.Rows[Last]["SalesAccountId"]);
                    // ddlSaleTaxAC.SelectedValue = Convert.ToString(dtInvoiceNewDetails.Rows[Last]["SaleTaxAccountId"]);
                    spanLastInvoice.Style.Add("display", "");
                    litLastInvoiceNo.Text = Convert.ToString(dtInvoiceNewDetails.Rows[Last]["InvNo"]);
                    litLastInvDate.Text = ClsGetDate.FillFromDate(Convert.ToString(dtInvoiceNewDetails.Rows[Last]["InvDate"]));
                    txtInvoiceDate.Text = Convert.ToDateTime(dtInvoiceNewDetails.Rows[Last]["InvDate"]).ToString("dd MMM yyyy");
                    string Str = Convert.ToString(dtInvoiceNewDetails.Rows[Last]["InvNo"]);

                    double Num;
                    bool isNum = double.TryParse(Str, out Num);
                    if (isNum)
                    {
                        txtInvNo.Text = (Convert.ToInt32(dtInvoiceNewDetails.Rows[Last]["InvNo"]) + 1).ToString();
                    }
                }
                else
                {
                    txtInvoiceDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
                    txtInvNo.Text = "1";
                }
                spanPgging.Style.Add("display", "none");
                btnDeleteInvoice.Style.Add("display", "none");
                //btnPrint.Style.Add("display", "none");
                btnPrintBill.Style.Add("display", "none");
            }
        }

        SetUserRight();

    }
    #endregion

    #region Fill Item Description
    protected void FillItemDesc()
    {
        objCust.ItemId = 0;
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlItemDesc.DataSource = ds.Tables[0];
            ddlItemDesc.DataTextField = "ItemDesc";
            ddlItemDesc.DataValueField = "ItemId";
            ddlItemDesc.DataBind();
            ddlItemDesc.Items.Insert(0, new ListItem("Select Item", "0"));


        }

    }
    #endregion

    #region Fill Cost Center
    protected void FillCostCenter()
    {

        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.CostCenterCode = 0;
        DataSet ds = new DataSet();
        ds = objCust.GetCostCenter();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlCostCenter.DataSource = ds.Tables[0];
            ddlCostCenter.DataTextField = "CostCenterName";
            ddlCostCenter.DataValueField = "CostCenterCode";
            ddlCostCenter.DataBind();
            ddlCostCenter.Items.Insert(0, new ListItem("Select cost center", "0"));

        }



    }
    #endregion

    #region Fill  Client A/C
    protected void FillClientAC()
    {

        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtClient = new DataTable();
        dtClient = objSalesInvoice.getAcFileAllLedger();
        if (dtClient.Rows.Count > 0)
        {
            ddlClientAC.DataSource = dtClient;
            ddlClientAC.DataTextField = "Title";
            ddlClientAC.DataValueField = "Id";
            ddlClientAC.DataBind();
            ddlClientAC.Items.Insert(0, new ListItem("Select Client A/c", "0"));

        }



    }
    protected void FillCardChargesaccount()
    {

        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtClient = new DataTable();
        dtClient = objSalesInvoice.getAcFileAllLedger();
        if (dtClient.Rows.Count > 0)
        {
            ddlCardChargesaccount.DataSource = dtClient;
            ddlCardChargesaccount.DataTextField = "Title";
            ddlCardChargesaccount.DataValueField = "Id";
            ddlCardChargesaccount.DataBind();
            ddlCardChargesaccount.Items.Insert(0, new ListItem("Select Card Charges A/c", "0"));

        }



    }



    #endregion

    #region Fill  FillSales A/c
    protected void FillSalesAc()
    {

        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtLedger = new DataTable();
        dtLedger = objSalesInvoice.getAcFileAllLedger();
        if (dtLedger.Rows.Count > 0)
        {
            ddlSaleAC.DataSource = dtLedger;
            ddlSaleAC.DataTextField = "Title";
            ddlSaleAC.DataValueField = "Id";
            ddlSaleAC.DataBind();
            ddlSaleAC.Items.Insert(0, new ListItem("Select Sales A/C", "0"));


            //ddlSaleTaxAC.DataSource = dtLedger;
            //ddlSaleTaxAC.DataTextField = "Title";
            //ddlSaleTaxAC.DataValueField = "Id";
            //ddlSaleTaxAC.DataBind();
            //ddlSaleTaxAC.Items.Insert(0, new ListItem("Select Sales A/C", "0"));

        }



    }
    #endregion

    #region Add Details

    public void DataforTbltrans(Int32 InvoiceSummaryID)
    {
        for (int i = 0; i < 2; i++)
        {
            objSalesInvoice.TransId = 0;

            objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvoiceDate.Text);

            objSalesInvoice.DataFrom = "BOUTORDER";
            objSalesInvoice.InvoiceNo = txtInvNo.Text.ToUpper();

            if (txtCrDays.Text != "")
            {
                objSalesInvoice.Crdays = Convert.ToInt32(txtCrDays.Text);
            }
            else
            {
                objSalesInvoice.Crdays = 0;
            }

            /**** Add CR Days *******/
            System.DateTime today = Convert.ToDateTime(txtInvoiceDate.Text);
            if (txtCrDays.Text != "")
            {
                System.DateTime answer = today.AddDays(Convert.ToInt32(txtCrDays.Text));
                objSalesInvoice.DueDate = answer;
            }
            else
            {
                System.DateTime answer = Convert.ToDateTime(txtInvoiceDate.Text);
                objSalesInvoice.DueDate = answer;
            }

            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.InvoiceSummaryId = InvoiceSummaryID; /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
            if (i == 0)  /* For Client account */
            {
                objSalesInvoice.AcFileId = Convert.ToInt32(ddlClientAC.SelectedValue);
                objSalesInvoice.TransAmt = Convert.ToDecimal(lblBillAmount.Text);
                objSalesInvoice.Description = txtDescription.Text;
            }

            if (i == 1) /* For Sales  account */
            {
                objSalesInvoice.AcFileId = Convert.ToInt32(ddlSaleAC.SelectedValue);
                objSalesInvoice.TransAmt = 0 - (Convert.ToDecimal(txtTotalGrossAmount.Text) + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text));
                objSalesInvoice.Description = txtDescription.Text;
            }


            objSalesInvoice.AddEditTrans();

        }
    }

    protected void btn_saveInvoice_Click(object sender, EventArgs e)
    {
        hdnButtonText.Value = "";
        if (Sessions.FromDate != null)
        {
            if (ViewState["s"] != null)
            {
                if ((Convert.ToDateTime(txtInvoiceDate.Text) >= Convert.ToDateTime(Sessions.FromDate)) && (Convert.ToDateTime(txtInvoiceDate.Text) <= Convert.ToDateTime(Sessions.ToDate)))
                {
                    if (Convert.ToDecimal(lblBillAmount.Text) >= 0)
                    {
                        int Flag = 0;
                        //if (((rdbCash.Checked == true) || (rdbcashCredit.Checked == true)) && (Convert.ToDecimal(txtCashReceived.Text) >= Convert.ToDecimal(lblBillAmount.Text)))
                        if (((rdbCash.Checked == true) || (rdbcashCredit.Checked == true)) )
                        {
                            // /******/
                            if (ddlCashReceipt.SelectedValue.ToString() != "0")
                            {
                                Flag = 1;
                            }

                        }
                        else
                        {
                            if (rdbCredit.Checked == true)
                            {
                                Flag = 1;
                            }

                        }
                        if (Flag == 1)
                        {

                            if (hdnInvoiceSummaryId.Value == "")
                            {
                                hdnInvoiceSummaryId.Value = "0";
                            }
                            /************ Add Invoice Summary **************/
                            if (Convert.ToInt32(hdnInvoiceSummaryId.Value) > 0)
                            {
                                objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(hdnInvoiceSummaryId.Value);
                            }
                            else
                            {
                                objSalesInvoice.InvoiceSummaryId = 0;
                            }
                            objSalesInvoice.InvNo = txtInvNo.Text.ToUpper();
                            objSalesInvoice.InvDate = Convert.ToDateTime(txtInvoiceDate.Text);
                            if (txtCrDays.Text != "")
                            {
                                objSalesInvoice.CrDays = Convert.ToInt32(txtCrDays.Text);
                            }
                            else
                            {
                                objSalesInvoice.CrDays = 0;
                            }
                            objSalesInvoice.storeId = Convert.ToInt32(ddlCostCenter.SelectedValue);
                            objSalesInvoice.ClientAccountId = Convert.ToInt32(ddlClientAC.SelectedValue);
                            objSalesInvoice.SalesAccountId = Convert.ToInt32(ddlSaleAC.SelectedValue);
                            //objSalesInvoice.SaleTaxAccountId = Convert.ToInt32(ddlSaleTaxAC.SelectedValue);
                            objSalesInvoice.TotalQuantity = Convert.ToDecimal(txtTotalQuantity.Text);
                            objSalesInvoice.TotalGrossAmount = Convert.ToDecimal(txtTotalGrossAmount.Text);
                            //objSalesInvoice.TotalSaleTaxAmount = Convert.ToDecimal(txtSTtotalAmount.Text);
                            // objSalesInvoice.TotalAmountIncludeTax = Convert.ToDecimal(txtTotalAmountIncludingTaxe.Text);
                            objSalesInvoice.CartageAmount = Convert.ToDecimal(txtCartagecharges.Text);
                            objSalesInvoice.BillAmount = Convert.ToDecimal(lblBillAmount.Text);
                            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            objSalesInvoice.Description = txtDescription.Text;
                            objSalesInvoice.InvoiceDiscount = Convert.ToDecimal(txtDiscount.Text);
                            objSalesInvoice.DataFrom = "BOUTORDER";
                            objSalesInvoice.CashReceived = Convert.ToDecimal(txtCashReceived.Text);
                            objSalesInvoice.CashReturned = Convert.ToDecimal(txtCashReturned.Text);
                            objSalesInvoice.CashReceivedAcFileID = Convert.ToInt32(ddlCashReceipt.SelectedValue);
                            if (rdbCash.Checked == true)
                            {
                                objSalesInvoice.POSSaleType = rdbCash.Text;
                            }
                            if (rdbcashCredit.Checked == true)
                            {
                                objSalesInvoice.POSSaleType = rdbcashCredit.Text;
                                objSalesInvoice.CreditCardNo = txtCreditCarNo.Text;
                                objSalesInvoice.Approval = txtApprovelNO.Text;
                            }
                            if (rdbCredit.Checked == true)
                            {
                                objSalesInvoice.POSSaleType = rdbCredit.Text;
                            }
                            if (Convert.ToString(txtCardCharge.Text) != "")
                            {
                                objSalesInvoice.CardChargesRate = Convert.ToDecimal(txtCardCharge.Text);
                            }
                            if (Convert.ToString(hdnCardamount.Value) != "")
                            {
                                objSalesInvoice.CardChargesAmount = Convert.ToDecimal(hdnCardamount.Value);
                            }

                            ////////Add 3 new column
                            if (txtCustomerName.Text != "")
                            {
                                objSalesInvoice.CustomerName = txtCustomerName.Text;
                            }
                            else
                            {
                                objSalesInvoice.CustomerName = "-";
                            }
                            if (txtContactNumber.Text != "")
                            {
                                objSalesInvoice.ContactNumber = txtContactNumber.Text;
                            }
                            else
                            {
                                objSalesInvoice.ContactNumber = "-";
                            }
                            if (txtEmailAddress.Text != "")
                            {
                                objSalesInvoice.EmailAddress = txtEmailAddress.Text;
                            }
                            else
                            {
                                objSalesInvoice.EmailAddress = "-";
                            }


                            objSalesInvoice.CardChargesAcfileId = Convert.ToInt32(ddlCardChargesaccount.SelectedValue);

                            objSalesInvoice.OrderStatus = Convert.ToString(ddlStatus.SelectedValue);
                            DataTable dtresult = new DataTable();
                            dtresult = objSalesInvoice.AddEditBoutiqueOrderSummary();

                            BasicDetailClear();

                            /////
                            /************ Add Trans ***************/

                            objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            objSalesInvoice.DataFrom = "BOUTORDER";
                            objSalesInvoice.DeleteTrans();
                            DataforTbltrans(Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()));
                            #region Commented code
                            ///////////////////////Modify date 24 Nov 2012 /////////
                            //if (rdbCredit.Checked == true) //for credit
                            //{
                            //    DataforTbltrans(Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()));
                            //}
                          //if (rdbCash.Checked == true) //for Cash
                          //{
                             

                              //add new 2 rows data for cash
                              //for (int i = 0; i < 2; i++)
                              //{
                              //    objSalesInvoice.TransId = 0;

                              //    objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvoiceDate.Text);

                              //    objSalesInvoice.DataFrom = "BOUTORDER";
                              //    objSalesInvoice.InvoiceNo = txtInvNo.Text.ToUpper();

                              //    if (txtCrDays.Text != "")
                              //    {
                              //        objSalesInvoice.Crdays = Convert.ToInt32(txtCrDays.Text);
                              //    }
                              //    else
                              //    {
                              //        objSalesInvoice.Crdays = 0;
                              //    }

                              //    /**** Add CR Days *******/
                              //    System.DateTime today = Convert.ToDateTime(txtInvoiceDate.Text);
                              //    if (txtCrDays.Text != "")
                              //    {
                              //        System.DateTime answer = today.AddDays(Convert.ToInt32(txtCrDays.Text));
                              //        objSalesInvoice.DueDate = answer;
                              //    }
                              //    else
                              //    {
                              //        System.DateTime answer = Convert.ToDateTime(txtInvoiceDate.Text);
                              //        objSalesInvoice.DueDate = answer;
                              //    }

                              //    objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                              //    objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                              //    if (i == 0)  /* For Client account */
                              //    {
                              //        objSalesInvoice.AcFileId = Convert.ToInt32(ddlClientAC.SelectedValue);
                              //        objSalesInvoice.TransAmt = 0 - (Convert.ToDecimal(lblBillAmount.Text)); 
                              //        objSalesInvoice.Description = txtDescription.Text;

                              //    }

                              //    if (i == 1) /* For CashReceipt  account */
                              //    {
                              //        objSalesInvoice.AcFileId = Convert.ToInt32(ddlCashReceipt.SelectedValue);
                              //        objSalesInvoice.TransAmt = (Convert.ToDecimal(lblBillAmount.Text)); 
                              //        objSalesInvoice.Description = txtDescription.Text;
                              //    }


                              //    objSalesInvoice.AddEditTrans();

                              //}

                       //   }
                            //else if (rdbcashCredit.Checked == true) //Credit card
                            //{
                            //    DataforTbltrans(Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()));

                            //    //add new 2 rows data 
                            //    for (int i = 0; i < 2; i++)
                            //    {
                            //        objSalesInvoice.TransId = 0;

                            //        objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvoiceDate.Text);

                            //        objSalesInvoice.DataFrom = "BOUTORDER";
                            //        objSalesInvoice.InvoiceNo = txtInvNo.Text.ToUpper();

                            //        if (txtCrDays.Text != "")
                            //        {
                            //            objSalesInvoice.Crdays = Convert.ToInt32(txtCrDays.Text);
                            //        }
                            //        else
                            //        {
                            //            objSalesInvoice.Crdays = 0;
                            //        }

                            //        /**** Add CR Days *******/
                            //        System.DateTime today = Convert.ToDateTime(txtInvoiceDate.Text);
                            //        if (txtCrDays.Text != "")
                            //        {
                            //            System.DateTime answer = today.AddDays(Convert.ToInt32(txtCrDays.Text));
                            //            objSalesInvoice.DueDate = answer;
                            //        }
                            //        else
                            //        {
                            //            System.DateTime answer = Convert.ToDateTime(txtInvoiceDate.Text);
                            //            objSalesInvoice.DueDate = answer;
                            //        }

                            //        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            //        objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                            //        if (i == 0)  /* For Client account */
                            //        {
                            //            objSalesInvoice.AcFileId = Convert.ToInt32(ddlCashReceipt.SelectedValue);
                            //            objSalesInvoice.TransAmt = Convert.ToDecimal(txtCashReceived.Text) - Convert.ToDecimal(txtCashReturned.Text);
                            //            objSalesInvoice.Description = txtDescription.Text;
                            //        }

                            //        if (i == 1) /* For CashReceipt  account */
                            //        {
                            //            objSalesInvoice.AcFileId = Convert.ToInt32(ddlClientAC.SelectedValue);
                            //            objSalesInvoice.TransAmt = 0 - (Convert.ToDecimal(txtCashReceived.Text) - Convert.ToDecimal(txtCashReturned.Text));
                            //            objSalesInvoice.Description = txtDescription.Text;
                            //        }


                            //        objSalesInvoice.AddEditTrans();
                            //    }

                            //    //add new 2 rows data for Credit card txtCardCharge
                            //    if (txtCardCharge.Text != "0" && txtCardCharge.Text != "" && txtCardCharge.Text != "0.00")
                            //    {
                            //        for (int i = 0; i < 2; i++)
                            //        {
                            //            objSalesInvoice.TransId = 0;
                            //            objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvoiceDate.Text);
                            //            objSalesInvoice.DataFrom = "BOUTORDER";
                            //            objSalesInvoice.InvoiceNo = txtInvNo.Text.ToUpper();
                            //            if (txtCrDays.Text != "")
                            //            {
                            //                objSalesInvoice.Crdays = Convert.ToInt32(txtCrDays.Text);
                            //            }
                            //            else
                            //            {
                            //                objSalesInvoice.Crdays = 0;
                            //            }
                            //            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            //            objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/

                            //            if (i == 0)  /* For Client account */
                            //            {
                            //                objSalesInvoice.AcFileId = Convert.ToInt32(ddlCardChargesaccount.SelectedValue);
                            //                if (Convert.ToString(hdnCardamount.Value) != "")
                            //                {
                            //                    objSalesInvoice.TransAmt = Convert.ToDecimal(hdnCardamount.Value);
                            //                }
                            //            }
                            //            if (i == 1)  /* For Client account */
                            //            {

                            //                objSalesInvoice.AcFileId = Convert.ToInt32(ddlCashReceipt.SelectedValue);
                            //                if (Convert.ToString(hdnCardamount.Value) != "")
                            //                {
                            //                    objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(hdnCardamount.Value);
                            //                }
                            //            }
                            //            objSalesInvoice.Description = "Credit Card Charges On  " + txtDescription.Text; //Credit Card Charges On 
                            //            objSalesInvoice.AddEditTrans();
                            //        }
                            //    }

                            //}

                            /////add new records in trans according to alter charges

                            //DataTable dtTransData = (DataTable)Session["s"];

                            //if (dtTransData.Rows.Count > 0)
                            //{
                            //    for (int x = 0; x < dtTransData.Rows.Count; x++)
                            //    {
                            //        if (Convert.ToString(dtTransData.Rows[x]["AlterCharges"]) != "0.00" && Convert.ToString(dtTransData.Rows[x]["AlterCharges"]) != "0")
                            //        {

                            //            for (int i = 0; i < 2; i++)
                            //            {
                            //                objSalesInvoice.TransId = 0;

                            //                objSalesInvoice.TransactionDate = Convert.ToDateTime(txtInvoiceDate.Text);

                            //                objSalesInvoice.DataFrom = "BOUTORDER";
                            //                objSalesInvoice.InvoiceNo = txtInvNo.Text.ToUpper();

                            //                if (txtCrDays.Text != "")
                            //                {
                            //                    objSalesInvoice.Crdays = Convert.ToInt32(txtCrDays.Text);
                            //                }
                            //                else
                            //                {
                            //                    objSalesInvoice.Crdays = 0;
                            //                }

                            //                /**** Add CR Days *******/
                            //                System.DateTime today = Convert.ToDateTime(txtInvoiceDate.Text);
                            //                if (txtCrDays.Text != "")
                            //                {
                            //                    System.DateTime answer = today.AddDays(Convert.ToInt32(txtCrDays.Text));
                            //                    objSalesInvoice.DueDate = answer;
                            //                }
                            //                else
                            //                {
                            //                    System.DateTime answer = Convert.ToDateTime(txtInvoiceDate.Text);
                            //                    objSalesInvoice.DueDate = answer;
                            //                }

                            //                objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                            //                objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                            //                if (i == 0)  /* For Dr. account */
                            //                {
                            //                    objSalesInvoice.AcFileId = Convert.ToInt32(dtTransData.Rows[x]["DRID"].ToString()); //Convert.ToInt32(ddlCashReceipt.SelectedValue);
                            //                    objSalesInvoice.TransAmt = Convert.ToDecimal(dtTransData.Rows[x]["AlterCharges"].ToString()); //Convert.ToDecimal(txtCashReceived.Text) - Convert.ToDecimal(txtCashReturned.Text);
                            //                    objSalesInvoice.Description = "Charges on Inv. No. " + txtInvNo.Text.ToUpper() + " Item : " + Convert.ToString(dtTransData.Rows[x]["ItemDesc"]);
                            //                }

                            //                if (i == 1) /* For Cr.  account */
                            //                {
                            //                    objSalesInvoice.AcFileId = Convert.ToInt32(dtTransData.Rows[x]["CRID"].ToString()); // Convert.ToInt32(ddlClientAC.SelectedValue);
                            //                    objSalesInvoice.TransAmt = 0 - Convert.ToDecimal(dtTransData.Rows[x]["AlterCharges"].ToString()); // 0 - (Convert.ToDecimal(txtCashReceived.Text) - Convert.ToDecimal(txtCashReturned.Text));
                            //                    objSalesInvoice.Description = "Charges on Inv. No. " + txtInvNo.Text.ToUpper() + " Item : " + Convert.ToString(dtTransData.Rows[x]["ItemDesc"]);
                            //                }
                            //                objSalesInvoice.AddEditTrans();
                            //            }

                            //        }
                            //    }
                              //}

                            #endregion
                             logMessage("Summary result :" + Convert.ToString(dtresult.Rows[0]["InvoiceSummaryId"].ToString()), IpAddress);
                            
                              logMessage("Save Detail Start", IpAddress);
                              
                            /************ Add Invoice Detail **************/
                            if (dtresult.Rows.Count > 0)
                            {
                                if (ViewState["s"] != null)
                                {
                                    
                                    int intEmpty = 0;
                                    DataTable dt = new DataTable();
                                    dt = (DataTable)ViewState["s"];
                                    logMessage("Final Total record : " + Convert.ToString(dt.Rows.Count), IpAddress);
                                    if (dt.Rows.Count > 0)
                                    {
                                       
                                        for (int i = 0; i < dt.Rows.Count; i++)
                                        {
                                            objSalesInvoice.ItemId = 0;
                                            objSalesInvoice.ItemCode = Convert.ToInt32(dt.Rows[i]["ItemCode"].ToString());
                                            objSalesInvoice.Quantity = Convert.ToDecimal(dt.Rows[i]["Quantity"].ToString());
                                            objSalesInvoice.Rate = Convert.ToDecimal(dt.Rows[i]["Rate"].ToString());
                                            objSalesInvoice.GrossAmount = Convert.ToDecimal(dt.Rows[i]["GrossAmount"].ToString());
                                            objSalesInvoice.SaleTax = Convert.ToDecimal(dt.Rows[i]["SaleTax"].ToString());
                                            objSalesInvoice.SaleTaxAmount = Convert.ToDecimal(dt.Rows[i]["SaleTaxAmount"].ToString());
                                            objSalesInvoice.AmountIncludeTaxes = Convert.ToDecimal(dt.Rows[i]["AmountIncludeTaxes"].ToString());
                                            objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(dtresult.Rows[0]["InvoiceSummaryId"].ToString()); /***** dtresult.Rows[0] is for invoicesummaryid which is new****/
                                            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                                            objSalesInvoice.DataFrom = "BOUTORDER";
                                            objSalesInvoice.storeId = Convert.ToInt32(ddlCostCenter.SelectedValue);

                                            //Add Alter charges ,Dr. , Cr.
                                            if (dt.Rows[i]["AlterCharges"].ToString() != "")
                                            {
                                                objSalesInvoice.AlterCharges = Convert.ToDecimal(dt.Rows[i]["AlterCharges"].ToString());
                                            }
                                            if (dt.Rows[i]["DRID"].ToString() != "")
                                            {
                                                objSalesInvoice.DRID = Convert.ToInt32(dt.Rows[i]["DRID"].ToString());
                                            }
                                            else
                                            {
                                                objSalesInvoice.DRID = intEmpty;
                                            }

                                            if (dt.Rows[i]["CRID"].ToString() != "")
                                            {
                                                objSalesInvoice.CRID = Convert.ToInt32(dt.Rows[i]["CRID"].ToString());
                                            }
                                            else
                                            {
                                                objSalesInvoice.CRID = intEmpty;
                                            }

                                            if (dt.Rows[i]["BoutiqueDiscount"].ToString() != "")
                                            {
                                                objSalesInvoice.BoutiqueDiscount = Convert.ToDecimal(dt.Rows[i]["BoutiqueDiscount"].ToString());
                                            }

                                            if (dt.Rows[i]["BoutiqueAmountAfterDis"].ToString() != "")
                                            {
                                                objSalesInvoice.BoutiqueAmountAfterDis = Convert.ToDecimal(dt.Rows[i]["BoutiqueAmountAfterDis"].ToString());
                                            }

                                            DataTable dtInvoiceSummary = new DataTable();
                                            dtInvoiceSummary = objSalesInvoice.AddEditBoutiqueOrderDetail();

                                            // Trans Entry For Contract Purchase //

                                        }
                                    }
                                }

                            }

  logMessage("Save Detail end", IpAddress);


                            if (Convert.ToInt32(hdnInvoiceSummaryId.Value) > 0)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Updated.');location.href='BoutiqueOrder.aspx';", true);

                            }

                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record Saved.');location.href='BoutiqueOrder.aspx';", true);
                            }
                        }
                        else
                        {
                            if (ddlCashReceipt.SelectedValue.ToString() == "0" && rdbcashCredit.Checked == true)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please select Credit Card A/C.');", true);
                            }
                            else if (ddlCashReceipt.SelectedValue.ToString() == "0" && rdbCash.Checked == true)
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please select Cash Receipt A/C.');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Invalid Cash Received Amount.');", true);
                            }
                        }

                    }

                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Invalid Invoice Amount.');", true);

                    }
                }

                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Date out of accouting year.');", true);

                }
            }

            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please Fill Items Details.');", true);
            }
        }
    }
    #endregion

    #region Cancel
    protected void btn_cancel_Click(object sender, EventArgs e)
    {


        Response.Redirect("BoutiqueOrder.aspx");

    }
    #endregion

    #region Fill session Table
    public void filltable()
    {
        if (ViewState["s"] == null)
        {

            DataColumn column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "ItemCode";
            column.Caption = "ItemCode";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Id";
            column.Caption = "Id";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ItemDesc";
            column.Caption = "ItemDesc";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Quantity";
            column.Caption = "Quantity";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "Rate";
            column.Caption = "Rate";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "GrossAmount";
            column.Caption = "GrossAmount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "SaleTax";
            column.Caption = "SaleTax";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "SaleTaxAmount";
            column.Caption = "SaleTaxAmount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "AmountIncludeTaxes";
            column.Caption = "AmountIncludeTaxes";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            // /**********/

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "AlterCharges";
            column.Caption = "AlterCharges";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "DRID";
            column.Caption = "DRID";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "DRTitle";
            column.Caption = "DRTitle";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "CRID";
            column.Caption = "CRID";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "CRTitle";
            column.Caption = "CRTitle";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);


            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "BoutiqueDiscount";
            column.Caption = "BoutiqueDiscount";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Decimal");
            column.ColumnName = "BoutiqueAmountAfterDis";
            column.Caption = "BoutiqueAmountAfterDis";
            column.ReadOnly = false;
            dttbl.Columns.Add(column);

            /**********/


        }
        else
        {
            dttbl = (DataTable)ViewState["s"];
        }
    }
    #endregion

    #region Add New Bill details
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
         logMessage("add new  record : "+Convert.ToString(dttbl.Rows.Count), IpAddress);
        hdnButtonText.Value = "";
        //AddDiscount();
        //// alter charges validation

        int MsgFlag = 0;

        if (txtAlterCharges.Text == "" || txtAlterCharges.Text == "0.00" || txtAlterCharges.Text == "0")
        {

            ddlDR.SelectedValue = "0";
            ddlCR.SelectedValue = "0";

            MsgFlag = 1;
        }
        else
        {
            if (ddlDR.SelectedValue.ToString() == "0" && ddlCR.SelectedValue.ToString() == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please select Dr. and Cr.');", true);
            }
            else if (ddlDR.SelectedValue.ToString() == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please select Dr.');", true);
            }
            else if (ddlCR.SelectedValue.ToString() == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Please select Cr.');", true);
            }
            else
            {
                MsgFlag = 1;
            }

            //RequiredFieldValidator10.Enabled = true;
            //RequiredFieldValidator11.Enabled = true;

        }

        if (MsgFlag == 1)
        {

            filltable();
            if (hdndetail.Value == "")
            {
                DataRow row;
                if (dttbl.Rows.Count > 0)
                {

                    int maxid = 0;
                    for (int i = 0; i < dttbl.Rows.Count; i++)
                    {
                        if (dttbl.Rows[i]["Id"].ToString() != "")
                        {
                            if (maxid < Convert.ToInt32(dttbl.Rows[i]["Id"].ToString()))
                            {
                                maxid = Convert.ToInt32(dttbl.Rows[i]["Id"].ToString());
                            }
                        }
                    }
                    decimal Rate = 0;
                    decimal SaleTex = 0;

                    /**********/
                    decimal AlterCharges = 0;

                    /**********/

                    row = dttbl.NewRow();
                    row["Id"] = maxid + 1;
                    row["ItemCode"] = Convert.ToInt32(ddlItemDesc.SelectedValue.ToString());
                    row["ItemDesc"] = Convert.ToString(ddlItemDesc.SelectedItem.Text);
                    row["Quantity"] = Convert.ToDecimal(txtQuantity.Text);
                    if (lblRateAfterDiscount.Text != "")
                    {
                        Rate = Convert.ToDecimal(lblRateAfterDiscount.Text);
                    }
                    //if (txtSt.Text != "")
                    //{
                    SaleTex = 0;
                    //}


                    row["Rate"] = Convert.ToDecimal(txtRate.Text);
                    row["GrossAmount"] = Rate * Convert.ToDecimal(txtQuantity.Text);
                    row["SaleTax"] = SaleTex;
                    row["SaleTaxAmount"] = (SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100;
                    row["AmountIncludeTaxes"] = (Rate * Convert.ToDecimal(txtQuantity.Text)) + ((SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100);

                    // /**********/
                    if (txtAlterCharges.Text != "")
                    {
                        AlterCharges = Convert.ToDecimal(txtAlterCharges.Text);
                    }

                    row["AlterCharges"] = AlterCharges;
                    if (ddlDR.SelectedValue.ToString() != "0")
                    {
                        row["DRID"] = Convert.ToInt32(ddlDR.SelectedValue.ToString());
                        row["DRTitle"] = ddlDR.SelectedItem.Text;
                    }
                    else
                    {
                        row["DRTitle"] = "";
                    }

                    if (ddlCR.SelectedValue.ToString() != "0")
                    {
                        row["CRID"] = Convert.ToInt32(ddlCR.SelectedValue.ToString());
                        row["CRTitle"] = ddlCR.SelectedItem.Text;
                    }
                    else
                    {
                        row["CRTitle"] = "";
                    }
                    if (txtBoutiqueDiscount.Text != "")
                    {

                        row["BoutiqueDiscount"] = Convert.ToDecimal(txtBoutiqueDiscount.Text);
                    }
                    if (lblRateAfterDiscount.Text != "")
                    {

                        row["BoutiqueAmountAfterDis"] = Convert.ToDecimal(lblRateAfterDiscount.Text);
                    }


                    /**********/

                }
                else
                {
                    decimal Rate = 0;
                    decimal SaleTex = 0;
                    /**********/
                    decimal AlterCharges = 0;

                    /**********/
                    row = dttbl.NewRow();
                    row["Id"] = 1;
                    row["ItemCode"] = Convert.ToInt32(ddlItemDesc.SelectedValue.ToString());
                    row["ItemDesc"] = Convert.ToString(ddlItemDesc.SelectedItem.Text);
                    row["Quantity"] = Convert.ToDecimal(txtQuantity.Text);
                    if (lblRateAfterDiscount.Text != "")
                    {
                        Rate = Convert.ToDecimal(lblRateAfterDiscount.Text);
                    }
                    //if (txtSt.Text != "")
                    //{
                    SaleTex = 0;
                    //}
                    row["Rate"] = txtRate.Text;
                    row["GrossAmount"] = Rate * Convert.ToDecimal(txtQuantity.Text);
                    row["SaleTax"] = SaleTex;
                    row["SaleTaxAmount"] = (SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100;
                    row["AmountIncludeTaxes"] = (Rate * Convert.ToDecimal(txtQuantity.Text)) + ((SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100);

                    ///  /**********/
                    if (txtAlterCharges.Text != "")
                    {
                        AlterCharges = Convert.ToDecimal(txtAlterCharges.Text);
                    }

                    row["AlterCharges"] = AlterCharges;
                    if (ddlDR.SelectedValue.ToString() != "0")
                    {
                        row["DRID"] = Convert.ToInt32(ddlDR.SelectedValue.ToString());
                        row["DRTitle"] = ddlDR.SelectedItem.Text;
                    }
                    //else
                    //{
                    //    row["DRID"] = null;
                    //}

                    if (ddlCR.SelectedValue.ToString() != "0")
                    {
                        row["CRID"] = Convert.ToInt32(ddlCR.SelectedValue.ToString());
                        row["CRTitle"] = ddlCR.SelectedItem.Text;
                    }
                    //else
                    //{
                    //    row["CRID"] = null;
                    //}

                    /**********/
                    if (txtBoutiqueDiscount.Text != "")
                    {

                        row["BoutiqueDiscount"] = Convert.ToDecimal(txtBoutiqueDiscount.Text);
                    }
                    if (lblRateAfterDiscount.Text != "")
                    {

                        row["BoutiqueAmountAfterDis"] = Convert.ToDecimal(lblRateAfterDiscount.Text);
                    }


                }
                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
            }

            else
            {
                decimal Rate = 0;
                decimal AlterCharges = 0;
                decimal SaleTex = 0;
                for (int i = 0; i < dttbl.Rows.Count; i++)
                {
                    if (dttbl.Rows[i]["Id"].ToString() == hdndetail.Value)
                    {
                        dttbl.Rows[i]["ItemCode"] = Convert.ToInt32(ddlItemDesc.SelectedValue.ToString());
                        dttbl.Rows[i]["ItemDesc"] = Convert.ToString(ddlItemDesc.SelectedItem.Text);
                        dttbl.Rows[i]["Quantity"] = Convert.ToDecimal(txtQuantity.Text);
                        if (lblRateAfterDiscount.Text != "")
                        {
                            Rate = Convert.ToDecimal(lblRateAfterDiscount.Text);
                        }
                        //if (txtSt.Text != "")
                        //{
                        SaleTex = 0;
                        //}
                        dttbl.Rows[i]["Rate"] = txtRate.Text;
                        dttbl.Rows[i]["GrossAmount"] = Rate * Convert.ToDecimal(txtQuantity.Text);
                        dttbl.Rows[i]["SaleTax"] = SaleTex;
                        dttbl.Rows[i]["SaleTaxAmount"] = (SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100;
                        dttbl.Rows[i]["AmountIncludeTaxes"] = (Rate * Convert.ToDecimal(txtQuantity.Text)) + ((SaleTex * (Rate * Convert.ToDecimal(txtQuantity.Text))) / 100);

                        /**********/
                        //dttbl.Rows[i]["CCCharges"] = ""; // Convert.ToDecimal(txtCCCharge.Text);
                        //dttbl.Rows[i]["AlterCharges"] = Convert.ToDecimal(txtAlterCharges.Text);
                        //dttbl.Rows[i]["OtherCharges"] = Convert.ToDecimal(txtOtherCharges.Text);

                        ///  /**********/
                        if (txtAlterCharges.Text != "")
                        {
                            AlterCharges = Convert.ToDecimal(txtAlterCharges.Text);
                        }

                        dttbl.Rows[i]["AlterCharges"] = AlterCharges;

                        dttbl.Rows[i]["AlterCharges"] = AlterCharges;
                        if (ddlDR.SelectedValue.ToString() != "0")
                        {
                            dttbl.Rows[i]["DRID"] = Convert.ToInt32(ddlDR.SelectedValue.ToString());
                            dttbl.Rows[i]["DRTitle"] = ddlDR.SelectedItem.Text;
                        }
                        //else
                        //{
                        //    row["DRID"] = null;
                        //}

                        if (ddlCR.SelectedValue.ToString() != "0")
                        {
                            dttbl.Rows[i]["CRID"] = Convert.ToInt32(ddlCR.SelectedValue.ToString());
                            dttbl.Rows[i]["CRTitle"] = ddlCR.SelectedItem.Text;
                        }
                        //else
                        //{
                        //    row["CRID"] = null;
                        //}

                        /**********/
                        if (txtBoutiqueDiscount.Text != "")
                        {

                            dttbl.Rows[i]["BoutiqueDiscount"] = Convert.ToDecimal(txtBoutiqueDiscount.Text);
                        }

                        if (lblRateAfterDiscount.Text != "")
                        {

                            dttbl.Rows[i]["BoutiqueAmountAfterDis"] = Convert.ToDecimal(lblRateAfterDiscount.Text);
                        }



                    }
                }
            }
            ViewState.Add("s", dttbl);

               logMessage("Session record : "+Convert.ToString(dttbl.Rows.Count), IpAddress);
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            ShowTotal();
            if (dttbl.Rows.Count > 0)
            {
                dgGallery.Visible = true;
            }

            ClearInvoiceDetail();
            txtBarCodeNo.Focus();
        }
    }
    #endregion

    #region Grid Event
    protected void dgGallery_RowDataBound(object sender, DataGridItemEventArgs e)
    {
        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{
        //    ((Label)e.Item.FindControl("lblInvoiceDate")).Text = ClsGetDate.FillFromDate(DataBinder.Eval(e.Item.DataItem, "InvoiceDate").ToString());

        //}



    }
    protected void dgGallery_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        dttbl = (DataTable)ViewState["s"];
        if (e.CommandName == "Delete")
        {
            int j = 0;
            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    dttbl.Rows.RemoveAt(i);
                }
            }
            dttbl.AcceptChanges();
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            ViewState.Add("s", dttbl);
            ShowTotal();

        }

        if (e.CommandName == "Edit")
        {
            lblPurchaseRate.Text = "";

            for (int i = 0; i < dttbl.Rows.Count; i++)
            {
                if (dttbl.Rows[i]["Id"].ToString() == e.CommandArgument.ToString())
                {
                    ddlItemDesc.SelectedValue = dttbl.Rows[i]["ItemCode"].ToString();
                    txtQuantity.Text = dttbl.Rows[i]["Quantity"].ToString();
                    txtRate.Text = dttbl.Rows[i]["Rate"].ToString();
                    hdndetail.Value = dttbl.Rows[i]["Id"].ToString();
                    txtBarCodeNo.Text = dttbl.Rows[i]["ItemCode"].ToString();
                    //
                    /*******/
                    txtAlterCharges.Text = dttbl.Rows[i]["AlterCharges"].ToString();

                    if (dttbl.Rows[i]["DRID"].ToString() != "")
                    {
                        ddlDR.SelectedValue = dttbl.Rows[i]["DRID"].ToString();
                    }
                    if (dttbl.Rows[i]["CRID"].ToString() != "")
                    {
                        ddlCR.SelectedValue = dttbl.Rows[i]["CRID"].ToString();
                    }
                    txtBoutiqueDiscount.Text = dttbl.Rows[i]["BoutiqueDiscount"].ToString();
                    lblRateAfterDiscount.Text = dttbl.Rows[i]["BoutiqueAmountAfterDis"].ToString();

                    objCust.ItemId = Convert.ToInt32(txtBarCodeNo.Text);
                    objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
                    DataSet ds1 = new DataSet();
                    ds1 = objCust.getPurchasePP();
                    if (ds1.Tables[0].Rows.Count > 0)
                    {
                        lblPurchaseRate.Text = "P/P : " + (Convert.ToDecimal(ds1.Tables[0].Rows[0]["Rate"]) / Convert.ToDecimal(ds1.Tables[0].Rows[0]["BoutiqueConvRate"])).ToString("0.00");
                    }


                }
            }
        }
    }
    #endregion

    #region ClearInvoiceDetail
    protected void ClearInvoiceDetail()
    {
        ddlItemDesc.SelectedValue = "0";
        txtQuantity.Text = "";
        txtRate.Text = "";
        lblPurchaseRate.Text = "";
        hdndetail.Value = "";

        txtAlterCharges.Text = "";
        ddlDR.SelectedValue = "0";
        ddlCR.SelectedValue = "0";
        RequiredFieldValidator10.Enabled = false;
        RequiredFieldValidator11.Enabled = false;
        txtBoutiqueDiscount.Text = "0.00";
        txtBarCodeNo.Text = "";
        lblRateAfterDiscount.Text = "0.00";
        // txtSt.Text = "";
    }

    public void BasicDetailClear()
    {
        txtCustomerName.Text = "";
        txtContactNumber.Text = "";
        txtEmailAddress.Text = "";
    }

    #endregion

    #region Show Total
    protected void ShowTotal()
    {
        Decimal Quantity = 0;
        Decimal TotalGrossAmount = 0;
        Decimal TotalStAmount = 0;
        Decimal amountIncludingTax = 0;

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["s"];
        if (dt.Rows.Count > 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                Quantity += Convert.ToDecimal(dt.Rows[i]["Quantity"]);
                TotalGrossAmount += Convert.ToDecimal(dt.Rows[i]["GrossAmount"]);
                TotalStAmount += Convert.ToDecimal(dt.Rows[i]["SaleTaxAmount"]);
                amountIncludingTax += (Convert.ToDecimal(dt.Rows[i]["SaleTaxAmount"]) + Convert.ToDecimal(dt.Rows[i]["GrossAmount"]));

            }
        }

        txtTotalQuantity.Text = String.Format("{0:C}", Quantity).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        txtTotalGrossAmount.Text = String.Format("{0:C}", TotalGrossAmount).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        lblBillAmount.Text = String.Format("{0:C}", (amountIncludingTax + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');
        txtCashReturned.Text = String.Format("{0:C}", (Convert.ToDecimal(txtCashReceived.Text) - (amountIncludingTax + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text)))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' ');

        if (txtCardCharge.Text != "")
        {
            hdnCardamount.Value = Convert.ToString((amountIncludingTax + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text)) * (Convert.ToDecimal(txtCardCharge.Text) / 100));
            lblCardamount.Text = Convert.ToDecimal((amountIncludingTax + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text)) * (Convert.ToDecimal(txtCardCharge.Text) / 100)).ToString("0.00");
        }



    }
    #endregion

    #region Pagging
    private void bindpagging()
    {
        Last = 0;
        Corrent = 0;
        FirstRecord = 0;
        LastRecord = 0;
        //  int row = Convert.ToInt32(Request["row"]);
        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objSalesInvoice.InvoiceSummaryId = 0;
        DataTable dt = new DataTable();
        objSalesInvoice.DataFrom = "BOUTORDER";
        dt = objSalesInvoice.getBoutiqueOrderSummary();
        if (dt.Rows.Count > 0)
        {
            Session["InvoiceSummary"] = dt;
            Last = dt.Rows.Count;
            int i = Last - 1;
            FirstRecord = Convert.ToInt32(dt.Rows[0]["row"]);
            LastRecord = Convert.ToInt32(dt.Rows[i]["row"]);
            //Corrent = Convert.ToInt32(dt.Rows[0]["Row"]);
            Session["FirstRecord"] = FirstRecord;
            Session["LastRecord"] = LastRecord;
            Session["Corrent"] = LastRecord;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "msg", "location.href='BoutiqueOrder.aspx?addnew=1';", true);
        }


    }
    protected void lnkFirst_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["FirstRecord"];
        BindInvoice();

    }
    protected void lnkPrevious_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["FirstRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) - 1;
            BindInvoice();
        }

    }
    protected void lnkNext_Click(object sender, EventArgs e)
    {
        if (Session["Corrent"] != Session["LastRecord"])
        {
            Session["Corrent"] = Convert.ToInt32(Session["Corrent"]) + 1;
            BindInvoice();
        }
    }
    protected void lnLast_Click(object sender, EventArgs e)
    {
        Session["Corrent"] = Session["LastRecord"];
        BindInvoice();
    }
    #endregion

    #region Fill invoice summary
    protected void BindInvoice()
    {
        if (Session["InvoiceSummary"] != null)
        {
            DataTable dtInvoiceSummary = (DataTable)Session["InvoiceSummary"];
            if (dtInvoiceSummary.Rows.Count > 0)
            {

                DataView DvInvoiceSummary = dtInvoiceSummary.DefaultView;
                DvInvoiceSummary.RowFilter = "Row = '" + Session["Corrent"].ToString() + "'";
                dtInvoiceSummary = DvInvoiceSummary.ToTable();
                if (dtInvoiceSummary.Rows.Count > 0)
                {
                    txtInvNo.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["InvNo"]);
                    txtInvoiceDate.Text = Convert.ToDateTime(dtInvoiceSummary.Rows[0]["InvDate"].ToString()).ToString("dd MMM yyyy");

                    if (Convert.ToString(dtInvoiceSummary.Rows[0]["CrDays"]) != "0")
                    {
                        txtCrDays.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CrDays"]);
                    }
                    else
                    {
                        txtCrDays.Text = "";
                    }

                    //// bind details
                    if (dtInvoiceSummary.Rows[0]["CustomerName"].ToString() != "-")
                    {
                        txtCustomerName.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CustomerName"]);
                    }
                    else
                    {
                        txtCustomerName.Text = "";
                    }
                    if (dtInvoiceSummary.Rows[0]["ContactNumber"].ToString() != "-")
                    {
                        txtContactNumber.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["ContactNumber"]);
                    }
                    else
                    {
                        txtContactNumber.Text = "";
                    }
                    if (dtInvoiceSummary.Rows[0]["EmailAddress"].ToString() != "-")
                    {
                        txtEmailAddress.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["EmailAddress"]);
                    }
                    else
                    {
                        txtEmailAddress.Text = "";
                    }


                    ddlCostCenter.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["storeId"]);
                    ddlClientAC.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["ClientAccountId"]);
                    ddlSaleAC.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["SalesAccountId"]);
                    txtTotalQuantity.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["TotalQuantity"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));
                    txtTotalGrossAmount.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["TotalGrossAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

                    txtCartagecharges.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["CartageAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

                    lblBillAmount.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["BillAmount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

                    hdnInvoiceSummaryId.Value = Convert.ToString(dtInvoiceSummary.Rows[0]["InvoiceSummaryId"]);

                    txtDiscount.Text = Convert.ToString(String.Format("{0:C}", dtInvoiceSummary.Rows[0]["InvoiceDiscount"]).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

                    txtDescription.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["Description"]);
                    txtCardCharge.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CardChargesRate"]);
                    lblCardamount.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CardChargesAmount"]);
                    hdnCardamount.Value = Convert.ToString(dtInvoiceSummary.Rows[0]["CardChargesAmount"]);
                    ddlCardChargesaccount.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["CardChargesAcfileId"]);
                    ddlStatus.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["OrderStatus"]);

                    fillInvoiceDetails(Convert.ToInt32(dtInvoiceSummary.Rows[0]["InvoiceSummaryId"]));
                    if (Convert.ToString(dtInvoiceSummary.Rows[0]["POSSaleType"]) == "Cash")
                    {
                        rdbCash.Checked = true;
                        rdbcashCredit.Checked = false;
                        rdbCredit.Checked = false;
                        txtCreditCarNo.Enabled = false;
                        txtApprovelNO.Enabled = false;

                        FillCashReceived(1);
                        txtCashReceived.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CashReceived"]);
                        txtCashReturned.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CashReturned"]);

                    }
                    if (Convert.ToString(dtInvoiceSummary.Rows[0]["POSSaleType"]) == "Credit Card")
                    {
                        rdbCash.Checked = false;
                        rdbcashCredit.Checked = true;
                        rdbCredit.Checked = false;

                        //rdbcashCredit.Checked = true;
                        txtCreditCarNo.Enabled = true;
                        txtApprovelNO.Enabled = true;
                        txtCreditCarNo.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["POSSaleType"]);
                        txtApprovelNO.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["Approval"]);
                        txtCashReceived.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CashReceived"]);
                        txtCashReturned.Text = Convert.ToString(dtInvoiceSummary.Rows[0]["CashReturned"]);
                        FillCashReceived(2);

                    }
                    if (Convert.ToString(dtInvoiceSummary.Rows[0]["POSSaleType"]) == "Credit")
                    {
                        rdbCash.Checked = false;
                        rdbcashCredit.Checked = false;
                        rdbCredit.Checked = true;

                        txtCreditCarNo.Enabled = false;
                        txtApprovelNO.Enabled = false;
                        txtCashReceived.Text = "0.00";
                        txtCashReturned.Text = "0.00";
                        txtCashReceived.Enabled = false;
                        txtCashReturned.Enabled = false;
                        FillCashReceived(3);

                    }

                    ddlCashReceipt.SelectedValue = Convert.ToString(dtInvoiceSummary.Rows[0]["CashReceivedAcFileID"]);



                    /* For invoice Update */
                    //hdnInvoiceId.Value = Convert.ToString(dtInvoiceSummary.Rows[0]["InvoiceID"]);


                    //objSalesInvoice.CashReceived = Convert.ToDecimal(txtCashReceived.Text);
                    //objSalesInvoice.CashReturned = Convert.ToDecimal(txtCashReturned.Text);
                    //objSalesInvoice.CashReceivedAcFileID = 0;

                }
            }

        }
    }
    #endregion

    #region fillInvoiceDetails
    protected void fillInvoiceDetails(int pid)
    {
        ViewState["s"] = null;

        DataTable dtInvoice = new DataTable();
        objSalesInvoice.InvoiceSummaryId = pid;
        objSalesInvoice.CustomerCode = Convert.ToDecimal(Sessions.CustomerCode);
        objSalesInvoice.DataFrom = "BOUTORDER";
        dtInvoice = objSalesInvoice.getBoutiqueOrderDetails();

        if (dtInvoice.Rows.Count > 0)
        {
            for (int i = 0; i < dtInvoice.Rows.Count; i++)
            {
                filltable();
                DataRow row;
                row = dttbl.NewRow();
                row["Id"] = i + 1;

                row["ItemCode"] = Convert.ToInt32(dtInvoice.Rows[i]["ItemCode"]);
                row["ItemDesc"] = Convert.ToString(dtInvoice.Rows[i]["ItemDesc"]);
                row["Quantity"] = Convert.ToDecimal(dtInvoice.Rows[i]["Quantity"]);
                row["Rate"] = Convert.ToDecimal(dtInvoice.Rows[i]["Rate"]);
                row["GrossAmount"] = Convert.ToDecimal(dtInvoice.Rows[i]["GrossAmount"]);
                row["SaleTax"] = Convert.ToDecimal(dtInvoice.Rows[i]["SaleTax"]);
                row["SaleTaxAmount"] = Convert.ToDecimal(dtInvoice.Rows[i]["SaleTaxAmount"]);
                row["AmountIncludeTaxes"] = Convert.ToDecimal(dtInvoice.Rows[i]["AmountIncludeTaxes"]);

                //get altercharges, dr, cr
                if (dtInvoice.Rows[i]["AlterCharges"].ToString() != "")
                {
                    row["AlterCharges"] = Convert.ToDecimal(dtInvoice.Rows[i]["AlterCharges"]);
                }

                if (dtInvoice.Rows[i]["DRID"].ToString() != "")
                {
                    row["DRID"] = Convert.ToInt32(dtInvoice.Rows[i]["DRID"]);
                    row["DRTitle"] = Convert.ToString(dtInvoice.Rows[i]["DRTitle"]);
                }


                if (dtInvoice.Rows[i]["CRID"].ToString() != "")
                {
                    row["CRID"] = Convert.ToInt32(dtInvoice.Rows[i]["CRID"]);
                    row["CRTitle"] = Convert.ToString(dtInvoice.Rows[i]["CRTitle"]);
                }
                row["BoutiqueDiscount"] = Convert.ToDecimal(dtInvoice.Rows[i]["BoutiqueDiscount"]);
                row["BoutiqueAmountAfterDis"] = Convert.ToDecimal(dtInvoice.Rows[i]["BoutiqueAmountAfterDis"]);


                dttbl.Rows.Add(row);
                dttbl.AcceptChanges();
                ViewState.Add("s", dttbl);
            }
            dgGallery.DataSource = dttbl;
            dgGallery.DataBind();
            dgGallery.Visible = true;

        }
        else
        {
            dgGallery.Visible = false;
        }

    }
    #endregion

    #region Fill item rate
    protected void ddlItemDesc_SelectedIndexChanged(object sender, EventArgs e)
    {
        objCust.ItemId = Convert.ToInt32(ddlItemDesc.SelectedValue);
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        if (ds.Tables[0].Rows.Count == 1)
        {
            txtRate.Text = Convert.ToString(ds.Tables[0].Rows[0]["SaleRate"]);
            //  lblPurchaseRate.Text = "P/P : " + Convert.ToString(ds.Tables[0].Rows[0]["PurchaseRate"].ToString()); 
            txtBarCodeNo.Text = ddlItemDesc.SelectedValue;
            lblRateAfterDiscount.Text = Convert.ToString(ds.Tables[0].Rows[0]["SaleRate"]);
        }
        else
        {
            txtRate.Text = "";
            lblPurchaseRate.Text = "";
        }
        objCust.ItemId = Convert.ToInt32(ddlItemDesc.SelectedValue);
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        objCust.DataFrom = "";
        DataSet ds1 = new DataSet();
        ds1 = objCust.getPurchasePP();
        if (ds1.Tables[0].Rows.Count > 0)
        {

            lblPurchaseRate.Text = "P/P : " + (Convert.ToDecimal(ds1.Tables[0].Rows[0]["Rate"]) / Convert.ToDecimal(ds1.Tables[0].Rows[0]["BoutiqueConvRate"])).ToString("0.00");
        }

        //  AddDiscount();

        ScriptManager.RegisterStartupScript(this, GetType(), "test", "func('" + txtQuantity.ClientID + "');", true);
    }
    protected void txtBarCodeNo_TextChanged(object sender, EventArgs e)
    {
        objCust.ItemId = Convert.ToInt32(txtBarCodeNo.Text);
        objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataSet ds = new DataSet();
        ds = objCust.getItems();
        if (ds.Tables[0].Rows.Count == 1)
        {
            txtRate.Text = Convert.ToString(ds.Tables[0].Rows[0]["SaleRate"]);
            //  lblPurchaseRate.Text = "P/P : " + Convert.ToString(ds.Tables[0].Rows[0]["PurchaseRate"].ToString()); ;
            ddlItemDesc.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["ItemId"]);
            objCust.ItemId = Convert.ToInt32(txtBarCodeNo.Text);
            objCust.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            DataSet ds1 = new DataSet();
            ds1 = objCust.getPurchasePP();
            if (ds1.Tables[0].Rows.Count > 0)
            {
                lblPurchaseRate.Text = "P/P : " + (Convert.ToDecimal(ds1.Tables[0].Rows[0]["Rate"]) / Convert.ToDecimal(ds1.Tables[0].Rows[0]["BoutiqueConvRate"])).ToString("0.00");
            }
            txtQuantity.Focus();
        }
        else
        {
            txtBarCodeNo.Text = "";
            txtRate.Text = "";
            lblPurchaseRate.Text = "";
            ddlItemDesc.SelectedValue = "0";
            txtBarCodeNo.Focus();
        }



        //AddDiscount();
        ScriptManager.RegisterStartupScript(this, GetType(), "test", "func('" + txtQuantity.ClientID + "');", true);
    }



    #endregion

    #region Cartage

    //protected void txtDiscount_TextChanged(object sender, EventArgs e)
    //{
    //    if (txtDiscount.Text == "")
    //    {
    //        txtDiscount.Text = "0.00";
    //    }
    //    lblBillAmount.Text = Convert.ToString(String.Format("{0:C}", (Convert.ToDecimal(txtTotalGrossAmount.Text) + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

    //}

    //protected void txtCartagecharges_TextChanged(object sender, EventArgs e)
    //{
    //    if (txtCartagecharges.Text == "")
    //    {
    //        txtCartagecharges.Text = "0.00";
    //    }
    //    lblBillAmount.Text = Convert.ToString(String.Format("{0:C}", (Convert.ToDecimal(txtTotalGrossAmount.Text) + Convert.ToDecimal(txtCartagecharges.Text) - Convert.ToDecimal(txtDiscount.Text))).Replace('-', ' ').Replace('$', ' ').Replace('(', ' ').Replace(')', ' '));

    //}
    #endregion

    #region Delete Invoice
    protected void btnDeleteInvoice_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(hdnInvoiceSummaryId.Value) > 0)
        {
            int result = 0;
            objSalesInvoice.InvoiceSummaryId = Convert.ToInt32(hdnInvoiceSummaryId.Value);
            objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
            objSalesInvoice.DataFrom = "BOUTORDER";
            result = objSalesInvoice.deleteAllBoutiqueOrderData();
            if (result > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "msg", "alert('Record deleted successfully.');location.href='BoutiqueOrder.aspx';", true);
            }
        }

    }
    #endregion

    protected void btnPrintBill_Click(object sender, EventArgs e)
    {
        string urllast = "PrintBoutiqueOrder.aspx?InvoiceSummaryId=" + Convert.ToString(hdnInvoiceSummaryId.Value);
        ScriptManager.RegisterStartupScript(this, typeof(string), "OPEN_WINDOW", "window.open( '" + urllast + "', null, 'height=700,width=850,scrollbars=1,status=yes' );", true);

    }
    protected void SetUserRight()
    {
        if (Sessions.UserRole.ToString() != "Admin")
        {

            string PageUrl = Request.Url.ToString();
            string[] splitPageUrl = PageUrl.Split(new Char[] { '/', '?' });
            for (int i = 0; i < splitPageUrl.Length; i++)
            {
                if (Convert.ToString(splitPageUrl[i]).Contains(".aspx"))
                {
                    PageUrl = Convert.ToString(splitPageUrl[i]);
                }

            }

            User ObjUser = new User();
            ObjUser.PageName = PageUrl;
            ObjUser.Userid_UserRole = Convert.ToDecimal(Sessions.UserId);
            DataTable dt = ObjUser.getPageUserRights();
            if (Convert.ToString(dt.Rows[0]["IsAccess"]) != "True")
            {
                Response.Redirect(Request.UrlReferrer.ToString());
            }
            if (Convert.ToString(dt.Rows[0]["IsDelete"]) != "True")
            {
                btnDeleteInvoice.Visible = false;
            }
            if (Convert.ToString(dt.Rows[0]["IsPrint"]) != "True")
            {

                btnPrintBill.Visible = false;
            }

            if (Convert.ToString(dt.Rows[0]["IsAdd"]) != "True")
            {
                lblbutton.Visible = false;
                if (Convert.ToString(Request["addnew"]) == "1")
                {

                    ImageButton2.Visible = false;
                }
            }



            if (Convert.ToString(dt.Rows[0]["IsEdit"]) != "True")
            {
                if (Convert.ToString(Request["addnew"]) != "1")
                {

                    ImageButton2.Visible = false;
                }
            }

        }
    }
    protected void lblbutton_Click(object sender, EventArgs e)
    {
        Response.Redirect("BoutiqueOrder.aspx?addnew=1");
    }
    protected void OnCheckChangedCash(object sender, EventArgs e)
    {
        FillCashReceived(1);
        ScriptManager.RegisterStartupScript(this, GetType(), "test", "func('" + ddlCashReceipt.ClientID + "');", true);
    }
    protected void OnCheckChangedCreditCard(object sender, EventArgs e)
    {
        FillCashReceived(2);
        ScriptManager.RegisterStartupScript(this, GetType(), "test", "func('" + txtCreditCarNo.ClientID + "');", true);
    }
    protected void OnCheckChangedCredit(object sender, EventArgs e)
    {
        FillCashReceived(3);
        ScriptManager.RegisterStartupScript(this, GetType(), "test", "func('" + txtApprovelNO.ClientID + "');", true);

        if (rdbCredit.Checked == true)
        {
            //  txtDescription.Text = "Inv. # : " + txtInvNo.Text + ", Dated : " + ClsGetDate.FillFromDate(txtInvoiceDate.Text) + ", Credit Sale";
            txtDescription.Text = "Credit Sale Inv. # : " + txtInvNo.Text + ", Dated : " + ClsGetDate.FillFromDate(txtInvoiceDate.Text);
        }
        txtDescription.Focus();

    }
    #region Fill  Cash received A/c
    protected void FillCashReceived(int Type)
    {

        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtLedger = new DataTable();
        if (Type == 1)
        {
            dtLedger = objSalesInvoice.getAcFileByCashInHand();
            txtCreditCarNo.Enabled = false;
            txtApprovelNO.Enabled = false;
            txtCashReceived.Enabled = true;
            // ddlCashReceipt.Focus();
            lblCashReceipt.Text = "Cash Receipt A/C ";
            trApproval.Style.Add("display", "none");
            trCashReceipt.Style.Add("display", "");
            trCreditCard.Style.Add("display", "none");
            trCard1.Style.Add("display", "none");
            trCard2.Style.Add("display", "none");

            RequiredFieldValidator7.Enabled = false;
        }
        if (Type == 2)
        {
            dtLedger = objSalesInvoice.getAcFileBySundryDebtorOnly();
            txtCreditCarNo.Enabled = true;
            txtApprovelNO.Enabled = true;
            txtCashReceived.Enabled = true;
            // txtCreditCarNo.Focus();
            lblCashReceipt.Text = "Credit Card A/C ";
            trApproval.Style.Add("display", "");
            trCashReceipt.Style.Add("display", "");
            trCreditCard.Style.Add("display", "");
            trCard1.Style.Add("display", "");
            trCard2.Style.Add("display", "");
            txtCashReceived.Text = lblBillAmount.Text;
            RequiredFieldValidator7.Enabled = true;
        }
        if (Type == 3)
        {
            trApproval.Style.Add("display", "none");
            trCashReceipt.Style.Add("display", "none");
            trCreditCard.Style.Add("display", "none");
            trCard1.Style.Add("display", "none");
            trCard2.Style.Add("display", "none");
            dtLedger = objSalesInvoice.getAcFileBySundryDebtorOnly();
            txtCreditCarNo.Enabled = false;
            txtApprovelNO.Enabled = false;
            txtCashReceived.Text = "0.00";
            txtCashReturned.Text = "0.00";
            txtCashReceived.Enabled = false;
            txtCashReturned.Enabled = false;
            //  ddlCashReceipt.Focus();
            lblCashReceipt.Text = "Credit A/C ";

            RequiredFieldValidator7.Enabled = false;
            RequiredFieldValidator1.Enabled = false;
        }

        if (dtLedger.Rows.Count > 0)
        {
            ddlCashReceipt.DataSource = dtLedger;
            ddlCashReceipt.DataTextField = "Title";
            ddlCashReceipt.DataValueField = "Id";
            ddlCashReceipt.DataBind();
            //ddlCashReceipt.Items.Insert(0, new ListItem("Select  A/C", "0"));

        }



    }
    #endregion
    protected void ddlCashReceipt_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (rdbCash.Checked == true)
        {
            txtDescription.Text = "Inv. # : " + txtInvNo.Text + ", Dated : " + ClsGetDate.FillFromDate(txtInvoiceDate.Text) + ", Cash Sale";
        }

        if (rdbcashCredit.Checked == true)
        {
            txtDescription.Text = "C.C. Inv. #  : " + txtInvNo.Text + ", Dated : " + ClsGetDate.FillFromDate(txtInvoiceDate.Text) + ", Credit Card Sale. Credit Card # :" + txtCreditCarNo.Text + " Apr # " + txtApprovelNO.Text;
        }

        if (rdbCredit.Checked == true)
        {
            txtDescription.Text = "Inv. # : " + txtInvNo.Text + ", Dated : " + ClsGetDate.FillFromDate(txtInvoiceDate.Text) + ", Credit Sale";
        }
        txtDescription.Focus();
    }

    /// <summary>
    /// Bind Dr and cr
    /// </summary>
    #region Fill Accounts

    protected void FillAllLedgerAc()
    {

        objSalesInvoice.CustomerCode = decimal.Parse(Sessions.CustomerCode);
        DataTable dtLedger = new DataTable();
        dtLedger = objSalesInvoice.getAcFileAllLedger();
        if (dtLedger.Rows.Count > 0)
        {

            ddlDR.DataSource = dtLedger;
            ddlDR.DataTextField = "Title";
            ddlDR.DataValueField = "Id";
            ddlDR.DataBind();
            ddlDR.Items.Insert(0, new ListItem("Select", "0"));

            ddlCR.DataSource = dtLedger;
            ddlCR.DataTextField = "Title";
            ddlCR.DataValueField = "Id";
            ddlCR.DataBind();
            ddlCR.Items.Insert(0, new ListItem("Select", "0"));



        }



    }
    #endregion

    # region commented code



    # endregion

    protected void txtContactNumber_OnChanged(object sender, EventArgs e)
    {
        if (txtContactNumber.Text != "")
        {
            objSalesInvoice.ContactNumber = txtContactNumber.Text;
            DataTable dt = new DataTable();

            dt = objSalesInvoice.getCustomerSummeryByContactNumber();
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["CustomerName"].ToString() != "-")
                {
                    txtCustomerName.Text = Convert.ToString(dt.Rows[0]["CustomerName"]);
                }
                else
                {
                    txtCustomerName.Text = "";
                }
                if (dt.Rows[0]["ContactNumber"].ToString() != "-")
                {
                    txtContactNumber.Text = Convert.ToString(dt.Rows[0]["ContactNumber"]);
                }
                else
                {
                    txtContactNumber.Text = "";
                }
                if (dt.Rows[0]["EmailAddress"].ToString() != "-")
                {
                    txtEmailAddress.Text = Convert.ToString(dt.Rows[0]["EmailAddress"]);
                }
                else
                {
                    txtEmailAddress.Text = "";
                }
            }
            else
            {
                txtCustomerName.Text = "";
                // txtContactNumber.Text = "";
                txtEmailAddress.Text = "";
                txtCustomerName.Focus();
            }
            mpeAddDetails.Show();
            // AjaxControlToolkit.ModalPopupExtender mpepopup = (AjaxControlToolkit.ModalPopupExtender)rptReseller.Items[rpitem.ItemIndex].FindControl("mpeAddDoc");

        }


    }

    protected void txtRate_changed(object sender, EventArgs e)
    {

        AddDiscount();

        txtBoutiqueDiscount.Focus();
    }
    protected void txtBoutiqueDiscount_changed(object sender, EventArgs e)
    {

        AddDiscount();

        lblRateAfterDiscount.Focus();
    }
    protected void AddDiscount()
    {
        if (txtRate.Text != "")
        {
            if (Convert.ToDecimal(txtRate.Text) > 0)
            {
                lblRateAfterDiscount.Text = (Convert.ToDecimal(txtRate.Text) - (Convert.ToDecimal(txtRate.Text) * (Convert.ToDecimal(txtBoutiqueDiscount.Text) / 100))).ToString("0.00");
                txtAlterCharges.Focus();

            }

            else
            {
                lblRateAfterDiscount.Text = (Convert.ToDecimal(txtRate.Text)).ToString("0.00");
            }
        }
    }


    public void logMessage(string logMessage, string IpAddress)
    {
        FileStream fs = null;
        StreamWriter sw = null;
        string filePath = Server.MapPath("images\\") + IpAddress + "_log.txt";
        //try
        //{
            if (!File.Exists(filePath))
            {
                fs = File.Create(filePath);
                fs.Close();
            }
            sw = File.AppendText(filePath);
            sw.WriteLine(DateTime.Now.ToLongTimeString() + "|" + logMessage);
            sw.Flush();
            sw.Close();
        //}
        //catch { }
        //finally { fs = null; sw = null; };
    }
}