﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TMInnermaster.master" AutoEventWireup="true"
    CodeFile="SingleTMOppositionSearchClient.aspx.cs" Inherits="SingleTMOppositionSearchClient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphead" runat="Server">
    <asp:Label ID="lblTitle" runat="server" Text="Opposition Entry"></asp:Label>
    <script type="text/javascript">
        function Count(text, long) {
            var maxlength = new Number(long); // Change number to your max length.
            if (text.value.length > maxlength) {
                text.value = text.value.substring(0, maxlength);

            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainHome" runat="Server">
    <asp:UpdateProgress runat="server" ID="pageupdateprogress">
        <ProgressTemplate>
            <div style="text-align: center; position: fixed; width: 100%">
                <div style="position: static; width: 100%">
                    <div style="position: absolute; top: -100px; margin-left: 37%;">
                        <img src="images/loader.gif" />
                    </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updatepanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnOppositionMainId" runat="server" />
            <div style="width: 100%; float: left">
                <div style="float: left; width: 60%">
                    <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                        width="100%">
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                Opp.#/Rect.# :
                            </td>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 5px; text-align: left">
                                <table>
                                    <tr>
                                        <td align="left" valign="middle" style="padding: 2px 2px 2px 0px;">
                                            <asp:TextBox Enabled="false" ID="txtOppsitionNo" TabIndex="1" CssClass="input_box33"
                                                Style="float: left; text-transform: uppercase;" MaxLength="20" runat="server"></asp:TextBox>
                                            &nbsp; &nbsp; &nbsp;
                                        </td>
                                        <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                            <asp:DropDownList Enabled="false" Style="display: none" ID="ddlYear" TabIndex="2"
                                                runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; display: none">
                                            <span id="spanPgging" runat="server" style="padding: 0 20px 0 20px; font-weight: bold;
                                                font-size: 18px">
                                                <asp:LinkButton ID="lnkFirst" Text="|<" runat="server" OnClick="lnkFirst_Click"> </asp:LinkButton>
                                                <asp:LinkButton ID="lnkPrevious" Style="margin-left: 10px;" Text="<" runat="server"
                                                    OnClick="lnkPrevious_Click"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkNext" Text=">" Style="margin-left: 10px;" runat="server" OnClick="lnkNext_Click"></asp:LinkButton>
                                                <asp:LinkButton ID="lnLast" Text=">|" Style="margin-left: 10px;" runat="server" OnClick="lnLast_Click"></asp:LinkButton></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                          <tr>
                    <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                        File # :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                        <asp:TextBox Enabled="false" ID="txtFileNo" TabIndex="3" MaxLength="10" runat="server" Width="100px"
                            AutoPostBack="true"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" Display="Dynamic"
                            ErrorMessage="Please Enter  File # " Text="*" ControlToValidate="txtFileNo" ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                        Opposition Date :
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                        <asp:TextBox  Enabled="false" ID="txtOppositionDate" TabIndex="3" MaxLength="25" 
                            runat="server" Width="100px"></asp:TextBox>

                        <asp:RequiredFieldValidator ID="RequiredFieldValidator135" runat="server" Display="Dynamic"
                            ErrorMessage="Please Fill  Opposition Date." Text="*" ControlToValidate="txtOppositionDate"
                            ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtOppositionDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid  Opposition Date !!"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 110px; padding: 2px 2px 2px 8px;">
                                Opposition type :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:DropDownList Enabled="false" ID="ddlOppositiontype" TabIndex="3" runat="server"
                                    Style="width: 550px">
                                    <asp:ListItem Text="Applicant" Value="Applicant"></asp:ListItem>
                                    <asp:ListItem Text="Opponent" Value="Opponent"></asp:ListItem>
                                    <asp:ListItem Text="Registered Proprietor" Value="RegisteredProprietor"></asp:ListItem>
                                    <asp:ListItem Text="Applicant For Rectification" Value="ApplicantForRectification"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Status :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:DropDownList Enabled="false" ID="ddlstatusOpposition" TabIndex="4" runat="server"
                                    Style="width: 550px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Remarks:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtRemarksOpposition" onKeyUp="Count(this,250)"
                                    onChange="Count(this,250);capitalizeMe(this)" TabIndex="5" Rows="2" TextMode="MultiLine"
                                    Style="width: 544px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 110px; padding: 2px 2px 2px 8px; font-size: 18px; text-decoration: underline;">
                                <br />
                                <b>Trademark Details</b>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Application #:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtApplicationNo" onChange="isNumericint(this)"
                                    MaxLength="10" TabIndex="5" runat="server" AutoPostBack="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Our client:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:DropDownList Enabled="false" ID="ddlOurClient" runat="server" TabIndex="5" Style="width: 550px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" Display="Dynamic"
                                    ErrorMessage="Please Select Our client." Text="*" InitialValue="0" ControlToValidate="ddlOurClient"
                                    ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Trademark :
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txttradeMark" TabIndex="5" onChange="capitalizeMe(this)"
                                    TextMode="SingleLine" runat="server" MaxLength="100" Width="538px" CssClass="input_box2"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px; width: 10%">
                                Status:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:DropDownList Enabled="false" ID="ddlStatus" TabIndex="5" runat="server" Style="width: 550px">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Display="Dynamic"
                                    InitialValue="0" ErrorMessage="Please select Status" Text="*" ControlToValidate="ddlStatus"
                                    ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 110px; padding: 2px 2px 2px 8px; font-size: 18px; text-decoration: underline;">
                                <br />
                                <b>Journal Details</b>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                TradeMark Journal #:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtTradeMarkJournalNo" onChange="isNumericint(this)"
                                    MaxLength="10" TabIndex="6" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Date Of Journal:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtJournalDate" TabIndex="7" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtJournalDate'),this);"
                                    runat="server" Width="100px"></asp:TextBox>
                                <%-- <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                    onclick="scwShow(scwID('ctl00_MainHome_txtJournalDate'),this);" />--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                    ControlToValidate="txtJournalDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid  Journal Date !!"
                                    Display="None"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Date Of Publication:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtDateOfPublication" TabIndex="8" MaxLength="25"
                                    onclick="scwShow(scwID('ctl00_MainHome_txtDateOfPublication'),this);" runat="server"
                                    Width="100px"></asp:TextBox>
                                <%-- <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                    onclick="scwShow(scwID('ctl00_MainHome_txtDateOfPublication'),this);" />--%>
                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                    ErrorMessage="Please Fill  Publication Date." Text="*" ControlToValidate="txtDateOfPublication"
                                    ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                    ControlToValidate="txtDateOfPublication" ValidationGroup="AddInvoice" ErrorMessage="Invalid  Publication Date !!"
                                    Display="None"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                1st Extension:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtDateOfExtension" TabIndex="9" MaxLength="25"
                                    onclick="scwShow(scwID('ctl00_MainHome_txtDateOfExtension'),this);" runat="server"
                                    Width="100px"></asp:TextBox>
                                <%--    <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                    onclick="scwShow(scwID('ctl00_MainHome_txtDateOfExtension'),this);" />--%>
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator75" runat="server" Display="Dynamic"
                                    ErrorMessage="Please Fill  Extension Date." Text="*" ControlToValidate="txtDateOfExtension"
                                    ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator565" runat="server"
                                    ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                    ControlToValidate="txtDateOfExtension" ValidationGroup="AddInvoice" ErrorMessage="Invalid  Extension Date !!"
                                    Display="None"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                2nd Extension:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtDateOfExtension2" TabIndex="10" MaxLength="25"
                                    onclick="scwShow(scwID('ctl00_MainHome_txtDateOfExtension2'),this);" runat="server"
                                    Width="100px"></asp:TextBox>
                                <%--   <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                    onclick="scwShow(scwID('ctl00_MainHome_txtDateOfExtension2'),this);" />--%>
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="Dynamic"
                                    ErrorMessage="Please Fill 2nd Extension Date." Text="*" ControlToValidate="txtDateOfExtension2"
                                    ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                    ControlToValidate="txtDateOfExtension2" ValidationGroup="AddInvoice" ErrorMessage="Invalid 2nd Extension Date !!"
                                    Display="None"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Last Date Of Filling Opposition:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtLastDateOfOpposition" TabIndex="11" MaxLength="25"
                                    onclick="scwShow(scwID('ctl00_MainHome_txtLastDateOfOpposition'),this);" runat="server"
                                    Width="100px"></asp:TextBox>
                                <%-- <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                    onclick="scwShow(scwID('ctl00_MainHome_txtLastDateOfOpposition'),this);" />--%>
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic"
                                    ErrorMessage="Please Fill Last  Date Of Filling Opposition." Text="*" ControlToValidate="txtLastDateOfOpposition"
                                    ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                    ControlToValidate="txtLastDateOfOpposition" ValidationGroup="AddInvoice" ErrorMessage="Invalid  Last  Date Of Filling Opposition !!"
                                    Display="None"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 110px; padding: 2px 2px 2px 8px; font-size: 18px; text-decoration: underline;">
                                <br />
                                <b>Other Side TradeMark Details</b>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Application #:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtApplicationNoDetails" onChange="isNumericint(this)"
                                    MaxLength="10" TabIndex="16" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                TradeMark:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtTradeMarkDetails" MaxLength="100" TabIndex="17"
                                    runat="server" Style="width: 550px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Application Class:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtApplicationclassDetails" MaxLength="100" TabIndex="18"
                                    Style="width: 550px" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" Display="Dynamic"
                                ErrorMessage="Please Enter Application Class" Text="*" ControlToValidate="txtApplicationclassDetails"
                                ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Application Filled On:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtApplicationFilledOn" TabIndex="19" MaxLength="25"
                                    onclick="scwShow(scwID('ctl00_MainHome_txtApplicationFilledOn'),this);" runat="server"
                                    Width="100px"></asp:TextBox>
                                <%--  <img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                    onclick="scwShow(scwID('ctl00_MainHome_txtApplicationFilledOn'),this);" />--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                    ControlToValidate="txtApplicationFilledOn" ValidationGroup="AddInvoice" ErrorMessage="Invalid  Application Filled On !!"
                                    Display="None"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Period Of Use:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtApplicationUser" MaxLength="100" TabIndex="20"
                                    Style="width: 550px" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" Display="Dynamic"
                                ErrorMessage="Please Enter Period Of Use" Text="*" ControlToValidate="txtApplicationUser"
                                ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>

                          <tr>
                    <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                       Journal Date:
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                        <asp:TextBox ID="txtOtherJournalDate" Enabled="false" TabIndex="20" MaxLength="25"
                            runat="server" Width="100px"></asp:TextBox>
                       
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtOtherJournalDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid  Journal Date !!"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                 <tr>
                    <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                        Publication Date:
                    </td>
                    <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                        <asp:TextBox ID="txtOtherPublicationDate" Enabled="false" TabIndex="20" MaxLength="25" 
                            runat="server" Width="100px"></asp:TextBox>
                       
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                            ControlToValidate="txtApplicationFilledOn" ValidationGroup="AddInvoice" ErrorMessage="Invalid  Publication Date !!"
                            Display="None"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                        <tr>
                            <td colspan="2" style="width: 110px; padding: 2px 2px 2px 8px; font-size: 18px; text-decoration: underline;">
                                <br />
                                <b>Other Side Client Details</b>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Other Side Client Name:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtSideClientName" MaxLength="100" TabIndex="21"
                                    Style="width: 550px" runat="server"></asp:TextBox>
                                <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" Display="Dynamic"
                                ErrorMessage="Please Enter Other Side Client Name" Text="*" ControlToValidate="txtSideClientName"
                                ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Other Side Client Address:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtSideClientAddress" MaxLength="150" TabIndex="22"
                                    TextMode="MultiLine" Rows="3" Style="width: 550px" onKeyUp="Count(this,250)"
                                    onChange="Count(this,250);capitalizeMe(this)" runat="server"></asp:TextBox>
                                <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" Display="Dynamic"
                                ErrorMessage="Please Enter Other Side Client Address" Text="*" ControlToValidate="txtSideClientAddress"
                                ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 110px; padding: 2px 2px 2px 8px; font-size: 18px; text-decoration: underline;">
                                <br />
                                <b>Other Side Advocate Details</b>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Other Side Advocate Name:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtOtherSideAdvocateName" MaxLength="100" TabIndex="23"
                                    Style="width: 550px" runat="server"></asp:TextBox>
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" Display="Dynamic"
                                ErrorMessage="Please Enter   Other Side Advocate Name" Text="*" ControlToValidate="txtOtherSideAdvocateName"
                                ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Other Side Advocate Address:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtOtherSideAdvocateAddress" TextMode="MultiLine"
                                    Rows="3" MaxLength="150" TabIndex="24" onKeyUp="Count(this,250)" onChange="Count(this,250);capitalizeMe(this)"
                                    Style="width: 550px" runat="server"></asp:TextBox>
                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" Display="Dynamic"
                                ErrorMessage="Please Enter Other Side Advocate Address" Text="*" ControlToValidate="txtOtherSideAdvocateAddress"
                                ValidationGroup="AddInvoice"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 110px; padding: 2px 2px 2px 8px; font-size: 18px; text-decoration: underline;">
                                <br />
                                <b>Reminder</b>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Reminder Date:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtReminderDate" TabIndex="24" MaxLength="25" onclick="scwShow(scwID('ctl00_MainHome_txtReminderDate'),this);"
                                    runat="server" Width="100px"></asp:TextBox>
                                <%--<img src="images/Calendar.gif" style="vertical-align: top;" title='Click Here' alt='Click Here'
                                    onclick="scwShow(scwID('ctl00_MainHome_txtReminderDate'),this);" />--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="^\d{2}\s{1}(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s{1}\d{4}$"
                                    ControlToValidate="txtReminderDate" ValidationGroup="AddInvoice" ErrorMessage="Invalid   Reminder Date !!"
                                    Display="None"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="width: 72px; padding: 2px 2px 2px 8px;">
                                Reminder:
                            </td>
                            <td align="left" valign="top" style="padding: 2px 2px 2px 8px; text-align: left">
                                <asp:TextBox Enabled="false" ID="txtReminder" TextMode="MultiLine" Rows="3" MaxLength="150"
                                    TabIndex="24" onKeyUp="Count(this,250)" onChange="Count(this,250);capitalizeMe(this)"
                                    Style="width: 550px" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="trOpposition1" runat="server">
                            <td colspan="2" style="font-size: 18px; text-decoration: underline;">
                                <br />
                                <br />
                                <b>Opposition Proceedings - Details & History</b>
                            </td>
                        </tr>
                        <tr id="trOpposition2" runat="server">
                            <td colspan="2">
                                <div id="sundryDR" runat="server" style="width: 660px; padding: 10px;">
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                <asp:HiddenField ID="hdndetail2" runat="server" />
                                                <asp:DataGrid ID="dgGallery2" Width="500px" runat="server" AutoGenerateColumns="False"
                                                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                                    AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                                    BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery2_RowDataBound"
                                                    OnItemCommand="dgGallery2_ItemCommand">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="S.No.">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <%# Container.DataSetIndex + 1%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Date">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDate" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Remarks">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="left" Width="70%" />
                                                            <ItemTemplate>
                                                                <%# Eval("DetailsRemark")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr id="trHearing1" runat="server">
                            <td colspan="2" style="font-size: 18px; text-decoration: underline;">
                                <b>Hearing Details</b>
                            </td>
                        </tr>
                        <tr id="trHearing2" runat="server">
                            <td colspan="2">
                                <div id="Div1" runat="server" style="width: 660px; padding: 10px;">
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                <asp:HiddenField ID="hdndetail" runat="server" />
                                                <asp:DataGrid ID="dgGallery" Width="500px" runat="server" AutoGenerateColumns="False"
                                                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                                    AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                                    BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgGallery_RowDataBound"
                                                    OnItemCommand="dgGallery_ItemCommand">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="S.No.">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <%# Container.DataSetIndex + 1%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Hearing Date">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="left" Width="10%" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblHearingDate" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Before">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="left" Width="20%" />
                                                            <ItemTemplate>
                                                                <%# Eval("Before")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Comments/Arguments">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="left" Width="45%" />
                                                            <ItemTemplate>
                                                                <%# Eval("Comment")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr id="Document1" runat="server">
                            <td colspan="2" style="font-size: 18px; text-decoration: underline;">
                                <b>Opposition Document</b>
                            </td>
                        </tr>
                        <tr id="Document2" runat="Server">
                            <td colspan="2">
                                <div id="Div2" runat="server" style="width: 660px; padding: 10px;">
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                <asp:DataGrid ID="dgdocument" Width="500px" runat="server" AutoGenerateColumns="False"
                                                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                                    AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                                    BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgdocument_RowDataBound">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="S.No.">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Top" />
                                                            <ItemTemplate>
                                                                <%# Container.DataSetIndex + 1%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Image/Document">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="left" Width="20%" VerticalAlign="Top" />
                                                            <ItemTemplate>
                                                                <div class="slide" id="d1" runat="server">
                                                                    <div class="highslide-gallery" id="Naveen" runat="server" style="position: relative;
                                                                        z-index: 50;">
                                                                        <a id="thumb1" href="<%# "images/TMImages/"  + Eval("Image") %>" class="highslide"
                                                                            onclick="return hs.expand(this)">
                                                                            <img id="i1" src='<%# "images/TMImages/"+ Eval("Image") %>' width="100" height="100"
                                                                                alt="." border="0" /></a>
                                                                    </div>
                                                                </div>
                                                                <div id="imageDocument" style="display: none" runat="server">
                                                                    <a href="<%# "images/TMImages/"+ Eval("Image") %>" target="_blank">Download </a>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--    <asp:TemplateColumn HeaderText="Show To Client">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="left" Width="15%" VerticalAlign="Top" />
                                                            <ItemTemplate>
                                                                <%# Eval("IsShowtoClient")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>--%>
                                                        <asp:TemplateColumn HeaderText="Remarks">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="left" Width="50%" VerticalAlign="Top" />
                                                            <ItemTemplate>
                                                                <%# Eval("Remark")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr id="trForm1" runat="server">
                            <td colspan="2" style="font-size: 18px; text-decoration: underline;">
                                <b>Form Entries</b>
                            </td>
                        </tr>
                        <tr id="trForm2" runat="server">
                            <td colspan="2">
                                <div id="dvForm" runat="server" style="width: 660px; padding: 10px;">
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                <asp:DataGrid ID="dgForm" Width="500px" runat="server" AutoGenerateColumns="False"
                                                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                                    AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                                    BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem" OnItemDataBound="dgForm_RowDataBound">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="S.No.">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle Width="5%" HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <%# Container.DataSetIndex + 1%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Form #">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="left" Width="30%" />
                                                            <ItemTemplate>
                                                                <%# Eval("FormName")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Filling Date">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="left" Width="10%" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFillingDate" runat="server"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Remarks">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="left" Width="50%" />
                                                            <ItemTemplate>
                                                                <%# Eval("Comment")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td style="width: 435px;">
                                            <span>
                                                <%--   <asp:Button ID="lblbutton" OnClientClick="location.href='Opposition.aspx?addnew=1'"
                                            Style="cursor: pointer" runat="server" Text="Add New" />--%>
                                                <%-- <asp:Button ID="lblbutton" Style="cursor: pointer" runat="server" Text="Add New"
                                                    OnClick="lblbutton_Click" />--%>
                                                <%--  <asp:Button Text="Delete" OnClientClick="return askDeleteInvoice();" ID="btnDeleteInvoice"
                                                    runat="server" OnClick="btnDeleteInvoice_Click" />--%>
                                            </span>&nbsp;&nbsp;&nbsp;&nbsp; <span id="spanLastInvoice" runat="server" style="display: none">
                                                <b>Last Inv No:
                                                    <asp:Literal ID="litLastInvoiceNo" runat="server"></asp:Literal>&nbsp;&nbsp;<asp:Literal
                                                        ID="litLastInvDate" runat="server"></asp:Literal></b></span>
                                        </td>
                                        <td style="width: 400px">
                                            <table cellpadding="0" cellspacing="0" style="padding: 5px 0px 0px 0px; width: 100%">
                                                <tr>
                                                    <td align="right" valign="top" style="padding: 2px 2px 2px 8px; width: 33%;">
                                                    </td>
                                                    <td align="left" valign="top" style="padding: 2px 0px 2px 5px; text-align: right;
                                                        width: 33%;">
                                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="true"
                                                            ShowSummary="false" ValidationGroup="AddInvoice" />
                                                        <%--<asp:ImageButton ID="ImageButton2" TabIndex="32" runat="server" ImageUrl="~/Images/btn-save.gif"
                                                            OnClick="btn_saveInvoice_Click" ValidationGroup="AddInvoice" />--%>
                                                        &nbsp;
                                                    </td>
                                                    <td style="width: 33%; padding: 2px 2px 20px 3px;">
                                                        <%--  <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/bt-cancal.gif"
                                                            OnClick="btn_cancel_Click" />--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="float: left; width: 37%">
                    <table cellspacing="0" style="float: left; padding: 10px;" border="0" cellpadding="0"
                        width="100%">
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                Search By :<asp:DropDownList ID="ddlSearchBy" runat="server">
                                    <asp:ListItem Text="Opp.#/Rect.#" Value="OppRect"></asp:ListItem>
                                    <asp:ListItem Text="TradeMark" Value="TradeMark"></asp:ListItem>
                                    <asp:ListItem Text="Application #" Value="ApplicationNum"></asp:ListItem>
                                    <asp:ListItem Text="View All" Value="ViewAll"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                                <asp:ValidationSummary ID="ValidationSummary4" runat="server" ShowMessageBox="true"
                                    ShowSummary="false" ValidationGroup="Serch" />
                                <asp:Button Text="Search" ID="btnSearch" ValidationGroup="Serch" OnClick="btnSearch_Click"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="middle" style="padding: 2px 2px 2px 8px;">
                                <asp:DataGrid ID="dgagentSearch" Width="380px" runat="server" AutoGenerateColumns="False"
                                    AllowPaging="false" PageSize="30" PagerSettings-Mode="Numeric" HeaderStyle-CssClass="gridheader"
                                    AlternatingItemStyle-CssClass="gridAlternateItem" GridLines="none" BorderColor="black"
                                    OnItemCommand="grdCustomer_RowCommand" BorderStyle="Dotted" BorderWidth="1" ItemStyle-CssClass="gridItem">
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Opposition #">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="15%" HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <%# Eval("OppositionNo")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Customer Name">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="25%" HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <%# Eval("TmCustomerName")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="TradeMark">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="20%" HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <%# Eval("TradeMark")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Application #">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="15%" HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <%# Eval("ApplicationNum")%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Action">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle Width="15%" HorizontalAlign="left" />
                                            <ItemTemplate>
                                                <asp:Button ID="Button1" Style='<%# Convert.ToString(Eval("TmOppositionId"))!="" ?"display:": "display:none" %>'
                                                    Text="View " CommandName="Edit" CommandArgument='<%#Eval("TmOppositionId") %>'
                                                    runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
